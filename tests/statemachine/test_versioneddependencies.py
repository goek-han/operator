#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import itertools
import os
import random
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import semver

import yaook.statemachine.versioneddependencies as versioneddependencies
import yaook.statemachine.version_utils as version_utils


class MockVersionedDependency(
        versioneddependencies.VersionedDependency):
    def __init__(self, versions,
                 version_selector=None,
                 **kwargs):
        if version_selector is None:
            version_selector = unittest.mock.Mock(
                versioneddependencies.VersionSelector
            )
            version_selector.filter = lambda vs: filter(
                version_selector.selects_version,
                vs)
            version_selector.version_format = unittest.mock.Mock(
                versioneddependencies.VersionFormat,
            )
        super().__init__(url="myurl",
                         version_selector=version_selector,
                         **kwargs)
        self.versions = versions

    async def _get_available_versions(self):
        return self.versions

    async def _get_versioned_url(self):
        raise NotImplementedError()


class TestLoadPinFile(unittest.IsolatedAsyncioTestCase):

    async def test__load_pin_file(self):
        versioneddependencies._load_pin_file.cache_clear()
        mo = unittest.mock.mock_open(read_data="myurl: 1.3.4\n")
        with unittest.mock.patch("builtins.open", mo, create=True):
            self.assertEqual(versioneddependencies._load_pin_file(),
                             {"myurl": "1.3.4"})

    async def test__load_pin_file_not_found(self):
        versioneddependencies._load_pin_file.cache_clear()
        mo = unittest.mock.mock_open()
        mo.side_effect = FileNotFoundError
        with unittest.mock.patch("builtins.open", mo, create=True):
            self.assertEqual(versioneddependencies._load_pin_file(), {})


class TestVersionSelector(unittest.TestCase):
    class VersionSelectorTest(versioneddependencies.VersionSelector):
        def selects_version(self, version) -> bool:
            raise NotImplementedError

    def setUp(self):
        self.vst = self.VersionSelectorTest()

    def test_filter_filters_for_selected_versions(self):
        with contextlib.ExitStack() as stack:
            filter_ = stack.enter_context(unittest.mock.patch(
                "builtins.filter"
            ))

            self.vst.filter(sentinel.input_)

        filter_.assert_called_once_with(
            self.vst.selects_version,
            sentinel.input_,
        )


@ddt.ddt
class TestSemVer(unittest.TestCase):
    def setUp(self):
        self.sv = versioneddependencies.SemVer()

    @unittest.mock.patch("semver.VersionInfo.parse")
    def test_parse_uses_semver(self, parse_version_info):
        result = self.sv.parse(str(sentinel.version_s))
        parse_version_info.assert_called_once_with(str(sentinel.version_s))
        self.assertEqual(result, parse_version_info())

    def test_serialize_uses_str(self):
        m = unittest.mock.MagicMock()
        result = self.sv.serialize(m)
        m.__str__.assert_called_once_with()
        self.assertEqual(result, m.__str__())

    def test_serialize_adds_prefix(self):
        sver = versioneddependencies.SemVer(prefix="myprefix-")
        ver = semver.VersionInfo(1, 2)
        self.assertEqual(
            "myprefix-1.2.0",
            sver.serialize(ver)
        )

    def test_serialize_adds_suffix(self):
        sver = versioneddependencies.SemVer(suffix="-management")
        ver = semver.VersionInfo(1, 2)
        self.assertEqual(
            "1.2.0-management",
            sver.serialize(ver)
        )

    @ddt.data(
        ("1.2.0", semver.VersionInfo(1, 2)),
        ("1.2.0-pre", semver.VersionInfo(1, 2, 0, "pre")),
    )
    def test_parse_valid_versions(self, data):
        instr, expected = data
        self.assertEqual(
            self.sv.parse(instr),
            expected,
        )

    @ddt.data(
        ("myprefix-1.2.0", semver.VersionInfo(1, 2)),
        ("myprefix-1.2.0-pre", semver.VersionInfo(1, 2, 0, "pre")),
    )
    def test_parse_valid_versions_with_prefix(self, data):
        semver = versioneddependencies.SemVer(prefix="myprefix-")
        instr, expected = data
        self.assertEqual(
            semver.parse(instr),
            expected,
        )

    @ddt.data(
        ("1.2.0-management", semver.VersionInfo(1, 2)),
        ("1.2.0-pre-management", semver.VersionInfo(1, 2, 0, "pre")),
    )
    def test_parse_valid_versions_with_suffix(self, data):
        semver = versioneddependencies.SemVer(suffix="-management")
        instr, expected = data
        self.assertEqual(
            semver.parse(instr),
            expected,
        )

    def test_parse_with_prefix_raises(self):
        semver = versioneddependencies.SemVer(prefix="myprefix-")
        self.assertRaises(ValueError, semver.parse, "1.2.3")

    def test_parse_with_suffix_raises(self):
        semver = versioneddependencies.SemVer(suffix="-management")
        self.assertRaises(ValueError, semver.parse, "1.2.3")


@ddt.ddt
class TestSemVerSelector(unittest.TestCase):
    @ddt.unpack
    @ddt.data(
        ("1.2.0", [">=1.2.0", "<2.0.0"], True),
        ("1.2.0", [">1.2.0", "<2.0.0"], False),
        ("1.2.0-prerelease", [">=1.2.0", "<2.0.0"], False),
    )
    def test_selects_version_matches_against_all_requirements(
            self,
            version_s,
            requirements,
            passes):
        vs = versioneddependencies.SemVerSelector(requirements)

        self.assertEqual(
            vs.selects_version(vs.version_format.parse(version_s)),
            passes,
        )

    def test_selects_version_uses_match_on_all_requirements(self):
        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = False
        version.match.return_value = True
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.SemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertIn(unittest.mock.call(sentinel.req1),
                      version.match.mock_calls)
        self.assertIn(unittest.mock.call(sentinel.req2),
                      version.match.mock_calls)
        self.assertIn(unittest.mock.call(sentinel.req3),
                      version.match.mock_calls)

        self.assertTrue(result)

    @ddt.data(0, 1, 2)
    def test_selects_version_returns_false_if_any_match_returns_false(
            self,
            nth):
        def return_false_on_nth():
            for i in itertools.count():
                yield i != nth

        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = False
        version.match.side_effect = return_false_on_nth()
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.SemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertFalse(result)

    def test_selects_version_returns_false_if_prelease(self):
        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = "nonempty"
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.SemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertFalse(result)
        version.match.assert_not_called()

    @ddt.unpack
    @ddt.data(
        (">=1.0.0", True),
        ("<2.0.0", True),
        ("!=1.2.3", True),
        (">=2.0.0", False),
        ("==1.2.3", False),
    )
    def test_selects_version_with_examples(self, requirement, result):
        sv = versioneddependencies.SemVerSelector([requirement])
        ver = sv.version_format.parse("1.2.4")
        self.assertEqual(sv.selects_version(ver), result)


@ddt.ddt
class TestYaookSemVerSelector(unittest.TestCase):
    def test_selects_version_uses_match_on_all_requirements(self):
        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = False
        version.match.return_value = True
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.YaookSemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertIn(unittest.mock.call(sentinel.req1),
                      version.match.mock_calls)
        self.assertIn(unittest.mock.call(sentinel.req2),
                      version.match.mock_calls)
        self.assertIn(unittest.mock.call(sentinel.req3),
                      version.match.mock_calls)

        self.assertTrue(result)

    @ddt.data(0, 1, 2)
    def test_selects_version_returns_false_if_any_match_returns_false(
            self,
            nth):
        def return_false_on_nth():
            for i in itertools.count():
                yield i != nth

        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = False
        version.match.side_effect = return_false_on_nth()
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.YaookSemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertFalse(result)

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_USE_ALPHA": "0",
        "YAOOK_OP_VERSIONS_USE_ROLLING": "0"})
    def test_selects_version_returns_false_if_prelease(self):
        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = "nonempty"
        reqs = [sentinel.req1, sentinel.req2, sentinel.req3]

        vs = versioneddependencies.YaookSemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertFalse(result)
        version.match.assert_not_called()

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_USE_ALPHA": "0",
        "YAOOK_OP_VERSIONS_USE_ROLLING": "1"})
    def test_selects_version_uses_match_on_rolling_if_rolling_allowed(self):
        version = unittest.mock.Mock(["match", "prerelease"])
        version.prerelease = "rolling.123"
        version.match.return_value = True
        reqs = [sentinel.req1]

        vs = versioneddependencies.YaookSemVerSelector(reqs)

        result = vs.selects_version(version)

        self.assertTrue(result)
        version.match.assert_called_once_with(sentinel.req1)

    @ddt.unpack
    @ddt.data(
        ("1.2.4-alpha", [">=1.0.0", "<2.0.0", "!=1.2.3", "==1.2.4",
                         "==1.2.4-alpha"]),
        ("1.2.4-rolling.1", [">=1.0.0", "<2.0.0", "!=1.2.3", "==1.2.4",
                             "==1.2.4-rolling.1"]),
    )
    @unittest.mock.patch.dict(os.environ, {}, clear=True)
    def test__filter_version_prerelease_default(
            self,
            version_s, req_candidates):
        version = versioneddependencies.SemVer().parse(version_s)
        for requirement in req_candidates:
            vs = versioneddependencies.YaookSemVerSelector([requirement])
            self.assertFalse(vs.selects_version(version))

    @ddt.unpack
    @ddt.data(
        ("1.2.4-alpha", [">=1.0.0", "<2.0.0", "!=1.2.3",
                         "==1.2.4-alpha"], True),
        ("1.2.4-alpha", ["==1.2.3", ">=2.0.0", "==1.2.4"], False),
    )
    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_USE_ALPHA": "1"}, clear=True)
    def test__filter_version_alpha(
            self,
            version_s, req_candidates, result):
        version = versioneddependencies.SemVer().parse(version_s)
        for requirement in req_candidates:
            vs = versioneddependencies.YaookSemVerSelector([requirement])
            self.assertEqual(vs.selects_version(version), result)

    @ddt.unpack
    @ddt.data(
        ("1.2.4-rolling.1", [">=1.0.0", "<2.0.0", "!=1.2.3",
                             "==1.2.4-rolling.1"], True),
        ("1.2.4-rolling.1", ["==1.2.3", ">=2.0.0", "==1.2.4"], False),
    )
    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_USE_ROLLING": "1"}, clear=True)
    def test__filter_version_rolling(
            self,
            version_s, req_candidates, result):
        version = versioneddependencies.SemVer().parse(version_s)
        for requirement in req_candidates:
            vs = versioneddependencies.YaookSemVerSelector([requirement])
            self.assertEqual(vs.selects_version(version), result)


@ddt.ddt
class TestBitnamiVersion(unittest.TestCase):
    def setUp(self):
        self.sv = versioneddependencies.BitnamiVersion()

    def test_parse_splits_in_two_versions(self):
        semver_software, semver_os = self.sv.parse("1.2.3-debian-23-r42")
        self.assertEqual(
            semver_software,
            semver.VersionInfo(1, 2, 3),
        )
        self.assertEqual(
            semver_os,
            semver.VersionInfo(23, 42),
        )

    def test_serialise_recombines_versions(self):
        result = self.sv.serialize(
            (semver.VersionInfo(1, 2, 3), semver.VersionInfo(23, 42)),
        )

        self.assertEqual(
            result,
            "1.2.3-debian-23-r42",
        )

    @ddt.data(
        ("1.2.3-debian-10-r1"),
        ("10.2.4-debian-9-r1485"),
    )
    def test_parse_serialize_roundtrip(self, version_s):
        self.assertEqual(
            version_s,
            self.sv.serialize(self.sv.parse(version_s)),
        )

    def test_preserves_order(self):
        sorted_version_strings = [
            ("9.0.0-debian-11-r101"),
            ("10.0.5-debian-9-r100"),
            ("10.0.5-debian-10-r23"),
            ("10.0.5-debian-10-r42"),
            ("10.1.5-debian-9-r1"),
        ]

        shuffled_version_strings = list(sorted_version_strings)
        random.shuffle(shuffled_version_strings)

        reversed_version_strings = reversed(sorted_version_strings)

        sorted_versions = [
            self.sv.parse(v)
            for v in sorted_version_strings
        ]
        shuffled_versions = [
            self.sv.parse(v)
            for v in shuffled_version_strings
        ]
        reversed_versions = [
            self.sv.parse(v)
            for v in reversed_version_strings
        ]

        sorted_shuffled_versions = sorted(shuffled_versions)
        sorted_reversed_versions = sorted(reversed_versions)

        self.assertSequenceEqual(
            sorted_shuffled_versions,
            sorted_versions,
        )
        self.assertSequenceEqual(
            sorted_reversed_versions,
            sorted_versions,
        )
        self.assertSequenceEqual(
            [self.sv.serialize(v) for v in sorted_shuffled_versions],
            sorted_version_strings,
        )
        self.assertSequenceEqual(
            [self.sv.serialize(v) for v in sorted_reversed_versions],
            sorted_version_strings,
        )


@ddt.ddt
class TestBitnamiVersionSelector(unittest.TestCase):
    @ddt.unpack
    @ddt.data(
        ("1.0.0-debian-10-r38", False),
        ("1.0.0-debian-10-r39", True),
        ("1.0.1-debian-10-r1", True),
        ("1.0.1-debian-10-r38", True),
        ("1.0.1-debian-10-r39", True),
        ("1.1.0-debian-10-r1", True),
        ("1.1.0-debian-10-r38", True),
        ("1.1.0-debian-10-r39", True),
        ("2.0.0-debian-10-r1", False),
    )
    def test_selects_version_uses_disjunctions_of_conjunctions(
            self,
            version_s, expected_acceptance):
        bvs = versioneddependencies.BitnamiVersionSelector([
            (["==1.0.0"], [">=10.39.0"]),
            ([">1.0.0", "<2.0.0"], []),
        ])

        self.assertEqual(
            bvs.selects_version(bvs.version_format.parse(version_s)),
            expected_acceptance,
        )


@ddt.ddt
class TestVersionedDependency(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.dep = MockVersionedDependency([])

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_get_latest_version(self, _load_pin_file):
        _load_pin_file.return_value = {}
        dep = MockVersionedDependency([
            "0.0.1",
            "0.1.0",
            "0.1.1",
            "1.0.0",
            "1.0.2",
            "2.1.3",
            "devel",
            "-1,3.2.5",
        ])
        dep.version_selector.version_format = versioneddependencies.SemVer()
        ver = await dep.get_latest_version(sentinel.ctx)
        self.assertEqual(ver, "2.1.3")

    async def test__get_valid_versions_parses_versions(self):
        def parse_impl(v):
            return str(v)

        with contextlib.ExitStack() as stack:
            _get_available_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep,
                    "_get_available_versions",
                )
            )
            _get_available_versions.return_value = [
                sentinel.av1,
                sentinel.av2,
                sentinel.av3,
                sentinel.av4,
            ]

            self.dep.version_selector.version_format.parse.side_effect =\
                parse_impl

            result = await self.dep._get_valid_versions()

        self.assertCountEqual(
            [
                "sentinel.av1",
                "sentinel.av2",
                "sentinel.av3",
                "sentinel.av4",
            ],
            result,
        )
        self.assertIn(
            unittest.mock.call(sentinel.av1),
            self.dep.version_selector.version_format.parse.mock_calls,
        )
        self.assertIn(
            unittest.mock.call(sentinel.av2),
            self.dep.version_selector.version_format.parse.mock_calls,
        )
        self.assertIn(
            unittest.mock.call(sentinel.av3),
            self.dep.version_selector.version_format.parse.mock_calls,
        )
        self.assertIn(
            unittest.mock.call(sentinel.av4),
            self.dep.version_selector.version_format.parse.mock_calls,
        )

    async def test__get_valid_versions_drops_unparseable_versions(self):
        def parse_impl(v):
            if v == sentinel.av2:
                raise ValueError
            return str(v)

        with contextlib.ExitStack() as stack:
            _get_available_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep,
                    "_get_available_versions",
                )
            )
            _get_available_versions.return_value = [
                sentinel.av1,
                sentinel.av2,
                sentinel.av3,
                sentinel.av4,
            ]

            self.dep.version_selector.version_format.parse.side_effect =\
                parse_impl

            result = await self.dep._get_valid_versions()

        self.assertCountEqual(
            [
                "sentinel.av1",
                "sentinel.av3",
                "sentinel.av4",
            ],
            result,
        )

    async def test_get_latest_version_prefers_pin_if_available(self):
        with contextlib.ExitStack() as stack:
            _load_pin_file = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.versioneddependencies._load_pin_file"
            ))
            _load_pin_file.return_value = {"myurl": sentinel.pinned_version}

            dep = MockVersionedDependency([])

        self.assertEqual(
            await dep.get_latest_version(sentinel.ctx),
            sentinel.pinned_version,
        )

    @ddt.unpack
    @ddt.data(
        (11, 10),
        (10, 5),
        (6, 5),
        (5, 4),
        (3, 2),
    )
    async def test_get_latest_version_returns_highest_selected_version(
            self,
            less_than, expected_result):
        def selects_version_impl(v):
            return v < less_than

        numbers = [
            3, 2, 10, 4, 5,
        ]
        valid_calls = [
            unittest.mock.call(number)
            for number in numbers
        ]

        with contextlib.ExitStack() as stack:
            _get_valid_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_valid_versions"
                )
            )
            _get_valid_versions.return_value = list(numbers)

            self.dep.version_selector.selects_version.side_effect = \
                selects_version_impl
            self.dep.version_selector.version_format.serialize.return_value = \
                sentinel.serialized_version

            result = await self.dep.get_latest_version(sentinel.ctx)

        _get_valid_versions.assert_awaited_once_with()

        self.assertTrue(self.dep.version_selector.selects_version.mock_calls)
        for call in self.dep.version_selector.selects_version.mock_calls:
            self.assertIn(call, valid_calls)

        self.assertEqual(result, sentinel.serialized_version)
        self.dep.version_selector.version_format.serialize\
            .assert_called_once_with(expected_result)

    async def test_get_latest_version_remembers_result(self):
        def selects_version_impl(v):
            return v == 5

        with contextlib.ExitStack() as stack:
            _get_valid_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_valid_versions"
                )
            )
            _get_valid_versions.return_value = [
                3, 2, 10, 4, 5,
            ]

            self.dep.version_selector.selects_version.side_effect = \
                selects_version_impl
            self.dep.version_selector.version_format.serialize.return_value = \
                sentinel.serialized_version

            await self.dep.get_latest_version(sentinel.ctx)

            self.dep.version_selector.selects_version.reset_mock()
            self.dep.version_selector.version_format.serialize.reset_mock()
            _get_valid_versions.reset_mock()

            result = await self.dep.get_latest_version(sentinel.ctx)

        self.assertEqual(result, sentinel.serialized_version)
        self.dep.version_selector.version_format.serialize.assert_not_called()
        self.dep.version_selector.selects_version.assert_not_called()

    async def test_get_latest_version_raises_KeyError_if_no_matching_version_found(self):  # noqa:E501
        with contextlib.ExitStack() as stack:
            _get_valid_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_valid_versions"
                )
            )
            _get_valid_versions.return_value = [
                3, 2, 10, 4, 5,
            ]

            self.dep.version_selector.selects_version.return_value = False

            with self.assertRaises(KeyError):
                await self.dep.get_latest_version(sentinel.ctx)

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_get_latest_version_respects_pin(self, _load_pin_file):
        _load_pin_file.return_value = {"myurl": "1.3.4"}
        dep = MockVersionedDependency([
            "1.0.2",
            "2.1.3",
        ])
        ver = await dep.get_latest_version(sentinel.ctx)
        self.assertEqual(ver, "1.3.4")

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_get_latest_version_respects_ignore_pin(
            self, _load_pin_file):
        _load_pin_file.return_value = {"myurl": "1.3.4"}
        dep = MockVersionedDependency([
            "1.0.2",
            "2.1.3",
        ])
        dep.version_selector.version_format = versioneddependencies.SemVer()
        ver = await dep.get_latest_version(sentinel.ctx, ignore_pin=True)
        self.assertEqual(ver, "2.1.3")

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_get_latest_version_throws_key_error_if_none_found(
            self, _load_pin_file):
        _load_pin_file.return_value = {}
        dep = MockVersionedDependency(
            [
                "1.0.2",
                "2.1.3",
            ],
            version_selector=versioneddependencies.SemVerSelector([
                "<1.0.0",
            ]),
        )
        with self.assertRaises(KeyError):
            await dep.get_latest_version(sentinel.ctx, ignore_pin=True)

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_get_latest_version_respects_selector(
            self, _load_pin_file):
        _load_pin_file.return_value = {}
        dep = MockVersionedDependency(
            [
                "0.0.1",
                "0.1.0",
                "0.1.1",
                "1.0.0",
                "1.0.2",
                "2.1.3",
                "devel",
                "-1,3.2.5",
            ],
            version_selector=versioneddependencies.SemVerSelector([
                ">=1.0.1",
                "<2.0.0",
            ]),
        )
        ver = await dep.get_latest_version(sentinel.ctx)
        self.assertEqual(ver, "1.0.2")

    def test__get_override_version_nothing_without_variable(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        self.assertIsNone(dep._get_override_version())

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_OVERRIDE": "abc:\ndef"})
    def test__get_override_version_raises_wrong_variable(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        with self.assertRaisesRegex(
                ValueError,
                "Provided version override environment variable is not valid "
                "yaml.*"):
            dep._get_override_version()

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_OVERRIDE": "abcdef"})
    def test__get_override_version_raises_wrong_variable_type(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        with self.assertRaisesRegex(
                ValueError,
                "provided version override environment "
                "variable is not a valid dictionary.*"):
            dep._get_override_version()

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_OVERRIDE": "abc: def"})
    def test__get_override_version_ignored_wrong_url(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        self.assertIsNone(dep._get_override_version())

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_OVERRIDE": "myurl: this.is/great:123"})
    def test__get_override_version_matching_url(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        self.assertEqual("this.is/great:123", dep._get_override_version())

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_VERSIONS_OVERRIDE":
            "test: 123\nmyurl: this.is/great:123\nabc: def"})
    def test__get_override_version_matching_url_multiline(self):
        versioneddependencies._get_override_versions.cache_clear()
        dep = MockVersionedDependency(
            [],
            version_selector=versioneddependencies.SemVerSelector([]),
        )
        self.assertEqual("this.is/great:123", dep._get_override_version())


class TestVersionedDockerImage(unittest.IsolatedAsyncioTestCase):
    DOCKER_CONFIG_PATH = str(sentinel.dockerconfig)

    def setUp(self):
        self._env_patcher = unittest.mock.patch.dict(
            os.environ, {
                "YAOOK_OP_DOCKER_CONFIG": self.DOCKER_CONFIG_PATH
            })
        self._env_patcher.start()

        self.version_selector = unittest.mock.Mock(
            versioneddependencies.VersionSelector,
        )

        self.dep = versioneddependencies.VersionedDockerImage(
            "test.domain.org/test/image/path",
            version_selector=self.version_selector,
        )

    def tearDown(self):
        self._env_patcher.stop()

    def test__get_credentials(self):
        mo = unittest.mock.mock_open(
            read_data='{"auths":{"testdomain":{"auth":'
                      '"dXNlcm5hbWU6cGFzc3dvcmQ="}}}')
        with unittest.mock.patch("builtins.open", mo, create=True):
            self.assertEqual(
                self.dep._get_credentials("testdomain"),
                ("username", "password"),
            )
            mo.assert_called_with(self.DOCKER_CONFIG_PATH)

    def test__get_credentials_wrong_domain(self):
        mo = unittest.mock.mock_open(
            read_data='{"auths":{"testdomain":{"auth":'
                      '"dXNlcm5hbWU6cGFzc3dvcmQ="}}}')
        with unittest.mock.patch("builtins.open", mo, create=True):
            self.assertRaises(
                KeyError,
                self.dep._get_credentials,
                "someotherdomain",
            )
            mo.assert_called_with(self.DOCKER_CONFIG_PATH)

    def test__get_credentials_no_file_specified(self):
        # This removes the YAOOK_OP_DOCKER_CONFIG env variable
        self._env_patcher.stop()

        self.assertRaises(
            KeyError,
            self.dep._get_credentials,
            "testdomain",
        )

    @unittest.mock.patch("dxf.DXF")
    def test__get_image_versions(self, DXF):
        DXF().list_aliases.return_value = sentinel.tags
        DXF.reset_mock()

        ver = self.dep._get_image_versions(sentinel.auth)

        DXF.assert_called_once_with(
            "test.domain.org",
            "test/image/path",
            auth=sentinel.auth,
        )
        self.assertEqual(ver, sentinel.tags)
        DXF().list_aliases.assert_called_once_with()

    @unittest.mock.patch("dxf.DXF")
    def test__get_image_versions_uses_registry_docker_io_for_hostless_images(
            self,
            DXF):
        DXF().list_aliases.return_value = sentinel.tags
        DXF.reset_mock()

        dep = versioneddependencies.VersionedDockerImage(
            "test/image",
            version_selector=self.version_selector,
        )
        ver = dep._get_image_versions(sentinel.auth)

        DXF.assert_called_once_with(
            "registry-1.docker.io",
            "test/image",
            auth=sentinel.auth,
        )
        self.assertEqual(ver, sentinel.tags)
        DXF().list_aliases.assert_called_once_with()

    @unittest.mock.patch("dxf.DXF")
    def test__get_image_versions_uses_registry_docker_io_for_docker_io(
            self,
            DXF):
        DXF().list_aliases.return_value = sentinel.tags
        DXF.reset_mock()

        dep = versioneddependencies.VersionedDockerImage(
            "docker.io/test/image",
            version_selector=self.version_selector,
        )
        ver = dep._get_image_versions(sentinel.auth)

        DXF.assert_called_once_with(
            "registry-1.docker.io",
            "test/image",
            auth=sentinel.auth,
        )
        self.assertEqual(ver, sentinel.tags)
        DXF().list_aliases.assert_called_once_with()

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         ".VersionedDockerImage._get_image_versions")
    async def test__get_available_versions_auth_helper_passes_none_to_authenticate_if_no_credentials_are_present(  # noqa:E501
            self,
            _get_image_versions):
        with contextlib.ExitStack() as stack:
            _get_credentials = stack.enter_context(unittest.mock.patch.object(
                self.dep, "_get_credentials",
            ))
            _get_credentials.side_effect = KeyError

            _get_image_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_image_versions",
                )
            )
            _get_image_versions.return_value = sentinel.tags

            ver = await self.dep._get_available_versions()
            self.assertEqual(ver, sentinel.tags)
            _get_image_versions.assert_called_with(unittest.mock.ANY)

        auth_helper = _get_image_versions.mock_calls[0][1][0]
        whatever_d_is = unittest.mock.Mock(["authenticate"])
        auth_helper(whatever_d_is, sentinel.response)
        whatever_d_is.authenticate.assert_called_once_with(
            None,
            None,
            response=sentinel.response,
        )

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         ".VersionedDockerImage._get_image_versions")
    async def test__get_available_versions_runs__get_image_versions_in_executor(  # noqa:E501
            self,
            _get_image_versions):
        with contextlib.ExitStack() as stack:
            _get_credentials = stack.enter_context(unittest.mock.patch.object(
                self.dep, "_get_credentials",
            ))
            _get_credentials.side_effect = KeyError

            _get_image_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_image_versions",
                )
            )
            _get_image_versions.return_value = sentinel.tags

            loop = asyncio.get_event_loop()
            run_in_executor = stack.enter_context(unittest.mock.patch.object(
                loop, "run_in_executor",
                new=unittest.mock.AsyncMock(),
            ))
            run_in_executor.return_value = sentinel.execution_result

            ver = await self.dep._get_available_versions()

        run_in_executor.assert_called_once_with(
            None,
            _get_image_versions,
            unittest.mock.ANY,
        )
        self.assertEqual(ver, sentinel.execution_result)

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         ".VersionedDockerImage._get_image_versions")
    async def test__get_available_versions_passes_auth_helper_to_get_image_versions_if_credentials_are_available(  # noqa:E501
            self,
            _get_image_versions):
        with contextlib.ExitStack() as stack:
            _get_credentials = stack.enter_context(unittest.mock.patch.object(
                self.dep, "_get_credentials",
            ))
            _get_credentials.return_value = (
                sentinel.username, sentinel.password,
            )

            _get_image_versions = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "_get_image_versions",
                )
            )
            _get_image_versions.return_value = sentinel.tags

            await self.dep._get_available_versions()
            _get_image_versions.assert_called_with(unittest.mock.ANY)

        auth_helper = _get_image_versions.mock_calls[0][1][0]
        whatever_d_is = unittest.mock.Mock(["authenticate"])
        auth_helper(whatever_d_is, sentinel.response)
        whatever_d_is.authenticate.assert_called_once_with(
            sentinel.username,
            sentinel.password,
            response=sentinel.response,
        )

    async def test_get_versioned_url_composes_latest_version_with_url(self):
        versioneddependencies._get_override_versions.cache_clear()
        with contextlib.ExitStack() as stack:
            get_latest_version = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "get_latest_version",
                )
            )
            get_latest_version.return_value = "the-latest-version"

            url = await self.dep.get_versioned_url(sentinel.ctx)

        self.assertEqual(
            url,
            "test.domain.org/test/image/path:the-latest-version",
        )

    async def test_get_versioned_url_respects_override(self):
        versioneddependencies._get_override_versions.cache_clear()
        with contextlib.ExitStack() as stack:
            get_latest_version = stack.enter_context(
                unittest.mock.patch.object(
                    self.dep, "get_latest_version",
                )
            )
            get_latest_version.return_value = "the-latest-version"

            stack.enter_context(
                unittest.mock.patch.dict(os.environ, {
                    "YAOOK_OP_VERSIONS_OVERRIDE":
                    "test.domain.org/test/image/path: no.we.now/use/this:url"}
                )
            )

            url = await self.dep.get_versioned_url(sentinel.ctx)

        self.assertEqual(
            url,
            "no.we.now/use/this:url",
        )


@ddt.ddt
class TestConfigurableVersionedDockerImage(unittest.IsolatedAsyncioTestCase):
    def setUp(self):

        self.version_selector = unittest.mock.Mock(
            versioneddependencies.VersionSelector,
        )

        self.latest_version = "cutting_edge_version"
        self.image_selector = "img/selector"

    default_registry = "registry.gitlab.com/yaook/images"

    def _mock_registry_env_var(self, registry=None):
        os_env = {}
        if registry is not None:
            os_env["YAOOK_OP_DOCKER_REGISTRY_BASE_PATH"] = registry
        self._env_patcher = unittest.mock.patch.dict(os.environ, os_env)
        self._env_patcher.start()

    def tearDown(self):
        if self._env_patcher:
            self._env_patcher.stop()

    @ddt.unpack
    @ddt.data(
        (None, default_registry),
        ("my_registry", "my_registry"),
        ("", "")
    )
    async def test___init__(self, env_var_val, expected_registry):
        self._mock_registry_env_var(registry=env_var_val)

        sep = "/"
        if not expected_registry:
            sep = ""

        expected_url = f"{expected_registry}{sep}{self.image_selector}"

        dep = versioneddependencies.ConfigurableVersionedDockerImage(
            self.image_selector, version_selector=self.version_selector
        )

        self.assertEqual(expected_url, dep.url)

    @ddt.data("/", "/my_registry", "my_registry/", "////")
    async def test___init___raises(self, registry_base_path):
        self._mock_registry_env_var(registry=registry_base_path)
        with self.assertRaisesRegex(ValueError, registry_base_path):
            versioneddependencies.ConfigurableVersionedDockerImage(
                self.image_selector, version_selector=self.version_selector
            )

    @ddt.data(None, "some_registry")
    async def test_get_versioned_url(self, registry_env_var):
        self._mock_registry_env_var(registry=registry_env_var)

        url = "some url"
        dep = versioneddependencies.ConfigurableVersionedDockerImage(
            self.image_selector, version_selector=self.version_selector
        )
        dep.url = url

        with contextlib.ExitStack() as stack:
            latest_version_mock = stack.enter_context(
                unittest.mock.patch.object(dep, "get_latest_version")
            )
            latest_version_mock.return_value = self.latest_version
            expected_versioned_url = f"{url}:{self.latest_version}"

            result = await dep.get_versioned_url(unittest.mock.sentinel.ctx)

            self.assertEqual(expected_versioned_url, result)
            latest_version_mock.assert_called_once_with(
                unittest.mock.sentinel.ctx)


class TestVersionedDockerImageWithDockerRegistryOverride(unittest.IsolatedAsyncioTestCase):  # noqa:E501
    def setUp(self):
        self._env_patcher = unittest.mock.patch.dict(
            os.environ, {
                "YAOOK_OP_DOCKER_REGISTRY_OVERRIDE": "{"
                "'DEFAULT_REGISTRY' : 'test1.domain.org', "
                "'registry.gitlab.com' : 'test2.domain.org'}"
            })
        self._env_patcher.start()

        self.version_selector = unittest.mock.Mock(
            versioneddependencies.VersionSelector,
        )

    def tearDown(self):
        versioneddependencies._get_registry_overrides.cache_clear()
        self._env_patcher.stop()

    def test_docker_registry_override_default_registry_long_path(self):
        versioneddependencies._get_registry_overrides.cache_clear()

        dep = versioneddependencies.VersionedDockerImage(
            "somepath/someimage",
            version_selector=self.version_selector,
        )

        self.assertEqual(
            dep.url,
            "test1.domain.org/somepath/someimage",
        )

    def test_docker_registry_override_default_registry_short_path(self):
        versioneddependencies._get_registry_overrides.cache_clear()

        dep = versioneddependencies.VersionedDockerImage(
            "someimage",
            version_selector=self.version_selector,
        )

        self.assertEqual(
            dep.url,
            "test1.domain.org/someimage",
        )

    def test_docker_registry_override_not_to_be_overridden(self):
        versioneddependencies._get_registry_overrides.cache_clear()

        dep = versioneddependencies.VersionedDockerImage(
            "registry2.gitlab.com/someimage",
            version_selector=self.version_selector,
        )

        self.assertEqual(
            dep.url,
            "registry2.gitlab.com/someimage",
        )

    def test_docker_registry_override(self):
        versioneddependencies._get_registry_overrides.cache_clear()

        dep = versioneddependencies.VersionedDockerImage(
            "registry.gitlab.com/yaook/images/someimage",
            version_selector=self.version_selector,
        )

        self.assertEqual(
            dep.url,
            "test2.domain.org/yaook/images/someimage",
        )

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_DOCKER_REGISTRY_OVERRIDE": "abc:\ndef"})
    def test__get_registry_overrides_raises_wrong_variable(self):
        versioneddependencies._get_registry_overrides.cache_clear()
        with self.assertRaisesRegex(
                ValueError,
                "Provided image registry override environment variable is not "
                "valid yaml: .*"):
            versioneddependencies._get_registry_overrides()

    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_DOCKER_REGISTRY_OVERRIDE": "abcdef"})
    def test__get_registry_overrides_raises_wrong_variable_type(self):
        versioneddependencies._get_registry_overrides.cache_clear()
        with self.assertRaisesRegex(
                ValueError,
                "provided image registry override environment "
                "variable is not a valid dictionary"):
            versioneddependencies._get_registry_overrides()


@ddt.ddt()
class TestReleaseAwareVersionedDependency(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.ctx = unittest.mock.Mock(["parent_spec"])

        self.dep_a = unittest.mock.Mock(
            versioneddependencies.VersionedDependency)
        self.dep_b = unittest.mock.Mock(
            versioneddependencies.VersionedDependency)
        self.deps = {
            "rel-a": self.dep_a,
            "rel-b": self.dep_b,
        }

        self.ravd = versioneddependencies.ReleaseAwareVersionedDependency(
            self.deps,
            targetfn=lambda ctx: version_utils.get_target_release(ctx)
        )

    def _set_target_version(self, target):
        self.ctx.parent_spec = {"targetRelease": target}

    @ddt.data(
        "rel-a",
        "rel-b",
    )
    async def test_get_latest_version_passthrough(self, release):
        self._set_target_version(release)
        await self.ravd.get_latest_version(self.ctx)
        for k, v in self.deps.items():
            if k == release:
                v.get_latest_version.assert_called_once()
            else:
                v.get_latest_version.assert_not_called()

    @ddt.data(
        "rel-a",
        "rel-b",
    )
    async def test_get_versioned_url_passthrough(self, release):
        self._set_target_version(release)
        await self.ravd.get_versioned_url(self.ctx)
        for k, v in self.deps.items():
            if k == release:
                v.get_versioned_url.assert_called_once()
            else:
                v.get_versioned_url.assert_not_called()

    @ddt.data(
        "rel-a",
        "rel-b",
    )
    async def test__get_versioned_url_passthrough(self, release):
        self._set_target_version(release)
        await self.ravd._get_versioned_url(self.ctx)
        for k, v in self.deps.items():
            if k == release:
                v._get_versioned_url.assert_called_once()
            else:
                v._get_versioned_url.assert_not_called()


@ddt.ddt
class TestSuffixedVersionedDockerImage(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.url = "image"
        self.suffix = "1.0"
        self.suffixed_url = f"{self.url}:{self.suffix}"
        self.version_selector = unittest.mock.Mock(
            versioneddependencies.VersionSelector,
        )

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_url_image_split(self, _load_pin_file):
        _load_pin_file.return_value = {}
        dep = versioneddependencies.SuffixedVersionedDockerImage(
            self.suffixed_url,
            version_selector=self.version_selector
        )
        self.assertEqual(self.url, dep.url)
        self.assertEqual(self.suffixed_url, dep.suffixed_url)
        self.assertEqual(self.suffix, dep.suffix)

    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_version_pin_file(self, _load_pin_file):
        _load_pin_file.return_value = {
            self.suffixed_url: "0.0.15",
        }
        dep = versioneddependencies.SuffixedVersionedDockerImage(
            self.suffixed_url,
            version_selector=self.version_selector
        )
        ver = await dep.get_latest_version(sentinel.ctx)
        self.assertEqual(ver, "0.0.15")

        image = await dep.get_versioned_url(sentinel.ctx)
        self.assertEqual(image, f"{self.url}:0.0.15")

    async def test_invalid_image(self):
        with self.assertRaises(ValueError):
            _ = versioneddependencies.SuffixedVersionedDockerImage(
                "image/without/tag",
                version_selector=self.version_selector
            )


@ddt.ddt
class TestBitnamiVersionedDockerImage(unittest.IsolatedAsyncioTestCase):
    @unittest.mock.patch("yaook.statemachine.versioneddependencies"
                         "._load_pin_file")
    async def test_url_image_split(self, _load_pin_file):
        _load_pin_file.return_value = {}
        dep = versioneddependencies.BitnamiVersionedDockerImage(
            "image",
            release="1.0"
        )
        self.assertEqual("image:1.0", dep.suffixed_url)
        self.assertEqual("image", dep.url)


@ddt.ddt
class TestMappedVersionedDependency(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.ctx = unittest.mock.Mock(["parent_spec"])
        self.mapping = {
            "queens": "not-queens",
            "train": "not-train",
            "yoga": "not-yoga",
        }

    async def test_mapping(self):
        dep = versioneddependencies.MappedVersionedDependency(
            self.mapping,
            targetfn=lambda ctx: "yoga"
        )
        ver = await dep.get_latest_version(self.ctx)
        self.assertEqual(ver, "not-yoga")

        url = await dep.get_versioned_url(self.ctx)
        self.assertEqual(url, "not-yoga")
