#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import contextlib
import copy
import itertools
import os
import socket
import unittest
import unittest.mock
import uuid

from datetime import datetime

import kubernetes_asyncio.client
import kubernetes_asyncio.client as kclient

import yaook.statemachine.api_utils as api_utils


class Testextract_metadata(unittest.TestCase):
    def test_returns_dict_contents(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
            "labels": "fnord",
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        self.assertEqual(
            api_utils.extract_metadata(obj),
            metadata,
        )

    def test_injects_empty_labels_if_missing_from_the_source(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        self.assertDictEqual(
            api_utils.extract_metadata(obj),
            {
                "name": "foo",
                "namespace": "bar",
                "labels": {},
            },
        )

    def test_converts_None_labels_to_empty_dict(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
            "labels": None,
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        self.assertDictEqual(
            api_utils.extract_metadata(obj),
            {
                "name": "foo",
                "namespace": "bar",
                "labels": {},
            },
        )

    def test_keeps_existing_labels_intact(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
            "labels": {"foo": "bar"},
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        self.assertDictEqual(
            api_utils.extract_metadata(obj),
            {
                "name": "foo",
                "namespace": "bar",
                "labels": {"foo": "bar"},
            },
        )

    def test_returns_copy_of_dict_contents(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        self.assertIsNot(
            api_utils.extract_metadata(obj),
            metadata,
        )

    def test_removes_unsupported_keys_from_metadata_dict(self):
        metadata = {
            "name": "foo",
            "namespace": "bar",
            "unsupported": "key",
            "managedFields": "also-unsupported",
        }

        obj = {
            "foo": "bar",
            "metadata": metadata,
        }

        result = api_utils.extract_metadata(obj)
        self.assertNotEqual(result, metadata)
        self.assertEqual(
            {
                "name": "foo",
                "namespace": "bar",
                "labels": {},
            },
            result,
        )

    def test_returns_metadata_dict_from_k8s_object(self):
        obj = kubernetes_asyncio.client.V1Pod(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                namespace="foo",
                name="bar",
            )
        )

        result = api_utils.extract_metadata(obj)

        self.assertEqual(result["namespace"], "foo")
        self.assertEqual(result["name"], "bar")

    def test_supported_reverse_mapping(self):
        obj = kubernetes_asyncio.client.V1Pod(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                deletion_timestamp=unittest.mock.sentinel.deletion_timestamp,
                creation_timestamp=unittest.mock.sentinel.creation_timestamp,
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                labels=unittest.mock.sentinel.labels,
                annotations=unittest.mock.sentinel.annotations,
                resource_version=unittest.mock.sentinel.rversion,
            )
        )

        result = api_utils.extract_metadata(obj)

        self.assertEqual(result["deletionTimestamp"],
                         unittest.mock.sentinel.deletion_timestamp)
        self.assertEqual(result["creationTimestamp"],
                         unittest.mock.sentinel.creation_timestamp)
        self.assertEqual(result["labels"], unittest.mock.sentinel.labels)
        self.assertEqual(result["name"], unittest.mock.sentinel.name)
        self.assertEqual(result["namespace"], unittest.mock.sentinel.namespace)
        self.assertEqual(result["annotations"],
                         unittest.mock.sentinel.annotations)
        self.assertEqual(result["uid"], unittest.mock.sentinel.uid)
        self.assertEqual(result["resourceVersion"],
                         unittest.mock.sentinel.rversion)

    def test_no_unsupported_keys(self):
        obj = kubernetes_asyncio.client.V1Pod(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                managed_fields=unittest.mock.sentinel.managed_fields,
            )
        )

        result = api_utils.extract_metadata(obj)

        self.assertNotIn("managedFields", result)
        self.assertNotIn("managed_fields", result)


class Testgenerate_update_timestamp(unittest.TestCase):
    def test_uses_random_token_and_timestamp(self):
        with contextlib.ExitStack() as stack:
            # XXX: we cannot patch the datetime.datetime class here, because
            # it is a builtin. we also cannot patch the datetime.datetime
            # module member here because the name `datetime` has already been
            # imported to yaook.statemachine.api_utils at the time the patch
            # would happen, rendering it ineffective.
            datetime_mock = stack.enter_context(
                unittest.mock.patch("yaook.statemachine.api_utils.datetime"),
            )
            datetime_mock.utcnow.return_value = datetime(
                2020, 11, 4, 7, 33, 0, 123456,
            )
            token_urlsafe = stack.enter_context(
                unittest.mock.patch("secrets.token_urlsafe"),
            )
            token_urlsafe.return_value = "random"

            self.assertEqual(
                api_utils.generate_update_timestamp(),
                "2020-11-04T07:33:00.123456Z+random",
            )


class Testk8s_obj_to_yaml_data(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.volume_mount_0 = kclient.V1VolumeMount(
            mount_path="/test/path/0",
            name="testname0",
        )
        self.volume_mount_1 = kclient.V1VolumeMount(
            mount_path="/test/path/1",
            name="testname1",
        )
        self.container = kclient.V1Container(
            name="container",
            volume_mounts=[
                self.volume_mount_0,
                self.volume_mount_1,
            ],
        )

    def test_converts_key_name(self):
        out = api_utils.k8s_obj_to_yaml_data(self.volume_mount_0)
        self.assertIn("name", out)
        self.assertIn("mountPath", out)
        self.assertNotIn("mount_path", out)
        self.assertEqual("testname0", out["name"])
        self.assertEqual("/test/path/0", out["mountPath"])

    def test_converts_key_name_nested(self):
        out = api_utils.k8s_obj_to_yaml_data(self.container)
        self.assertIn("volumeMounts", out)
        self.assertIn("name", out)
        self.assertNotIn("volume_mounts", out)

        out_vm = out["volumeMounts"][0]
        self.assertIn("name", out_vm)
        self.assertIn("mountPath", out_vm)
        self.assertNotIn("mount_path", out_vm)


class Test_copy_poped(unittest.TestCase):
    def test_pops_key(self):
        d = {"foo": "bar", "a": "b"}
        result = api_utils._copy_poped(d, "foo")
        self.assertEqual(result, {"a": "b"})

    def test_does_not_modify_original(self):
        d = {"foo": "bar", "a": "b"}
        api_utils._copy_poped(d, "foo")
        self.assertEqual(d, {"foo": "bar", "a": "b"})


class Testdeep_has_changes(unittest.TestCase):
    def test_ignores_surplus_keys_on_lhs(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {
                    "k": "foo",
                },
                {

                }
            )
        )

    def test_detects_surplus_keys_on_rhs(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {

                },
                {
                    "k": "foo",
                }
            )
        )

    def test_ignores_surplus_empty_list_on_rhs(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {

                },
                {
                    "k": [],
                }
            )
        )

    def test_ignores_surplus_empty_dict_on_rhs(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {

                },
                {
                    "k": {},
                }
            )
        )

    def test_handles_surplus_int_on_rhs(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {
                },
                {
                    "k": 123,
                }
            )
        )

    def test_deep_compares_dict(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {
                    "k": {
                        "x": "y"
                    }
                },
                {
                    "k": {
                        "x": "y",
                        "n": []
                    }
                }
            )
        )

    def test_deep_compares_list(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {
                    "k": [
                        "x", "y",
                    ]
                },
                {
                    "k": [
                        "x", "z",
                    ]
                }
            )
        )

    def test_compares_list_length(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {
                    "k": [
                        "x", "y", "z",
                    ]
                },
                {
                    "k": [
                        "x", "y",
                    ]
                }
            )
        )

    def test_compares_list_length_new_longer(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {
                    "k": [
                        "x", "y",
                    ]
                },
                {
                    "k": [
                        "x", "y", "z",
                    ]
                }
            )
        )

    def test_detects_changed_values(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                {
                    "k": "foo",
                },
                {
                    "k": "bar",
                }
            )
        )

    def test_falls_back_to_eq(self):
        self.assertTrue(
            api_utils.deep_has_changes(
                unittest.mock.sentinel.v1,
                unittest.mock.sentinel.v2,
            )
        )
        self.assertFalse(
            api_utils.deep_has_changes(
                unittest.mock.sentinel.v1,
                unittest.mock.sentinel.v1,
            )
        )

    def test_ignores_order_for_volumeMounts(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {
                    "volumeMounts": [
                        {
                            "name": "mount1",
                            "mountPath": "/mount1",
                        },
                        {
                            "name": "mount2",
                            "mountPath": "/mount2",
                        },
                    ]
                },
                {
                    "volumeMounts": [
                        {
                            "name": "mount2",
                            "mountPath": "/mount2",
                        },
                        {
                            "name": "mount1",
                            "mountPath": "/mount1",
                        },
                    ]
                }
            )
        )

    def test_ignores_order_for_env(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {
                    "env": [
                        {
                            "name": "var1",
                            "value": "value1",
                        },
                        {
                            "name": "var2",
                            "value": "value2",
                        },
                    ]
                },
                {
                    "env": [
                        {
                            "name": "var2",
                            "value": "value2",
                        },
                        {
                            "name": "var1",
                            "value": "value1",
                        },
                    ]
                }
            )
        )

    def test_ignores_order_for_volumes(self):
        self.assertFalse(
            api_utils.deep_has_changes(
                {
                    "volumes": [
                        {
                            "name": "var1",
                            "emptyDir": {},
                        },
                        {
                            "name": "var2",
                            "emptyDir": {},
                        },
                    ]
                },
                {
                    "volumes": [
                        {
                            "name": "var2",
                            "emptyDir": {},
                        },
                        {
                            "name": "var1",
                            "emptyDir": {},
                        },
                    ]
                }
            )
        )


class Testmatches_labels(unittest.TestCase):
    def test_empty_selector_matches_everything(self):
        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {},
            )
        )

        self.assertTrue(
            api_utils.matches_labels(
                {},
                {},
            )
        )

        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                    "baz": "fnord",
                },
                {},
            )
        )

    def test_ignores_labels_not_in_selector(self):
        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {
                    "foo": "bar",
                },
            )
        )

        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                    "baz": "fnord",
                },
                {
                    "foo": "bar",
                },
            )
        )

    def test_requires_labels_from_selector(self):
        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {
                    "foo": "bar",
                },
            )
        )

        self.assertFalse(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {
                    "baz": "fnord",
                },
            )
        )

    def test_requires_that_values_match(self):
        self.assertTrue(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {
                    "foo": "bar",
                },
            )
        )

        self.assertFalse(
            api_utils.matches_labels(
                {
                    "foo": "bar",
                },
                {
                    "foo": "fnord",
                },
            )
        )


class TestLabelExpression(unittest.TestCase):
    def test_from_dict_extracts_parts(self):
        result = api_utils.LabelExpression.from_dict({
            "key": "foo",
            "operator": "In",
            "values": ["abc", "def"],
        })

        self.assertEqual(result.key, "foo")
        self.assertEqual(result.operator, api_utils.SelectorOperator.IN)
        self.assertSequenceEqual(result.values, ["abc", "def"])

    def test_from_dict_copies_values(self):
        values = ["abc", "def"]

        result = api_utils.LabelExpression.from_dict({
            "key": "foo",
            "operator": "In",
            "values": values,
        })

        self.assertIsNot(result.values, values)
        values.append("baz")
        self.assertNotEqual(result.values, values)

    def test_from_dict_raises_ValueError_if_values_are_missing(self):
        with self.assertRaisesRegex(
                ValueError,
                "missing values in label expression requiring values"):
            api_utils.LabelExpression.from_dict({
                "key": "foo",
                "operator": "In",
            })

    def test_from_dict_allows_omitting_values_if_operator_is_exists(self):
        result = api_utils.LabelExpression.from_dict({
            "key": "foo",
            "operator": "Exists",
        })

        self.assertIsNone(result.values)

    def test_from_dict_ignores_values_for_Exists(self):
        result = api_utils.LabelExpression.from_dict({
            "key": "foo",
            "operator": "Exists",
            "values": ["foo", "bar"],
        })

        self.assertIsNone(result.values)

    def test_from_dict_ignores_values_for_DoesNotExists(self):
        result = api_utils.LabelExpression.from_dict({
            "key": "foo",
            "operator": "DoesNotExist",
            "values": ["foo", "bar"],
        })

        self.assertIsNone(result.values)

    def test_from_api_object_extracts_parts(self):
        result = api_utils.LabelExpression.from_api_object(
            kclient.V1LabelSelectorRequirement(
                key="foo",
                operator="In",
                values=["abc", "def"],
            ),
        )

        self.assertEqual(result.key, "foo")
        self.assertEqual(result.operator, api_utils.SelectorOperator.IN)
        self.assertSequenceEqual(result.values, ["abc", "def"])

    def test_from_api_object_copies_values(self):
        values = ["abc", "def"]

        result = api_utils.LabelExpression.from_api_object(
            kclient.V1LabelSelectorRequirement(
                key="foo",
                operator="In",
                values=values,
            ),
        )

        self.assertIsNot(result.values, values)
        values.append("baz")
        self.assertNotEqual(result.values, values)

    def test_from_api_object_raises_ValueError_if_values_are_missing(self):
        obj_ = kclient.V1LabelSelectorRequirement(
            key="foo",
            operator="In",
            values=None,
        )

        with self.assertRaisesRegex(
                ValueError,
                "missing values in label expression requiring values"):
            api_utils.LabelExpression.from_api_object(obj_)

    def test_from_api_object_allows_omitting_values_if_operator_is_exists(self):  # NOQA
        obj_ = kclient.V1LabelSelectorRequirement(
            key="foo",
            operator="Exists",
        )

        result = api_utils.LabelExpression.from_api_object(obj_)

        self.assertIsNone(result.values)

    def test_from_api_object_ignores_values_for_Exists(self):
        obj_ = kclient.V1LabelSelectorRequirement(
            key="foo",
            operator="Exists",
            values=["foo", "bar"],
        )

        result = api_utils.LabelExpression.from_api_object(obj_)

        self.assertIsNone(result.values)

    def test_from_api_object_ignores_values_for_DoesNotExists(self):
        obj_ = kclient.V1LabelSelectorRequirement(
            key="foo",
            operator="DoesNotExist",
            values=["foo", "bar"],
        )

        result = api_utils.LabelExpression.from_api_object(obj_)

        self.assertIsNone(result.values)

    def test_as_api_selector_for_exists(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.EXISTS,
            values=None,
        )

        self.assertEqual(le.as_api_selector(), "foo")

    def test_as_api_selector_for_does_not_exist(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_EXISTS,
            values=None,
        )

        self.assertEqual(le.as_api_selector(), "!foo")

    def test_as_api_selector_for_in(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=["abc", "def"],
        )

        self.assertEqual(le.as_api_selector(), "foo in (abc, def)")

    def test_as_api_selector_for_in_with_single_value(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=["abc"],
        )

        self.assertEqual(le.as_api_selector(), "foo=abc")

    def test_as_api_selector_for_notin(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_IN,
            values=["abc", "def"],
        )

        self.assertEqual(le.as_api_selector(), "foo notin (abc, def)")

    def test_values_are_immutable_tuple(self):
        values = ["bar", "baz"]

        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=values,
        )

        self.assertIsInstance(le.values, tuple)
        self.assertSequenceEqual(le.values, values)

    def test_frozen(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=["abc", "def"],
        )

        with self.assertRaises(AttributeError):
            le.key = "fnord"

        with self.assertRaises(AttributeError):
            le.operator = api_utils.SelectorOperator.NOT_IN

        with self.assertRaises(AttributeError):
            le.values = ("def", "ghi")

    def test_object_matches_with_exists_returns_true_if_label_is_present(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.EXISTS,
            values=None,
        )

        self.assertTrue(le.object_matches({"foo": "bar"}))
        self.assertTrue(le.object_matches({"foo": "baz"}))
        self.assertTrue(le.object_matches({"foo": "baz", "other": "label"}))

    def test_object_matches_with_exists_returns_false_if_label_is_absent(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.EXISTS,
            values=None,
        )

        self.assertFalse(le.object_matches({"bar": "baz"}))
        self.assertFalse(le.object_matches({}))

    def test_object_matches_with_not_exists_returns_false_if_label_is_present(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_EXISTS,
            values=None,
        )

        self.assertFalse(le.object_matches({"foo": "bar"}))
        self.assertFalse(le.object_matches({"foo": "baz"}))
        self.assertFalse(le.object_matches({"foo": "baz", "other": "label"}))

    def test_object_matches_with_not_exists_returns_true_if_label_is_absent(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_EXISTS,
            values=None,
        )

        self.assertTrue(le.object_matches({"bar": "baz"}))
        self.assertTrue(le.object_matches({}))

    def test_object_matches_with_in_returns_false_if_label_is_absent(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=("v1", "v2"),
        )

        self.assertFalse(le.object_matches({"bar": "v1"}))
        self.assertFalse(le.object_matches({}))

    def test_object_matches_with_in_returns_false_if_label_is_present_with_wrong_value(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=("v1", "v2"),
        )

        self.assertFalse(le.object_matches({"foo": "v3"}))
        self.assertFalse(le.object_matches({"foo": "v"}))

    def test_object_matches_with_in_returns_true_if_label_is_present_with_any_correct_value(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.IN,
            values=("v1", "v2"),
        )

        self.assertTrue(le.object_matches({"foo": "v1"}))
        self.assertTrue(le.object_matches({"foo": "v2"}))
        self.assertTrue(le.object_matches({"foo": "v2", "other": "label"}))

    def test_object_matches_with_not_in_returns_true_if_label_is_absent(self):
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_IN,
            values=("v1", "v2"),
        )

        self.assertTrue(le.object_matches({"bar": "v1"}))
        self.assertTrue(le.object_matches({}))

    def test_object_matches_with_not_in_returns_true_if_label_is_present_with_wrong_value(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_IN,
            values=("v1", "v2"),
        )

        self.assertTrue(le.object_matches({"foo": "v3"}))
        self.assertTrue(le.object_matches({"foo": "v"}))

    def test_object_matches_with_not_in_returns_false_if_label_is_present_with_any_correct_value(self):  # NOQA
        le = api_utils.LabelExpression(
            key="foo",
            operator=api_utils.SelectorOperator.NOT_IN,
            values=("v1", "v2"),
        )

        self.assertFalse(le.object_matches({"foo": "v1"}))
        self.assertFalse(le.object_matches({"foo": "v2"}))
        self.assertFalse(le.object_matches({"foo": "v2", "other": "label"}))


class TestLabelSelector(unittest.TestCase):
    def test_from_dict_converts_match_labels_into_LabelExpressions(self):
        match = {"foo": "bar", "baz": "fnord"}

        result = api_utils.LabelSelector.from_dict({
            "matchLabels": match,
        })

        self.assertIsInstance(result, api_utils.LabelSelector)
        self.assertCountEqual(
            result.match_expressions,
            [
                api_utils.LabelExpression(
                    key="foo",
                    operator=api_utils.SelectorOperator.IN,
                    values=["bar"],
                ),
                api_utils.LabelExpression(
                    key="baz",
                    operator=api_utils.SelectorOperator.IN,
                    values=["fnord"],
                ),
            ]
        )

    def test_from_dict_defaults_match_labels(self):
        result = api_utils.LabelSelector.from_dict({})

        self.assertIsInstance(result, api_utils.LabelSelector)
        self.assertCountEqual(
            result.match_expressions,
            [],
        )

    def test_from_dict_raises_ValueError_on_unknown_keys(self):
        with self.assertRaisesRegex(ValueError, r"unsupported match"):
            api_utils.LabelSelector.from_dict(
                {"foo": "bar"},
            )

    def test_from_dict_does_not_modify_input(self):
        selector = {
            "matchLabels": {},
        }
        selector_bak = copy.deepcopy(selector)

        api_utils.LabelSelector.from_dict(selector)

        self.assertEqual(selector, selector_bak)

    def test_from_dict_uses_LabelExpression_helper(self):
        with contextlib.ExitStack() as stack:
            from_dict = stack.enter_context(unittest.mock.patch.object(
                api_utils.LabelExpression,
                "from_dict",
            ))

            ls = api_utils.LabelSelector.from_dict(
                {
                    "matchExpressions": [
                        unittest.mock.sentinel.expr1,
                        unittest.mock.sentinel.expr2,
                    ],
                },
            )

        self.assertCountEqual(
            from_dict.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.expr1),
                unittest.mock.call(unittest.mock.sentinel.expr2),
            ]
        )

        self.assertCountEqual(
            ls.match_expressions,
            [
                from_dict(),
                from_dict(),
            ],
        )

    def test_from_dict_converts_match_expressions_into_LabelExpressions(self):
        ls = api_utils.LabelSelector.from_dict(
            {
                "matchExpressions": [
                    {
                        "key": "foo",
                        "operator": "Exists",
                    },
                    {
                        "key": "bar",
                        "operator": "NotIn",
                        "values": ["prod", "qa"],
                    },
                ],
            },
        )

        self.assertCountEqual(
            ls.match_expressions,
            [
                api_utils.LabelExpression(
                    key="foo",
                    operator=api_utils.SelectorOperator.EXISTS,
                    values=None,
                ),
                api_utils.LabelExpression(
                    key="bar",
                    operator=api_utils.SelectorOperator.NOT_IN,
                    values=("prod", "qa"),
                ),
            ],
        )

    def test_from_api_object_converts_matchLabels_into_LabelExpressions(self):
        match = {"foo": "bar", "baz": "fnord"}

        result = api_utils.LabelSelector.from_api_object(
            kclient.V1LabelSelector(
                match_labels=match,
            ),
        )

        self.assertIsInstance(result, api_utils.LabelSelector)
        self.assertCountEqual(
            result.match_expressions,
            [
                api_utils.LabelExpression(
                    key="foo",
                    operator=api_utils.SelectorOperator.IN,
                    values=["bar"],
                ),
                api_utils.LabelExpression(
                    key="baz",
                    operator=api_utils.SelectorOperator.IN,
                    values=["fnord"],
                ),
            ]
        )

    def test_from_api_object_uses_LabelExpression_helper(self):
        with contextlib.ExitStack() as stack:
            from_api_object = stack.enter_context(unittest.mock.patch.object(
                api_utils.LabelExpression,
                "from_api_object",
            ))

            ls = api_utils.LabelSelector.from_api_object(
                kclient.V1LabelSelector(
                    match_expressions=[
                        unittest.mock.sentinel.expr1,
                        unittest.mock.sentinel.expr2,
                    ],
                ),
            )

        self.assertCountEqual(
            from_api_object.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.expr1),
                unittest.mock.call(unittest.mock.sentinel.expr2),
            ]
        )

        self.assertCountEqual(
            ls.match_expressions,
            [
                from_api_object(),
                from_api_object(),
            ],
        )

    def test_from_api_object_converts_match_expressions_into_LabelExpressions(self):  # NOQA
        ls = api_utils.LabelSelector.from_api_object(
            kclient.V1LabelSelector(
                match_expressions=[
                    kclient.V1LabelSelectorRequirement(
                        key="foo",
                        operator="Exists",
                    ),
                    kclient.V1LabelSelectorRequirement(
                        key="bar",
                        operator="NotIn",
                        values=["prod", "qa"],
                    ),
                ],
            ),
        )

        self.assertCountEqual(
            ls.match_expressions,
            [
                api_utils.LabelExpression(
                    key="foo",
                    operator=api_utils.SelectorOperator.EXISTS,
                    values=None,
                ),
                api_utils.LabelExpression(
                    key="bar",
                    operator=api_utils.SelectorOperator.NOT_IN,
                    values=("prod", "qa"),
                ),
            ],
        )

    def test_match_expressions_are_immutable_tuple(self):
        match = {"foo": "bar"}

        result = api_utils.LabelSelector(match_labels=match)

        self.assertIsInstance(result.match_expressions, tuple)

    def test_object_matches_without_any_criteria_returns_true(self):
        ls = api_utils.LabelSelector(match_labels={})

        self.assertTrue(ls.object_matches(unittest.mock.sentinel.labels))

    def test_object_matches_behaviour_is_the_same_as_matches_labels(self):
        match_labels = {
            "foo": "bar",
            "baz": "fnord",
        }

        test_cases = [
            {},
            {"foo": "bar"},
            {"baz": "fnord"},
            {"foo": "bar", "baz": "fnord"},
            {"foo": "bar", "baz": "fnord", "other": "label"},
            {"other": "label"},
        ]

        ls = api_utils.LabelSelector(match_labels=match_labels)

        for case in test_cases:
            self.assertEqual(
                api_utils.matches_labels(case, match_labels),
                ls.object_matches(case),
                repr(case),
            )

    @unittest.mock.patch("yaook.statemachine.api_utils.LabelExpression")
    def test_object_matches_uses_LabelExpression(self, LabelExpression):
        object_matches_mock = LabelExpression().object_matches
        ls = api_utils.LabelSelector(match_labels={"foo": "bar"})

        for r in [True, False]:
            object_matches_mock.reset_mock()
            object_matches_mock.return_value = r
            result = ls.object_matches(unittest.mock.sentinel.labels)
            object_matches_mock.assert_called_once_with(
                unittest.mock.sentinel.labels,
            )
            self.assertEqual(result, r)

    def test_as_api_selector_gives_reasonable_result(self):
        match = {
            "foo": "bar",
            "baz": "fnord",
        }
        ls = api_utils.LabelSelector(match_labels=match)

        result = ls.as_api_selector()

        self.assertEqual(
            result,
            "foo=bar,baz=fnord",
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.LabelExpression")
    def test_as_api_selector_uses_LabelExpression(self, LabelExpression):
        def expr_generator():
            for i in itertools.count():
                mock = unittest.mock.Mock()
                mock.as_api_selector = unittest.mock.Mock()
                mock.as_api_selector.return_value = f"expr{i}"
                yield mock

        LabelExpression.side_effect = expr_generator()
        ls = api_utils.LabelSelector(
            match_labels={"foo": "bar", "baz": "fnord"},
        )

        self.assertEqual(
            ls.as_api_selector(),
            "expr0,expr1",
        )


class Testmulti_selector_list(unittest.IsolatedAsyncioTestCase):
    def _make_selector(self, api_str):
        selector = unittest.mock.Mock(["as_api_selector"])
        selector.as_api_selector.return_value = api_str
        return selector

    async def _async_list(self, gen):
        result = []
        async for item in gen:
            result.append(item)
        return result

    async def test_multi_selector_list_calls_function_for_each_selector_and_returns_joined(self):  # NOQA
        selectors = [
            self._make_selector(unittest.mock.sentinel.selector1),
            self._make_selector(unittest.mock.sentinel.selector2),
            self._make_selector(unittest.mock.sentinel.selector3),
        ]

        def list_generator():
            for i in itertools.count():
                yield [
                    getattr(unittest.mock.sentinel, "item{}_{}".format(i, j))
                    for j in range(2)
                ]

        listfn = unittest.mock.AsyncMock()
        listfn.side_effect = list_generator()

        result = await self._async_list(api_utils.multi_selector_list(
            listfn,
            selectors,
        ))

        self.assertCountEqual(
            listfn.mock_calls,
            [
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector1),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector2),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector3),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.item0_0,
                unittest.mock.sentinel.item0_1,
                unittest.mock.sentinel.item1_0,
                unittest.mock.sentinel.item1_1,
                unittest.mock.sentinel.item2_0,
                unittest.mock.sentinel.item2_1,
            ]
        )


class Testinject_scheduling_keys(unittest.TestCase):
    def test_does_nothing_new_if_no_scheduling_keys_are_given(self):
        pod_spec = {}
        pod_spec_bak = copy.deepcopy(pod_spec)

        api_utils.inject_scheduling_keys(pod_spec, [])

        self.assertEqual(pod_spec, pod_spec_bak)

    def test_adds_scheduling_key_tolerations(self):
        pod_spec = {}

        api_utils.inject_scheduling_keys(
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
            ],
        )

    def test_keeps_existing_tolerations_in_place(self):
        pod_spec = {
            "tolerations": [
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
            ],
        }

        api_utils.inject_scheduling_keys(
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
                {
                    "key": "other",
                    "operator": "Exists",
                    "effect": "NoSchedule",
                },
            ],
        )

    def test_adds_node_affinity_for_scheduling_keys(self):
        pod_spec = {}

        api_utils.inject_scheduling_keys(
            pod_spec,
            {"foo", "bar"},
        )

        self.assertCountEqual(
            pod_spec["affinity"]["nodeAffinity"]
            ["requiredDuringSchedulingIgnoredDuringExecution"]
            ["nodeSelectorTerms"],
            [
                {
                    "matchExpressions": [
                        {
                            "key": "foo",
                            "operator": "Exists",
                            "values": [],
                        },
                    ],
                },
                {
                    "matchExpressions": [
                        {
                            "key": "bar",
                            "operator": "Exists",
                            "values": [],
                        },
                    ],
                },
            ],
        )

    def test_rejects_existing_node_affinity_requirements(self):
        pod_spec = {
            "affinity": {
                "nodeAffinity": {
                    "requiredDuringSchedulingIgnoredDuringExecution": {
                        "nodeSelectorTerms": [
                            unittest.mock.sentinel.anything,
                        ],
                    },
                },
            },
        }
        pod_spec_bak = copy.deepcopy(pod_spec)

        with self.assertRaisesRegex(
                ValueError,
                "conflict: scheduling keys given, but the Pod already has "
                "scheduling requirements set"):
            api_utils.inject_scheduling_keys(
                pod_spec,
                {"foo", "bar"},
            )

        self.assertEqual(pod_spec, pod_spec_bak)

    def test_rejects_existing_node_selector(self):
        pod_spec = {
            "nodeSelector": unittest.mock.sentinel.anything,
        }
        pod_spec_bak = copy.deepcopy(pod_spec)

        with self.assertRaisesRegex(
                ValueError,
                "conflict: scheduling keys given, but the Pod already has "
                "scheduling requirements set"):
            api_utils.inject_scheduling_keys(
                pod_spec,
                {"foo", "bar"},
            )

        self.assertEqual(pod_spec, pod_spec_bak)


class Testextract_pod_references_from_api_object(unittest.TestCase):
    def setUp(self):
        self.namespace = str(uuid.uuid4())

    def test_returns_empty_for_minimal_pod(self):
        pod = kclient.V1PodSpec(containers=[])
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [],
        )

    def test_returns_reference_to_config_map_from_volume(self):
        pod = kclient.V1PodSpec(
            containers=[],
            volumes=[
                kclient.V1Volume(
                    name="volume",
                    config_map=kclient.V1ConfigMapVolumeSource(
                        name="foo",
                    ),
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="foo",
                ),
            ],
        )

    def test_returns_reference_to_secret_from_volume(self):
        pod = kclient.V1PodSpec(
            containers=[],
            volumes=[
                kclient.V1Volume(
                    name="volume",
                    secret=kclient.V1SecretVolumeSource(
                        secret_name="bar",
                    ),
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="bar",
                ),
            ],
        )

    def test_returns_reference_to_pvc_from_volume(self):
        pvc = kclient.V1PersistentVolumeClaimVolumeSource(
            claim_name="baz",
        )

        pod = kclient.V1PodSpec(
            containers=[],
            volumes=[
                kclient.V1Volume(
                    name="volume",
                    persistent_volume_claim=pvc
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="persistentvolumeclaims",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    def test_returns_reference_to_resources_from_projected_volume(self):
        sources = [
            kclient.V1VolumeProjection(
                config_map=kclient.V1ConfigMapProjection(
                    name="p1",
                ),
            ),
            kclient.V1VolumeProjection(
                secret=kclient.V1SecretProjection(
                    name="p2",
                ),
            ),
        ]

        pod = kclient.V1PodSpec(
            containers=[],
            volumes=[
                kclient.V1Volume(
                    name="volume",
                    projected=kclient.V1ProjectedVolumeSource(
                        sources=sources,
                    ),
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="p1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="p2",
                ),
            ],
        )

    def test_returns_reference_to_resources_from_environment_in_containers(self):  # NOQA
        env = [
            kclient.V1EnvVar(
                name="VAR1",
                value_from=kclient.V1EnvVarSource(
                    config_map_key_ref=kclient.V1ConfigMapKeySelector(
                        name="c1",
                        key="foo",
                    ),
                ),
            ),
            kclient.V1EnvVar(
                name="VAR2",
                value_from=kclient.V1EnvVarSource(
                    secret_key_ref=kclient.V1SecretKeySelector(
                        name="s1",
                        key="bar",
                    ),
                ),
            ),
            kclient.V1EnvVar(
                name="VAR3",
                value="xyz",
            ),
        ]
        env_from = [
            kclient.V1EnvFromSource(
                config_map_ref=kclient.V1ConfigMapEnvSource(
                    name="c2",
                ),
            ),
            kclient.V1EnvFromSource(
                secret_ref=kclient.V1SecretEnvSource(
                    name="s2",
                ),
            ),
        ]

        pod = kclient.V1PodSpec(
            containers=[
                kclient.V1Container(
                    name="foo",
                    env=env,
                    env_from=env_from,
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c2",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s2",
                ),
            ],
        )

    def test_handles_container_without_resources(self):
        pod = kclient.V1PodSpec(
            containers=[
                kclient.V1Container(
                    name="foo",
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [],
        )

    def test_returns_reference_to_resources_from_environment_in_init_containers(self):  # NOQA
        env = [
            kclient.V1EnvVar(
                name="VAR1",
                value_from=kclient.V1EnvVarSource(
                    config_map_key_ref=kclient.V1ConfigMapKeySelector(
                        name="c1",
                        key="foo",
                    ),
                ),
            ),
            kclient.V1EnvVar(
                name="VAR2",
                value_from=kclient.V1EnvVarSource(
                    secret_key_ref=kclient.V1SecretKeySelector(
                        name="s1",
                        key="bar",
                    ),
                ),
            ),
            kclient.V1EnvVar(
                name="VAR3",
                value="xyz",
            ),
        ]
        env_from = [
            kclient.V1EnvFromSource(
                config_map_ref=kclient.V1ConfigMapEnvSource(
                    name="c2",
                ),
            ),
            kclient.V1EnvFromSource(
                secret_ref=kclient.V1SecretEnvSource(
                    name="s2",
                ),
            ),
        ]

        pod = kclient.V1PodSpec(
            containers=[],
            init_containers=[
                kclient.V1Container(
                    name="foo",
                    env=env,
                    env_from=env_from,
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c2",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s2",
                ),
            ],
        )

    def test_returns_empty_list_when_projected_sources_is_none(self):
        pod = kclient.V1PodSpec(
            containers=[],
            volumes=[
                kclient.V1Volume(
                    name="volume",
                    projected=kclient.V1ProjectedVolumeSource(
                        sources=None
                    ),
                ),
            ],
        )
        self.assertCountEqual(
            api_utils.extract_pod_references_from_api_object(
                pod,
                self.namespace,
            ),
            [],
        )


class Testextract_pod_references(unittest.IsolatedAsyncioTestCase):
    async def test_deserializes_and_delegates(self):
        obj = {"foo": unittest.mock.sentinel.obj}

        with contextlib.ExitStack() as stack:
            deserialize = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.ApiClient.deserialize",
            ))
            deserialize.return_value = unittest.mock.sentinel.pod_spec

            from_api_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils."
                "extract_pod_references_from_api_object",
            ))
            from_api_object.return_value = unittest.mock.sentinel.result

            dumps = stack.enter_context(unittest.mock.patch(
                "json.dumps",
            ))

            result = api_utils.extract_pod_references(
                obj,
                unittest.mock.sentinel.namespace,
            )

        dumps.assert_called_once_with(obj)

        deserialize.assert_called_once_with(
            unittest.mock.ANY,
            kubernetes_asyncio.client.V1PodSpec,
        )

        _, (data, _), _ = deserialize.mock_calls[0]
        self.assertEqual(data.data, dumps())

        from_api_object.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            unittest.mock.sentinel.namespace,
        )

        self.assertEqual(result, unittest.mock.sentinel.result)

    def test_accepts_pod_object(self):
        self.assertCountEqual(
            api_utils.extract_pod_references(
                kclient.V1PodSpec(containers=[]),
                "foo",
            ),
            [],
        )


class Testextract_cds_references(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.namespace = str(uuid.uuid4())

    async def test_empty_spec(self):
        self.assertCountEqual(
            api_utils.extract_cds_references(
                {
                    "template": {
                        "spec": {
                            "containers": [],
                        },
                    },
                },
                self.namespace,
            ),
            [],
        )

    def test_parses_pod_template_using_helper(self):
        with contextlib.ExitStack() as stack:
            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.return_value = [
                unittest.mock.sentinel.ref1,
                unittest.mock.sentinel.ref2,
            ]

            result = api_utils.extract_cds_references(
                {
                    "template": {
                        "spec": unittest.mock.sentinel.pod_spec,
                    },
                },
                self.namespace,
            )

        extract_pod_references.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            self.namespace,
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.ref1,
                unittest.mock.sentinel.ref2,
            ],
        )

    async def test_extracts_all_config_maps_from_volume_template(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "configMap": {
                                    "name": "foo",
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "configMap": {
                                    "name": "bar",
                                },
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "configMap": {
                                "name": "baz",
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="foo",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="bar",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    async def test_extracts_all_secrets_from_volume_template(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "secret": {
                                    "secretName": "foo",
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "secret": {
                                    "secretName": "bar",
                                },
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "secret": {
                                "secretName": "baz",
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="foo",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="bar",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    async def test_extracts_all_pvcs_from_volume_template(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "persistentVolumeClaim": {
                                    "claimName": "foo",
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "persistentVolumeClaim": {
                                    "claimName": "bar",
                                },
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "persistentVolumeClaim": {
                                "claimName": "baz",
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="persistentvolumeclaims",
                    namespace=self.namespace,
                    name="foo",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="persistentvolumeclaims",
                    namespace=self.namespace,
                    name="bar",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="persistentvolumeclaims",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    async def test_extracts_mixed_types(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "configMap": {
                                    "name": "foo",
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "secret": {
                                    "secretName": "bar",
                                },
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "persistentVolumeClaim": {
                                "claimName": "baz",
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="foo",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="bar",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="persistentvolumeclaims",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    async def test_ignores_unknown_types(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "flexVolume": {},
                            },
                        },
                        "n2": {
                            "template": {
                                "flexVolume": {},
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "flexVolume": {},
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [],
        )

    async def test_works_without_default(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "configMap": {
                                    "name": "foo",
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "configMap": {
                                    "name": "bar",
                                },
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="foo",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="bar",
                ),
            ],
        )

    async def test_works_with_only_default(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "default": {
                        "template": {
                            "configMap": {
                                "name": "baz",
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="baz",
                ),
            ],
        )

    async def test_extracts_all_resources_from_projected_volumes(self):
        self.maxDiff = None

        cds = {
            "template": {
                "spec": {
                    "containers": [],
                },
            },
            "volumeTemplates": [
                {
                    "nodeMap": {
                        "n1": {
                            "template": {
                                "projected": {
                                    "sources": [
                                        {
                                            "configMap": {
                                                "name": "c1",
                                            },
                                        },
                                        {
                                            "secret": {
                                                "name": "s1",
                                            },
                                        },
                                    ],
                                },
                            },
                        },
                        "n2": {
                            "template": {
                                "projected": {
                                    "sources": [
                                        {
                                            "configMap": {
                                                "name": "c2",
                                            },
                                        },
                                        {
                                            "secret": {
                                                "name": "s2",
                                            },
                                        },
                                    ],
                                },
                            },
                        },
                    },
                    "default": {
                        "template": {
                            "projected": {
                                "sources": [
                                    {
                                        "configMap": {
                                            "name": "c3",
                                        },
                                    },
                                    {
                                        "secret": {
                                            "name": "s3",
                                        },
                                    },
                                ],
                            },
                        },
                    },
                },
            ],
        }

        self.assertCountEqual(
            api_utils.extract_cds_references(
                cds,
                self.namespace,
            ),
            [
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c2",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="configmaps",
                    namespace=self.namespace,
                    name="c3",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s1",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s2",
                ),
                api_utils.ResourceReference(
                    api_version="v1",
                    plural="secrets",
                    namespace=self.namespace,
                    name="s3",
                ),
            ],
        )


class Testsplit_api_version(unittest.TestCase):
    def test_split_core_api_version(self):
        self.assertEqual(
            api_utils.split_api_version("v1"),
            ("", "v1"),
        )

        self.assertEqual(
            api_utils.split_api_version("v1beta1"),
            ("", "v1beta1"),
        )

    def test_split_extension_api_version(self):
        self.assertEqual(
            api_utils.split_api_version("apps/v1"),
            ("apps", "v1"),
        )

        self.assertEqual(
            api_utils.split_api_version("rbac.magic/v1beta1"),
            ("rbac.magic", "v1beta1"),
        )


class Testjoin_api_version(unittest.TestCase):
    def test_join_core_api_version(self):
        self.assertEqual(
            api_utils.join_api_version("", "v1"),
            "v1",
        )

        self.assertEqual(
            api_utils.join_api_version("", "v1beta1"),
            "v1beta1",
        )

    def test_join_extension_api_version(self):
        self.assertEqual(
            api_utils.join_api_version("apps", "v1"),
            "apps/v1",
        )

        self.assertEqual(
            api_utils.join_api_version("rbac.magic", "v1beta1"),
            "rbac.magic/v1beta1",
        )


class Testencode_secret_data(unittest.TestCase):
    def test_wraps_in_base64(self):
        self.assertEqual(
            api_utils.encode_secret_data({
                "k1": "key1",
                "k2": "äöü",
                # this is a nasty surrogate pair which couldn’t pass through
                # utf-8 encoding by default, just to prove a point.
                "k3": b"\xed\xb0\x80",
            }),
            {
                "k1": base64.b64encode(b"key1").decode("ascii"),
                "k2": base64.b64encode("äöü".encode("utf-8")).decode("ascii"),
                "k3": base64.b64encode(b"\xed\xb0\x80").decode("ascii"),
            }
        )

    def test_custom_encoding(self):
        self.assertEqual(
            api_utils.encode_secret_data({
                "k1": "key1",
                "k2": "äöü",
                # this is a nasty surrogate pair which couldn’t pass through
                # utf-8 encoding by default, just to prove a point.
                "k3": b"\xed\xb0\x80",
            }, encoding="latin1"),
            {
                "k1": base64.b64encode(b"key1").decode("ascii"),
                "k2": base64.b64encode("äöü".encode("latin1")).decode("ascii"),
                "k3": base64.b64encode(b"\xed\xb0\x80").decode("ascii"),
            }
        )


class Testdecode_secret_data(unittest.TestCase):
    def test_unwraps_from_base64(self):
        self.assertEqual(
            api_utils.decode_secret_data({
                "k1": "a2V5MQ==",
                "k2": "w6TDtsO8",
                "k3": "Zm9vYmFy",
            }),
            {
                "k1": "key1",
                "k2": "äöü",
                "k3": "foobar",
            }
        )

    def test_custom_encoding(self):
        self.assertEqual(
            api_utils.decode_secret_data({
                "k1": "a2V5MQ==",
                "k2": "5Pb8",
                "k3": "Zm9vYmFy",
            }, encoding="latin1"),
            {
                "k1": "key1",
                "k2": "äöü",
                "k3": "foobar",
            }
        )


class Testget_cluster_domain(unittest.TestCase):
    @unittest.mock.patch("socket.gethostbyname_ex")
    @unittest.mock.patch.dict(os.environ, clear=True)
    def test_raises_on_nxdomain(self, gethostbyname_ex):
        api_utils.get_cluster_domain.cache_clear()
        gethostbyname_ex.side_effect = socket.gaierror

        self.assertRaises(KeyError, api_utils.get_cluster_domain)

    @unittest.mock.patch("socket.gethostbyname_ex")
    @unittest.mock.patch.dict(os.environ, clear=True)
    def test_gets_domain(self, gethostbyname_ex):
        api_utils.get_cluster_domain.cache_clear()
        gethostbyname_ex.return_value = (
            "kubernetes.default.svc.test.cluster.domain",)

        self.assertEqual(api_utils.get_cluster_domain(),
                         "test.cluster.domain")
        gethostbyname_ex.assert_called_once_with("kubernetes.default.svc")

    @unittest.mock.patch("socket.gethostbyname_ex")
    @unittest.mock.patch.dict(os.environ, {
        "YAOOK_OP_CLUSTER_DOMAIN": "abc.123"})
    def test_gets_domain_uses_envvar(self, gethostbyname_ex):
        api_utils.get_cluster_domain.cache_clear()

        self.assertEqual(api_utils.get_cluster_domain(),
                         "abc.123")
        gethostbyname_ex.assert_not_called()
