import unittest
import unittest.mock
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.watcher as watcher

import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.yaook as yaook

import ddt


@ddt.ddt
class TestYaookResourceReadinessMixin(unittest.IsolatedAsyncioTestCase):
    class YaookResourceReadynesTest(yaook.YaookResourceReadinessMixin,
                                    k8s.KubernetesReference):
        def __init__(self, resource_interface_factory, **kwargs):
            super().__init__(**kwargs)
            self.rif = resource_interface_factory
            self.get_mock = unittest.mock.Mock()

        def _create_resource_interface(self, api_client):
            return self.rif(api_client)

        async def get(self, ctx):
            return self.get_mock()

        async def get_all(self, ctx):
            raise NotImplementedError()

        async def reconcile(self, ctx):
            raise NotImplementedError()

        async def delete(self, ctx):
            raise NotImplementedError

        async def cleanup_orphans(self, ctx, protect):
            raise NotImplementedError

    def setUp(self):
        self.rif = unittest.mock.Mock([])
        self.ri = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        self.rif.return_value = self.ri
        self.fr = self.YaookResourceReadynesTest(
            resource_interface_factory=self.rif,
            component=sentinel.component,
        )

    @ddt.data(
        context.Phase.UPDATED,
        context.Phase.UPDATING,
    )
    async def test__is_updated_True(self, phase):
        ret = self.fr._is_updated({
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": phase.value,
                "updatedGeneration": 25,
            }
        })
        self.assertTrue(ret)

    @ddt.data(
        context.Phase.WAITING_FOR_DEPENDENCY,
        context.Phase.CREATED,
        context.Phase.BACKING_OFF,
    )
    async def test__is_updated_False(self, phase):
        ret = self.fr._is_updated({
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": phase.value,
                "updatedGeneration": 25,
            }
        })
        self.assertFalse(ret)

    @ddt.unpack
    @ddt.data(
        (25, 25, True),
        (2, 1, False),
        (1, 2, False),
        (1, 1, True),
    )
    async def test__is_updated_generation(
            self, generation, observed_generation, expected):
        ret = self.fr._is_updated({
            "metadata": {
                "generation": generation
            },
            "status": {
                "phase": context.Phase.UPDATED.value,
                "updatedGeneration": observed_generation,
            }
        })
        self.assertEqual(expected, ret)

    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    async def test_is_ready_calls__is_updated(self, _is_updated):
        self.fr.get_mock.return_value = kclient.V1ObjectReference(
            name=sentinel.name,
            namespace=sentinel.namespace)
        self.ri.read_status.return_value = {
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": context.Phase.UPDATED.value,
                "updatedGeneration": 25,
            }
        }

        ctx = unittest.mock.Mock()
        await self.fr.is_ready(ctx)
        _is_updated.assert_called_once_with(self.ri.read_status.return_value)

    @ddt.data(
        context.Phase.UPDATING,
        context.Phase.WAITING_FOR_DEPENDENCY,
        context.Phase.CREATED,
        context.Phase.BACKING_OFF,
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_event_calls__is_updated(
            self, phase, _is_updated):
        value = {
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": phase.value,
                "updatedGeneration": 25,
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.ADDED
        event.raw_object = value
        self.fr._handle_dependency_event(ctx, event)
        _is_updated.assert_called_once_with(value)

    @ddt.data(
        ("test.yaook.cloud", False),
        ("some.other.value", True),
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_ignores_resource_version_change_depending_cr(
            self, data, _is_updated):
        _is_updated.return_value = True
        self.ri.group = data[0]

        old_value = {
            "metadata": {
                "resourceVersion": 123,
                "generation": 25
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        value = {
            "metadata": {
                "resourceVersion": 124,
                "generation": 25
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.MODIFIED
        event.old_raw_object = old_value
        event.raw_object = value
        self.assertEqual(data[1], self.fr._handle_dependency_event(ctx, event))
        if not data[1]:
            _is_updated.assert_not_called()

    @ddt.data(
        ("test.yaook.cloud", False),
        ("some.other.value", True),
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_ignores_conditions_change_depending_cr(
            self, data, _is_updated):
        _is_updated.return_value = True
        self.ri.group = data[0]

        old_value = {
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
                "conditions": ["mycondition"],
            }
        }

        value = {
            "metadata": {
                "generation": 25
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
                "conditions": ["myothercondition"],
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.MODIFIED
        event.old_raw_object = old_value
        event.raw_object = value
        self.assertEqual(data[1], self.fr._handle_dependency_event(ctx, event))
        if not data[1]:
            _is_updated.assert_not_called()

    @ddt.data(
        ("test.yaook.cloud", False),
        ("some.other.value", True),
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_ignores_last_update_change_depending_cr(
            self, data, _is_updated):
        _is_updated.return_value = True
        self.ri.group = data[0]

        old_value = {
            "metadata": {
                "generation": 25,
                "annotations": {
                     context.ANNOTATION_LAST_UPDATE: "123",
                },
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        value = {
            "metadata": {
                "generation": 25,
                "annotations": {
                     context.ANNOTATION_LAST_UPDATE: "456",
                },
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.MODIFIED
        event.old_raw_object = old_value
        event.raw_object = value
        self.assertEqual(data[1], self.fr._handle_dependency_event(ctx, event))
        if not data[1]:
            _is_updated.assert_not_called()

    @ddt.data(
        ("test.yaook.cloud", False),
        ("some.other.value", True),
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_ignores_managed_fields_change_depending_cr(
            self, data, _is_updated):
        _is_updated.return_value = True
        self.ri.group = data[0]

        old_value = {
            "metadata": {
                "generation": 25,
                "managedFields": "something",
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        value = {
            "metadata": {
                "generation": 25,
                "managedFields": "else",
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.MODIFIED
        event.old_raw_object = old_value
        event.raw_object = value
        self.assertEqual(data[1], self.fr._handle_dependency_event(ctx, event))
        if not data[1]:
            _is_updated.assert_not_called()

    @ddt.data(
        ("test.yaook.cloud", False),
        ("some.other.value", True),
    )
    @unittest.mock.patch(
        "yaook.statemachine.yaook.YaookResourceReadinessMixin._is_updated")
    def test__handle_dependency_handles_real_change_depending_cr(
            self, data, _is_updated):
        _is_updated.return_value = True
        self.ri.group = data[0]

        old_value = {
            "metadata": {
                "generation": 25,
                "resourceVersion": 123,
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 25,
                "conditions": ["mycondition"],
            }
        }

        value = {
            "metadata": {
                "generation": 26,
                "resourceVersion": 124,
            },
            "status": {
                "phase": "UPDATED",
                "updatedGeneration": 26,
                "conditions": ["myothercondition"],
            }
        }

        ctx = unittest.mock.Mock()

        event = unittest.mock.Mock(watcher.StatefulWatchEvent)
        event.type_ = watcher.EventType.MODIFIED
        event.old_raw_object = old_value
        event.raw_object = value
        self.assertTrue(self.fr._handle_dependency_event(ctx, event))
        _is_updated.assert_called_once_with(value)

    async def test_is_ready_missing(self):
        ctx = unittest.mock.Mock(["namespace", "parent_name", "parent_kind",
                                  "parent_api_version", "instance"])
        self.fr.get_mock.side_effect = exceptions.ResourceNotPresent("", ctx)

        ret = await self.fr.is_ready(ctx)
        self.assertFalse(ret)

    async def test_is_ready_duplicate(self):
        ctx = unittest.mock.Mock(["namespace", "parent_name", "parent_kind",
                                  "parent_api_version", "instance"])
        self.fr.get_mock.side_effect = exceptions.AmbiguousRequest("", ctx)

        ret = await self.fr.is_ready(ctx)
        self.assertFalse(ret)

    async def test_is_ready_404(self):
        ctx = unittest.mock.Mock(["namespace", "parent_name", "parent_kind",
                                  "parent_api_version", "instance"])
        self.fr.get_mock.side_effect = exceptions.ResourceNotPresent(
            None, unittest.mock.Mock())

        ret = await self.fr.is_ready(ctx)
        self.assertFalse(ret)

    async def test_get_listeners(self):
        ret = self.fr.get_listeners()[-1]
        self.assertIsInstance(ret, context.KubernetesListener)
        self.assertEqual(ret.listener, self.fr._handle_dependency_event)


@ddt.ddt
class TestYaookReadyResource(unittest.IsolatedAsyncioTestCase):
    class YRR(yaook.YaookReadyResource):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        def _create_resource_interface(self, api_client):
            raise NotImplementedError()

        async def _make_body(self, ctx, dep):
            raise NotImplementedError()

        async def _needs_update(self, current, new):
            raise NotImplementedError()

    def setUp(self):
        self.yrr = self.YRR(
            component=sentinel.component,
        )

    def test_uses_yaook_resource_readyness(self):
        self.assertIsInstance(self.yrr, yaook.YaookResourceReadinessMixin)

    def test_uses_single_object_state(self):
        self.assertIsInstance(self.yrr, k8s.SingleObject)
