import base64
import contextlib
import pathlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.certmanager as certmanager
import yaook.statemachine.resources.k8s_storage as k8s_storage
import yaook.statemachine.versioneddependencies as versioneddependencies
import yaook.statemachine.context as context

from .utils import ResourceTestMixin


class TestConfigMap(unittest.IsolatedAsyncioTestCase):
    class ConfigMapTest(ResourceTestMixin, k8s_storage.ConfigMap):
        pass

    def test_needs_update_compares_data(self):
        m = unittest.mock.MagicMock()
        cm = self.ConfigMapTest()

        for r in [True, False]:
            m.data.reset_mock()
            m.data.__ne__.return_value = r
            result = cm._needs_update(
                m,
                {"data": unittest.mock.sentinel.new_data},
            )
            m.data.__ne__.assert_called_once_with(
                unittest.mock.sentinel.new_data
            )
            self.assertEqual(result, r)

    def test_needs_update_returns_true_on_label_change(self):
        cm = self.ConfigMapTest()

        old = kclient.V1ConfigMap(
            data={},
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                },
            )
        )

        new = {
            "data": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar"
                },
            },
        }

        self.assertTrue(
            cm._needs_update(old, new)
        )

    async def test_adopt_object_calls_inherited_and_sets_immutable_flag_for_cow(self):  # noqa:E501
        cm = self.ConfigMapTest(copy_on_write=True)
        obj = {}

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.k8s_storage.SingleObject.adopt_object",
                new=unittest.mock.AsyncMock(),
            ))

            await cm.adopt_object(sentinel.ctx, obj)

        adopt_object.assert_awaited_once_with(sentinel.ctx, obj)
        self.assertEqual(obj["immutable"], True)

    async def test_adopt_object_calls_inherited_and_sets_immutable_to_false_for_non_cow(self):  # noqa:E501
        cm = self.ConfigMapTest(copy_on_write=False)
        obj = {}

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.k8s_storage.SingleObject.adopt_object",
                new=unittest.mock.AsyncMock(),
            ))

            await cm.adopt_object(sentinel.ctx, obj)

        adopt_object.assert_awaited_once_with(sentinel.ctx, obj)
        self.assertEqual(obj["immutable"], False)


@ddt.ddt
class TestPolicyConfigMap(unittest.IsolatedAsyncioTestCase):

    default_policy_filename = "policy.json"
    default_default_policy_filename = "policy.yaml"

    @ddt.data(None, "dummy")
    def test_constructor(self, policy_filename):
        kwargs = {
            "metadata": "some metadata",
            "policy_spec_key": "parent_key",
        }
        expected_filename = self.default_policy_filename
        if policy_filename:
            expected_filename = policy_filename
            kwargs.update({"filename": policy_filename})

        cm = k8s_storage.PolicyConfigMap(**kwargs)

        self.assertEqual(cm._filename, expected_filename)

    @unittest.mock.patch("yaook.common.policy.build_policy_configmap")
    @unittest.mock.patch("yaook.statemachine.k8s_storage.evaluate_metadata")
    async def test__make_body(
            self,
            mock_evaluate_metadata,
            mock_build_policy_configmap):

        # arrange path to default policy file
        expected_owner = type(self)
        abs_path = pathlib.Path(__file__)
        self_path_pieces = self.__module__.split('.')
        for _ in self_path_pieces:
            abs_path = abs_path.parent
        path_pieces = [
            "tests",
            "statemachine",
            "resources",
            "static",
            "default_policies.yaml"
        ]
        expected_default_policy_filepath = \
            abs_path / pathlib.Path(*path_pieces)

        # arrange configmap metadata
        provided_metadata = sentinel.metadata_provider
        expected_metadata = "some metadata"
        mock_evaluate_metadata.return_value = expected_metadata

        # arrange configmap data
        expected_data = "some data"
        mock_build_policy_configmap.return_value = expected_data

        # arrange expected call to build_policy_configmap
        expected_policy_filename = "some policy filename"
        expected_policies = {"pol_key1": "pol_value1"}
        mock_ctx = unittest.mock.Mock(["namespace", "parent_spec"])
        provided_parent_key = "some key"
        mock_ctx.parent_spec = {provided_parent_key: expected_policies}

        # arrange policyconfigmapstate
        cm = k8s_storage.PolicyConfigMap(
            metadata=provided_metadata,
            policy_spec_key=provided_parent_key,
            filename=expected_policy_filename,
        )
        cm._owned_by = expected_owner

        # act
        result = await cm._make_body(mock_ctx, sentinel.deps)

        # assert
        mock_evaluate_metadata.assert_called_once_with(
            mock_ctx,
            provided_metadata,
        )
        mock_build_policy_configmap.assert_called_once_with(
            expected_policies,
            defaults_filename=expected_default_policy_filepath,
            policy_filename=expected_policy_filename
        )
        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": expected_metadata,
                "data": expected_data,
            }
        )


@ddt.ddt
class TestReleaseAwarePolicyConfigMap(unittest.IsolatedAsyncioTestCase):
    def test_constructor(self):
        expected_metadata = "some metadata"
        vdep = versioneddependencies.VersionedDockerImage(
            "imageurl",
            versioneddependencies.SemVerSelector([]),
        )
        kwargs = {
            "metadata": expected_metadata,
            "versioned_dependencies": vdep,
        }

        cm = k8s_storage.ReleaseAwarePolicyConfigMap(**kwargs)

        self.assertEqual(cm._metadata, expected_metadata)
        self.assertEqual(cm._versioned_dependencies, vdep)

    @unittest.mock.patch("yaook.common.validator.build_policy_configmap")
    @unittest.mock.patch("yaook.statemachine.k8s_storage.evaluate_metadata")
    async def test__make_body(
            self,
            mock_evaluate_metadata,
            mock_build_policy_configmap):

        # arrange path to default policy file
        expected_owner = type(self)
        # arrange configmap metadata
        provided_metadata = sentinel.metadata_provider
        expected_metadata = {"key": "some metadata"}
        mock_evaluate_metadata.return_value = expected_metadata
        # this annotation will be added in make_body
        expected_metadata.update(
            {
                "annotations": {
                    context.ANNOTATION_VERSIONED_DEPENDENCY: "",
                    context.ANNOTATION_POLICY: "test_policies"
                }
            }
        )

        mock_ctx = unittest.mock.Mock(["namespace", "parent_spec"])
        mock_ctx.parent_spec.get.return_value = "test_policies"

        # arrange policyconfigmapstate
        cm = k8s_storage.ReleaseAwarePolicyConfigMap(
            metadata=provided_metadata,
        )
        cm._owned_by = expected_owner

        # act
        result = await cm._make_body(mock_ctx, sentinel.deps)

        # assert
        mock_evaluate_metadata.assert_called_once_with(
            mock_ctx,
            provided_metadata,
        )
        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": expected_metadata,
                "data": {},
            }
        )


class TestCAConfigMap(unittest.IsolatedAsyncioTestCase):

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    @unittest.mock.patch("yaook.statemachine.k8s_storage.evaluate_metadata")
    async def test__make_body_uses_evaluate_metadata(
            self, evaluate_metadata, build_ca_configmap):
        cm = k8s_storage.CAConfigMap(
            metadata=sentinel.metadata_provider,
        )

        build_ca_configmap.return_value = {"ca-bundle.crt": sentinel.cadata}
        evaluate_metadata.return_value = sentinel.metadata

        result = await cm._make_body(sentinel.ctx, sentinel.deps)

        evaluate_metadata.assert_called_once_with(
            sentinel.ctx,
            sentinel.metadata_provider,
        )
        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": sentinel.metadata,
                "data": {"ca-bundle.crt": sentinel.cadata},
            }
        )

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    async def test__make_body_constructs_configmap_with_cas(
            self, build_ca_configmap):
        cm = k8s_storage.CAConfigMap(
            metadata=lambda x: {"k": sentinel.metadata},
        )

        build_ca_configmap.return_value = {
            "ca-bundle.crt": unittest.mock.sentinel.cabundle,
            "12345.ca": unittest.mock.sentinel.singleca
        }

        result = await cm._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.deps,
        )

        build_ca_configmap.assert_called_once_with(set())

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "k": sentinel.metadata,
                },
                "data": {
                    "ca-bundle.crt": unittest.mock.sentinel.cabundle,
                    "12345.ca": unittest.mock.sentinel.singleca
                },
            }
        )

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    @unittest.mock.patch("yaook.statemachine.interfaces.issuer_interface")
    @unittest.mock.patch("yaook.statemachine.interfaces.secret_interface")
    @unittest.mock.patch("yaook.statemachine.k8s_storage.extract_password")
    async def test__make_body_constructs_configmap_from_issuer_ref(
            self, extract_password, secret_interface, issuer_interface,
            build_ca_configmap):

        issuer = unittest.mock.AsyncMock()
        issuer.read.return_value = {"spec": {"ca": {
                                              "secretName": "doener"}}}
        issuer_interface.return_value = issuer

        secret = unittest.mock.AsyncMock()
        secret.read.return_value = unittest.mock.MagicMock(["metadata"])
        secret_interface.return_value = secret

        issuer_ref = unittest.mock.MagicMock(["name"])
        issuer_ref.name = "test"

        cm = k8s_storage.CAConfigMap(
            metadata=lambda x: {"k": sentinel.metadata},
            issuer_ref=issuer_ref,
        )

        build_ca_configmap.return_value = {
            "root.ca": unittest.mock.sentinel.singleca
        }

        ctx = unittest.mock.Mock(["parent_spec", "api_client", "namespace"])
        parent_spec = unittest.mock.MagicMock()
        parent_spec.get.return_value = {"name": "doener"}
        ctx.parent_spec = parent_spec

        extract_password.return_value = unittest.mock.sentinel.secret_cadata

        result = await cm._make_body(
            ctx,
            unittest.mock.sentinel.deps,
        )

        build_ca_configmap.assert_called_once_with({sentinel.secret_cadata})
        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "k": sentinel.metadata,
                },
                "data": {
                    "root.ca": unittest.mock.sentinel.singleca
                },
            }
        )

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    async def test__get_user_certs_extracts_certs_from_cr(
            self, build_ca_configmap):
        cm = k8s_storage.CAConfigMap(
            metadata=lambda x: {"k": sentinel.metadata},
            usercerts_spec_key=unittest.mock.sentinel.spec_key,
        )

        build_ca_configmap.return_value = {
            "ca-bundle.crt": unittest.mock.sentinel.cabundle,
            "12345.ca": unittest.mock.sentinel.singleca
        }

        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            unittest.mock.sentinel.spec_key: ["cert01", "cert02"],
        }

        result = await cm._make_body(
            ctx,
            unittest.mock.sentinel.deps,
        )

        build_ca_configmap.assert_called_once_with({"cert01", "cert02"})
        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "k": sentinel.metadata,
                },
                "data": {
                    "ca-bundle.crt": unittest.mock.sentinel.cabundle,
                    "12345.ca": unittest.mock.sentinel.singleca
                },
            }
        )

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    @unittest.mock.patch("yaook.statemachine.k8s_storage.extract_password")
    async def test__get_cas_from_secrets_extracts_correctly(
            self, extract_password, build_ca_configmap):
        cert_secret = certmanager.EmptyTlsSecret(
            unittest.mock.sentinel.secretname
        )

        cm = k8s_storage.CAConfigMap(
            metadata="test-ca-certificates",
            certificate_secrets_states=[cert_secret]
        )

        build_ca_configmap.return_value = {
            "ca-bundle.crt": unittest.mock.sentinel.cabundle,
            "12345.ca": unittest.mock.sentinel.singleca
        }
        extract_password.return_value = unittest.mock.sentinel.secret_cadata

        await cm._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.deps,
        )

        build_ca_configmap.assert_called_once_with({
            unittest.mock.sentinel.secret_cadata
        })

    @unittest.mock.patch("yaook.common.ca.build_ca_configmap")
    async def test_sets_copy_on_write_per_default(self, build_ca_configmap):

        cm = k8s_storage.CAConfigMap(
            metadata="test-ca-certificates",
        )

        self.assertTrue(cm.copy_on_write)


class TestSecret(unittest.IsolatedAsyncioTestCase):
    class SecretTest(ResourceTestMixin, k8s_storage.Secret):
        pass

    def test_needs_update_compares_type_and_data(self):
        m = unittest.mock.MagicMock()
        s = self.SecretTest()
        new = {
            "type": unittest.mock.sentinel.type_,
            "data": unittest.mock.sentinel.data,
        }

        for rtype in [True, False]:
            for rdata in [True, False]:
                m.data.reset_mock()
                m.data.__ne__.return_value = rdata
                m.type.reset_mock()
                m.type.__ne__.return_value = rtype

                result = s._needs_update(m, new)

                m.type.__ne__.assert_called_once_with(
                    unittest.mock.sentinel.type_
                )
                if not rtype:
                    m.data.__ne__.assert_called_once_with(
                        unittest.mock.sentinel.data,
                    )

                self.assertEqual(result, rtype or rdata)

    def test_needs_update_returns_true_on_label_change(self):
        s = self.SecretTest()

        old = kclient.V1Secret(
            data={},
            type="",
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )

        new = {
            "type": "",
            "data": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar"
                },
            },
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    async def test_adopt_object_calls_inherited_and_sets_immutable_flag_for_cow(self):  # noqa:E501
        s = self.SecretTest(copy_on_write=True)
        obj = {}

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.k8s_storage.SingleObject.adopt_object",
                new=unittest.mock.AsyncMock(),
            ))

            await s.adopt_object(sentinel.ctx, obj)

        adopt_object.assert_awaited_once_with(sentinel.ctx, obj)
        self.assertEqual(obj["immutable"], True)

    async def test_adopt_object_calls_inherited_and_sets_immutable_to_false_for_non_cow(self):  # noqa:E501
        s = self.SecretTest(copy_on_write=False)
        obj = {}

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.k8s_storage.SingleObject.adopt_object",
                new=unittest.mock.AsyncMock(),
            ))

            await s.adopt_object(sentinel.ctx, obj)

        adopt_object.assert_awaited_once_with(sentinel.ctx, obj)
        self.assertEqual(obj["immutable"], False)


class Testgenerate_password(unittest.TestCase):
    def test_defaults_to_32_bytes_of_entropy(self):
        with contextlib.ExitStack() as stack:
            token = stack.enter_context(unittest.mock.patch(
                "secrets.token_urlsafe"
            ))
            token.return_value = "generated password"

            result = k8s_storage.generate_password()

        token.assert_called_once_with(32)

        self.assertIn("generated password", result)

    def test_generates_non_evil_password(self):
        password_raw = str(uuid.uuid4())

        with contextlib.ExitStack() as stack:
            token = stack.enter_context(unittest.mock.patch(
                "secrets.token_urlsafe"
            ))
            token.return_value = password_raw

            result = k8s_storage.generate_password(sentinel.nbytes)

        token.assert_called_once_with(sentinel.nbytes)

        self.assertEqual(result, password_raw)


class TestAutoGeneratedPassword(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.agp = k8s_storage.AutoGeneratedPassword(
            metadata=("resource-name-", True),
        )

    def test_needs_update_returns_true_if_password_is_missing_in_old(self):
        current = unittest.mock.Mock(["data"])
        current.data = {"unrelated": "blob"}
        self.assertTrue(
            self.agp._needs_update(current, {"data": {"password": "foo"}})
        )

    def test_needs_update_returns_false_if_password_is_missing_in_both(self):
        current = unittest.mock.Mock(["data"])
        current.data = {"unrelated": "blob"}
        self.assertFalse(
            self.agp._needs_update(current, {"data": {}})
        )

    def test_needs_update_returns_false_if_password_is_present_in_both(self):
        current = unittest.mock.Mock(["data"])
        current.data = {"unrelated": "blob", "password": "something"}
        self.assertFalse(
            self.agp._needs_update(
                current,
                {"data": {"password": "something-else"}}
            )
        )

    async def test__make_body_uses_generate_password_by_default(self):
        password_raw = str(uuid.uuid4())
        password_b64 = base64.b64encode(
            password_raw.encode("ascii")
        ).decode(
            "ascii"
        )

        self.assertEquals(self.agp.generator, k8s_storage.generate_password)

        self.agp.generator = unittest.mock.MagicMock()
        self.agp.generator.return_value = password_raw

        result = await self.agp._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.agp.generator.assert_called_once_with(32)

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "generateName": "resource-name-",
                },
                "type": "Opaque",
                "data": {
                    "password": password_b64,
                }
            }
        )

    async def test__make_body_uses_passed_generator(self):
        password_raw = str(uuid.uuid4())
        password_b64 = base64.b64encode(
            password_raw.encode("ascii")
        ).decode(
            "ascii"
        )

        generate_password = unittest.mock.MagicMock()
        generate_password.return_value = password_raw
        agp = k8s_storage.AutoGeneratedPassword(
            metadata=("resource-name-", True),
            generator=generate_password,
        )

        result = await agp._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.dependencies,
        )

        generate_password.assert_called_once_with(32)

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "generateName": "resource-name-",
                },
                "type": "Opaque",
                "data": {
                    "password": password_b64,
                }
            }
        )

    async def test__make_body_uses_passed_generator_and_length(self):
        expected_length = 10
        password_raw = str(uuid.uuid4())
        password_b64 = base64.b64encode(
            password_raw.encode("ascii")
        ).decode(
            "ascii"
        )

        generate_password = unittest.mock.MagicMock()
        generate_password.return_value = password_raw
        agp = k8s_storage.AutoGeneratedPassword(
            metadata=("resource-name-", True),
            nbytes=expected_length,
            generator=generate_password,
        )
        result = await agp._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.dependencies,
        )

        generate_password.assert_called_once_with(expected_length)

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "generateName": "resource-name-",
                },
                "type": "Opaque",
                "data": {
                    "password": password_b64,
                }
            }
        )

    async def test__make_body_uses_evaluate_metadata(self):
        password_raw = str(uuid.uuid4())

        self.agp = k8s_storage.AutoGeneratedPassword(
            metadata=sentinel.metadata_provider,
        )

        with contextlib.ExitStack() as stack:
            token = stack.enter_context(unittest.mock.patch(
                "secrets.token_urlsafe"
            ))
            token.return_value = password_raw

            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.k8s_storage.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            result = await self.agp._make_body(
                sentinel.ctx,
                sentinel.dependencies,
            )

        evaluate_metadata.assert_called_once_with(
            sentinel.ctx,
            sentinel.metadata_provider,
        )

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": sentinel.metadata,
                "type": "Opaque",
                "data": {
                    "password": unittest.mock.ANY,
                }
            }
        )

    async def test__make_body_generates_secret_with_password_of_custom_length(self):  # noqa
        password_raw = str(uuid.uuid4())
        password_b64 = base64.b64encode(
            (password_raw).encode("ascii")
        ).decode(
            "ascii"
        )

        self.agp = k8s_storage.AutoGeneratedPassword(
            metadata=("resource-name-", True),
            nbytes=42,
        )

        with contextlib.ExitStack() as stack:
            token = stack.enter_context(unittest.mock.patch(
                "secrets.token_urlsafe"
            ))
            token.return_value = password_raw

            result = await self.agp._make_body(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.dependencies,
            )

        token.assert_called_once_with(42)

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "generateName": "resource-name-",
                },
                "type": "Opaque",
                "data": {
                    "password": password_b64,
                }
            }
        )
