#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import ddt
import itertools
import logging
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

from datetime import datetime

import kubernetes_asyncio.client

import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.watcher as watcher

import yaook.statemachine.resources.instancing as instancing

from .utils import (
    wrap_node_list,
    SingleObjectMock,
)


@ddt.ddt()
class TestInstanceGroup(unittest.IsolatedAsyncioTestCase):
    @ddt.data("0", "0%")
    def test_get_max_unavailable_for_zero(self, max_unavailable):
        group = instancing.InstanceGroup(None, max_unavailable, None)
        self.assertEquals(0, group.get_max_unavailable(None))

    @ddt.unpack
    @ddt.data(
        ("0", 0),
        ("10", 10),
        ("-1", -1),
        ("42", 42),
        ("1337", 1337),
    )
    def test_get_max_unavailable_for_fixed_number(self, max_unavailable,
                                                  expected):
        group = instancing.InstanceGroup(None, max_unavailable, None)
        self.assertEquals(expected, group.get_max_unavailable(None))

    @ddt.unpack
    @ddt.data(
        # max_unavailable, amount of configured instances, expected
        ("0%", 100, 0),
        ("10%", 100, 10),
        ("1%", 100, 1),
        ("1%", 10, 1),
        ("1%", 1, 1),
        ("15%", 10, 1),
        ("15%", 20, 3),
    )
    def test_get_max_unavailable_for_percentage(self, max_unavailable,
                                                configured,
                                                expected):
        group = instancing.InstanceGroup(None, max_unavailable, None)
        self.assertEquals(expected, group.get_max_unavailable(configured))


class TestInstancedResource(unittest.IsolatedAsyncioTestCase):
    class InstancedResourceTest(instancing.InstancedResource):
        async def reconcile(self, *args, **kwargs):
            raise NotImplementedError

        async def get_target_instances(self, ctx):
            pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock(
            component=self.component,
            add_dependencies=[unittest.mock.sentinel.dependency_wrapped],
        )
        self.ir = self.InstancedResourceTest(
            wrapped_state=self.wrapped,
            component=self.component,
            add_dependencies=[unittest.mock.sentinel.dependency_owned],
        )

    def test_combines_dependencies(self):
        self.assertIn(
            unittest.mock.sentinel.dependency_wrapped,
            self.ir._dependencies,
        )
        self.assertIn(
            unittest.mock.sentinel.dependency_owned,
            self.ir._dependencies,
        )
        self.assertEqual(
            len(self.ir._dependencies),
            2,
        )

    def test_returns_listeners_from_wrapped_state(self):
        with unittest.mock.patch.object(
                self.wrapped,
                "get_listeners") as get_listeners:
            get_listeners.return_value = [
                unittest.mock.sentinel.wrapped_listener
            ]

            listeners = self.ir.get_listeners()

        self.assertIn(
            unittest.mock.sentinel.wrapped_listener,
            listeners,
        )

    def test_resource_interface_from_wrapped_state(self):
        with unittest.mock.patch.object(
                self.wrapped,
                "_autocreate_resource_interface") as cri:
            ri = self.ir._create_resource_interface(
                unittest.mock.sentinel.api_client,
            )

        cri.assert_called_once_with(unittest.mock.sentinel.api_client)

        self.assertEqual(ri, cri())

    async def test_get_raises_AmbiguousRequest_if_called_without_instance(
            self):
        ctx = unittest.mock.Mock(["parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance",
                                  "namespace"])
        ctx.instance = None

        with self.assertRaises(exceptions.AmbiguousRequest):
            await self.ir.get(ctx)

    async def test_get_calls_wrapped_if_called_with_instance(self):
        ctx = unittest.mock.Mock(["instance"])
        ctx.instance = unittest.mock.sentinel.instance

        with unittest.mock.patch.object(
                self.wrapped, "get",
                new=unittest.mock.AsyncMock()) as wrapped_get:
            wrapped_get.return_value = unittest.mock.sentinel.result

            result = await self.ir.get(ctx)

        wrapped_get.assert_awaited_once_with(ctx)
        self.assertEqual(result, unittest.mock.sentinel.result)

    async def test_get_all_calls_wrapped(self):
        ctx = unittest.mock.sentinel.ctx

        with unittest.mock.patch.object(
                self.wrapped, "get_all",
                new=unittest.mock.AsyncMock()) as wrapped_get_all:
            wrapped_get_all.return_value = unittest.mock.sentinel.result

            result = await self.ir.get_all(ctx)

        wrapped_get_all.assert_awaited_once_with(ctx, False)
        self.assertEqual(result, unittest.mock.sentinel.result)

    def test_labels_calls_wrapped(self):
        ctx = unittest.mock.sentinel.ctx

        with unittest.mock.patch.object(
                self.wrapped, "labels") as wrapped_labels:
            wrapped_labels.return_value = unittest.mock.sentinel.result

            result = self.ir.labels(ctx)

        wrapped_labels.assert_called_once_with(ctx)
        self.assertEqual(result, unittest.mock.sentinel.result)

    async def test_get_used_resources_returns_union_for_all_resources(self):
        def generate_results():
            yield [
                unittest.mock.sentinel.f1,
                unittest.mock.sentinel.f2,
            ]
            yield [
                unittest.mock.sentinel.f2,
                unittest.mock.sentinel.f3,
            ]
            yield [
                unittest.mock.sentinel.f3,
                unittest.mock.sentinel.f4,
            ]

        def instanced(instance):
            return {unittest.mock.sentinel.instance: instance}

        ctx = unittest.mock.Mock(["with_instance"])
        ctx.with_instance.side_effect = instanced

        with contextlib.ExitStack() as stack:
            get_all = stack.enter_context(unittest.mock.patch.object(
                self.ir, "get_all",
            ))
            get_all.return_value = {
                unittest.mock.sentinel.node1: unittest.mock.sentinel.dummy,
                unittest.mock.sentinel.node2: unittest.mock.sentinel.dummy,
                unittest.mock.sentinel.node3: unittest.mock.sentinel.dummy,
            }

            get_used_resources = stack.enter_context(
                unittest.mock.patch.object(
                    self.wrapped, "get_used_resources",
                )
            )
            get_used_resources.side_effect = generate_results()

            result = await self.ir.get_used_resources(ctx)

        get_all.assert_awaited_once_with(ctx, True)

        self.assertCountEqual(
            get_used_resources.await_args_list,
            [
                unittest.mock.call({
                    unittest.mock.sentinel.instance:
                        unittest.mock.sentinel.node1
                }),
                unittest.mock.call({
                    unittest.mock.sentinel.instance:
                        unittest.mock.sentinel.node2
                }),
                unittest.mock.call({
                    unittest.mock.sentinel.instance:
                        unittest.mock.sentinel.node3
                }),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.f1,
                unittest.mock.sentinel.f2,
                unittest.mock.sentinel.f3,
                unittest.mock.sentinel.f4,
            ]
        )

    def test___set_name___forwards_to_wrapped(self):
        wrapped = unittest.mock.MagicMock(["__set_name__", "_dependencies"])
        ir = self.InstancedResourceTest(wrapped_state=wrapped)

        ir.__set_name__(unittest.mock.sentinel.owner,
                        unittest.mock.sentinel.name)

        wrapped.__set_name__.assert_called_once_with(
            unittest.mock.sentinel.owner,
            unittest.mock.sentinel.name,
        )

    def test___set_name___forwards_to_superclass(self):
        with contextlib.ExitStack() as stack:
            set_name = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.KubernetesResource.__set_name__",
            ))

            wrapped = unittest.mock.MagicMock(["__set_name__",
                                               "_dependencies"])
            ir = self.InstancedResourceTest(wrapped_state=wrapped)

            ir.__set_name__(unittest.mock.sentinel.owner,
                            unittest.mock.sentinel.name)

        set_name.assert_called_once_with(
            unittest.mock.sentinel.owner,
            unittest.mock.sentinel.name,
        )

    async def test_delete_calls_orphan_on_all_nested_resources(self):
        intf = unittest.mock.Mock([])
        intf.list_ = unittest.mock.AsyncMock()
        intf.list_.return_value = [
            sentinel.obj1,
            sentinel.obj2,
            sentinel.obj3,
        ]

        def metadata_transform(obj):
            return {
                "namespace": f"{obj}-namespace",
                "name": f"{obj}-name"
            }

        wrapped = unittest.mock.Mock(["get_resource_interface", "labels"])
        wrapped._dependencies = []
        wrapped._orphan = unittest.mock.AsyncMock()
        wrapped.get_resource_interface.return_value = intf
        wrapped.labels.return_value = sentinel.labels

        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = metadata_transform

            ir = self.InstancedResourceTest(wrapped_state=wrapped)

            await ir.delete(ctx, dependencies=sentinel.deps)

        wrapped.get_resource_interface.assert_called_once_with(ctx)
        wrapped.labels.assert_called_once_with(ctx)
        intf.list_.assert_awaited_once_with(
            ctx.namespace,
            label_selector=sentinel.labels,
        )
        self.assertCountEqual(
            wrapped._orphan.await_args_list,
            [
                unittest.mock.call(ctx,
                                   "sentinel.obj1-namespace",
                                   "sentinel.obj1-name"),
                unittest.mock.call(ctx,
                                   "sentinel.obj2-namespace",
                                   "sentinel.obj2-name"),
                unittest.mock.call(ctx,
                                   "sentinel.obj3-namespace",
                                   "sentinel.obj3-name"),
            ]
        )


class _PerNodeMixinBase:
    def __init__(self, component):
        self.component = component

    def get_listeners(self):
        return []


class _PerNodeMixinTest(instancing.PerNodeMixin,
                        _PerNodeMixinBase):
    pass


class TestPerNodeMixin(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.pnm = _PerNodeMixinTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

    def test_subscribes_to_node_updates_as_broadcast(self):
        self.assertIn(
            context.KubernetesListener(
                '', 'v1', 'nodes',
                self.pnm._handle_node_event,
                broadcast=True, component=self.component),
            self.pnm.get_listeners(),
        )

    def test_get_listeners_adds_from_superclass(self):
        with contextlib.ExitStack() as stack:
            get_listeners_super = stack.enter_context(
                unittest.mock.patch.object(
                    _PerNodeMixinBase, "get_listeners",
                )
            )
            get_listeners_super.return_value = [
                unittest.mock.sentinel.a,
                unittest.mock.sentinel.b,
            ]

            result = self.pnm.get_listeners()

        self.assertIn(unittest.mock.sentinel.a, result)
        self.assertIn(unittest.mock.sentinel.b, result)

    def test__handle_node_event_returns_true_if_added(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.ADDED

        self.assertTrue(self.pnm._handle_node_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_node_event_returns_true_if_deleted(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        self.assertTrue(self.pnm._handle_node_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_node_event_returns_true_if_old_object_is_none(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = None

        self.assertTrue(self.pnm._handle_node_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_node_event_returns_true_if_labels_changed(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.metadata = unittest.mock.Mock([])
        ev.old_object.metadata.labels = unittest.mock.sentinel.foo
        ev.object_ = unittest.mock.Mock([])
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.labels = unittest.mock.sentinel.bar

        self.assertTrue(self.pnm._handle_node_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_node_event_returns_false_if_labels_unchanged(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.metadata = unittest.mock.Mock([])
        ev.old_object.metadata.labels = unittest.mock.sentinel.foo
        ev.object_ = unittest.mock.Mock([])
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.labels = unittest.mock.sentinel.foo

        self.assertFalse(self.pnm._handle_node_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def _make_selector(self, api_str):
        selector = unittest.mock.Mock(["as_api_selector"])
        selector.as_api_selector.return_value = api_str
        return selector

    def _node_mock(self, name):
        node = unittest.mock.Mock([])
        node.metadata = unittest.mock.Mock([])
        node.metadata.name = name
        node.metadata.labels = sentinel.labels
        return node

    def test__get_node_selectors_wraps_scheduling_keys_in_LabelSelectors(self):
        self.assertCountEqual(
            self.pnm._get_node_selectors(unittest.mock.sentinel.ctx),
            [
                api_utils.LabelSelector(match_expressions=[
                    api_utils.LabelExpression(
                        key="foo",
                        operator=api_utils.SelectorOperator.EXISTS,
                        values=None,
                    ),
                ]),
                api_utils.LabelSelector(match_expressions=[
                    api_utils.LabelExpression(
                        key="bar",
                        operator=api_utils.SelectorOperator.EXISTS,
                        values=None,
                    ),
                ]),
            ],
        )

    async def test__get_target_nodes_uses_get_selected_nodes_union(self):
        ctx = unittest.mock.Mock(["api_client"])

        with contextlib.ExitStack() as stack:
            get_node_selectors = stack.enter_context(
                unittest.mock.patch.object(
                    self.pnm, "_get_node_selectors"
                )
            )
            get_node_selectors.return_value = unittest.mock.sentinel.selectors

            get_selected_nodes_union = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.get_selected_nodes_union",
                new=unittest.mock.AsyncMock(),
            ))
            get_selected_nodes_union.return_value = [
                unittest.mock.sentinel.node1,
                unittest.mock.sentinel.node2,
                unittest.mock.sentinel.node3,
            ]

            result = await self.pnm._get_target_nodes(ctx)

        get_node_selectors.assert_called_once_with(ctx)
        get_selected_nodes_union.assert_called_once_with(
            ctx.api_client,
            unittest.mock.sentinel.selectors,
        )

        self.assertEqual(
            result,
            [
                unittest.mock.sentinel.node1,
                unittest.mock.sentinel.node2,
                unittest.mock.sentinel.node3,
            ]
        )

    async def test__get_target_nodes_deduplicates_nodes_by_name(self):
        ctx = unittest.mock.Mock(["api_client"])

        node_lists = [
            [
                self._node_mock("node0"),
                self._node_mock("node1"),
            ],
            [
                self._node_mock("node1"),
                self._node_mock("node2"),
            ],
            [
                self._node_mock("node2"),
                self._node_mock("node3"),
            ],
        ]

        def node_list_generator():
            for node_list in node_lists:
                yield wrap_node_list(node_list)

        v1 = unittest.mock.Mock(["list_node"])
        v1.list_node = unittest.mock.AsyncMock()
        v1.list_node.side_effect = node_list_generator()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            get_node_selectors = stack.enter_context(
                unittest.mock.patch.object(
                    self.pnm, "_get_node_selectors"
                )
            )
            get_node_selectors.return_value = [
                self._make_selector(unittest.mock.sentinel.selector1),
                self._make_selector(unittest.mock.sentinel.selector2),
                self._make_selector(unittest.mock.sentinel.selector3),
            ]

            result = await self.pnm._get_target_nodes(ctx)

        get_node_selectors.assert_called_once_with(ctx)
        CoreV1Api.assert_called_once_with(ctx.api_client)

        self.assertCountEqual(
            v1.list_node.mock_calls,
            [
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector1),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector2),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector3),
            ],
        )

        self.assertCountEqual(
            [node.metadata.name for node in result],
            [
                "node0",
                "node1",
                "node2",
                "node3",
            ]
        )

    async def test_get_target_instances_returns_node_names(self):
        with contextlib.ExitStack() as stack:
            get_target_nodes = stack.enter_context(unittest.mock.patch.object(
                self.pnm, "_get_target_nodes",
            ))
            get_target_nodes.return_value = [
                self._node_mock("node0"),
                self._node_mock("node1"),
                self._node_mock("node2"),
                self._node_mock("node3"),
            ]

            result = await self.pnm.get_target_instances(
                unittest.mock.sentinel.ctx,
            )

        get_target_nodes.assert_awaited_once_with(unittest.mock.sentinel.ctx)

        self.assertCountEqual(
            result,
            [
                "node0",
                "node1",
                "node2",
                "node3",
            ],
        )


class TestStatelessInstancedResource(unittest.IsolatedAsyncioTestCase):
    class StatelessInstancedResourceTest(
            instancing.StatelessInstancedResource):
        async def get_target_instances(self, ctx):
            raise NotImplementedError

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.sir = self.StatelessInstancedResourceTest(
            wrapped_state=self.wrapped,
            component=self.component,
        )

    async def test__get_current_metadata_fetches_by_labels_and_extracts_metadata(self):  # NOQA
        ctx = unittest.mock.Mock(["api_client", "namespace"])

        self.wrapped.resource_interface_mock.list_.return_value = [
            "object1", "object2", "object3",
        ]

        labels = {
            "foo": "bar",
            "baz": "fnord",
        }
        selector = api_utils.LabelSelector(
            match_labels=labels,
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.NOT_EXISTS,
                values=None,
            )],
        )

        with contextlib.ExitStack() as stack:
            def extract_metadata_proc(obj):
                return {
                    "namespace": "ns-{}".format(obj),
                    "name": "name-{}".format(obj),
                }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "labels",
            ))
            labels_mock.return_value = labels

            result = await self.sir._get_current_metadata(ctx)

        self.wrapped.resource_interface_mock.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=selector.as_api_selector(),
        )

        self.assertCountEqual(
            result,
            [
                {
                    "namespace": "ns-object1",
                    "name": "name-object1",
                },
                {
                    "namespace": "ns-object2",
                    "name": "name-object2",
                },
                {
                    "namespace": "ns-object3",
                    "name": "name-object3",
                },
            ]
        )

    def test__find_stale_instances_returns_instances_without_matching_node(
            self): # NOQA
        metadata = [
            {
                "name": "instance-1",
                "labels": {
                    context.LABEL_INSTANCE: "node-1",
                }
            },
            {
                "name": "instance-2",
                "labels": {
                    context.LABEL_INSTANCE: "node-2",
                }
            },
            {
                "name": "instance-3",
                "labels": {
                    context.LABEL_INSTANCE: "node-3",
                }
            },
            {
                "name": "instance-4",
                "labels": {
                    context.LABEL_INSTANCE: "node-4",
                }
            },
        ]

        nodes = {
            "node-1": None,
            "node-4": None,
        }

        result = self.sir._find_stale_instances(metadata, nodes)

        self.assertCountEqual(
            result,
            [
                "instance-2",
                "instance-3",
            ]
        )

    def test__find_stale_instances_returns_duplicate_instances(self):
        metadata = [
            {
                "name": "instance-1",
                "labels": {
                    context.LABEL_INSTANCE: "node-1",
                }
            },
            {
                "name": "instance-2",
                "labels": {
                    context.LABEL_INSTANCE: "node-2",
                }
            },
            {
                "name": "instance-3",
                "labels": {
                    context.LABEL_INSTANCE: "node-1",
                }
            },
            {
                "name": "instance-4",
                "labels": {
                    context.LABEL_INSTANCE: "node-4",
                }
            },
        ]

        nodes = {
            "node-1": None,
            "node-4": None,
        }

        result = self.sir._find_stale_instances(metadata, nodes)

        self.assertCountEqual(
            result,
            [
                "instance-2",
                "instance-3",
            ]
        )

    async def test__reconcile_nodes_calls_reconcile_for_each_node(self):
        def node_mock(name: str):
            return kubernetes_asyncio.client.V1Node(
                metadata=kubernetes_asyncio.client.V1ObjectMeta(
                    name=name
                )
            )

        def node_ctx(node_name: str, node_data: str):
            return getattr(unittest.mock.sentinel, "node_ctx_{}".format(
                node_name
            ))

        ctx = unittest.mock.Mock(["with_instance"])
        ctx.with_instance.side_effect = node_ctx

        nodes = {
            "node_1": None,
            "node_2": None,
        }

        with contextlib.ExitStack() as stack:
            wrapped_reconcile = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "reconcile",
                new=unittest.mock.AsyncMock(),
            ))
            wrapped_reconcile.return_value = False

            await self.sir._reconcile_nodes(
                nodes,
                ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        self.assertCountEqual(
            wrapped_reconcile.mock_calls,
            [
                unittest.mock.call(
                    node_ctx("node_1", None),
                    dependencies=unittest.mock.sentinel.deps,
                ),
                unittest.mock.call(
                    node_ctx("node_2", None),
                    dependencies=unittest.mock.sentinel.deps,
                ),
            ]
        )

    async def test__reconcile_nodes_runs_all_even_if_one_fails(self):
        def node_mock(name: str):
            return kubernetes_asyncio.client.V1Node(
                metadata=kubernetes_asyncio.client.V1ObjectMeta(
                    name=name
                )
            )

        def node_ctx(node_name: str, node_data: str):
            return getattr(unittest.mock.sentinel, "node_ctx_{}".format(
                node_name
            ))

        def bool_generator_impl():
            yield True
            while True:
                yield False

        bool_generator = bool_generator_impl()

        def reconcile_side_effect(ctx, *args, **kwargs):
            if "node_2" in str(ctx):
                raise RuntimeError("crash!")
            return next(bool_generator)

        ctx = unittest.mock.Mock(["with_instance", "logger"])
        ctx.with_instance.side_effect = node_ctx

        nodes = {
            "node_1": None,
            "node_2": None,
            "node_3": None,
        }

        with contextlib.ExitStack() as stack:
            wrapped_reconcile = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "reconcile",
                new=unittest.mock.AsyncMock(),
            ))
            wrapped_reconcile.side_effect = reconcile_side_effect

            with self.assertRaises(RuntimeError):
                await self.sir._reconcile_nodes(
                    nodes,
                    ctx,
                    dependencies=unittest.mock.sentinel.deps,
                )

        self.assertCountEqual(
            wrapped_reconcile.mock_calls,
            [
                unittest.mock.call(
                    node_ctx("node_1", None),
                    dependencies=unittest.mock.sentinel.deps,
                ),
                unittest.mock.call(
                    node_ctx("node_2", None),
                    dependencies=unittest.mock.sentinel.deps,
                ),
                unittest.mock.call(
                    node_ctx("node_3", None),
                    dependencies=unittest.mock.sentinel.deps,
                ),
            ]
        )

    async def test__reconcile_nodes_gathers_exceptions(self):
        def node_mock(name: str):
            return kubernetes_asyncio.client.V1Node(
                metadata=kubernetes_asyncio.client.V1ObjectMeta(
                    name=name
                )
            )

        def node_ctx(node_name: str, node_data: str):
            return getattr(unittest.mock.sentinel, "node_ctx_{}".format(
                node_name
            ))

        def bool_generator_impl():
            yield True
            while True:
                yield False

        bool_generator = bool_generator_impl()

        def reconcile_side_effect(ctx, *args, **kwargs):
            if "node_1" in str(ctx):
                raise ValueError("crash!")
            if "node_2" in str(ctx):
                raise LookupError("crash!")
            return next(bool_generator)

        ctx = unittest.mock.Mock(["with_instance", "logger"])
        ctx.with_instance.side_effect = node_ctx

        nodes = {
            "node_1": None,
            "node_2": None,
            "node_3": None,
        }

        with contextlib.ExitStack() as stack:
            wrapped_reconcile = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "reconcile",
                new=unittest.mock.AsyncMock(),
            ))
            wrapped_reconcile.side_effect = reconcile_side_effect

            with self.assertRaisesRegex(
                    RuntimeError,
                    r"node_1 \(ValueError: crash!\), "
                    r"node_2 \(LookupError: crash!\)"):
                await self.sir._reconcile_nodes(
                    nodes,
                    ctx,
                    dependencies=unittest.mock.sentinel.deps,
                )

    async def test__reconcile_rejects_instance(self):
        ctx = unittest.mock.Mock(["instance"])
        ctx.instance = unittest.mock.sentinel.instance

        with self.assertRaisesRegex(RuntimeError,
                                    r"nesting .* not supported yet"):
            await self.sir.reconcile(
                ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

    async def test__reconcile_invokes_wrapped_reconcile(self):
        ctx = unittest.mock.Mock(["instance", "api_client"])
        ctx.instance = None

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "get_target_instances",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_target_instances.return_value = unittest.mock.sentinel.nodes

            get_current_metadata = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_get_current_metadata",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_current_metadata.return_value = unittest.mock.sentinel.metadata

            find_stale_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_find_stale_instances",
                )
            )
            find_stale_instances.return_value = []

            reconcile_nodes = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_reconcile_nodes",
                    new=unittest.mock.AsyncMock(),
                )
            )

            await self.sir.reconcile(
                ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        get_target_instances.assert_called_once_with(ctx)
        get_current_metadata.assert_called_once_with(ctx)
        find_stale_instances.assert_called_once_with(
            unittest.mock.sentinel.metadata,
            unittest.mock.sentinel.nodes,
        )
        reconcile_nodes.assert_called_once_with(
            unittest.mock.sentinel.nodes,
            ctx,
            dependencies=unittest.mock.sentinel.deps,
        )

    async def test__reconcile_orphans_stale_instances(self):
        ctx = unittest.mock.Mock(["instance", "api_client", "namespace"])
        ctx.instance = None

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "get_target_instances",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_target_instances.return_value = unittest.mock.sentinel.nodes

            get_current_metadata = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_get_current_metadata",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_current_metadata.return_value = unittest.mock.sentinel.metadata

            find_stale_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_find_stale_instances",
                )
            )
            find_stale_instances.return_value = [
                "instance-1", "instance-2",
            ]

            reconcile_nodes = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_reconcile_nodes",
                    new=unittest.mock.AsyncMock(),
                )
            )

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_orphan",
            ))

            await self.sir.reconcile(
                ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        get_target_instances.assert_called_once_with(ctx)
        get_current_metadata.assert_called_once_with(ctx)
        find_stale_instances.assert_called_once_with(
            unittest.mock.sentinel.metadata,
            unittest.mock.sentinel.nodes,
        )
        reconcile_nodes.assert_called_once_with(
            unittest.mock.sentinel.nodes,
            ctx,
            dependencies=unittest.mock.sentinel.deps,
        )

        self.assertCountEqual(
            _orphan.await_args_list,
            [
                unittest.mock.call(ctx, ctx.namespace, "instance-1"),
                unittest.mock.call(ctx, ctx.namespace, "instance-2"),
            ]
        )

    async def test__reconcile_always_returns_true_if_instances_were_orphaned(self):  # NOQA
        ctx = unittest.mock.Mock(["instance", "api_client", "namespace"])
        ctx.instance = None

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "get_target_instances",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_target_instances.return_value = unittest.mock.sentinel.nodes

            get_current_metadata = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_get_current_metadata",
                    new=unittest.mock.AsyncMock(),
                )
            )
            get_current_metadata.return_value = unittest.mock.sentinel.metadata

            find_stale_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_find_stale_instances",
                )
            )
            find_stale_instances.return_value = [
                "instance-1", "instance-2",
            ]

            reconcile_nodes = stack.enter_context(
                unittest.mock.patch.object(
                    self.sir, "_reconcile_nodes",
                    new=unittest.mock.AsyncMock(),
                )
            )
            reconcile_nodes.return_value = False

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_orphan",
            ))

            await self.sir.reconcile(
                ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        get_target_instances.assert_awaited_once_with(ctx)
        get_current_metadata.assert_awaited_once_with(ctx)
        find_stale_instances.assert_called_once_with(
            unittest.mock.sentinel.metadata,
            unittest.mock.sentinel.nodes,
        )
        reconcile_nodes.assert_awaited_once_with(
            unittest.mock.sentinel.nodes,
            ctx,
            dependencies=unittest.mock.sentinel.deps,
        )

        self.assertCountEqual(
            _orphan.await_args_list,
            [
                unittest.mock.call(ctx, ctx.namespace, "instance-1"),
                unittest.mock.call(ctx, ctx.namespace, "instance-2"),
            ]
        )


class TestPerNode(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.pn = instancing.PerNode(
            wrapped_state=self.wrapped,
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

    def test_has_per_node_mixin(self):
        self.assertIsInstance(self.pn, instancing.PerNodeMixin)

    def test_is_stateless_instanced_resource(self):
        self.assertIsInstance(
            self.pn,
            instancing.StatelessInstancedResource,
        )


class TestPerSetEntry(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.accessor = unittest.mock.AsyncMock()
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.pse = instancing.PerSetEntry(
            accessor=self.accessor,
            wrapped_state=self.wrapped,
            component=self.component,
        )

    def test_is_stateless_instanced_resource(self):
        self.assertIsInstance(
            self.pse,
            instancing.StatelessInstancedResource,
        )

    async def test_calls_accessor(self):
        ctx = unittest.mock.Mock()
        self.accessor.return_value = {"abc", "def"}

        instances = await self.pse.get_target_instances(ctx)

        self.assertEqual(instances, {"abc": None, "def": None})
        self.accessor.assert_called_once_with(ctx)


class TestPerStatefulSetPod(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.component = str(uuid.uuid4())
        self.statefulset = unittest.mock.Mock(["_get_current"])
        self.statefulset._get_current = unittest.mock.AsyncMock()
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.pssp = instancing.PerStatefulSetPod(
            statefulset=self.statefulset,
            wrapped_state=self.wrapped,
            component=self.component,
        )

    def test_is_stateless_instanced_resource(self):
        self.assertIsInstance(
            self.pssp,
            instancing.StatelessInstancedResource,
        )

    def test_declares_statefulset_as_dependency(self):
        self.assertIn(
            self.statefulset,
            self.pssp._dependencies,
        )

    async def test_creates_entry_per_statefulset_pod(self):
        ctx = unittest.mock.Mock()
        self.statefulset._get_current.return_value = \
            kubernetes_asyncio.client.V1StatefulSet(
                metadata=kubernetes_asyncio.client.V1ObjectMeta(
                    name="foo",
                ),
                spec=kubernetes_asyncio.client.V1StatefulSetSpec(
                    replicas=5,
                    selector=sentinel.selector,
                    service_name=sentinel.service_name,
                    template=sentinel.template,
                ),
            )

        instances = await self.pssp.get_target_instances(ctx)

        self.assertEqual(
            instances,
            {
                "foo-0": None,
                "foo-1": None,
                "foo-2": None,
                "foo-3": None,
                "foo-4": None
            })
        self.statefulset._get_current.assert_called_once_with(ctx)


class TestStatefulInstancedResource(unittest.IsolatedAsyncioTestCase):
    class StatefulPerNodeTest(instancing.StatefulInstancedResource):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        def _get_spec_state(self, *args, **kwargs):
            raise NotImplementedError

        def _get_run_state(self, *args, **kwargs):
            raise NotImplementedError

        async def get_target_instances(self, *args, **kwargs):
            raise NotImplementedError

    def setUp(self):
        def instance_context(instance, instance_data):
            return {"instanced_context": instance,
                    "instance_data": instance_data}

        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.ctx = unittest.mock.Mock(["namespace", "api_client",
                                       "field_manager"])
        self.ctx.logger = logging.getLogger(
            __name__ + "." + type(self).__qualname__,
        )
        self.ctx.with_instance = unittest.mock.Mock([])
        self.ctx.with_instance.side_effect = instance_context
        self.deps = unittest.mock.sentinel.deps
        self.spn = self.StatefulPerNodeTest(
            wrapped_state=self.wrapped,
            component=self.component,
        )

        self.default_instances = {
            "node1": unittest.mock.sentinel.instance_1_data,
            "node2": unittest.mock.sentinel.instance_2_data,
            "node3": unittest.mock.sentinel.instance_3_data,
            "node4": unittest.mock.sentinel.instance_4_data,
        }
        self.get_target_instances_mock = unittest.mock.AsyncMock()
        self.get_target_instances_mock.return_value = self.default_instances

        self.ri_create_mock = unittest.mock.AsyncMock()

        self.get_spec_state_mock = unittest.mock.Mock([])
        self.get_spec_state_mock.side_effect = NotImplementedError()

        self.get_max_unavailable_mock = unittest.mock.Mock([])
        self.get_max_unavailable_mock.return_value = 1

        self.get_run_state_mock = unittest.mock.Mock([])
        self.get_run_state_mock.side_effect = NotImplementedError()

        self.instances = {}
        self.leftovers = []
        self.get_current_instances_mock = unittest.mock.AsyncMock()
        self.get_current_instances_mock.return_value = (
            self.instances, self.leftovers,
        )

        self.__patches = [
            unittest.mock.patch.object(self.spn, "get_target_instances",
                                       new=self.get_target_instances_mock),
            unittest.mock.patch.object(self.spn, "_get_current_instances",
                                       new=self.get_current_instances_mock),
            unittest.mock.patch.object(
                self.wrapped.resource_interface_mock, "create",
                new=self.ri_create_mock),
            unittest.mock.patch.object(self.spn, "_get_spec_state",
                                       new=self.get_spec_state_mock),
            unittest.mock.patch.object(self.spn, "_get_run_state",
                                       new=self.get_run_state_mock),
        ]

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def _default_patches(self):
        for p in self.__patches:
            p.start()

    def _mock_object(self, name, spec=None, body=None):
        if spec is None and body is None:
            body = getattr(unittest.mock.sentinel, f"body_{name}")
        elif spec is not None:
            if body is None:
                body = {"metadata": {"name": name}}
            else:
                body = dict(body)
            body["spec"] = spec
        return instancing.ResourceInfo(
            {"name": name, "namespace": self.ctx.namespace},
            body,
        )

    def test_is_instanced_resource(self):
        self.assertIsInstance(self.spn, instancing.InstancedResource)

    async def test__get_current_instances_extracts_metadata_and_returns_mapping(self):  # NOQA
        def metadata_generator(instance):
            node = instance["index"] % 3
            return {
                "name": instance["key"],
                "labels": {
                    context.LABEL_INSTANCE: f"node{node}",
                },
            }

        self.wrapped.resource_interface_mock.list_.return_value = [
            {"key": unittest.mock.sentinel.resource1, "index": 0},
            {"key": unittest.mock.sentinel.resource2, "index": 1},
            {"key": unittest.mock.sentinel.resource3, "index": 2},
            {"key": unittest.mock.sentinel.resource4, "index": 3},
        ]

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = metadata_generator

            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.spn, "labels",
            ))
            labels_mock.return_value = {"foo": "bar", "baz": "fnord"}

            assignment, leftovers = await self.spn._get_current_instances(
                self.ctx,
            )

        labels_mock.assert_called_once_with(self.ctx)

        self.wrapped.resource_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels={"foo": "bar", "baz": "fnord"},
            ).as_api_selector(),
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource1,
                    "index": 0,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource2,
                    "index": 1,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource3,
                    "index": 2,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource4,
                    "index": 3,
                }),
            ],
        )

        self.assertDictEqual(
            assignment,
            {
                "node0": [
                    instancing.ResourceInfo(
                        metadata_generator(obj),
                        obj,
                    )
                    for obj in [
                        {
                            "key": unittest.mock.sentinel.resource1,
                            "index": 0,
                        },
                        {
                            "key": unittest.mock.sentinel.resource4,
                            "index": 3,
                        },
                    ]
                ],
                "node1": [
                    instancing.ResourceInfo(
                        metadata_generator(obj),
                        obj,
                    )
                    for obj in [
                        {
                            "key": unittest.mock.sentinel.resource2,
                            "index": 1,
                        },
                    ]
                ],
                "node2": [
                    instancing.ResourceInfo(
                        metadata_generator(obj),
                        obj,
                    )
                    for obj in [
                        {
                            "key": unittest.mock.sentinel.resource3,
                            "index": 2,
                        },
                    ]
                ],
            }
        )
        self.assertCountEqual(leftovers, [])

    async def test__get_current_instances_moves_objects_without_instance_label_into_leftovers(self):  # NOQA
        def metadata_generator(instance):
            node = instance["index"]
            if node == 2:
                return {
                    "name": instance["key"],
                    "labels": {},
                }
            if node == 3:
                return {
                    "name": instance["key"],
                }
            return {
                "name": instance["key"],
                "labels": {
                    context.LABEL_INSTANCE: f"node{node % 3}",
                },
            }

        self.wrapped.resource_interface_mock.list_.return_value = [
            {"key": unittest.mock.sentinel.resource1, "index": 0},
            {"key": unittest.mock.sentinel.resource2, "index": 1},
            {"key": unittest.mock.sentinel.resource3, "index": 2},
            {"key": unittest.mock.sentinel.resource4, "index": 3},
        ]

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = metadata_generator

            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.spn, "labels",
            ))
            labels_mock.return_value = {"foo": "bar", "baz": "fnord"}

            assignment, leftovers = await self.spn._get_current_instances(
                self.ctx,
            )

        labels_mock.assert_called_once_with(self.ctx)

        self.wrapped.resource_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels={"foo": "bar", "baz": "fnord"},
            ).as_api_selector(),
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource1,
                    "index": 0,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource2,
                    "index": 1,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource3,
                    "index": 2,
                }),
                unittest.mock.call({
                    "key": unittest.mock.sentinel.resource4,
                    "index": 3,
                }),
            ],
        )

        self.assertDictEqual(
            assignment,
            {
                "node0": [
                    instancing.ResourceInfo(
                        metadata_generator(obj),
                        obj,
                    )
                    for obj in [
                        {
                            "key": unittest.mock.sentinel.resource1,
                            "index": 0,
                        },
                    ]
                ],
                "node1": [
                    instancing.ResourceInfo(
                        metadata_generator(obj),
                        obj,
                    )
                    for obj in [
                        {
                            "key": unittest.mock.sentinel.resource2,
                            "index": 1,
                        },
                    ]
                ],
            }
        )
        self.assertCountEqual(
            leftovers,
            [
                instancing.ResourceInfo(
                    metadata_generator(obj),
                    obj,
                )
                for obj in [
                    {
                        "key": unittest.mock.sentinel.resource3,
                        "index": 2,
                    },
                    {
                        "key": unittest.mock.sentinel.resource4,
                        "index": 3,
                    },
                ]
            ],
        )

    def test__select_winner_queries_states_of_all_resources(self):
        self.get_run_state_mock.side_effect = None
        self.get_run_state_mock.return_value = \
            instancing.ResourceRunState.READY
        self.get_spec_state_mock.side_effect = None
        self.get_spec_state_mock.return_value = \
            instancing.ResourceSpecState.UP_TO_DATE
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                        "deletionTimestamp": datetime(2020, 1, 2, 1, 0, 0),
                    },
                    unittest.mock.sentinel.body1,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body2,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body3,
                ),
            ],
        )

        self.assertCountEqual(
            self.get_run_state_mock.mock_calls,
            [
                unittest.mock.call(self.ctx, unittest.mock.sentinel.body1),
                unittest.mock.call(self.ctx, unittest.mock.sentinel.body2),
                unittest.mock.call(self.ctx, unittest.mock.sentinel.body3),
            ]
        )

        self.assertCountEqual(
            self.get_spec_state_mock.mock_calls,
            [
                unittest.mock.call(
                    self.ctx,
                    unittest.mock.sentinel.intention,
                    unittest.mock.sentinel.body1,
                ),
                unittest.mock.call(
                    self.ctx,
                    unittest.mock.sentinel.intention,
                    unittest.mock.sentinel.body2,
                ),
                unittest.mock.call(
                    self.ctx,
                    unittest.mock.sentinel.intention,
                    unittest.mock.sentinel.body3,
                ),
            ]
        )

    def test__select_winner_returns_none_on_empty_input(self):
        self.get_run_state_mock.side_effect = None
        self.get_run_state_mock.return_value = \
            instancing.ResourceRunState.READY
        self.get_spec_state_mock.side_effect = None
        self.get_spec_state_mock.return_value = \
            instancing.ResourceSpecState.UP_TO_DATE
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [],
        )

        self.assertIsNone(winner)
        self.assertCountEqual(to_delete, [])

    def test__select_winner_returns_None_if_all_require_immediate_deletion(self):  # NOQA
        self.get_run_state_mock.side_effect = None
        self.get_run_state_mock.return_value = \
            instancing.ResourceRunState.DELETION_REQUIRED

        def generate_spec_state():
            for i in itertools.count():
                yield instancing.ResourceSpecState.STALE
                yield instancing.ResourceSpecState.UP_TO_DATE

        self.get_spec_state_mock.side_effect = generate_spec_state()
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                (
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body1,
                ),
                (
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body2,
                ),
            ],
        )

        self.assertIsNone(winner)
        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ]
        )

    def test__select_winner_returns_None_if_all_require_immediate_deletion(self):  # NOQA
        self.get_run_state_mock.side_effect = None
        self.get_run_state_mock.return_value = \
            instancing.ResourceRunState.DELETION_REQUIRED

        def generate_spec_state():
            for i in itertools.count():
                yield instancing.ResourceSpecState.STALE
                yield instancing.ResourceSpecState.UP_TO_DATE

        self.get_spec_state_mock.side_effect = generate_spec_state()
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body1,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                    },
                    unittest.mock.sentinel.body2,
                ),
            ],
        )

        self.assertIsNone(winner)
        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ]
        )

    def test__select_winner_does_not_return_deleted_as_to_delete_or_winner(self):  # NOQA
        def generate_run_state():
            for i in range(2):
                yield instancing.ResourceRunState.READY
            for i in range(2):
                yield instancing.ResourceRunState.DELETION_REQUIRED

        self.get_run_state_mock.side_effect = generate_run_state()

        def generate_spec_state():
            for i in itertools.count():
                yield instancing.ResourceSpecState.STALE
                yield instancing.ResourceSpecState.UP_TO_DATE

        self.get_spec_state_mock.side_effect = generate_spec_state()
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": unittest.mock.sentinel.foo,
                    },
                    unittest.mock.sentinel.body1,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": unittest.mock.sentinel.foo,
                    },
                    unittest.mock.sentinel.body2,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": unittest.mock.sentinel.foo,
                    },
                    unittest.mock.sentinel.body3,
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name4,
                        "namespace": unittest.mock.sentinel.namespace4,
                        "deletionTimestamp": unittest.mock.sentinel.foo,
                    },
                    unittest.mock.sentinel.body4,
                ),
            ],
        )

        self.assertIsNone(winner)
        self.assertCountEqual(
            to_delete,
            [],
        )

    def test__select_winner_returns_oldest_stale_but_ready_resource_if_no_up_to_date(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name3,
                    namespace=unittest.mock.sentinel.namespace3,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.READY)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name2,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace2,
        )

    def test__select_winner_returns_prefers_ready_stale_over_unknown_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.UNKNOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.READY)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_ready_stale_over_shutting_down_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.SHUTTING_DOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.READY)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_handles_missing_deletion_timestamp(self):
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.READY)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_ready_stale_over_starting_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.STARTING,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.READY)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_starting_stale_over_unknown_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                (
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.UNKNOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                (
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.UNKNOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                (
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.STARTING,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.STARTING)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_starting_stale_over_unknown_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.SHUTTING_DOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.SHUTTING_DOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.STARTING,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.STARTING)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_unknown_stale_over_shutting_down_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.SHUTTING_DOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.SHUTTING_DOWN,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.UNKNOWN,
                     instancing.ResourceSpecState.STALE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.UNKNOWN)
        self.assertEqual(spec_state, instancing.ResourceSpecState.STALE)
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    def test__select_winner_returns_prefers_unknown_up_to_date_over_ready_stale(self):  # NOQA
        def generate_run_state(_, instance):
            return instance[1]

        self.get_run_state_mock.side_effect = generate_run_state

        def generate_spec_state(ctx, intention, instance):
            return instance[2]

        self.get_spec_state_mock.side_effect = generate_spec_state
        self._default_patches()

        winner, to_delete = self.spn._select_winner(
            self.ctx,
            unittest.mock.sentinel.intention,
            [
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name1,
                        "namespace": unittest.mock.sentinel.namespace1,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 2, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name2,
                        "namespace": unittest.mock.sentinel.namespace2,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 0, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.READY,
                     instancing.ResourceSpecState.STALE),
                ),
                instancing.ResourceInfo(
                    {
                        "name": unittest.mock.sentinel.name3,
                        "namespace": unittest.mock.sentinel.namespace3,
                        "deletionTimestamp": None,
                        "creationTimestamp": datetime(2020, 1, 1, 1, 0, 0),
                    },
                    (unittest.mock.sentinel.res2,
                     instancing.ResourceRunState.UNKNOWN,
                     instancing.ResourceSpecState.UP_TO_DATE),
                ),
            ],
        )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )
        self.assertIsNotNone(winner)
        winner_ref, run_state, spec_state = winner
        self.assertEqual(run_state, instancing.ResourceRunState.UNKNOWN)
        self.assertEqual(
            spec_state,
            instancing.ResourceSpecState.UP_TO_DATE,
        )
        self.assertEqual(
            winner_ref.name,
            unittest.mock.sentinel.name3,
        )
        self.assertEqual(
            winner_ref.namespace,
            unittest.mock.sentinel.namespace3,
        )

    async def test__compile_full_state_returns_info_even_for_empty_targets(self):  # NOQA
        def generate_body(ctx, deps):
            return {"for": ctx["instanced_context"]}

        with contextlib.ExitStack() as stack:
            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.return_value = (None, [])

            make_body = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_make_complete_body",
            ))
            make_body.side_effect = generate_body

            state, to_delete = await self.spn._compile_full_state(
                self.ctx,
                self.deps,
                {"foo": None, "bar": None},
                {},
            )

        self.assertCountEqual(
            make_body.await_args_list,
            [
                unittest.mock.call(
                    self.ctx.with_instance("foo", None),
                    self.deps,
                ),
                unittest.mock.call(
                    self.ctx.with_instance("bar", None),
                    self.deps,
                ),
            ]
        )

        self.assertDictEqual(
            state,
            {
                "foo": instancing.InstanceState(
                    winner=None,
                    intended_body={"for": "foo"},
                    instance_data=None,
                    has_any_resources=False,
                    may_create_resources=True,
                ),
                "bar": instancing.InstanceState(
                    winner=None,
                    intended_body={"for": "bar"},
                    instance_data=None,
                    has_any_resources=False,
                    may_create_resources=True,
                ),
            },
        )
        self.assertCountEqual(to_delete, [])

    async def test__compile_full_state_marks_resources_on_wrong_instances_for_deletion(self):  # NOQA
        with contextlib.ExitStack() as stack:
            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.return_value = (None, [])

            stack.enter_context(unittest.mock.patch.object(
                self.spn, "_make_complete_body",
            ))

            state, to_delete = await self.spn._compile_full_state(
                self.ctx,
                self.deps,
                {"foo": None, "bar": None},
                {
                    "baz": [
                        instancing.ResourceInfo(
                            {
                                "name": unittest.mock.sentinel.name1,
                                "namespace": unittest.mock.sentinel.namespace1,
                            },
                            unittest.mock.sentinel.body1,
                        ),
                        instancing.ResourceInfo(
                            {
                                "name": unittest.mock.sentinel.name2,
                                "namespace": unittest.mock.sentinel.namespace2,
                            },
                            unittest.mock.sentinel.body2,
                        ),
                    ]
                },
            )

        self.assertCountEqual(
            to_delete,
            [
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name1,
                    namespace=unittest.mock.sentinel.namespace1,
                ),
                kubernetes_asyncio.client.V1ObjectReference(
                    name=unittest.mock.sentinel.name2,
                    namespace=unittest.mock.sentinel.namespace2,
                ),
            ],
        )

    async def test__compile_full_state_extracts_node_info_from_assignments(self):  # NOQA
        def generate_body(ctx, deps):
            return {"for": ctx["instanced_context"]}

        def generate_winner(ctx, *_):
            return (
                {"winner_for": ctx["instanced_context"]},
                [],
            )

        with contextlib.ExitStack() as stack:
            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.side_effect = generate_winner

            make_body = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_make_complete_body",
            ))
            make_body.side_effect = generate_body

            state, to_delete = await self.spn._compile_full_state(
                self.ctx,
                self.deps,
                {"foo": None, "bar": None, "baz": None},
                {
                    "foo": [
                        unittest.mock.sentinel.info1,
                        unittest.mock.sentinel.info2,
                        unittest.mock.sentinel.info3,
                    ],
                    "bar": [
                        unittest.mock.sentinel.info4,
                        unittest.mock.sentinel.info5,
                    ],
                },
            )

        self.assertCountEqual(
            make_body.await_args_list,
            [
                unittest.mock.call(
                    {"instanced_context": "foo", "instance_data": None},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "bar", "instance_data": None},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "baz", "instance_data": None},
                    self.deps,
                ),
            ]
        )

        self.assertCountEqual(
            select_winner.mock_calls,
            [
                unittest.mock.call(
                    {"instanced_context": "foo", "instance_data": None},
                    {"for": "foo"},
                    [
                        unittest.mock.sentinel.info1,
                        unittest.mock.sentinel.info2,
                        unittest.mock.sentinel.info3,
                    ],
                ),
                unittest.mock.call(
                    {"instanced_context": "bar", "instance_data": None},
                    {"for": "bar"},
                    [
                        unittest.mock.sentinel.info4,
                        unittest.mock.sentinel.info5,
                    ],
                ),
                unittest.mock.call(
                    {"instanced_context": "baz", "instance_data": None},
                    {"for": "baz"},
                    [],
                ),
            ]
        )

        self.assertDictEqual(
            state,
            {
                "foo": instancing.InstanceState(
                    winner={"winner_for": "foo"},
                    intended_body={"for": "foo"},
                    instance_data=None,
                    has_any_resources=True,
                    may_create_resources=True,
                ),
                "bar": instancing.InstanceState(
                    winner={"winner_for": "bar"},
                    intended_body={"for": "bar"},
                    instance_data=None,
                    has_any_resources=True,
                    may_create_resources=True,
                ),
                "baz": instancing.InstanceState(
                    winner={"winner_for": "baz"},
                    intended_body={"for": "baz"},
                    instance_data=None,
                    has_any_resources=False,
                    may_create_resources=True,
                ),
            },
        )

    async def test__compile_full_state_adds_to_to_delete(self):
        def generate_body(ctx, deps):
            return {"for": ctx["instanced_context"]}

        def generate_winner(ctx, *_):
            return (
                {"winner_for": ctx["instanced_context"]},
                [
                    {"from": ctx["instanced_context"], "index": 0},
                    {"from": ctx["instanced_context"], "index": 1},
                ],
            )

        with contextlib.ExitStack() as stack:
            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.side_effect = generate_winner

            make_body = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_make_complete_body",
            ))
            make_body.side_effect = generate_body

            state, to_delete = await self.spn._compile_full_state(
                self.ctx,
                self.deps,
                {"foo": None, "bar": None, "baz": None},
                {
                    "foo": [
                        unittest.mock.sentinel.info1,
                        unittest.mock.sentinel.info2,
                        unittest.mock.sentinel.info3,
                    ],
                    "bar": [
                        unittest.mock.sentinel.info4,
                        unittest.mock.sentinel.info5,
                    ],
                },
            )

        self.assertCountEqual(
            make_body.await_args_list,
            [
                unittest.mock.call(
                    {"instanced_context": "foo", "instance_data": None},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "bar", "instance_data": None},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "baz", "instance_data": None},
                    self.deps,
                ),
            ]
        )

        self.assertCountEqual(
            select_winner.mock_calls,
            [
                unittest.mock.call(
                    {"instanced_context": "foo", "instance_data": None},
                    {"for": "foo"},
                    [
                        unittest.mock.sentinel.info1,
                        unittest.mock.sentinel.info2,
                        unittest.mock.sentinel.info3,
                    ],
                ),
                unittest.mock.call(
                    {"instanced_context": "bar", "instance_data": None},
                    {"for": "bar"},
                    [
                        unittest.mock.sentinel.info4,
                        unittest.mock.sentinel.info5,
                    ],
                ),
                unittest.mock.call(
                    {"instanced_context": "baz", "instance_data": None},
                    {"for": "baz"},
                    [],
                ),
            ]
        )

        self.assertCountEqual(
            to_delete,
            [
                {"from": "foo", "index": 0},
                {"from": "foo", "index": 1},
                {"from": "bar", "index": 0},
                {"from": "bar", "index": 1},
                {"from": "baz", "index": 0},
                {"from": "baz", "index": 1},
            ],
        )

    async def test_reconcile_creates_object_if_none_exists(self):  # NOQA
        self.get_target_instances_mock.return_value = {
            "node1": unittest.mock.sentinel.node1_data,
        }
        self._default_patches()

        body = unittest.mock.sentinel.body
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = body

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.get_current_instances_mock.assert_awaited_once_with(self.ctx)
        self.get_target_instances_mock.assert_awaited_once_with(self.ctx)
        make_body.assert_awaited_once_with(
            {"instanced_context": "node1",
             "instance_data": sentinel.node1_data},
            self.deps,
        )
        adopt_object.assert_awaited_once_with(
            {"instanced_context": "node1",
             "instance_data": sentinel.node1_data},
            body,
        )
        self.ri_create_mock.assert_awaited_once_with(
            self.ctx.namespace,
            body,
            field_manager=self.ctx.field_manager,
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=1,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=1,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_creates_multiple_missing_objects(self):  # NOQA
        self._default_patches()

        def body_generator(instanced_ctx, _):
            return {"body_for": instanced_ctx["instanced_context"]}

        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.side_effect = body_generator

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.get_current_instances_mock.assert_awaited_once_with(self.ctx)
        self.get_target_instances_mock.assert_awaited_once_with(self.ctx)
        self.assertCountEqual(
            make_body.await_args_list,
            [
                unittest.mock.call(
                    {"instanced_context": "node1",
                     "instance_data": sentinel.instance_1_data},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "node2",
                     "instance_data": sentinel.instance_2_data},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "node3",
                     "instance_data": sentinel.instance_3_data},
                    self.deps,
                ),
                unittest.mock.call(
                    {"instanced_context": "node4",
                     "instance_data": sentinel.instance_4_data},
                    self.deps,
                ),
            ],
        )
        self.assertCountEqual(
            adopt_object.await_args_list,
            [
                unittest.mock.call(
                    {"instanced_context": "node1",
                     "instance_data": sentinel.instance_1_data},
                    {"body_for": "node1"},
                ),
                unittest.mock.call(
                    {"instanced_context": "node2",
                     "instance_data": sentinel.instance_2_data},
                    {"body_for": "node2"},
                ),
                unittest.mock.call(
                    {"instanced_context": "node3",
                     "instance_data": sentinel.instance_3_data},
                    {"body_for": "node3"},
                ),
                unittest.mock.call(
                    {"instanced_context": "node4",
                     "instance_data": sentinel.instance_4_data},
                    {"body_for": "node4"},
                ),
            ],
        )
        self.assertCountEqual(
            self.ri_create_mock.await_args_list,
            [
                unittest.mock.call(
                    self.ctx.namespace,
                    {"body_for": "node1"},
                    field_manager=self.ctx.field_manager,
                ),
                unittest.mock.call(
                    self.ctx.namespace,
                    {"body_for": "node2"},
                    field_manager=self.ctx.field_manager,
                ),
                unittest.mock.call(
                    self.ctx.namespace,
                    {"body_for": "node3"},
                    field_manager=self.ctx.field_manager,
                ),
                unittest.mock.call(
                    self.ctx.namespace,
                    {"body_for": "node4"},
                    field_manager=self.ctx.field_manager,
                ),
            ]
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=4,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=4,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_creates_no_object_if_any_resource_exists(self):  # NOQA
        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
            ]

            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                {"node1": [unittest.mock.sentinel.body1]},
                [],
            )

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                {
                    "node1": instancing.InstanceState(
                        winner=None,
                        intended_body=unittest.mock.sentinel.body1,
                        instance_data=None,
                        has_any_resources=True,
                        may_create_resources=True,
                    ),
                },
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        get_current_instances.assert_awaited_once_with(self.ctx)
        get_target_instances.assert_awaited_once_with(self.ctx)
        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=1,
                    nexisting_instances=1,
                    nexisting_resources=1,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_creates_no_object_if_forbidden(self):  # NOQA
        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
            ]

            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                {},
                [],
            )

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                {
                    "node1": instancing.InstanceState(
                        winner=None,
                        intended_body=unittest.mock.sentinel.body1,
                        instance_data=None,
                        has_any_resources=False,
                        may_create_resources=False,
                    ),
                },
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        get_current_instances.assert_awaited_once_with(self.ctx)
        get_target_instances.assert_awaited_once_with(self.ctx)
        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=1,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_deletes_leftovers(self):
        self.get_target_instances_mock.return_value = {}
        self.leftovers[:] = [self._mock_object("instance1"),
                             self._mock_object("instance2")]
        self._default_patches()

        body = unittest.mock.sentinel.body
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = body

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(self.ctx.namespace, "instance1"),
                unittest.mock.call(self.ctx.namespace, "instance2"),
            ],
        )

        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=0,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_deletes_objects_with_no_matching_node(self):
        self.get_target_instances_mock.return_value = {}
        self.instances.update({
            "node2": [self._mock_object("instance1")],
            "node3": [self._mock_object("instance3")],
        })
        self._default_patches()

        body = unittest.mock.sentinel.body
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = body

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(self.ctx.namespace, "instance1"),
                unittest.mock.call(self.ctx.namespace, "instance3"),
            ],
        )

        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=0,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_selects_winner_on_matching_nodes_and_deletes_others(self):  # NOQA
        obj1 = self._mock_object("instance1")
        obj2 = self._mock_object("instance2")

        self.get_target_instances_mock.return_value = {
            "node1": unittest.mock.sentinel.node1_data,
        }
        self.instances.update({
            "node1": [obj1, obj2],
        })
        self._default_patches()

        body = unittest.mock.sentinel.body
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = body

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.return_value = (
                instancing.WinnerInfo(
                    kubernetes_asyncio.client.V1ObjectReference(
                        namespace=unittest.mock.sentinel.namespace1,
                        name=unittest.mock.sentinel.name1,
                    ),
                    instancing.ResourceRunState.READY,
                    instancing.ResourceSpecState.UP_TO_DATE,
                ),
                [
                    kubernetes_asyncio.client.V1ObjectReference(
                        namespace=unittest.mock.sentinel.namespace2,
                        name=unittest.mock.sentinel.name2,
                    ),
                ],
            )

            await self.spn.reconcile(self.ctx, self.deps)

        make_body.assert_awaited_once_with(
            {"instanced_context": "node1",
             'instance_data': sentinel.node1_data},
            self.deps,
        )
        adopt_object.assert_awaited_once_with(
            {"instanced_context": "node1",
             'instance_data': sentinel.node1_data},
            body,
        )

        select_winner.assert_called_once_with(
            {"instanced_context": "node1",
             'instance_data': sentinel.node1_data},
            body,
            [obj1, obj2],
        )

        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(unittest.mock.sentinel.namespace2,
                                   unittest.mock.sentinel.name2),
            ],
        )

        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=1,
                    nexisting_instances=1,
                    nexisting_resources=2,
                    nupdated=1,
                    nready=1,
                    navailable=1,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_does_not_create_new_resource_even_if_no_winner_elected(self):  # NOQA
        obj1 = self._mock_object("instance1")
        obj2 = self._mock_object("instance2")

        self.get_target_instances_mock.return_value = {
            "node1": unittest.mock.sentinel.node1_data,
        }
        self.instances.update({
            "node1": [obj1, obj2],
        })
        self._default_patches()

        body = unittest.mock.sentinel.body
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = body

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_select_winner",
            ))
            select_winner.return_value = (
                None,
                [
                    kubernetes_asyncio.client.V1ObjectReference(
                        namespace=unittest.mock.sentinel.namespace2,
                        name=unittest.mock.sentinel.name2,
                    ),
                ],
            )

            await self.spn.reconcile(self.ctx, self.deps)

        make_body.assert_awaited_once_with(
            {"instanced_context": "node1",
             "instance_data": sentinel.node1_data},
            self.deps,
        )
        adopt_object.assert_awaited_once_with(
            {"instanced_context": "node1",
             "instance_data": sentinel.node1_data},
            body,
        )

        select_winner.assert_called_once_with(
            {"instanced_context": "node1",
             "instance_data": sentinel.node1_data},
            body,
            [obj1, obj2],
        )

        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(unittest.mock.sentinel.namespace2,
                                   unittest.mock.sentinel.name2),
            ],
        )

        self.ri_create_mock.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=1,
                    nexisting_instances=1,
                    nexisting_resources=2,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_deletes_up_to_N_stale_resources_if_all_are_ready_1(self):  # NOQA
        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                {
                    "node1": instancing.InstanceState(
                        winner=instancing.WinnerInfo(
                            ref=kubernetes_asyncio.client.V1ObjectReference(
                                name=unittest.mock.sentinel.name1,
                                namespace=unittest.mock.sentinel.namespace1,
                            ),
                            run_state=instancing.ResourceRunState.READY,
                            spec_state=instancing.ResourceSpecState.STALE,
                        ),
                        intended_body=unittest.mock.sentinel.body1,
                        instance_data=None,
                        has_any_resources=True,
                        may_create_resources=True,
                    ),
                    "node2": instancing.InstanceState(
                        winner=instancing.WinnerInfo(
                            ref=kubernetes_asyncio.client.V1ObjectReference(
                                name=unittest.mock.sentinel.name2,
                                namespace=unittest.mock.sentinel.namespace2,
                            ),
                            run_state=instancing.ResourceRunState.READY,
                            spec_state=instancing.ResourceSpecState.STALE,
                        ),
                        intended_body=unittest.mock.sentinel.body2,
                        instance_data=None,
                        has_any_resources=True,
                        may_create_resources=True,
                    ),
                    "node3": instancing.InstanceState(
                        winner=instancing.WinnerInfo(
                            ref=kubernetes_asyncio.client.V1ObjectReference(
                                name=unittest.mock.sentinel.name3,
                                namespace=unittest.mock.sentinel.namespace3,
                            ),
                            run_state=instancing.ResourceRunState.READY,
                            spec_state=instancing.ResourceSpecState.STALE,
                        ),
                        intended_body=unittest.mock.sentinel.body3,
                        instance_data=None,
                        has_any_resources=True,
                        may_create_resources=True,
                    ),
                },
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()

        self.assertEqual(
            len(self.wrapped.resource_interface_mock.delete.await_args_list),
            1,
        )
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace1,
                    unittest.mock.sentinel.name1,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "1",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=0,
                    nready=2,
                    navailable=2,
                    ncreated=0,
                    ndeleted=1,
                ),
            )],
        )

    async def test_reconcile_deletes_up_to_N_stale_resources_if_all_are_ready_2(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "2", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()

        self.assertEqual(
            len(self.wrapped.resource_interface_mock.delete.await_args_list),
            2,
        )
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace1,
                    unittest.mock.sentinel.name1,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.namespace2,
                    unittest.mock.sentinel.name2,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "2",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=0,
                    nready=1,
                    navailable=1,
                    ncreated=0,
                    ndeleted=2,
                ),
            )],
        )

    async def test_reconcile_unknown_resources_block_upgrading_stale(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "2", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()
        self.wrapped.resource_interface_mock.delete.assert_not_awaited()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "2",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=2,
                    nready=1,
                    navailable=1,
                    ncreated=0,
                    ndeleted=0,
                ),
            )],
        )

    async def test_reconcile_updates_unknown_stale_resources(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "4", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()
        self.wrapped.resource_interface_mock.delete.assert_awaited_once_with(
            unittest.mock.sentinel.namespace2,
            unittest.mock.sentinel.name2,
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "4",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=2,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=1,
                ),
            )],
        )

    async def test_reconcile_updates_unknown_stale_resources_do_not_delete(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "2", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()
        self.wrapped.resource_interface_mock.delete.assert_not_called()

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "2",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=2,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                ),
            )]
        )

    async def test_deleted_stale_unknown_do_not_miscount(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.UNKNOWN,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "2", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace1,
                    unittest.mock.sentinel.name1,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.namespace2,
                    unittest.mock.sentinel.name2,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "2",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=0,
                    nready=1,
                    navailable=1,
                    ncreated=0,
                    ndeleted=2,
                ),
            )],
        )

    async def test_reconcile_does_not_delete_up_to_date_and_ready_resources(self):  # NOQA
        fake_instances = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.
                    UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(None, "2", None), fake_instances)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()

        self.assertEqual(
            len(self.wrapped.resource_interface_mock.delete.await_args_list),
            2,
        )
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace2,
                    unittest.mock.sentinel.name2,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.namespace3,
                    unittest.mock.sentinel.name3,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [instancing.InstanceGroup(
                None, "2",
                instancing.InstanceStatistics(
                    nconfigured=3,
                    nexisting_instances=3,
                    nexisting_resources=3,
                    nupdated=1,
                    nready=1,
                    navailable=1,
                    ncreated=0,
                    ndeleted=2,
                ),
            )],
        )

    async def test_reconcile_multi_group_1(self):  # NOQA
        fake_instances1 = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }
        fake_instances2 = {
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node4": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name4,
                        namespace=unittest.mock.sentinel.namespace4,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node5": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name5,
                        namespace=unittest.mock.sentinel.namespace5,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }
        fake_instances = {**fake_instances1, **fake_instances2}

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
                "node4",
                "node5",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group1"),
                    "1", None), fake_instances1),
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group2"),
                    "2", None), fake_instances2)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3", "node4", "node5"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()

        self.assertEqual(
            len(self.wrapped.resource_interface_mock.delete.await_args_list),
            3,
        )
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace1,
                    unittest.mock.sentinel.name1,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.namespace3,
                    unittest.mock.sentinel.name3,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.namespace4,
                    unittest.mock.sentinel.name4,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [
                instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group1"),
                    "1",
                    instancing.InstanceStatistics(
                        nconfigured=2,
                        nexisting_instances=2,
                        nexisting_resources=2,
                        nupdated=0,
                        nready=1,
                        navailable=1,
                        ncreated=0,
                        ndeleted=1,
                    )),
                instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group2"),
                    "2",
                    instancing.InstanceStatistics(
                        nconfigured=3,
                        nexisting_instances=3,
                        nexisting_resources=3,
                        nupdated=0,
                        nready=1,
                        navailable=1,
                        ncreated=0,
                        ndeleted=2,
                    )),
            ]
        )

    async def test_reconcile_multi_group_2(self):  # NOQA
        fake_instances1 = {
            "node1": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name1,
                        namespace=unittest.mock.sentinel.namespace1,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body1,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node2": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name2,
                        namespace=unittest.mock.sentinel.namespace2,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body2,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }
        fake_instances2 = {
            "node3": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name3,
                        namespace=unittest.mock.sentinel.namespace3,
                    ),
                    run_state=instancing.ResourceRunState.STARTING,
                    spec_state=instancing.ResourceSpecState.UP_TO_DATE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node4": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name4,
                        namespace=unittest.mock.sentinel.namespace4,
                    ),
                    run_state=instancing.ResourceRunState.SHUTTING_DOWN,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
            "node5": instancing.InstanceState(
                winner=instancing.WinnerInfo(
                    ref=kubernetes_asyncio.client.V1ObjectReference(
                        name=unittest.mock.sentinel.name5,
                        namespace=unittest.mock.sentinel.namespace5,
                    ),
                    run_state=instancing.ResourceRunState.READY,
                    spec_state=instancing.ResourceSpecState.STALE,
                ),
                intended_body=unittest.mock.sentinel.body3,
                instance_data=None,
                has_any_resources=True,
                may_create_resources=True,
            ),
        }
        fake_instances = {**fake_instances1, **fake_instances2}

        with contextlib.ExitStack() as stack:
            get_target_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "get_target_instances",
                )
            )
            get_target_instances.return_value = [
                "node1",
                "node2",
                "node3",
                "node4",
                "node5",
            ]

            current_instances = {
                x: [None] for x in get_target_instances.return_value}
            get_current_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_get_current_instances",
                )
            )
            get_current_instances.return_value = (
                current_instances,
                [],
            )

            _group_instances = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_group_instances",
                )
            )
            _group_instances.return_value = [
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group1"),
                    "1", None), fake_instances1),
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group2"),
                    "2", None), fake_instances2)
            ]

            compile_full_state = stack.enter_context(
                unittest.mock.patch.object(
                    self.spn, "_compile_full_state",
                )
            )
            compile_full_state.return_value = (
                fake_instances,
                [],
            )

            update_stats = stack.enter_context(unittest.mock.patch.object(
                self.spn, "_update_stats",
            ))

            await self.spn.reconcile(self.ctx, self.deps)

        self.assertIn(
            unittest.mock.call(self.ctx, fake_instances),
            _group_instances.mock_calls,
        )
        get_target_instances.assert_awaited_once_with(self.ctx)
        get_current_instances.assert_awaited_once_with(self.ctx)
        compile_full_state.assert_awaited_once_with(
            self.ctx,
            self.deps,
            ["node1", "node2", "node3", "node4", "node5"],
            current_instances,
        )

        self.ri_create_mock.assert_not_called()
        self.wrapped.resource_interface_mock.create.assert_not_called()
        self.wrapped.resource_interface_mock.patch.assert_not_called()

        self.assertEqual(
            len(self.wrapped.resource_interface_mock.delete.await_args_list),
            1,
        )
        self.assertCountEqual(
            self.wrapped.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.namespace2,
                    unittest.mock.sentinel.name2,
                ),
            ],
        )

        update_stats.assert_awaited_once_with(
            self.ctx,
            [
                instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group1"),
                    "1",
                    instancing.InstanceStatistics(
                        nconfigured=2,
                        nexisting_instances=2,
                        nexisting_resources=2,
                        nupdated=1,
                        nready=1,
                        navailable=1,
                        ncreated=0,
                        ndeleted=1,
                    )),
                instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(name="group2"),
                    "2",
                    instancing.InstanceStatistics(
                        nconfigured=3,
                        nexisting_instances=3,
                        nexisting_resources=3,
                        nupdated=1,
                        nready=1,
                        navailable=2,
                        ncreated=0,
                        ndeleted=0,
                    )),
            ]
        )

    async def test__make_complete_body_adopts_object_too(self):
        with contextlib.ExitStack() as stack:
            make_body = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_make_body",
            ))
            make_body.return_value = unittest.mock.sentinel.body

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "adopt_object",
            ))

            result = await self.spn._make_complete_body(
                self.ctx,
                self.deps,
            )

        make_body.assert_awaited_once_with(self.ctx, self.deps)
        adopt_object.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.body,
        )
        self.assertEqual(result, unittest.mock.sentinel.body)


class TestStatefulPerNode(unittest.IsolatedAsyncioTestCase):
    class StatefulPerNodeTest(instancing.StatefulPerNode):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        def _get_spec_state(self, *args, **kwargs):
            raise NotImplementedError

        def _get_run_state(self, *args, **kwargs):
            raise NotImplementedError

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.wrapped = SingleObjectMock(
            component=self.component,
        )
        self.spn = self.StatefulPerNodeTest(
            wrapped_state=self.wrapped,
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )
        self.ctx = unittest.mock.Mock(["namespace", "api_client",
                                       "field_manager", "logger",
                                       "parent_kind"])
        self.ctx.parent_kind = str(sentinel.parent_kind)

        self.fake_yaook_disruption_budgets = []
        self.ydb_interface_mock = unittest.mock.Mock()
        self.ydb_interface = unittest.mock.AsyncMock()
        self.ydb_interface_mock.return_value = self.ydb_interface
        self.ydb_interface.list_.return_value = \
            self.fake_yaook_disruption_budgets
        self.ydb_interface.api_version = "yaook.cloud/v1"

        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.interfaces.yaookdisruptionbudget_interface",  # noqa: E501
                new=self.ydb_interface_mock),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def test_is_stateful_instanced_resource(self):
        self.assertIsInstance(self.spn,
                              instancing.StatefulInstancedResource)

    def test_has_per_node_mixin(self):
        self.assertIsInstance(self.spn,
                              instancing.PerNodeMixin)

    async def test__group_instances_without_disruption_budget(self):
        instances = {
            "node1": sentinel.instance_data1,
            "node2": sentinel.instance_data2,
            "node3": sentinel.instance_data3,
        }
        grouped_instances = await self.spn._group_instances(self.ctx,
                                                            instances)

        self.assertEquals(
            grouped_instances,
            [
                (instancing.InstanceGroup(None, "1", None), instances)
            ]
        )

    async def test__group_instances_with_single_disruption_budget(self):
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.ydb_name1,
                    "namespace": sentinel.ydb_namespace1,
                },
                "spec": {
                    "maxUnavailable": "10%",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "somekey": "somevalue"
                        }
                    }]
                }
            }
        )
        instances1 = {
            "node1": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"somekey": "somevalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
            "node2": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"somekey": "somevalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances2 = {
            "node3": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"someotherkey": "someothervalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances = {**instances1, **instances2}
        grouped_instances = await self.spn._group_instances(self.ctx,
                                                            instances)

        self.assertEquals(
            grouped_instances,
            [
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(
                        name=sentinel.ydb_name1,
                        namespace=sentinel.ydb_namespace1,
                    ), "10%", None), instances1),
                (instancing.InstanceGroup(None, "1", None), instances2),
            ]
        )

    async def test__group_instances_with_two_disruption_budgets(self):
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.ydb_name1,
                    "namespace": sentinel.ydb_namespace1,
                },
                "spec": {
                    "maxUnavailable": "10%",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "somekey": "somevalue"
                        }
                    }]
                }
            }
        )
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.ydb_name2,
                    "namespace": sentinel.ydb_namespace2,
                },
                "spec": {
                    "maxUnavailable": "3",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "someotherkey": "someothervalue"
                        }
                    }]
                }
            }
        )
        instances1 = {
            "node1": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"somekey": "somevalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
            "node2": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"somekey": "somevalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances2 = {
            "node3": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"someotherkey": "someothervalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances = {**instances1, **instances2}
        grouped_instances = await self.spn._group_instances(self.ctx,
                                                            instances)

        self.assertEquals(
            grouped_instances,
            [
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(
                        name=sentinel.ydb_name1,
                        namespace=sentinel.ydb_namespace1,
                    ), "10%", None), instances1),
                (instancing.InstanceGroup(
                    kubernetes_asyncio.client.V1ObjectReference(
                        name=sentinel.ydb_name2,
                        namespace=sentinel.ydb_namespace2,
                    ), "3", None), instances2),
                (instancing.InstanceGroup(None, "1", None), {}),
            ]
        )

    async def test__group_instances_raises_on_overlap(self):
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.ydb_name1,
                    "namespace": sentinel.ydb_namespace1,
                },
                "spec": {
                    "maxUnavailable": "10%",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "somekey": "somevalue"
                        }
                    }]
                }
            }
        )
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.ydb_name2,
                    "namespace": sentinel.ydb_namespace2,
                },
                "spec": {
                    "maxUnavailable": "3",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "someotherkey": "someothervalue"
                        }
                    }]
                }
            }
        )
        instances1 = {
            "node1": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"somekey": "somevalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
            "node2": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={
                    "somekey": "somevalue",
                    "someotherkey": "someothervalue"
                },
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances2 = {
            "node3": instancing.InstanceState(
                winner=None,
                intended_body=None,
                instance_data={"someotherkey": "someothervalue"},
                has_any_resources=None,
                may_create_resources=None,
            ),
        }
        instances = {**instances1, **instances2}
        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.spn._group_instances(self.ctx, instances)

    @unittest.mock.patch("yaook.statemachine.api_utils.format_timestamp")
    async def test__update_stats_writes_back_to_disruption_budget(self, format_timestamp):  # noqa: E501
        format_timestamp.return_value = "today_is_the_day"
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.group1_name,
                    "namespace": sentinel.group1_namespace,
                },
                "spec": {
                    "maxUnavailable": "10%",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "somekey": "somevalue"
                        }
                    }]
                }
            }
        )

        await self.spn._update_stats(self.ctx, [
            instancing.InstanceGroup(
                kubernetes_asyncio.client.V1ObjectReference(
                    name=sentinel.group1_name,
                    namespace=sentinel.group1_namespace,
                ),
                "10%",
                instancing.InstanceStatistics(
                    nconfigured=10,
                    nexisting_instances=11,
                    nexisting_resources=12,
                    nupdated=13,
                    nready=14,
                    navailable=15,
                    ncreated=16,
                    ndeleted=17,
                )
            )
        ])

        self.ydb_interface.patch_status.assert_called_once_with(
            sentinel.group1_namespace,
            sentinel.group1_name,
            k8s._encode_body({
                "apiVersion": "yaook.cloud/v1",
                "kind": "YaookDisruptionBudget",
                "status": {
                    "nodes": [{
                        "type": "sentinel.parent_kind-%s" % self.component,
                        "lastUpdateTime": "today_is_the_day",
                        "configuredInstances": 10,
                        "existingInstances": 11,
                        "updatedInstances": 13,
                        "readyInstances": 14,
                        "availableInstances": 15,
                    }]
                }
            }),
            field_manager="sentinel.parent_kind-%s" % self.component
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.format_timestamp")
    async def test__update_stats_removes_from_disruption_budget(self, format_timestamp):  # noqa: E501
        format_timestamp.return_value = "today_is_the_day"
        self.fake_yaook_disruption_budgets.append(
            {
                "metadata": {
                    "name": sentinel.group1_name,
                    "namespace": sentinel.group1_namespace,
                },
                "spec": {
                    "maxUnavailable": "10%",
                    "nodeSelectors": [{
                        "matchLabels": {
                            "somekey": "somevalue"
                        }
                    }]
                },
                "status": {
                    "nodes": [{
                        "type": "sentinel.parent_kind-%s" % self.component,
                        "lastUpdateTime": "today_is_the_day",
                        "configuredInstances": 10,
                        "existingInstances": 11,
                        "updatedInstances": 13,
                        "readyInstances": 14,
                        "availableInstances": 15,
                    }]
                }
            }
        )
        self.ydb_interface.read_status.return_value = \
            self.fake_yaook_disruption_budgets[0]

        await self.spn._update_stats(self.ctx, [
            instancing.InstanceGroup(
                kubernetes_asyncio.client.V1ObjectReference(
                    name=sentinel.group1_name,
                    namespace=sentinel.group1_namespace,
                ),
                "10%",
                instancing.InstanceStatistics(
                    nconfigured=0,
                    nexisting_instances=0,
                    nexisting_resources=0,
                    nupdated=0,
                    nready=0,
                    navailable=0,
                    ncreated=0,
                    ndeleted=0,
                )
            )
        ])

        self.ydb_interface.patch_status.assert_called_once_with(
            sentinel.group1_namespace,
            sentinel.group1_name,
            k8s._encode_body({
                "apiVersion": "yaook.cloud/v1",
                "kind": "YaookDisruptionBudget",
                "status": {},
            }),
            field_manager="sentinel.parent_kind-%s" % self.component
        )
