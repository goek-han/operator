import contextlib
import pathlib
import tempfile
import unittest
import unittest.mock

import kubernetes_asyncio.client

import yaook.statemachine.interfaces as interfaces

import yaook.statemachine.resources.k8s as k8s


def wrap_node_list(nodes):
    mock = kubernetes_asyncio.client.V1NodeList(
        items=list(nodes),
        metadata=kubernetes_asyncio.client.V1ListMeta()
    )
    return mock


def ResourceInterfaceMock(api_version="test.yaook.cloud/v1", plural="mocked"):
    result = unittest.mock.Mock(spec=interfaces.ResourceInterface)
    result.api_version = api_version
    result.plural = plural
    result.create = unittest.mock.AsyncMock()
    result.read = unittest.mock.AsyncMock()
    result.list_ = unittest.mock.AsyncMock()
    result.patch = unittest.mock.AsyncMock()
    result.delete = unittest.mock.AsyncMock()
    return result


class TemplateTestMixin:
    def setUp(self):
        super().setUp()
        self._exit_stack = contextlib.ExitStack()
        self._cm = self._exit_stack.__enter__()

        self._template_path = pathlib.Path(self._cm.enter_context(
            tempfile.TemporaryDirectory()
        ))
        with (self._template_path / "simple").open("w") as f:
            f.write("""\
test:
    arg: {{ template_arg }}
""")

        with (self._template_path / "ctx").open("w") as f:
            f.write("""\
test:
    namespace: {{ namespace }}
    parent: {{ crd_spec.replicas }}
    param: {{ params.foo }}
""")

    def tearDown(self):
        self._exit_stack.__exit__(None, None, None)
        del self._cm
        super().tearDown()


class ResourceTestMixin:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.make_body_mock = unittest.mock.AsyncMock()
        self.make_body_mock.side_effect = NotImplementedError
        self.resource_interface_mock = ResourceInterfaceMock()

    def _create_resource_interface(self, api_client):
        return self.resource_interface_mock

    async def _make_body(self, *args, **kwargs):
        return await self.make_body_mock(*args, **kwargs)


class KubernetesObjectStateMock(k8s.KubernetesResource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.resource_interface_mock = unittest.mock.Mock(
            spec=interfaces.ResourceInterface,
        )
        self.resource_interface_mock.create = unittest.mock.AsyncMock()
        self.resource_interface_mock.read = unittest.mock.AsyncMock()
        self.resource_interface_mock.list_ = unittest.mock.AsyncMock()
        self.resource_interface_mock.patch = unittest.mock.AsyncMock()
        self.resource_interface_mock.delete = unittest.mock.AsyncMock()
        self.resource_interface_mock.delete_collection = \
            unittest.mock.AsyncMock()

    def _create_resource_interface(self, api_client):
        return self.resource_interface_mock

    async def get(self, ctx):
        pass

    async def get_all(self, ctx):
        pass

    async def reconcile(self, ctx, dependencies):
        pass

    async def delete(self, ctx):
        pass


class SingleObjectMock(k8s.SingleObject, KubernetesObjectStateMock):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.needs_update_mock = unittest.mock.Mock([])
        self.needs_update_mock.side_effect = NotImplementedError()
        self.make_body_mock = unittest.mock.AsyncMock([])
        self.make_body_mock.side_effect = NotImplementedError()

    def _needs_update(self, old, new):
        return self.needs_update_mock(old, new)

    async def _make_body(self, ctx, dependencies):
        return await self.make_body_mock(ctx, dependencies)
