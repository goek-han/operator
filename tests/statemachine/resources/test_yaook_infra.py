#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import copy
import unittest
import unittest.mock

from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions

import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.yaook_infra as yaook_infra


class TestMySQLService(unittest.TestCase):
    class MySQLServiceStateTest(yaook_infra.MySQLService):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.mds = self.MySQLServiceStateTest()
        self.v1 = {
            "metadata": {
                "foo": "bar",
                "baz": "fnord",
                "labels": {
                    "foo": "foo"
                },
            },
            "spec": {
                "database": "db",
                "targetRelease": "10.2",
                "replicas": 2,
                "storageSize": "8Gi",
                "proxy": {
                    "replicas": 1,
                },
                "implementation": "MariaDB",
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_targetRelease_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["targetRelease"] = "abc"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageSize_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageSize"] = "12Gi"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_appears(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageClassName"] = "foo"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_disappears(self):
        self.v1["spec"]["storageClassName"] = "foo"
        v2 = copy.deepcopy(self.v1)
        del v2["spec"]["storageClassName"]

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_changes(self):
        self.v1["spec"]["storageClassName"] = "foo"
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageClassName"] = "bar"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_mysqlConfig_appears(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["mysqlConfig"] = sentinel.v2

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_mysqlConfig_disappears(self):
        self.v1["spec"]["mysqlConfig"] = sentinel.v1
        v2 = copy.deepcopy(self.v1)
        del v2["spec"]["mysqlConfig"]

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_mysqlConfig_changes(self):
        self.v1["spec"]["mysqlConfig"] = sentinel.v1
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["mysqlConfig"] = sentinel.v2

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_mysqlConfig_present_but_unchanged(self):  # noqa
        self.v1["spec"]["mysqlConfig"] = sentinel.v1
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["mysqlConfig"] = sentinel.v1

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_replicas_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["replicas"] = 4

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_backup_changed(self):
        self.v1["spec"]["backup"] = {"mysqldump": False}

        v2 = copy.deepcopy(self.v1)
        v2["spec"]["backup"]["mysqldump"] = True

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_proxy_replicas_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["proxy"]["replicas"] = 4

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_metadata_changes_only_in_last_update_annotation(self):  # noqa:E501
        self.v1["metadata"]["annotations"] = {
            context.ANNOTATION_LAST_UPDATE: "v1",
        }

        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["annotations"][context.ANNOTATION_LAST_UPDATE] = "v2"

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_cacertificates_changes(self):
        self.v1["spec"]["caCertificates"] = ["cert1"]
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["caCertificates"] = ["cert2"]

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_service_monitor_changes(self):
        self.v1["spec"]["serviceMonitor"] = ["mon1"]
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["serviceMonitor"] = ["mon2"]

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_cacertificates_order_changes(self):
        self.v1["spec"]["caCertificates"] = ["cert2", "cert1"]
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["caCertificates"] = ["cert1", "cert2"]

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_database_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["database"] = "x"

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_on_label_change(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["labels"]["foo"] = "bar"

        self.assertTrue(self.mds._needs_update(self.v1, v2))


class TestMySQLUser(unittest.IsolatedAsyncioTestCase):
    class TestMySQLUserStateTest(yaook_infra.MySQLUser):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.mus = self.TestMySQLUserStateTest()
        self.v1 = {
            "metadata": {},
            "spec": {
                "serviceRef": {"name": "foo-db"},
                "user": "foouser",
                "passwordSecretKeyRef": unittest.mock.sentinel.foo,
                "databasePrivileges": ["ALL PRIVILEGES"],
                "globalPrivileges": [],
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_secret_key_ref_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["passwordSecretKeyRef"] = unittest.mock.sentinel.bar

        self.assertTrue(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_service_ref_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["serviceRef"]["name"] = unittest.mock.sentinel.bar

        self.assertTrue(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_database_privileges_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["databasePrivileges"][0] = unittest.mock.sentinel.bar

        self.assertTrue(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_global_privileges_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["globalPrivileges"] = [unittest.mock.sentinel.bar]

        self.assertTrue(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        self.v1["metadata"]["foo"] = "bar"
        self.v1["metadata"]["baz"] = "fnord"

        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        self.v1["metadata"]["foo"] = "bar"
        self.v1["metadata"]["baz"] = "fnord"

        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.mus._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_user_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["user"] = "x"

        self.assertFalse(self.mus._needs_update(self.v1, v2))

    async def test_get_used_resources_returns_empty_if_resource_not_present(self):  # noqa
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.mus, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                ctx,
            )

            self.assertCountEqual(
                await self.mus.get_used_resources(ctx),
                [],
            )

    async def test_get_used_resources_returns_referenced_secret_of_existing_resource(self):  # noqa
        ctx = unittest.mock.Mock(["namespace"])

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.mus, "_get_current",
            ))
            _get_current.return_value = {
                "spec": {
                    "passwordSecretKeyRef": {
                        "name": unittest.mock.sentinel.secret_name,
                    }
                }
            }

            self.assertCountEqual(
                await self.mus.get_used_resources(ctx),
                [
                    api_utils.ResourceReference(
                        api_version="v1",
                        plural="secrets",
                        namespace=ctx.namespace,
                        name=unittest.mock.sentinel.secret_name,
                    )
                ],
            )

            _get_current.assert_called_once_with(ctx, True)


class TestSimpleMySQLUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.db = unittest.mock.Mock(k8s.KubernetesReference)
        self.pwd = unittest.mock.Mock(k8s.KubernetesReference)

        self.smus = yaook_infra.SimpleMySQLUser(
            metadata=sentinel.metadata_provider,
            database=self.db,
            username=str(sentinel.username),
            password_secret=self.pwd,
            password_key=sentinel.pwd_key,
        )

    def test_is_mysql_user_state(self):
        self.assertIsInstance(self.smus, yaook_infra.MySQLUser)

    def test_declares_dependencies(self):
        self.assertIn(self.db, self.smus._dependencies)
        self.assertIn(self.pwd, self.smus._dependencies)

    async def test__make_body_composes_all_the_things(self):
        self.db.get.return_value = kclient.V1ObjectReference(
            name=sentinel.db_name,
        )

        self.pwd.get.return_value = kclient.V1ObjectReference(
            name=sentinel.pwd_secret_name,
        )

        with contextlib.ExitStack() as stack:
            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.yaook_infra.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            ctx = unittest.mock.Mock(["instance"])

            result = await self.smus._make_body(
                ctx,
                sentinel.deps,
            )

        evaluate_metadata.assert_called_once_with(
            ctx,
            sentinel.metadata_provider,
        )

        self.db.get.assert_awaited_once_with(ctx)
        self.pwd.get.assert_awaited_once_with(ctx)

        self.assertDictEqual(
            result,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "MySQLUser",
                "metadata": sentinel.metadata,
                "spec": {
                    "user": str(sentinel.username),
                    "passwordSecretKeyRef": {
                        "name": sentinel.pwd_secret_name,
                        "key": sentinel.pwd_key,
                    },
                    "serviceRef": {
                        "name": sentinel.db_name,
                    },
                },
            },
        )

    async def test__make_body_templates_username(self):
        self.smus._username = "{instance}-test"

        self.db.get.return_value = kclient.V1ObjectReference(
            name=sentinel.db_name,
        )

        self.pwd.get.return_value = kclient.V1ObjectReference(
            name=sentinel.pwd_secret_name,
        )

        ctx = unittest.mock.Mock(["instance"])
        ctx.instance = "myinstance"

        with contextlib.ExitStack() as stack:
            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.yaook_infra.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            result = await self.smus._make_body(
                ctx,
                sentinel.deps,
            )

        evaluate_metadata.assert_called_once_with(
            ctx,
            sentinel.metadata_provider,
        )

        self.db.get.assert_awaited_once_with(ctx)
        self.pwd.get.assert_awaited_once_with(ctx)

        self.assertDictEqual(
            result,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "MySQLUser",
                "metadata": sentinel.metadata,
                "spec": {
                    "user": "myinstance-test",
                    "passwordSecretKeyRef": {
                        "name": sentinel.pwd_secret_name,
                        "key": sentinel.pwd_key,
                    },
                    "serviceRef": {
                        "name": sentinel.db_name,
                    },
                },
            },
        )


class TestAMQPServer(unittest.TestCase):
    class AMQPServerStateTest(yaook_infra.AMQPServer):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.ast = self.AMQPServerStateTest()
        self.v1 = {
            "metadata": {
                "foo": "bar",
                "baz": "fnord"
            },
            "spec": {
                "imageRef": "xyz",
                "replicas": 2,
                "implementation": "RabbitMQ",
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_imageRef_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["imageRef"] = "abc"

        self.assertTrue(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_replicas_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["replicas"] = 4

        self.assertTrue(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageclass_changes(self):
        self.v1["spec"]["storageClassName"] = "123"
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageClassName"] = "456"

        self.assertTrue(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_service_monitor_changes(self):
        self.v1["spec"]["serviceMonitor"] = ["mon1"]
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["serviceMonitor"] = ["mon2"]

        self.assertTrue(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_metadata_changes_only_in_last_update_annotation(self):  # noqa:E501
        self.v1["metadata"]["annotations"] = {
            context.ANNOTATION_LAST_UPDATE: "v1",
        }

        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["annotations"][context.ANNOTATION_LAST_UPDATE] = "v2"

        self.assertFalse(self.ast._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.ast._needs_update(self.v1, v2))


class TestAMQPUser(unittest.IsolatedAsyncioTestCase):
    class AMQPUserStateTest(yaook_infra.AMQPUser):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.aus = self.AMQPUserStateTest()
        self.v1 = {
            "metadata": {
                "foo": "bar",
                "baz": "fnord"
            },
            "spec": {
                "amqpServerName": "foo-mq",
                "user": "foouser",
                "passwordSecretKeyRef": unittest.mock.sentinel.foo,
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.aus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_secret_key_ref_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["passwordSecretKeyRef"] = unittest.mock.sentinel.bar

        self.assertTrue(self.aus._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.aus._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.aus._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_amqpServerName_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["amqpServerName"] = "x"

        self.assertFalse(self.aus._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_user_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["user"] = "x"

        self.assertFalse(self.aus._needs_update(self.v1, v2))

    async def test_get_used_resources_returns_empty_if_resource_not_present(self):  # noqa
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.aus, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                ctx,
            )

            self.assertCountEqual(
                await self.aus.get_used_resources(ctx),
                [],
            )

    async def test_get_used_resources_returns_referenced_secret_of_existing_resource(self):  # noqa
        ctx = unittest.mock.Mock(["namespace"])

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.aus, "_get_current",
            ))
            _get_current.return_value = {
                "spec": {
                    "passwordSecretKeyRef": {
                        "name": unittest.mock.sentinel.secret_name,
                    }
                }
            }

            self.assertCountEqual(
                await self.aus.get_used_resources(ctx),
                [
                    api_utils.ResourceReference(
                        api_version="v1",
                        plural="secrets",
                        namespace=ctx.namespace,
                        name=unittest.mock.sentinel.secret_name,
                    )
                ],
            )

            _get_current.assert_called_once_with(ctx, True)


class TestSimpleAMQPUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.srv = unittest.mock.Mock(k8s.KubernetesReference)
        self.pwd = unittest.mock.Mock(k8s.KubernetesReference)

        self.saus = yaook_infra.SimpleAMQPUser(
            metadata=sentinel.metadata_provider,
            server=self.srv,
            username_format="foo-{instance}-{name}",
            password_secret=self.pwd,
            password_key=sentinel.pwd_key,
        )

    def test_is_mysql_user_state(self):
        self.assertIsInstance(self.saus, yaook_infra.AMQPUser)

    def test_declares_dependencies(self):
        self.assertIn(self.srv, self.saus._dependencies)
        self.assertIn(self.pwd, self.saus._dependencies)

    async def test__make_body_composes_all_the_things(self):
        self.srv.get.return_value = kclient.V1ObjectReference(
            name=sentinel.srv_name,
        )

        self.pwd.get.return_value = kclient.V1ObjectReference(
            name=sentinel.pwd_secret_name,
        )

        ctx = unittest.mock.Mock()
        ctx.instance = "magic-instance"
        ctx.parent_name = "myname"

        with contextlib.ExitStack() as stack:
            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.yaook_infra.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            result = await self.saus._make_body(
                ctx,
                sentinel.deps,
            )

        evaluate_metadata.assert_called_once_with(
            ctx,
            sentinel.metadata_provider,
        )

        self.srv.get.assert_awaited_once_with(ctx)
        self.pwd.get.assert_awaited_once_with(ctx)

        self.assertDictEqual(
            result,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "AMQPUser",
                "metadata": sentinel.metadata,
                "spec": {
                    "user": "foo-magic-instance-myname",
                    "passwordSecretKeyRef": {
                        "name": sentinel.pwd_secret_name,
                        "key": sentinel.pwd_key,
                    },
                    "serverRef": {
                        "name": sentinel.srv_name,
                    },
                },
            },
        )


class TestMemcachedService(unittest.TestCase):
    class MemcachedServiceStateTest(yaook_infra.MemcachedService):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.mss = self.MemcachedServiceStateTest()
        self.v1 = {
            "metadata": {
                "foo": "bar",
                "baz": "fnord",
                "annotations": {
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": {
                "imageRef": "xyz",
                "memory": 512,
                "connections": 567,
                "replicas": 2
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_imageRef_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["imageRef"] = "abc"

        self.assertTrue(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_memory_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["memory"] = "1024"

        self.assertTrue(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_connections_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["connections"] = 123

        self.assertTrue(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_replicas_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["replicas"] = 4

        self.assertTrue(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_metadata_changes_only_in_last_update_annotation(self):  # noqa:E501
        self.v1["metadata"]["annotations"][context.ANNOTATION_LAST_UPDATE] = (
            "v1"
        )

        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["annotations"][context.ANNOTATION_LAST_UPDATE] = "v2"

        self.assertFalse(self.mss._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.mss._needs_update(self.v1, v2))


class TestOVSDBService(unittest.TestCase):
    class OVSDBServiceStateTest(yaook_infra.OVSDBService):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.mds = self.OVSDBServiceStateTest()
        self.v1 = {
            "metadata": {
                "foo": "bar",
                "baz": "fnord",
                "labels": {
                    "foo": "foo"
                },
            },
            "spec": {
                "dbSchema": "northbound",
                "backup": {
                    "schedule": "0 12 * * *"
                },
                "imageRef": "example.com/ovsdb:1.2.3",
                "issuerRef": {
                    "name": "ovn-central-ca-issuer",
                },
                "replicas": 2,
                "storageSize": "8Gi",
            }
        }

    def test__needs_update_returns_false_if_unchanged(self):
        v2 = copy.deepcopy(self.v1)

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_imageRef_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["imageRef"] = "foo.com/bar"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageSize_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageSize"] = "12Gi"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_appears(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageClassName"] = "foo"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_disappears(self):
        self.v1["spec"]["storageClassName"] = "foo"
        v2 = copy.deepcopy(self.v1)
        del v2["spec"]["storageClassName"]

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_storageClassName_changes(self):
        self.v1["spec"]["storageClassName"] = "foo"
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["storageClassName"] = "bar"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_issuerRef_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["issuerRef"]["name"] = "otherIssuer"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_replicas_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["replicas"] = 4

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_if_metadata_changes(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["baz"] = "frobnicate"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_metadata_changes_only_in_last_update_annotation(self):  # noqa:E501
        self.v1["metadata"]["annotations"] = {
            context.ANNOTATION_LAST_UPDATE: "v1",
        }

        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["annotations"][context.ANNOTATION_LAST_UPDATE] = "v2"

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_metadata_key_not_set(self):
        v2 = copy.deepcopy(self.v1)
        del v2["metadata"]["baz"]

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_false_if_just_dbSchema_changed(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["dbSchema"] = "southbound"

        self.assertFalse(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_on_label_change(self):
        v2 = copy.deepcopy(self.v1)
        v2["metadata"]["labels"]["foo"] = "bar"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_on_backup_change(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["backup"]["schedule"] = "0 0 * * *"

        self.assertTrue(self.mds._needs_update(self.v1, v2))

    def test__needs_update_returns_true_on_resources_change(self):
        v2 = copy.deepcopy(self.v1)
        v2["spec"]["resources"] = {"ovsdb": {
            "requests": {"cpu": "10m", "memory": "20k"},
        }}

        self.assertTrue(self.mds._needs_update(self.v1, v2))
