import unittest
import uuid
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.yaook_keystone as yaook_keystone

from .utils import ResourceTestMixin


class TestKeystoneUser(unittest.TestCase):
    class KeystoneUserTest(ResourceTestMixin, yaook_keystone.KeystoneUser):
        pass

    def test_needs_update_compares_password_secret(self):
        s = self.KeystoneUserTest()

        self.assertTrue(
            s._needs_update(
                {},
                {"spec": {"password_secret": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"unrelated": "foo"}},
                {"spec": {"password_secret": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"password_secret": "bar"}},
                {"spec": {"password_secret": "foo"}}
            ),
        )
        self.assertFalse(
            s._needs_update(
                {"spec": {"password_secret": "foo"}},
                {"spec": {"password_secret": "foo"}}
            ),
        )

    def test_needs_update_compares_keystone_ref(self):
        s = self.KeystoneUserTest()

        self.assertTrue(
            s._needs_update(
                {},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"unrelated": "foo"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"keystoneRef": "bar"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertFalse(
            s._needs_update(
                {"spec": {"keystoneRef": "foo"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )


class TestStaticKeystoneUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.username = str(uuid.uuid4())
        self.keystone = unittest.mock.Mock(k8s.KubernetesReference)
        self.keystone.get.return_value = kclient.V1ObjectReference(
            name=sentinel.keystone_name,
        )

        self.sku = yaook_keystone.StaticKeystoneUser(
            username=self.username,
            keystone=self.keystone,
        )

    async def test__make_body_creates_keystoneuser_with_dependent_state(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {"keystoneRef": {"kind": sentinel.keystonereftype}}
        result = await self.sku._make_body(
            ctx,
            sentinel.dependencies,
        )

        self.keystone.get.assert_awaited_once_with(ctx)

        self.assertEqual(
            result,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "KeystoneUser",
                "metadata": {
                    "generateName": "{}-".format(self.username),
                },
                "spec": {
                    "keystoneRef": {
                        "name": sentinel.keystone_name,
                        "kind": sentinel.keystonereftype,
                    },
                }
            }
        )


class TestStaticKeystoneUserWithParent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.username = str(uuid.uuid4())
        self.keystone = unittest.mock.Mock(k8s.KubernetesReference)
        self.keystone.get.return_value = kclient.V1ObjectReference(
            name=sentinel.keystone_name,
        )

        self.dku = yaook_keystone.StaticKeystoneUserWithParent(
            username=self.username,
            keystone=self.keystone,
        )

    async def test__make_body_creates_keystoneuser_with_dependent_state(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {"keystoneRef": {"kind": sentinel.keystonereftype}}
        ctx.parent_name = "nodeX"
        result = await self.dku._make_body(
            ctx,
            sentinel.dependencies,
        )

        self.keystone.get.assert_awaited_once_with(ctx)

        self.assertEqual(
            result,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "KeystoneUser",
                "metadata": {
                    "generateName": "{}-nodeX-".format(self.username),
                },
                "spec": {
                    "keystoneRef": {
                        "name": sentinel.keystone_name,
                        "kind": sentinel.keystonereftype,
                    },
                }
            }
        )


class TestKeystoneEndpoint(unittest.TestCase):
    class KeystoneEndpointTest(ResourceTestMixin,
                               yaook_keystone.KeystoneEndpoint):
        pass

    def test_needs_update_compares_endpoints(self):
        s = self.KeystoneEndpointTest()

        self.assertTrue(
            s._needs_update(
                {},
                {"spec": {"endpoints": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"unrelated": "foo"}},
                {"spec": {"endpoints": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"endpoints": unittest.mock.sentinel.bar}},
                {"spec": {"endpoints": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertFalse(
            s._needs_update(
                {"spec": {"endpoints": unittest.mock.sentinel.foo}},
                {"spec": {"endpoints": unittest.mock.sentinel.foo}}
            ),
        )

    def test_needs_update_compares_description(self):
        s = self.KeystoneEndpointTest()

        self.assertTrue(
            s._needs_update(
                {},
                {"spec": {"description": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"unrelated": "foo"}},
                {"spec": {"description": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"description": unittest.mock.sentinel.bar}},
                {"spec": {"description": unittest.mock.sentinel.foo}}
            ),
        )
        self.assertFalse(
            s._needs_update(
                {"spec": {"description": unittest.mock.sentinel.foo}},
                {"spec": {"description": unittest.mock.sentinel.foo}}
            ),
        )

    def test_needs_update_compares_keystone_ref(self):
        s = self.KeystoneEndpointTest()

        self.assertTrue(
            s._needs_update(
                {},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"unrelated": "foo"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertTrue(
            s._needs_update(
                {"spec": {"keystoneRef": "bar"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )
        self.assertFalse(
            s._needs_update(
                {"spec": {"keystoneRef": "foo"}},
                {"spec": {"keystoneRef": "foo"}}
            ),
        )


class TestInstancedKeystoneUser(unittest.IsolatedAsyncioTestCase):
    async def test__make_body_generates_user_with_instance_suffix(self):
        keystone = unittest.mock.Mock(k8s.KubernetesReference)
        keystone.get.return_value = kclient.V1ObjectReference(
            name=sentinel.keystone_name,
        )

        iku = yaook_keystone.InstancedKeystoneUser(
            prefix="prefix",
            keystone=keystone,
        )
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {"keystoneRef": {"kind": sentinel.keystonereftype}}
        ctx.instance = "some-instance"

        result = await iku._make_body(ctx, unittest.mock.sentinel.deps)

        keystone.get.assert_awaited_once_with(ctx)

        self.assertEqual(
            result,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "KeystoneUser",
                "metadata": {
                    "generateName": "prefix-some-instance-",
                },
                "spec": {
                    "keystoneRef": {
                        "name": sentinel.keystone_name,
                        "kind": sentinel.keystonereftype,
                    },
                }
            }
        )
