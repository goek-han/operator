#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient
import yaook.common.config as common_config
import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.cue as cue
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.resources as resources


class TestCueMixin(unittest.IsolatedAsyncioTestCase):
    class CueMixinTest(cue.CueMixin):
        async def _get_cue_inputs(self, ctx):
            return sentinel.inputs

    def setUp(self):
        self.cm = self.CueMixinTest()

    @unittest.mock.patch("yaook.common.config.build_config")
    async def test__render_cue_config_evaluates_inputs_in_cue(
            self,
            build_config):
        await self.cm._render_cue_config(sentinel.ctx)

        build_config.assert_called_once_with(sentinel.inputs)

    @unittest.mock.patch("yaook.common.config.build_config")
    async def test__render_cue_config_translates_validation_error(
            self,
            build_config):
        build_config.side_effect = common_config.ConfigValidationError(
            "the message"
        )

        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.cm._render_cue_config(sentinel.ctx)

        build_config.assert_called_once_with(sentinel.inputs)


class TestCueLayer(unittest.IsolatedAsyncioTestCase):
    class CueLayerTest(cue.CueLayer):
        async def get_layer(self, ctx):
            return sentinel.layer

    def setUp(self):
        self.cl = self.CueLayerTest()

    def test_no_dependencies_by_default(self):
        self.assertSequenceEqual(self.cl.get_dependencies(), [])

    def test_allows_adding_dependencies_in_constructor(self):
        cl = self.CueLayerTest(add_dependencies=[sentinel.dep1, sentinel.dep2])
        self.assertCountEqual(
            cl.get_dependencies(),
            [sentinel.dep1, sentinel.dep2],
        )

    def test_allows_adding_dependencies_via__declare_dependencies(self):
        self.cl._declare_dependencies(sentinel.dep1)
        self.assertCountEqual(
            self.cl.get_dependencies(),
            [sentinel.dep1],
        )


class TestLayeredCueMixin(unittest.IsolatedAsyncioTestCase):
    class LayeredCueTest(cue.LayeredCueMixin):
        def __init__(self, **kwargs):
            self._recorded_dependencies = []
            super().__init__(**kwargs)

        def _declare_dependencies(self, *deps):
            self._recorded_dependencies.extend(deps)

    def setUp(self):
        self.lcm = self.LayeredCueTest()
        self.layer1 = unittest.mock.Mock(cue.CueLayer)
        self.layer1.get_dependencies.return_value = [sentinel.dep1]
        self.layer1.get_layer.return_value = {
            sentinel.cfg1: common_config.ConfigDeclaration(
                flavor=sentinel.flavor1,
                contents=[
                    sentinel.v1_1_1,
                    sentinel.v1_1_2,
                ],
            ),
            sentinel.cfg2:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor2,
                contents=[
                    sentinel.v1_2_1,
                    sentinel.v1_2_2,
                ],
            ),
        }
        self.layer2 = unittest.mock.Mock(cue.CueLayer)
        self.layer2.get_dependencies.return_value = [sentinel.dep2]
        self.layer2.get_layer.return_value = {
            sentinel.cfg2:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor2,
                contents=[
                    sentinel.v2_2_1,
                    sentinel.v2_2_2,
                ],
            ),
            sentinel.cfg3:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor3,
                contents=[
                    sentinel.v2_3_1,
                    sentinel.v2_3_2,
                ],
            ),
        }
        self.layer3 = unittest.mock.Mock(cue.CueLayer)
        self.layer3.get_dependencies.return_value = [sentinel.dep3]
        self.layer3.get_layer.return_value = {
            sentinel.cfg3:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor3,
                contents=[
                    sentinel.v3_3_1,
                    sentinel.v3_3_2,
                ],
            ),
            sentinel.cfg1:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor1,
                contents=[
                    sentinel.v3_1_1,
                    sentinel.v3_1_2,
                ],
            ),
        }
        self.broken_layer = unittest.mock.Mock(cue.CueLayer)
        self.broken_layer.get_dependencies.return_value = []
        self.broken_layer.get_layer.return_value = {
            sentinel.cfg3:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor1,
                contents=[
                    sentinel.v3_3_1,
                    sentinel.v3_3_2,
                ],
            ),
            sentinel.cfg1:  common_config.ConfigDeclaration(
                flavor=sentinel.flavor3,
                contents=[
                    sentinel.v3_1_1,
                    sentinel.v3_1_2,
                ],
            ),
        }

    async def test__get_cue_inputs_joins_layers_with_matching_flavors(self):
        self.lcm.add_layer(self.layer1)

        result = await self.lcm._get_cue_inputs(sentinel.ctx)

        self.layer1.get_layer.assert_awaited_once_with(sentinel.ctx)
        self.assertDictEqual(
            result,
            await self.layer1.get_layer(sentinel.ctx),
        )

        self.layer1.reset_mock()
        self.lcm.add_layer(self.layer2)
        self.lcm.add_layer(self.layer3)

        result = await self.lcm._get_cue_inputs(sentinel.ctx)

        self.layer1.get_layer.assert_awaited_once_with(sentinel.ctx)
        self.layer2.get_layer.assert_awaited_once_with(sentinel.ctx)
        self.layer3.get_layer.assert_awaited_once_with(sentinel.ctx)
        self.assertDictEqual(
            result,
            {
                sentinel.cfg1: common_config.ConfigDeclaration(
                    flavor=sentinel.flavor1,
                    contents=[
                        sentinel.v1_1_1,
                        sentinel.v1_1_2,
                        sentinel.v3_1_1,
                        sentinel.v3_1_2,
                    ],
                ),
                sentinel.cfg2: common_config.ConfigDeclaration(
                    flavor=sentinel.flavor2,
                    contents=[
                        sentinel.v1_2_1,
                        sentinel.v1_2_2,
                        sentinel.v2_2_1,
                        sentinel.v2_2_2,
                    ],
                ),
                sentinel.cfg3: common_config.ConfigDeclaration(
                    flavor=sentinel.flavor3,
                    contents=[
                        sentinel.v2_3_1,
                        sentinel.v2_3_2,
                        sentinel.v3_3_1,
                        sentinel.v3_3_2,
                    ],
                ),
            }
        )

    async def test__get_cue_inputs_rejects_merging_layers_with_mismatching_config_flavors(self):  # noqa:E501
        self.lcm.add_layer(self.layer1)

        result = await self.lcm._get_cue_inputs(sentinel.ctx)

        self.layer1.get_layer.assert_awaited_once_with(sentinel.ctx)
        self.assertDictEqual(
            result,
            await self.layer1.get_layer(sentinel.ctx),
        )

        self.layer1.reset_mock()
        self.lcm.add_layer(self.layer2)
        self.lcm.add_layer(self.layer3)
        self.lcm.add_layer(self.broken_layer)

        with self.assertRaisesRegex(
                ValueError,
                r"mixing different configuration flavors in a single file is "
                r"not supported: got different flavors .*flavor3 and .*flavor1"
                r" from cue layers for .*cfg3"):
            await self.lcm._get_cue_inputs(sentinel.ctx)

        self.broken_layer.get_layer.assert_awaited_once_with(sentinel.ctx)

    def test_no_dependencies_by_default(self):
        self.assertCountEqual(
            self.lcm._recorded_dependencies,
            [],
        )

    def test_declares_dependencies_of_layers_added_at_construction_time(self):
        lcm = self.LayeredCueTest(
            add_cue_layers=[self.layer1, self.layer2],
        )
        self.assertCountEqual(
            lcm._recorded_dependencies,
            [
                sentinel.dep1,
                sentinel.dep2,
            ],
        )

    def test_declares_dependencies_of_layers_added_later_on(self):
        self.lcm.add_layer(self.layer3)
        self.assertCountEqual(
            self.lcm._recorded_dependencies,
            [
                sentinel.dep3,
            ],
        )


class TestCueConfigMap(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ccms = cue.CueConfigMap(
            metadata=sentinel.metadata_provider,
        )

    async def test_assembles_config_map_with_cue_data(self):
        with contextlib.ExitStack() as stack:
            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            _render = stack.enter_context(unittest.mock.patch.object(
                self.ccms, "_render_cue_config",
            ))
            _render.return_value = sentinel.config

            result = await self.ccms._make_body(
                sentinel.ctx,
                sentinel.deps,
            )

        evaluate_metadata.assert_called_once_with(
            sentinel.ctx, sentinel.metadata_provider,
        )

        _render.assert_awaited_once_with(sentinel.ctx)

        self.assertDictEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": sentinel.metadata,
                "data": sentinel.config,
            }
        )


class TestCueSecret(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ccms = cue.CueSecret(
            metadata=sentinel.metadata_provider,
        )

    async def test_assembles_secret_with_utf8encoded_cue_data(self):
        with contextlib.ExitStack() as stack:
            evaluate_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.evaluate_metadata",
            ))
            evaluate_metadata.return_value = sentinel.metadata

            _render = stack.enter_context(unittest.mock.patch.object(
                self.ccms, "_render_cue_config",
            ))
            _render.return_value = sentinel.config

            encode_secret_data = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.encode_secret_data",
            ))
            encode_secret_data.return_value = sentinel.encoded_config

            result = await self.ccms._make_body(
                sentinel.ctx,
                sentinel.deps,
            )

        evaluate_metadata.assert_called_once_with(
            sentinel.ctx, sentinel.metadata_provider,
        )

        _render.assert_awaited_once_with(sentinel.ctx)

        encode_secret_data.assert_called_once_with(sentinel.config,
                                                   encoding="utf-8")

        self.assertDictEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": sentinel.metadata,
                "type": "Opaque",
                "data": sentinel.encoded_config,
            }
        )


class TestAMQPConnectionLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.svc = unittest.mock.Mock(resources.KubernetesReference)
        self.password = unittest.mock.Mock(resources.KubernetesReference)
        self.amqptl = cue.AMQPTransportLayer(
            target=sentinel.target,
            service=self.svc,
            username=lambda _: sentinel.username,
            password_secret=self.password,
        )

    def test_declares_service_as_dependency(self):
        self.assertIn(self.svc, self.amqptl.get_dependencies())

    def test_declares_password_as_dependency(self):
        self.assertIn(self.password, self.amqptl.get_dependencies())

    @unittest.mock.patch("yaook.statemachine.resources.base.extract_password")
    async def test_get_layer_returns_transport_uri_info(
            self,
            extract_password):
        extract_password.return_value = sentinel.password

        service_ref = kclient.V1ObjectReference(
            name="mq-service-name",
            namespace="mq-service-namespace",
        )
        self.svc.get.return_value = service_ref
        host = f"{service_ref.name}.{service_ref.namespace}"

        self.assertDictEqual(
            await self.amqptl.get_layer(sentinel.ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[{
                        "DEFAULT": {
                            "#transport_url_username": sentinel.username,
                            "#transport_url_hosts": [host],
                            "#transport_url_password": sentinel.password,
                            "#transport_url_vhost": "",
                        }
                    }],
                ),
            }
        )

        extract_password.assert_awaited_once_with(
            sentinel.ctx,
            self.password,
        )

        self.svc.get.assert_awaited_once_with(sentinel.ctx)


class TestDatabaseConnectionLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.svc = unittest.mock.Mock(resources.KubernetesReference)
        self.password = unittest.mock.Mock(resources.KubernetesReference)
        self.dcl = cue.DatabaseConnectionLayer(
            target=sentinel.target,
            service=self.svc,
            database_name=sentinel.db_name,
            username=lambda _: sentinel.username,
            password_secret=self.password,
            config_section=sentinel.config_section,
        )

    def test_declares_service_as_dependency(self):
        self.assertIn(self.svc, self.dcl.get_dependencies())

    def test_declares_password_as_dependency(self):
        self.assertIn(self.password, self.dcl.get_dependencies())

    @unittest.mock.patch("yaook.statemachine.resources.base.extract_password")
    async def test_get_layer_returns_database_connection_info(
            self,
            extract_password):
        extract_password.return_value = sentinel.password

        service_ref = kclient.V1ObjectReference(
            name="db-service-name",
            namespace="db-service-namespace",
        )
        self.svc.get.return_value = service_ref
        host = f"{service_ref.name}.{service_ref.namespace}"

        self.assertDictEqual(
            await self.dcl.get_layer(sentinel.ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[{
                        sentinel.config_section: {
                            "#connection_username": sentinel.username,
                            "#connection_database": sentinel.db_name,
                            "#connection_host": host,
                            "#connection_password": sentinel.password
                        }
                    }],
                ),
            },
        )

        extract_password.assert_awaited_once_with(
            sentinel.ctx,
            self.password,
        )

        self.svc.get.assert_awaited_once_with(sentinel.ctx)


class TestKeystoneAuthLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.credentials = unittest.mock.Mock(resources.KubernetesReference)
        self.endpoint_cfg = unittest.mock.Mock(resources.KubernetesReference)
        self.kal = cue.KeystoneAuthLayer(
            target=sentinel.target,
            config_section=sentinel.config_section,
            credentials_secret=self.credentials,
            endpoint_config=self.endpoint_cfg,
        )

    def test_declares_credentials_as_dependency(self):
        self.assertIn(self.credentials, self.kal.get_dependencies())

    def test_declares_endpoint_config_as_dependency(self):
        self.assertIn(self.endpoint_cfg, self.kal.get_dependencies())

    def test__map_credentials_with_password_auth_and_names(self):
        secret_data = {
            "OS_AUTH_TYPE": "password",
            "OS_USERNAME": sentinel.username,
            "OS_PASSWORD": sentinel.password,
            "OS_PROJECT_NAME": sentinel.project_name,
            "OS_USER_DOMAIN_NAME": sentinel.user_domain_name,
            "OS_PROJECT_DOMAIN_NAME": sentinel.project_domain_name,
        }

        result = self.kal._map_credentials(secret_data)

        self.assertEqual(result["auth_type"], "password")
        self.assertEqual(result["username"], sentinel.username)
        self.assertEqual(result["password"], sentinel.password)
        self.assertEqual(result["project_name"], sentinel.project_name)
        self.assertEqual(result["project_domain_name"],
                         sentinel.project_domain_name)
        self.assertEqual(result["user_domain_name"],
                         sentinel.user_domain_name)

    def test__map_credentials_rejects_non_password_auth_type(self):
        secret_data = {
            "OS_AUTH_TYPE": "foobar",
            "OS_USERNAME": sentinel.username,
            "OS_PASSWORD": sentinel.password,
            "OS_PROJECT_NAME": sentinel.project_name,
            "OS_USER_DOMAIN_NAME": sentinel.user_domain_name,
            "OS_PROJECT_DOMAIN_NAME": sentinel.project_domain_name,
        }

        with self.assertRaisesRegex(
                ValueError,
                "unsupported OS_AUTH_TYPE for KeystoneAuthLayer: foobar"):
            self.kal._map_credentials(secret_data)

    def test__map_endpoint_config(self):
        memcached_servers = "pod_prefix-0:123,pod_prefix-1:123"
        configmap_data = {
            "OS_INTERFACE": sentinel.interface,
            "OS_IDENTITY_API_VERSION": sentinel.identity_api_version,
            "OS_AUTH_URL": sentinel.auth_url,
            "MEMCACHED_SERVERS": memcached_servers,
        }

        result = self.kal._map_endpoint_config(configmap_data)

        self.assertEqual(result["valid_interfaces"],
                         [sentinel.interface])
        self.assertEqual(result["auth_version"], sentinel.identity_api_version)
        self.assertEqual(result["auth_url"], sentinel.auth_url)
        self.assertEqual(result["memcached_servers"],
                         memcached_servers.split(','))

    def test__map_endpoint_config_with_interface_override(self):
        memcached_servers = "pod_prefix-0:123,pod_prefix-1:123"
        configmap_data = {
            "OS_INTERFACE": sentinel.interface,
            "OS_IDENTITY_API_VERSION": sentinel.identity_api_version,
            "OS_AUTH_URL": sentinel.auth_url,
            "MEMCACHED_SERVERS": memcached_servers,
        }

        self.kal = cue.KeystoneAuthLayer(
            target=sentinel.target,
            credentials_secret=self.credentials,
            endpoint_config=self.endpoint_cfg,
            interface_override=sentinel.override,
        )

        result = self.kal._map_endpoint_config(configmap_data)

        self.assertEqual(result["valid_interfaces"],
                         [sentinel.override])
        self.assertEqual(result["auth_version"], sentinel.identity_api_version)
        self.assertEqual(result["auth_url"], sentinel.auth_url)
        self.assertEqual(result["memcached_servers"],
                         memcached_servers.split(','))

    def test__map_endpoint_config_sets_interface_if_keystone_authtoken(self):
        memcached_servers = "pod_prefix-0:123,pod_prefix-1:123"
        configmap_data = {
            "OS_INTERFACE": sentinel.interface,
            "OS_IDENTITY_API_VERSION": sentinel.identity_api_version,
            "OS_AUTH_URL": sentinel.auth_url,
            "MEMCACHED_SERVERS": memcached_servers,
        }

        self.kal = cue.KeystoneAuthLayer(
            target=sentinel.target,
            credentials_secret=self.credentials,
            endpoint_config=self.endpoint_cfg,
        )

        result = self.kal._map_endpoint_config(configmap_data)

        self.assertEqual(result["valid_interfaces"],
                         [sentinel.interface])
        self.assertEqual(result["auth_version"], sentinel.identity_api_version)
        self.assertEqual(result["auth_url"], sentinel.auth_url)
        self.assertEqual(result["memcached_servers"],
                         memcached_servers.split(','))
        self.assertEqual(result["interface"], sentinel.interface)

    @unittest.mock.patch("yaook.statemachine.interfaces.config_map_interface")
    @unittest.mock.patch("yaook.statemachine.interfaces.secret_interface")
    async def test_get_layer_maps_environment_to_config(
            self,
            secret_interface,
            config_map_interface,
            ):
        secret_interface.return_value.read = unittest.mock.AsyncMock()
        secret_interface.return_value.read.return_value = kclient.V1Secret(
            data=api_utils.encode_secret_data({
                "key1": "value1",
            }),
        )

        config_map_interface.return_value.read = unittest.mock.AsyncMock()
        config_map_interface.return_value.read.return_value = \
            kclient.V1ConfigMap(
                data={
                    "cfg2": "value2",
                },
            )

        self.credentials.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.credentials_namespace,
            name=sentinel.credentials_name,
        )

        self.endpoint_cfg.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.endpoint_namespace,
            name=sentinel.endpoint_name,
        )

        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _map_credentials = stack.enter_context(unittest.mock.patch.object(
                self.kal, "_map_credentials",
            ))
            _map_credentials.return_value = sentinel.mapped_credentials

            _map_endpoint_config = stack.enter_context(
                unittest.mock.patch.object(
                    self.kal, "_map_endpoint_config",
                )
            )
            _map_endpoint_config.return_value = sentinel.mapped_config

            result = await self.kal.get_layer(ctx)

        secret_interface.assert_called_once_with(ctx.api_client)
        self.credentials.get.assert_awaited_once_with(ctx)
        secret_interface.return_value.read.assert_awaited_once_with(
            sentinel.credentials_namespace,
            sentinel.credentials_name,
        )
        _map_credentials.assert_called_once_with({"key1": "value1"})

        config_map_interface.assert_called_once_with(ctx.api_client)
        self.endpoint_cfg.get.assert_awaited_once_with(ctx)
        config_map_interface.return_value.read.assert_awaited_once_with(
            sentinel.endpoint_namespace,
            sentinel.endpoint_name,
        )
        _map_endpoint_config.assert_called_once_with({"cfg2": "value2"})

        self.assertDictEqual(
            result,
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[
                        {
                            sentinel.config_section:
                                sentinel.mapped_credentials,
                        },
                        {
                            sentinel.config_section: sentinel.mapped_config,
                        },
                    ],
                ),
            },
        )

    @unittest.mock.patch("yaook.statemachine.interfaces.config_map_interface")
    @unittest.mock.patch("yaook.statemachine.interfaces.secret_interface")
    async def test_get_layer_with_custom_username(
            self,
            secret_interface,
            config_map_interface):
        kal = cue.KeystoneAuthLayer(
            target=sentinel.target,
            credentials_secret=self.credentials,
            endpoint_config=self.endpoint_cfg,
            config_section=sentinel.section,
        )

        secret_interface.return_value.read = unittest.mock.AsyncMock()
        secret_interface.return_value.read.return_value = kclient.V1Secret(
            data=api_utils.encode_secret_data({
                "key1": "value1",
            }),
        )

        config_map_interface.return_value.read = unittest.mock.AsyncMock()
        config_map_interface.return_value.read.return_value = \
            kclient.V1ConfigMap(
                data={
                    "cfg2": "value2",
                },
            )

        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _map_credentials = stack.enter_context(unittest.mock.patch.object(
                kal, "_map_credentials",
            ))
            _map_credentials.return_value = sentinel.mapped_credentials

            _map_endpoint_config = stack.enter_context(
                unittest.mock.patch.object(
                    kal, "_map_endpoint_config",
                )
            )
            _map_endpoint_config.return_value = sentinel.mapped_config

            result = await kal.get_layer(ctx)

        self.assertDictEqual(
            result,
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[
                        {
                            sentinel.section: sentinel.mapped_credentials,
                        },
                        {
                            sentinel.section: sentinel.mapped_config,
                        },
                    ],
                ),
            },
        )


class TestLabelSelectedLayer(unittest.IsolatedAsyncioTestCase):
    class LabelSelectedLayerTest(cue.LabelSelectedLayer):
        async def _get_target_labels(self, ctx):
            raise NotImplementedError

        async def _get_config_templates(self, ctx):
            return [
                cue.LabelledOverlay(
                    selectors=[api_utils.LabelSelector(match_labels={
                        "l1": "v1",
                    })],
                    configs={
                        sentinel.config_key1: sentinel.config_value1_1,
                        sentinel.unrelated: sentinel.unrelated,
                    },
                ),
                cue.LabelledOverlay(
                    selectors=[
                        api_utils.LabelSelector(match_labels={
                            "l2": "v2",
                        }),
                        api_utils.LabelSelector(match_labels={
                            "l1": "v1",
                        }),
                    ],
                    configs={
                        sentinel.config_key2: sentinel.config_value2_2,
                        sentinel.config_key1: sentinel.config_value2_1,
                        sentinel.unrelated: sentinel.unrelated,
                    },
                ),
                cue.LabelledOverlay(
                    selectors=[api_utils.LabelSelector(match_labels={
                        "l3": "v3",
                    })],
                    configs={
                        sentinel.config_key3: sentinel.config_value3_3,
                        sentinel.unrelated: sentinel.unrelated,
                    },
                ),
            ]

    def setUp(self):
        self.lsl = self.LabelSelectedLayerTest(
            target_map={
                sentinel.config_key1: sentinel.cue_package1,
                sentinel.config_key2: sentinel.cue_package2,
                sentinel.config_key3: sentinel.cue_package1,
            }
        )

    async def test_get_layer_combines_configuration_based_on_labels(self):
        with contextlib.ExitStack() as stack:
            get_target_labels = stack.enter_context(unittest.mock.patch.object(
                self.lsl, "_get_target_labels",
            ))
            get_target_labels.return_value = {
                "l1": "v1",
            }

            self.assertEqual(
                await self.lsl.get_layer(sentinel.ctx),
                {
                    sentinel.cue_package1: common_config.ConfigDeclaration(
                        flavor=common_config.OSLO_CONFIG,
                        contents=[
                            sentinel.config_value1_1,
                            sentinel.config_value2_1,
                        ],
                    ),
                    sentinel.cue_package2: common_config.ConfigDeclaration(
                        flavor=common_config.OSLO_CONFIG,
                        contents=[
                            sentinel.config_value2_2,
                        ],
                    ),
                }
            )

        get_target_labels.assert_awaited_once_with(sentinel.ctx)

        with contextlib.ExitStack() as stack:
            get_target_labels = stack.enter_context(unittest.mock.patch.object(
                self.lsl, "_get_target_labels",
            ))
            get_target_labels.return_value = {
                "l1": "v1",
                "l2": "v2",
                "l3": "v3",
            }

            self.assertDictEqual(
                await self.lsl.get_layer(sentinel.ctx),
                {
                    sentinel.cue_package1: common_config.ConfigDeclaration(
                        flavor=common_config.OSLO_CONFIG,
                        contents=[
                            sentinel.config_value1_1,
                            sentinel.config_value2_1,
                            sentinel.config_value3_3,
                        ],
                    ),
                    sentinel.cue_package2: common_config.ConfigDeclaration(
                        flavor=common_config.OSLO_CONFIG,
                        contents=[
                            sentinel.config_value2_2,
                        ],
                    ),
                }
            )


class TestNodeLabelSelectedLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.accessor = unittest.mock.Mock([])
        self.nlsl = cue.NodeLabelSelectedLayer(
            target_map={
                "key1": "some",
                "key2": "some",
                "key3": "some",
                "key4": "some",
            },
            accessor=self.accessor,
        )

    async def test__get_target_labels_retrieves_node_labels_from_instance(self):  # noqa
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            read = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock(),
            ))
            read.return_value = kclient.V1Node(
                metadata=kclient.V1ObjectMeta(
                    labels=sentinel.labels,
                ),
            )

            await self.nlsl._get_target_labels(ctx)

        read.assert_awaited_once_with(ctx.instance)

    async def test__get_config_templates_wraps_snippets_in_labelled_overlays(self):  # noqa: E501
        self.accessor.return_value = [
            {
                "nodeSelectors": [
                    {
                        "matchLabels": {
                            "l1": "v1",
                            "l2": "v1",
                            "l3": "v3",
                        },
                    },
                    {
                        "matchLabels": {
                            "l4": "v1",
                        },
                    },
                ],
                "key1": sentinel.data1_1,
                "key2": sentinel.data1_2,
                "key3": sentinel.data1_3,
                "somekeytobeignored": sentinel.ignored_value
            },
            {
                "nodeSelectors": [
                    {
                        "matchLabels": {
                            "l1": "v1",
                            "l2": "v2",
                            "l4": "v4",
                        },
                    },
                ],
                "key1": sentinel.data2_1,
                "key2": sentinel.data2_2,
                "key4": sentinel.data2_4,
            },
        ]

        self.maxDiff = None
        self.assertCountEqual(
            await self.nlsl._get_config_templates(sentinel.ctx),
            [
                cue.LabelledOverlay(
                    selectors=[
                        api_utils.LabelSelector(match_labels={
                            "l1": "v1",
                            "l2": "v1",
                            "l3": "v3",
                        }),
                        api_utils.LabelSelector(match_labels={
                            "l4": "v1",
                        }),
                    ],
                    configs={
                        "key1": sentinel.data1_1,
                        "key2": sentinel.data1_2,
                        "key3": sentinel.data1_3,
                    },
                ),
                cue.LabelledOverlay(
                    selectors=[api_utils.LabelSelector(match_labels={
                        "l1": "v1",
                        "l2": "v2",
                        "l4": "v4",
                    })],
                    configs={
                        "key1": sentinel.data2_1,
                        "key2": sentinel.data2_2,
                        "key4": sentinel.data2_4,
                    },
                ),
            ]
        )

        self.accessor.assert_called_once_with(sentinel.ctx)

    async def test__get_config_templates_raises_Exception_for_accessor(self):
        self.accessor.side_effect = Exception()

        with self.assertRaises(
                exceptions.ConfigurationInvalid):
            await self.nlsl._get_config_templates(sentinel.ctx)


class TestNodeLabelSelectedSecretInjectionLayer(
        unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.accessor = unittest.mock.Mock([])
        self.nlnlssil = cue.NodeLabelSelectedSecretInjectionLayer(
            target_map={"someConfig": "some", "someConfig2": "some"},
            accessor=self.accessor,
        )

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test__get_config_templates_injects_secrets(self, read):
        self.accessor.return_value = [
            {
                "nodeSelectors": [
                    {
                        "matchLabels": {
                            "l1": "v1",
                            "l2": "v1",
                            "l3": "v3",
                        },
                    },
                    {
                        "matchLabels": {
                            "l4": "v1",
                        },
                    },
                ],
                "someConfig": [
                    {
                        "secretName": "mysecretname",
                        "items": [
                            {
                                "key": "secretkey",
                                "path": "/DEFAULT/myconfig",
                            }
                        ]
                    }
                ],
                "configthatshouldbeignored": [],
            },
            {
                "nodeSelectors": [
                    {
                        "matchLabels": {
                            "l1": "v1",
                            "l2": "v2",
                            "l4": "v4",
                        },
                    },
                ],
                "someConfig2": [
                    {
                        "secretName": "mysecretname2",
                        "items": [
                            {
                                "key": "secretkey",
                                "path": "/DEFAULT/myotherpath",
                            }
                        ]
                    }
                ],
            },
        ]

        ctx = unittest.mock.Mock(["api_client", "namespace"])
        read.side_effect = [
            kclient.V1Secret(
                data={"secretkey": "c2VjcmV0Y29udGVudA=="}
            ),
            kclient.V1Secret(
                data={"secretkey": "b3RoZXJjb250ZW50"}
            )
        ]

        self.maxDiff = None
        self.assertCountEqual(
            await self.nlnlssil._get_config_templates(ctx),
            [
                cue.LabelledOverlay(
                    selectors=[
                        api_utils.LabelSelector(match_labels={
                            "l1": "v1",
                            "l2": "v1",
                            "l3": "v3",
                        }),
                        api_utils.LabelSelector(match_labels={
                            "l4": "v1",
                        }),
                    ],
                    configs={
                        "someConfig": {
                            "DEFAULT": {
                                "myconfig": "secretcontent",
                            }
                        },
                    },
                ),
                cue.LabelledOverlay(
                    selectors=[
                        api_utils.LabelSelector(match_labels={
                            "l1": "v1",
                            "l2": "v2",
                            "l4": "v4",
                        }),
                    ],
                    configs={
                        "someConfig2": {
                            "DEFAULT": {
                                "myotherpath": "othercontent",
                            }
                        },
                    },
                ),
            ]
        )

        self.accessor.assert_called_once_with(ctx)
        read.assert_has_calls([
            unittest.mock.call(unittest.mock.ANY, "mysecretname"),
            unittest.mock.call(unittest.mock.ANY, "mysecretname2"),
        ])


class TestSecretInjectionLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.sil = cue.SecretInjectionLayer(
            target=sentinel.target,
            accessor="subkey",
        )

    @unittest.mock.patch("yaook.statemachine.resources.get_injected_secrets")
    async def test_injects_secrets_from_spec_key(self, get_injected_secrets):
        get_injected_secrets.return_value = sentinel.secrets

        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {"subkey": sentinel.input_}

        self.assertDictEqual(
            await self.sil.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=sentinel.secrets,
                ),
            },
        )

        get_injected_secrets.assert_awaited_once_with(
            ctx,
            sentinel.input_,
        )

    @unittest.mock.patch("yaook.statemachine.resources.get_injected_secrets")
    async def test_injects_secrets_from_function(self, get_injected_secrets):
        get_injected_secrets.return_value = sentinel.secrets

        accessor = unittest.mock.Mock([])
        accessor.return_value = sentinel.input_

        self.sil = cue.SecretInjectionLayer(
            target=sentinel.target,
            accessor=accessor,
        )

        ctx = unittest.mock.Mock(["parent_spec"])

        self.assertDictEqual(
            await self.sil.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=sentinel.secrets,
                ),
            },
        )

        accessor.assert_called_once_with(ctx)

        get_injected_secrets.assert_awaited_once_with(
            ctx,
            sentinel.input_,
        )


class TestSpecLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.sl = cue.SpecLayer(
            target=sentinel.target,
            accessor="subkey",
        )

    async def test_extracts_config_from_spec_key(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {"subkey": sentinel.input_}

        self.assertDictEqual(
            await self.sl.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[sentinel.input_],
                ),
            },
        )

    async def test_defaults_from_spec_key(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {}

        self.assertDictEqual(
            await self.sl.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[{}],
                ),
            },
        )

    async def test_isolates_itself_from_mutable_inputs(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "subkey": {"key": "value"},
        }

        result = await self.sl.get_layer(ctx)
        self.assertIsNot(ctx.parent_spec["subkey"],
                         result[sentinel.target].contents)

    async def test_extracts_config_from_function(self):
        accessor = unittest.mock.Mock([])
        accessor.return_value = sentinel.input_

        self.sl = cue.SpecLayer(
            target=sentinel.target,
            accessor=accessor,
        )

        ctx = unittest.mock.Mock(["parent_spec"])

        self.assertDictEqual(
            await self.sl.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[sentinel.input_],
                ),
            },
        )

        accessor.assert_called_once_with(ctx)

    async def test_custom_flavor(self):
        accessor = unittest.mock.Mock([])
        accessor.return_value = sentinel.input_

        flavor = unittest.mock.Mock(common_config.ConfigFlavor)

        self.sl = cue.SpecLayer(
            target=sentinel.target,
            accessor=accessor,
            flavor=flavor,
        )

        flavor.declare.return_value = sentinel.declared_config

        ctx = unittest.mock.Mock(["parent_spec"])

        self.assertDictEqual(
            await self.sl.get_layer(ctx),
            {
                sentinel.target: sentinel.declared_config,
            },
        )

        flavor.declare.assert_called_once_with([sentinel.input_])
        accessor.assert_called_once_with(ctx)


class TestMetadataSecretLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.proxy_secret = \
            unittest.mock.sentinel.proxy_secret
        self.msl = cue.MetadataSecretLayer(
            proxy_secret=self.proxy_secret,
        )

    def test_is_cue_layer(self):
        self.assertIsInstance(self.msl, cue.CueLayer)

    def test_declares_dependency(self):
        self.assertIn(self.proxy_secret, self.msl.get_dependencies())

    @unittest.mock.patch("yaook.statemachine.resources.extract_password")
    async def test_injects_compute_and_placement_passwords(
            self,
            extract_password):
        ctx = sentinel.ctx

        extract_password.return_value = sentinel.metadata_secret

        result = await self.msl.get_layer(ctx)

        extract_password.assert_awaited_once_with(
            ctx, self.proxy_secret,
        )

        self.assertEqual(
            result,
            {"neutron_metadata_agent":
                common_config.OSLO_CONFIG.declare([{
                    "DEFAULT": {
                        "metadata_proxy_shared_secret":
                            sentinel.metadata_secret,
                    },
                }])},
        )


class TestRegionNameConfigLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.pcl = cue.RegionNameConfigLayer(
            target=sentinel.target,
            config_section=sentinel.config_section,
        )

    async def test_get_layer_returns_region_name_config_layer(
            self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "region": {"name": sentinel.os_region_name},
        }

        self.assertEqual(
            await self.pcl.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[{
                        sentinel.config_section: {
                            "os_region_name": sentinel.os_region_name,
                            "region_name": sentinel.os_region_name,
                        }
                    }],
                ),
            },
        )

    async def test_make_region_name_config_overlay(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "region": {"name": sentinel.os_region_name},
        }
        result = self.pcl.make_region_name_config_overlay(ctx)

        self.assertEqual(
            result,
            {
                sentinel.config_section: {
                    "os_region_name": sentinel.os_region_name,
                    "region_name": sentinel.os_region_name,
                },
            },
        )

    async def test_get_layer_returns_default_region_name(
            self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {}
        self.pcl.allow_not_defined = True

        self.assertEqual(
            await self.pcl.get_layer(ctx),
            {
                sentinel.target: common_config.ConfigDeclaration(
                    flavor=common_config.OSLO_CONFIG,
                    contents=[{}],
                ),
            },
        )
