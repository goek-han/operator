#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import unittest
import unittest.mock

import kubernetes_asyncio.client

from . import testutils


class TestApiClientMock(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.mock = testutils.ApiClientMock()
        self.api_client = self.mock.client

    async def asyncTearDown(self):
        await self.api_client.close()

    def test__parse_url(self):
        self.assertEqual(
            self.mock._parse_url(
                "http://localhost/api/v1/namespaces/namespace/pods"
            ),
            testutils.URLInfo(
                namespace="namespace",
                api_group="",
                api_group_version="v1",
                plural="pods",
                name=None,
                subresource=None,
            ),
        )
        self.assertEqual(
            self.mock._parse_url(
                "http://localhost/apis/apps/v1/namespaces/some_namespace/"
                "deployments/name"
            ),
            testutils.URLInfo(
                namespace="some_namespace",
                api_group="apps",
                api_group_version="v1",
                plural="deployments",
                name="name",
                subresource=None,
            ),
        )
        self.assertEqual(
            self.mock._parse_url(
                "http://localhost/apis/api_group/v1/objects"
            ),
            testutils.URLInfo(
                namespace=None,
                api_group="api_group",
                api_group_version="v1",
                plural="objects",
                name=None,
                subresource=None,
            ),
        )
        self.assertEqual(
            self.mock._parse_url(
                "http://localhost/apis/api_group/v1/namespaces/namespace/"
                "objects"
            ),
            testutils.URLInfo(
                namespace="namespace",
                api_group="api_group",
                api_group_version="v1",
                plural="objects",
                name=None,
                subresource=None,
            ),
        )

    async def test_does_not_crash_on_simple_v1_listing(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.list_namespaced_pod("namespace")

    async def test_create_and_read_back_pod(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertIsInstance(readback, kubernetes_asyncio.client.V1Pod)
        self.assertEqual(readback.spec.containers, [])

        readback = await v1.list_namespaced_pod("namespace")
        self.assertEqual(readback.items[0].metadata.name, "foo")
        self.assertIsNotNone(readback.metadata.resource_version)

    async def test_create_returns_resource_version(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })
        self.assertIsNotNone(result.metadata.resource_version)

    async def test_readback_does_not_return_objects_from_other_namespaces(
            self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })

        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.read_namespaced_pod("foo", "other_namespace")

        self.assertEqual(info.exception.status, 404)

        readback = await v1.list_namespaced_pod("other_namespace")
        self.assertEqual(readback.items, [])

    async def test_list_all_namespaces(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_pod_for_all_namespaces()
        self.assertEqual(readback.items[0].metadata.name, "foo")

    async def test_list_by_simple_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                    "label2": "v2",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "baz",
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label1=v1",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo", "bar"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label1=v1,label2=v1",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label1=v1,label2=v2",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label2=v1",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label2=v2",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar"],
        )

    async def test_list_by_existence_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                    "label2": "v2",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label2",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo", "bar"],
        )

    async def test_list_by_non_existence_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="!label2",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar"],
        )

    async def test_list_by_IN_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v2",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "baz",
                "labels": {
                    "label1": "v3",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label1 in (v1,v2)",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo", "bar"],
        )

    async def test_list_by_NOTIN_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v2",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "baz",
                "labels": {
                    "label1": "v3",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            label_selector="label1 notin (v1,v2)",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["baz"],
        )

    async def test_list_by_field_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "baz",
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
            field_selector="metadata.name==bar",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            field_selector="metadata.name=bar",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar"],
        )

        readback = await v1.list_namespaced_pod(
            "namespace",
            field_selector="metadata.name!=bar",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo", "baz"],
        )

    async def test_patch_with_json_patch(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.patch_namespaced_pod("foo", "namespace", [
            {"op": "add", "path": "/metadata/labels/xyz", "value": "abc"},
        ])

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.labels["xyz"], "abc")

    async def test_status_patch_with_json_patch_only_patches_status(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.patch_namespaced_pod_status("foo", "namespace", [
            {"op": "add", "path": "/metadata/labels/xyz", "value": "abc"},
            {"op": "add", "path": "/status", "value": {"phase": "Running"}},
        ])

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.status.phase, "Running")
        self.assertNotIn("xyz", readback.metadata.labels)

    async def test_patch_with_replacement(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.patch_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                    "xyz": "abc",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.labels["xyz"], "abc")

    async def test_patch_with_replacement_cannot_change_namespace(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.patch_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                    "xyz": "abc",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.labels["xyz"], "abc")
        self.assertEqual(readback.metadata.namespace, "namespace")

    async def test_patch_with_replacement_cannot_change_name(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.patch_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                    "xyz": "abc",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.name, "foo")

    async def test_patch_rejects_with_409_if_resource_version_mismatches(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.patch_namespaced_pod("foo", "namespace", {
                "apiVersion": "v1",
                "kind": "Pod",
                "metadata": {
                    "name": "foo",
                    "resourceVersion": "invaild",
                    "labels": {
                        "label2": "v1",
                        "xyz": "abc",
                    }
                },
                "spec": {
                    "containers": [],
                }
            })

        self.assertEqual(info.exception.status, 409)

    async def test_patch_creates_if_not_exists(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.patch_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.labels["label1"], "v1")

    async def test_replace_rejects_with_404_if_objects_does_not_exist(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.replace_namespaced_pod("foo", "namespace", {
                "apiVersion": "v1",
                "kind": "Pod",
                "metadata": {
                    "name": "foo",
                    "labels": {
                        "label2": "v1",
                        "xyz": "abc",
                    }
                },
                "spec": {
                    "containers": [],
                }
            })

        self.assertEqual(info.exception.status, 404)

    async def test_replace_rejects_with_409_if_resource_version_mismatches(self):  # NOQA
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.replace_namespaced_pod("foo", "namespace", {
                "apiVersion": "v1",
                "kind": "Pod",
                "metadata": {
                    "name": "foo",
                    "resourceVersion": "invaild",
                    "labels": {
                        "label2": "v1",
                        "xyz": "abc",
                    }
                },
                "spec": {
                    "containers": [],
                }
            })

        self.assertEqual(info.exception.status, 409)

    async def test_replace_replaces(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.replace_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label2": "v1",
                    "xyz": "abc",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.metadata.labels["xyz"], "abc")

    async def test_replace_cannot_replace_uid(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        created = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.replace_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label2": "v1",
                    "xyz": "abc",
                },
                "uid": "foo",
            },
            "spec": {
                "containers": [],
            }
        })

        self.assertEqual(readback.metadata.uid, created.metadata.uid)

    async def test_replace_cannot_replace_namespace(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        created = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.replace_namespaced_pod("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label2": "v1",
                    "xyz": "abc",
                },
                "uid": "foo",
            },
            "spec": {
                "containers": [],
            }
        })

        self.assertEqual(readback.metadata.namespace,
                         created.metadata.namespace)

    async def test_replace_status_only_replaces_status(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
                "restartPolicy": "Always",
            }
        })

        await v1.replace_namespaced_pod_status("foo", "namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label2": "v1",
                    "xyz": "abc",
                }
            },
            "spec": {
                "containers": [],
                "restartPolicy": "Never",
            },
            "status": {
                "phase": "Running",
            },
        })

        readback = await v1.read_namespaced_pod("foo", "namespace")
        self.assertEqual(readback.status.phase, "Running")
        self.assertEqual(readback.spec.restart_policy, "Always")
        self.assertNotIn("xyz", readback.metadata.labels)

    async def test_hook_create(self):
        def identity(v, **kwargs):
            v = copy.deepcopy(v)
            v["metadata"].setdefault("annotations", {})["hooked"] = "true"
            return v

        hook = unittest.mock.Mock()
        hook.side_effect = identity

        self.mock.add_hook(
            "batch", "v1", "jobs", "POST",
            hook,
        )

        request_body = {
            "apiVersion": "batch/v1",
            "kind": "Job",
            "metadata": {
                "name": "foo",
                "namespace": "namespace",
            },
            "spec": {
                "template": {},
            }
        }

        request_body_with_uid = copy.deepcopy(request_body)
        request_body_with_uid["metadata"]["uid"] = unittest.mock.ANY

        batchv1 = kubernetes_asyncio.client.BatchV1Api(self.api_client)
        response = await batchv1.create_namespaced_job(
            "namespace",
            request_body,
        )

        hook.assert_called_once_with(
            request_body_with_uid,
            namespace="namespace",
        )

        self.assertEqual(response.metadata.annotations,
                         {"hooked": "true"})

        response = await batchv1.read_namespaced_job(
            "foo",
            "namespace",
        )

        self.assertEqual(response.metadata.annotations,
                         {"hooked": "true"})

    async def test_hook_patch(self):
        def identity(v, **kwargs):
            v = copy.deepcopy(v)
            v["metadata"].setdefault("annotations", {})["hooked"] = "true"
            return v

        hook = unittest.mock.Mock()
        hook.side_effect = identity

        self.mock.add_hook(
            "batch", "v1", "jobs", "PATCH",
            hook,
        )

        request_body = {
            "apiVersion": "batch/v1",
            "kind": "Job",
            "metadata": {
                "name": "foo",
                "namespace": "namespace",
            },
            "spec": {
                "template": {},
            }
        }

        request_body_with_uid = copy.deepcopy(request_body)
        request_body_with_uid["metadata"]["uid"] = unittest.mock.ANY

        batchv1 = kubernetes_asyncio.client.BatchV1Api(self.api_client)
        response = await batchv1.create_namespaced_job(
            "namespace",
            request_body,
        )

        hook.assert_not_called()

        self.assertIsNone(response.metadata.annotations)

        request_body2 = {
            "apiVersion": "batch/v1",
            "kind": "Job",
            "metadata": {
                "name": "foo",
                "namespace": "namespace",
            },
            "spec": {
                "template": {
                    "spec": {
                        "containers": [],
                    },
                },
            }
        }

        request_body2_with_uid = copy.deepcopy(request_body2)
        request_body2_with_uid["metadata"]["uid"] = unittest.mock.ANY

        response = await batchv1.patch_namespaced_job(
            "foo", "namespace",
            request_body2,
        )

        hook.assert_called_once_with(
            request_body2_with_uid,
            old_body=request_body_with_uid,
        )

        response = await batchv1.read_namespaced_job(
            "foo",
            "namespace",
        )

        self.assertEqual(response.metadata.annotations,
                         {"hooked": "true"})

    async def test_create_with_generate_name(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"generateName": "foo-"},
            "spec": {
                "containers": [],
            }
        })

        name = result.metadata.name
        self.assertNotEqual(name, "foo-")
        self.assertTrue(name.startswith("foo-"))

        readback = await v1.read_namespaced_pod(name, "namespace")
        self.assertEqual(readback.metadata.name, name)

    async def test_crud(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"generateName": "foo-"},
            "spec": {
                "containers": [],
            }
        })

        name = result.metadata.name

        readback = await v1.read_namespaced_pod(name, "namespace")
        self.assertEqual(readback.metadata.name, name)

        updated = await v1.patch_namespaced_pod(
            name, "namespace",
            [{"op": "add", "path": "/status", "value": {"phase": "Pending"}}],
        )
        self.assertEqual(updated.status.phase, "Pending")

        readback = await v1.read_namespaced_pod(name, "namespace")
        self.assertEqual(readback.status.phase, "Pending")

        await v1.delete_namespaced_pod(name, "namespace")

        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.read_namespaced_pod(name, "namespace")

        self.assertEqual(info.exception.status, 404)

    async def test_crud_status(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"generateName": "foo-"},
            "spec": {
                "containers": [],
            },
            "status": {
                "phase": "Init",
            }
        })

        name = result.metadata.name

        readback = await v1.read_namespaced_pod_status(name, "namespace")
        self.assertEqual(readback.metadata.name, name)
        self.assertEqual(readback.status.phase, "Init")

        updated = await v1.patch_namespaced_pod_status(
            name, "namespace",
            [{"op": "add", "path": "/status", "value": {"phase": "Pending"}}],
        )
        self.assertEqual(updated.status.phase, "Pending")

        readback = await v1.read_namespaced_pod_status(name, "namespace")
        self.assertEqual(readback.status.phase, "Pending")

    async def test_patch_cannot_change_uid(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"generateName": "foo-"},
            "spec": {
                "containers": [],
            }
        })

        name = result.metadata.name

        readback = await v1.read_namespaced_pod(name, "namespace")
        self.assertEqual(readback.metadata.name, name)

        updated = await v1.patch_namespaced_pod(
            name, "namespace",
            [{"op": "add", "path": "/metadata/uid", "value": "foo"}],
        )
        self.assertEqual(updated.metadata.uid, readback.metadata.uid)

    async def test_create_fills_in_namespace(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("some-namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })
        self.assertEqual(result.metadata.namespace, "some-namespace")

        readback = await v1.read_namespaced_pod("foo", "some-namespace")
        self.assertEqual(readback.metadata.namespace, "some-namespace")

    async def test_create_fills_in_uid(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        result = await v1.create_namespaced_pod("some-namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {"name": "foo"},
            "spec": {
                "containers": [],
            }
        })
        self.assertIsNotNone(result.metadata.uid)

        readback = await v1.read_namespaced_pod("foo", "some-namespace")
        self.assertEqual(readback.metadata.uid, result.metadata.uid)

    async def test_delete_on_non_existant_object_raises_404(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        with self.assertRaises(kubernetes_asyncio.client.ApiException) as info:
            await v1.delete_namespaced_pod("foo", "namespace")
        self.assertEqual(info.exception.status, 404)

    async def test_delete_collection_by_label_selector(self):
        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "foo",
                "labels": {
                    "label1": "v1",
                    "label2": "v1",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "bar",
                "labels": {
                    "label1": "v1",
                    "label2": "v2",
                }
            },
            "spec": {
                "containers": [],
            }
        })

        await v1.create_namespaced_pod("namespace", {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "name": "baz",
            },
            "spec": {
                "containers": [],
            }
        })

        readback = await v1.list_namespaced_pod(
            "namespace",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["foo", "bar", "baz"],
        )

        await v1.delete_collection_namespaced_pod(
            "namespace",
            label_selector="label1=v1,label2=v1",
        )
        readback = await v1.list_namespaced_pod(
            "namespace",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["bar", "baz"],
        )

        await v1.delete_collection_namespaced_pod(
            "namespace",
            label_selector="label1=v1",
        )
        readback = await v1.list_namespaced_pod(
            "namespace",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            ["baz"],
        )

        await v1.delete_collection_namespaced_pod(
            "namespace",
        )
        readback = await v1.list_namespaced_pod(
            "namespace",
        )
        self.assertEqual(
            [pod.metadata.name for pod in readback.items],
            [],
        )

    async def test_provides_version(self):
        ver = kubernetes_asyncio.client.VersionApi(self.api_client)
        version = await ver.get_code()
        self.assertEqual("1", version.major)
        self.assertEqual("21", version.minor)
