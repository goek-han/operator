#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import configparser
import dataclasses
import io
import ipaddress
import copy
import json
import logging
import re
import secrets
import typing
import unittest.mock
import urllib.parse
import uuid

import jsonpatch

import kubernetes_asyncio.client

import yaook.op.common
import yaook.op.daemon
import yaook.statemachine
import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.version_utils as version_utils


CATEST_SKIP_ANNOTATION = f"{context.TEST_ANNOTATION_PREFIX}/skip-ca-certs"
PDBTEST_SKIP_ANNOTATION = f"{context.TEST_ANNOTATION_PREFIX}/skip-pdb"


DEMO_CA_CERTIFICATE = """-----BEGIN CERTIFICATE-----
MIIFZTCCA02gAwIBAgIUVZtvTblE3gwv5o6+OaEHdun9OBQwDQYJKoZIhvcNAQEL
BQAwQjELMAkGA1UEBhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UE
CgwTRGVmYXVsdCBDb21wYW55IEx0ZDAeFw0yMTAxMDQwOTUyMTVaFw0yMjAxMDQw
OTUyMTVaMEIxCzAJBgNVBAYTAlhYMRUwEwYDVQQHDAxEZWZhdWx0IENpdHkxHDAa
BgNVBAoME0RlZmF1bHQgQ29tcGFueSBMdGQwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQDF40f+Vs3KdMZbOKez+7BagAZfd7OjDeFFcRpNvFoc+S3qJh/b
67/w7KuehSkI024CBLRO4nBZW+WqbLgbs11bW4bpqeiw3rxxIn7ekKIUsLE0dkQ3
t1UnQmesPtYTZop6fcy4RDqgK5nHk/cjyD3TtiJTxzF/8gbzA1Z96yxq5xtmn8fp
Hs9pDZJERcrqxi92SfGzF02ScI+YCSQIcJkwixKHfdX7ycFITtDcIdcIskc9QI1G
2rv3W729D/cCH4xJNtKqEDQlmZnUYyKwr+Kftx6wjtzw1Tcx+c5TvlBDd2ZbapS4
I2SLgXdDayz23mtkhBv9ydRhivxqv1AtmH87gxBcR1pQoz1VLD8viTNmlgZXmexY
wwI+Uql/qO2H1CrhPq0SFGLToD41UysNiRXdD3FwRHqVtmRzFj4cKWw1FHTM4H+Z
yN+CagOu96AvfN3++VtLBNFs71ypMwcvEkrEjs8lrJf28i27w5BAyELfMj6jq6QD
x7EozL7TWDc2KEFzZSwVPgeEJo37j3qgZwZJzndVAFNSfmEsRDZ80aypjnhCOBpI
SEhROCI63IUzE0RrBLczh13pgWIt1w5rZERpBItALaFAG5RHvDcCo06gAnZMrk8z
Wc3pwBzeJz2L8n6LDTMO3KF+EHKB4BOSXOesewJdBl9H/P6aGGzsxwKObQIDAQAB
o1MwUTAdBgNVHQ4EFgQUW4Cy7rOBKWgkN8df9t3Y5p6IJsMwHwYDVR0jBBgwFoAU
W4Cy7rOBKWgkN8df9t3Y5p6IJsMwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B
AQsFAAOCAgEAcYeV5rfAK+m+X4MRbMyEHcFmhDPD4k5YB9z/YQ/rkrcEZMTpVzEf
Nzy0Zr6e/x5Op8ZHzxeovPSREQdhfd06p+jSXDO3bOu9QiePL+nHSe8ujO8HkFnT
BzuEscmN4ITmSeiUJwaBNGgE38wq3a4sluhsbWtBlJSM7g17RKniW80wZ9tzH8gS
rLWGxNjSGVk/WI0TM9Xdc0Zi8JgrzJLsdsAuBtwaH25SLnlha1DCgLzQFgvX7CpG
QsHDvT034tpkcD+ldiRx2XZ8gutQvauCQQSvsAxEJAr51QX764thT4t/l4Qy2I/8
s/BvJ0KndTcb3i0t3gyv6kLnlgX68Y4lwkROUd4KU0jQg2YfziSBJtPkk7KlT0MO
BFRxJsZK5toAYp5LN2KXxP7Yq/mAlWsEt/9AvWc2Dx1iLXNil5AqBue2phCY4iSU
ZJGFuPAmIcOPjchTrV8f0ErLlTtBtur62v994X/bpY0EpuywXf9ebjISyKZ4BeZi
GFr1v5YVh2wX8alNHPgnu1VJkuIAGNrSgAY0LwGr0Y0SEvnrmJzreCW3lwf42hsd
OEWbPpAJDON39uRwWK8PmwuxISovyFM3ORRFdu2Nlw676ua8tS6DqNfvkQVHtgu8
y29QiKpnk5obrRbkoJI219QsW+L8Mc8Ijf2M/a+WXlirsbAhjy/JKD0=
-----END CERTIFICATE-----"""


LABEL_IGNORE_DURING_TESTS = "tests.yaook.cloud/ignore-me"


def _skip_test_resources(resources, skip_annotation=None):
    for name, body in resources.items():
        if LABEL_IGNORE_DURING_TESTS in body.get(
                "metadata", {}).get("labels", {}):
            continue
        if skip_annotation is not None and skip_annotation in body.get(
                "metadata", {}).get("annotations", {}):
            continue
        yield name, body


def _generate_name(prefix):
    return prefix + secrets.token_hex(8)


def _parse_config(s, *, decode=False):
    if decode:
        s = base64.b64decode(s.encode("ascii")).decode("utf-8")
    cfg = configparser.ConfigParser(
        # XXX: oslo_config uses a completely different parser, but it is close
        # enough to work in our tests.
        interpolation=configparser.ExtendedInterpolation()
    )
    cfg.read_file(io.StringIO(s))
    return cfg


@dataclasses.dataclass(frozen=True)
class URLInfo:
    api_group: str
    api_group_version: str
    plural: str
    namespace: typing.Optional[str]
    name: typing.Optional[str]
    subresource: typing.Optional[str]

    @property
    def object_type_key(self):
        return self.api_group, self.api_group_version, self.plural


class FieldSelector:
    def __init__(self, value=""):
        self.filters = []
        elements = [x for x in value.split(",") if x]
        for element in elements:
            if "!=" in element:
                key, value = element.split("!=")
                def filter(v): return v != value
            else:
                key, value = [p for p in element.split("=") if p]
                def filter(v): return v == value
            keypath = key.split(".")
            self.filters.append((keypath, filter))

    @staticmethod
    def _iterate_filter(data, keypath, filter):
        if len(keypath) == 0:
            return filter(data)
        return FieldSelector._iterate_filter(
            data.get(keypath[0]), keypath[1:], filter)

    def object_matches(self, data):
        for keypath, filter in self.filters:
            if not FieldSelector._iterate_filter(data, keypath, filter):
                return False
        return True


class ApiClientMock:
    def __init__(self):
        super().__init__()
        self.client = kubernetes_asyncio.client.ApiClient()
        self._request_mock = unittest.mock.AsyncMock()
        self._request_mock.side_effect = self._request
        self.client.request = self._request_mock
        self._objects = {}
        self._hooks = {}
        self._resource_version = 1

    @property
    def mock_calls(self):
        return self._request_mock.mock_calls

    def get_namespaced_objects(self, api_group, api_group_version, plural,
                               namespace):
        return self._objects.get(
            (api_group, api_group_version, plural), {}
        ).get(namespace, {})

    def get_cluster_objects(self, api_group, api_group_version, plural):
        return self._objects.get(
            (api_group, api_group_version, plural), {}
        ).get(None, {})

    def get_all_objects(self, api_group, api_group_version, plural):
        return {
            f"{namespace or ''}/{name}": object_
            for namespace, objects in self._objects.get(
                (api_group, api_group_version, plural), {}
            ).items()
            for name, object_ in objects.items()
        }

    def add_hook(self, api_group, api_group_version, plural, method, func):
        self._hooks.setdefault(
            (api_group, api_group_version, plural),
            {},
        ).setdefault(method, []).append(func)

    def remove_hook(self, api_group, api_group_version, plural, method, func):
        self._hooks.setdefault(
            (api_group, api_group_version, plural),
            {},
        ).setdefault(method, []).remove(func)

    def _make_response(self, content_type, data):
        def getheader(name):
            if name.lower() == "content-type":
                return content_type
            raise NotImplementedError

        response = unittest.mock.Mock(["getheader", "status"])
        response.getheader.side_effect = getheader
        response.status = 200
        response.data = data
        return response

    def _make_json_response(self, data):
        try:
            data_serialized = json.dumps(data).encode("utf-8")
        except TypeError:
            raise ValueError(
                f"failed to encode response as JSON: {data!r}",
            )
        return self._make_response("application/json; charset=utf-8",
                                   data_serialized)

    def _parse_url(self, url_str: str) -> URLInfo:
        url = urllib.parse.urlparse(url_str)
        segments = url.path.split("/")[1:]
        if segments[0] == "api":
            # core API
            api_group = ""
            segments = segments[1:]
        elif segments[0] == "apis":
            api_group = segments[1]
            segments = segments[2:]
        else:
            raise ValueError(f"unsupported url path: {url.path!r}")

        api_group_version = segments[0]
        if segments[1] == "namespaces":
            namespace = segments[2]
            segments = segments[3:]
        else:
            namespace = None
            segments = segments[1:]
        plural = segments[0]
        segments = segments[1:]
        if len(segments) == 2:
            name, subresource = segments
        elif segments:
            name, = segments
            subresource = None
        else:
            name = None
            subresource = None

        return URLInfo(
            api_group=api_group,
            api_group_version=api_group_version,
            plural=plural,
            namespace=namespace,
            name=name,
            subresource=subresource,
        )

    def _parse_label_selector(
            self,
            selector: str,
    ) -> yaook.statemachine.LabelSelector:
        # This splits `selector` on all `,` that are not inside parentheses
        # So "a,b (c,d, e),foo" is split to `["a", "b (c,d, e)", "foo"]`
        expressions = re.split(r',(?![^(]+\))', selector)
        match_expressions = []
        for expr in expressions:
            if "=" in expr:
                lhs, rhs = expr.split("=", 1)
                match_expressions.append(
                    yaook.statemachine.LabelExpression(
                        key=lhs,
                        operator=yaook.statemachine.SelectorOperator.IN,
                        values=[rhs],
                    ),
                )
            elif " " in expr:
                lhs, op, rhs = expr.split(" ", 2)
                if op == "in":
                    operator = yaook.statemachine.SelectorOperator.IN
                elif op == "notin":
                    operator = yaook.statemachine.SelectorOperator.NOT_IN
                else:
                    raise ValueError(
                        "operator mut be in or notin, but is %s" % op)
                values = [x.strip() for x in rhs.strip("()").split(",")]
                match_expressions.append(
                    yaook.statemachine.LabelExpression(
                        key=lhs,
                        operator=operator,
                        values=values,
                    )
                )
            else:
                if expr.startswith("!"):
                    operator = yaook.statemachine.SelectorOperator.NOT_EXISTS
                    expr = expr[1:]
                else:
                    operator = yaook.statemachine.SelectorOperator.EXISTS
                match_expressions.append(
                    yaook.statemachine.LabelExpression(
                        key=expr,
                        operator=operator,
                        values=None,
                    ),
                )
        return yaook.statemachine.LabelSelector(
            match_expressions=match_expressions,
        )

    def _make_error_response(self, status, body="unknown error"):
        exc = kubernetes_asyncio.client.exceptions.ApiException(
            status=status,
        )
        exc.body = body.encode("utf-8")
        raise exc

    def _apply_hooks(
            self, api_group, api_group_version, plural, method, body,
            hook_kwargs):
        hooks = self._hooks.get(
            (api_group, api_group_version, plural), {}
        ).get(method, [])

        for hook in hooks:
            body = hook(body, **hook_kwargs)
            body = copy.deepcopy(body)

        return body

    def _create_request(self, url_info, body):
        try:
            prefix = body["metadata"]["generateName"]
        except KeyError:
            name = body["metadata"]["name"]
        else:
            while True:
                name = _generate_name(prefix)
                try:
                    self.get_object(
                        *url_info.object_type_key,
                        url_info.namespace,
                        name,
                    )
                except KeyError:
                    break
            body["metadata"]["name"] = name

        body["metadata"]["namespace"] = url_info.namespace
        body["metadata"]["uid"] = str(uuid.uuid4())

        url_info = dataclasses.replace(url_info, name=name)
        body = self._apply_hooks(
            *url_info.object_type_key,
            "POST",
            body,
            {
                "namespace": url_info.namespace,
            },
        )
        self.put_object(*url_info.object_type_key,
                        url_info.namespace,
                        url_info.name,
                        body)
        body = copy.deepcopy(body)
        body.setdefault("metadata", {})["resourceVersion"] = \
            str(self._resource_version)
        return self._make_json_response(body)

    def get_object(self, api_group, api_group_version, plural, namespace,
                   name):
        objects = self._objects[api_group, api_group_version, plural]
        namespaced = objects[namespace]
        return namespaced[name]

    def put_object(self, api_group, api_group_version, plural, namespace,
                   name, body):
        self._objects.setdefault(
            (api_group, api_group_version, plural), {}
        ).setdefault(namespace, {})[name] = body
        self._resource_version += 1

    def del_object(self, api_group, api_group_version, plural, namespace,
                   name):
        del self._objects.setdefault(
            (api_group, api_group_version, plural), {}
        ).setdefault(namespace, {})[name]

    def _version_request(self):
        return self._make_json_response(
            {
                "major": "1",
                "minor": "21",
                "gitVersion": "wedontcare",
                "gitCommit": "wedontcare",
                "gitTreeState": "wedontcare",
                "buildDate": "wedontcare",
                "goVersion": "wedontcare",
                "compiler": "wedontcare",
                "platform": "wedontcare"
            }
        )

    def _read_request(self, url_info):
        try:
            body = self.get_object(
                *url_info.object_type_key,
                url_info.namespace,
                url_info.name,
            )
        except KeyError:
            return self._make_error_response(404)
        body = copy.deepcopy(body)
        body.setdefault("metadata", {})["resourceVersion"] = \
            str(self._resource_version)
        return self._make_json_response(body)

    def _list_request(self, url_info, query_params):
        label_selector = yaook.statemachine.LabelSelector(
            match_labels={}
        )
        field_selector = FieldSelector()

        for name, value in query_params:
            if name == "labelSelector":
                label_selector = self._parse_label_selector(value)
            if name == "fieldSelector":
                field_selector = FieldSelector(value)

        if url_info.namespace is None:
            objects = self.get_all_objects(
                *url_info.object_type_key
            ).values()
        else:
            objects = self.get_namespaced_objects(
                *url_info.object_type_key, url_info.namespace,
            ).values()

        items = [
            item
            for item in objects
            if label_selector.object_matches(
                item.get("metadata", {}).get("labels", {})
            ) and field_selector.object_matches(item)
        ]

        return self._make_json_response({
            "items": items,
            "metadata": {
                "resourceVersion": str(self._resource_version),
            },
        })

    def _patch_request(self, url_info, content_type, request_body):
        try:
            body = self.get_object(
                *url_info.object_type_key,
                url_info.namespace,
                url_info.name,
            )
        except KeyError:
            body = None
        if 'kind' in request_body and \
                request_body['kind'][:-1].lower() not in url_info.plural:

            error_message = "Conflict: {} is not part of {}"\
              .format(request_body['kind'], url_info.plural)
            raise self._make_error_response(500, error_message)

        if content_type == "application/json-patch+json":
            if isinstance(request_body, list):
                patch = jsonpatch.JsonPatch(request_body)
                if body is None:
                    return self._make_error_response(404)
                new_body = patch.apply(body)
            else:
                version = request_body.get("metadata", {}).get(
                    "resourceVersion",
                    None,
                )
                if version is not None:
                    if version != str(self._resource_version):
                        return self._make_error_response(409)
                new_body = request_body
        else:
            return self._make_error_response(415, "unsupported content type")

        if body:
            new_body["metadata"]["uid"] = body["metadata"]["uid"]
        else:
            new_body["metadata"]["uid"] = str(uuid.uuid4())
        new_body["metadata"]["namespace"] = url_info.namespace
        new_body["metadata"]["name"] = url_info.name
        new_body = self._apply_hooks(
            *url_info.object_type_key,
            "PATCH",
            new_body,
            {
                "old_body": body,
            },
        )

        new_resource = new_body
        if url_info.subresource is not None:
            new_resource = copy.deepcopy(body)
            new_resource[url_info.subresource] = \
                new_body[url_info.subresource]

        self.put_object(
            *url_info.object_type_key,
            url_info.namespace,
            url_info.name,
            new_resource,
        )
        return self._make_json_response(new_resource)

    def _replace_request(self, url_info, request_body):
        try:
            existing = self.get_object(
                *url_info.object_type_key,
                url_info.namespace,
                url_info.name,
            )
        except KeyError:
            return self._make_error_response(404)

        version = request_body.get(
            "metadata", {}
        ).get("resourceVersion", None)
        if version is not None:
            if version != str(self._resource_version):
                return self._make_error_response(409)

        request_body["metadata"]["uid"] = existing["metadata"]["uid"]
        request_body["metadata"]["namespace"] = \
            existing["metadata"]["namespace"]

        new_resource = request_body
        if url_info.subresource is not None:
            new_resource = copy.deepcopy(existing)
            new_resource[url_info.subresource] = \
                request_body[url_info.subresource]

        self.put_object(
            *url_info.object_type_key,
            url_info.namespace,
            url_info.name,
            new_resource,
        )
        return self._make_json_response(new_resource)

    def _delete_request(self, url_info):
        if url_info.subresource:
            return self._make_error_response(400)

        try:
            self.del_object(
                *url_info.object_type_key,
                url_info.namespace,
                url_info.name,
            )
        except KeyError:
            return self._make_error_response(404)
        return self._make_json_response({})

    def _delete_collection_request(self, url_info, query_params):
        if url_info.subresource:
            return self._make_error_response(400)
        selector = yaook.statemachine.LabelSelector(
            match_labels={}
        )

        for name, value in query_params:
            if name == "labelSelector":
                selector = self._parse_label_selector(value)

        if url_info.namespace is None:
            objects = self.get_all_objects(
                *url_info.object_type_key
            ).values()
        else:
            objects = self.get_namespaced_objects(
                *url_info.object_type_key, url_info.namespace,
            ).values()

        items = [
            item
            for item in objects
            if selector.object_matches(
                item.get("metadata", {}).get("labels", {})
            )
        ]

        for item in items:
            self.del_object(
                *url_info.object_type_key,
                item["metadata"]["namespace"],
                item["metadata"]["name"],
            )
        return self._make_json_response({})

    async def _request(self, method, url, *, headers, query_params, body,
                       **kwargs):
        if isinstance(body, bytes):
            body = json.loads(body.decode("utf-8"))
        if "/version" in url:
            return self._version_request()
        url_info = self._parse_url(url)
        if method == "POST":
            # isolate the request from the storage
            body = copy.deepcopy(body)
            return self._create_request(url_info, body)
        elif method == "GET":
            if url_info.name is not None:
                return self._read_request(url_info)
            else:
                return self._list_request(url_info, query_params)
        elif method == "PUT":
            body = copy.deepcopy(body)
            return self._replace_request(
                url_info, body,
            )
        elif method == "PATCH":
            # isolate the request from the storage
            body = copy.deepcopy(body)
            return self._patch_request(url_info, headers["Content-Type"],
                                       body)
        elif method == "DELETE":
            if url_info.name is not None:
                return self._delete_request(url_info)
            else:
                return self._delete_collection_request(url_info, query_params)
        return self._make_error_response(500, body="method not implemented")

    def _dump_object(self, obj):
        LIMIT = 8192
        obj = copy.deepcopy(obj)
        # we start with removing apiVersion, kind and metadata/name+namespace,
        # because it's already given in the tree view
        obj.get("metadata", {}).pop("name", None)
        obj.get("metadata", {}).pop("namespace", None)
        obj.pop("apiVersion", None)
        obj.pop("kind", None)
        dump = json.dumps(obj)
        # arbitrary limit
        if len(dump) <= LIMIT:
            return dump
        for suspect_key in ["spec", "data"]:
            if suspect_key in obj:
                obj[suspect_key] = \
                    str(unittest.mock.sentinel.removed_for_readability)
        dump = json.dumps(obj)
        if len(dump) <= LIMIT:
            return dump
        return str(unittest.mock.sentinel.removed_for_readability)

    def _dump_call(self, call):
        LIMIT = 4096
        KEYS_TO_REMOVE = [
            ["_preload_content", "_request_timeout", "post_params"],
            ["headers"], ["query_params"], ["body"],
        ]
        dump = str(call)
        for to_remove in KEYS_TO_REMOVE:
            if len(dump) <= LIMIT:
                return dump
            fn, args, kwargs = call
            for suspect_key in to_remove:
                if kwargs.pop(suspect_key, None) is not None:
                    kwargs.setdefault(
                        "some_args",
                        unittest.mock.sentinel.removed_for_readability,
                    )

            call = unittest.mock.call(*args, **kwargs)
            dump = str(call)
        if len(dump) <= LIMIT:
            return dump
        _, args, kwargs = call
        return unittest.mock.call(
            *args[:1],
            ...,
            unittest.mock.sentinel.removed_for_readability
        )

    def dump(self, logger):
        CONT_CHAR = "│ "
        NODE_CHAR = "├└"

        dump = ["", "  === API Requests ==="]
        for call in self.mock_calls:
            dump.append(f"      {self._dump_call(call)}")
        dump.append("  === END OF API Requests ===")
        logger.debug("%s", "\n".join(dump))
        dump = ["", "  === Object dump ==="]
        for i_group, (object_type_key, namespaces) in enumerate(
                sorted(self._objects.items(), key=lambda x: x[0])):
            last_group = i_group == len(self._objects) - 1
            group, version, plural = object_type_key
            if group:
                object_type_label = f"{plural}.{group}/{version}"
            else:
                object_type_label = f"{plural}/{version}"
            dump.append(
                f"  {NODE_CHAR[last_group]} "
                f"\x1b[7;1m{object_type_label}\x1b[0m "
                f"{object_type_key}"
            )
            group_char = CONT_CHAR[last_group]
            for i_ns, (ns, objects) in enumerate(
                    sorted(namespaces.items(), key=lambda x: (x[0] or ""))):
                last_ns = i_ns == len(namespaces) - 1
                dump.append(
                    f"  {group_char}   {NODE_CHAR[last_ns]} "
                    f"\x1b[4;1m{ns}\x1b[0m"
                )
                ns_char = CONT_CHAR[last_ns]
                for i_obj, (name, object_) in enumerate(
                        sorted(objects.items(), key=lambda x: x[0])):
                    last_obj = i_obj == len(objects) - 1
                    dump.append(
                        f"  {group_char}   {ns_char}   {NODE_CHAR[last_obj]} "
                        f"\x1b[1m{name}\x1b[0m"
                    )
                    dump.append(
                        f"  {group_char}   {ns_char}   {CONT_CHAR[last_obj]} "
                        f"  {self._dump_object(object_)}"
                    )
        dump.append("  === END OF Object dump ===")
        logger.debug("%s", "\n".join(dump))


class ApiClientTestCase(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.client_mock = ApiClientMock()
        self.api_client = self.client_mock.client

        # we want to detect and alert on accidental use of the websocket client
        # as we currently do not have a good way of mocking that one.
        self.__ws_client_patch = unittest.mock.patch(
            "kubernetes_asyncio.stream.WsApiClient",
            new=unittest.mock.Mock(
                side_effect=AssertionError("WsApiClient not mocked"),
            ),
        )
        self.__ws_client_patch.start()

    async def asyncTearDown(self):
        self.__ws_client_patch.stop()
        await self.api_client.close()


class CustomResourceTestCase(ApiClientTestCase):
    run_podspec_tests = True
    run_copy_on_write_naming_tests = True
    run_defaultsasecretmount_test = True
    run_imagepullsecrets_test = True
    run_cacerts_mysqlservice_test = True
    run_needs_update_test = True
    run_pdb_defined_test = True
    run_ssl_terminator_has_correct_reload_test = True
    run_ingress_class_test = True
    run_topology_spread_test = True
    run_context_modification_test = True

    @classmethod
    def setUpClass(cls) -> None:
        if cls is CustomResourceTestCase:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()

    def setUp(self):
        self._get_available_versions_patcher = unittest.mock.patch(
            'yaook.statemachine.versioneddependencies.VersionedDependency'
            '.get_latest_version')
        self._get_available_versions_mock = \
            self._get_available_versions_patcher.start()
        self._get_available_versions_mock.return_value = "0.0.1"
        self._get_cluster_domain_patcher = unittest.mock.patch(
            'yaook.statemachine.api_utils.get_cluster_domain'
        )
        self._get_cluster_domain_mock = \
            self._get_cluster_domain_patcher.start()
        self._get_cluster_domain_mock.return_value = "test.dns.domain"

    def tearDown(self):
        self._get_available_versions_patcher.stop()
        self._get_cluster_domain_patcher.stop()

    def _configure_cr(self, cr, initial_body):
        api_version = yaook.statemachine.join_api_version(
            cr.API_GROUP,
            cr.API_GROUP_VERSION,
        )
        initial_body["metadata"].setdefault("uid", str(uuid.uuid4()))
        initial_body["apiVersion"] = api_version
        initial_body["kind"] = cr.KIND
        self.logger = logging.getLogger(
            "{}.{}".format(type(self).__module__,
                           type(self).__qualname__)
        )
        self.cr = cr(logger=self.logger)
        namespace = initial_body["metadata"]["namespace"]
        name = initial_body["metadata"]["name"]
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            namespace, name, initial_body,
        )
        self.cr_namespace = namespace
        self.cr_name = name
        self.ctx = self._make_context()
        self.assertIsNotNone(self.cr_namespace)

    def _make_context(self):
        api_version = f"{self.cr.API_GROUP}/{self.cr.API_GROUP_VERSION}"
        body = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            self.cr_namespace, self.cr_name,
        )
        self.assertEqual(body["metadata"]["name"], self.cr_name)
        self.assertEqual(body["metadata"]["namespace"], self.cr_namespace)
        self.assertEqual(body["kind"], self.cr.KIND)
        self.assertEqual(body["apiVersion"], api_version)
        return yaook.statemachine.Context(
            parent=body,
            parent_intf=self.cr.get_resource_interface(self.api_client),
            instance=None,
            instance_data=None,
            namespace=body["metadata"]["namespace"],
            api_client=self.api_client,
            logger=self.logger,
            field_manager=str(uuid.uuid4()),
        )

    def _provide_keystone(self, namespace, name=None):
        if name is None:
            name = f"keystone-{str(uuid.uuid4()).split('-')[0]}"
        internal_api_name = f"{name}-internal-api"
        ca_config_name = f"{name}-ca-config"
        self.client_mock.put_object(
            "", "v1", "configmaps",
            namespace, internal_api_name,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "namespace": namespace,
                    "name": internal_api_name,
                    "labels": {
                        context.LABEL_COMPONENT:
                            yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
                        context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        context.LABEL_PARENT_GROUP: "yaook.cloud",
                        context.LABEL_PARENT_NAME: name,
                        context.LABEL_PARENT_VERSION: "v1",
                        LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "data": {
                    "OS_AUTH_URL": f"http://{name}:5000/v3",
                    "OS_INTERFACE": "internal",
                    "OS_IDENTITY_API_VERSION": "3",
                    "MEMCACHED_SERVERS": "memcache-server-0:11211"
                },
            }
        )
        self.client_mock.put_object(
            "", "v1", "configmaps",
            namespace, ca_config_name,
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "namespace": namespace,
                    "name": ca_config_name,
                    "labels": {
                        context.LABEL_COMPONENT:
                            yaook.op.common.KEYSTONE_CA_CERTIFICATES_COMPONENT,
                        context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        context.LABEL_PARENT_GROUP: "yaook.cloud",
                        context.LABEL_PARENT_NAME: name,
                        context.LABEL_PARENT_VERSION: "v1",
                        LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "data": {
                    "ca-bundle.crt": "ca_bundle"
                },
            }
        )
        self.client_mock.put_object(
            "yaook.cloud", "v1", "keystonedeployments",
            namespace, name,
            {
                "apiVersion": "v1",
                "kind": "KeystoneDeployment",
                "metadata": {
                    "namespace": namespace,
                    "name": name,
                    "generation": 123,
                },
                "status": {
                    "observedGeneration": 123,
                    "updatedGeneration": 123,
                    "phase": "Updated",
                },
                "spec": {
                    "targetRelease": "queens",
                },
            }
        )
        return name

    def _provide_nova(self, namespace, name=None):
        if name is None:
            name = f"nova-{str(uuid.uuid4()).split('-')[0]}"

        self.client_mock.put_object(
            "yaook.cloud", "v1", "novadeployments",
            namespace, name,
            {
                "apiVersion": "v1",
                "kind": "NovaDeployment",
                "metadata": {
                    "namespace": namespace,
                    "name": name,
                    "generation": 123,
                },
                "status": {
                    "observedGeneration": 123,
                    "updatedGeneration": 123,
                    "phase": "Updated",
                },
                "spec": {
                    "region": {
                        "name": "Test"
                    }
                }
            },
        )
        return name

    def _make_all_jobs_succeed_immediately(self):
        def succeed_jobs(j, **kwargs):
            self.logger.info("made job %s succeed immediately", j["metadata"])
            j.setdefault("status", {})["succeeded"] = 1
            return j

        self.client_mock.add_hook(
            "batch", "v1", "jobs", "POST", succeed_jobs,
        )
        self.client_mock.add_hook(
            "batch", "v1", "jobs", "PATCH", succeed_jobs,
        )

    def _make_all_cronjobs_succeed_immediately(self):
        def succeed_cronjobs(cj, **kwargs):
            self.logger.info("made cronjob %s succeed immediately",
                             cj["metadata"])
            cj.setdefault("status", {})["succeeded"] = 1
            return cj

        self.client_mock.add_hook(
            "batch", "v1beta1", "cronjobs", "POST", succeed_cronjobs,
        )
        self.client_mock.add_hook(
            "batch", "v1beta1", "cronjobs", "PATCH", succeed_cronjobs,
        )

    def _make_all_databases_ready_immediately(self):
        def create_db_service(db, **kwargs):
            self.logger.info("made database %s ready immediately",
                             db["metadata"])
            namespace = db["metadata"]["namespace"]
            name = db["metadata"]["name"]
            self.client_mock.put_object(
                "", "v1", "services",
                namespace, name,
                {
                    "apiVersion": "v1",
                    "kind": "Service",
                    "metadata": {
                        "namespace": namespace,
                        "name": name,
                        "labels": {
                            context.LABEL_COMPONENT: "public_service",
                            context.LABEL_PARENT_GROUP: "infra.yaook.cloud",
                            context.LABEL_PARENT_NAME: name,
                            context.LABEL_PARENT_PLURAL: "mysqlservices",
                            LABEL_IGNORE_DURING_TESTS: "true",
                        },
                    },
                },
            )
            return db

        def patch_mysqlservice(db, **kwargs):
            self.logger.info("made database %s ready immediately",
                             db["metadata"])
            db.setdefault("metadata", {})["generation"] = 1
            db.setdefault("status", {})["observedGeneration"] = 1
            db.setdefault("status", {})["updatedGeneration"] = 1
            db["status"]["phase"] = "Updated"
            return db

        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "mysqlservices", "PATCH",
            create_db_service,
        )
        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "mysqlservices", "PATCH",
            patch_mysqlservice,
        )

    def _make_all_database_users_ready_immediately(self):
        def patch_mysqluser(db, **kwargs):
            self.logger.info("made database user %s ready immediately",
                             db["metadata"])
            db.setdefault("metadata", {})["generation"] = 1
            db.setdefault("status", {})["observedGeneration"] = 1
            db.setdefault("status", {})["updatedGeneration"] = 1
            db["status"]["phase"] = "Updated"
            return db

        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "mysqlusers", "PATCH",
            patch_mysqluser,
        )

    def _make_all_mqs_succeed_immediately(self):
        def create_amqp_service(db, **kwargs):
            self.logger.info("made amqp server %s ready immediately",
                             db["metadata"])
            namespace = db["metadata"]["namespace"]
            name = db["metadata"]["name"]
            self.client_mock.put_object(
                "", "v1", "services",
                namespace, name,
                {
                    "apiVersion": "v1",
                    "kind": "Service",
                    "metadata": {
                        "namespace": namespace,
                        "name": name,
                        "labels": {
                            context.LABEL_COMPONENT: "public_service",
                            context.LABEL_PARENT_GROUP: "infra.yaook.cloud",
                            context.LABEL_PARENT_NAME: name,
                            context.LABEL_PARENT_PLURAL: "amqpservers",
                            LABEL_IGNORE_DURING_TESTS: "true",
                        },
                    },
                },
            )
            return db

        def patch_amqp_service(amqp, **kwargs):
            self.logger.info("made amqp server %s ready immediately",
                             amqp["metadata"])
            amqp.setdefault("metadata", {})["generation"] = 1
            amqp.setdefault("status", {})["observedGeneration"] = 1
            amqp.setdefault("status", {})["updatedGeneration"] = 1
            amqp["status"]["phase"] = "Updated"
            return amqp

        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "amqpservers", "PATCH",
            create_amqp_service,
        )
        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "amqpservers", "PATCH",
            patch_amqp_service,
        )

    def _make_all_mq_users_ready_immediately(self):
        def patch_amqpuser(mq, **kwargs):
            self.logger.info("made mq user %s ready immediately",
                             mq["metadata"])
            mq.setdefault("metadata", {})["generation"] = 1
            mq.setdefault("status", {})["observedGeneration"] = 1
            mq.setdefault("status", {})["updatedGeneration"] = 1
            mq["status"]["phase"] = "Updated"
            return mq

        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "amqpusers", "PATCH",
            patch_amqpuser,
        )

    def _make_all_memcached_succeed_immediately(self):
        def create_memcached_statefulset(db, **kwargs):
            self.logger.info("made memcached statefulset %s ready immediately",
                             db["metadata"])
            namespace = db["metadata"]["namespace"]
            name = db["metadata"]["name"]
            self.client_mock.put_object(
                "apps", "v1", "statefulsets",
                namespace, name,
                {
                    "apiVersion": "apps/v1",
                    "kind": "StatefulSet",
                    "metadata": {
                        "namespace": namespace,
                        "name": name,
                        "labels": {
                            context.LABEL_COMPONENT: "memcached",
                            context.LABEL_PARENT_GROUP: "infra.yaook.cloud",
                            context.LABEL_PARENT_NAME: name,
                            context.LABEL_PARENT_PLURAL: "memcachedservices",
                            LABEL_IGNORE_DURING_TESTS: "true",
                        },
                    },
                    # The pod spec create by this statefulset is later tested
                    # by `test_imagepullsecret_in_podspec_for_statefulsets`,
                    # but as the statefulset is created by MemcachedService and
                    # not directly from KeystoneDeployment thus
                    # `imagePullSecrets` isn't populated and we are adding it
                    # here
                    "spec": {
                        "selector": {
                            "matchLabels": {}
                        },
                        "serviceName": "test_serviceName",
                        "template": {
                            "spec": {
                                "imagePullSecrets": [
                                    {
                                        "name": "testpullsecret"
                                    }
                                ],
                                "containers": []
                            }
                        }
                    }
                },
            )
            return db

        def create_memcached_service(db, **kwargs):
            self.logger.info("made memcached server %s ready immediately",
                             db["metadata"])
            namespace = db["metadata"]["namespace"]
            name = db["metadata"]["name"]
            self.client_mock.put_object(
                "", "v1", "services",
                namespace, name,
                {
                    "apiVersion": "v1",
                    "kind": "Service",
                    "metadata": {
                        "namespace": namespace,
                        "name": name,
                        "labels": {
                            context.LABEL_COMPONENT: "service",
                            context.LABEL_PARENT_GROUP: "infra.yaook.cloud",
                            context.LABEL_PARENT_NAME: name,
                            context.LABEL_PARENT_PLURAL: "memcachedservices",
                            LABEL_IGNORE_DURING_TESTS: "true",
                        },
                    },
                },
            )
            return db

        def patch_memcached_service(memcached, **kwargs):
            self.logger.info("made memcached server %s ready immediately",
                             memcached["metadata"])
            memcached.setdefault("metadata", {})["generation"] = 1
            memcached.setdefault("status", {})["observedGeneration"] = 1
            memcached.setdefault("status", {})["updatedGeneration"] = 1
            memcached["status"]["phase"] = "Updated"
            return memcached

        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "memcachedservices", "PATCH",
            create_memcached_service,
        )
        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "memcachedservices", "PATCH",
            create_memcached_statefulset,
        )
        self.client_mock.add_hook(
            "infra.yaook.cloud", "v1", "memcachedservices", "PATCH",
            patch_memcached_service,
        )

    def _make_all_keystoneusers_complete_immediately(self):
        def create_keystoneuser_secret(kus, **kwargs):
            self.logger.info("made keystone user %s complete immediately",
                             kus["metadata"])
            name = kus["metadata"]["name"]
            namespace = kus["metadata"]["namespace"]
            secret_name = f"{kus['metadata']['name']}-idcreds"
            self.client_mock.put_object(
                "", "v1", "secrets",
                namespace, secret_name,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": namespace,
                        "name": secret_name,
                        "labels": {
                            context.LABEL_COMPONENT: "credentials",
                            context.LABEL_PARENT_GROUP: "yaook.cloud",
                            context.LABEL_PARENT_NAME: name,
                            context.LABEL_PARENT_PLURAL: "keystoneusers",
                            LABEL_IGNORE_DURING_TESTS: "true",
                        },
                    },
                    "data": api_utils.encode_secret_data({
                        "OS_USERNAME": f"{name}.",
                        "OS_PASSWORD": str(uuid.uuid4()),
                        "OS_PROJECT_DOMAIN_NAME": "Default",
                        "OS_USER_DOMAIN_NAME": "Default",
                        "OS_AUTH_TYPE": "password",
                        "OS_PROJECT_NAME": str(uuid.uuid4()),
                    }),
                },
            )
            return kus

        def patch_keystone_user(kus, **kwargs):
            self.logger.info("made keystone user %s ready immediately",
                             kus["metadata"])
            kus.setdefault("metadata", {})["generation"] = 1
            kus.setdefault("status", {})["observedGeneration"] = 1
            kus.setdefault("status", {})["updatedGeneration"] = 1
            kus["status"]["phase"] = "Updated"
            return kus

        self.client_mock.add_hook(
            "yaook.cloud", "v1", "keystoneusers", "PATCH",
            create_keystoneuser_secret,
        )
        self.client_mock.add_hook(
            "yaook.cloud", "v1", "keystoneusers", "PATCH",
            patch_keystone_user,
        )

    def _make_all_keystoneendpoints_complete_immediately(self):
        def patch_keystone_endpoint(kep, **kwargs):
            self.logger.info("made keystone endpoint %s ready immediately",
                             kep["metadata"])
            kep.setdefault("metadata", {})["generation"] = 1
            kep.setdefault("status", {})["observedGeneration"] = 1
            kep.setdefault("status", {})["updatedGeneration"] = 1
            kep["status"]["phase"] = "Updated"
            return kep

        self.client_mock.add_hook(
            "yaook.cloud", "v1", "keystoneendpoints", "PATCH",
            patch_keystone_endpoint,
        )

    def _make_all_certificates_succeed_immediately(self):
        def succeed_certificates(c, **kwargs):
            self.logger.info(
                "made certificate %s issuing complete",
                c["metadata"])
            c.setdefault("status", {}).\
                setdefault("conditions", []).\
                append({"type": "Ready", "status": "True"})
            c["status"]["notBefore"] = str(
                getattr(unittest.mock.sentinel,
                        "notBefore-%s" % c["metadata"]["name"]))
            secret = self.client_mock.get_object("", "v1", "secrets",
                                                 c["metadata"]["namespace"],
                                                 c["spec"]["secretName"])
            new_data = api_utils.decode_secret_data(secret["data"])
            new_data["ca.crt"] = DEMO_CA_CERTIFICATE.encode('utf-8')
            new_data["tls.crt"] = "<not a real certificate>"
            new_data["tls.key"] = "<not a real key>"
            secret["data"] = api_utils.encode_secret_data(new_data)
            self.client_mock.put_object("", "v1", "secrets",
                                        c["metadata"]["namespace"],
                                        c["spec"]["secretName"],
                                        secret)
            return c

        self.client_mock.add_hook(
            "cert-manager.io", "v1", "certificates", "PATCH",
            succeed_certificates,
        )
        self.client_mock.add_hook(
            "cert-manager.io", "v1", "certificates", "PATCH",
            succeed_certificates,
        )

    def _make_all_issuers_ready_immediately(self):
        def make_issuer_ready(c, **kwargs):
            self.logger.info(
                "made issuer %s ready immediately",
                c["metadata"])
            c.setdefault("status", {}).\
                setdefault("conditions", []).\
                append({"type": "Ready", "status": "True"})
            return c

        self.client_mock.add_hook(
            "cert-manager.io", "v1", "issuers", "PATCH",
            make_issuer_ready,
        )
        self.client_mock.add_hook(
            "cert-manager.io", "v1", "issuers", "PATCH",
            make_issuer_ready,
        )

    def _make_all_deployments_ready_immediately(self):
        def make_deployment_ready(depl, **kwargs):
            self.logger.info(
                "made deployment %s ready immediately",
                depl["metadata"])
            depl.setdefault("status", {})
            depl["status"]["replicas"] = depl["spec"]["replicas"]
            depl["status"]["updatedReplicas"] = depl["status"]["replicas"]
            depl["status"]["availableReplicas"] = depl["status"]["replicas"]
            depl["status"]["observedGeneration"] = \
                depl["metadata"].setdefault("generation", 1)
            return depl

        self.client_mock.add_hook(
            "apps", "v1", "deployments", "PATCH",
            make_deployment_ready,
        )
        self.client_mock.add_hook(
            "apps", "v1", "deployments", "PATCH",
            make_deployment_ready,
        )

    def _make_all_statefulsets_ready_immediately(self):
        def make_statefulset_ready(depl, **kwargs):
            self.logger.info(
                "made statefulset %s ready immediately",
                depl["metadata"])
            depl.setdefault("status", {})
            depl["status"]["updateRevision"] = "autoready"
            depl["status"]["currentRevision"] = "autoready"
            # replicas is optional and the default is 1
            depl["status"]["replicas"] = depl["spec"].get("replicas", 1)
            depl["status"]["readyReplicas"] = depl["status"]["replicas"]
            depl["status"]["observedGeneration"] = \
                depl["metadata"].setdefault("generation", 1)
            return depl

        self.client_mock.add_hook(
            "apps", "v1", "statefulsets", "PATCH",
            make_statefulset_ready,
        )
        self.client_mock.add_hook(
            "apps", "v1", "statefulsets", "PATCH",
            make_statefulset_ready,
        )

    def _make_all_policy_configmaps_ready(self):
        def ready_configmaps(cm, **kwargs):
            self.logger.info("made configmap %s ready", cm["metadata"])
            cm["data"].update(
                {
                    "policy.yaml": {}
                }
            )
            return cm

        self.client_mock.add_hook(
            "", "v1", "configmaps", "POST", ready_configmaps,
        )
        self.client_mock.add_hook(
            "", "v1", "configmaps", "PATCH", ready_configmaps,
        )

    def _make_all_dependencies_complete_immediately(self):
        self._make_all_memcached_succeed_immediately()
        self._make_all_jobs_succeed_immediately()
        self._make_all_cronjobs_succeed_immediately()
        self._make_all_databases_ready_immediately()
        self._make_all_database_users_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_keystoneendpoints_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()
        self._make_all_statefulsets_ready_immediately()
        self._make_all_policy_configmaps_ready()

    def _mock_issuer_with_secret(self, issuername):
        self.client_mock.put_object(
            "cert-manager.io", "v1", "issuers", self.cr_namespace,
            issuername,
            {
                "apiVersion": "cert-manager.io/v1",
                "kind": "Issuer",
                "metadata": {
                    "name": issuername,
                    "namespace": self.cr_namespace,
                },
                "spec": {
                    "ca": {
                        "secretName": "testcasecret",
                    }
                }
            },
        )
        self.client_mock.put_object(
            "", "v1", "secrets",
            self.cr_namespace, "testcasecret",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": self.cr_namespace,
                    "name": "testcasecret",
                },
                "data": {
                    "tls.crt": base64.b64encode(DEMO_CA_CERTIFICATE.encode(
                                                'utf-8')).decode('utf-8')
                },
            },
        )

    def _mock_labelled_nodes(self, node_labels, annotations=None):
        demonet = ipaddress.ip_network("192.0.2.0/24")
        for i, (node_name, labels) in enumerate(node_labels.items()):
            node = {
                "apiVersion": "v1",
                "kind": "Node",
                "metadata": {
                    "name": node_name,
                    "labels": labels,
                    "uid": i,
                },
                "spec": {},
                "status": {
                    "addresses": [
                        {
                            "address": str(demonet[i+1]),
                            "type": "InternalIP",
                        },
                        {
                            "address": node_name,
                            "type": "Hostname",
                        },
                    ],
                },
            }
            if annotations is None:
                self.client_mock.put_object(
                    "", "v1", "nodes", None, node_name, node,
                )
            else:
                node["metadata"]["annotations"] = annotations
                self.client_mock.put_object(
                    "", "v1", "nodes", None, node_name, node,
                )

    async def asyncTearDown(self):
        self.client_mock.dump(self.logger)
        await super().asyncTearDown()

    async def _run_test_for_each_cds_pod(self, test, skip_annotation=None):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        cds = self.client_mock.get_all_objects(
            "apps.yaook.cloud", "v1", "configureddaemonsets")
        for name, body in _skip_test_resources(cds, skip_annotation):
            test(name, body.get("spec", {}).get("template", {}))

    async def _run_test_for_each_deployment_pod(
            self, test, skip_annotation=None):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = self.client_mock.get_all_objects(
            "apps", "v1", "deployments")
        for name, body in _skip_test_resources(deployments, skip_annotation):
            test(name, body.get("spec", {}).get("template", {}))

    async def _run_test_for_each_statefulset_pod(
            self, test, skip_annotation=None):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulsets = self.client_mock.get_all_objects(
            "apps", "v1", "statefulsets")
        for name, body in _skip_test_resources(statefulsets, skip_annotation):
            test(name, body.get("spec", {}).get("template", {}))

    async def _run_test_for_each_job_pod(self, test, skip_annotation=None):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        jobs = self.client_mock.get_all_objects(
            "batch", "v1", "jobs")
        for name, body in _skip_test_resources(jobs, skip_annotation):
            test(name, body.get("spec", {}).get("template", {}))

    async def _run_test_for_each_cronjob_pod(self, test, skip_annotation=None):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        cronjobs = self.client_mock.get_all_objects(
            "batch", "v1beta1", "cronjobs")
        for name, body in _skip_test_resources(cronjobs, skip_annotation):
            test(name, body.get("spec", {}).get("jobTemplate", {})
                 .get("spec", {}).get("template", {}))

    async def _run_test_for_each_mysqlservice(self, test):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        mysqlservices = self.client_mock.get_all_objects(
            "infra.yaook.cloud", "v1", "mysqlservices")
        for name, body in _skip_test_resources(mysqlservices):
            test(name, body.get("spec", {}))

    def _test_probes_in_podspec(self, name, podspec):
        containers = podspec.get("spec", {}).get("containers", [])
        num_probing_containers = 0
        for container in containers:
            probes_readiness = "readinessProbe" in container
            probes_liveness = "livenessProbe" in container
            cname = container.get("name")
            if "api" in cname:
                # all api containers must do the probes
                self.assertTrue(
                    probes_liveness,
                    f"Container {name}/{cname} is missing a livenessProbe"
                )
                self.assertTrue(
                    probes_readiness,
                    f"Container {name}/{cname} is missing a readinessProbe"
                )
            if probes_readiness and probes_liveness:
                num_probing_containers += 1
        msg = f"At least one container in pod {name} must do a readiness " \
              f"and liveness probe"
        self.assertGreaterEqual(num_probing_containers, 1, msg)

    async def test_k8s_probes_defined_for_cds(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cds_pod(self._test_probes_in_podspec)

    async def test_k8s_probes_defined_for_deployments(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = self.client_mock.get_all_objects(
            "apps", "v1", "deployments")
        for name, body in deployments.items():
            if "api" in name:
                self._test_probes_in_podspec(
                    name,
                    body.get("spec", {}).get("template", {}))

    def _test_ca_in_podspec(self, name, podspec):
        containers = list(podspec.get("spec", {}).get("containers", []))
        containers.extend(podspec.get("spec", {}).get("initContainers", []))
        for container in containers:
            mount_path = None

            cname = container.get("name")
            if cname.startswith("service-reload"):
                continue
            for mount in container.get("volumeMounts", []):
                mount_path = mount.get("mountPath")
                if mount_path in [
                        "/etc/pki/tls/certs",
                        "/etc/ssl/certs",
                        "/etc/ssl/certs/ca-certificates.crt"]:
                    break
            else:
                self.fail(f"Container {name}/{cname} does not have CA certs")

            if mount_path is not None:
                if mount_path.endswith(".crt"):
                    expected_ca_bundle = mount_path
                else:
                    expected_ca_bundle = mount_path + "/ca-bundle.crt"

                for env in container.get("env", []):
                    if env["name"] == "REQUESTS_CA_BUNDLE":
                        self.assertEqual(env["value"], expected_ca_bundle,
                                         f"{name}/{cname}")
                        break
                else:
                    self.fail(f"REQUESTS_CA_BUNDLE not set in {name}/{cname}")

    async def test_ca_in_podspec_for_cds(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cds_pod(
            self._test_ca_in_podspec,
            CATEST_SKIP_ANNOTATION,
        )

    async def test_ca_in_podspec_for_deployments(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_ca_in_podspec,
            CATEST_SKIP_ANNOTATION,
        )

    async def test_ca_in_podspec_for_statefulsets(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_ca_in_podspec,
            CATEST_SKIP_ANNOTATION,
        )

    async def test_ca_in_podspec_for_jobs(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_job_pod(
            self._test_ca_in_podspec,
            CATEST_SKIP_ANNOTATION,
        )

    async def test_service_reload_defined_for_deployments(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = self.client_mock.get_all_objects(
            "apps", "v1", "deployments")
        for name, body in deployments.items():
            containers = body.get("spec", {}).get("containers", [])
            uses_certificates = False
            for container in containers:
                if container["name"] == "ssl-terminator":
                    uses_certificates = True
                if any([
                    x for x in container.get("volumeMounts", [])
                        if x["name"] == "tls-secret"]):
                    uses_certificates = True
            if uses_certificates:
                self.assertTrue(
                    body.get("spec", {}).get("shareProcessNamespace")
                )
                self.assertIn(
                    "service-reload",
                    [x["name"] for x in containers]
                )

    async def test_copy_on_write_resource_generates_name(self):
        if not self.run_copy_on_write_naming_tests:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        for resource in self.cr.sm._components.values():
            if hasattr(resource, "copy_on_write") and resource.copy_on_write:
                instance = await resource._get_current(self.ctx)
                if hasattr(instance, "to_dict"):
                    instance = instance.to_dict()
                self.assertIn("generate_name", instance["metadata"])
                self.assertNotEquals(
                    None, instance["metadata"]["generate_name"])

    async def test_certificate_is_never_directly_dependent(self):
        for res, deps in self.cr.sm._dependencies.items():
            if not isinstance(
                    res,
                    yaook.statemachine.ReadyCertificateSecretReference):
                if any([
                    isinstance(x, yaook.statemachine.Certificate)
                    for x in deps
                ]):
                    self.fail(
                        "State %s depends on a CertificateState. It should "
                        "rather depend on a ReadyCertificateSecretReference"
                        % res)

    async def test_certificate_secret_is_never_directly_dependent(self):
        for res, deps in self.cr.sm._dependencies.items():
            if not isinstance(
                    res,
                    yaook.statemachine.Certificate):
                if any([
                    isinstance(x, yaook.statemachine.EmptyTlsSecret)
                    for x in deps
                ]):
                    self.fail(
                        "State %s depends on a CertificateState. It should "
                        "rather depend on a ReadyCertificateSecretReference"
                        % res)

    def _test_deprecated_serviceaccount_field_in_podspec(self, name, podspec):
        if not podspec or not podspec.get("spec"):
            self.fail(f"Pod {name} had empty podspec (was '{podspec}').")
        pod_sa = podspec.get("spec", {}).get("serviceAccount")
        if pod_sa is not None:
            self.fail(f"Pod {name} sets the deprecated field "
                      f"'spec.serviceAccount' to {pod_sa}. "
                      f"Use 'spec.serviceAccountName' instead.")

    async def test_deprecated_serviceaccount_in_podspec_for_cds(self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cds_pod(
            self._test_deprecated_serviceaccount_field_in_podspec)

    async def test_deprecated_serviceaccount_in_podspec_for_deployments(self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_deprecated_serviceaccount_field_in_podspec)

    async def test_deprecated_serviceaccount_in_podspec_for_statefulsets(
            self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_deprecated_serviceaccount_field_in_podspec)

    async def test_deprecated_serviceaccount_in_podspec_for_jobs(self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_job_pod(
            self._test_deprecated_serviceaccount_field_in_podspec)

    async def test_deprecated_serviceaccount_in_podspec_for_cronjobs(self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cronjob_pod(
            self._test_deprecated_serviceaccount_field_in_podspec)

    def _test_default_serviceaccount_token_not_mounted_in_podspec(
            self, name, podspec):
        if not podspec or not podspec.get("spec"):
            self.fail(f"Pod {name} had empty podspec (was '{podspec}').")
        automount_sa_token = podspec.get("spec", {})\
            .get("automountServiceAccountToken", True)
        if automount_sa_token:
            pod_sa_name = podspec.get("spec", {})\
                .get("serviceAccountName", "default")
            if pod_sa_name == "default":
                self.fail(f"Pod {name} uses the default "
                          f"service account without setting "
                          f"spec.automountServiceAccountToken to False.")

    async def test_serviceaccount_token_mount_in_podspec_for_cds(self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cds_pod(
            self._test_default_serviceaccount_token_not_mounted_in_podspec)

    async def test_serviceaccount_token_mount_in_podspec_for_deployments(
            self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_default_serviceaccount_token_not_mounted_in_podspec)

    async def test_serviceaccount_token_mount_in_podspec_for_statefulsets(
            self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_default_serviceaccount_token_not_mounted_in_podspec)

    async def test_serviceaccount_token_mount_in_podspec_for_jobs(
            self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_job_pod(
            self._test_default_serviceaccount_token_not_mounted_in_podspec)

    async def test_serviceaccount_token_mount_in_podspec_for_cronjobs(
            self):
        if not self.run_defaultsasecretmount_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cronjob_pod(
            self._test_default_serviceaccount_token_not_mounted_in_podspec)

    def _test_prepare_imagepullsecret_in_podspec(self):
        cr_object_name, = self.client_mock.get_all_objects(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
        )
        cr_object = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            cr_object_name.split("/")[0], cr_object_name.split("/")[1]
        )
        cr_object["spec"]["imagePullSecrets"] = [{"name": "testpullsecret"}]
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            cr_object["metadata"]["namespace"], cr_object["metadata"]["name"],
            cr_object
        )

    def _test_imagepullsecret_in_podspec(self, name, podspec):
        ips = podspec.get("spec", {}).get("imagePullSecrets")
        if ips != [{"name": "testpullsecret"}]:
            self.fail(f"Pod {name} does not have imagePullSecrets even "
                      "though we tried to set them.")

    async def test_imagepullsecret_in_podspec_for_cds(self):
        if not self.run_imagepullsecrets_test:
            raise unittest.SkipTest("skip requested")
        self._test_prepare_imagepullsecret_in_podspec()
        await self._run_test_for_each_cds_pod(
            self._test_imagepullsecret_in_podspec)

    async def test_imagepullsecret_in_podspec_for_deployments(self):
        if not self.run_imagepullsecrets_test:
            raise unittest.SkipTest("skip requested")
        self._test_prepare_imagepullsecret_in_podspec()
        await self._run_test_for_each_deployment_pod(
            self._test_imagepullsecret_in_podspec)

    async def test_imagepullsecret_in_podspec_for_statefulsets(self):
        if not self.run_imagepullsecrets_test:
            raise unittest.SkipTest("skip requested")
        self._test_prepare_imagepullsecret_in_podspec()
        await self._run_test_for_each_statefulset_pod(
            self._test_imagepullsecret_in_podspec)

    async def test_imagepullsecret_in_podspec_for_jobs(self):
        if not self.run_imagepullsecrets_test:
            raise unittest.SkipTest("skip requested")
        self._test_prepare_imagepullsecret_in_podspec()
        await self._run_test_for_each_job_pod(
            self._test_imagepullsecret_in_podspec)

    def _test_cacerts_in_mysqlservice(self, name, mysqlspec):
        cacert = mysqlspec.get("caCertificates")
        if cacert != [DEMO_CA_CERTIFICATE]:
            self.fail(f"MysqlService {name} does not have the caCertifcates "
                      "specified in the service Custom Resource.")

    def _test_prepare_cacerts_in_mysqlservice(self):
        cr_object_name, = self.client_mock.get_all_objects(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
        )
        cr_object = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            cr_object_name.split("/")[0], cr_object_name.split("/")[1]
        )
        cr_object["spec"]["caCertificates"] = [DEMO_CA_CERTIFICATE]
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            cr_object["metadata"]["namespace"], cr_object["metadata"]["name"],
            cr_object
        )

    async def test_cacerts_in_mysqlservice(self):
        if not self.run_cacerts_mysqlservice_test:
            raise unittest.SkipTest("skip requested")
        self._test_prepare_cacerts_in_mysqlservice()
        await self._run_test_for_each_mysqlservice(
            self._test_cacerts_in_mysqlservice)

    async def test_no_wrong_needs_update(self):
        if not self.run_needs_update_test:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        self.ctx = self._make_context()

        print(f"Dependencies Keys: {self.cr.sm._dependencies.keys()}")

        for state in self.cr.sm._dependencies.keys():

            dependencies_by_component = {
                dep_state.component: dep_state
                for dep_state in self.cr.sm._dependencies[state]
            }

            if await state.reconcile(self.ctx,
                                     dependencies=dependencies_by_component):
                self.fail(
                    "State %s needs an update even though reconcilation "
                    "already finished successfully. This will cause "
                    "issues with upgrades. Please fix the _needs_update "
                    "method of this state." % state
                )

    def test_listeners_all_defined(self):
        listeners = self.cr.get_listeners()
        for listener in listeners:
            if isinstance(listener, context.KubernetesListener):
                self.assertIn(
                    (listener.api_group, listener.version, listener.plural),
                    yaook.op.daemon.AVAILABLE_WATCHERS.keys(),
                    msg="please add a watcher to yaook/op/daemon.py")

    def _test_pdb_defined(self, name, podspec):
        podlabels = podspec.get("metadata", {}).get("labels")

        pdbs = self.client_mock.get_all_objects(
            "policy", "v1beta1", "poddisruptionbudgets")
        for pdb in pdbs.values():
            pdb_matchlabels = pdb.get("spec", {}).get("selector", {}
                                                      ).get("matchLabels")
            if api_utils.matches_labels(podlabels, pdb_matchlabels):
                return

        self.fail(f"Pod {name} does not have a matching PodDisruptionBudget "
                  "set. Please let the operator generate one. You can do this "
                  "by e.g. adding a QuorumPodDisruptionBudgetState for this "
                  "resource.")

    async def test_pdb_for_deployment(self):
        if not self.run_pdb_defined_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_pdb_defined,
            PDBTEST_SKIP_ANNOTATION,
        )

    async def test_pdb_for_statefulset(self):
        if not self.run_pdb_defined_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_pdb_defined,
            PDBTEST_SKIP_ANNOTATION,
        )

    async def test_pdb_for_cds(self):
        if not self.run_pdb_defined_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_cds_pod(
            self._test_pdb_defined)

    def _test_ssl_terminator_has_correct_reload(self, name, podspec):
        containers = podspec.get("spec", {}).get("containers", [])
        has_ssl_terminator = any([
            "ssl-terminator" in cs["name"] for cs in containers])
        if not has_ssl_terminator:
            raise unittest.SkipTest("not relevant")

        for container in containers:
            if "service-reload" in container["name"]:
                env = {x["name"]: x["value"] for x in container.get("env", [])}
                if env.get("YAOOK_SERVICE_RELOAD_MODULE") != "traefik":
                    self.fail(f"Pod {name} uses an ssl terminator but "
                              "the service-reload does not reload it.")

    async def test_ssl_terminator_has_correct_reload_for_deployment(self):
        if not self.run_ssl_terminator_has_correct_reload_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_ssl_terminator_has_correct_reload,
        )

    async def test_ssl_terminator_has_correct_reload_for_statefulset(self):
        if not self.run_ssl_terminator_has_correct_reload_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_ssl_terminator_has_correct_reload,
        )

    async def test_ingress_class(self):
        if not self.run_ingress_class_test:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        ingresses_beta = self.client_mock.get_all_objects(
            "networking.k8s.io", "v1beta1", "ingresses"
        )
        for name, body in _skip_test_resources(ingresses_beta):
            self.assertIn(
                "ingressClassName",
                body["spec"],
                (name, body["metadata"]["namespace"]),
            )

        ingresses_stable = self.client_mock.get_all_objects(
            "networking.k8s.io", "v1", "ingresses"
        )
        for name, body in _skip_test_resources(ingresses_stable):
            self.assertIn(
                "ingressClassName",
                body["spec"],
                (name, body["metadata"]["namespace"]),
            )

    async def test_creates_resources_for_ssl_terminator_monitoring(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_creates_resources_for_ssl_terminator_monitoring
        )

    def _helper_get_secret_mounts(self, podspec, container):
        volume_names = [
            volumeMount["name"]
            for volumeMount in container.get("volumeMounts", [])
            ]

        secret_names = [
            volume["secret"]["secretName"]
            for volume in podspec["spec"].get("volumes", [])
            if volume["name"] in volume_names
            and "secret" in volume
            ]

        return secret_names

    def _helper_get_ssl_terminator_metrics_ports(self, podspec, name):
        metric_ports = []
        for container in podspec["spec"]["containers"]:
            if "ssl-terminator" in container["image"]:
                try:
                    metric_port = [
                        env["value"] for env in container.get("env", [])
                        if env["name"] == "METRICS_PORT"
                        ][0]
                except IndexError:
                    self.fail("Unable to find missing METRICS_PORT Env-Var at "
                              "SSL-Terminator Container in the deployment "
                              f"{name}.")
                if metric_port in metric_ports:
                    self.fail("Two SSL Terminators are set to the same "
                              "METRICS_PORT. This prevents one SSL Terminator "
                              "from starting")
                metric_ports.append(
                    (int(metric_port),
                        self._helper_get_secret_mounts(podspec, container))
                )
        return metric_ports

    def _test_creates_resources_for_ssl_terminator_monitoring(
            self, name, podspec):
        metric_ports = self._helper_get_ssl_terminator_metrics_ports(
            podspec,
            name
            )

        # Get all services with a selector, that matches the podspec
        services = self.client_mock.get_all_objects(
            "", "v1", "services")
        services = [
            spec for name, spec in services.items()
            if "spec" in spec  # because some test services do not have a spec
            and api_utils.matches_labels(
                podspec["metadata"]["labels"],
                spec["spec"]["selector"]
            )
        ]

        # get all servicemoniors
        servicemonitors = self.client_mock.get_all_objects(
            "monitoring.coreos.com", "v1", "servicemonitors")
        servicemonitors = [spec for name, spec in servicemonitors.items()]

        # create list with all ports, that match an servicemonitor endpoint
        endpoints = []
        for servicemonitor in servicemonitors:
            endpoints.extend([(
                endpoint["port"],
                endpoint.get(
                    "tlsConfig", {}).get(
                        "ca", {}).get("secret", {}).get("name"),
                servicemonitor["spec"]["selector"]["matchLabels"])
                for endpoint in servicemonitor["spec"]["endpoints"]])

        service_ports = []
        for endpoint in endpoints:
            service_ports.extend([
                (port["targetPort"], endpoint[1])
                for service in services
                for port in service["spec"]["ports"]
                if api_utils.matches_labels(
                    service["metadata"]["labels"],
                    endpoint[2]
                )
                and port.get("name") in [
                    endpoint[0]
                ]
            ])

        service_monitor_count = {}
        for metric_port, secret in metric_ports:
            service_monitor_count[metric_port] = 0

        for port, secret in service_ports:
            metric_port_secrets = []
            for metric_port in metric_ports:
                if port == metric_port[0]:
                    metric_port_secrets.extend(metric_port[1])
            if secret not in metric_port_secrets:
                self.fail("Certificate secret of a servicemonitor does not "
                          "match on the mounted secrets in the container. "
                          f"Expexctiong the secret {secret} for the port "
                          f"{port} of the deployment {name} but found "
                          f"{metric_port_secrets}.")

            if port in [metric_port[0] for metric_port in metric_ports]:
                service_monitor_count[port] += 1

        for port, count in service_monitor_count.items():
            if count != 1:
                self.fail(f"For the METRICS_PORT {port} of the deployment "
                          f"{name} match {count} servicemonitors instead of "
                          "the expected 1.")
            self.assertEqual(count, 1)

    async def test_volume_mounts_for_ssl_terminator(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_volume_mounts_for_ssl_terminator
        )

    def _test_volume_mounts_for_ssl_terminator(self, name, podspec):
        for container in podspec["spec"]["containers"]:
            if "ssl-terminator" in container["image"]:
                volume_mounts = [
                    mount["mountPath"] for mount in container["volumeMounts"]
                ]
                if "/data" not in volume_mounts:
                    self.fail("Unable to find the mount for the tls secret "
                              f"at /data for the container {container['name']}"
                              f"in the deployment {name}.")
                if "/config" not in volume_mounts:
                    self.fail("Unable to find the mount for the config "
                              "at /config of the container "
                              f"{container['name']} in the deployment {name}.")

                for mount in container["volumeMounts"]:
                    if "external" in container["name"]:
                        if mount["mountPath"] == "/data":
                            self.assertEqual(
                                mount["name"],
                                "tls-secret-external")
                            mount["name"] = "tls-secret-external"
                        if mount["mountPath"] == "/config":
                            self.assertEqual(
                                mount["name"],
                                "ssl-terminator-external-config")
                            mount["name"] = "ssl-terminator-external-config"
                    else:
                        if mount["mountPath"] == "/data":
                            self.assertEqual(mount["name"], "tls-secret")
                        if mount["mountPath"] == "/config":
                            self.assertEqual(
                                mount["name"],
                                "ssl-terminator-config")

    async def test_volume_mounts_for_config_reloader(self):
        if not self.run_podspec_tests:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_volume_mounts_for_config_reloader
        )

    def _test_volume_mounts_for_config_reloader(self, name, podspec):
        for container in podspec["spec"]["containers"]:
            if "ssl-terminator" in container["image"]:
                volume_mounts = [
                    mount["mountPath"] for mount in container["volumeMounts"]
                ]
                if "/config" not in volume_mounts:
                    self.fail("Unable to find the mount for the config "
                              "at /config of the container "
                              f"{container['name']} in the deployment {name}.")

                for mount in container["volumeMounts"]:
                    if "external" in container["name"]:
                        if mount["mountPath"] == "/config":
                            self.assertEqual(
                                mount["name"],
                                "ssl-terminator-external-config")
                            mount["name"] = "ssl-terminator-external-config"
                    else:
                        if mount["mountPath"] == "/data":
                            self.assertEqual(mount["name"], "tls-secret")

    def _test_host_topology_spread_set(self, name, podspec):
        topologySpreads = podspec["spec"].get("topologySpreadConstraints", [])
        if not topologySpreads:
            self.fail(f"No spec.topologySpreadConstraints "
                      f"were given for pod {name}")
        if len(topologySpreads) != 1:
            self.fail(f"{len(topologySpreads)} spec.topologySpreadConstraints "
                      f"were given for pod {name}. Expected only one.")
        topologySpread = topologySpreads[0]

        max_skew = topologySpread.get("maxSkew")
        expected_max_skew = 1
        if max_skew != expected_max_skew:
            self.fail(f"spec.topologySpreadConstraints.maxSkew was "
                      f"'{max_skew}' for pod {name}. "
                      f"Expected {expected_max_skew}.")

        topology_key = topologySpread.get("topologyKey")
        expected_topology_key = "kubernetes.io/hostname"
        if topology_key != expected_topology_key:
            self.fail(f"spec.topologySpreadConstraints.topologyKey "
                      f"was '{topology_key}' for pod {name}. "
                      f"Expected {expected_topology_key}.")

        when_unsatisfiable = topologySpread.get("whenUnsatisfiable")
        expected_when_unsatisfiable = "ScheduleAnyway"
        if when_unsatisfiable != expected_when_unsatisfiable:
            self.fail(f"spec.topologySpreadConstraints.whenUnsatisfiable "
                      f"was '{when_unsatisfiable}' for pod {name}. "
                      f"Expected {expected_when_unsatisfiable}.")

    async def test_host_topology_spread_set_for_statefulset(self):
        if not self.run_topology_spread_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_statefulset_pod(
            self._test_host_topology_spread_set
        )

    async def test_host_topology_spread_set_for_deployment(self):
        if not self.run_topology_spread_test:
            raise unittest.SkipTest("skip requested")
        await self._run_test_for_each_deployment_pod(
            self._test_host_topology_spread_set
        )

    async def test_does_not_modify_context_parent(self):
        if not self.run_context_modification_test:
            raise unittest.SkipTest("skip requested")
        self._make_all_dependencies_complete_immediately()
        old_parent = copy.deepcopy(self.ctx.parent)
        await self.cr.sm.ensure(self.ctx)
        if old_parent != self.ctx.parent:
            self.fail(
                "Something in this custom resources modifies the context. "
                "This is probably a really bad idea, please fix it."
            )


class DatabaseTestMixin():

    async def test_database_container_resources(self):
        # Assumes that the CRD includes the crd.#database definition.
        crd_spec = self.ctx.parent_spec
        expected_db_resources = crd_spec["database"]["resources"]
        expected_proxy_resources = crd_spec["database"]["proxy"]["resources"]

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        db_interface = interfaces.mysqlservice_interface(self.api_client)
        db_service, = await db_interface.list_(
            self.cr_namespace,
        )
        self.assertEqual(
            db_service["spec"]["resources"],
            expected_db_resources)
        self.assertEqual(
            db_service["spec"]["proxy"]["resources"],
            expected_proxy_resources)


class MessageQueueTestMixin():

    async def test_messagequeue_container_resources(self):
        # Assumes that the CRD includes the crd.#messagequeue definition.
        crd_spec = self.ctx.parent_spec
        expected_resources = crd_spec["messageQueue"]["resources"]

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        msgqueue_interface = interfaces.amqpserver_interface(self.api_client)
        msgqueue_server, = await msgqueue_interface.list_(
            self.cr_namespace,
        )
        self.assertEqual(
            msgqueue_server["spec"]["resources"],
            expected_resources)


class MemcachedTestMixin():

    async def test_memcached_container_resources(self):
        # Assumes that the CRD includes the crd.#memcached definition.
        crd_spec = self.ctx.parent_spec
        expected_resources = crd_spec["memcached"]["resources"]

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        memcached_interface = interfaces.memcachedservice_interface(
            self.api_client)
        memcached_service, = await memcached_interface.list_(
            self.cr_namespace,
        )
        self.assertEqual(
            memcached_service["spec"]["resources"],
            expected_resources)


class ReleaseAwareCustomResourceTestCase(CustomResourceTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        if cls is ReleaseAwareCustomResourceTestCase:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()

    async def test_sets_deployedopenstackversion_on_success(self):
        cr, = await self.ctx.parent_intf.list_(self.ctx.namespace)
        self.assertNotIn("installedRelease", cr.get("status", {}))

        self._make_all_dependencies_complete_immediately()
        await self.cr.reconcile(self.ctx, 0)

        cr, = await self.ctx.parent_intf.list_(self.ctx.namespace)
        self.assertEqual(
            version_utils.get_target_release(self.ctx),
            cr["status"]["installedRelease"]
        )


def find_secret_volume(podspec, secret_name):
    for volume in podspec.volumes:
        if volume.secret is None:
            continue
        if volume.secret.secret_name != secret_name:
            continue
        return volume.name
    raise LookupError(f"volume for secret {secret_name} not found")


def find_configmap_volume(podspec, configmap_name):
    for volume in podspec.volumes:
        if volume.config_map is None:
            continue
        if volume.config_map.name != configmap_name:
            continue
        return volume.name
    raise LookupError(f"volume for configMap {configmap_name} not found")


def find_pvc_volume(podspec, pvc_name):
    for volume in podspec.volumes:
        if volume.persistent_volume_claim is None:
            continue
        if volume.persistent_volume_claim.claim_name != pvc_name:
            continue
        return volume.name
    raise LookupError(f"volume for persistenVolumeClaim {pvc_name} not found")


def find_volume_mountpoint(podspec, volume_name, container_name):
    for container_candidate in podspec.containers:
        if container_candidate.name == container_name:
            container = container_candidate
            break
    else:
        raise LookupError(f"container {container_name} not found")

    for mount in container.volume_mounts:
        if mount.name != volume_name:
            continue
        return mount.mount_path

    raise LookupError(f"volume {volume_name} not mounted in {container_name}")


def find_projected_secret_mapping(
        podspec: kubernetes_asyncio.client.V1PodSpec,
        secret: str,
        container_name: str,
) -> typing.Mapping[
    str, typing.Collection[str]
]:
    """
    Build a mapping which maps the keys of a secret to all of its projected
    mountpoints inside a given container.

    Cavats:

    - Ignores non-projected volumes
    - Ignores secret keys which are not mapped explicitly via `items`
    """

    volume_mapping = {}

    for volume_candidate in podspec.volumes:
        if volume_candidate.projected is None:
            continue
        for source in volume_candidate.projected.sources:
            if source.secret is None:
                continue
            if source.secret.name != secret:
                continue
            for item in source.secret.items:
                volume_mapping.setdefault(volume_candidate.name, {})[
                    item.key
                ] = item.path

    if not volume_mapping:
        raise LookupError(
            f"secret {secret} is not projected into any volume"
        )

    mount_mapping = {}
    for volume_name, paths in volume_mapping.items():
        try:
            mountpoint = find_volume_mountpoint(podspec, volume_name,
                                                container_name)
        except LookupError:
            continue

        for secret_key, secret_path in paths.items():
            mount_mapping.setdefault(secret_key, []).append(
                f"{mountpoint}/{secret_path}"
            )

    if not mount_mapping:
        raise LookupError(
            f"secret {secret}, which is projected in volumes "
            f"{','.join(volume_mapping.keys())}, is not mounted in container"
            f" {container_name}"
        )

    return mount_mapping


class CustomL2UseResourceTestCase(CustomResourceTestCase):
    # Use this class as parent for all classes, testing resources that needs
    # l2 agents.
    @classmethod
    def setUpClass(cls) -> None:
        if cls is CustomL2UseResourceTestCase:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()

    async def test_delete_removes_annotation(self):
        annotations = {
            context.ANNOTATION_L2_MIGRATION_LOCK + self.ctx.parent_kind: '',
        }
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, annotations)
        await self.cr.sm.ensure(self.ctx)

        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )
        self.assertEqual(
            nodes.items[0].metadata.annotations,
            annotations,
        )
        await self.cr.api_state.delete(self.ctx, [])
        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )
        self.assertEqual(
            nodes.items[0].metadata.annotations,
            {},
        )

    async def test_lockl2_set_annotation(self):
        node_labels = self.default_node_setup
        for node in node_labels.keys():
            node_labels[node][context.LABEL_L2_REQUIRE_MIGRATION] = \
                'False'
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(node_labels, {})
        self.stop_l2lock_patches()
        await self.cr.sm.ensure(self.ctx)

        v1 = kubernetes_asyncio.client.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )

        self.assertEqual(
            nodes.items[0].metadata.annotations,
            {
                context.ANNOTATION_L2_MIGRATION_LOCK + self.ctx.parent_kind: '',  # noqa: E501
            },
        )
        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, 'True'
            ) == 'False'
        )
        self.start_l2lock_patches()


def unique_resources(seed: str) -> dict:
    """Create a unique resources object.

    :param index: A unique index value for this container.
    :returns: A valid container resources dict.

    The idea behind this is to be able to check that resource constraints
    from the CR end up with the correct container.
    """
    value = int.from_bytes(seed.encode(), 'little')
    # Add 1 to all values to avoid zero values.
    return {
        "limits": {"memory": f"{value % 1000 + 1}M"},
        "requests": {
            "cpu": f"{value % 20 + 1}",
            "memory": f"{value % 1000+ 1 }M"
        },
    }


def generate_resources_dict(*names: typing.List[str]) -> dict:
    """Build resources for multiple containers as dict

    For the dictionary key use the base name without the "path".
    """
    return {name.split(".")[-1]: unique_resources(name) for name in names}


def generate_db_resources(path: str = "database") -> dict:
    """Build resources for database service"""
    return generate_resources_dict(
        f"{path}.mariadb-galera",
        f"{path}.backup-creator",
        f"{path}.backup-shifter",
        f"{path}.mysqld-exporter",
    )


def generate_db_proxy_resources(path: str = "database.proxy") -> dict:
    """Build resources for database service proxy"""
    return generate_resources_dict(
        f"{path}.create-ca-bundle",
        f"{path}.haproxy",
        f"{path}.service-reload",
    )


def generate_amqp_resources(path: str = "messageQueue") -> dict:
    """Build resources for messagequeue service"""
    return generate_resources_dict(
        f"{path}.rabbitmq",
    )


def generate_memcached_resources(path: str = "memcached") -> dict:
    """Build resources for memcached service"""
    return generate_resources_dict(
        f"{path}.memcached",
    )


def container_resources(
        res: typing.Any,
        index: int,
        is_init: bool = False) -> dict:
    """Return the resources for a container as dict

    :param res: A kubernetes workload resource
        (deployment, sts, job, cronjob).
    :param index: The index of the container.
    :param is_init: Whether this is an init container.
    :returns: The resources as a dicutionary.

    This is mainly a shortcut to make tests more readable.
    """
    if hasattr(res, "spec"):
        # This is a proper object
        if hasattr(res.spec, "job_template"):
            # This is a CronJob
            spec = res.spec.job_template.spec.template.spec
        else:
            spec = res.spec.template.spec
        if is_init:
            return spec.init_containers[index].resources.to_dict()
        return spec.containers[index].resources.to_dict()
    else:
        # We assume it's a dict (ConfiguredDaemonSet)
        container = res["spec"]["template"]["spec"]["containers"][index]
        return container["resources"]
