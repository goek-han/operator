#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import random
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

from tests import testutils
import yaook.common.config
from yaook.common.config import ConfigValidationError, CueConfigReference


class Test_format_str(unittest.TestCase):
    def test_string_escaping(self):
        vals = [
            ("foo\nbar\tbaz\"'fnord", '"foo\\nbar\\tbaz\\"\'fnord"'),
            ("\\foo\\nbar\\baz\nfnord\\",
             '"\\\\foo\\\\nbar\\\\baz\\nfnord\\\\"'),
            ("\\(", '"\\\\("'),
            ("\x03\x00", '"\\x03\\x00"'),
        ]

        for raw, encoded in vals:
            self.assertEqual(yaook.common.config._format_str(raw),
                             encoded)


class Test_format_bytes(unittest.TestCase):
    def test_bytes_escaping(self):
        vals = [
            (b"foo\nbar\tbaz\"'fnord", "'foo\\nbar\\tbaz\"\\'fnord'"),
            (b"\\foo\\nbar\\baz\nfnord\\",
             "'\\\\foo\\\\nbar\\\\baz\\nfnord\\\\'"),
            (b"\\(", "'\\\\('"),
            (b"\x03\xfd\x00", "'\\x03\\xfd\\x00'"),
        ]

        for raw, encoded in vals:
            self.assertEqual(yaook.common.config._format_bytes(raw),
                             encoded)


class Test_format_struct_label(unittest.TestCase):
    def test_returns_valid_labels_verbatimly(self):
        values = [
            "abc",
            "_foo",
            "_#bar",
            "_#x0123",
            "#x123",
            "$foo",
            "_foo",
            "abc123",
        ]
        for v in values:
            self.assertEqual(
                yaook.common.config._format_struct_label(v),
                v,
            )

    def test_escapes_strange_labels_with__format_str(self):
        values = [
            "foo bar",
            "foo.bar",
            "01234",
            "|magic|",
            "_",
            "$",
            " foo",
        ]

        for v in values:
            self.assertEqual(
                yaook.common.config._format_struct_label(v),
                yaook.common.config._format_str(v),
            )


class Test_serialize_struct(unittest.TestCase):
    @unittest.mock.patch("yaook.common.config._format_struct_label")
    @unittest.mock.patch("yaook.common.config._serialize_value")
    def test_serializes_keys_and_values_appropriately(
            self,
            serialize_value,
            format_struct_label,
            ):
        format_struct_label.return_value = "serialized-key"
        serialize_value.return_value = "serialized-value"

        self.assertEqual(
            yaook.common.config._serialize_struct({
                sentinel.key: sentinel.value,
            }),
            '{\n'
            'serialized-key: serialized-value\n'
            '}',
        )

    @unittest.mock.patch("yaook.common.config._format_struct_label")
    @unittest.mock.patch("yaook.common.config._serialize_value")
    def test_with_multiple_fields(
            self,
            serialize_value,
            format_struct_label,
            ):
        format_struct_label.side_effect = lambda x: f"serialized-key-{x}"
        serialize_value.side_effect = lambda x: f"serialized-value-{x}"

        self.assertEqual(
            yaook.common.config._serialize_struct({
                sentinel.key1: sentinel.value1,
                sentinel.key2: sentinel.value2,
                sentinel.key3: sentinel.value3,
            }),
            '{\n'
            'serialized-key-sentinel.key1: serialized-value-sentinel.value1\n'
            'serialized-key-sentinel.key2: serialized-value-sentinel.value2\n'
            'serialized-key-sentinel.key3: serialized-value-sentinel.value3\n'
            '}',
        )


class Test_serialize_list(unittest.TestCase):
    @unittest.mock.patch("yaook.common.config._serialize_value")
    def test_serializes_values_appropriately(
            self,
            serialize_value,
            ):
        serialize_value.return_value = "serialized-value"

        self.assertEqual(
            yaook.common.config._serialize_list([sentinel.value]),
            '['
            'serialized-value'
            ']',
        )

    @unittest.mock.patch("yaook.common.config._serialize_value")
    def test_with_multiple_values(
            self,
            serialize_value,
            ):
        serialize_value.side_effect = lambda x: f"serialized-value-{x}"

        self.assertEqual(
            yaook.common.config._serialize_list([
                sentinel.value1,
                sentinel.value2,
                sentinel.value3,
            ]),
            '['
            'serialized-value-sentinel.value1, '
            'serialized-value-sentinel.value2, '
            'serialized-value-sentinel.value3'
            ']',
        )


class Test_serialize_value(unittest.TestCase):
    def test_unsupported_types(self):
        values = [object(), sentinel.foo, unittest.mock.Mock()]

        for v in values:
            with self.assertRaisesRegex(
                    TypeError,
                    r"unsupported type for serialisation to Cue: .+"):
                yaook.common.config._serialize_value(v)

    @unittest.mock.patch("yaook.common.config._format_str")
    def test_str(self, _format_str):
        _format_str.return_value = sentinel.result

        self.assertEqual(
            yaook.common.config._serialize_value("input string"),
            sentinel.result,
        )

        _format_str.assert_called_once_with("input string")

    @unittest.mock.patch("yaook.common.config._format_bytes")
    def test_bytes(self, _format_bytes):
        _format_bytes.return_value = sentinel.result

        self.assertEqual(
            yaook.common.config._serialize_value(b"input bytes"),
            sentinel.result,
        )

        _format_bytes.assert_called_once_with(b"input bytes")

    def test_bool(self):
        pairs = [
            (True, "true"),
            (False, "false"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_value(raw),
                serialized,
            )

    def test_int(self):
        pairs = [
            (-1024, "-1024"),
            (-1, "-1"),
            (0, "0"),
            (3, "3"),
            (23, "23"),
            (2342, "2342"),
        ]
        i = random.randint(-100000, 100000)
        pairs.append((i, str(i)))

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_value(raw),
                serialized,
                raw,
            )

    def test_float(self):
        pairs = [
            (-10.24, "-10.24"),
            (-1.0, "-1.0"),
            (-0.0, "-0.0"),
            (0.0, "0.0"),
            (3.0, "3.0"),
            (2.3, "2.3"),
            (234.2, "234.2"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_value(raw),
                serialized,
                raw,
            )

    def test_float_rejects_non_numbers(self):
        values = [
            float("-inf"),
            float("inf"),
            float("nan"),
        ]

        for raw in values:
            with self.assertRaisesRegex(
                    ValueError,
                    r"unsupported floating-point value for serialisation to "
                    r"Cue: .+"):
                yaook.common.config._serialize_value(raw)

    def test_none(self):
        self.assertEqual(
            yaook.common.config._serialize_value(None),
            "null",
        )

    @unittest.mock.patch("yaook.common.config._serialize_struct")
    def test_dict_uses_serialize_struct(self, serialize_struct):
        serialize_struct.return_value = sentinel.serialized

        self.assertEqual(
            yaook.common.config._serialize_value({
                "k": "v",
            }, 'module-foo', 'section-bar'),
            sentinel.serialized,
        )

        serialize_struct.assert_called_once_with({
            "k": "v",
        }, 'module-foo', 'section-bar')

    @unittest.mock.patch("yaook.common.config._serialize_list")
    def test_list_uses_serialize_struct(self, serialize_list):
        serialize_list.return_value = sentinel.serialized

        self.assertEqual(
            yaook.common.config._serialize_value([1, 2, 3]),
            sentinel.serialized,
        )

        serialize_list.assert_called_once_with([1, 2, 3])

    def test_cue_reference_is_unpacked(self):
        self.assertEqual(
            yaook.common.config._serialize_value(
                yaook.common.config.CueConfigReference(sentinel.ref),
            ),
            sentinel.ref,
        )


class Test_serialize_openstack_primitive_value(unittest.TestCase):
    def test_unsupported_types(self):
        values = [object(), sentinel.foo, unittest.mock.Mock()]

        for v in values:
            with self.assertRaisesRegex(
                    TypeError,
                    r"unsupported type for serialisation to OpenStack config"
                    r": .+"):
                yaook.common.config._serialize_openstack_primitive_value(v)

    def test_str(self):
        pairs = [
            ("foo", "foo"),
            ("123", "123"),
            ("abc def", "abc def"),
            (" abc def ", "' abc def '"),
            ("'tis some visitor", "''tis some visitor'"),
            ("$foo$$", "$$foo$$$$"),
            ("\\$foo$", "$foo$$"),
            ("\\\\$foo", "\\$foo"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_openstack_primitive_value(raw),
                serialized,
            )

    def test_str_rejects_newlines(self):
        with self.assertRaises(NotImplementedError):
            yaook.common.config._serialize_openstack_primitive_value(
                "foo\nbar",
            )

    def test_bool(self):
        pairs = [
            (True, "true"),
            (False, "false"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_openstack_primitive_value(raw),
                serialized,
            )

    def test_int(self):
        pairs = [
            (-1024, "-1024"),
            (-1, "-1"),
            (0, "0"),
            (3, "3"),
            (23, "23"),
            (2342, "2342"),
        ]
        i = random.randint(-100000, 100000)
        pairs.append((i, str(i)))

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_openstack_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float(self):
        pairs = [
            (-10.24, "-10.24"),
            (-1.0, "-1.0"),
            (-0.0, "-0.0"),
            (0.0, "0.0"),
            (3.0, "3.0"),
            (2.3, "2.3"),
            (234.2, "234.2"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_openstack_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float_rejects_non_numbers(self):
        values = [
            float("-inf"),
            float("inf"),
            float("nan"),
        ]

        for raw in values:
            with self.assertRaisesRegex(
                    ValueError,
                    r"unsupported floating-point value for serialisation to "
                    r"OpenStack config: .+"):
                yaook.common.config._serialize_openstack_primitive_value(raw)


@ddt.ddt
class Test_serialize_wsrep_provider_options(unittest.TestCase):
    def test_joins_simple_values(self):
        self.assertEqual(
            yaook.common.config._serialize_wsrep_provider_options({
                "k1": "v1",
                "k2": 2,
                "k3": "yes",
                "k4": "foo bar",
                "k5": 23.42,
            }),
            "k1=v1;k2=2;k3=yes;k4=foo bar;k5=23.42",
        )

    @ddt.data(object(), sentinel.foo, unittest.mock.Mock(),
              [], (), {})
    def test_rejects_complex_types(self, v):
        with self.assertRaisesRegex(
                TypeError,
                "unsupported value type for wsrep_provider_options: .+"):
            yaook.common.config._serialize_wsrep_provider_options({
                "k": v,
            })


@ddt.ddt
class Test_serialize_optimizer_switch(unittest.TestCase):
    def test_joins_simple_values(self):
        self.assertEqual(
            yaook.common.config._serialize_optimizer_switch({
                "k1": True,
                "k2": False
            }),
            "k1=on,k2=off",
        )


class Test_serialize_mysql_primitive_value(unittest.TestCase):
    def test_unsupported_types(self):
        values = [object(), sentinel.foo, unittest.mock.Mock(),
                  [], {}, ()]

        for v in values:
            with self.assertRaisesRegex(
                    TypeError,
                    r"unsupported type for serialisation to MySQL config"
                    r": .+"):
                yaook.common.config._serialize_mysql_primitive_value(v)

    def test_str(self):
        pairs = [
            ("foo", "foo"),
            ("123", "123"),
            ("abc def", "\"abc\\sdef\""),
            (" abc def ", "\"\\sabc\\sdef\\s\""),
            (" abc \"def\" ", "\"\\sabc\\s\\\"def\\\"\\s\""),
            ("'tis some visitor", "\"\\'tis\\ssome\\svisitor\""),
            ("line1\nline2", "\"line1\\nline2\""),
            ("\"magic\"", "\"\\\"magic\\\"\""),
            ("32M", "32M"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_mysql_primitive_value(raw),
                serialized,
                raw,
            )

    def test_bool(self):
        pairs = [
            (True, "1"),
            (False, "0"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_mysql_primitive_value(raw),
                serialized,
            )

    def test_int(self):
        pairs = [
            (-1024, "-1024"),
            (-1, "-1"),
            (0, "0"),
            (3, "3"),
            (23, "23"),
            (2342, "2342"),
        ]
        i = random.randint(-100000, 100000)
        pairs.append((i, str(i)))

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_mysql_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float(self):
        pairs = [
            (-10.24, "-10.24"),
            (-1.0, "-1.0"),
            (-0.0, "-0.0"),
            (0.0, "0.0"),
            (3.0, "3.0"),
            (2.3, "2.3"),
            (234.2, "234.2"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_mysql_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float_rejects_non_numbers(self):
        values = [
            float("-inf"),
            float("inf"),
            float("nan"),
        ]

        for raw in values:
            with self.assertRaisesRegex(
                    ValueError,
                    r"unsupported floating-point value for serialisation to "
                    r"MySQL config: .+"):
                yaook.common.config._serialize_mysql_primitive_value(raw)


class Testto_mysql_ini(unittest.TestCase):
    def test_serializes_primitive_values_in_sections(self):
        self.assertSequenceEqual(
            yaook.common.config.to_mysql_ini(
                {
                    "section1": {
                        "k1": "v1",
                        "k2": " v2 ",
                        "k3": "'tis time",
                    },
                    "section3": {
                        "k4": 2342,
                    },
                },
                "foo",
            ).split("\n"),
            [
                "[section1]",
                "k1=v1",
                "k2=\"\\sv2\\s\"",
                "k3=\"\\'tis\\stime\"",
                "",
                "[section3]",
                "k4=2342",
                "",
            ]
        )

    def test_treats_none_as_flag(self):
        self.assertSequenceEqual(
            yaook.common.config.to_mysql_ini(
                {
                    "section1": {
                        "k1": None,
                    },
                },
                "foo",
            ).split("\n"),
            [
                "[section1]",
                "k1",
                "",
            ]
        )

    def test_composes_wsrep_provider_options(self):
        self.assertSequenceEqual(
            yaook.common.config.to_mysql_ini(
                {
                    "section1": {
                        "wsrep_provider_options": {
                            "k1": "v1",
                            "k2": "foo bar",
                            "k3": 12.3,
                        },
                    },
                },
                "foo",
            ).split("\n"),
            [
                "[section1]",
                "wsrep_provider_options=\"k1=v1;k2=foo\\sbar;k3=12.3\"",
                "",
            ]
        )

    def test_composes_optimizer_switch(self):
        self.assertSequenceEqual(
            yaook.common.config.to_mysql_ini(
                {
                    "section1": {
                        "optimizer_switch": {
                            "k1": True,
                            "k2": False,
                            "k3": True,
                        },
                    },
                },
                "foo",
            ).split("\n"),
            [
                "[section1]",
                "optimizer_switch=k1=on,k2=off,k3=on",
                "",
            ]
        )


@ddt.ddt
class Test_flatten_cuttlefish(unittest.TestCase):
    @ddt.data(
        1,
        1.2,
        True,
        "foo",
    )
    def test_returns_primitive_values_unchanged(self, value):
        input_ = [
            ("k", value),
        ]

        self.assertEqual(
            list(yaook.common.config._flatten_cuttlefish(list(input_))),
            input_,
        )

    def test_emits_keys_of_nested_dictionaries_with_dot_separation(self):
        input_ = {
            "k1": {
                "k1_1": "v1",
                "k1_2": "v2",
                "k1_3": {
                    "k1_3_1": "v3",
                }
            },
            "k2": {
                "k2_1": "v4",
            }
        }

        self.assertCountEqual(
            yaook.common.config._flatten_cuttlefish(input_.items()),
            [
                ("k1.k1_1", "v1"),
                ("k1.k1_2", "v2"),
                ("k1.k1_3.k1_3_1", "v3"),
                ("k2.k2_1", "v4"),
            ],
        )

    def test_emits_keys_of_lists_as_dot_separated_numeric_subkeys(self):
        input_ = {
            "k1": [
                "v1",
                "v2",
                "v3",
            ],
            "k2": {
                "k2_1": [
                    "v4",
                    "v5",
                ]
            }
        }

        self.assertCountEqual(
            yaook.common.config._flatten_cuttlefish(input_.items()),
            [
                ("k1.1", "v1"),
                ("k1.2", "v2"),
                ("k1.3", "v3"),
                ("k2.k2_1.1", "v4"),
                ("k2.k2_1.2", "v5"),
            ]
        )


@ddt.ddt
class Test_serialize_cuttlefish_primitive_value(unittest.TestCase):
    def test_unsupported_types(self):
        values = [object(), sentinel.foo, unittest.mock.Mock(),
                  [], {}, ()]

        for v in values:
            with self.assertRaisesRegex(
                    TypeError,
                    r"unsupported type for serialization to Cuttlefish config"
                    r": .+"):
                yaook.common.config._serialize_cuttlefish_primitive_value(v)

    def test_str(self):
        pairs = [
            ("foo", "foo"),
            ("123", "123"),
            ("abc def", "abc def"),
            ("32M", "32M"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_cuttlefish_primitive_value(raw),
                serialized,
                raw,
            )

    @ddt.data(
        " foo",
        "foo ",
        "foo\nbar",
    )
    def test_invalid_str(self, value):
        with self.assertRaisesRegex(
                ValueError,
                r"string .+ cannot be serialized to Cuttlefish config"):
            yaook.common.config._serialize_cuttlefish_primitive_value(value)

    def test_bool(self):
        pairs = [
            (True, "true"),
            (False, "false"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_cuttlefish_primitive_value(raw),
                serialized,
            )

    def test_int(self):
        pairs = [
            (-1024, "-1024"),
            (-1, "-1"),
            (0, "0"),
            (3, "3"),
            (23, "23"),
            (2342, "2342"),
        ]
        i = random.randint(-100000, 100000)
        pairs.append((i, str(i)))

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_cuttlefish_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float(self):
        pairs = [
            (-10.24, "-10.24"),
            (-1.0, "-1.0"),
            (-0.0, "-0.0"),
            (0.0, "0.0"),
            (3.0, "3.0"),
            (2.3, "2.3"),
            (234.2, "234.2"),
        ]

        for raw, serialized in pairs:
            self.assertEqual(
                yaook.common.config._serialize_cuttlefish_primitive_value(raw),
                serialized,
                raw,
            )

    def test_float_rejects_non_numbers(self):
        values = [
            float("-inf"),
            float("inf"),
            float("nan"),
        ]

        for raw in values:
            with self.assertRaisesRegex(
                    ValueError,
                    r"unsupported floating-point value for serialization to "
                    r"Cuttlefish config: .+"):
                yaook.common.config._serialize_cuttlefish_primitive_value(raw)


class Testto_cuttlefish(unittest.TestCase):
    @unittest.mock.patch("yaook.common.config._flatten_cuttlefish")
    @unittest.mock.patch(
        "yaook.common.config._serialize_cuttlefish_primitive_value",
    )
    def test_flattens_and_serializes(
            self,
            _serialize_cuttlefish_primitive_value,
            _flatten_cuttlefish):
        _flatten_cuttlefish.return_value = [
            ("k1", sentinel.v1),
            ("k2", sentinel.v2),
            ("k3", sentinel.v3),
        ]

        def fake_serialize(v):
            return f"serialized_{v.name}"

        _serialize_cuttlefish_primitive_value.side_effect = fake_serialize

        result = yaook.common.config.to_cuttlefish({
            "data": sentinel.data,
        }, "foo")

        _flatten_cuttlefish.assert_called_once_with(
            {"data": sentinel.data}.items(),
        )

        self.assertCountEqual(
            [
                unittest.mock.call(sentinel.v1),
                unittest.mock.call(sentinel.v2),
                unittest.mock.call(sentinel.v3),
            ],
            _serialize_cuttlefish_primitive_value.mock_calls,
        )

        self.assertEqual(
            result.split("\n"),
            [
                "k1=serialized_v1",
                "k2=serialized_v2",
                "k3=serialized_v3",
            ]
        )


class Testto_openstack_conf(unittest.TestCase):
    def test_serializes_primitive_values_in_sections(self):
        self.assertSequenceEqual(
            yaook.common.config.to_openstack_conf(
                {
                    "section1": {
                        "k1": "v1",
                        "k2": " v2 ",
                        "k3": "'tis time",
                    },
                    "section3": {
                        "k4": 2342,
                    },
                },
                "foo",
            ).split("\n"),
            [
                "[section1]",
                "k1 = v1",
                "k2 = ' v2 '",
                "k3 = ''tis time'",
                "",
                "[section3]",
                "k4 = 2342",
                "",
            ]
        )

    def test_joins_lists_with_comma(self):
        self.assertSequenceEqual(
            yaook.common.config.to_openstack_conf(
                {
                    "section1": {
                        "k1": ["v1", " v2 ", "v3"],
                    },
                },
                "nova",
            ).split("\n"),
            [
                "[section1]",
                "k1 = v1,' v2 ',v3",
                "",
            ]
        )

    def test_joins_nova_pci_options_as_separate_keys(self):
        self.assertSequenceEqual(
            yaook.common.config.to_openstack_conf(
                {
                    "pci": {
                        "alias": ["v1", " v2 ", "v3"],
                    },
                },
                "nova",
            ).split("\n"),
            [
                "[pci]",
                "alias = v1",
                "alias = ' v2 '",
                "alias = v3",
                "",
            ]
        )

    def test_raises_if_multivalued_key_is_a_string(self):
        with self.assertRaisesRegexp(
                TypeError,
                r"multi-valued key alias in section pci must be a non-string "
                r"sequence, got .*"):
            yaook.common.config.to_openstack_conf(
                {
                    "pci": {
                        "alias": "foo",
                    },
                },
                "nova",
            )

    def test_raises_if_multivalued_key_is_not_a_sequence(self):
        with self.assertRaisesRegexp(
                TypeError,
                r"multi-valued key alias in section pci must be a non-string "
                r"sequence, got .*"):
            yaook.common.config.to_openstack_conf(
                {
                    "pci": {
                        "alias": {"foo": "bar"},
                    },
                },
                "nova",
            )

    def test_list_with_brackets_extra_type(self):
        self.assertSequenceEqual(
            yaook.common.config.to_openstack_conf(
                {
                    "DEFAULT": {
                        "k1": ["v1", " v2 "],
                        "enabled_import_methods": ["v1", " v2 "],
                    },
                    "image_import_opts": {
                        "image_import_plugins": ["plugin1", "plugin2"],
                    },
                },
                "glance",
            ).split("\n"),
            [
                "[DEFAULT]",
                "k1 = v1,' v2 '",
                "enabled_import_methods = [v1,' v2 ']",
                "",
                "[image_import_opts]",
                "image_import_plugins = [plugin1,plugin2]",
                "",
            ]
        )


class Testmysql_normalize_underscores(unittest.TestCase):
    def test_replaces_dashes_with_underscores(self):
        self.assertDictEqual(
            yaook.common.config.mysql_normalize_underscores({
                "section1": {
                    "key-1": sentinel.k1,
                    "key_2": sentinel.k2,
                },
                "section-2": {
                    "key-3": sentinel.k1,
                    "key_4": sentinel.k2,
                },
            }),
            {
                "section1": {
                    "key_1": sentinel.k1,
                    "key_2": sentinel.k2,
                },
                "section-2": {
                    "key_3": sentinel.k1,
                    "key_4": sentinel.k2,
                },
            }
        )


class TestConfig(unittest.TestCase):

    CONFIGINPUT = [
        {
            "DEFAULT": {
                "debug": True
            },
            "auth": {
                "methods": [
                    "password",
                    "token",
                ]
            }
        },
        {
            "DEFAULT": {
                "#transport_url_password": "abc123"
            },
            "database": {
                "#connection_password": "keystone"
            }
        },
        {
            "testreference": CueConfigReference("database"),
            "test.strange key": {
                "abc": "123"
            }
        }
    ]

    def setUp(self):
        self.maxDiff = None

    def test_genconfig(self):
        output = {
            'conf': {
                'DEFAULT': {
                    'debug': True,
                    'use_stderr': True,
                    'use_json': True
                },
                'auth': {
                    'methods': [
                        'password',
                        'token'
                    ]
                },
                'cache': {
                    'backend': 'oslo_cache.memcache_pool',
                    'enabled': True,
                    'backend_argument': ['memcached_expire_time:660'],
                },
                'database': {
                    'connection': 'mysql+pymysql://keystone:keystone@mysql-keystone-mariadb:3306/keystone?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt',  # noqa: E501
                    'connection_recycle_time': 280,
                },
                'oslo_middleware': {'enable_proxy_headers_parsing': True},
                'oslo_policy': {
                    'enforce_new_defaults': False,
                    'enforce_scope': False,
                    'policy_file': '/etc/keystone/policy.yaml'
                },
                'testreference': {
                    'connection': 'mysql+pymysql://keystone:keystone@mysql-keystone-mariadb:3306/keystone?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt',  # noqa: E501
                    'connection_recycle_time': 280,
                },
                'test.strange key': {
                    'abc': '123'
                }
            }
        }
        config = yaook.common.config._genconfig("keystone", self.CONFIGINPUT)
        self.assertDictEqual(config, output)

    def test_genconfig_exception(self):
        input = self.CONFIGINPUT
        input[0]["auth"]["methods"] = 123
        self.assertRaises(ConfigValidationError,
                          yaook.common.config._genconfig, "keystone", input)

    def test_build_config(self):
        config_key = "keystone.conf"
        expected_dict = {
            config_key: '[DEFAULT]\nuse_stderr = true\nuse_json = true\ndebug = true\n\n[auth]\nmethods = password,token\n\n[cache]\nbackend = oslo_cache.memcache_pool\nbackend_argument = memcached_expire_time:660\nenabled = true\n\n[database]\nconnection = mysql+pymysql://keystone:keystone@mysql-keystone-mariadb:3306/keystone?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt\nconnection_recycle_time = 280\n\n[oslo_middleware]\nenable_proxy_headers_parsing = true\n\n[oslo_policy]\nenforce_new_defaults = false\nenforce_scope = false\npolicy_file=/etc/keystone/policy.yaml\n\n[test.strange key]\nabc = 123\n\n[testreference]\nconnection = mysql+pymysql://keystone:keystone@mysql-keystone-mariadb:3306/keystone?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt\nconnection_recycle_time = 280\n'  # noqa: E501
        }
        expected_config = testutils._parse_config(expected_dict[config_key])

        observed_dict = yaook.common.config.build_config(
            {"keystone": yaook.common.config.OSLO_CONFIG.declare(
                self.CONFIGINPUT
            )}
        )
        observed_config = testutils._parse_config(observed_dict[config_key])

        self.assertEqual(expected_config, observed_config)
        self.assertEqual(len(expected_dict), len(observed_dict))
