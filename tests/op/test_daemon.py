#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import copy
import datetime
from ddt import ddt, data, unpack
import functools
import itertools
from kubernetes_asyncio.client import Configuration, rest
import random
import signal
import typing

import unittest
from unittest.mock import call, AsyncMock, MagicMock, sentinel
import yaook.op.daemon
from yaook.op.daemon import OperatorDaemon, ReconcileReason
from yaook.statemachine import (
    api_utils,
    context,
    CustomResource,
    watcher,
    exceptions,
)

T = typing.TypeVar("T")


class TestResource(CustomResource):
    API_GROUP = "apps.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "testresources"
    KIND = "TestResource"


@ddt
class TestOperatorDaemon(unittest.IsolatedAsyncioTestCase):
    service_watcher = "service_watcher"
    secret_watcher = "secret_watcher"
    compute_node_watcher = "compute_node_watcher"
    node_watcher = "node_watcher"
    job_watcher = "job_watcher"
    pod_watcher = "pod_watcher"
    statefulset_watcher = "statefulset_watcher"

    def setUp(self):
        self.config = Configuration()
        self.namespace = "test_namespace"
        self.runner_count = 3
        self.field_manager = f"operator.yaook.cloud:" \
                             f"{TestResource.PLURAL}.{TestResource.API_GROUP}"
        self.od = OperatorDaemon(
            config=self.config,
            namespace=self.namespace,
            runner_count=self.runner_count,
        )

    @unittest.mock.patch("yaook.op.tasks.RestartingTask")
    @unittest.mock.patch("yaook.op.tasks.TaskQueue")
    def test___init__(self, mock_task_queue, mock_restarting_task):
        operator_daemon = OperatorDaemon(
            config=self.config,
            namespace=self.namespace,
            runner_count=self.runner_count,
        )

        self.assertTrue(operator_daemon.logger, msg="No logger")
        self.assertEqual(operator_daemon._namespace, self.namespace)
        self.assertEqual(operator_daemon._config, self.config)

        self.assertTrue(operator_daemon._task_queue)
        mock_task_queue.assert_called_once_with()

        self.assertTrue(operator_daemon._task_queue_runners)
        mock_restarting_task.assert_has_calls(
            [
                unittest.mock.call(
                    operator_daemon._task_queue.run,
                    logger=operator_daemon.logger.getChild("runner1")),
                unittest.mock.call(
                    operator_daemon._task_queue.run,
                    logger=operator_daemon.logger.getChild("runner2")),
                unittest.mock.call(
                    operator_daemon._task_queue.run,
                    logger=operator_daemon.logger.getChild("runner3"))
            ],
        )

        empty_dict_attrs = [
            '_listeners', '_crs', '_cr_watches', '_reconcile_reasons'
        ]
        for attr in empty_dict_attrs:
            msg = f"Expected empty {attr}. " \
                  f"Observed: '{getattr(operator_daemon, attr)}'"
            self.assertEqual(getattr(operator_daemon, attr), {}, msg=msg)

    def test__add_reconcile_reason_existing_cr(self):
        existing_cr = TestResource(logger=self.od.logger)
        existing_namespace = self.namespace
        existing_name = "test_name"
        existing_reason, new_reason = random.sample(list(ReconcileReason), 2)
        self.od._reconcile_reasons = {
            (existing_cr, existing_namespace, existing_name): {existing_reason}
        }
        expected_reasons = {existing_reason, new_reason}

        self.od._add_reconcile_reason(
            existing_cr, existing_namespace, existing_name, new_reason
        )

        self.assertEqual(len(self.od._reconcile_reasons), 1)
        observed_reasons = self.od._reconcile_reasons[
            (existing_cr, existing_namespace, existing_name)
        ]
        self.assertEqual(expected_reasons, observed_reasons)

    def test__add_reconcile_reason_non_existing_cr(self):
        new_cr = TestResource(logger=self.od.logger)
        new_namespace = self.namespace
        new_name = "test_name"
        new_reason = random.choice(list(ReconcileReason))
        expected_reasons = {new_reason}

        self.od._add_reconcile_reason(
            new_cr, new_namespace, new_name, new_reason
        )

        self.assertEqual(len(self.od._reconcile_reasons), 1)
        observed_reasons = self.od._reconcile_reasons[
            (new_cr, new_namespace, new_name)
        ]
        self.assertEqual(expected_reasons, observed_reasons)

    def test__pop_reconcile_reasons_existing_cr(self):
        existing_cr = TestResource(logger=self.od.logger)
        existing_namespace = self.namespace
        existing_name = "test_name"
        existing_reason = random.choice(list(ReconcileReason))
        self.od._reconcile_reasons = {
            (existing_cr, existing_namespace, existing_name): {existing_reason}
        }
        expected_reasons = {existing_reason}

        observed_reasons = self.od._pop_reconcile_reasons(
            existing_cr, existing_namespace, existing_name
        )

        self.assertEqual(self.od._reconcile_reasons, {})
        self.assertEqual(expected_reasons, observed_reasons)

    def test__pop_reconcile_reasons_non_existing_cr(self):
        other_cr = TestResource(logger=self.od.logger)
        other_reason = random.choice(list(ReconcileReason))
        namespace = self.namespace
        name = "test_name"
        self.od._reconcile_reasons = {
            (other_cr, namespace, name): {other_reason}
        }

        cr = TestResource(logger=self.od.logger)

        observed_reasons = self.od._pop_reconcile_reasons(cr, namespace, name)

        self.assertEqual(len(self.od._reconcile_reasons), 1)
        self.assertIn((other_cr, namespace, name), self.od._reconcile_reasons)
        self.assertNotIn((cr, namespace, name), self.od._reconcile_reasons)
        self.assertEqual(observed_reasons, set())

    @unittest.mock.patch("yaook.op.daemon.PatchingApiClient")
    async def test__make_client(self, mock_api_client):
        client = self.od._make_client()

        mock_api_client.assert_called_once_with(self.config)
        self.assertTrue(hasattr(client, "__aenter__"))
        self.assertTrue(hasattr(client, "__aexit__"))

    async def test__do_schedule_periodic_reconcile(self):
        with contextlib.ExitStack() as stack:

            mock_sleep = stack.enter_context(unittest.mock.patch(
                "asyncio.sleep"
            ))

            mock_crs = [MagicMock() for _ in range(2)]
            watchers_raw = {
                mock_crs[i]: [
                    {
                        "metadata": {
                            "namespace": f"cr{i}_ns0",
                            "name": f"cr{i}_n0",
                        }
                    },
                ]
                for i in range(len(mock_crs))
            }

            expected_num_iterations = sum([len(watchers_raw[cr])
                                           for cr in mock_crs])

            mock_watchers = []
            for cr, watcher_raws in watchers_raw.items():
                mock_watcher = MagicMock()
                mock_watcher.get_all.return_value = [
                    (raw, 'dummy') for raw in watcher_raws
                ]
                self.od._cr_watches[cr] = mock_watcher
                mock_watchers.append(mock_watcher)

            expected_add_reconcile_reason_calls = [
                call(
                    cr, raw["metadata"]["namespace"], raw["metadata"]["name"],
                    yaook.op.daemon.ReconcileReason.INTERVAL
                )
                for cr, raws in watchers_raw.items()
                for raw in raws
            ]

            mock_add_reconcile_reason = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.daemon.OperatorDaemon._add_reconcile_reason"
                ))

            expected_task_queue_push_calls = [
                call(
                    self.od._reconcile_cr,
                    (cr, raw["metadata"]["namespace"], raw["metadata"]["name"])
                )
                for cr, raws in watchers_raw.items()
                for raw in raws
            ]

            mock_task_queue_push = stack.enter_context(unittest.mock.patch(
                "yaook.op.tasks.TaskQueue.push"
            ))

            await self.od._do_schedule_periodic_reconcile()

            for mock_watcher in mock_watchers:
                mock_watcher.get_all.assert_called_once()
            mock_add_reconcile_reason.assert_has_calls(
                expected_add_reconcile_reason_calls, any_order=True
            )
            mock_task_queue_push.assert_has_calls(
                expected_task_queue_push_calls, any_order=True
            )
            self.assertEqual(
                mock_task_queue_push.call_count,
                expected_num_iterations
            )
            mock_sleep.assert_called_once()
            delay = mock_sleep.call_args[0][0]
            self.assertGreaterEqual(delay, 3500)
            self.assertLessEqual(delay, 3700)

    @unpack
    @data(
        (True, context.Phase.UPDATING),
        (True, context.Phase.CREATED),
        (True, context.Phase.BACKING_OFF),
        (False, context.Phase.UPDATING),
        (False, context.Phase.CREATED),
        (False, context.Phase.BACKING_OFF),
    )
    def test__needs_self_reconcile_resource_changed(
            self, status_exists, phase):
        new_raw = {
            "metadata": {
                "generation": "new_gen",
            },
        }
        if status_exists:
            new_raw["status"] = {
                "observedGeneration": "old_gen",
                "phase": phase.value,
            }

        self.assertIs(self.od._needs_self_reconcile(new_raw), True)

    @data(
        context.Phase.WAITING_FOR_DEPENDENCY,
        context.Phase.UPDATED,
    )
    def test__needs_self_reconcile_resource_not_changed(self, phase):
        new_raw = {
            "metadata": {
                "generation": "old_gen",
            },
            "status": {
                "observedGeneration": "old_gen",
                "phase": phase.value,
            }
        }

        self.assertIs(self.od._needs_self_reconcile(new_raw), False)

    def test__needs_self_reconcile_prev_not_successful(self):
        new_raw = {
            "metadata": {
                "generation": "old_gen",
            },
            "status": {
                "observedGeneration": "old_gen",
                "phase": context.Phase.BACKING_OFF.value,
            }
        }

        self.assertIs(self.od._needs_self_reconcile(new_raw), True)

    def test__needs_interval_reconcile_old_last_reconcile(self):
        new_raw = {
            "status": {
                "conditions": [{
                    "type": "Converged",
                    "lastUpdateTime": "2000-01-01T00:00:01Z",
                }],
            }
        }

        self.assertTrue(self.od._needs_interval_reconcile(new_raw))

    def test__needs_interval_reconcile_current_last_reconcile(self):
        new_raw = {
            "status": {
                "conditions": [{
                    "type": "Converged",
                    "lastUpdateTime": api_utils.format_timestamp(
                        datetime.datetime.now() - datetime.timedelta(seconds=5)
                    ),
                }],
            }
        }

        self.assertFalse(self.od._needs_interval_reconcile(new_raw))

    @unittest.mock.patch("yaook.op.daemon.get_cr_field_manager")
    @unittest.mock.patch("yaook.statemachine.CustomResource")
    @unittest.mock.patch("yaook.op.daemon.get_cr_logger")
    @unittest.mock.patch("yaook.statemachine.Context")
    def test__build_cr_context(
            self, mock_context, mock_get_cr_logger, mock_resource,
            mock_get_cr_field_manager):

        expected_namespace = self.namespace
        expected_cr = mock_resource
        expected_name = "instance name"
        instance = {
            "metadata": {
                "name": expected_name,
            },
        }

        expected_api_client = MagicMock()
        expected_parent = instance
        expected_instance = None

        expected_parent_intf = "parent interface"
        mock_resource.get_resource_interface.return_value = \
            expected_parent_intf

        expected_logger = MagicMock()
        mock_get_cr_logger.return_value = expected_logger

        expected_field_manager = self.field_manager
        mock_get_cr_field_manager.return_value = expected_field_manager

        expected_context = "return value"
        mock_context.return_value = expected_context

        observed_context = self.od._build_cr_context(
            expected_api_client,
            expected_cr,
            namespace=expected_namespace,
            instance=instance,
        )

        self.assertEqual(expected_context, observed_context)
        mock_context.assert_called_once_with(
            parent=expected_parent,
            parent_intf=expected_parent_intf,
            namespace=expected_namespace,
            api_client=expected_api_client,
            instance=expected_instance,
            instance_data=None,
            logger=expected_logger,
            field_manager=expected_field_manager,
        )
        mock_get_cr_logger.assert_called_once_with(
            self.od.logger,
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_resource.get_resource_interface.assert_called_once_with(
            expected_api_client,
        )
        mock_get_cr_field_manager.assert_called_once_with(expected_cr)

    @unpack
    @data(
        # [reasons, needs_self_reconcile, needs_interval_reconcile],
        [set(), True, False],
        [set(), False, False],
        [{ReconcileReason.OBJECT_MODIFIED}, True, False],
        [{ReconcileReason.OBJECT_MODIFIED}, False,
            False],  # reconcile() not called
        [{ReconcileReason.OBJECT_ADDED}, True, False],
        [{ReconcileReason.OBJECT_ADDED}, False, False],
        [{ReconcileReason.OBJECT_MODIFIED, ReconcileReason.LISTENER}, True,
            False],
        [{ReconcileReason.OBJECT_MODIFIED, ReconcileReason.LISTENER}, False,
            False],
        [{ReconcileReason.INTERVAL}, False, False],  # reconcile() not called
        [{ReconcileReason.INTERVAL}, False, True],
        [{ReconcileReason.OBJECT_MODIFIED, ReconcileReason.INTERVAL}, True,
            False],
    )
    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._build_cr_context")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._needs_interval_reconcile")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._needs_self_reconcile")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._pop_reconcile_reasons")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._make_client")
    async def test__reconcile_cr_no_exception(
            self, reasons, needs_self_reconcile, needs_interval_reconcile,
            mock_make_client, mock_pop_reasons, mock_needs_reconcile,
            mock_interval_reconcile,
            mock_build_cr_context, mock_task_queue_push):
        """Testing the cases when no methods called during _reconcile_cr()
        raises an exception."""

        # We should not call reconcile is when OBJECT_MODIFIED
        # is the only reason and needs_self_reconcile is False causing the
        # reason to be discarded.
        expects_call_to_reconcile = not (
            reasons == {ReconcileReason.OBJECT_MODIFIED} and
            not needs_self_reconcile
        )
        # We should also not call reconcile if INTERVAL is the only reason and
        # needs_interval_reconcile is False causing the reason to be discarded.
        expects_call_to_reconcile = expects_call_to_reconcile and not (
            reasons == {ReconcileReason.INTERVAL} and
            not needs_interval_reconcile
        )

        expected_namespace = self.namespace
        expected_name = "test name"

        expected_generation = "old_generation_val"
        expected_obj_mapping = {
            "metadata": {
                "generation": expected_generation,
            }
        }
        expected_api_client = MagicMock()

        mock_resource = MagicMock()
        expected_cr = mock_resource

        amock_resource_interface = AsyncMock()
        amock_resource_interface.read.return_value = expected_obj_mapping
        mock_resource.get_resource_interface.return_value = \
            amock_resource_interface

        async def async_reconcile_side_effect(ctx, generation):
            return False

        mock_resource.reconcile.side_effect = async_reconcile_side_effect

        mock_pop_reasons.return_value = reasons
        mock_needs_reconcile.return_value = needs_self_reconcile
        mock_interval_reconcile.return_value = needs_interval_reconcile

        mock_make_client.return_value = expected_api_client

        expected_context = MagicMock()
        mock_build_cr_context.return_value = expected_context

        await self.od._reconcile_cr(
            expected_cr,
            expected_namespace,
            expected_name,
        )

        mock_make_client.assert_called_once_with()
        mock_pop_reasons.assert_called_once_with(
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_needs_reconcile.assert_called_once_with(expected_obj_mapping)

        if expects_call_to_reconcile:
            mock_resource.reconcile.assert_called_once_with(
                expected_context, expected_generation
            )
        else:
            mock_resource.reconcile.assert_not_called()
            mock_task_queue_push.assert_not_called()

    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._build_cr_context")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._needs_interval_reconcile")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._needs_self_reconcile")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._pop_reconcile_reasons")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._make_client")
    async def test__reconcile_cr_skips_if_pause_annotation_is_set(
            self,
            mock_make_client, mock_pop_reasons, mock_needs_reconcile,
            mock_interval_reconcile,
            mock_build_cr_context, mock_task_queue_push):
        """Testing the cases when no methods called during _reconcile_cr()
        raises an exception."""

        expected_namespace = self.namespace
        expected_name = "test name"

        expected_generation = "old_generation_val"
        expected_obj_mapping = {
            "metadata": {
                "generation": expected_generation,
                "annotations": {
                    context.ANNOTATION_PAUSE: "",
                }
            }
        }
        expected_api_client = MagicMock()

        mock_resource = MagicMock()
        expected_cr = mock_resource

        amock_resource_interface = AsyncMock()
        amock_resource_interface.read.return_value = expected_obj_mapping
        mock_resource.get_resource_interface.return_value = \
            amock_resource_interface

        async def async_reconcile_side_effect(ctx, generation):
            return False

        mock_resource.reconcile.side_effect = async_reconcile_side_effect

        mock_pop_reasons.return_value = {sentinel.some_reason}

        mock_make_client.return_value = expected_api_client

        expected_context = MagicMock()
        mock_build_cr_context.return_value = expected_context

        await self.od._reconcile_cr(
            expected_cr,
            expected_namespace,
            expected_name,
        )

        mock_make_client.assert_called_once_with()
        mock_pop_reasons.assert_called_once_with(
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_needs_reconcile.assert_not_called()

        mock_resource.reconcile.assert_not_called()
        mock_task_queue_push.assert_not_called()

    @data(404, 400)
    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._pop_reconcile_reasons")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._make_client")
    async def test__reconcile_cr_read_raises(
            self, status,
            mock_make_client, mock_pop_reasons, mock_task_queue_push):
        """Testing the cases when resource_interface.read() raises."""

        expected_namespace = self.namespace
        expected_name = "test name"

        expected_api_client = MagicMock()
        mock_make_client.return_value.__aenter__.return_value = \
            expected_api_client

        mock_resource = MagicMock()
        expected_cr = mock_resource

        amock_resource_interface = AsyncMock()
        amock_resource_interface.read.side_effect = \
            rest.ApiException(status=status)
        mock_resource.get_resource_interface.return_value = \
            amock_resource_interface

        if status == 404:
            await self.od._reconcile_cr(
                expected_cr,
                expected_namespace,
                expected_name,
            )
        else:
            with self.assertRaises(rest.ApiException):
                await self.od._reconcile_cr(
                    expected_cr, expected_namespace, expected_name)

        mock_pop_reasons.assert_called_once_with(
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_make_client.assert_called_once_with()
        mock_resource.get_resource_interface \
            .assert_called_once_with(expected_api_client)
        amock_resource_interface.read \
            .assert_called_once_with(expected_namespace, expected_name)
        mock_resource.reconcile.assert_not_called()
        mock_task_queue_push.assert_not_called()

    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._add_reconcile_reason")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._build_cr_context")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._pop_reconcile_reasons")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._make_client")
    async def test__reconcile_cr_reconcile_raises(
            self, mock_make_client, mock_pop_reasons,
            mock_build_cr_context, mock_add_reason, mock_task_queue_push):
        """Testing the cases when cr_obj.reconcile() raises."""

        class TestException(Exception):
            pass

        expected_namespace = self.namespace
        expected_name = "test name"

        expected_generation = "old_generation_val"
        expected_obj_mapping = {
            "metadata": {
                "generation": expected_generation,
            }
        }

        expected_api_client = MagicMock()
        mock_make_client.return_value.__aenter__.return_value = \
            expected_api_client

        mock_resource = MagicMock()
        expected_cr = mock_resource

        amock_resource_interface = AsyncMock()
        amock_resource_interface.read.return_value = expected_obj_mapping
        mock_resource.get_resource_interface.return_value = \
            amock_resource_interface

        mock_resource.reconcile.side_effect = TestException()

        expected_context = MagicMock()
        mock_build_cr_context.return_value = expected_context

        with self.assertRaises(TestException):
            await self.od._reconcile_cr(
                expected_cr, expected_namespace, expected_name)

        mock_pop_reasons.assert_called_once_with(
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_make_client.assert_called_once_with()
        mock_resource.get_resource_interface \
            .assert_called_once_with(expected_api_client)
        amock_resource_interface.read \
            .assert_called_once_with(expected_namespace, expected_name)
        mock_resource.reconcile.assert_called_once_with(
            expected_context, expected_generation
        )
        mock_add_reason.assert_called_once_with(
            expected_cr, expected_namespace, expected_name,
            ReconcileReason.RETRY
        )
        mock_task_queue_push.assert_not_called()

    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._add_reconcile_reason")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._build_cr_context")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._pop_reconcile_reasons")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._make_client")
    async def test__reconcile_cr_reconcile_again_if_trigger_reconcile(
            self, mock_make_client, mock_pop_reasons,
            mock_build_cr_context, mock_add_reason, mock_task_queue_push):

        expected_namespace = self.namespace
        expected_name = "test name"

        expected_generation = "old_generation_val"
        expected_obj_mapping = {
            "metadata": {
                "generation": expected_generation,
            }
        }

        expected_api_client = MagicMock()
        mock_make_client.return_value.__aenter__.return_value = \
            expected_api_client

        mock_resource = MagicMock()
        expected_cr = mock_resource

        amock_resource_interface = AsyncMock()
        amock_resource_interface.read.return_value = expected_obj_mapping
        mock_resource.get_resource_interface.return_value = \
            amock_resource_interface

        mock_resource.reconcile.side_effect = exceptions.TriggerReconcile("")

        expected_context = MagicMock()
        mock_build_cr_context.return_value = expected_context

        await self.od._reconcile_cr(
            expected_cr, expected_namespace, expected_name)

        mock_pop_reasons.assert_called_once_with(
            expected_cr,
            expected_namespace,
            expected_name,
        )
        mock_make_client.assert_called_once_with()
        mock_resource.get_resource_interface \
            .assert_called_once_with(expected_api_client)
        amock_resource_interface.read \
            .assert_called_once_with(expected_namespace, expected_name)
        mock_resource.reconcile.assert_called_once_with(
            expected_context, expected_generation
        )
        mock_add_reason.assert_called_once_with(
            expected_cr, expected_namespace, expected_name,
            ReconcileReason.RETRY
        )
        mock_task_queue_push.assert_called_once_with(
            self.od._reconcile_cr,
            (expected_cr, expected_namespace, expected_name),
        )

    def test__judge_cr_watch_event_returns_none_for_deletion(self):
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.DELETED

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertIsNone(judgement)

    def test__judge_cr_watch_event_returns_OBJECT_ADDED_for_creation(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.ADDED

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_ADDED)

    def test__judge_cr_watch_event_returns_OBJECT_ADDED_if_old_object_is_missing(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = None

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_ADDED)

    def test__judge_cr_watch_event_returns_None_if_modified_but_no_critical_fields_modified(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {
                "generation": sentinel.generation1,
            },
        }
        ev.raw_object = ev.old_raw_object

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertIsNone(judgement)

    def test__judge_cr_watch_event_returns_OBJECT_ADDED_if_pause_annotation_disappeared(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {
                "generation": sentinel.generation1,
                "annotations": {
                    context.ANNOTATION_PAUSE: "",
                },
            },
        }
        ev.raw_object = copy.deepcopy(ev.old_raw_object)
        del ev.raw_object["metadata"]["annotations"][context.ANNOTATION_PAUSE]

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_ADDED)

    def test__judge_cr_watch_event_returns_None_if_pause_annotation_is_present_on_both(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {
                "generation": sentinel.generation1,
                "annotations": {
                    context.ANNOTATION_PAUSE: "",
                },
            },
        }
        ev.raw_object = ev.old_raw_object

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertIsNone(judgement)

    def test__judge_cr_watch_event_returns_OBJECT_MODIFIED_if_modified_and_generation_changed(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {
                "generation": sentinel.generation1,
            },
        }
        ev.raw_object = copy.deepcopy(ev.old_raw_object)
        ev.raw_object["metadata"]["generation"] = sentinel.generation2

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_MODIFIED)

    def test__judge_cr_watch_event_returns_OBJECT_MODIFIED_if_modified_and_generation_missing(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {},
        }
        ev.raw_object = copy.deepcopy(ev.old_raw_object)

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_MODIFIED)

    def test__judge_cr_watch_event_returns_OBJECT_MODIFIED_if_modified_and_deletionTimestamp_changed(self):  # noqa:E501
        # arrange event
        ev = unittest.mock.Mock(watcher.StatefulWatchEvent)
        ev.type_ = watcher.EventType.MODIFIED

        # arrange resource diff
        ev.old_object = sentinel.old
        ev.old_raw_object = {
            "metadata": {
                "generation": sentinel.generation1,
            },
        }
        ev.raw_object = copy.deepcopy(ev.old_raw_object)
        ev.raw_object["metadata"]["deletionTimestamp"] = sentinel.ts

        # act
        judgement = self.od._judge_cr_watch_event(sentinel.cr_obj, ev)

        # assert
        self.assertEqual(judgement, ReconcileReason.OBJECT_MODIFIED)

    async def test__watch_cr_uses__judge_cr_watch_event(self):
        with contextlib.ExitStack() as stack:
            # arrange the mocks for the environment
            expected_cr = MagicMock()
            mock_make_client = stack.enter_context(unittest.mock.patch(
                "yaook.op.daemon.OperatorDaemon._make_client"
            ))

            expected_api_client = MagicMock()
            mock_make_client.return_value.__aenter__.return_value = \
                expected_api_client

            mock_add_reconcile_reason = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.daemon.OperatorDaemon._add_reconcile_reason"
                )
            )

            mock_task_queue_push = stack.enter_context(unittest.mock.patch(
                "yaook.op.tasks.TaskQueue.push"
            ))

            mock_judge_cr_event = stack.enter_context(
                unittest.mock.patch.object(
                    self.od, "_judge_cr_watch_event",
                )
            )

            # arrange a streamer to submit events to the watch
            event_queue = asyncio.Queue()
            startup_complete = asyncio.Event()

            async def event_streamer(_):
                startup_complete.set()
                while True:
                    item = await event_queue.get()
                    try:
                        if item == sentinel.stop_stream:
                            return
                        yield item
                    finally:
                        event_queue.task_done()

            mock_watcher = MagicMock()
            self.od._cr_watches[expected_cr] = mock_watcher

            mock_watcher.stream_events.side_effect = event_streamer

            # arrange the running watch task
            watch_task = asyncio.create_task(
                self.od._watch_cr(expected_cr)
            )

            # assert that the startup has executed correctly
            await startup_complete.wait()
            self.assertFalse(watch_task.done())
            mock_make_client.assert_called_once_with()
            mock_watcher.stream_events \
                .assert_called_once_with(expected_api_client)

            # act+assert by pushing events and checking the results

            # TEST: no reconcile reason
            mock_judge_cr_event.return_value = None
            ev = unittest.mock.Mock()
            event_queue.put_nowait(ev)

            # wait for the event to be processed before asserting
            await event_queue.join()
            mock_judge_cr_event.assert_called_once_with(
                expected_cr,
                ev,
            )
            mock_add_reconcile_reason.assert_not_called()
            mock_task_queue_push.assert_not_called()

            # cleanup
            mock_add_reconcile_reason.reset_mock()
            mock_task_queue_push.reset_mock()
            mock_judge_cr_event.reset_mock()

            # TEST: non-None reconcile reason
            mock_judge_cr_event.return_value = sentinel.reason
            ev = unittest.mock.Mock()
            event_queue.put_nowait(ev)

            # wait for the event to be processed before asserting
            await event_queue.join()
            mock_judge_cr_event.assert_called_once_with(
                expected_cr,
                ev,
            )
            mock_add_reconcile_reason.assert_called_once_with(
                expected_cr,
                ev.reference.namespace,
                ev.reference.name,
                sentinel.reason,
            )
            mock_task_queue_push.assert_called_once_with(
                self.od._reconcile_cr,
                (expected_cr, ev.reference.namespace, ev.reference.name),
            )

        event_queue.put_nowait(sentinel.stop_stream)
        self.assertTrue(await watch_task)

    async def test__watch_builtin(self):
        with contextlib.ExitStack() as stack:
            expected_event_key = ("first", "second", "third")
            expected_events = ["event1", "event2"]

            expected_stream_factory_callable = MagicMock()
            expected_stream_factory = MagicMock()
            expected_stream_factory_callable.return_value = \
                expected_stream_factory

            expected_api_client = MagicMock()
            mock_make_client = stack.enter_context(unittest.mock.patch(
                "yaook.op.daemon.OperatorDaemon._make_client"
            ))
            mock_make_client.return_value.__aenter__.return_value = \
                expected_api_client

            async def stream_events_side_effect(api_client):
                for ev in expected_events:
                    yield ev

            mock_stateful_watcher = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.StatefulWatcher"
            ))
            expected_watcher = MagicMock()
            mock_stateful_watcher[T].return_value = expected_watcher
            expected_watcher.stream_events.side_effect = \
                stream_events_side_effect

            expected_dispatch_calls = [
                call(expected_api_client, expected_event_key, event)
                for event in expected_events
            ]

            mock_dispatch_event = stack.enter_context(unittest.mock.patch(
                "yaook.op.daemon.OperatorDaemon._dispatch_event"
            ))

            ret_val = await self.od._watch_builtin(
                expected_event_key, expected_stream_factory_callable)

            expected_stream_factory_callable \
                .assert_called_with(namespace=self.od._namespace)
            mock_stateful_watcher[T] \
                .assert_called_once_with(
                    expected_stream_factory,
                    restore_state_on_restart=True,
                )
            mock_make_client.assert_called_once_with()
            mock_stateful_watcher[T].return_value.stream_events \
                .assert_called_once_with(expected_api_client)
            mock_dispatch_event.assert_has_calls(
                expected_dispatch_calls, any_order=True
            )
            self.assertEqual(mock_dispatch_event.call_count,
                             len(expected_events))
            self.assertIs(ret_val, True)

    async def test__watch_external(self):
        with contextlib.ExitStack() as stack:
            expected_events = ["event1", "event2"]

            expected_stream_factory_callable = MagicMock()
            expected_stream_factory = MagicMock()
            expected_stream_factory_callable.return_value = \
                expected_stream_factory

            expected_api_client = MagicMock()
            mock_make_client = stack.enter_context(unittest.mock.patch(
                "yaook.op.daemon.OperatorDaemon._make_client"
            ))
            mock_make_client.return_value.__aenter__.return_value = \
                expected_api_client

            async def stream_events_side_effect():
                for ev in expected_events:
                    yield ev

            mock_external_watcher = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.ExternalWatcher"
            ))
            mock_external_watcher[T] = MagicMock()
            mock_external_watcher[T].stream_events.side_effect = \
                stream_events_side_effect

            expected_dispatch_calls = [
                call(expected_api_client, mock_external_watcher[T], event)
                for event in expected_events
            ]

            mock_dispatch_external_event = stack.enter_context(
                unittest.mock.patch(
                    "yaook.op.daemon.OperatorDaemon._dispatch_external_event"
                ))

            ret_val = await self.od._watch_external(
                mock_external_watcher[T])

            mock_make_client.assert_called_once_with()
            mock_external_watcher[T].stream_events.assert_called_once_with()
            mock_dispatch_external_event.assert_has_calls(
                expected_dispatch_calls, any_order=True
            )
            self.assertEqual(mock_dispatch_external_event.call_count,
                             len(expected_events))
            self.assertIs(ret_val, True)

    @data(True, False)
    @unittest.mock.patch("yaml.dump")
    @unittest.mock.patch(
        "yaook.op.daemon.OperatorDaemon._add_reconcile_reason")
    @unittest.mock.patch("yaook.op.tasks.TaskQueue.push")
    def test__deliver_event(
            self,
            listener_return_value,
            mock_task_queue_push,
            mock_add_reconcile_reason,
            mock_yaml_dump,
    ):
        expected_name = "parent name"
        expected_namespace = self.namespace
        expected_cr = MagicMock()
        expected_event = MagicMock()

        expected_ctx = MagicMock()
        expected_ctx.namespace = expected_namespace
        expected_ctx.parent_name = expected_name

        mock_yaml_dump.return_value = "yaml"

        mock_listener = MagicMock(return_value=listener_return_value)

        self.od._deliver_event(
            mock_listener,
            expected_cr,
            expected_ctx,
            expected_event,
        )

        mock_listener.assert_called_once_with(expected_ctx, expected_event)
        if listener_return_value:
            mock_add_reconcile_reason.assert_called_once_with(
                expected_cr, expected_namespace, expected_name,
                ReconcileReason.LISTENER
            )
            mock_task_queue_push.assert_called_once_with(
                self.od._reconcile_cr,
                (expected_cr, expected_namespace, expected_name),
            )
        else:
            mock_add_reconcile_reason.assert_not_called()
            mock_task_queue_push.assert_not_called()

    @unittest.mock.patch("yaook.op.daemon.get_cr_field_manager")
    @unittest.mock.patch("yaook.statemachine.Context")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._deliver_event")
    def test__broadcast_event(
            self, mock_deliver_event, mock_context, mock_get_field_manager):

        expected_listener_func = "dummy func"
        expected_api_client = MagicMock()
        expected_cr = MagicMock()
        expected_event = MagicMock()

        mock_watcher = MagicMock(name="watcher")
        self.od._cr_watches[expected_cr] = mock_watcher
        num_bodies = 2
        expected_namespaces = [
            f"{self.namespace}{i}" for i in range(num_bodies)
        ]
        watched_bodies = []
        for i in range(num_bodies):
            watched_bodies.append({
                "metadata": {
                    "namespace": expected_namespaces[i]
                }
            })
        mock_watcher.get_all.return_value = [
            ('dummy', cr_body) for cr_body in watched_bodies
        ]

        expected_context = mock_context.return_value

        expected_parent_intf = "dummy resource intf"
        expected_cr.get_resource_interface.return_value = expected_parent_intf
        expected_field_manager = mock_get_field_manager.return_value

        expected_context_calls = [
            call(
                api_client=expected_api_client,
                logger=self.od.logger,
                namespace=expected_namespaces[i],
                parent=watched_bodies[i],
                parent_intf=expected_parent_intf,
                field_manager=expected_field_manager,
                instance=None,
                instance_data=None,
            )
            for i in range(num_bodies)
        ]
        expected_deliver_event_calls = [
            call(expected_listener_func, expected_cr, expected_context,
                 expected_event)
            for _ in range(num_bodies)
        ]

        self.od._broadcast_event(
            expected_listener_func,
            expected_api_client,
            expected_cr,
            expected_event,
        )

        mock_context.assert_has_calls(expected_context_calls, any_order=True)
        self.assertEqual(mock_context.call_count, num_bodies)
        mock_deliver_event \
            .assert_has_calls(expected_deliver_event_calls, any_order=True)
        self.assertEqual(mock_deliver_event.call_count, num_bodies)

    @unpack
    @data(*itertools.product([True, False], repeat=4))
    @unittest.mock.patch("yaook.statemachine.extract_metadata")
    @unittest.mock.patch("yaook.op.daemon.ref_matches_cr_obj")
    @unittest.mock.patch("yaook.op.daemon.get_cr_field_manager")
    @unittest.mock.patch("yaook.statemachine.Context")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._deliver_event")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._broadcast_event")
    @unittest.mock.patch("yaook.statemachine.recover_parent_reference")
    def test__dispatch_event(
            self,
            recoverable_parent_ref,
            body_is_gettable_by_name,
            ref_matches_cr,
            component_matches,
            mock_recover_parent_ref,
            mock_broadcast,
            mock_deliver,
            mock_context,
            mock_get_field_manager,
            mock_ref_matches_cr,
            mock_extract_metadata,
    ):
        expected_api_client = MagicMock()
        ev_key = MagicMock()
        expected_event = MagicMock()

        component = "my component"
        expected_namespace = self.namespace
        expected_name = "test name"

        recovered_instance = "instance"
        expected_instance = recovered_instance

        # setup recovered_parent_ref
        recovered_parent_ref = MagicMock()
        recovered_parent_ref.namespace = expected_namespace
        recovered_parent_ref.name = expected_name

        def recover_parent_ref_side_effect(obj_metadata):
            if recoverable_parent_ref:
                return recovered_parent_ref, recovered_instance
            raise ValueError

        mock_recover_parent_ref.side_effect = recover_parent_ref_side_effect

        # setup listeners
        bc_cr = MagicMock()
        non_bc_cr = MagicMock()
        broadcast_listener = MagicMock()
        broadcast_listener.listener = "bc listener"
        broadcast_listener.broadcast = True
        broadcast_listener.component = component
        broadcast_listener.broadcast_filter = None
        broadcast_filter_listener = MagicMock()
        broadcast_filter_listener.listener = "bc filter listener"
        broadcast_filter_listener.broadcast = True
        broadcast_filter_listener.component = component
        broadcast_filter_listener.broadcast_filter = {"test": "test"}
        non_bc_listener = MagicMock()
        non_bc_listener.listener = "non-bc listener"
        non_bc_listener.broadcast = False
        if component_matches:
            non_bc_listener.component = component
        else:
            non_bc_listener.component = "non-matching component"
        self.od._listeners = {
            ev_key: [
                (bc_cr, broadcast_listener),
                (non_bc_cr, non_bc_listener),
            ]
        }
        expected_bc_listener = broadcast_listener.listener
        expected_non_bc_listener = non_bc_listener.listener

        # setup context
        expected_context = "returned context"
        mock_context.return_value = expected_context

        # setup extract_metadata
        mock_extract_metadata.return_value = {
            "labels": {"state.yaook.cloud/component": component}
        }

        # setup parent interface
        expected_parent_intf = "parent intf"
        non_bc_cr.get_resource_interface.return_value = expected_parent_intf

        # setup watcher get_by_name
        mock_watch = MagicMock()
        self.od._cr_watches = {bc_cr: mock_watch, non_bc_cr: mock_watch}
        gettable_body = "gotten body"

        def watch_get_by_name_side_effect(namespace, name):
            if body_is_gettable_by_name:
                return gettable_body, None
            raise KeyError

        mock_watch.get_by_name.side_effect = watch_get_by_name_side_effect
        expected_parent = gettable_body

        # setup field_manager
        expected_field_manager = self.field_manager
        mock_get_field_manager.return_value = expected_field_manager

        # other
        expect_to_deliver_event = \
            recoverable_parent_ref and body_is_gettable_by_name and \
            ref_matches_cr and component_matches

        mock_ref_matches_cr.return_value = ref_matches_cr

        # act
        self.od._dispatch_event(expected_api_client, ev_key, expected_event)

        # assert
        mock_extract_metadata.assert_called_once_with(expected_event.object_)
        mock_recover_parent_ref \
            .assert_called_once_with(mock_extract_metadata.return_value)
        # assert broadcast related
        mock_broadcast.assert_called_once_with(
            expected_bc_listener,
            expected_api_client,
            bc_cr,
            expected_event
        )
        bc_cr.get_resource_interface.assert_not_called()
        broadcast_filter_listener.get_resource_interface.assert_not_called()

        # assert non-broadcast related
        if expect_to_deliver_event:
            non_bc_cr.get_resource_interface \
                .assert_called_once_with(expected_api_client)
            mock_context.assert_called_once_with(
                api_client=expected_api_client,
                logger=self.od.logger,
                namespace=expected_namespace,
                parent=expected_parent,
                parent_intf=expected_parent_intf,
                field_manager=expected_field_manager,
                instance=expected_instance,
                instance_data=None,
            )
            mock_deliver.assert_called_once_with(
                expected_non_bc_listener, non_bc_cr,
                expected_context, expected_event
            )
        else:
            non_bc_cr.get_resource_interface.assert_not_called()
            mock_deliver.assert_not_called()
            mock_context.assert_not_called()

    @unittest.mock.patch("yaook.statemachine.extract_metadata")
    @unittest.mock.patch("yaook.statemachine.Context")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._broadcast_event")
    @unittest.mock.patch("yaook.statemachine.recover_parent_reference")
    def test__dispatch_event_broadcast_no_filter_match(
            self,
            mock_recover_parent_ref,
            mock_broadcast,
            mock_context,
            mock_extract_metadata,
    ):
        expected_api_client = MagicMock()
        ev_key = MagicMock()
        expected_event = MagicMock()

        component = "my component"
        expected_namespace = self.namespace
        expected_name = "test name"

        recovered_instance = "instance"

        # setup recovered_parent_ref
        recovered_parent_ref = MagicMock()
        recovered_parent_ref.namespace = expected_namespace
        recovered_parent_ref.name = expected_name

        def recover_parent_ref_side_effect(obj_metadata):
            return recovered_parent_ref, recovered_instance

        mock_recover_parent_ref.side_effect = recover_parent_ref_side_effect

        # setup listeners
        bc_cr = MagicMock()
        broadcast_filter_listener = MagicMock()
        broadcast_filter_listener.listener = "bc filter listener"
        broadcast_filter_listener.broadcast = True
        broadcast_filter_listener.component = component
        broadcast_filter_listener.broadcast_filter = None

        self.od._listeners = {
            ev_key: [
                (bc_cr, broadcast_filter_listener),
            ]
        }

        # setup extract_metadata
        mock_extract_metadata.return_value = {
            "labels": {"state.yaook.cloud/component": component}
        }

        # act
        self.od._dispatch_event(expected_api_client, ev_key, expected_event)

        # assert
        mock_extract_metadata.assert_called_once_with(expected_event.object_)
        mock_recover_parent_ref \
            .assert_called_once_with(mock_extract_metadata.return_value)
        # assert broadcast related
        mock_broadcast.assert_called_once_with(
            broadcast_filter_listener.listener,
            expected_api_client,
            bc_cr,
            expected_event
        )

    @unittest.mock.patch("yaook.statemachine.extract_metadata")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._broadcast_event")
    @unittest.mock.patch("yaook.statemachine.recover_parent_reference")
    def test__dispatch_event_broadcast_filter_match(
            self,
            mock_recover_parent_ref,
            mock_broadcast,
            mock_extract_metadata,
    ):
        expected_api_client = MagicMock()
        ev_key = MagicMock()
        expected_event = MagicMock()

        component = "my component"
        expected_namespace = self.namespace
        expected_name = "test name"

        recovered_instance = "instance"

        # setup recovered_parent_ref
        recovered_parent_ref = MagicMock()
        recovered_parent_ref.namespace = expected_namespace
        recovered_parent_ref.name = expected_name

        def recover_parent_ref_side_effect(obj_metadata):
            return recovered_parent_ref, recovered_instance

        mock_recover_parent_ref.side_effect = recover_parent_ref_side_effect

        # setup listeners
        bc_cr = MagicMock()
        broadcast_filter_listener = MagicMock()
        broadcast_filter_listener.listener = "bc filter listener"
        broadcast_filter_listener.broadcast = True
        broadcast_filter_listener.component = component
        broadcast_filter_listener.broadcast_filter = {"test": "test"}

        self.od._listeners = {
            ev_key: [
                (bc_cr, broadcast_filter_listener),
            ]
        }

        # setup extract_metadata
        mock_extract_metadata.return_value = {
            "labels": {
                "state.yaook.cloud/component": component,
                "test": "test"
                }
        }

        # act
        self.od._dispatch_event(expected_api_client, ev_key, expected_event)

        # assert
        mock_extract_metadata.assert_called_once_with(expected_event.object_)
        mock_recover_parent_ref \
            .assert_called_once_with(mock_extract_metadata.return_value)
        # assert broadcast related
        mock_broadcast.assert_called_once_with(
            broadcast_filter_listener.listener,
            expected_api_client,
            bc_cr,
            expected_event
        )

    @unittest.mock.patch("yaook.statemachine.extract_metadata")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._broadcast_event")
    @unittest.mock.patch("yaook.statemachine.recover_parent_reference")
    def test__dispatch_event_broadcast_filter_match_two_listener(
            self,
            mock_recover_parent_ref,
            mock_broadcast,
            mock_extract_metadata,
    ):
        expected_api_client = MagicMock()
        ev_key = MagicMock()
        expected_event = MagicMock()

        component = "my component"
        expected_namespace = self.namespace
        expected_name = "test name"

        recovered_instance = "instance"

        # setup recovered_parent_ref
        recovered_parent_ref = MagicMock()
        recovered_parent_ref.namespace = expected_namespace
        recovered_parent_ref.name = expected_name

        def recover_parent_ref_side_effect(obj_metadata):
            return recovered_parent_ref, recovered_instance

        mock_recover_parent_ref.side_effect = recover_parent_ref_side_effect

        # setup listeners
        bc_cr = MagicMock()
        bc_cr_filter = MagicMock()
        broadcast_filter_listener = MagicMock()
        broadcast_filter_listener.listener = "bc filter listener"
        broadcast_filter_listener.broadcast = True
        broadcast_filter_listener.component = component
        broadcast_filter_listener.broadcast_filter = {"test": "test"}
        broadcast_listener = MagicMock()
        broadcast_listener.listener = "bc listener"
        broadcast_listener.broadcast = True
        broadcast_listener.component = component
        broadcast_listener.broadcast_filter = None

        self.od._listeners = {
            ev_key: [
                (bc_cr_filter, broadcast_filter_listener),
                (bc_cr, broadcast_listener),
            ]
        }

        # setup extract_metadata
        mock_extract_metadata.return_value = {
            "labels": {
                "state.yaook.cloud/component": component,
                "test": "test"
                }
        }

        # act
        self.od._dispatch_event(expected_api_client, ev_key, expected_event)

        # assert
        mock_extract_metadata.assert_called_once_with(expected_event.object_)
        mock_recover_parent_ref \
            .assert_called_once_with(mock_extract_metadata.return_value)
        # assert broadcast related
        mock_broadcast.assert_has_calls([
            call(broadcast_filter_listener.listener, expected_api_client,
                 bc_cr_filter, expected_event),
            call(broadcast_listener.listener, expected_api_client, bc_cr,
                 expected_event)
        ])

    @unpack
    @data(*itertools.product([True, False]))
    @unittest.mock.patch("yaook.op.daemon.get_cr_field_manager")
    @unittest.mock.patch("yaook.statemachine.Context")
    @unittest.mock.patch("yaook.op.daemon.OperatorDaemon._deliver_event")
    def test__dispatch_external_event(
            self,
            body_is_gettable_by_name,
            mock_deliver,
            mock_context,
            mock_get_field_manager,
    ):
        expected_api_client = MagicMock()
        watcher = MagicMock()
        expected_event = MagicMock()

        expected_namespace = self.namespace
        expected_name = "test name"

        # setup listeners
        non_bc_cr = MagicMock()
        non_bc_listener = MagicMock()
        self.od._external_listeners = {
            watcher: [
                (non_bc_cr, non_bc_listener),
            ],
        }
        expected_non_bc_listener = non_bc_listener

        # setup context
        expected_context = "returned context"
        mock_context.return_value = expected_context

        # setup parent interface
        expected_parent_intf = "parent intf"
        non_bc_cr.get_resource_interface.return_value = expected_parent_intf

        # setup watcher get_by_name
        mock_watch = MagicMock()
        self.od._cr_watches = {non_bc_cr: mock_watch}
        gettable_body = {
            "metadata": {
                "name": expected_name,
                "namespace": expected_namespace,
            }
        }

        def watch_get_by_name_side_effect(namespace, name):
            if body_is_gettable_by_name:
                return gettable_body, None
            raise KeyError

        mock_watch.get_by_name.side_effect = watch_get_by_name_side_effect
        expected_parent = gettable_body

        # setup field_manager
        expected_field_manager = self.field_manager
        mock_get_field_manager.return_value = expected_field_manager

        # other
        expect_to_deliver_event = body_is_gettable_by_name

        # act
        self.od._dispatch_external_event(
            expected_api_client, watcher, expected_event)

        # assert
        if expect_to_deliver_event:
            non_bc_cr.get_resource_interface \
                .assert_called_once_with(expected_api_client)
            mock_context.assert_called_once_with(
                api_client=expected_api_client,
                logger=self.od.logger,
                namespace=expected_namespace,
                parent=expected_parent,
                parent_intf=expected_parent_intf,
                field_manager=expected_field_manager,
                instance=None,
                instance_data=None,
            )
            mock_deliver.assert_called_once_with(
                expected_non_bc_listener, non_bc_cr,
                expected_context, expected_event
            )
        else:
            non_bc_cr.get_resource_interface.assert_not_called()
            mock_deliver.assert_not_called()
            mock_context.assert_not_called()

    async def test_run(self):

        listen_keys = ["k1", "k2", "k3", "k4", "42"]
        stream_builders = {
            listen_key: MagicMock()
            for listen_key in listen_keys
        }

        expected_listener = 'dummy listener 1'
        self.od._listeners = {
            listen_key: [expected_listener] for listen_key in listen_keys
        }

        with unittest.mock.patch.dict(
                yaook.op.daemon.AVAILABLE_WATCHERS, stream_builders):

            async def wait_side_effect():
                return False

            with contextlib.ExitStack() as stack:
                mock_get_event_loop = stack.enter_context(unittest.mock.patch(
                    "asyncio.get_event_loop",
                ))
                mock_stop_signal = stack.enter_context(unittest.mock.patch(
                    "asyncio.Event"
                ))
                mock_stop_signal.return_value.wait.side_effect = \
                    wait_side_effect
                mock_restart_task = stack.enter_context(unittest.mock.patch(
                    "yaook.op.tasks.RestartingTask"
                ))

                mock_restart_task.return_value.wait_for_termination = \
                    AsyncMock()

                # setup resource class
                mock_resource_class = stack.enter_context(unittest.mock.patch(
                    "tests.op.test_daemon.TestResource"
                ))
                mock_resource = MagicMock()
                mock_resource_class.return_value = mock_resource

                # setup listeners
                mock_listeners = [
                    MagicMock(context.KubernetesListener)
                    for _ in listen_keys]
                mock_resource.get_listeners.return_value = mock_listeners
                for i in range(len(listen_keys)):
                    mock_listeners[i].object_key = listen_keys[i]

                # setup registry
                mock_registry = stack.enter_context(unittest.mock.patch(
                    "yaook.statemachine.registry"
                ))
                registry_dict = {
                    "k1": ("plural", "group", "version", mock_resource_class),
                }
                mock_registry.registry = registry_dict
                event_key = "group", "version", "plural"

                # setup expected restarting_task calls
                expected_rt_calls = [
                    call(self.od._schedule_periodic_reconcile,
                         logger=self.od.logger),
                ]
                registered_crs = [mock_resource] * len(registry_dict)
                for cr in registered_crs:
                    new_call = call(functools.partial(self.od._watch_cr, cr), )
                    expected_rt_calls.append(new_call)
                for listen_key in listen_keys:
                    expected_rt_calls.append(call(
                        functools.partial(self.od._watch_builtin, listen_key,
                                          stream_builders[listen_key])
                    ))
                expected_num_tasks = len(expected_rt_calls)

                # setup expected signal handler calls
                expected_add_signal_handler_calls = [
                    call(signal.SIGTERM, mock_stop_signal.return_value.set),
                    call(signal.SIGINT, mock_stop_signal.return_value.set),
                ]

                # act
                await self.od.run()

                # asserts
                self.assertEqual(self.od._crs[event_key], mock_resource)
                self.assertIn(mock_resource, self.od._cr_watches)
                for listen_key in listen_keys:
                    observed_listeners = self.od._listeners[listen_key]
                    self.assertIn(expected_listener, observed_listeners)
                    self.assertEqual(len(observed_listeners), 2)

                mock_get_event_loop.return_value.add_signal_handler \
                    .assert_has_calls(expected_add_signal_handler_calls)

                # Cannot compare with assert_has_calls like this,
                # since partial != partial:
                # mock_restarting_task.assert_has_calls(expected_rt_calls,
                # any_order=True)
                # Instead we only count the number of calls.
                self.assertEqual(mock_restart_task.call_count,
                                 expected_num_tasks)

                mock_stop_signal.return_value.wait.assert_called_once_with()

                self.assertEqual(
                    mock_restart_task.return_value.start.call_count,
                    expected_num_tasks
                )
                self.assertEqual(
                    mock_restart_task.return_value.stop.call_count,
                    expected_num_tasks
                )
                self.assertEqual(
                    mock_restart_task.return_value.wait_for_termination.
                    call_count,
                    expected_num_tasks
                )
