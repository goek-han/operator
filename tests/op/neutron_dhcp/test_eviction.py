#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid

import openstack
import openstack.exceptions

# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.op.neutron_dhcp.eviction as eviction
import yaook.statemachine.customresource as customresource


NAMESPACE = f"namespace-{uuid.uuid4()}"
NAME = f"name-{uuid.uuid4()}"


def repeatable_generator(iterable):
    def generator(*args, **kwargs):
        yield from iterable
    return generator


class TestDHCPAgentInfo(unittest.TestCase):
    def test_get_raises_lookup_error_if_not_found(self):
        client = unittest.mock.Mock(["agents"])
        client.agents.return_value = []

        with self.assertRaisesRegex(
                LookupError,
                "correct on correct not found"):
            resource.get_network_agent(client,
                                       host="correct",
                                       binary="correct",
                                       )

    def test_disable_invokes_api(self):
        client = unittest.mock.Mock(["put"])
        agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        agent.id = unittest.mock.sentinel.agent_id

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any", # noqa E501
            ))

            resource.disable_network_agent(client, agent)

        client.put.assert_called_once_with(
            f"/agents/{agent.id}",
            json={
                "agent": {
                    "admin_state_up": False,
                },
            },
        )

        raise_.assert_called_once_with(client.put())


class Testmigrate_network(unittest.TestCase):
    def test_migrate_network(self):
        conn = unittest.mock.Mock(["network"])
        network_id = unittest.mock.sentinel.id_
        new_agent = unittest.mock.sentinel.id_
        old_agent = unittest.mock.sentinel.id_

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any", # noqa E501
            ))

            eviction.migrate_network(conn, network_id, new_agent, old_agent)

        conn.network.post.assert_called_once_with(
            f"/agents/{new_agent}/dhcp-networks",
            json={
                "network_id": network_id,
            },
        )
        conn.network.delete.assert_called_once_with(
            f"/agents/{old_agent}/dhcp-networks/{network_id}",
        )
        calls = [
            unittest.mock.call(conn.network.post()),
            unittest.mock.call(conn.network.delete()),
        ]
        raise_.assert_has_calls(calls)


class TestEvictor(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock(
            "yaook.statemachine.interfaces.ResourceInterfaceWithStatus",
        )
        self.connection_info = {
            "auth": unittest.mock.sentinel.auth,
            "interface": unittest.mock.sentinel.interface,
        }
        self.e = eviction.Evictor(
            interface=self.intf,
            namespace=NAMESPACE,
            node_name=NAME,
            connection_info=self.connection_info,
            reason="some-reason",
            max_parallel_migrations=5,
        )

        self.os_connection = unittest.mock.Mock()
        self.agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        self.agent.id = unittest.mock.sentinel.agent_id
        self.agent.host = NAME
        self.agent.binary = "neutron-dhcp-agent"
        self.agent.is_alive = True
        self.agent.is_admin_state_up = True

        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = self.os_connection

        self.get_network_agent_mock = unittest.mock.Mock([])
        self.get_network_agent_mock.return_value = self.agent
        self.disable_network_agent_mock = unittest.mock.Mock([])

        self.compute_next_agent_mock = unittest.mock.Mock([])
        self.compute_next_agent_mock.return_value = [[], []]

        self.start_actions_mock = unittest.mock.Mock([])
        self.start_actions_mock.return_value = (0, 0)

        self.collect_network_status_mock = unittest.mock.Mock([])
        self.collect_network_status_mock.return_value = {
            k: []
            for k in eviction.NetworkStatusClass
        }

        self.iterate_mock = unittest.mock.Mock([])

        self.__os_patches = [
            unittest.mock.patch.object(self.e, "_collect_network_status",
                                       new=self.collect_network_status_mock),
            unittest.mock.patch.object(self.e, "_start_actions",
                                       new=self.start_actions_mock),
            unittest.mock.patch.object(self.e, "_compute_next_agent",
                                       new=self.compute_next_agent_mock),
            unittest.mock.patch.object(self.e, "_dump_status"),
        ]
        self.__iterate_patches = [
            unittest.mock.patch.object(self.e, "_connect",
                                       new=self.connect_mock),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.get_network_agent",
                new=self.get_network_agent_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.disable_network_agent",
                new=self.disable_network_agent_mock,
            ),
            unittest.mock.patch.object(self.e, "_iterate",
                                       new=self.iterate_mock),
        ]
        self.__patches = []

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def _patch_os(self):
        for p in self.__os_patches:
            p.start()
            self.__patches.append(p)

    def _patch_iterate(self):
        for p in self.__iterate_patches:
            p.start()
            self.__patches.append(p)

    def _mock_network(self, *, id_=None, status="ACTIVE"):
        network = unittest.mock.Mock([])
        network.id = str(uuid.uuid4() if id_ is None else id_)
        network.status = status
        network.subnet_ids = [network.id + "-subnet"]
        return network

    def test__connect(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(unittest.mock.patch(
                "openstack.connect",
            ))

            result = self.e._connect()

        connect.assert_called_once_with(
            auth=unittest.mock.sentinel.auth,
            interface=unittest.mock.sentinel.interface,
        )
        self.assertEqual(result, connect())

    def test__collect_network_status_bins_agents_up(self):
        self.os_connection.network.dhcp_agent_hosting_networks.side_effect = \
            repeatable_generator([
                self._mock_network(id_=i, status=status)
                for i, status in enumerate(sorted(eviction.STATUS_MAP.keys()))
            ])

        result = self.e._collect_network_status(
            self.os_connection,
            self.agent,
            eviction.STATUS_MAP,
        )

        self.os_connection.network.dhcp_agent_hosting_networks.\
            assert_called_once_with(
                agent=self.agent.id,
            )

        self.maxDiff = None
        self.assertDictEqual(
            {k: {v.id for v in vs} for k, vs in result.items()},
            {
                eviction.NetworkStatusClass.MIGRATABLE: {"0", "2", "3"},
                eviction.NetworkStatusClass.PENDING: {"1"},
                eviction.NetworkStatusClass.UNHANDLEABLE: set(),
            }
        )

    def test__collect_network_status_unhandleable_networks(self):
        self.os_connection.network.dhcp_agent_hosting_networks.side_effect = \
            repeatable_generator([
                self._mock_network(id_=1, status='foobar')
            ])
        result = self.e._collect_network_status(
            self.os_connection,
            self.agent,
            eviction.STATUS_MAP,
        )
        self.assertDictEqual(
            {k: {v.id for v in vs} for k, vs in result.items()},
            {
                eviction.NetworkStatusClass.MIGRATABLE: set(),
                eviction.NetworkStatusClass.PENDING: set(),
                eviction.NetworkStatusClass.UNHANDLEABLE: {"1"},
            }
        )

    def test__compute_next_agent_generates_migration(self):
        mock_agents = [self.agent]
        for i in ["up", "disabled", "dead"]:
            agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
            agent.id = "id-" + i
            agent.host = "host-" + i
            agent.binary = "neutron-dhcp-agent"
            if i == "dead":
                agent.is_alive = False
            else:
                agent.is_alive = True
            if i == "disabled":
                agent.is_admin_state_up = False
            else:
                agent.is_admin_state_up = True
            mock_agents.append(agent)
        self.os_connection.network.agents.side_effect = \
            repeatable_generator(mock_agents)
        self.os_connection.network.network_hosting_dhcp_agents.side_effect = \
            repeatable_generator([self.agent])
        status = {k: [] for k in eviction.NetworkStatusClass}
        status[eviction.NetworkStatusClass.MIGRATABLE] = [
            self._mock_network(id_="id1"),
        ]

        result, unhandable = self.e._compute_next_agent(
            self.os_connection,
            self.agent,
            status,
        )

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    "id-up",
                    self.agent.id,
                    eviction.migrate_network,
                ),
            ],
        )
        self.assertCountEqual(
            unhandable,
            [],
        )

    def test__compute_next_agent_no_dhcp(self):
        mock_agents = [self.agent]
        agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        agent.id = unittest.mock.sentinel.second_agent
        agent.host = "host-2"
        agent.binary = "neutron-dhcp-agent"
        agent.is_alive = True
        agent.is_admin_state_up = True
        mock_agents.append(agent)
        self.os_connection.network.agents.side_effect = \
            repeatable_generator(mock_agents)

        subnet = unittest.mock.Mock(openstack.network.v2.subnet.Subnet)
        subnet.is_dhcp_enabled = False
        self.os_connection.network.get_subnet.return_value = subnet
        self.os_connection.network.network_hosting_dhcp_agents.side_effect = \
            repeatable_generator([self.agent])
        status = {k: [] for k in eviction.NetworkStatusClass}
        status[eviction.NetworkStatusClass.MIGRATABLE] = [
            self._mock_network(id_="id1"),
        ]

        result, unhandable = self.e._compute_next_agent(
            self.os_connection,
            self.agent,
            status,
        )
        self.assertCountEqual(
            result,
            [],
        )
        self.assertCountEqual(
            unhandable,
            [],
        )

    def test__compute_next_agent_no_possible_agent(self):
        mock_agents = [self.agent]
        self.os_connection.network.agents.side_effect = \
            repeatable_generator(mock_agents)

        subnet = unittest.mock.Mock(openstack.network.v2.subnet.Subnet)
        subnet.is_dhcp_enabled = True
        self.os_connection.network.get_subnet.result = subnet
        self.os_connection.network.network_hosting_dhcp_agents.side_effect = \
            repeatable_generator([self.agent])
        status = {k: [] for k in eviction.NetworkStatusClass}
        network_mock = self._mock_network(id_="id1")
        status[eviction.NetworkStatusClass.MIGRATABLE] = [
            network_mock,
        ]

        result, unhandable = self.e._compute_next_agent(
            self.os_connection,
            self.agent,
            status,
        )
        self.assertCountEqual(
            result,
            [],
        )
        self.assertCountEqual(
            unhandable,
            [
                network_mock,
            ],
        )

    def test__start_actions_starts_actions_up_to_max_parallel_migrations(self): # NOQA
        actions = [
            eviction.ActionSpec("id1", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id2", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id3", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id4", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id5", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id6", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
        )

        self.assertEqual(result, (5, 0))
        actions[0].action.assert_called_once_with(
            self.os_connection,
            "id1",
            self.agent.id,
            "new-agent",
        )
        actions[1].action.assert_called_once_with(
            self.os_connection,
            "id2",
            self.agent.id,
            "new-agent",
        )
        actions[2].action.assert_called_once_with(
            self.os_connection,
            "id3",
            self.agent.id,
            "new-agent",
        )
        actions[3].action.assert_called_once_with(
            self.os_connection,
            "id4",
            self.agent.id,
            "new-agent",
        )
        actions[4].action.assert_called_once_with(
            self.os_connection,
            "id5",
            self.agent.id,
            "new-agent",
        )
        actions[5].action.assert_not_called()

    def test__start_actions_skips_and_tries_next_on_failure(self):
        actions = [
            eviction.ActionSpec("id1", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id2", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id3", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
            eviction.ActionSpec("id4", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
        ]
        actions[2].action.side_effect = openstack.exceptions.HttpException

        result = self.e._start_actions(
            self.os_connection,
            actions,
        )

        self.assertEqual(result, (3, 1))
        actions[0].action.assert_called_once_with(
            self.os_connection,
            "id1",
            self.agent.id,
            "new-agent",
        )
        actions[1].action.assert_called_once_with(
            self.os_connection,
            "id2",
            self.agent.id,
            "new-agent",
        )
        actions[2].action.assert_called_once_with(
            self.os_connection,
            "id3",
            self.agent.id,
            "new-agent",
        )
        actions[3].action.assert_called_once_with(
            self.os_connection,
            "id4",
            self.agent.id,
            "new-agent",
        )

    def test__start_actions_handles_one_action(self):
        actions = [
            eviction.ActionSpec("id1", self.agent.id, "new-agent",
                                unittest.mock.Mock(["__name__"])),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
        )

        self.assertEqual(result, (1, 0))
        actions[0].action.assert_called_once_with(
            self.os_connection,
            "id1",
            self.agent.id,
            "new-agent",
        )

    def test__os_iterate_disables_network_agent(self):
        self._patch_iterate()

        self.e._os_iterate()

        self.connect_mock.assert_called_once_with()
        self.get_network_agent_mock.assert_called_once_with(
            self.os_connection.network,
            host=NAME,
            binary="neutron-dhcp-agent",
        )
        self.disable_network_agent_mock.assert_called_once_with(
            self.os_connection.network,
            self.agent,
        )

    def test__os_iterate_uses_up_status_map_if_up_and_present(self):
        self._patch_iterate()

        result = self.e._os_iterate()

        self.iterate_mock.assert_called_once_with(
            self.os_connection,
            self.agent,
            eviction.STATUS_MAP,
        )

        self.assertEqual(result, self.iterate_mock())

    def test__iterate_collects_and_starts_actions(self):
        self._patch_os()

        status = self.collect_network_status_mock.return_value
        status[unittest.mock.sentinel.foo] = unittest.mock.sentinel.bar
        status = dict(status)

        self.compute_next_agent_mock.return_value = [
            [],
            [],
        ]

        self.start_actions_mock.return_value = (0, 0)

        self.e._iterate(
                self.os_connection,
                self.agent,
                unittest.mock.sentinel.status_map,
            )

        self.collect_network_status_mock.assert_called_once_with(
            self.os_connection,
            self.agent,
            unittest.mock.sentinel.status_map,
        )
        self.compute_next_agent_mock.assert_called_once_with(
            self.os_connection,
            self.agent,
            status,
        )
        self.start_actions_mock.assert_called_once_with(
            self.os_connection,
            unittest.mock.ANY,
        )

    def test__iterate_includes_unhandleable_networks_from_status(self):
        self._patch_os()

        status = self.collect_network_status_mock.return_value
        status[eviction.NetworkStatusClass.MIGRATABLE] = [1, 2, 3]
        network = self._mock_network(id_=4)
        status[eviction.NetworkStatusClass.UNHANDLEABLE] = [network]
        status = dict(status)

        self.start_actions_mock.return_value = (3, 0)

        result = self.e._iterate(
            self.os_connection,
            self.agent,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migrated_networks,
            3,
        )
        self.assertEqual(
            result.unhandleable_networks,
            1,
        )

    def test__iterate_includes_unhandleable_networks_from_list(self):
        self._patch_os()

        network = self._mock_network(id_=4)
        self.compute_next_agent_mock.return_value = [[], [network]]

        result = self.e._iterate(
            self.os_connection,
            self.agent,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.unhandleable_networks,
            1,
        )

    def test__iterate_includes_failing_networks_in_final_count(self):
        self._patch_os()

        status = self.collect_network_status_mock.return_value
        status[eviction.NetworkStatusClass.MIGRATABLE] = [1, 2, 3]
        status[eviction.NetworkStatusClass.UNHANDLEABLE] = []
        status = dict(status)

        self.start_actions_mock.return_value = (2, 1)

        result = self.e._iterate(
            self.os_connection,
            self.agent,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migrated_networks,
            2,
        )
        self.assertEqual(
            result.unhandleable_networks,
            1,
        )

    def test__iterate_gathers_statistics(self):
        self._patch_os()

        status = self.collect_network_status_mock.return_value
        status[eviction.NetworkStatusClass.MIGRATABLE] = [1, 2, 3]
        status[eviction.NetworkStatusClass.PENDING] = [4, 5]
        status[eviction.NetworkStatusClass.UNHANDLEABLE] = [
            self._mock_network(id_="7"),
            self._mock_network(id_="8"),
            self._mock_network(id_="9"),
            self._mock_network(id_="10"),
        ]
        status = dict(status)
        self.start_actions_mock.return_value = (3, 0)

        result = self.e._iterate(
            self.os_connection,
            self.agent,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migrated_networks,
            3,
        )
        self.assertEqual(
            result.pending_networks,
            2,
        )
        self.assertEqual(
            result.unhandleable_networks,
            4,
        )
        self.assertEqual(
            result.unhandleable_agent,
            False,
        )

    async def test_iterate_runs_os_iterate_in_executor(self):
        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.AsyncMock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result

        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop

            _os_iterate = stack.enter_context(unittest.mock.patch.object(
                self.e, "_os_iterate",
            ))

            result = await self.e.iterate()

        self.assertEqual(result, unittest.mock.sentinel.result)

        loop.run_in_executor.assert_awaited_once_with(
            None,
            _os_iterate,
        )

        get_event_loop.assert_called_once_with()

    async def test_post_status_updates_custom_resource_status(self):
        status = unittest.mock.Mock()
        status.as_json_object.return_value = {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            update_status = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))

            await self.e.post_status(status)

        update_status.assert_awaited_once_with(
            self.intf,
            NAMESPACE,
            NAME,
            conditions=unittest.mock.ANY,
        )

        _, _, kwargs = update_status.mock_calls[0]
        conditions = kwargs["conditions"]
        self.assertCountEqual(
            conditions,
            [
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.EVICTED.value,
                    status=resource.ResourceEvictingStatus.EVICTING.
                    value,
                    reason="some-reason",
                    message='{"foo": "bar"}',
                ),
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    status="False",
                    reason="some-reason",
                    message="Eviction in progress",
                ),
            ],
        )

    async def test_run_iterates_till_migration_done(self):
        status_list = []
        status_list.append(
            eviction.EvictionStatus(
                migrated_networks=5,
                migratable_networks=4,
                pending_networks=0,
                unhandleable_networks=0,
                unhandleable_agent=False,
            )
        )
        status_list.append(
            eviction.EvictionStatus(
                migrated_networks=4,
                migratable_networks=0,
                pending_networks=0,
                unhandleable_networks=0,
                unhandleable_agent=False,
            )
        )

        with contextlib.ExitStack() as stack:
            common_iterate = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.commoneviction.Evictor.iterate",
            ))
            post_status = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.commoneviction.Evictor.post_status",
            ))
            common_iterate.side_effect = status_list
            await self.e.run(poll_interval=1)

        self.assertEqual(2, post_status.await_count)
        self.assertEqual(2, common_iterate.await_count)
        calls = [
            unittest.mock.call(),
            unittest.mock.call(),
        ]
        common_iterate.assert_has_calls(calls)
