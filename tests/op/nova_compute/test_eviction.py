#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
from unittest.mock import AsyncMock, MagicMock
import uuid

import openstack.exceptions

import yaook.op.nova_compute.eviction as eviction
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.customresource as customresource
from unittest.mock import patch


NAMESPACE = f"namespace-{uuid.uuid4()}"
NAME = f"name-{uuid.uuid4()}"


def repeatable_generator(iterable):
    def generator(*args, **kwargs):
        yield from iterable
    return generator


class TestComputeServiceInfo(unittest.TestCase):
    def test_get_with_real_api_response(self):
        api_response = [
            {
                'binary': 'nova-conductor',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'nova-conductor-66579c5845-hz72p',
                'id': '754d1b95-820c-44f5-b584-096bcef38b2f',
                'state': 'down',
                'status': 'enabled',
                'updated_at': '2020-12-17T09:05:19.000000',
                'zone': 'internal'
            },
            {
                'binary': 'nova-scheduler',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'nova-scheduler-7c97676cd9-kjxb6',
                'id': '2ef89e09-9d4a-471d-90b2-cf06306f9cfe',
                'state': 'down',
                'status': 'enabled',
                'updated_at': '2020-12-17T09:05:33.000000',
                'zone': 'internal'
            },
            {
                'binary': 'nova-compute',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'managed-k8s-worker-0',
                'id': '810421c2-7c0e-47d6-83c7-667f416b823e',
                'state': 'up',
                'status': 'enabled',
                'updated_at': '2020-12-18T08:37:29.000000',
                'zone': 'nova'
            },
            {
                'binary': 'nova-compute',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'managed-k8s-worker-1',
                'id': 'f26da26e-6ca7-43c0-8810-efea8cd0bace',
                'state': 'up',
                'status': 'enabled',
                'updated_at': '2020-12-18T08:37:29.000000',
                'zone': 'nova'
            },
            {
                'binary': 'nova-compute',
                'disabled_reason': 'nova-compute-operator: DesiredState',
                'forced_down': False,
                'host': 'managed-k8s-worker-2',
                'id': '0653a4a1-b4f9-4a22-ad2d-c0610557e3d7',
                'state': 'down',
                'status': 'disabled',
                'updated_at': '2020-12-17T14:50:09.000000',
                'zone': 'nova'
            },
            {
                'binary': 'nova-conductor',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'nova-conductor-6c4c94497b-4wgb9',
                'id': '9834d718-edfb-4a4a-8901-f37eeb7400a1',
                'state': 'up',
                'status': 'enabled',
                'updated_at': '2020-12-18T08:37:33.000000',
                'zone': 'internal'
            },
            {
                'binary': 'nova-scheduler',
                'disabled_reason': None,
                'forced_down': False,
                'host': 'nova-scheduler-69cd8fc976-9zkxf',
                'id': '77b473e8-3410-4f15-9b36-a5af05f9a683',
                'state': 'up',
                'status': 'enabled',
                'updated_at': '2020-12-18T08:37:29.000000',
                'zone': 'internal'
            }
        ]

        client = unittest.mock.Mock(["get"])
        client.get.return_value.json.return_value = {
            "services": api_response,
        }

        result = eviction.ComputeServiceInfo.get(
            client,
            host="managed-k8s-worker-2",
            binary="nova-compute",
        )

        client.get.assert_called_once_with(
            "/os-services",
        )

        self.assertIsInstance(result, eviction.ComputeServiceInfo)
        self.assertEqual(result.id_, "0653a4a1-b4f9-4a22-ad2d-c0610557e3d7")
        self.assertEqual(result.host, "managed-k8s-worker-2")
        self.assertEqual(result.binary, "nova-compute")
        self.assertFalse(result.is_up)
        self.assertFalse(result.is_enabled)
        self.assertEqual(result.disable_reason,
                         "nova-compute-operator: DesiredState")
        self.assertFalse(result.is_forced_down)

        result = eviction.ComputeServiceInfo.get(
            client,
            host="managed-k8s-worker-1",
            binary="nova-compute",
        )

        self.assertIsInstance(result, eviction.ComputeServiceInfo)
        self.assertEqual(result.id_, "f26da26e-6ca7-43c0-8810-efea8cd0bace")
        self.assertEqual(result.host, "managed-k8s-worker-1")
        self.assertEqual(result.binary, "nova-compute")
        self.assertTrue(result.is_up)
        self.assertTrue(result.is_enabled)
        self.assertIsNone(result.disable_reason)
        self.assertFalse(result.is_forced_down)

    def test_get_raises_lookup_error_if_not_found(self):
        api_response = [
            {
                "host": "foo",
                "binary": "bar",
            },
            {
                "host": "correct",
                "binary": "bar",
            },
            {
                "host": "foo",
                "binary": "correct",
            },
        ]

        client = unittest.mock.Mock(["get"])
        client.get.return_value.json.return_value = {
            "services": api_response,
        }

        with self.assertRaisesRegex(
                LookupError,
                "correct on correct not found"):
            eviction.ComputeServiceInfo.get(client,
                                            host="correct",
                                            binary="correct")

    def test_disable_invokes_api(self):
        id_ = str(uuid.uuid4())

        csi = eviction.ComputeServiceInfo(
            id_=id_,
            host=unittest.mock.sentinel.host,
            binary=unittest.mock.sentinel.binary,
            is_up=unittest.mock.sentinel.is_up,
            is_enabled=unittest.mock.sentinel.is_enabled,
            disable_reason=unittest.mock.sentinel.disable_reason,
            is_forced_down=unittest.mock.sentinel.is_forced_down,
        )

        client = unittest.mock.Mock(["put"])

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))

            csi.disable(client, "some-reason")

        client.put.assert_called_once_with(
            f"/os-services/{id_}",
            json={
                "status": "disabled",
                "disabled_reason": "some-reason",
            },
        )

        raise_.assert_called_once_with(client.put())


class Testdo_live_migrate_server(unittest.TestCase):
    class FakePort:
        def __init__(self, is_port_security_enabled):
            self.is_port_security_enabled = is_port_security_enabled

    def test_scheduled_live_migration(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = unittest.mock.sentinel.id_

        eviction.do_live_migrate_server(conn, server_id, None, None)

        conn.compute.live_migrate_server.assert_called_once_with(
            server_id,
            host=None,
            force=False,
        )

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_poll_migration_for_queens(self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(True),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "queens", None)
        mock_poll_migration.assert_awaited_once()

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_poll_migration_for_rocky(self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(True),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "rocky", None)
        mock_poll_migration.assert_awaited_once()

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_poll_migration_for_stein(self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(True),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "stein", None)
        mock_poll_migration.assert_awaited_once()

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_poll_migration_for_train(self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(True),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "train", None)
        mock_poll_migration.assert_awaited_once()

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_normal_migration_for_queens_without_portsec(
            self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(False),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "queens", None)
        mock_poll_migration.assert_not_awaited()
        conn.compute.live_migrate_server.assert_called_once_with(
            server_id,
            host=None,
            force=False,
        )

    @patch("yaook.op.nova_compute.eviction.poll_migration",
           new_callable=AsyncMock)
    def test_do_normal_migration_for_ussuri(self, mock_poll_migration):
        conn = unittest.mock.Mock(["compute", "network"])
        conn.network.ports.return_value = [
            Testdo_live_migrate_server.FakePort(True),
            Testdo_live_migrate_server.FakePort(False),
        ]
        server_id = unittest.mock.sentinel.id_
        mock_poll_migration.return_value = None
        eviction.do_live_migrate_server(conn, server_id, "ussuri", None)
        mock_poll_migration.assert_not_awaited()
        conn.compute.live_migrate_server.assert_called_once_with(
            server_id,
            host=None,
            force=False,
        )


class Test_check_root_device_is_local(unittest.TestCase):
    def test_root_device_is_local_volume_attached(self):
        openstack_client = unittest.mock.Mock(["volume"])
        mock_server = MagicMock()
        mock_server.attached_volumes = []
        result = eviction.check_root_device_is_local(openstack_client,
                                                     mock_server)
        self.assertTrue(result)

    def test_root_device_is_local_volume_not_attached_root(self):
        openstack_client = unittest.mock.Mock(["volume"])
        mock_server = MagicMock()
        mock_volume = MagicMock()
        mock_volume.id = unittest.mock.Mock()
        mock_server.root_device_name = unittest.mock.sentinel.root_device_name_
        mock_server.attached_volumes = [mock_volume]
        mock_volume.attachments = [
            {"device": unittest.mock.sentinel.root_device_name_}]

        openstack_client.volume.get_volume.return_value = mock_volume
        result = eviction.check_root_device_is_local(openstack_client,
                                                     mock_server)
        self.assertFalse(result)

    def test_root_device_is_local_volume_not_attached_not_root(self):
        openstack_client = unittest.mock.Mock(["volume"])
        mock_server = MagicMock()
        mock_volume = MagicMock()
        mock_volume.id = unittest.mock.Mock()
        mock_server.root_device_name = unittest.mock.sentinel.root_device_name_
        mock_server.attached_volumes = [mock_volume]
        mock_volume.attachments = [
            {"device": unittest.mock.sentinel.root_device_diff_name_}]

        openstack_client.volume.get_volume.return_value = mock_volume
        result = eviction.check_root_device_is_local(openstack_client,
                                                     mock_server)
        self.assertTrue(result)


class Test_set_migration_speed(unittest.IsolatedAsyncioTestCase):
    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_set_migration_speed(self, mock_kubernetes_run,
                                       mock_k8s_stream_client,
                                       mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods

        await eviction.set_migration_speed(
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_,
            unittest.mock.sentinel.instance_name_,
            unittest.mock.sentinel.migration_speed_)

        mock_pods.list_.assert_called_once_with(
            unittest.mock.sentinel.namespace_,
            label_selector={
                "state.yaook.cloud/parent-name": unittest.mock.sentinel.node_,
                "state.yaook.cloud/parent-plural": "novacomputenodes",
                "state.yaook.cloud/component": "compute"
            },)

        mock_kubernetes_run.assert_called_once_with(
                unittest.mock.ANY,
                namespace=unittest.mock.sentinel.namespace_,
                pod=unittest.mock.sentinel.pod_,
                container="libvirtd",
                command=[
                    "/bin/bash", "-c",
                    "virsh migrate-setspeed --bandwidth "
                    f"{unittest.mock.sentinel.migration_speed_} "
                    f"{unittest.mock.sentinel.instance_name_}"]
                )


class Test_check_virsh_is_emtpy(unittest.IsolatedAsyncioTestCase):
    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_is_empty(self, mock_kubernetes_run, mock_k8s_stream_client,
                            mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods
        k8s_response = MagicMock()
        k8s_response.returncode = 1
        mock_kubernetes_run.return_value = k8s_response

        result = await eviction.check_virsh_is_emtpy(
            MagicMock(),
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_)

        mock_pods.list_.assert_called_once_with(
            unittest.mock.sentinel.namespace_,
            label_selector={
                "state.yaook.cloud/parent-name": unittest.mock.sentinel.node_,
                "state.yaook.cloud/parent-plural": "novacomputenodes",
                "state.yaook.cloud/component": "compute"
            },)

        mock_kubernetes_run.assert_called_once_with(
                unittest.mock.ANY,
                namespace=unittest.mock.sentinel.namespace_,
                pod=unittest.mock.sentinel.pod_,
                container="libvirtd",
                command=[
                    "/bin/bash", "-c",
                    "virsh list --all | grep instance"]
                )
        self.assertTrue(result)

    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_is_not_empty(self, mock_kubernetes_run,
                                mock_k8s_stream_client, mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods
        k8s_response = MagicMock()
        k8s_response.returncode = 0
        mock_kubernetes_run.return_value = k8s_response

        result = await eviction.check_virsh_is_emtpy(
            MagicMock(),
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_)

        mock_pods.list_.assert_called_once_with(
            unittest.mock.sentinel.namespace_,
            label_selector={
                "state.yaook.cloud/parent-name": unittest.mock.sentinel.node_,
                "state.yaook.cloud/parent-plural": "novacomputenodes",
                "state.yaook.cloud/component": "compute"
            },)

        mock_kubernetes_run.assert_called_once_with(
                unittest.mock.ANY,
                namespace=unittest.mock.sentinel.namespace_,
                pod=unittest.mock.sentinel.pod_,
                container="libvirtd",
                command=[
                    "/bin/bash", "-c",
                    "virsh list --all | grep instance"]
                )
        self.assertFalse(result)

    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_pod_is_missing(self, mock_kubernetes_run,
                                  mock_k8s_stream_client, mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = []
        mock_pod_interface.return_value = mock_pods

        result = await eviction.check_virsh_is_emtpy(
            MagicMock(),
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_)

        mock_pods.list_.assert_called_once_with(
            unittest.mock.sentinel.namespace_,
            label_selector={
                "state.yaook.cloud/parent-name": unittest.mock.sentinel.node_,
                "state.yaook.cloud/parent-plural": "novacomputenodes",
                "state.yaook.cloud/component": "compute"
            },)

        self.assertIsNone(result)


class Test_check_ovs_flows_present(unittest.IsolatedAsyncioTestCase):
    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_no_flow_ready(self, mock_kubernetes_run,
                                 mock_k8s_stream_client, mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods
        k8s_response = MagicMock()
        k8s_response.returncode = 1
        mock_kubernetes_run.return_value = k8s_response

        result = await eviction.check_ovs_flows_present(
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_,
            [
                "test_mac_1"
            ]
        )

        mock_pods.list_.assert_called_once_with(
                unittest.mock.sentinel.namespace_,
                label_selector={
                    "state.yaook.cloud/parent-name":
                        unittest.mock.sentinel.node_,
                    "state.yaook.cloud/component": "l2_agent"
                },
            )
        mock_kubernetes_run.assert_called_once_with(
            unittest.mock.ANY,
            namespace=unittest.mock.sentinel.namespace_,
            pod=unittest.mock.sentinel.pod_,
            container="neutron-openvswitch-agent",
            command=[
                "/bin/bash", "-c", 'test $(ovs-ofctl dump-flows br-int | '
                'grep -Fo -e test_mac_1 | sort -u | wc -l) -eq 1'],
            )
        self.assertFalse(result)

    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_flow_ready(self, mock_kubernetes_run,
                              mock_k8s_stream_client, mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods
        k8s_response = MagicMock()
        k8s_response.returncode = 0
        mock_kubernetes_run.return_value = k8s_response

        result = await eviction.check_ovs_flows_present(
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_,
            [
                "test_mac_1"
            ]
        )
        mock_pods.list_.assert_called_once_with(
                unittest.mock.sentinel.namespace_,
                label_selector={
                    "state.yaook.cloud/parent-name":
                        unittest.mock.sentinel.node_,
                    "state.yaook.cloud/component": "l2_agent"
                },
            )
        mock_kubernetes_run.assert_called_once_with(
            unittest.mock.ANY,
            namespace=unittest.mock.sentinel.namespace_,
            pod=unittest.mock.sentinel.pod_,
            container="neutron-openvswitch-agent",
            command=[
                "/bin/bash", "-c", 'test $(ovs-ofctl dump-flows br-int | '
                'grep -Fo -e test_mac_1 | sort -u | wc -l) -eq 1'],
            )
        self.assertTrue(result)

    @patch("yaook.op.nova_compute.eviction.interfaces.pod_interface")
    @patch("yaook.op.nova_compute.eviction.kstream.WsApiClient")
    @patch("yaook.op.nova_compute.eviction.kubernetes_run",
           new_callable=AsyncMock)
    async def test_multiple_macs_flows_not_ready(self, mock_kubernetes_run,
                                                 mock_k8s_stream_client,
                                                 mock_pod_interface):
        mock_k8s_stream_client.__aexit__.return_value = AsyncMock()
        k8s_client = unittest.mock.Mock(["configuration"])
        mock_pods = AsyncMock()
        mock_pod = MagicMock()
        mock_pod.metadata = MagicMock()
        mock_pod.metadata.name = unittest.mock.sentinel.pod_
        mock_pods.list_.return_value = [mock_pod]
        mock_pod_interface.return_value = mock_pods
        k8s_response = MagicMock()
        k8s_response.returncode = 1
        mock_kubernetes_run.return_value = k8s_response
        macs = [
                "test_mac_1",
                "test_mac_2",
                "test_mac_3"
            ]

        result = await eviction.check_ovs_flows_present(
            k8s_client,
            unittest.mock.sentinel.namespace_,
            unittest.mock.sentinel.node_,
            macs

        )
        mock_pods.list_.assert_called_once_with(
                unittest.mock.sentinel.namespace_,
                label_selector={
                    "state.yaook.cloud/parent-name":
                        unittest.mock.sentinel.node_,
                    "state.yaook.cloud/component": "l2_agent"
                },
            )
        mock_kubernetes_run.assert_called_once_with(
            unittest.mock.ANY,
            namespace=unittest.mock.sentinel.namespace_,
            pod=unittest.mock.sentinel.pod_,
            container="neutron-openvswitch-agent",
            command=[
                "/bin/bash", "-c", 'test $(ovs-ofctl dump-flows br-int | '
                f'grep -Fo -e {macs[0]} -e {macs[1]} -e {macs[2]} | sort -u | '
                'wc -l) -eq 3']
            )
        self.assertFalse(result)


class Test_do_poll_migration(unittest.IsolatedAsyncioTestCase):
    async def test_server_already_migrated(self):
        mock_server = MagicMock()
        mock_server.hypervisor_hostname.return_value = "dest_host"
        openstack_client = unittest.mock.Mock(["compute"])
        openstack_client.compute.get_server.return_value = mock_server
        result = await eviction.poll_migration(openstack_client, None,
                                               "test_id", "src_host",
                                               "test_namespace")
        self.assertIsNone(result)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.get_migration_speed")
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_migration_not_started(self,
                                         mock_nova_client,
                                         mock_get_migration_speed,
                                         mock_set_migration_speed,
                                         mock_check_ovs_flows_present,
                                         mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = []
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute"])
        openstack_client.auth = {}
        openstack_client.compute.get_server.return_value = mock_server
        k8s_client = MagicMock()
        mock_get_migration_speed.return_value = \
            unittest.mock.sentinel.migration_speed_
        result = await eviction.poll_migration(openstack_client,
                                               k8s_client, "test_id",
                                               "src_host", "test_namespace")
        mock_set_migration_speed.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "src_host",
            unittest.mock.sentinel.instance_name_,
            unittest.mock.sentinel.migration_speed_)
        mock_check_ovs_flows_present.assert_not_called()
        self.assertEquals(result, 5)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_migration_started_flows_not_ready(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = False
        result = await eviction.poll_migration(
            openstack_client, k8s_client, "test_id", "src_host",
            "test_namespace")

        mock_set_migration_speed.assert_not_called()
        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_])
        self.assertEquals(result, 10)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_migration_started_flows_ready(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_migration.disk_remaining_bytes = 0
        mock_migration.memory_processed_bytes = 0
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = True
        result = await eviction.poll_migration(openstack_client, k8s_client,
                                               "test_id", "src_host",
                                               "test_namespace")

        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_])
        mock_set_migration_speed.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "src_host",
            unittest.mock.sentinel.instance_name_, 0)
        self.assertEquals(result, 10.0)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_migration_started_flows_ready_disk_pending(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_migration.disk_remaining_bytes = 1337
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = True
        result = await eviction.poll_migration(openstack_client, k8s_client,
                                               "test_id", "src_host",
                                               "test_namespace")

        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_])
        self.assertEquals(result, 10)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_migration_started_flows_ready_memory_started(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_migration.disk_remaining_bytes = 0
        mock_migration.memory_processed_bytes = 1337
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = True
        result = await eviction.poll_migration(openstack_client, k8s_client,
                                               "test_id", "src_host",
                                               "test_namespace")

        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_])
        self.assertEquals(result, None)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_ignores_port_without_port_security(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        mock_port2 = MagicMock()
        mock_port2.mac_address = unittest.mock.sentinel.mac2_
        mock_port2.is_port_security_enabled = False
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port, mock_port2]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = False
        result = await eviction.poll_migration(
            openstack_client, k8s_client, "test_id", "src_host",
            "test_namespace")

        mock_set_migration_speed.assert_not_called()
        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_])
        self.assertEquals(result, 10)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_works_with_mutliple_ports(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = True
        mock_port2 = MagicMock()
        mock_port2.mac_address = unittest.mock.sentinel.mac2_
        mock_port2.is_port_security_enabled = True
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port, mock_port2]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = False
        result = await eviction.poll_migration(
            openstack_client, k8s_client, "test_id", "src_host",
            "test_namespace")

        mock_set_migration_speed.assert_not_called()
        mock_check_ovs_flows_present.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "dest_host",
            [unittest.mock.sentinel.mac_, unittest.mock.sentinel.mac2_])
        self.assertEquals(result, 10)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    @patch("yaook.op.nova_compute.eviction.check_ovs_flows_present",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.set_migration_speed",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.Client")
    async def test_skips_flow_check_if_no_port_with_portsecurity(
            self, mock_nova_client, mock_set_migration_speed,
            mock_check_ovs_flows_present, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        mock_server = MagicMock()
        mock_server.hypervisor_hostname = "src_host"
        mock_server.instance_name = unittest.mock.sentinel.instance_name_
        mock_migration = MagicMock()
        mock_migration.dest_node = "dest_host"
        mock_migration.disk_remaining_bytes = 0
        mock_port = MagicMock()
        mock_port.mac_address = unittest.mock.sentinel.mac_
        mock_port.is_port_security_enabled = False
        nova_client = unittest.mock.Mock(["server_migrations"])
        nova_client.server_migrations.list.return_value = [mock_migration]
        mock_nova_client.return_value = nova_client
        openstack_client = unittest.mock.Mock(["compute", "network"])
        openstack_client.compute.get_server.return_value = mock_server
        openstack_client.network.ports.return_value = [mock_port]
        openstack_client.auth = {}
        k8s_client = MagicMock()
        mock_check_ovs_flows_present.return_value = True
        result = await eviction.poll_migration(openstack_client, k8s_client,
                                               "test_id", "src_host",
                                               "test_namespace")

        mock_check_ovs_flows_present.assert_not_called()
        mock_set_migration_speed.assert_called_once_with(
            unittest.mock.ANY, "test_namespace", "src_host",
            unittest.mock.sentinel.instance_name_, 0)
        self.assertEquals(result, None)


class Testdo_offline_migrate_server(unittest.TestCase):
    def test_scheduled_offline_migration(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = unittest.mock.sentinel.id_

        eviction.do_offline_migrate_server(conn, server_id, None, None)

        conn.compute.migrate_server.assert_called_once_with(server_id)


class Testdo_confirm_server(unittest.TestCase):
    def test_confirm_resize(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = unittest.mock.sentinel.id_

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))

            eviction.do_confirm_server(conn, server_id, None, None)

        conn.compute.post.assert_called_once_with(
            f"/servers/{server_id}/action",
            json={"confirmResize": None},
        )
        raise_.assert_called_once_with(conn.compute.post())


class Testdo_offload_server(unittest.TestCase):
    def test_offload(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = str(uuid.uuid4())

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))

            eviction.do_offload_server(conn, server_id, None, None)

        conn.compute.post.assert_called_once_with(
            f"/servers/{server_id}/action",
            json={"shelveOffload": None},
        )
        raise_.assert_called_once_with(conn.compute.post())


class Testdo_force_delete_server(unittest.TestCase):
    def test_force_deletion(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = str(uuid.uuid4())

        eviction.do_force_delete_server(conn, server_id, None, None)

        conn.compute.delete_server.assert_called_once_with(
            server_id,
            force=True,
        )


class Testdo_evacuate_server(unittest.TestCase):
    def test_evacuation(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = str(uuid.uuid4())

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))

            eviction.do_evacuate_server(conn, server_id, None, None)

        conn.compute.post.assert_called_once_with(
            f"/servers/{server_id}/action",
            json={
                "evacuate": {},
            },
        )
        raise_.assert_called_once_with(conn.compute.post())


class Testdo_activate_server(unittest.TestCase):
    def test_activation(self):
        conn = unittest.mock.Mock(["compute"])
        server_id = str(uuid.uuid4())

        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))

            eviction.do_activate_server(conn, server_id, None, None)

        conn.compute.post.assert_called_once_with(
            f"/servers/{server_id}/action",
            json={
                "os-resetState": {"state": "active"},
            },
        )
        raise_.assert_called_once_with(conn.compute.post())


class TestEvictor(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock(
            "yaook.statemachine.interfaces.ResourceInterfaceWithStatus",
        )
        self.connection_info = {
            "auth": unittest.mock.sentinel.auth,
            "interface": unittest.mock.sentinel.interface,
        }
        self.k8s_client = unittest.mock.Mock()
        self.e = eviction.Evictor(
            interface=self.intf,
            namespace=NAMESPACE,
            node_name=NAME,
            connection_info=self.connection_info,
            reason="some-reason",
            max_parallel_migrations=3,
            release=unittest.mock.sentinel.release,
            k8s_api_client=self.k8s_client
        )

        self.os_connection = unittest.mock.Mock()
        self.compute_service = unittest.mock.Mock(eviction.ComputeServiceInfo)
        self.compute_service.id_ = unittest.mock.sentinel.cs_id
        self.compute_service.host = NAME
        self.compute_service.binary = "nova-compute"
        self.compute_service.is_up = True
        self.compute_service.is_enabled = True
        self.compute_service.is_forced_down = False
        self.compute_service.disable_reason = None

        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = (self.os_connection)

        self.get_compute_service_mock = unittest.mock.Mock([])
        self.get_compute_service_mock.return_value = self.compute_service

        self.read_node_mock = unittest.mock.AsyncMock([])
        self.os_system_mock = unittest.mock.Mock([])

        self.compute_actions_mock = unittest.mock.Mock([])
        self.compute_actions_mock.return_value = []

        self.start_actions_mock = unittest.mock.Mock([])
        self.start_actions_mock.return_value = 0

        self.collect_server_status_mock = unittest.mock.Mock([])
        self.collect_server_status_mock.return_value = {
            k: []
            for k in eviction.ServerStatusClass
        }

        self.iterate_mock = unittest.mock.Mock([])

        self.__os_patches = [
            unittest.mock.patch.object(self.e, "_collect_server_status",
                                       new=self.collect_server_status_mock),
            unittest.mock.patch.object(self.e, "_start_actions",
                                       new=self.start_actions_mock),
            unittest.mock.patch.object(self.e, "_compute_actions",
                                       new=self.compute_actions_mock),
            unittest.mock.patch.object(self.e, "_dump_status"),
        ]
        self.__iterate_patches = [
            unittest.mock.patch.object(self.e, "_connect",
                                       new=self.connect_mock),
            unittest.mock.patch(
                "yaook.op.nova_compute.eviction.ComputeServiceInfo.get",
                new=self.get_compute_service_mock,
            ),
            unittest.mock.patch.object(self.e, "_iterate",
                                       new=self.iterate_mock),
        ]
        self.__os_iterate_patches = [
            unittest.mock.patch(
                "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.CoreV1Api.read_node", # noqa E501
                new=self.read_node_mock
            ),
            unittest.mock.patch(
                "yaook.op.nova_compute.eviction.os",
                new=self.os_system_mock
            ),
        ]
        self.__patches = []

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def _patch_os(self):
        for p in self.__os_patches:
            p.start()
            self.__patches.append(p)

    def _patch_iterate(self):
        for p in self.__iterate_patches:
            p.start()
            self.__patches.append(p)

    def _patch__os_iterate(self):
        for p in self.__os_iterate_patches:
            p.start()
            self.__patches.append(p)

    def _mock_server(self, *, id_=None, status="ACTIVE"):
        server = unittest.mock.Mock([])
        server.id = str(uuid.uuid4()) if id_ is None else id_
        server.status = status
        return server

    def test__connect(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(unittest.mock.patch(
                "openstack.connect",
            ))

            result = self.e._connect()

        connect.assert_called_once_with(
            auth=unittest.mock.sentinel.auth,
            interface=unittest.mock.sentinel.interface,
        )
        self.assertEqual(result, connect())
        self.assertEqual(connect().compute.default_microversion, "2.60")

    def test__collect_server_status_bins_servers_up(self):
        self.os_connection.compute.servers.side_effect = repeatable_generator([
            self._mock_server(id_=i, status=status)
            for i, status in enumerate(sorted(eviction.STATUS_MAP.keys()))
        ])

        result = self.e._collect_server_status(
            self.os_connection,
            eviction.STATUS_MAP,
        )

        self.os_connection.compute.servers.assert_called_once_with(
            host=NAME,
            all_tenants=True,
        )

        self.maxDiff = None
        self.assertDictEqual(
            {k: {v.id for v in vs} for k, vs in result.items()},
            {
                eviction.ServerStatusClass.MIGRATABLE: {0, 7},
                eviction.ServerStatusClass.PENDING: {1, 4, 6, 8, 9, 12, 14},
                eviction.ServerStatusClass.IGNORABLE: {2},
                eviction.ServerStatusClass.UNHANDLEABLE: {3, 10, 17},
                eviction.ServerStatusClass.MIGRATING: {5, 11},
                eviction.ServerStatusClass.OFFLOADABLE: {13},
                eviction.ServerStatusClass.OFFLINE_MIGRATABLE: {15},
                eviction.ServerStatusClass.FORCE_DELETABLE: {16},
                eviction.ServerStatusClass.CONFIRMABLE: {18},
                eviction.ServerStatusClass.EVACUATABLE: set(),
                eviction.ServerStatusClass.ACTIVATABLE: set(),
            }
        )

    def test__compute_actions_generates_live_migration_for_migratable(self):
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.MIGRATABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_live_migrate_server,
                    1.0,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_offline_migration_for_offline_migratable(self):  # NOQA
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.OFFLINE_MIGRATABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_offline_migrate_server,
                    1.0,
                    True,
                ),
            ],
        )

    def test__compute_actions_generates_force_deletion_for_force_deletable(self):  # NOQA
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.FORCE_DELETABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_force_delete_server,
                    0.5,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_offload_for_offloadable(self):
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.OFFLOADABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_offload_server,
                    0.75,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_confirm_for_confirmable(self):
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.CONFIRMABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_confirm_server,
                    0.25,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_activation_for_activatable(self):
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.ACTIVATABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_activate_server,
                    0.1,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_pending_for_activatable_rebuild_if_protected(self):  # NOQA
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "id1", unittest.mock.Mock(["__name__"]), 1.0,
                        causes_local_rebuild=True,
                    ),
                    eviction.ActionSpec(
                        "id2", unittest.mock.Mock(["__name__"]), 1.0,
                    ),
                    eviction.ActionSpec(
                        "id3", unittest.mock.Mock(["__name__"]), 1.0,
                        causes_local_rebuild=True,
                    ),
                ],
                10,
            ),
            3,
        )

        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.ACTIVATABLE] = [
            self._mock_server(id_="id1", status="REBUILD"),
            self._mock_server(id_="id2"),
            self._mock_server(id_="id3", status="ACTIVE"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id2",
                    eviction.do_activate_server,
                    0.1,
                    False,
                ),
                eviction.ActionSpec(
                    "id3",
                    eviction.do_activate_server,
                    0.1,
                    False,
                ),
            ],
        )

    def test__compute_actions_generates_evacuation_for_evacuatable(self):
        status = {k: [] for k in eviction.ServerStatusClass}
        status[eviction.ServerStatusClass.EVACUATABLE] = [
            self._mock_server(id_="id1"),
        ]

        result = self.e._compute_actions(status)

        self.assertCountEqual(
            result,
            [
                eviction.ActionSpec(
                    "id1",
                    eviction.do_evacuate_server,
                    0.2,
                    causes_local_rebuild=True,
                ),
            ],
        )

    def test__start_actions_starts_actions_up_to_cost(self):
        actions = [
            eviction.ActionSpec("id1", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
            eviction.ActionSpec("id2", unittest.mock.Mock(["__name__"]), 0.75,
                                False),
            eviction.ActionSpec("id3", unittest.mock.Mock(["__name__"]), 0.25,
                                False),
            eviction.ActionSpec("id4", unittest.mock.Mock(["__name__"]), 0.1,
                                False),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
            2,
        )

        self.assertEqual(result, 3)
        actions[0].action.assert_called_once_with(
            self.os_connection, "id1", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[1].action.assert_called_once_with(
            self.os_connection, "id2", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[2].action.assert_called_once_with(
            self.os_connection, "id3", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[3].action.assert_not_called()

    def test__start_actions_does_not_exceed_cost(self):
        actions = [
            eviction.ActionSpec("id1", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
            eviction.ActionSpec("id2", unittest.mock.Mock(["__name__"]), 0.75,
                                False),
            eviction.ActionSpec("id3", unittest.mock.Mock(["__name__"]), 0.2,
                                False),
            eviction.ActionSpec("id4", unittest.mock.Mock(["__name__"]), 0.1,
                                False),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
            2,
        )

        self.assertEqual(result, 3)
        actions[0].action.assert_called_once_with(
            self.os_connection, "id1", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[1].action.assert_called_once_with(
            self.os_connection, "id2", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[2].action.assert_called_once_with(
            self.os_connection, "id3", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[3].action.assert_not_called()

    def test__start_actions_starts_low_cost_actions_further_down_the_list_if_possible(self):  # NOQA
        actions = [
            eviction.ActionSpec("id1", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
            eviction.ActionSpec("id2", unittest.mock.Mock(["__name__"]), 0.75,
                                False),
            eviction.ActionSpec("id3", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
            eviction.ActionSpec("id4", unittest.mock.Mock(["__name__"]), 0.1,
                                False),
            eviction.ActionSpec("id5", unittest.mock.Mock(["__name__"]), 0.1,
                                False),
            eviction.ActionSpec("id6", unittest.mock.Mock(["__name__"]), 0.2,
                                False),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
            2,
        )

        self.assertEqual(result, 4)
        actions[0].action.assert_called_once_with(
            self.os_connection, "id1", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[1].action.assert_called_once_with(
            self.os_connection, "id2", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[2].action.assert_not_called()
        actions[3].action.assert_called_once_with(
            self.os_connection, "id4", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[4].action.assert_called_once_with(
            self.os_connection, "id5", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[5].action.assert_not_called()

    def test__start_actions_skips_and_tries_next_on_failure(self):
        actions = [
            eviction.ActionSpec("id1", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
            eviction.ActionSpec("id2", unittest.mock.Mock(["__name__"]), 0.75,
                                False),
            eviction.ActionSpec("id3", unittest.mock.Mock(["__name__"]), 0.2,
                                False),
            eviction.ActionSpec("id4", unittest.mock.Mock(["__name__"]), 0.1,
                                False),
        ]
        actions[2].action.side_effect = openstack.exceptions.HttpException

        result = self.e._start_actions(
            self.os_connection,
            actions,
            2,
        )

        self.assertEqual(result, 3)
        actions[0].action.assert_called_once_with(
            self.os_connection, "id1", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[1].action.assert_called_once_with(
            self.os_connection, "id2", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[2].action.assert_called_once_with(
            self.os_connection, "id3", unittest.mock.sentinel.release,
            self.k8s_client)
        actions[3].action.assert_called_once_with(
            self.os_connection, "id4", unittest.mock.sentinel.release,
            self.k8s_client)

    def test__start_actions_handles_less_actions_than_budget(self):
        actions = [
            eviction.ActionSpec("id1", unittest.mock.Mock(["__name__"]), 1.0,
                                False),
        ]

        result = self.e._start_actions(
            self.os_connection,
            actions,
            2,
        )

        self.assertEqual(result, 1)
        actions[0].action.assert_called_once_with(
            self.os_connection, "id1", unittest.mock.sentinel.release,
            self.k8s_client)

    def test__confirm_resize_does_nothing_if_no_server_was_migrated(self):
        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_not_called()
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_does_nothing_for_live_migrations(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        False,
                    ),
                ],
                1,
            ),
            1,
        )

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_not_called()
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_does_not_confirm_if_not_verify_resize(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="RESIZE")

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_repeatedly_checks_status_if_not_verify_resize(self):  # NOQA
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="RESIZE")

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize.assert_not_called()

        self.os_connection.compute.get_server.reset_mock()
        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="RESIZE")

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_calls_confirm_if_verify_resize(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="VERIFY_RESIZE")

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize\
            .assert_called_once_with("some-id")

    def test__confirm_resize_drops_server_if_not_resize(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="other_status")

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize.assert_not_called()

        self.os_connection.compute.get_server.reset_mock()

        self.e._confirm_resize(self.os_connection)

        self.os_connection.compute.get_server.assert_not_called()
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_double_checks(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "some-id",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="VERIFY_RESIZE")
        self.e._confirm_resize(self.os_connection)
        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize\
            .assert_called_once_with("some-id")

        self.os_connection.compute.get_server.reset_mock()
        self.os_connection.compute.confirm_server_resize.reset_mock()
        self.os_connection.compute.get_server.return_value = \
            self._mock_server(id_="some-id", status="ACTIVE")
        self.e._confirm_resize(self.os_connection)
        self.os_connection.compute.get_server.assert_called_once_with(
            "some-id",
        )
        self.os_connection.compute.confirm_server_resize.assert_not_called()

        self.os_connection.compute.get_server.reset_mock()
        self.os_connection.compute.confirm_server_resize.reset_mock()
        self.e._confirm_resize(self.os_connection)
        self.os_connection.compute.get_server.assert_not_called()
        self.os_connection.compute.confirm_server_resize.assert_not_called()

    def test__confirm_resize_handles_multiple_servers(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "id1",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                    eviction.ActionSpec(
                        "id2",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                    eviction.ActionSpec(
                        "id3",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                3,
            ),
            3,
        )

        def generate_server(id_):
            status = "ACTIVE"
            if id_ == "id2":
                status = "VERIFY_RESIZE"
            elif id_ == "id3":
                status = "VERIFY_RESIZE"
            return self._mock_server(id_=id_, status=status)

        self.os_connection.compute.get_server.side_effect = generate_server

        self.e._confirm_resize(self.os_connection)

        self.assertCountEqual(
            self.os_connection.compute.get_server.mock_calls,
            [
                unittest.mock.call("id1"),
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

        self.assertCountEqual(
            self.os_connection.compute.confirm_server_resize.mock_calls,
            [
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

    def test__confirm_resize_handles_http_errors_and_moves_on(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "id1",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                    eviction.ActionSpec(
                        "id2",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                    eviction.ActionSpec(
                        "id3",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                3,
            ),
            3,
        )

        def generate_server(id_):
            status = "ACTIVE"
            if id_ == "id2":
                status = "VERIFY_RESIZE"
            elif id_ == "id3":
                status = "VERIFY_RESIZE"
            return self._mock_server(id_=id_, status=status)

        self.os_connection.compute.get_server.side_effect = generate_server
        self.os_connection.compute.confirm_server_resize.side_effect = \
            openstack.exceptions.HttpException

        self.e._confirm_resize(self.os_connection)

        self.assertCountEqual(
            self.os_connection.compute.get_server.mock_calls,
            [
                unittest.mock.call("id1"),
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

        self.assertCountEqual(
            self.os_connection.compute.confirm_server_resize.mock_calls,
            [
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

    def test__confirm_resize_aggregates_work(self):
        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "id1",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                1,
            ),
            1,
        )

        self.assertEqual(
            self.e._start_actions(
                self.os_connection,
                [
                    eviction.ActionSpec(
                        "id2",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                    eviction.ActionSpec(
                        "id3",
                        unittest.mock.Mock(["__name__"]),
                        1.0,
                        True,
                    ),
                ],
                2,
            ),
            2,
        )

        def generate_server(id_):
            status = "ACTIVE"
            if id_ == "id2":
                status = "VERIFY_RESIZE"
            elif id_ == "id3":
                status = "VERIFY_RESIZE"
            return self._mock_server(id_=id_, status=status)

        self.os_connection.compute.get_server.side_effect = generate_server

        self.e._confirm_resize(self.os_connection)

        self.assertCountEqual(
            self.os_connection.compute.get_server.mock_calls,
            [
                unittest.mock.call("id1"),
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

        self.assertCountEqual(
            self.os_connection.compute.confirm_server_resize.mock_calls,
            [
                unittest.mock.call("id2"),
                unittest.mock.call("id3"),
            ],
        )

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    async def test__is_node_down_api_available(self, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        self._patch__os_iterate()
        self.read_node_mock.return_value = unittest.mock.Mock()
        self.read_node_mock.return_value.status.conditions = \
            [unittest.mock.Mock()]
        self.read_node_mock.return_value.status.conditions[0].type = 'Ready'
        self.read_node_mock.return_value.status.conditions[0].status = 'True'

        result = await self.e._is_node_down(NAME)
        self.assertFalse(result)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    async def test__is_node_down_not_pingable(self, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        self._patch__os_iterate()
        self.read_node_mock.return_value = unittest.mock.Mock()
        self.read_node_mock.return_value.status.conditions = \
            [unittest.mock.Mock()]
        self.read_node_mock.return_value.status.conditions[0].type = 'Ready'
        self.read_node_mock.return_value.status.conditions[0].status = \
            'NotAvailable'
        self.os_system_mock.system = unittest.mock.Mock()
        self.os_system_mock.system.return_value = 512

        result = await self.e._is_node_down(NAME)
        self.assertTrue(result)

    @patch(
        "yaook.op.nova_compute.eviction.kubernetes_asyncio.client.ApiClient")
    async def test__is_node_down_pingable(self, mock_k8s_client):
        mock_k8s_client.__aexit__.return_value = AsyncMock()
        self._patch__os_iterate()
        self.read_node_mock.return_value = unittest.mock.Mock()
        self.read_node_mock.return_value.status.conditions = \
            [unittest.mock.Mock()]
        self.read_node_mock.return_value.status.conditions[0].type = 'Ready'
        self.read_node_mock.return_value.status.conditions[0].status = \
            'NotAvailable'
        self.os_system_mock.system = unittest.mock.Mock()
        self.os_system_mock.system.return_value = 0
        result = await self.e._is_node_down(NAME)
        self.assertFalse(result)

    @patch("yaook.op.nova_compute.eviction.Evictor._is_node_down",
           new_callable=AsyncMock)
    @patch("yaook.op.nova_compute.eviction.asyncio.run")
    async def test__os_iterate_disables_compute_service(
            self,
            mock_asyncio_run,
            mock_is_node_down):
        self._patch_iterate()

        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.Mock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result

        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop

            self.e._os_iterate()

        self.connect_mock.assert_called_once_with()
        self.get_compute_service_mock.assert_called_once_with(
            self.os_connection.compute,
            host=NAME,
            binary="nova-compute",
        )
        mock_asyncio_run.assert_called_once_with(
            unittest.mock.ANY,
        )
        self.compute_service.disable.assert_called_once_with(
            self.os_connection.compute,
            "nova-compute-operator: some-reason",
        )

    @patch("yaook.op.nova_compute.eviction.asyncio.run")
    async def test__os_iterate_uses_up_status_map_if_up_and_present(
            self,
            _
            ):
        self._patch_iterate()

        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.Mock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result

        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop
            result = self.e._os_iterate()

        self.iterate_mock.assert_called_once_with(
            self.os_connection,
            eviction.STATUS_MAP,
        )

        self.assertEqual(result, self.iterate_mock())

    @patch("yaook.op.nova_compute.eviction.asyncio.run")
    async def test__os_iterate_uses_down_status_map_if_down_but_not_forced(
            self,
            mock_asyncio_run
            ):
        self._patch_iterate()
        self.compute_service.is_up = False
        self.compute_service.is_forced_down = False

        mock_asyncio_run.return_value = {}

        result = self.e._os_iterate()

        self.iterate_mock.assert_called_once_with(
            self.os_connection,
            eviction.DOWN_STATUS_MAP,
        )

        self.assertEqual(result, self.iterate_mock())

    async def test__os_iterate_uses_forced_down_status_map_if_service_gone(
            self):
        self._patch_iterate()

        self.get_compute_service_mock.side_effect = LookupError

        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.Mock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result

        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop
            result = self.e._os_iterate()

        self.iterate_mock.assert_called_once_with(
            self.os_connection,
            eviction.FORCED_DOWN_STATUS_MAP,
        )

        self.assertEqual(result, self.iterate_mock())

    @patch("yaook.op.nova_compute.eviction.asyncio.run")
    async def test__os_iterate_uses_forced_down_status_map_if_forced_down(
            self,
            mock_asyncio_run
            ):
        self._patch_iterate()

        self.compute_service.is_forced_down = True

        mock_asyncio_run.return_value = unittest.mock.sentinel.result

        result = self.e._os_iterate()

        self.iterate_mock.assert_called_once_with(
            self.os_connection,
            eviction.FORCED_DOWN_STATUS_MAP,
        )

        self.assertEqual(result, self.iterate_mock())

    def test__iterate_collects_and_starts_shuffled_actions(self):
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status[unittest.mock.sentinel.foo] = unittest.mock.sentinel.bar
        status = dict(status)

        self.compute_actions_mock.return_value = [
            unittest.mock.sentinel.actions
        ]

        def make_shuffle_observable(seq):
            self.assertEqual(seq, [unittest.mock.sentinel.actions])
            seq[:] = [unittest.mock.sentinel.shuffled]

        def check_shuffled(client, actions, max_cost):
            self.assertEqual(actions, [unittest.mock.sentinel.shuffled])
            return 0

        self.start_actions_mock.side_effect = check_shuffled

        with contextlib.ExitStack() as stack:
            shuffle = stack.enter_context(unittest.mock.patch(
                "random.shuffle"
            ))
            shuffle.side_effect = make_shuffle_observable
            self.e._iterate(
                self.os_connection,
                unittest.mock.sentinel.status_map,
            )

        shuffle.assert_called_once()
        self.collect_server_status_mock.assert_called_once_with(
            self.os_connection,
            unittest.mock.sentinel.status_map,
        )
        self.compute_actions_mock.assert_called_once_with(
            status,
        )
        self.start_actions_mock.assert_called_once_with(
            self.os_connection,
            unittest.mock.ANY,
            3,
        )

    def test__iterate_considers_in_progress_when_starting_actions(self):  # NOQA
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status[eviction.ServerStatusClass.MIGRATING] = [1, 2]
        status = dict(status)

        self.compute_actions_mock.return_value = [
            unittest.mock.sentinel.actions
        ]

        self.e._iterate(self.os_connection, unittest.mock.sentinel.status_map)

        self.start_actions_mock.assert_called_once_with(
            self.os_connection,
            unittest.mock.ANY,
            1,
        )

    def test__iterate_includes_started_migrations_in_final_count(self):
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status[eviction.ServerStatusClass.MIGRATABLE] = [1, 2, 3]
        status[eviction.ServerStatusClass.MIGRATING] = [4]
        status = dict(status)

        self.start_actions_mock.return_value = 2

        result = self.e._iterate(
            self.os_connection,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migratable_instances,
            4,
        )
        self.assertEqual(
            result.actions_in_progress,
            3,
        )

    def test__iterate_includes_pending_confirmations_in_in_progress(self):
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status[eviction.ServerStatusClass.MIGRATABLE] = [1, 2, 3]
        status[eviction.ServerStatusClass.MIGRATING] = [4]
        status = dict(status)

        self.os_connection.compute.get_server.return_value = self._mock_server(
            status="RESIZE",
        )

        self.start_actions_mock.return_value = 2

        self.e._confirm_needed.add("dummy")

        result = self.e._iterate(
            self.os_connection,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migratable_instances,
            4,
        )
        self.assertEqual(
            result.actions_in_progress,
            4,
        )

    def test__iterate_gathers_statistics(self):
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status[eviction.ServerStatusClass.MIGRATABLE] = [1, 2, 3]
        status[eviction.ServerStatusClass.OFFLINE_MIGRATABLE] = [12, 13]
        status[eviction.ServerStatusClass.MIGRATING] = [4]
        status[eviction.ServerStatusClass.PENDING] = [5, 6]
        status[eviction.ServerStatusClass.UNHANDLEABLE] = [
            self._mock_server(id_="7"),
            self._mock_server(id_="8"),
            self._mock_server(id_="9"),
            self._mock_server(id_="10"),
        ]
        status[eviction.ServerStatusClass.IGNORABLE] = [11]
        status = dict(status)

        result = self.e._iterate(
            self.os_connection,
            unittest.mock.sentinel.status_map,
        )

        self.assertEqual(
            result.migratable_instances,
            6,
        )
        self.assertEqual(
            result.actions_in_progress,
            1,
        )
        self.assertEqual(
            result.pending_instances,
            2,
        )
        self.assertEqual(
            result.unhandleable_instances,
            4,
        )

    def test__iterate_calls_confirm(self):
        self._patch_os()

        status = self.collect_server_status_mock.return_value
        status["foo"] = unittest.mock.sentinel.bar
        status = dict(status)

        with contextlib.ExitStack() as stack:
            confirm_resize = stack.enter_context(unittest.mock.patch.object(
                self.e, "_confirm_resize",
            ))

            self.e._iterate(self.os_connection,
                            unittest.mock.sentinel.status_map)

        confirm_resize.assert_called_once_with(self.os_connection)

    async def test__iterate_runs_os_iterate_in_executor(self):
        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.AsyncMock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result

        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop

            _os_iterate = stack.enter_context(unittest.mock.patch.object(
                self.e, "_os_iterate",
            ))

            result = await self.e.iterate()

        self.assertEqual(result, unittest.mock.sentinel.result)

        loop.run_in_executor.assert_awaited_once_with(
            None,
            _os_iterate,
        )

        get_event_loop.assert_called_once_with()

    async def test_post_status_updates_custom_resource_status(self):
        status = unittest.mock.Mock()
        status.as_json_object.return_value = {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            update_status = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))

            await self.e.post_status(status)

        update_status.assert_awaited_once_with(
            self.intf,
            NAMESPACE,
            NAME,
            conditions=unittest.mock.ANY,
        )

        _, _, kwargs = update_status.mock_calls[0]
        conditions = kwargs["conditions"]
        self.assertCountEqual(
            conditions,
            [
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.EVICTED.value,
                    status=resource.ResourceEvictingStatus.EVICTING.
                    value,
                    reason="some-reason",
                    message='{"foo": "bar"}',
                ),
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    status="False",
                    reason="some-reason",
                    message="Eviction in progress",
                ),
            ],
        )

    @patch("yaook.op.nova_compute.eviction.check_virsh_is_emtpy",
           new_callable=AsyncMock)
    async def test_is_migration_done_success(self, mock_check_virsh_is_emtpy):
        mock_check_virsh_is_emtpy.return_value = True
        status = eviction.EvictionStatus(migratable_instances=0,
                                         actions_in_progress=0,
                                         pending_instances=0,
                                         unhandleable_instances=0)
        self.e.is_forced_down = False
        result = await self.e.is_migration_done(status)

        self.assertTrue(result)
        mock_check_virsh_is_emtpy.assert_called_once_with(
            self.e._logger, self.k8s_client, NAMESPACE, NAME)

    @patch("yaook.op.nova_compute.eviction.check_virsh_is_emtpy",
           new_callable=AsyncMock)
    async def test_is_migration_done_virsh_check_unknown(
            self, mock_check_virsh_is_emtpy):
        mock_check_virsh_is_emtpy.return_value = None
        status = eviction.EvictionStatus(migratable_instances=0,
                                         actions_in_progress=0,
                                         pending_instances=0,
                                         unhandleable_instances=0)
        self.e.is_forced_down = False
        result = await self.e.is_migration_done(status)

        self.assertFalse(result)
        mock_check_virsh_is_emtpy.assert_called_once_with(
            self.e._logger, self.k8s_client, NAMESPACE, NAME)

    @patch("yaook.op.nova_compute.eviction.check_virsh_is_emtpy",
           new_callable=AsyncMock)
    async def test_is_migration_done_virsh_check_failed(
            self, mock_check_virsh_is_emtpy):
        mock_check_virsh_is_emtpy.return_value = False
        status = eviction.EvictionStatus(migratable_instances=0,
                                         actions_in_progress=0,
                                         pending_instances=0,
                                         unhandleable_instances=0)
        self.e.is_forced_down = False
        result = await self.e.is_migration_done(status)

        self.assertFalse(result)
        mock_check_virsh_is_emtpy.assert_called_once_with(
            self.e._logger, self.k8s_client, NAMESPACE, NAME)

    @patch("yaook.op.nova_compute.eviction.check_virsh_is_emtpy",
           new_callable=AsyncMock)
    async def test_is_migration_done_virsh_check_not_called(
            self, mock_check_virsh_is_emtpy):
        mock_check_virsh_is_emtpy.return_value = False
        status = eviction.EvictionStatus(migratable_instances=0,
                                         actions_in_progress=0,
                                         pending_instances=0,
                                         unhandleable_instances=0)
        self.e.is_forced_down = True
        result = await self.e.is_migration_done(status)

        self.assertTrue(result)
        mock_check_virsh_is_emtpy.assert_not_called()
