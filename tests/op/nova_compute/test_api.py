#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import os
import typing
import unittest.mock
import uuid

import yaook.op.common as op_common
import yaook.op.nova_compute as nova_compute
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource

from ... import testutils

NAMESPACE = "test-namespace"
NAME = "node2"


def _strip_nones(
        ds: typing.Iterable[typing.Mapping],
        ) -> typing.Iterable[typing.Mapping]:
    for d in ds:
        yield {
            k: v
            for k, v in d.items()
            if v is not None
        }


class NovaComputeNodeBaseTest(testutils.CustomL2UseResourceTestCase):
    # Needs to be disabled as the novacompute operator relies on SSA to
    # make patches to its own resource. Since our testutils do not implement
    # that this test can not pass.
    run_needs_update_test = False
    run_topology_spread_test = False
    # Needs to be disabled as the ApiState modifies the context
    run_context_modification_test = False

    def setUp(self):
        super().setUp()
        self.compute_status_mock = unittest.mock.Mock([])
        self.compute_status_mock.return_value = resource.ResourceStatus(
            up=True,
            enabled=True,
            disable_reason=None,
        )

        self.__patches = [
            unittest.mock.patch(
                "yaook.op.nova_compute.resources.ComputeStateResource"
                "._get_status",
                new=self.compute_status_mock,
            ),
            unittest.mock.patch(
                "yaook.op.nova_compute.resources.ComputeStateResource"
                "._update_status",
                new=self.compute_status_mock,
            ),
            unittest.mock.patch(
                "yaook.op.nova_compute.resources.ComputeStateResource"
                ".delete_nodes_from_hostaggreate"
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.base.write_ca_certificates"
            ),
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        self.__l2lock_patches = [
            unittest.mock.patch(
                "yaook.statemachine.resources.orchestration.L2Lock.reconcile",
            ),
        ]

        for p in self.__patches:
            p.start()
        self.start_l2lock_patches()

        self._env_backup = os.environ.pop(
            "YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE", None
        )
        os.environ["YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE"] = "dummy-image"
        # scheduling_key is needed, so testutils.CustomL2UseResourceTestCase.
        # test_delete_removes_annotation knows for wich key to filter the
        # nodes.
        self.scheduling_key = op_common.SchedulingKey.COMPUTE_HYPERVISOR.value

    def start_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.start()

    def stop_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.stop()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, op_common.NOVA_COMPUTE_OPERATOR_USER_SECRET,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": op_common.NOVA_COMPUTE_OPERATOR_USER_SECRET,
                },
                "data": {
                    "password": base64.b64encode(
                        "foobar2342".encode("utf-8"),
                    ).decode("ascii")
                },
            },
        )

    async def _prepare_message_queue(self):
        self._make_all_mqs_succeed_immediately()

        amqpservers = sm.amqpserver_interface(self.api_client)
        await amqpservers.create(
            NAMESPACE,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "AMQPServer",
                "metadata": {
                    "name": "amqp-server",
                },
                "spec": {},
                "status": {"phase": context.Phase.UPDATED.value}
            },
            "myfieldmanager"
        )

    def tearDown(self):
        self.stop_l2lock_patches()
        for p in self.__patches:
            p.stop()
        if self._env_backup is not None:
            os.environ["YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE"] = self._env_backup
        else:
            del os.environ["YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE"]
        super().tearDown()

    @classmethod
    def setUpClass(cls) -> None:
        if cls is NovaComputeNodeBaseTest:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()


class TestNovaComputeNode(NovaComputeNodeBaseTest):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_compute = {
            self.scheduling_key: str(uuid.uuid4()),
        }
        self.labels_compute_extra = dict(self.labels_compute)
        self.labels_compute_extra["extra-compute"] = str(uuid.uuid4())
        self.labels_compute_ceph = dict(self.labels_compute)
        self.labels_compute_ceph["extra-ceph"] = str(uuid.uuid4())
        self.default_node_setup = {
            "node1": {
                "unrelated": "key",
            },
            "node2": self.labels_compute,
            "node3": self.labels_compute,
            "node4": self.labels_compute_extra,
        }
        self.ceph_node_setup = dict(self.default_node_setup)
        self.ceph_node_setup["node3"] = self.labels_compute_ceph
        self.default_compute_nodes = ["node2", "node3", "node4"]

        self.ssh_key_secret_name = str(uuid.uuid4())
        self.public_keys_secret_name = str(uuid.uuid4())
        self.ca_config_map_name = str(uuid.uuid4())

        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            nova_compute.NovaComputeNode,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "publicKeysSecretRef": {
                        "name": self.public_keys_secret_name,
                    },
                    "caConfigMapName": self.ca_config_map_name,
                    "cephBackend": {
                        "enabled": False,
                    },
                    "novaConfig": [],
                    "messageQueue": {
                        "amqpServerRef": {
                            "name": "amqp-server",
                        },
                    },
                    "vnc": {
                        "issuerRef": {
                            "name": "vnc-ca-issuer"
                        },
                        "baseUrl": "vnc-base-url"
                    },
                    "state": "Enabled",
                    "targetRelease": "test",
                    "region": {
                        "name": "RegionOne",
                    },
                    "resources": testutils.generate_resources_dict(
                        "keygen",
                        "chown-nova",
                        "nova-compute",
                        "nova-compute-ssh",
                        "libvirtd",
                        "compute-evict-job",
                    ),
                },
                "status": {
                    "state": "Creating",
                },
            },
        )

        await self._prepare_message_queue()

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = sm.interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_creates_config_with_transport_url(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_config",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_user_password",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        nova_compute_conf = config.data["novacompute.conf"]
        cfg = testutils._parse_config(nova_compute_conf, decode=True)
        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://compute-{NAME}:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/ssl/certs/ca-bundle.crt",
        )

        await self.cr.sm.ensure(self.ctx)

        users = sm.interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_creates_config_with_vnc_section(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_config",
            },
        )
        nova_compute_conf = config.data["novacompute.conf"]
        cfg = testutils._parse_config(nova_compute_conf, decode=True)

        self.assertEqual(cfg.get('vnc', 'enabled'), 'true')
        self.assertEqual(cfg.get('vnc', 'novncproxy_base_url'), 'vnc-base-url')
        self.assertEqual(cfg.get('vnc', 'server_listen'), '0.0.0.0')

    async def test_compute_stateful_set_without_ceph_config(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        ceph_config_volume = [
            volume for volume in statefulset.spec.template.spec.volumes
            if volume.name == "ceph-configs"
        ][0]

        self.assertCountEqual(
            ceph_config_volume.projected.to_dict()["sources"],
            []
        )

    async def test_compute_agent_depends_on_network_l2(self):
        self._make_all_jobs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        compute, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        self.assertEqual(
            sm.LabelSelector.from_api_object(
                compute.spec.template.spec.affinity.pod_affinity.
                required_during_scheduling_ignored_during_execution[0].
                label_selector,
            ),
            sm.LabelSelector(
                match_labels={
                    op_common.LABEL_NETWORK_L2_PROVIDER: "true",
                }
            )
        )

    async def test_compute_stateful_set_links_to_service(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_service"},
        )
        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        self.assertEqual(
            statefulset.spec.service_name,
            service.metadata.name,
        )

    async def test_compute_stateful_set_pinned_to_node(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        self.assertEqual(
            statefulset.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.
            node_selector_terms[0].match_fields[0].to_dict(),
            {
                "key": "metadata.name",
                "operator": "In",
                "values": [NAME],
            },
        )

    async def test_compute_stateful_set_links_config_secret(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_config"},
        )

        config_volume = [
            volume for volume in statefulset.spec.template.spec.volumes
            if volume.name == "nova-config-volume"
        ][0]

        self.assertEqual(
            config_volume.secret.secret_name,
            config_secret.metadata.name,
        )

    async def test_compute_stateful_set_links_authorized_keys(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        public_keys_volume = [
            volume for volume in statefulset.spec.template.spec.volumes
            if volume.name == "public-key-material"
        ][0]

        self.assertEqual(
            public_keys_volume.secret.secret_name,
            self.public_keys_secret_name,
        )

    async def test_compute_statefullset_sets_root(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        for container in statefulset.spec.template.spec.containers:
            self.assertEqual(container.security_context.run_as_user, 0)

    async def test_compute_statefullset_sets_nova_id_for_keygen(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        self.assertEqual(
            statefulset.spec.template.spec.init_containers[0].
            security_context.run_as_user,
            2500008)

    async def test_compute_statefulset_sets_cinder_id(self):

        self._configure_cr(
            nova_compute.NovaComputeNode,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "targetRelease": "test",
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "publicKeysSecretRef": {
                        "name": self.public_keys_secret_name,
                    },
                    "caConfigMapName": self.ca_config_map_name,
                    "cephBackend": {
                        "enabled": False,
                    },
                    "novaConfig": [],
                    "messageQueue": {
                        "amqpServerRef": {
                            "name": "amqp-server",
                        },
                        "passwordSecretKeyRef": {
                            "name": "amqp-user-password",
                        },
                        "username": "compute-user",
                    },
                    "vnc": {
                        "issuerRef": {
                            "name": "vnc-ca-issuer"
                        },
                        "baseUrl": "vnc-base-url"
                    },
                    "state": "Enabled",
                    "ids": {
                        "cinderGid": "6789",
                    },
                },
                "status": {
                    "state": "Creating",
                },
            },
        )

        self._make_all_keystoneusers_complete_immediately()
        self._make_all_mq_users_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        for container in statefulset.spec.template.spec.containers:
            cinder_id = [x for x in container.env if x.name == "CINDER_GID"]
            self.assertEqual(
                1,
                len(cinder_id))
            self.assertEqual(cinder_id[0].value, "6789")

    async def test_compute_statefullset_sets_cinder_id_if_requested(self):
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_mq_users_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        for container in statefulset.spec.template.spec.containers:
            self.assertEqual(
                0,
                len([x for x in container.env if x.name == "CINDER_GID"]))

    async def test_creates_vnc_backend_certificate(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = sm.certificates_interface(self.api_client)
        services = sm.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_backend_certificate",
            },
        )

        self.assertIsNotNone(certificate)
        self.assertIn(f"{service.metadata.name}-vnc-backend.{NAMESPACE}.svc",
                      certificate["spec"]["dnsNames"])
        self.assertIn("yaook",
                      certificate["spec"]["subject"]["organizations"])
        self.assertEqual("3650h", certificate["spec"]["duration"])
        self.assertEqual("1460h", certificate["spec"]["renewBefore"])
        self.assertEqual("vnc-ca-issuer",
                         certificate["spec"]["issuerRef"]["name"])

    async def test_creates_sts_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = interfaces.stateful_set_interface(self.api_client)
        sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        self.assertEqual(
            testutils.container_resources(sts, 0, is_init=True),
            testutils.unique_resources("keygen")
        )
        self.assertEqual(
            testutils.container_resources(sts, 1, is_init=True),
            testutils.unique_resources("chown-nova")
        )
        self.assertEqual(
            testutils.container_resources(sts, 0),
            testutils.unique_resources("nova-compute")
        )
        self.assertEqual(
            testutils.container_resources(sts, 1),
            testutils.unique_resources("nova-compute-ssh")
        )
        self.assertEqual(
            testutils.container_resources(sts, 2),
            testutils.unique_resources("libvirtd")
        )

    async def test_creates_job_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        evict_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_state"},
        )

        self.assertEqual(
            testutils.container_resources(evict_job, 0),
            testutils.unique_resources("compute-evict-job")
        )

    async def test_evict_not_running_as_root(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        evict_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_state"},
        )
        security_context = evict_job._spec._template._spec._containers[0] \
            .security_context

        self.assertEqual(
            security_context.run_as_group, 2020
        )
        self.assertEqual(
            security_context.run_as_user, 2020
        )


class TestNovaComputeNode_WithCeph(NovaComputeNodeBaseTest):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_compute = {
            self.scheduling_key: str(uuid.uuid4()),
        }
        self.labels_compute_extra = dict(self.labels_compute)
        self.labels_compute_extra["extra-compute"] = str(uuid.uuid4())
        self.labels_compute_ceph = dict(self.labels_compute)
        self.labels_compute_ceph["extra-ceph"] = str(uuid.uuid4())
        self.default_node_setup = {
            "node1": {
                "unrelated": "key",
            },
            "node2": self.labels_compute,
            "node3": self.labels_compute,
            "node4": self.labels_compute_extra,
        }
        self.ceph_node_setup = dict(self.default_node_setup)
        self.ceph_node_setup["node3"] = self.labels_compute_ceph
        self.default_compute_nodes = ["node2", "node3", "node4"]

        self.ssh_key_secret_name = str(uuid.uuid4())
        self.public_keys_secret_name = str(uuid.uuid4())
        self.ca_config_map_name = str(uuid.uuid4())
        self.ceph_secret_uuid = str(uuid.uuid4())
        self.ceph_keyring_secret_name = str(uuid.uuid4())
        self.ceph_user_name = str(uuid.uuid4())

        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            nova_compute.NovaComputeNode,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "publicKeysSecretRef": {
                        "name": self.public_keys_secret_name,
                    },
                    "caConfigMapName": self.ca_config_map_name,
                    "cephBackend": {
                        "enabled": True,
                        "user": self.ceph_user_name,
                        "uuid": self.ceph_secret_uuid,
                        "keyringSecretName": self.ceph_keyring_secret_name,
                    },
                    "novaConfig": [],
                    "messageQueue": {
                        "amqpServerRef": {
                            "name": "amqp-server",
                        },
                        "passwordSecretKeyRef": {
                            "name": "amqp-user-password",
                        },
                        "username": "compute-user",
                    },
                    "targetRelease": "test",
                    "vnc": {
                        "issuerRef": {
                            "name": "vnc-ca-issuer"
                        },
                        "baseUrl": "vnc-base-url"
                    },
                    "state": "Enabled",
                    "region": {
                        "name": "RegionOne",
                    },
                },
                "status": {
                    "state": "Creating",
                },
            },
        )

        await self._prepare_message_queue()

    async def test_compute_stateful_set_with_ceph(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute"},
        )

        ceph_config_volume = [
            volume for volume in statefulset.spec.template.spec.volumes
            if volume.name == "ceph-configs"
        ][0]

        libvirt_container = [
            container
            for container in statefulset.spec.template.spec.containers
            if container.name == "libvirtd"
        ][0]

        ceph_config_volume_mount = [
            volume_mount
            for volume_mount in libvirt_container.volume_mounts
            if volume_mount.name == "ceph-configs"
        ][0]

        ceph_config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ceph_config"},
        )

        self.maxDiff = None
        self.assertCountEqual(
            _strip_nones(ceph_config_volume.projected.to_dict()["sources"]),
            [
                {
                    "secret": {
                        "name": self.ceph_keyring_secret_name,
                        "items": [
                            {
                                "key": self.ceph_user_name,
                                "path": "keyfile",
                                "mode": None,
                            },
                        ],
                        "optional": None,
                    },
                },
                {
                    "secret": {
                        "name": ceph_config_secret.metadata.name,
                        "items": [
                            {
                                "key": "ceph.conf",
                                "path": "ceph.conf",
                                "mode": None,
                            },
                            {
                                "key": "idmap",
                                "path": "idmap",
                                "mode": None,
                            },
                        ],
                        "optional": None,
                    },
                },
            ]
        )

        self.assertEqual(
            ceph_config_volume_mount.mount_path,
            "/etc/ceph",
        )

    async def test_update(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, {})
        agent_interface = \
            interfaces.nova_compute_node_interface(self.api_client)
        agent = await agent_interface.list_(
            NAMESPACE,
        )
        await self.cr.sm.ensure(self.ctx)
        agent, = await agent_interface.list_(
            NAMESPACE,
        )
        self.compute_status_mock.assert_called_with(
            self.ctx,
            await self.cr.api_state.get_openstack_connection_info(self.ctx),
            True
        )
        self.assertEqual(agent['status']['state'], 'Enabled')
