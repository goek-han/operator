#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest.mock

import yaook.op.keystone_resources as keystone_resources
import yaook.statemachine as sm

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "testendpoint"


class TestKeystoneEndpoint(testutils.CustomResourceTestCase):
    run_podspec_tests = False
    run_copy_on_write_naming_tests = False
    run_defaultsasecretmount_test = False
    run_imagepullsecrets_test = False
    run_needs_update_test = False
    run_cacerts_mysqlservice_test = False
    run_pdb_defined_test = False
    run_ingress_class_test = False
    run_ssl_terminator_has_correct_reload_test = False
    run_topology_spread_test = False
    run_context_modification_test = False

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone_resources.KeystoneEndpoint,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "KeystoneEndpoint",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                    "uid": "13aaaddf-2319-42d7-85a5-98c780738ce1",
                },
                "spec": {
                    "servicename": "nova",
                    "description": "Foo bar",
                    "servicetype": "compute",
                    "endpoints": {
                        "public": "abc",
                        "admin": "def",
                        "internal": "ghi",
                    },
                    "keystoneRef": {
                        "name": "target-keystone",
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname"
                    },
                },
            },
        )
        self.client_mock.put_object(
            "", "v1", "secrets", NAMESPACE,
            "keystone-admin-abcde",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "name": "keystone-admin-abcde",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "admin_credentials",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": sm.api_utils.encode_secret_data({
                    "OS_PASSWORD": "foobar2342",
                    "OS_USERNAME": "yaook-sys-maint",
                    "OS_PROJECT_NAME": "admin",
                    "OS_PROJECT_DOMAIN_NAME": "default",
                    "OS_USER_DOMAIN_NAME": "default",
                }),
            },
        )
        self.client_mock.put_object(
            "", "v1", "configmaps", NAMESPACE,
            "keystone-internal-api-defgh",
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "name": "keystone-internal-api-defgh",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "internal_config",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": {
                    "OS_AUTH_URL": "https://keystone-auth-url/v3",
                    "OS_IDENTITY_API_VERSION": "23",
                    "OS_INTERFACE": "internal",
                },
            },
        )
        self.client_mock.put_object(
            "", "v1", "configmaps", NAMESPACE,
            "keystone-ca-xyz",
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "name": "keystone-ca-xyz",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "ca_certs",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": {
                    "ca-bundle.crt": "ca_bundle"
                },
            },
        )
        self.client_mock.put_object(
            "yaook.cloud", "v1", "keystonedeployments",
            NAMESPACE, "target-keystone",
            {
                "apiVersion": "v1",
                "kind": "KeystoneDeployment",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "target-keystone",
                    "generation": 123,
                },
                "status": {
                    "observedGeneration": 123,
                    "updatedGeneration": 123,
                    "phase": "Updated",
                },
            }
        )

    async def test_reconciles_service(self):
        with contextlib.ExitStack() as stack:
            _reconcile_service = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr.create_endpoint, "_reconcile_service",
                )
            )

            await self.cr.sm.ensure(self.ctx)
        _reconcile_service.assert_called_once_with(
            unittest.mock.ANY,
            sm.resources._OpaqueParameters(params={
                "auth": {
                    "auth_url": "https://keystone-auth-url/v3",
                    "username": "yaook-sys-maint",
                    "password": "foobar2342",
                    "project_name": "admin",
                    "project_domain_name": "default",
                    "user_domain_name": "default",
                },
                "interface": "internal",
                "identity_api_version": "23",
                "cacert": "/tmp/13aaaddf-2319-42d7-85a5-98c780738ce1.crt",
                "region_name": "regionname",
            }),
            {
                "servicename": "nova",
                "description": "Foo bar",
                "servicetype": "compute",
                "endpoints": {
                    "public": "abc",
                    "admin": "def",
                    "internal": "ghi",
                },
                "keystoneRef": {
                    "name": "target-keystone",
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname"
                },
            },
        )
