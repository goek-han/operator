#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest.mock

import yaook.statemachine as sm
import yaook.op.common as op_common
import yaook.op.keystone_resources as keystone_resources

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "testuser"


class TestKeystoneUsers(testutils.CustomResourceTestCase):
    run_podspec_tests = False
    run_needs_update_test = False

    def tearDown(self):
        for patch in self.__patches:
            patch.stop()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone_resources.KeystoneUser,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "KeystoneUser",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                    "uid": "2b6a1849-c6ca-4883-8671-b7baa5c9beb9",
                },
                "spec": {
                    "keystoneRef": {
                        "name": "target-keystone",
                        "kind": "KeystoneDeployment",
                    },
                },
            },
        )
        self.client_mock.put_object(
            "", "v1", "secrets", NAMESPACE,
            "keystone-admin-abcde",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "name": "keystone-admin-abcde",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "admin_credentials",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": sm.api_utils.encode_secret_data({
                    "OS_PASSWORD": "foobar2342",
                    "OS_USERNAME": "yaook-sys-maint",
                    "OS_PROJECT_NAME": "admin",
                    "OS_PROJECT_DOMAIN_NAME": "default",
                    "OS_USER_DOMAIN_NAME": "default",
                }),
            },
        )
        self.client_mock.put_object(
            "", "v1", "configmaps", NAMESPACE,
            "keystone-internal-api-defgh",
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "name": "keystone-internal-api-defgh",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "internal_config",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": {
                    "OS_AUTH_URL": "https://keystone-auth-url/v3",
                    "OS_IDENTITY_API_VERSION": "23",
                    "OS_INTERFACE": "internal",
                },
            },
        )
        self.client_mock.put_object(
            "", "v1", "configmaps", NAMESPACE,
            "keystone-ca-xyz",
            {
                "apiVersion": "v1",
                "kind": "ConfigMap",
                "metadata": {
                    "name": "keystone-ca-xyz",
                    "namespace": NAMESPACE,
                    "labels": {
                        sm.context.LABEL_COMPONENT: "ca_certs",
                        sm.context.LABEL_PARENT_GROUP: "yaook.cloud",
                        sm.context.LABEL_PARENT_PLURAL: "keystonedeployments",
                        sm.context.LABEL_PARENT_NAME: "target-keystone",
                    },
                },
                "data": {
                    "ca-bundle.crt": "ca_bundle"
                },
            },
        )
        self.client_mock.put_object(
            "yaook.cloud", "v1", "keystonedeployments",
            NAMESPACE, "target-keystone",
            {
                "apiVersion": "v1",
                "kind": "KeystoneDeployment",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "target-keystone",
                    "generation": 123,
                },
                "status": {
                    "observedGeneration": 123,
                    "updatedGeneration": 123,
                    "phase": "Updated",
                },
            }
        )

        self._reconcile_user = unittest.mock.Mock()
        self.__patches = [
            unittest.mock.patch.object(
                self.cr.create_user, "_reconcile_user",
                new=self._reconcile_user,
            )
        ]
        for patch in self.__patches:
            patch.start()

    async def test_generates_credentials(self):
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        credentials, = await secrets.list_(
            NAMESPACE,
            {
                sm.context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
            },
        )

        data = sm.api_utils.decode_secret_data(credentials.data)
        self.assertEqual(data["OS_USER_DOMAIN_NAME"], "Default")
        self.assertEqual(data["OS_PROJECT_DOMAIN_NAME"], "Default")
        self.assertEqual(data["OS_USERNAME"],
                         "testuser.test-namespace.test.dns.domain")
        self.assertEqual(data["OS_PROJECT_NAME"], "service")
        self.assertEqual(data["OS_AUTH_TYPE"], "password")
        self.assertGreater(len(data["OS_PASSWORD"]), 32, data["OS_PASSWORD"])

    async def test_reconciles_user(self):
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        credentials, = await secrets.list_(
            NAMESPACE,
            {
                sm.context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
            },
        )
        data = sm.api_utils.decode_secret_data(credentials.data)

        self._reconcile_user.assert_called_once_with(
            unittest.mock.ANY,
            sm.resources._OpaqueParameters(params={
                "auth": {
                    "auth_url": "https://keystone-auth-url/v3",
                    "username": "yaook-sys-maint",
                    "password": "foobar2342",
                    "project_name": "admin",
                    "project_domain_name": "default",
                    "user_domain_name": "default",
                },
                "interface": "internal",
                "identity_api_version": "23",
                "cacert": "/tmp/2b6a1849-c6ca-4883-8671-b7baa5c9beb9.crt",
            }),
            sm.resources._OpaqueParameters(params={
                "username": data["OS_USERNAME"],
                "password": data["OS_PASSWORD"],
                "project_name": data["OS_PROJECT_NAME"],
            }),
        )
