#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import logging
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient
import openstack.exceptions
import keystoneauth1.exceptions

import yaook.op.keystone_resources.osresources as osresources
import yaook.statemachine as sm


def repeatable_generator(iterable):
    def generator(*args, **kwargs):
        yield from iterable
    return generator


class Testget_current_project(unittest.TestCase):
    def test_gets_project_from_project_id(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.get_project_id.return_value = \
            unittest.mock.sentinel.project_id
        client.identity.get_project.return_value = \
            unittest.mock.sentinel.project

        self.assertEqual(
            osresources.get_current_project(client),
            unittest.mock.sentinel.project,
        )

        client.identity.get_project.assert_called_once_with(
            unittest.mock.sentinel.project_id,
        )

        client.identity.get_project_id.assert_called_once_with()


class Testcreate_or_update_project(unittest.TestCase):
    def test_creates_project_if_it_does_not_exist_and_returns_it(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.projects.side_effect = repeatable_generator([])

        result = osresources.create_or_update_project(
            client,
            project_name=unittest.mock.sentinel.project_name,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.projects.assert_called_once_with(
            name=unittest.mock.sentinel.project_name,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.create_project.assert_called_once_with(
            name=unittest.mock.sentinel.project_name,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        self.assertEqual(result, client.identity.create_project())

    def test_returns_existing_project(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.projects.side_effect = repeatable_generator([
            unittest.mock.sentinel.project,
        ])

        result = osresources.create_or_update_project(
            client,
            project_name=unittest.mock.sentinel.project_name,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.projects.assert_called_once_with(
            name=unittest.mock.sentinel.project_name,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.create_project.assert_not_called()

        self.assertEqual(result, unittest.mock.sentinel.project)


class Testcreate_or_update_user(unittest.TestCase):
    def test_creates_user_if_it_does_not_exist_and_returns_it(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.users.side_effect = repeatable_generator([])

        result = osresources.create_or_update_user(
            client,
            None,
            None,
            username=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
            password=unittest.mock.sentinel.password,
        )

        client.identity.users.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.create_user.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
            password=unittest.mock.sentinel.password,
        )

        self.assertEqual(result, client.identity.create_user())

    def test_updates_user_if_it_exists_and_password_fails(self):
        user = unittest.mock.Mock(["id"])

        client = unittest.mock.Mock(["identity"])
        client.identity.users.side_effect = repeatable_generator([
            user,
        ])

        userclient = unittest.mock.Mock(["identity"])
        userclient.identity.get_token.side_effect = \
            keystoneauth1.exceptions.Unauthorized

        result = osresources.create_or_update_user(
            client,
            userclient,
            unittest.mock.Mock(),
            username=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
            password=unittest.mock.sentinel.password,
        )

        client.identity.users.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        userclient.identity.get_token.assert_called_once_with()

        client.identity.update_user.assert_called_once_with(
            user.id,
            password=unittest.mock.sentinel.password,
        )

        self.assertEqual(result, user)

    def test_does_nothing_if_user_exists_and_password_works(self):
        user = unittest.mock.Mock(["id"])

        client = unittest.mock.Mock(["identity"])
        client.identity.users.side_effect = repeatable_generator([
            user,
        ])

        userclient = unittest.mock.Mock(["identity"])
        userclient.identity.get_token.return_value = None

        result = osresources.create_or_update_user(
            client,
            userclient,
            unittest.mock.Mock(),
            username=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
            password=unittest.mock.sentinel.password,
        )

        client.identity.users.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        userclient.identity.get_token.assert_called_once_with()

        client.identity.update_user.assert_not_called()

        self.assertEqual(result, user)


class Testdelete_user(unittest.TestCase):
    def test_does_nothing_if_user_not_found(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.users.side_effect = repeatable_generator([])

        osresources.delete_user(
            client,
            username=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.users.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.delete_user.assert_not_called()

    def test_deletes_user_if_found(self):
        user = unittest.mock.Mock(["id"])

        client = unittest.mock.Mock(["identity"])
        client.identity.users.side_effect = repeatable_generator([
            user,
        ])

        osresources.delete_user(
            client,
            username=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.users.assert_called_once_with(
            name=unittest.mock.sentinel.username,
            domain_id=unittest.mock.sentinel.domain_id,
        )

        client.identity.delete_user.assert_called_once_with(user.id)


class Testcreate_or_update_role_assignment(unittest.TestCase):
    def setUp(self):
        self.admin_role = unittest.mock.Mock(["id"])
        self.user = unittest.mock.Mock(["id"])
        self.project = unittest.mock.Mock(["id", "assign_role_to_user"])

        self.client = unittest.mock.Mock(["identity"])
        self.client.identity.roles.side_effect = repeatable_generator([
            self.admin_role
        ])

    def test_assigns_role_to_user_via_project(self):
        osresources.create_or_update_project_membership(
            self.client,
            user=self.user,
            project=self.project,
            role=unittest.mock.sentinel.role_name,
        )

        self.client.identity.roles.assert_called_once_with(
            name=unittest.mock.sentinel.role_name,
        )

        self.project.assign_role_to_user.assert_called_once_with(
            self.client.identity,
            user=self.user,
            role=self.admin_role,
        )


class Testcreate_or_update_service(unittest.TestCase):
    def test_creates_service_if_it_does_not_exist_and_returns_it(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.services.side_effect = repeatable_generator([])

        result = osresources.create_or_update_service(
            client,
            type_=unittest.mock.sentinel.type_,
            name=unittest.mock.sentinel.name,
            description=unittest.mock.sentinel.description,
        )

        client.identity.services.assert_called_once_with(
            type=unittest.mock.sentinel.type_,
        )

        client.identity.create_service.assert_called_once_with(
            name=unittest.mock.sentinel.name,
            type=unittest.mock.sentinel.type_,
            description=unittest.mock.sentinel.description,
        )

        self.assertEqual(result, client.identity.create_service())

    def test_creates_service_if_existing_service_does_not_match_name(self):
        existing_service = unittest.mock.Mock([])
        existing_service.name = unittest.mock.sentinel.other_name

        client = unittest.mock.Mock(["identity"])
        client.identity.services.side_effect = repeatable_generator([
            existing_service,
        ])

        result = osresources.create_or_update_service(
            client,
            type_=unittest.mock.sentinel.type_,
            name=unittest.mock.sentinel.name,
            description=unittest.mock.sentinel.description,
        )

        client.identity.services.assert_called_once_with(
            type=unittest.mock.sentinel.type_,
        )

        client.identity.create_service.assert_called_once_with(
            name=unittest.mock.sentinel.name,
            type=unittest.mock.sentinel.type_,
            description=unittest.mock.sentinel.description,
        )

        self.assertEqual(result, client.identity.create_service())

    def test_updates_service_if_it_exists(self):
        existing_service = unittest.mock.Mock([])
        existing_service.name = unittest.mock.sentinel.other_name

        correct_service = unittest.mock.Mock(["id"])
        correct_service.name = unittest.mock.sentinel.name

        client = unittest.mock.Mock(["identity"])
        client.identity.services.side_effect = repeatable_generator([
            existing_service,
            correct_service,
        ])

        result = osresources.create_or_update_service(
            client,
            type_=unittest.mock.sentinel.type_,
            name=unittest.mock.sentinel.name,
            description=unittest.mock.sentinel.description,
        )

        client.identity.services.assert_called_once_with(
            type=unittest.mock.sentinel.type_,
        )

        client.identity.update_service.assert_called_once_with(
            correct_service.id,
            description=unittest.mock.sentinel.description,
        )

        self.assertEqual(result, correct_service)


class Testcreate_or_update_region(unittest.TestCase):
    def setUp(self):
        self.client = unittest.mock.Mock(["identity"])

    def test_creates_region_if_not_existing(self):
        self.client.identity.get_region.side_effect =\
            openstack.exceptions.NotFoundException

        osresources.create_or_update_region(self.client, "region1", None)

        self.client.identity.get_region.assert_called_once_with("region1")
        self.client.identity.create_region.assert_called_once_with(
            id="region1",
            parent_region_id=None,
        )
        self.client.identity.update_region.assert_not_called()

    def test_creates_region_if_not_existing_with_parent(self):
        self.client.identity.get_region.side_effect =\
            openstack.exceptions.NotFoundException

        osresources.create_or_update_region(
            self.client, "region1", "parentregion")

        self.client.identity.get_region.assert_any_call("region1")
        self.client.identity.get_region.assert_any_call("parentregion")
        self.client.identity.create_region.assert_any_call(
            id="parentregion",
            parent_region_id=None,
        )
        self.client.identity.create_region.assert_any_call(
            id="region1",
            parent_region_id="parentregion",
        )
        self.client.identity.update_region.assert_not_called()

    def test_nothing_if_region_exists(self):
        self.client.identity.get_region.side_effect = unittest.mock.Mock()

        osresources.create_or_update_region(self.client, "region1", None)

        self.client.identity.get_region.assert_called_once_with("region1")
        self.client.identity.create_region.assert_not_called()
        self.client.identity.update_region.assert_not_called()

    def test_updates_parent_region(self):
        r = unittest.mock.Mock("parent_region_id")
        r.parent_region_id = "wrong_parent_region"
        self.client.identity.get_region.return_value = r

        osresources.create_or_update_region(
            self.client, "region1", "correct_parent_region")

        self.client.identity.get_region.assert_any_call("region1")
        self.client.identity.get_region.assert_any_call(
            "correct_parent_region")
        self.client.identity.create_region.assert_not_called()
        self.client.identity.update_region.assert_called_once_with(
            "region1",
            parent_region_id="correct_parent_region",
        )


class Testreconcile_endpoints(unittest.TestCase):
    def setUp(self):
        self.service = unittest.mock.Mock(["id"])
        self.client = unittest.mock.Mock(["identity"])

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_endpoints_creates_missing_endpoint(
            self, create_or_update_region):
        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            None,
            existing_endpoints={},
            requested_endpoints={
                "interface1": "uri1",
            }
        )

        self.client.identity.create_endpoint.assert_called_once_with(
            service_id=self.service.id,
            region_id="region1",
            interface="interface1",
            url="uri1",
        )
        self.client.identity.update_endpoint.assert_not_called()
        self.client.identity.delete_endpoint.assert_not_called()
        create_or_update_region.assert_called_once_with(
            self.client,
            "region1",
            None
        )

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_endpoints_create_passes_parent_region(
            self, create_or_update_region):
        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            "parentregion",
            existing_endpoints={},
            requested_endpoints={
                "interface1": "uri1",
            }
        )

        self.client.identity.create_endpoint.assert_called_once_with(
            service_id=self.service.id,
            region_id="region1",
            interface="interface1",
            url="uri1",
        )
        self.client.identity.update_endpoint.assert_not_called()
        self.client.identity.delete_endpoint.assert_not_called()
        create_or_update_region.assert_called_once_with(
            self.client,
            "region1",
            "parentregion"
        )

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_endpoints_deletes_superfluous_endpoint(
            self, create_or_update_region):
        endpoint = unittest.mock.Mock(["id"])

        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            None,
            existing_endpoints={
                "interface1": endpoint,
            },
            requested_endpoints={},
        )

        self.client.identity.create_endpoint.assert_not_called()
        self.client.identity.update_endpoint.assert_not_called()
        self.client.identity.delete_endpoint.assert_called_once_with(
            endpoint.id,
        )
        create_or_update_region.assert_not_called()

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_updates_mismatching_existing_endpoint_url(
            self, create_or_update_region):
        endpoint = unittest.mock.Mock(["id", "url"])

        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            None,
            existing_endpoints={
                "interface1": endpoint,
            },
            requested_endpoints={
                "interface1": "some_url",
            },
        )

        self.client.identity.create_endpoint.assert_not_called()
        self.client.identity.delete_endpoint.assert_not_called()
        self.client.identity.update_endpoint.assert_called_once_with(
            endpoint.id,
            url="some_url",
            region_id="region1",
        )
        create_or_update_region.assert_not_called()

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_does_not_update_endpoint_with_matching_url(
            self, create_or_update_region):
        endpoint = unittest.mock.Mock(["id", "url"])
        endpoint.url = "some_url"

        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            None,
            existing_endpoints={
                "interface1": endpoint,
            },
            requested_endpoints={
                "interface1": "some_url",
            },
        )

        self.client.identity.create_endpoint.assert_not_called()
        self.client.identity.update_endpoint.assert_not_called()
        self.client.identity.delete_endpoint.assert_not_called()
        create_or_update_region.assert_not_called()

    @unittest.mock.patch(
        "yaook.op.keystone_resources.osresources.create_or_update_region")
    def test_reconcile_with_complex_scenario(self, create_or_update_region):
        ep1 = unittest.mock.Mock(["id", "url"])
        ep1.url = "url1"
        ep2 = unittest.mock.Mock(["id", "url"])
        ep2.url = "wrong_url"
        ep4 = unittest.mock.Mock(["id", "url"])

        osresources.reconcile_endpoints(
            self.client,
            self.service,
            "region1",
            "parentregion",
            existing_endpoints={
                "interface1": ep1,
                "interface2": ep2,
                "interface4": ep4,
            },
            requested_endpoints={
                "interface1": "url1",
                "interface2": "url2",
                "interface3": "url3",
            },
        )

        self.client.identity.create_endpoint.assert_called_once_with(
            service_id=self.service.id,
            region_id="region1",
            interface="interface3",
            url="url3",
        )
        self.client.identity.update_endpoint.assert_called_once_with(
           ep2.id,
           url="url2",
           region_id="region1",
        )
        self.client.identity.delete_endpoint.assert_called_once_with(
            ep4.id,
        )
        create_or_update_region.assert_called_once_with(
            self.client,
            "region1",
            "parentregion",
        )


class Testcreate_or_update_endpoints(unittest.TestCase):
    def test_maps_existing_endpoints_and_calls_reconcile(self):
        ep1 = unittest.mock.Mock([])
        ep1.region_id = unittest.mock.sentinel.r1
        ep1.interface = unittest.mock.sentinel.i1
        ep2 = unittest.mock.Mock([])
        ep2.region_id = unittest.mock.sentinel.r1
        ep2.interface = unittest.mock.sentinel.i2
        ep3 = unittest.mock.Mock([])
        ep3.region_id = unittest.mock.sentinel.r1
        ep3.interface = unittest.mock.sentinel.i3

        client = unittest.mock.Mock(["identity"])
        client.identity.endpoints.side_effect = repeatable_generator([
            ep1, ep2, ep3,
        ])

        service = unittest.mock.Mock(["id"])

        with contextlib.ExitStack() as stack:
            reconcile_endpoints = stack.enter_context(unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.reconcile_endpoints",
            ))

            osresources.create_or_update_endpoints(
                client,
                service,
                unittest.mock.sentinel.endpoint_map,
                sentinel.r1,
                "parentregion",
            )

        client.identity.endpoints.assert_called_once_with(
            service_id=service.id,
        )

        reconcile_endpoints.assert_called_once_with(
            client,
            service,
            sentinel.r1,
            "parentregion",
            {
                unittest.mock.sentinel.i1: ep1,
                unittest.mock.sentinel.i2: ep2,
                unittest.mock.sentinel.i3: ep3,
            },
            unittest.mock.sentinel.endpoint_map,
        )

    def test_maps_existing_endpoints_ignores_other_regions(self):
        ep1 = unittest.mock.Mock([])
        ep1.region_id = unittest.mock.sentinel.r1
        ep1.interface = unittest.mock.sentinel.i1
        ep2 = unittest.mock.Mock([])
        ep2.region_id = unittest.mock.sentinel.r2
        ep2.interface = unittest.mock.sentinel.i2
        ep3 = unittest.mock.Mock([])
        ep3.region_id = unittest.mock.sentinel.r3
        ep3.interface = unittest.mock.sentinel.i3

        client = unittest.mock.Mock(["identity"])
        client.identity.endpoints.side_effect = repeatable_generator([
            ep1, ep2, ep3,
        ])

        service = unittest.mock.Mock(["id"])

        with contextlib.ExitStack() as stack:
            reconcile_endpoints = stack.enter_context(unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.reconcile_endpoints",
            ))

            osresources.create_or_update_endpoints(
                client,
                service,
                unittest.mock.sentinel.endpoint_map,
                unittest.mock.sentinel.r2,
                "parentregion",
            )

        client.identity.endpoints.assert_called_once_with(
            service_id=service.id,
        )

        reconcile_endpoints.assert_called_once_with(
            client,
            service,
            unittest.mock.sentinel.r2,
            "parentregion",
            {
                unittest.mock.sentinel.i2: ep2,
            },
            unittest.mock.sentinel.endpoint_map,
        )


class Testget_service(unittest.TestCase):

    def test_get_service_when_exists(self):
        service = unittest.mock.Mock()

        client = unittest.mock.Mock(["identity"])
        client.identity.services.return_value = [service]

        ret = osresources.get_service(
            client,
            sentinel.servicetype,
        )

        self.assertEqual(service, ret)

        client.identity.services.assert_called_once_with(
            type=sentinel.servicetype,
        )

    def test_get_service_when_not_exists(self):
        client = unittest.mock.Mock(["identity"])
        client.identity.services.return_value = []

        ret = osresources.get_service(
            client,
            sentinel.servicetype,
        )

        self.assertEqual(None, ret)

        client.identity.services.assert_called_once_with(
            type=sentinel.servicetype,
        )


class Testdelete_endpoints(unittest.TestCase):
    def assert_not_called_with(self, mock, *args, **kwargs):
        try:
            mock.assert_any_call(*args, **kwargs)
        except AssertionError:
            return
        raise AssertionError(
            'Expected %s to not have been called.' %
            mock._format_mock_call_signature(args, kwargs))

    def test_deletes_endpoints(self):
        service = unittest.mock.Mock(["id"])
        service.id = unittest.mock.sentinel.service_id

        endpoint1 = unittest.mock.Mock(["id", "region_id"])
        endpoint1.id = unittest.mock.sentinel.endpoint1_id
        endpoint1.region_id = unittest.mock.sentinel.region_one

        endpoint2 = unittest.mock.Mock(["id", "region_id"])
        endpoint2.id = unittest.mock.sentinel.endpoint2_id
        endpoint2.region_id = unittest.mock.sentinel.region_two

        endpoint3 = unittest.mock.Mock(["id", "region_id"])
        endpoint3.id = unittest.mock.sentinel.endpoint3_id
        endpoint3.region_id = unittest.mock.sentinel.region_one

        client = unittest.mock.Mock(["identity"])
        client.identity.endpoints.return_value = [
            endpoint1, endpoint2, endpoint3
        ]

        osresources.delete_endpoints(
            client,
            service=service,
            region_id=unittest.mock.sentinel.region_one,
        )

        client.identity.endpoints.assert_called_once_with(
            service_id=unittest.mock.sentinel.service_id,
        )

        client.identity.delete_endpoint.assert_any_call(
            endpoint1.id,
        )

        self.assert_not_called_with(
            client.identity.delete_endpoint,
            endpoint2.id,
        )

        client.identity.delete_endpoint.assert_any_call(
            endpoint3.id,
        )


class Testdelete_service(unittest.TestCase):

    def test_deletes_service_with_no_endpoints(self):
        service = unittest.mock.Mock(["id"])
        service.id = unittest.mock.sentinel.id
        service.name = unittest.mock.sentinel.name

        client = unittest.mock.Mock(["identity"])
        client.identity.endpoints.return_value = []

        osresources.delete_service(
            client,
            service=service,
            logger=unittest.mock.Mock(),
        )

        client.identity.endpoints.assert_called_once_with(
            service_id=unittest.mock.sentinel.id,
        )

        client.identity.delete_service.assert_called_once_with(
            service.id,
        )

    def test_not_deletes_service_with_endpoints(self):
        service = unittest.mock.Mock(["id"])
        service.id = unittest.mock.sentinel.id
        service.name = unittest.mock.sentinel.name

        client = unittest.mock.Mock(["identity"])
        client.identity.endpoints.return_value = [
            unittest.mock.sentinel.someendpoint1,
        ]

        osresources.delete_service(
            client,
            service=service,
            logger=unittest.mock.Mock(),
        )

        client.identity.endpoints.assert_called_once_with(
            service_id=unittest.mock.sentinel.id,
        )

        client.identity.delete_service.assert_not_called()


class TestKeystoneResource(unittest.IsolatedAsyncioTestCase):
    class KeystoneResourceStateTest(sm.resources.KeystoneResource):
        async def update(self, ctx: sm.Context) -> bool:
            raise NotImplementedError

        async def delete(self, ctx: sm.Context):
            raise NotImplementedError

    def setUp(self):
        self.admin_credentials = unittest.mock.Mock(sm.KubernetesReference)
        self.endpoint_config = unittest.mock.Mock(sm.KubernetesReference)
        self.ca_config = unittest.mock.Mock(sm.KubernetesReference)
        self.krs = self.KeystoneResourceStateTest(
            admin_credentials=self.admin_credentials,
            endpoint_config=self.endpoint_config,
            ca_config=self.ca_config,
            finalizer=sentinel.finalizer,
        )

    def test_declares_dependencies(self):
        self.assertIn(self.admin_credentials, self.krs._dependencies)
        self.assertIn(self.endpoint_config, self.krs._dependencies)

    @unittest.mock.patch("yaook.statemachine.interfaces.secret_interface")
    @unittest.mock.patch("yaook.statemachine.interfaces.config_map_interface")
    @unittest.mock.patch("yaook.statemachine.resources.base.write_ca_certificates")  # noqa:E501
    async def test__make_connection_params_mixes_endpoint_config_and_creds(
            self,
            write_ca_certificates,
            config_map_interface,
            secret_interface):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {}

        self.admin_credentials.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.namespace,
            name=sentinel.secret_name,
        )

        self.endpoint_config.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.namespace,
            name=sentinel.cfg_name,
        )

        config_map_interface.return_value.read = unittest.mock.AsyncMock()
        config_map_interface.return_value.read.return_value = \
            kclient.V1ConfigMap(
                data={
                    "OS_AUTH_URL": sentinel.auth_url,
                    "OS_IDENTITY_API_VERSION": sentinel.identity_version,
                    "OS_INTERFACE": sentinel.interface,
                }
            )

        secret_interface.return_value.read = unittest.mock.AsyncMock()
        secret_interface.return_value.read.return_value = \
            kclient.V1Secret(
                data=sm.api_utils.encode_secret_data({
                    "OS_PASSWORD": "sentinel.password",
                    "OS_USERNAME": "sentinel.username",
                    "OS_USER_DOMAIN_NAME": "sentinel.user_domain_name",
                    "OS_PROJECT_NAME": "sentinel.project_name",
                    "OS_PROJECT_DOMAIN_NAME": "sentinel.project_domain_name",
                    "OS_AUTH_TYPE": "sentinel.auth_type",
                })
            )

        params = (await self.krs._get_connection_params(ctx)).params

        self.admin_credentials.get.assert_awaited_once_with(ctx)
        self.endpoint_config.get.assert_awaited_once_with(ctx)

        config_map_interface.assert_called_once_with(ctx.api_client)
        secret_interface.assert_called_once_with(ctx.api_client)
        config_map_interface.return_value.read.assert_awaited_once_with(
            sentinel.namespace,
            sentinel.cfg_name,
        )
        secret_interface.return_value.read.assert_awaited_once_with(
            sentinel.namespace,
            sentinel.secret_name,
        )

        self.assertEqual(params["interface"], sentinel.interface)
        self.assertEqual(params["identity_api_version"],
                         sentinel.identity_version)
        auth = params["auth"]
        self.assertEqual(auth["auth_url"], sentinel.auth_url)
        self.assertEqual(auth["password"], "sentinel.password")
        self.assertEqual(auth["username"], "sentinel.username")
        self.assertEqual(auth["user_domain_name"], "sentinel.user_domain_name")
        self.assertEqual(auth["project_name"], "sentinel.project_name")
        self.assertEqual(auth["project_domain_name"],
                         "sentinel.project_domain_name")

    @unittest.mock.patch("yaook.statemachine.interfaces.secret_interface")
    @unittest.mock.patch("yaook.statemachine.interfaces.config_map_interface")
    @unittest.mock.patch("yaook.statemachine.resources.base.write_ca_certificates")  # noqa:E501
    async def test__make_connection_params_with_region(
            self,
            write_ca_certificates,
            config_map_interface,
            secret_interface):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {}

        self.admin_credentials.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.namespace,
            name=sentinel.secret_name,
        )

        self.endpoint_config.get.return_value = kclient.V1ObjectReference(
            namespace=sentinel.namespace,
            name=sentinel.cfg_name,
        )

        config_map_interface.return_value.read = unittest.mock.AsyncMock()
        config_map_interface.return_value.read.return_value = \
            kclient.V1ConfigMap(
                data={
                    "OS_AUTH_URL": sentinel.auth_url,
                    "OS_IDENTITY_API_VERSION": sentinel.identity_version,
                    "OS_INTERFACE": sentinel.interface,
                }
            )

        secret_interface.return_value.read = unittest.mock.AsyncMock()
        secret_interface.return_value.read.return_value = \
            kclient.V1Secret(
                data=sm.api_utils.encode_secret_data({
                    "OS_PASSWORD": "sentinel.password",
                    "OS_USERNAME": "sentinel.username",
                    "OS_USER_DOMAIN_NAME": "sentinel.user_domain_name",
                    "OS_PROJECT_NAME": "sentinel.project_name",
                    "OS_PROJECT_DOMAIN_NAME": "sentinel.project_domain_name",
                    "OS_AUTH_TYPE": "sentinel.auth_type",
                })
            )

        params = (await self.krs._get_connection_params(
            ctx, region=sentinel.region_name)).params

        self.assertEqual(params["region_name"], sentinel.region_name)


class TestKeystoneUser(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.logger = logging.getLogger(__name__ + "." +
                                        type(self).__qualname__)
        self.user_secret_name = str(uuid.uuid4())
        self.user_password = str(uuid.uuid4())
        self.domain_id = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock()
        self.ctx.namespace = "testnamespace"
        self.ctx.parent_name = "parent-name"
        self.openstack_mock = unittest.mock.Mock([])
        self.openstack_mock_second = unittest.mock.Mock([])
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.side_effect = [
            self.openstack_mock, self.openstack_mock_second
        ]

        self.get_connection_params_mock = unittest.mock.AsyncMock()
        self.get_connection_params_mock.return_value = \
            sentinel.connection_params

        self.user_credentials = unittest.mock.Mock(sm.KubernetesReference)

        self.kus = osresources.KeystoneUser(
            admin_credentials=sentinel.admin_credentials,
            endpoint_config=sentinel.endpoint_config,
            ca_config=sentinel.ca_config,
            user_credentials=self.user_credentials,
            finalizer=str(uuid.uuid4()),
        )

        self.current_project_mock = unittest.mock.Mock([])
        self.current_project_mock.domain_id = self.domain_id
        self.get_current_project_mock = unittest.mock.Mock([])
        self.get_current_project_mock.return_value = self.current_project_mock

        self.auth_info = sm.resources._OpaqueParameters(params={
            "auth": {
                "auth_url": sentinel.auth_url,
                "password": sentinel.maint_password,
                "username": sentinel.maint_username,
                "user_domain_name": sentinel.user_domain_name,
                "project_name": sentinel.maint_project_name,
                "project_domain_name": sentinel.project_domain_name,
            },
            "interface": sentinel.interface,
        })
        self.auth_info_user = sm.resources._OpaqueParameters(params={
            "auth": {
                "auth_url": sentinel.auth_url,
                "password": sentinel.password,
                "username": sentinel.username,
                "user_domain_name": sentinel.user_domain_name,
                "project_name": sentinel.project_name,
                "project_domain_name": sentinel.project_domain_name,
            },
            "interface": sentinel.interface,
        })

        self._patches = [
            unittest.mock.patch("openstack.connect",
                                new=self.connect_mock),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.get_current_project",
                new=self.get_current_project_mock,
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources."
                "create_or_update_project",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources."
                "create_or_update_user",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.delete_user",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources."
                "create_or_update_project_membership",
            ),
            unittest.mock.patch.object(self.kus, "_get_connection_params",
                                       self.get_connection_params_mock),
        ]
        for p in self._patches:
            p.start()

    def tearDown(self):
        for p in reversed(self._patches):
            p.stop()

    def test_is_external_resource(self):
        self.assertIsInstance(self.kus, sm.ExternalResource)

    def test__reconcile_user_connects_to_openstack_with_given_credentials(self):  # noqa:E501
        self.kus._reconcile_user(
            self.logger,
            self.auth_info,
            sm.resources._OpaqueParameters(params={
                "username": sentinel.username,
                "password": sentinel.password,
                "project_name": sentinel.project_name,
            }),
        )

        self.connect_mock.assert_called_with(
            auth=self.auth_info.params["auth"],
            interface=sentinel.interface,
        )
        self.connect_mock.assert_called_with(
            auth=self.auth_info_user.params["auth"],
            interface=sentinel.interface,
        )

    def test__reconcile_user_manages_project(self):
        self.kus._reconcile_user(
            self.logger,
            self.auth_info,
            sm.resources._OpaqueParameters(params={
                "username": sentinel.username,
                "password": sentinel.password,
                "project_name": sentinel.project_name,
            }),
        )

        osresources.create_or_update_project.assert_called_once_with(
            self.openstack_mock,
            project_name=sentinel.project_name,
            domain_id=self.domain_id,
        )

    def test__reconcile_user_manages_user(self):
        self.kus._reconcile_user(
            self.logger,
            self.auth_info,
            sm.resources._OpaqueParameters(params={
                "username": sentinel.username,
                "password": sentinel.password,
                "project_name": sentinel.project_name,
            }),
        )

        osresources.create_or_update_user.assert_called_once_with(
            self.openstack_mock,
            self.openstack_mock_second,
            unittest.mock.ANY,
            username=unittest.mock.sentinel.username,
            domain_id=self.domain_id,
            password=unittest.mock.sentinel.password,
        )

    def test__reconcile_user_assigns_role(self):
        self.kus._reconcile_user(
            self.logger,
            self.auth_info,
            sm.resources._OpaqueParameters(params={
                "username": sentinel.username,
                "password": sentinel.password,
                "project_name": sentinel.project_name,
            }),
        )

        osresources.create_or_update_project_membership\
            .assert_called_once_with(
                self.openstack_mock,
                user=osresources.create_or_update_user(),
                project=osresources.create_or_update_project(),
                role="admin",
            )

    def test__delete_user_connects_to_openstack_with_given_parameters(self):
        self.kus._delete_user(
            self.logger,
            sm.resources._OpaqueParameters(params={
                "auth": sentinel.auth,
                "interface": sentinel.interface,
            }),
            unittest.mock.sentinel.username,
        )

        self.connect_mock.assert_called_once_with(
            auth=sentinel.auth,
            interface=sentinel.interface,
        )

    def test__delete_user_deletes_user(self):
        self.kus._delete_user(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            unittest.mock.sentinel.username,
        )

        osresources.delete_user.assert_called_once_with(
            self.openstack_mock,
            username=unittest.mock.sentinel.username,
            domain_id=self.domain_id,
        )

    @unittest.mock.patch("yaook.statemachine.secret_interface")
    async def test__get_user_credentials_wraps_in_opaque_params(
            self,
            secret_interface):
        secret_interface.return_value.read = unittest.mock.AsyncMock()
        secret_interface.return_value.read.return_value = kclient.V1Secret(
            data=sm.api_utils.encode_secret_data({
                "OS_PASSWORD": "the secret password",
                "OS_USERNAME": "target username",
                "OS_PROJECT_NAME": "target project name",
            })
        )

        self.user_credentials.get.return_value = kclient.V1ObjectReference(
            name=sentinel.user_credentials_name,
            namespace=sentinel.user_credentials_namespace,
        )

        ctx = unittest.mock.Mock()
        result = await self.kus._get_user_credentials(ctx)

        self.user_credentials.get.assert_awaited_once_with(ctx)

        secret_interface.assert_called_once_with(ctx.api_client)
        secret_interface.return_value.read.assert_awaited_once_with(
            sentinel.user_credentials_namespace,
            sentinel.user_credentials_name,
        )

        self.assertEqual(result.params["username"], "target username")
        self.assertEqual(result.params["project_name"], "target project name")
        self.assertEqual(result.params["password"], "the secret password")

    async def test_update_reads_secrets_and_runs_function_in_executor(self):
        with contextlib.ExitStack() as stack:
            loop = asyncio.get_event_loop()
            run_in_executor = stack.enter_context(unittest.mock.patch.object(
                loop, "run_in_executor",
                new=unittest.mock.AsyncMock(),
            ))

            _get_user_credentials = stack.enter_context(
                unittest.mock.patch.object(
                    self.kus, "_get_user_credentials",
                )
            )
            _get_user_credentials.return_value = sentinel.user_params

            await self.kus.update(
                self.ctx, {},
            )

        _get_user_credentials.assert_awaited_once_with(self.ctx)
        self.get_connection_params_mock.assert_awaited_once_with(self.ctx)

        run_in_executor.assert_awaited_once_with(
            None,
            self.kus._reconcile_user,
            self.ctx.logger,
            sentinel.connection_params,
            sentinel.user_params,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test_delete_gets_credentials_and_runs_function_in_executor(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "test.dns.domain"
        with contextlib.ExitStack() as stack:
            loop = asyncio.get_event_loop()
            run_in_executor = stack.enter_context(unittest.mock.patch.object(
                loop, "run_in_executor",
                new=unittest.mock.AsyncMock(),
            ))

            await self.kus.delete(
                self.ctx, {},
            )

        self.get_connection_params_mock.assert_awaited_once_with(self.ctx)

        run_in_executor.assert_awaited_once_with(
            None,
            self.kus._delete_user,
            self.ctx.logger,
            sentinel.connection_params,
            "parent-name.testnamespace.test.dns.domain",
        )


class TestKeystoneService(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.logger = logging.getLogger(__name__ + "." +
                                        type(self).__qualname__)
        self.username = str(uuid.uuid4())
        self.servicetype = str(uuid.uuid4())
        self.servicename = str(uuid.uuid4())
        self.description = str(uuid.uuid4())
        self.region_id = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock()
        self.ctx.parent_spec = {
            "servicename": self.servicename,
            "description": self.description,
            "servicetype": self.servicetype,
            "endpoints": {
                "public": "https://url1/",
                "internal": "https://url2/",
                "admin": "https://url3/",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
        }
        self.openstack_mock = unittest.mock.Mock([])
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = self.openstack_mock

        self.get_connection_params_mock = unittest.mock.AsyncMock()
        self.get_connection_params_mock.return_value = \
            sentinel.connection_params

        self.kes = osresources.KeystoneEndpoint(
            admin_credentials=sentinel.admin_credentials,
            endpoint_config=sentinel.endpoint_config,
            ca_config=sentinel.ca_config,
            finalizer=str(uuid.uuid4()),
        )

        self.openstack_service = unittest.mock.MagicMock()

        def create_get_service():
            mock = unittest.mock.MagicMock()
            mock.return_value = self.openstack_service
            return mock

        self._patches = [
            unittest.mock.patch("openstack.connect",
                                new=self.connect_mock),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources."
                "create_or_update_service",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources."
                "create_or_update_endpoints",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.get_service",
                new_callable=create_get_service,
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.delete_endpoints",
            ),
            unittest.mock.patch(
                "yaook.op.keystone_resources.osresources.delete_service",
            ),
            unittest.mock.patch.object(self.kes, "_get_connection_params",
                                       self.get_connection_params_mock),
        ]
        for p in self._patches:
            p.start()

    def tearDown(self):
        for p in reversed(self._patches):
            p.stop()

    def test_is_external_resource(self):
        self.assertIsInstance(self.kes, sm.ExternalResource)

    def test__reconcile_service_connects_to_openstack_with_admin_credentials(self):  # NOQA
        self.kes._reconcile_service(
            self.logger,
            sm.resources._OpaqueParameters(params={
                "auth": sentinel.auth,
                "interface": sentinel.interface,
            }),
            self.ctx.parent_spec,
        )

        self.connect_mock.assert_called_once_with(
            auth=sentinel.auth,
            interface=sentinel.interface,
        )

    def test__reconcile_service_manages_service(self):
        self.kes._reconcile_service(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            self.ctx.parent_spec,
        )

        osresources.create_or_update_service.assert_called_once_with(
            self.openstack_mock,
            type_=self.servicetype,
            name=self.servicename,
            description=self.description,
        )

    def test__reconcile_service_defaults_description_to_empty_string(self):
        spec = dict(self.ctx.parent_spec)
        del spec["description"]

        self.kes._reconcile_service(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            spec,
        )

        osresources.create_or_update_service.assert_called_once_with(
            self.openstack_mock,
            type_=self.servicetype,
            name=self.servicename,
            description="",
        )

    def test__reconcile_service_manages_endpoints(self):
        self.kes._reconcile_service(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            self.ctx.parent_spec,
        )

        osresources.create_or_update_endpoints.assert_called_once_with(
            self.openstack_mock,
            osresources.create_or_update_service(),
            {
                "public": "https://url1/",
                "internal": "https://url2/",
                "admin": "https://url3/",
            },
            "regionname",
            "parentregionname",
        )

    def test__delete_service_connects_to_openstack_with_given_credentials(self):  # noqa: E501
        self.kes._delete_service(
            self.logger,
            sm.resources._OpaqueParameters(params={
                "auth": sentinel.auth,
                "interface": sentinel.interface,
            }),
            {
                "servicetype": sentinel.servicetype,
                "region": {
                    "name": sentinel.region_name
                }
            },
        )

        self.connect_mock.assert_called_once_with(
            auth=sentinel.auth,
            interface=sentinel.interface,
        )

    def test__delete_service_deletes_the_service(self):
        self.kes._delete_service(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            {
                "servicetype": sentinel.servicetype,
                "region": {
                    "name": sentinel.region_name
                }
            },
        )

        osresources.delete_endpoints.assert_called_once_with(
            self.openstack_mock,
            self.openstack_service,
            sentinel.region_name,
        )

        osresources.delete_service.assert_called_once_with(
            self.openstack_mock,
            self.openstack_service,
            unittest.mock.ANY,
        )

    def test__delete_service_does_nothing_without_service(self):
        osresources.get_service.return_value = None

        self.kes._delete_service(
            self.logger,
            sm.resources._OpaqueParameters(params={}),
            {
                "servicetype": sentinel.servicetype,
                "region": {
                    "name": sentinel.region_name
                }
            },
        )

        osresources.delete_endpoints.assert_not_called()

        osresources.delete_service.assert_not_called()

    async def test_update_gets_credentials_and_runs_function_in_executor(self):
        with contextlib.ExitStack() as stack:
            loop = asyncio.get_event_loop()
            run_in_executor = stack.enter_context(unittest.mock.patch.object(
                loop, "run_in_executor",
                new=unittest.mock.AsyncMock(),
            ))

            await self.kes.update(
                self.ctx, {},
            )

        self.get_connection_params_mock.assert_awaited_once_with(self.ctx)

        run_in_executor.assert_awaited_once_with(
            None,
            self.kes._reconcile_service,
            self.ctx.logger,
            sentinel.connection_params,
            self.ctx.parent_spec,
        )

    async def test_delete_gets_credentials_and_runs_function_in_executor(self):
        with contextlib.ExitStack() as stack:
            loop = asyncio.get_event_loop()
            run_in_executor = stack.enter_context(unittest.mock.patch.object(
                loop, "run_in_executor",
                new=unittest.mock.AsyncMock(),
            ))

            await self.kes.delete(
                self.ctx, {},
            )

        self.get_connection_params_mock.assert_awaited_once_with(self.ctx)

        run_in_executor.assert_awaited_once_with(
            None,
            self.kes._delete_service,
            self.ctx.logger,
            sentinel.connection_params,
            self.ctx.parent_spec,
        )
