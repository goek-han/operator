#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import json
import os
import pprint
import unittest

import ddt

import yaook.op.common as op_common
import yaook.op.cinder as cinder
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "cinder"
CONFIG_FILE_NAME = "cinder.conf"
CONFIG_PATH = "/etc/cinder"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "volume:create_snapshot"
POLICY_RULE_VALUE = "is_admin:True"
CINDER_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


def _get_cinder_deployment_yaml(keystone_name):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
            "issuerRef": {
                "name": "issuername"
            },
            "api": {
                "ingress": {
                    "fqdn": "cinder-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 2,
                "resources": testutils.generate_resources_dict(
                    "api.cinder-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            "database": {
                "replicas": 2,
                "storageSize": "8Gi",
                "storageClassName": "foo-class",
                "proxy": {
                    "replicas": 1,
                    "resources": testutils.generate_db_proxy_resources(),
                },
                "backup": {
                    "schedule": "0 * * * *"
                },
                "resources": testutils.generate_db_resources(),
            },
            "messageQueue": {
                "replicas": 1,
                "storageSize": "2Gi",
                "storageClassName": "bar-class",
                "resources": testutils.generate_amqp_resources(),
            },
            "scheduler": {
                "replicas": 3,
                "resources": testutils.generate_resources_dict(
                    "scheduler.cinder-scheduler",
                ),
            },
            "cinderConfig": {},
            "cinderSecrets": [
                {
                    "secretName": CONFIG_SECRET_NAME,
                    "items": [{
                        "key": CONFIG_SECRET_KEY,
                        "path": "/DEFAULT/mytestsecret",
                    }],
                },
            ],
            "conversionVolume": {
                "emptyDir": {
                    "medium": "Memory",
                    "sizeLimit": "1Gi"
                }
            },
            "backends": {
                "test": {
                    "volume": {
                        "replicas": 4,
                        "resources": testutils.generate_resources_dict(
                            "volume.cinder-volume",
                        ),
                    },
                    "rbd": {
                        "keyringReference": "cinder-client-key",
                        "keyringUsername": "cinder",
                        "backendConfig": {
                            "someConfig": "someValue",
                            "rbd_pool": "cinder-test"
                        }
                    }
                }

            },
            "backup": {
                "cinderbackup1": {
                    "replicas": 2,
                    "cinderConfig": {
                        "DEFAULT": {"specialkey": "specialvalue"}
                    },
                    "cinderConfigSecrets": [{
                        "secretName": "cinderbackupspecialsecret",
                        "items": [{
                            "key": "mysecretkey",
                            "path": "/DEFAULT/specialsecretkey",
                        }],
                    }],
                    "terminationGracePeriod": 4242,
                    "resources": testutils.generate_resources_dict(
                        "backup.cinderbackup1.cinder-backup",
                    ),
                },
                "cinderbackup2": {
                    "replicas": 2,
                    "cinderConfig": {
                        "DEFAULT": {"specialkey": "specialvalue2"}
                    },
                    "cinderConfigSecrets": [{
                        "secretName": "cinderbackupspecialsecret",
                        "items": [{
                            "key": "mysecretkey",
                            "path": "/DEFAULT/specialsecretkey",
                        }],
                    }],
                    "terminationGracePeriod": 1337,
                    "resources": testutils.generate_resources_dict(
                        "backup.cinderbackup2.cinder-backup",
                    ),
                },
            },
            "databaseCleanup": {
                "schedule": "0 0 * * *",
                "deletionTimeRange": 13,
            },
            "targetRelease": "queens",
            "jobResources": testutils.generate_resources_dict(
                "job.cinder-db-sync-job",
                "job.cinder-db-upgrade-pre-job",
                "job.cinder-db-upgrade-post-job",
                "job.cinder-db-cleanup-cronjob",
            ),
        },
    }


@ddt.ddt
class TestCinderDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            cinder.Cinder,
            _get_cinder_deployment_yaml(self._keystone_name),
        )

        self.client_mock.put_object(
            "", "v1", "secrets", NAMESPACE, "cinderbackupspecialsecret",
            {
                "metadata": {
                    "name": "cinderbackupspecialsecret",
                    "namespace": NAMESPACE,
                },
                "data": {
                    # mysecretkey: highlysecretvalue
                    "mysecretkey": "aGlnaGx5c2VjcmV0dmFsdWU="
                }
            }
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = _get_cinder_deployment_yaml(self._keystone_name)
        self._configure_cr(cinder.Cinder, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_cinder_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(cinder.Cinder, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["cinder.conf"], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_schedule_cronjob(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_cleanup",
            }
        )

        self.assertEqual(cronjob.spec.schedule, "0 0 * * *")
        self.assertEqual(
            "13",
            cronjob.spec.job_template.spec.template.spec
            .containers[0].command[3]
        )

    async def test_db_cleanup_is_not_concurrent(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_cleanup",
            }
        )

        self.assertEqual(cronjob.spec.concurrency_policy, "Forbid")

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "cinder-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_creates_db_sync_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

    async def test_creates_db_cleanup_and_halts(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        cronjob_interface = interfaces.cronjob_interface(self.api_client)

        all_cronjobs = await cronjob_interface.list_(NAMESPACE)
        self.assertEqual(len(all_cronjobs), 1)
        db_cleanup, = all_cronjobs

        self.assertEqual(db_cleanup.metadata.labels[context.LABEL_COMPONENT],
                         "db_cleanup")

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 2)

    async def test_creates_scheduler_statefulset_with_replicas(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulset_interface = interfaces.stateful_set_interface(
            self.api_client
        )
        statefulset, = await statefulset_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "scheduler"}
        )
        self.assertEqual(statefulset.spec.replicas, 3)

    async def test_creates_volume_deployment_with_replicas(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(
            self.api_client
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 4)

    async def test_creates_deployments_with_rconfig(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(
            self.api_client
        )
        secrets = interfaces.secret_interface(self.api_client)

        deployments = await deployment_interface.list_(NAMESPACE,)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        config_volume_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},
        )

        assert deployments, "No deployments were found"
        for deployment in deployments:
            volume = deployment.spec.template.spec.volumes[0]
            if deployment.metadata.name == "cinder-api":
                secret_name = volume.projected.sources[0].secret.name
                self.assertEqual(secret_name, config_secret.metadata.name)
            else:
                secret_name = volume.secret.secret_name
                self.assertEqual(secret_name,
                                 config_volume_secret.metadata.name)

    async def test_creates_statefulsets_with_config(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        statefulset_interface = interfaces.stateful_set_interface(
            self.api_client
        )
        secrets = interfaces.secret_interface(self.api_client)

        statefulsets = await statefulset_interface.list_(NAMESPACE,)
        statefulsets = [
            x for x in statefulsets if
            x.metadata.labels[context.LABEL_COMPONENT]
            != "backup_agent_statefulsets"]
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        assert statefulsets, "No statefulsets were found"
        for statefulset in statefulsets:
            self.assertEqual(
                statefulset.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
            )

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            self.assertEqual(
                job.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
                str(job.metadata),
            )

    async def test_creates_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "foo-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_database_uri(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        cinder_conf = config.data["cinder.conf"]
        cfg = testutils._parse_config(cinder_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "cinderdeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "cinder-api",
        )

        cinder_conf = config.data["cinder.conf"]
        cfg = testutils._parse_config(cinder_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 1)
        self.assertEqual(mq["spec"]["storageSize"], "2Gi")
        self.assertEqual(mq["spec"]["storageClassName"], "bar-class")

    async def test_amqp_server_frontendIssuer_name(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        amqpserver = interfaces.amqpserver_interface(self.api_client)

        amqp, = await amqpserver.list_(
            NAMESPACE,
        )

        self.assertEqual(
            "issuername",
            amqp["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_transport_url(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        cinder_conf = config.data["cinder.conf"]
        cfg = testutils._parse_config(cinder_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://api:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "cinder-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "cinder-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://cinder-ingress:8080/v3/%(project_id)s",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "cinder")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "cinder-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "cinder")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "cinder-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_PARENT_PLURAL: "cinderdeployments"}
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.BLOCK_STORAGE_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.
                    BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_scheduler_statefulset(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = interfaces.stateful_set_interface(self.api_client)
        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "scheduler"}
        )

        self.assertEqual(
            statefulset.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_CINDER_SCHEDULER.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            statefulset.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.
                    BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.
                    BLOCK_STORAGE_CINDER_SCHEDULER.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_volume_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_CINDER_VOLUME.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.
                    BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.
                    BLOCK_STORAGE_CINDER_VOLUME.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        assert jobs, "No jobs were found"
        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_CINDER.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_CINDER.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    @ddt.data({}, CINDER_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        cinder_deployment_yaml = _get_cinder_deployment_yaml(
            self._keystone_name,
        )
        cinder_deployment_yaml["spec"].update(policy)
        self._configure_cr(cinder.Cinder, cinder_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cinder_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            cinder_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, CINDER_POLICY)
    async def test_creates_api_deployment_with_cert_volume(self, policy):
        cinder_deployment_yaml = _get_cinder_deployment_yaml(
            self._keystone_name,
        )
        cinder_deployment_yaml["spec"].update(policy)
        self._configure_cr(cinder.Cinder, cinder_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "cinderdeployments"
            },
        )

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 7)

        expected_volume_name = "ca-certs"

        self.assertEqual(
            len(api_deployment.spec.template.spec.containers[0].volume_mounts),
            3
        )
        cert_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[1]
        self.assertEqual(cert_volume_mount.name, expected_volume_name)
        self.assertEqual(cert_volume_mount.mount_path, "/etc/pki/tls/certs")

        cert_volume = api_deployment.spec.template.spec.volumes[1]
        self.assertEqual(cert_volume.name, expected_volume_name)
        self.assertEqual(cert_volume.config_map.name, ca_certs.metadata.name)

    @ddt.data({}, CINDER_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        cinder_deployment_yaml = _get_cinder_deployment_yaml(
            self._keystone_name,
        )
        cinder_deployment_yaml["spec"].update(policy)
        self._configure_cr(cinder.Cinder, cinder_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "cinder_policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 7)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    async def test_creates_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        cronjobs = interfaces.cronjob_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        volume_deployment, = await deployments.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        backup_sts_list = await statefulsets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "backup_agent_statefulsets"}
        )
        scheduler_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "scheduler"}
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )
        db_cleanup_job, = await cronjobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_cleanup"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.cinder-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(volume_deployment, 0),
            testutils.unique_resources("volume.cinder-volume")
        )
        self.assertEqual(
            testutils.container_resources(backup_sts_list[0], 0),
            testutils.unique_resources("backup.cinderbackup1.cinder-backup")
        )
        self.assertEqual(
            testutils.container_resources(backup_sts_list[1], 0),
            testutils.unique_resources("backup.cinderbackup2.cinder-backup")
        )
        self.assertEqual(
            testutils.container_resources(scheduler_sts, 0),
            testutils.unique_resources("scheduler.cinder-scheduler")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.cinder-db-sync-job")
        )
        self.assertEqual(
            testutils.container_resources(db_cleanup_job, 0),
            testutils.unique_resources("job.cinder-db-cleanup-cronjob")
        )

    @ddt.data({}, CINDER_POLICY)
    async def test_creates_policy_configmap(self, policy):
        cinder_deployment_yaml = _get_cinder_deployment_yaml(
            self._keystone_name,
        )
        cinder_deployment_yaml["spec"].update(policy)
        self._configure_cr(cinder.Cinder, cinder_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        cinder_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "cinder_policy"},
        )

        self.assertEqual(cinder_policy.kind, "ConfigMap")
        self.assertTrue(
            cinder_policy.metadata.name.startswith("cinder-policy"))

        cinder_policy_decoded = json.loads(
            cinder_policy.data[POLICY_FILE_NAME])

        for rule, value in policy.get("policy", {}).items():
            self.assertEqual(
                cinder_policy_decoded[rule],
                value
            )

    async def test_configures_backup_statefulset(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ss_intf = interfaces.stateful_set_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        backup_statefulsets = await ss_intf.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "backup_agent_statefulsets"},
        )

        self.assertEqual(
            len(backup_statefulsets),
            2,
        )

        for backup_service in ["cinderbackup1", "cinderbackup2"]:
            cfg, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "backup_configs",
                    context.LABEL_INSTANCE: backup_service,
                },
            )

            cinder_cfg = testutils._parse_config(
                cfg.data["cinder.conf"],
                decode=True,
            )

            self.assertIn(f"backup-{backup_service}",
                          cinder_cfg.get("DEFAULT", "transport_url"))

            self.assertIn(f"backup-{backup_service}",
                          cinder_cfg.get("database", "connection"))

            self.assertIn(f"backup-{backup_service}",
                          cinder_cfg.get("keystone_authtoken", "username"))

    async def test_not_sets_uid_gid(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        depl_intf = interfaces.deployment_interface(self.api_client)
        ss_intf = interfaces.stateful_set_interface(self.api_client)

        def test_env_not_contains_ids(container):
            if not isinstance(container, dict):
                container = container.to_dict()
            for env in container["env"]:
                if env["name"] in ["CINDER_UID", "CINDER_GID"]:
                    self.fail(
                        "should not set cinder uid or gid for container %s" %
                        container)

        api, = await depl_intf.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        test_env_not_contains_ids(api.spec.template.spec.containers[0])

        statefulsets = await ss_intf.list_(
                NAMESPACE,
        )
        for statefulset in statefulsets:
            test_env_not_contains_ids(
                statefulset.spec.template.spec.containers[0])

    async def test_sets_uid_gid_if_requested(self):
        cinderdeployment = _get_cinder_deployment_yaml(self._keystone_name)
        cinderdeployment["spec"]["ids"] = {"uid": "1234", "gid": "6789"}
        self._configure_cr(
            cinder.Cinder,
            cinderdeployment,
        )

        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        depl_intf = interfaces.deployment_interface(self.api_client)
        ss_intf = interfaces.stateful_set_interface(self.api_client)

        def test_env_not_contains_ids(container):
            if not isinstance(container, dict):
                container = container.to_dict()
            for env in container["env"]:
                if env["name"] == "CINDER_UID":
                    self.assertEqual(env["value"], "1234")
                elif env["name"] == "CINDER_GID":
                    self.assertEqual(env["value"], "6789")

        api, = await depl_intf.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        test_env_not_contains_ids(api.spec.template.spec.containers[0])

        statefulsets = await ss_intf.list_(
                NAMESPACE,
        )
        for statefulset in statefulsets:
            test_env_not_contains_ids(
                statefulset.spec.template.spec.containers[0])

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config"},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)
        lines = decoded["cinder.conf"].splitlines()

        self.assertIn(f"mytestsecret = {CONFIG_SECRET_VALUE}", lines)

    async def test_creates_volume_deployment_with_conversion_volume(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(
            self.api_client
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        conversion_volume = [
            volume
            for volume in deployment.spec.template.spec.volumes
            if volume.name == "varlibcinderconversion"
        ][0]
        self.assertEqual(conversion_volume.empty_dir.medium, "Memory")
        self.assertEqual(conversion_volume.empty_dir.size_limit, "1Gi")

    async def test_created_config_correct_region(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configs = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "backup_configs"},
        )

        for config in configs:
            cfg = testutils._parse_config(config.data["cinder.conf"],
                                          decode=True)

            self.assertEqual(cfg.get("keystone_authtoken", "region_name"),
                             "regionname")
            self.assertEqual(cfg.get("keystone_authtoken", "os_region_name"),
                             "regionname")


class TestCinderDeploymentsCeph(testutils.ReleaseAwareCustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            cinder.Cinder,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "cinder-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "scheduler": {
                        "replicas": 3
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "messageQueue": {
                        "replicas": 1,
                        "storageSize": "2Gi",
                    },
                    "cinderConfig": {
                        "ceph": {}
                    },
                    "conversionVolume": {
                        "emptyDir": {
                            "medium": ""
                        }
                    },
                    "backends": {
                        "ceph":
                        {
                            "volume": {
                                "replicas": 1
                            },
                            "rbd": {
                                "keyringReference": "cinder-client-key",
                                "keyringUsername": "cinder",
                                "backendConfig": {
                                    "someConfig": "someValue",
                                    "rbd_pool": "cinder-test"
                                }
                            }
                        }
                    },
                    "databaseCleanup": {
                        "schedule": "0 0 * * *",
                        "deletionTimeRange": 13,
                    },
                    "targetRelease": "queens",
                },
            },
        )
        self._provide_keystone(NAMESPACE)

    async def test_creates_config_with_ceph(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        keystone_conf = config.data["cinder.conf"]
        ceph_cfg = testutils._parse_config(config.data["ceph.conf"],
                                           decode=True)
        self.assertIn("rook", ceph_cfg.get("global", "mon_host"))

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertEqual("cinder.volume.drivers.rbd.RBDDriver",
                         cfg.get("ceph", "volume_driver"))

    async def test_deployments_template(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        volume_mounts = deployment.spec.template.spec.containers[0].\
            volume_mounts
        mount_paths = [x.mount_path for x in volume_mounts]

        self.assertIn("/etc/ceph/", mount_paths)

    async def test_deployment_template_keyfile(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )
        volumes = deployment.spec.template.spec.volumes
        key = [x.projected.sources for x in volumes if
               x.name == 'cinder-ceph-conf']

        self.assertEqual("cinder-client-key", key[0][1].secret.name)

    async def test_backend_host_ceph(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()

        self.ctx.parent_spec['backends']['ceph'] = {
            "rbd": {
                "keyringReference": "cinder-client-key",
                "keyringUsername": "cinder",
                "backendConfig": {
                    "someConfig": "someValue",
                    "backend_host": "hostname",
                    "rbd_pool": "cinder-test"
                }
            }
        }
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        keystone_conf = config.data["cinder.conf"]

        cfg = testutils._parse_config(keystone_conf, decode=True)

        self.assertEqual("hostname", cfg.get("ceph", "backend_host"))
        self.assertEqual("someValue", cfg.get("ceph", "someConfig"))

    async def test_backend_host_default_ceph(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        ceph_conf = config.data["cinder.conf"]
        cfg = testutils._parse_config(ceph_conf, decode=True)

        self.assertEqual("ceph-cinder-test", cfg.get("ceph", "backend_host"))
        self.assertEqual("someValue", cfg.get("ceph", "someConfig"))


class TestCinderDeploymentsNetapp(
        testutils.ReleaseAwareCustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            cinder.Cinder,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "cinder-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "scheduler": {
                        "replicas": 3
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "messageQueue": {
                        "replicas": 1,
                        "storageSize": "2Gi",
                    },
                    "conversionVolume": {
                        "emptyDir": {
                            "medium": ""
                        }
                    },
                    "backends": {
                        "netappbackend": {
                            "volume": {
                                "replicas": 4
                            },
                            "netapp": {
                                "login": "username",
                                "passwordReference": "netapp-password",
                                "server": "server",
                                "vserver": "vserver",
                                "copyoffloadConfigMap": {
                                    "name": "copyoffloadConfigMap",
                                },
                                "backendConfig": {
                                    "someConfig": "someValue",
                                },
                                "shares": ["someshare"]
                            }
                        }
                    },
                    "databaseCleanup": {
                        "schedule": "0 0 * * *",
                        "deletionTimeRange": 13,
                    },
                    "targetRelease": "queens",
                },
            },
        )
        self._provide_keystone(NAMESPACE)
        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, "netapp-password",
            {
                "apiVersion": "v1",
                "kind": "Secrets",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "netapp-password",
                },
                "data": {
                    "password": "bmV0YXBwcGFzc3dvcmQ=",
                },
            }
        )

    async def test_copyoffload_support(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        mount_point = "/na_copyoffload/na_copyoffload_64"
        # it is displayed in dezimal mode 0755==493
        default_mode = 0o755

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )

        cinder_volume = deployment.spec.template.spec.containers[0]
        cinder_config_map = \
            deployment.spec.template.spec.volumes[3].config_map
        self.assertEqual(
            mount_point, cinder_volume.volume_mounts[3].mount_path)
        self.assertEqual(default_mode, cinder_config_map.default_mode)

    async def test_creates_config_with_netapp(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        keystone_conf = config.data["cinder.conf"]

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertEqual("cinder.volume.drivers.netapp.common.NetAppDriver",
                         cfg.get("netappbackend", "volume_driver"))

    async def test_backend_host_netapp(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()

        self.ctx.parent_spec['backends'] = {
            "netappbackend": {
                "netapp": {
                    "login": "username",
                    "passwordReference": "netapp-password",
                    "server": "server",
                    "vserver": "vserver",
                    "copyoffloadConfigMap": {
                        "name": "copyoffloadConfigMap",
                    },
                    "backendConfig": {
                        "someConfig": "someValue",
                        "backend_host": "hostname"
                    },
                    "shares": ["someshare"]
                }
            }
        }
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        keystone_conf = config.data["cinder.conf"]

        cfg = testutils._parse_config(keystone_conf, decode=True)

        self.assertEqual("hostname", cfg.get("netappbackend", "backend_host"))
        self.assertEqual("someValue", cfg.get("netappbackend", "someConfig"))

    async def test_backend_host_default_ceph(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_config"},)
        ceph_conf = config.data["cinder.conf"]
        cfg = testutils._parse_config(ceph_conf, decode=True)

        self.assertEqual("server", cfg.get("netappbackend", "backend_host"))
        self.assertEqual("someValue", cfg.get("netappbackend", "someConfig"))

    async def test_statefulset_template(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "volume_deployment"}
        )

        cinder_volume = deployment.spec.template.spec.containers[0]
        self.assertIn("SYS_ADMIN",
                      cinder_volume.security_context.capabilities.add)

    async def test_creates_config_without_ceph(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["cinder.conf"]
        self.assertNotIn("ceph.conf", config.data)

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertNotIn("cinder_store", cfg.keys())


@ddt.ddt
class TestCinderDeploymentsUpgrade(
        testutils.ReleaseAwareCustomResourceTestCase):
    async def test_sets_deployedopenstackversion_on_success(self):
        raise unittest.SkipTest("not relevant")

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        cr = _get_cinder_deployment_yaml(self._keystone_name)
        cr["metadata"]["generation"] = 1
        cr["spec"]["imagePullSecrets"] = [
            {
                "name": "testpullsecret",
            }
        ]
        self._configure_cr(
            cinder.Cinder,
            cr,
        )

        self.client_mock.put_object(
            "", "v1", "secrets", NAMESPACE, "cinderbackupspecialsecret",
            {
                "metadata": {
                    "name": "cinderbackupspecialsecret",
                    "namespace": NAMESPACE,
                },
                "data": {
                    # mysecretkey: highlysecretvalue
                    "mysecretkey": "aGlnaGx5c2VjcmV0dmFsdWU="
                }
            }
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

        self._make_all_databases_ready_immediately()
        hooks = copy.deepcopy(self.client_mock._hooks)
        self._make_all_dependencies_complete_immediately()
        await self.cr.reconcile(self.ctx, 0)
        self.client_mock._hooks = hooks

        cr = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME,
        )
        cr["spec"]["targetRelease"] = "rocky"
        cr["metadata"]["generation"] = 2
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME, cr,
        )
        self.ctx = self._make_context()  # needed to set the updated spec

    async def test_does_upgrade(self):
        self._make_all_dependencies_complete_immediately()
        cinder = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("queens", cinder["status"]["installedRelease"])

        await self.cr.reconcile(self.ctx, 0)
        cinder = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("rocky", cinder["status"]["installedRelease"])
