#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import ddt
import uuid

import yaook.op.common as op_common
import yaook.statemachine as sm
import yaook.op.ceilometer as ceilometer
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "ceilometer"
CONFIG_FILE_NAME = "ceilometer.conf"
CONFIG_PATH = "/etc/ceilometer"


def _get_ceilometer_deployment_yaml(keystone_name):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "central": {
                "replicas": 3,
                "resources": testutils.generate_resources_dict(
                    "central.ceilometer-agent-central",
                ),
            },
            "notification": {
                "replicas": 3,
                "resources": testutils.generate_resources_dict(
                    "notification.ceilometer-agent-notification",
                ),
            },
            "messageQueue": {
                "replicas": 1,
                "storageSize": "2Gi",
                "storageClassName": "foo-class",
                "resources": testutils.generate_resources_dict(
                    "messageQueue.rabbitmq",
                ),
            },
            "issuerRef": {
                "name": "issuername"
            },
            "ceilometerConfig": {
                "DEFAULT": {
                },
            },
            "ceilometerCompute": {
                "configTemplates": [
                    {
                        "nodeSelectors": [
                            {"matchLabels": {}},  # all nodes
                        ],
                        "ceilometerComputeConfig": {
                        },
                    },
                ],
                "resources": testutils.generate_resources_dict(
                    "ceilometerCompute.ceilometer-compute-agent",
                ),
            },
            "targetRelease": "queens",
            "jobResources": testutils.generate_resources_dict(
                "job.ceilometer-upgrade-job",
            ),
        },
    }


@ddt.ddt
class TestCeilometerDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.MessageQueueTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self.labels_compute = {
            op_common.SchedulingKey.COMPUTE_HYPERVISOR.value:
                str(uuid.uuid4()),
        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_compute,
        }
        self.compute_nodes = ["node2"]
        self._configure_cr(
            ceilometer.Ceilometer,
            _get_ceilometer_deployment_yaml(self._keystone_name),
        )
        self._mock_issuer_with_secret("issuername")

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)

    async def test_agent_keystone_users_matches_keystone_reference(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        keystoneusers = interfaces.keystoneuser_interface(self.api_client)
        users = await keystoneusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "ceilometerdeployments",
            },
        )

        self.assertGreater(len(users), 1)

        for user in users:
            self.assertEqual(user["spec"]["keystoneRef"]["name"],
                             self._keystone_name)

    async def test_created_config_matches_keystone_user(self):
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["ceilometer.conf"],
                                      decode=True)

        self.assertEqual(cfg.get("service_credentials", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("service_credentials", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 1)
        self.assertEqual(mq["spec"]["storageSize"], "2Gi")
        self.assertEqual(mq["spec"]["storageClassName"], "foo-class")

    async def test_creates_config_with_transport_url(self):
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        ceilometer_conf = config.data["ceilometer.conf"]
        cfg = testutils._parse_config(ceilometer_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://api:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/ssl/certs/ca-bundle.crt",
        )

    async def test_applies_scheduling_key_to_central_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "central_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                CEILOMETER_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                CEILOMETER_CENTRAL.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.
                    CEILOMETER_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.
                    CEILOMETER_CENTRAL.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_notification_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "notification_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                CEILOMETER_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                CEILOMETER_NOTIFICATION.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.
                    CEILOMETER_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.
                    CEILOMETER_NOTIFICATION.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        assert jobs, "No jobs were found"
        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_CEILOMETER.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.
                        OPERATOR_CEILOMETER.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    async def test_configures_compute_agent_cds(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        cdss = interfaces.configured_daemon_set_interface(self.api_client)
        cds, = await cdss.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_agent_cds"},
        )

        self.assertCountEqual(
            cds["spec"]["targetNodes"],
            self.compute_nodes,
        )

    async def test_creates_central_deployment_with_replicas(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "central_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 3)

    async def test_creates_notification_deployment_with_replicas(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "notification_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 3)

    async def test_creates_upgrade_job_and_halts(self):
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        upgrade, = all_jobs

        self.assertEqual(upgrade.metadata.labels[context.LABEL_COMPONENT],
                         "upgrade")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        upgrade, = all_jobs

        self.assertEqual(upgrade.metadata.labels[context.LABEL_COMPONENT],
                         "upgrade")

    async def test_creates_containers_with_resources(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        central_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "central_deployment"}
        )
        notification_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "notification_deployment"}
        )

        daemonsets = interfaces.configured_daemon_set_interface(
            self.api_client)
        compute_ds, = await daemonsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_agent_cds"}
        )
        jobs = interfaces.job_interface(self.api_client)
        upgrade_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"},
        )

        self.assertEqual(
            testutils.container_resources(central_deployment, 0),
            testutils.unique_resources("central.ceilometer-agent-central")
        )
        self.assertEqual(
            testutils.container_resources(notification_deployment, 0),
            testutils.unique_resources(
                "notification.ceilometer-agent-notification")
        )
        self.assertEqual(
            testutils.container_resources(compute_ds, 0),
            testutils.unique_resources(
                "ceilometerCompute.ceilometer-compute-agent")
        )
        self.assertEqual(
            testutils.container_resources(upgrade_job, 0),
            testutils.unique_resources("job.ceilometer-upgrade-job")
        )

    async def test_amqp_server_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        amqpserver = interfaces.amqpserver_interface(self.api_client)

        amqp, = await amqpserver.list_(
            NAMESPACE,
        )

        self.assertEqual(
            "issuername",
            amqp["spec"]["frontendIssuerRef"]["name"],
        )
