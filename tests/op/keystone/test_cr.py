#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock
from unittest.mock import Mock
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.common.config as common_config
import yaook.statemachine as sm
import yaook.op.keystone.cr as keystone_cr


class TestEmptySecret(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.es = keystone_cr.EmptySecret(name_prefix="foobar-")

    def test_is_secret(self):
        self.assertIsInstance(self.es, sm.Secret)

    async def test_static_body(self):
        result = await self.es._make_body(
            sentinel.ctx,
            sentinel.dependencies,
        )

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "type": "Opaque",
                "metadata": {
                    "generateName": "foobar-",
                },
                "data": {},
            }
        )

    async def test_never_updates(self):
        self.assertFalse(self.es._needs_update(sentinel.foo,
                                               sentinel.bar))


class TestExternalAdminCredential(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.eac = keystone_cr.ExternalAdminCredential(metadata="foobar")

    def test_is_secret(self):
        self.assertIsInstance(self.eac, sm.Secret)

    @unittest.mock.patch("yaook.statemachine.extract_password")
    async def test_body(self, extract_password):
        ctx = Mock(["parent_spec"])
        ctx.parent_spec = {
            "username": str(sentinel.username),
            "password": {"name": sentinel.passwordsecret},
            "projectName": str(sentinel.projectname),
            "userDomainName": str(sentinel.userdomainname),
            "projectDomainName": str(sentinel.projectdomainname),
            "authType": str(sentinel.authtype),
        }
        extract_password.return_value = str(sentinel.password)

        result = await self.eac._make_body(
            ctx,
            sentinel.dependencies,
        )

        self.assertEqual(
            result,
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "type": "Opaque",
                "metadata": {"name": "foobar"},
                "data": sm.api_utils.encode_secret_data({
                    "OS_PASSWORD": str(sentinel.password),
                    "OS_USERNAME": str(sentinel.username),
                    "OS_PROJECT_NAME": str(sentinel.projectname),
                    "OS_USER_DOMAIN_NAME": str(sentinel.userdomainname),
                    "OS_PROJECT_DOMAIN_NAME": str(sentinel.projectdomainname),
                    "OS_AUTH_TYPE": str(sentinel.authtype),
                })
            }
        )
        extract_password.assert_called_once_with(ctx, sentinel.passwordsecret)


class TestAPIConfigmapTemplate(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.act = keystone_cr.APIConfigmapTemplate(
            interface=sentinel.interface,
            template=sentinel.template,
            component=str(sentinel.component),
        )
        self.patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="train")
            ),
        ]
        self.active_patches = []
        self._patch()

    def tearDown(self):
        for p in self.active_patches:
            p.stop()

    def _patch(self):
        for p in self.patches:
            p.start()
            self.active_patches.append(p)

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test_injects_interface(self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = Mock(["namespace", "parent_spec", "base_label_match",
                    "instance", "instance_data"])
        ctx.base_label_match.return_value = {}

        params = await self.act._get_template_parameters(ctx, {})

        self.assertEqual(params["vars"]["interface"], sentinel.interface)


class TestTemplatedKeystoneConfigMap(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.memcached_sfs = unittest.mock.Mock(sm.KubernetesReference)
        self.kcms = keystone_cr.TemplatedKeystoneConfigMap(
            memcached_sfs=self.memcached_sfs,
            template=sentinel.template,
            component=str(sentinel.component),
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test_injects_memcachedservers(self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        pod_prefix = "mypodprefix"
        namespace = "mynamespace"
        self.memcached_sfs.get.return_value = kclient.V1ObjectReference(
            name=pod_prefix,
            namespace=namespace,
        )

        ctx = Mock(["namespace", "parent_spec", "base_label_match",
                    "instance", "instance_data"])
        ctx.base_label_match.return_value = {}
        ctx.parent_spec = {
            "memcached": {
                "replicas": 2
            }
        }
        ctx.namespace = namespace

        params = await self.kcms._get_template_parameters(ctx, {})

        self.assertEqual(
            params["memcached_servers"],
            "mypodprefix-0.mypodprefix.mynamespace.svc:11211,"
            "mypodprefix-1.mypodprefix.mynamespace.svc:11211"
        )


class TestMemcachedConnectionLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.memcached_sfs = unittest.mock.Mock(
            sm.resources.KubernetesReference
        )
        self.memcached_cl = keystone_cr.MemcachedConnectionLayer(
            target=sentinel.target,
            memcached_sfs=self.memcached_sfs,
        )

    def test_declares_service_as_dependency(self):
        self.assertIn(self.memcached_sfs, self.memcached_cl.get_dependencies())

    async def test_get_layer_returns_memcached_connection_info(self):
        pod_prefix = "mypodprefix"
        namespace = "mynamespace"
        self.memcached_sfs.get.return_value = kclient.V1ObjectReference(
            name=pod_prefix,
            namespace=namespace,
        )

        ctx = unittest.mock.Mock(["namespace", "parent_spec",
                                 "base_label_match", "instance"])
        ctx.base_label_match.return_value = {}
        ctx.parent_spec = {
            "memcached": {
                "replicas": 2
            }
        }
        ctx.namespace = namespace

        memcached_servers = [
            "mypodprefix-0.mypodprefix.mynamespace.svc:11211",
            "mypodprefix-1.mypodprefix.mynamespace.svc:11211"
        ]

        self.assertDictEqual(
            await self.memcached_cl.get_layer(ctx),
            {
                sentinel.target: common_config.OSLO_CONFIG.declare([
                    {
                        "cache": {
                            "memcache_servers": memcached_servers,
                        },
                    },
                ])
            },
        )

        self.memcached_sfs.get.assert_awaited_once_with(ctx)
