#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import os
import pprint
import unittest

import ddt

import kubernetes_asyncio.client as kclient

import yaook.op.common as op_common
import yaook.op.keystone as keystone
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.resources as resources

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "keystone"
CONFIG_FILE_NAME = "keystone.conf"
CONFIG_PATH = "/etc/keystone"
POLICY_FILE_NAME = "policy.yaml"
POLICY_RULE_KEY = "identity:create_endpoint"
POLICY_RULE_VALUE = "role:member"
KEYSTONE_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}


@ddt.ddt()
class TestKeystoneDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MemcachedTestMixin):
    def _get_keystone_deployment_yaml(self):
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneDeployment",
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneConfig": {
                    "DEFAULT": {
                        "_transport_url_password": "rabbit-password",
                    },
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "api": {
                    "ingress": {
                        "fqdn": "keystone-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                    "wsgiProcesses": 15,
                    "resources": testutils.generate_resources_dict(
                        "api.keystone",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                "keyRotationSchedule": "*/2 * * * *",
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(),
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "targetRelease": "train",
                "jobResources": testutils.generate_resources_dict(
                    "job.keystone-bootstrap-job",
                    "job.keystone-credential-setup-job",
                    "job.keystone-db-sync-job",
                    "job.keystone-db-upgrade-post-job",
                    "job.keystone-db-upgrade-pre-job",
                    "job.keystone-fernet-setup-job",
                    "job.keystone-key-rotation-fernet-cronjob",
                    "job.keystone-key-rotation-credential-cronjob",
                ),
            }
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone.KeystoneDeployment,
            self._get_keystone_deployment_yaml(),
        )
        self._make_all_databases_ready_immediately()

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "keystonedeployments",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "keystone-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_creates_db_sync_and_key_jobs_and_halts(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 4)

        self.assertCountEqual(
            [job.metadata.labels[context.LABEL_COMPONENT] for job in all_jobs],
            [
                "policy_validation",
                "db_sync",
                "fernet_setup",
                "credential_setup"
            ],
        )

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 4)

        self.assertCountEqual(
            [job.metadata.labels[context.LABEL_COMPONENT] for job in all_jobs],
            [
                "policy_validation",
                "db_sync",
                "fernet_setup",
                "credential_setup"
            ],
        )

    async def test_creates_bootstrap_job_after_other_jobs(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(NAMESPACE)

        for job in jobs:
            await job_interface.patch(
                NAMESPACE, job.metadata.name,
                [{"op": "add", "path": "/status", "value": {"succeeded": 1}}],
            )

        await self.cr.sm.ensure(self.ctx)

        bootstrap_job, = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bootstrap"},
        )

    async def test_bootstrap_job_sets_region(self):
        self._make_all_dependencies_complete_immediately()
        job_interface = interfaces.job_interface(self.api_client)

        await self.cr.sm.ensure(self.ctx)

        bootstrap_job, = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bootstrap"},
        )
        region, = [
            x for x in
            bootstrap_job.spec.template.spec.containers[0].env
            if x.name == "REGION_ID"]
        self.assertEqual(region.value, "regionname")

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            if job._metadata.generate_name == "keystone-policy-validator-":
                continue
            self.assertEqual(
                job.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
                str(job.metadata),
            )

    @ddt.data({}, KEYSTONE_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        keystone_deployment_yaml = self._get_keystone_deployment_yaml()
        keystone_deployment_yaml["spec"].update(policy)
        self._configure_cr(keystone.KeystoneDeployment,
                           keystone_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        keystone_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            keystone_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, KEYSTONE_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        keystone_deployment_yaml = self._get_keystone_deployment_yaml()
        keystone_deployment_yaml["spec"].update(policy)
        self._configure_cr(keystone.KeystoneDeployment,
                           keystone_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 8)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH + "/keystone.conf",
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    @ddt.data({}, KEYSTONE_POLICY)
    async def test_creates_policy_configmap(self, policy):
        keystone_deployment_yaml = self._get_keystone_deployment_yaml()
        keystone_deployment_yaml["spec"].update(policy)
        self._configure_cr(keystone.KeystoneDeployment,
                           keystone_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        keystone_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(keystone_policy.kind, "ConfigMap")
        self.assertTrue(
            keystone_policy.metadata.name.startswith("keystone-policy"))

        self.assertEqual(keystone_policy.data, {})
        self.assertEqual(
            keystone_policy.metadata.annotations['state.yaook.cloud/policies'],
            str(policy.get("policy", {}))
        )

    async def test_creates_deployment_with_replica_spec_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        deployment, = await deployment_interface.list_(NAMESPACE)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        self.assertEqual(deployment.spec.replicas, 2)
        self.assertEqual(
            deployment.spec.template.spec.volumes[0].projected.sources[0]
                                                    .secret.name,
            config_secret.metadata.name,
        )

    async def test_creates_deployment_with_wsgi_workers(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)

        deployment, = await deployment_interface.list_(NAMESPACE)

        self.assertIn(
            kclient.V1EnvVar("WSGI_PROCESSES", "15"),
            deployment.spec.template.spec.containers[0].env,
        )

    async def test_creates_containers_with_resource(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        cronjobs = interfaces.cronjob_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api"},
        )
        bootstrap_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bootstrap"},
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"},
        )
        fernet_setup_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "fernet_setup"},
        )
        credential_setup_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "credential_setup"},
        )
        key_rotation_cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "key_rotation"},
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.keystone")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(bootstrap_job, 0),
            testutils.unique_resources("job.keystone-bootstrap-job")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.keystone-db-sync-job")
        )
        self.assertEqual(
            testutils.container_resources(fernet_setup_job, 0),
            testutils.unique_resources("job.keystone-fernet-setup-job")
        )
        self.assertEqual(
            testutils.container_resources(credential_setup_job, 0),
            testutils.unique_resources("job.keystone-credential-setup-job")
        )
        self.assertEqual(
            testutils.container_resources(key_rotation_cronjob, 0),
            testutils.unique_resources(
                "job.keystone-key-rotation-fernet-cronjob")
        )
        self.assertEqual(
            testutils.container_resources(key_rotation_cronjob, 1),
            testutils.unique_resources(
                "job.keystone-key-rotation-credential-cronjob")
        )

    async def test_creates_database_and_user(self):
        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "foo-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_database_uri(self):
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        keystone_conf = config.data["keystone.conf"]
        cfg = testutils._parse_config(keystone_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "keystonedeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "keystone",
        )

        keystone_conf = config.data["keystone.conf"]
        cfg = testutils._parse_config(keystone_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_creates_memcached(self):
        await self.cr.sm.ensure(self.ctx)

        memcacheds = interfaces.memcachedservice_interface(self.api_client)

        memcached, = await memcacheds.list_(NAMESPACE)

        self.assertEqual(
            memcached["spec"]["memory"],
            "512"
        )

        self.assertEqual(
            memcached["spec"]["connections"],
            "2000"
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "keystone")

        self.assertIsNotNone(service)

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "keystone")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "keystone-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "keystone")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "keystone")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "keystonedeployments",
            },
        )
        deployment, = await deployments.list_(NAMESPACE)

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key":
                                    op_common.SchedulingKey.IDENTITY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.IDENTITY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_KEYSTONE.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_KEYSTONE.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    async def test_creates_internal_configmap_with_endpoints(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        configmap = await configmaps.read(
            NAMESPACE,
            "os-internal-api",
        )

        services = interfaces.service_interface(self.api_client)
        service = await services.read(
            NAMESPACE,
            "keystone",
        )

        name = service.metadata.name
        port = [port_spec for port_spec in service.spec.ports
                if port_spec.name == "internal"][0].port

        statefulsets = interfaces.stateful_set_interface(self.api_client)
        statefulset, = await statefulsets.list_(
            NAMESPACE
        )

        memcached_servers = "{}-0.{}.{}.svc:11211,{}-1.{}.{}.svc:11211".format(
            statefulset.metadata.name, statefulset.metadata.name, NAMESPACE,
            statefulset.metadata.name, statefulset.metadata.name, NAMESPACE,
            )

        self.assertEqual(configmap.data["OS_INTERFACE"], "internal")
        self.assertEqual(configmap.data["OS_IDENTITY_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_VOLUME_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_AUTH_URL"],
                         "https://{}:{}/v3".format(name, port))
        self.assertEqual(configmap.data["MEMCACHED_SERVERS"],
                         memcached_servers)

    async def test_creates_public_configmap_with_endpoints(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        configmap = await configmaps.read(
            NAMESPACE,
            "os-public-api",
        )

        statefulsets = interfaces.stateful_set_interface(self.api_client)
        statefulset, = await statefulsets.list_(
            NAMESPACE
        )

        memcached_servers = "{}-0.{}.{}.svc:11211,{}-1.{}.{}.svc:11211".format(
            statefulset.metadata.name, statefulset.metadata.name, NAMESPACE,
            statefulset.metadata.name, statefulset.metadata.name, NAMESPACE,
            )

        self.assertEqual(configmap.data["OS_INTERFACE"], "public")
        self.assertEqual(configmap.data["OS_IDENTITY_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_VOLUME_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_AUTH_URL"], "https://{}/v3".format(
            "keystone-ingress:8080",
        ))
        self.assertEqual(configmap.data["MEMCACHED_SERVERS"],
                         memcached_servers)

    async def test_creates_api_deployment_with_correct_ssl_term_container_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api",
            }
        )

        ssl_term_container_spec = \
            api_deployment.spec.template.spec.containers[1]
        self.assertEqual(len(ssl_term_container_spec.env), 4)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "5000")
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9090")

    async def test_create_api_deploy_verify_ext_ssl_term_container_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api",
            }
        )

        external_ssl_term_container_spec = \
            api_deployment.spec.template.spec.containers[2]
        self.assertEqual(len(external_ssl_term_container_spec.env), 4)
        service_port_env_var = external_ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "5001")
        local_port_env_var = external_ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = external_ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9091")

    async def test_api_depends_on_db_syncs_or_upgrade(self):
        self.assertIn(self.cr.db_sync, self.cr.api._dependencies)
        self.assertIn(self.cr.db_upgrade_pre, self.cr.api._dependencies)

    async def test_upgrade_post_depends_on_api(self):
        self.assertIn(self.cr.api, self.cr.db_upgrade_post._dependencies)

    @unittest.mock.patch("yaook.statemachine.version_utils.is_upgrading")
    async def test_upgrade_does_not_run_rollout_jobs(self, is_upgrading):
        self.assertIsInstance(self.cr.db_sync, resources.Optional)
        for b in [True, False]:
            is_upgrading.return_value = b
            self.assertEquals(self.cr.db_sync.condition(self.ctx), not b)

    async def test_bootstrap_job_depends_on_db(self):
        self.assertIn(self.cr.db_sync, self.cr.bootstrap._dependencies)
        self.assertIn(self.cr.db_upgrade_pre, self.cr.bootstrap._dependencies)

    async def test_not_creates_db_upgrade_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_pre"},
        )

        self.assertEqual(0, len(jobs))

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_post"},
        )

        self.assertEqual(0, len(jobs))

    async def test_creates_key_rotation_cronjob(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "key_rotation",
            }
        )

        self.assertEqual(cronjob.spec.schedule, "*/2 * * * *")

    async def test_key_rotation_is_not_concurrent(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "key_rotation",
            }
        )

        self.assertEqual(cronjob.spec.concurrency_policy, "Forbid")


class TestKeystoneDeploymentsUpgrade(
        testutils.ReleaseAwareCustomResourceTestCase):
    def _get_keystone_deployment_yaml(self):
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneDeployment",
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneConfig": {
                    "DEFAULT": {
                        "_transport_url_password": "rabbit-password",
                    },
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "api": {
                    "ingress": {
                        "fqdn": "keystone-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                },
                "keyRotationSchedule": "*/2 * * * *",
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "1234",
                },
                "targetRelease": "train",
            }
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone.KeystoneDeployment,
            self._get_keystone_deployment_yaml(),
        )
        self._make_all_databases_ready_immediately()
        hooks = copy.deepcopy(self.client_mock._hooks)
        self._make_all_dependencies_complete_immediately()
        await self.cr.reconcile(self.ctx, 0)
        self.client_mock._hooks = hooks

        kst = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME,
        )
        kst["spec"]["targetRelease"] = "ussuri"
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME, kst,
        )
        self.ctx = self._make_context()  # needed to set the updated spec

    async def test_imagepullsecret_in_podspec_for_jobs(self):
        # ?
        # We need to remove the jobs from the initial queens run
        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)
        for job in jobs:
            await job_interface.delete(job.metadata.namespace,
                                       job.metadata.name)
        return await super().test_imagepullsecret_in_podspec_for_jobs()

    async def test_sets_deployedopenstackversion_on_success(self):
        raise unittest.SkipTest("not relevant")

    async def test_does_upgrade(self):
        self._make_all_dependencies_complete_immediately()
        keystones = interfaces.keystonedeployment_interface(self.api_client)
        keystone = await keystones.read(NAMESPACE, NAME)
        self.assertEqual("train", keystone["status"]["installedRelease"])

        await self.cr.reconcile(self.ctx, 0)
        keystone = await keystones.read(NAMESPACE, NAME)
        self.assertEqual("ussuri", keystone["status"]["installedRelease"])

    async def test_creates_db_upgrade_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_pre"},
        )

        self.assertEqual(1, len(jobs))

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_post"},
        )

        self.assertEqual(1, len(jobs))

    async def test_not_creates_db_sync_job(self):
        self._make_all_dependencies_complete_immediately()

        # We need to remove the job from the initial run
        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"},
        )
        await job_interface.delete(jobs[0].metadata.namespace,
                                   jobs[0].metadata.name)

        await self.cr.sm.ensure(self.ctx)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"},
        )

        self.assertEqual(0, len(jobs))


class TestKeystoneDeploymentsWithSecrets(
        testutils.ReleaseAwareCustomResourceTestCase):

    CONFIG_SECRET_NAME = "config-secret"
    CONFIG_SECRET_KEY = "mysecretkey"
    CONFIG_SECRET_VALUE = "mysecretvalue"

    def _get_keystone_deployment_yaml(self):
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneDeployment",
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneConfig": {
                    "DEFAULT": {
                        "_transport_url_password": "rabbit-password",
                    },
                },
                "keystoneSecrets": [
                    {
                        "secretName": self.CONFIG_SECRET_NAME,
                        "items": [{
                            "key": self.CONFIG_SECRET_KEY,
                            "path": "/DEFAULT/admin_token",
                        }],
                    },
                ],
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername",
                },
                "api": {
                    "ingress": {
                        "fqdn": "keystone-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                },
                "keyRotationSchedule": "*/2 * * * *",
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                },
                "targetRelease": "train",
            }
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone.KeystoneDeployment,
            self._get_keystone_deployment_yaml(),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, self.CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": self.CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        self.CONFIG_SECRET_KEY: self.CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config"},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)
        lines = decoded["keystone.conf"].splitlines()

        self.assertIn(f"admin_token = {self.CONFIG_SECRET_VALUE}", lines)


class TestExternalKeystoneDeployments(testutils.CustomResourceTestCase):
    PASSWORD_SECRET_NAME = "testadmin123-password-secret"

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            keystone.ExternalKeystoneDeployment,
            {
                "apiVersion": "yaook.cloud/v1",
                "kind": "ExternalKeystoneDeployment",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "username": "testadmin123",
                    "password": {
                        "name": self.PASSWORD_SECRET_NAME
                    },
                    "authURL": "https://external.keystone.url:5000/v3/",
                    "memcachedServers": [
                        "external-memcached-0.somedomain:11211",
                        "external-memcached-1.somedomain:11211"
                    ],
                    "projectName": "admin",
                    "userDomainName": "default",
                    "projectDomainName": "default",
                    "authType": "password",
                    "caCertificates": [testutils.DEMO_CA_CERTIFICATE],
                },
            },
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, self.PASSWORD_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": self.PASSWORD_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        "password": "testpassword123",
                    }),
                },
            )

    async def test_creates_admin_credentials(self):
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_ADMIN_CREDENTIALS_COMPONENT},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)

        self.assertEqual(decoded["OS_PASSWORD"], "testpassword123")
        self.assertEqual(decoded["OS_USERNAME"], "testadmin123")
        self.assertEqual(decoded["OS_PROJECT_NAME"], "admin")
        self.assertEqual(decoded["OS_USER_DOMAIN_NAME"], "default")
        self.assertEqual(decoded["OS_PROJECT_DOMAIN_NAME"], "default")
        self.assertEqual(decoded["OS_AUTH_TYPE"], "password")

    async def test_creates_internal_configmap_with_endpoints(self):
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        configmap, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_INTERNAL_API_COMPONENT},
        )

        self.assertEqual(configmap.data["OS_INTERFACE"], "public")
        self.assertEqual(configmap.data["OS_IDENTITY_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_VOLUME_API_VERSION"], "3")
        self.assertEqual(
            configmap.data["OS_AUTH_URL"],
            "https://external.keystone.url:5000/v3/"
        )
        self.assertEqual(
            configmap.data["MEMCACHED_SERVERS"],
            "external-memcached-0.somedomain:11211,"
            "external-memcached-1.somedomain:11211"
        )

    async def test_creates_public_configmap_with_endpoints(self):
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        configmap, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_PUBLIC_API_COMPONENT},
        )

        self.assertEqual(configmap.data["OS_INTERFACE"], "public")
        self.assertEqual(configmap.data["OS_IDENTITY_API_VERSION"], "3")
        self.assertEqual(configmap.data["OS_VOLUME_API_VERSION"], "3")
        self.assertEqual(
            configmap.data["OS_AUTH_URL"],
            "https://external.keystone.url:5000/v3/"
        )
        self.assertEqual(
            configmap.data["MEMCACHED_SERVERS"],
            "external-memcached-0.somedomain:11211,"
            "external-memcached-1.somedomain:11211"
        )
