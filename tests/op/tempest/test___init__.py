#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import yaook.common.config
import yaook.op.tempest as tempest
import yaook.statemachine as sm


class TestRegionLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.rl = tempest.RegionLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.rl, sm.CueLayer)

    async def test_injects_region(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "region": sentinel.region
        }

        result = await self.rl.get_layer(ctx)

        self.assertEqual(
            result,
            {"tempest":
                yaook.common.config.OSLO_CONFIG.declare([{
                    "identity": {
                        "region":
                            sentinel.region,
                    },
                }])},
        )


@ddt.ddt()
class TestTempestJob(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.tj = tempest.TempestJob(
            template="a",
            scheduling_keys=[],
        )

    @unittest.mock.patch(
        "yaook.statemachine.resources.BodyTemplateMixin."
        "_get_template_parameters"
    )
    async def test__get_template_parameters_inject_regex_for_service(
            self, _get_template_parameters):
        _get_template_parameters.return_value = {}
        ctx = unittest.mock.Mock([
            "parent_spec", "namespace"])
        ctx.parent_spec = {
            "target": {"service": "cinder"},
        }

        result = await self.tj._get_template_parameters(ctx, {})

        self.assertEqual(
            "tempest.api.volume",
            result["tempest_regex"]
        )

    @unittest.mock.patch(
        "yaook.statemachine.resources.BodyTemplateMixin."
        "_get_template_parameters"
    )
    async def test__get_template_parameters_inject_regex_directly(
            self, _get_template_parameters):
        _get_template_parameters.return_value = {}
        ctx = unittest.mock.Mock([
            "parent_spec", "namespace"])
        ctx.parent_spec = {
            "target": {"regex": "myregex"},
        }

        result = await self.tj._get_template_parameters(ctx, {})

        self.assertEqual(
            "myregex",
            result["tempest_regex"]
        )
