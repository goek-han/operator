#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
import yaook.op.infra_ironic as infra_ironic
import yaook.op.common as op_common

from ... import testutils

NAMESPACE = "test-namespace"
NAME = "some-infra-ironic"


class TestInfrastructureIronicDeployments(
        testutils.ReleaseAwareCustomResourceTestCase):
    def _get_infrastructure_ironic_deployment_yaml(self):
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "InfrastructureIronicDeployment",
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "ingressAddress": "10.2.3.4",
                "imageServer": {
                    "storageSize": "8Gi",
                    "storageClassName": "foo-storage-class",
                    "pvcAccessMode": "ReadWriteOnce",
                    "ingress": {
                        "fqdn": "ironic-image-server.domain.example",
                        "port": 2342,
                        "ingressClassName": "foo-ingress-class",
                    },
                    "resources": testutils.generate_resources_dict(
                        "imageServer.bootstrap-cfg",
                        "imageServer.httpd",
                    ),
                },
                "api": {
                    "replicas": 1,
                    "ingress": {
                        "fqdn": "ironic-api.domain.example",
                        "port": 2342,
                        "ingressClassName": "foo-ingress-class",
                    },
                    "resources": testutils.generate_resources_dict(
                        "api.ironic-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                "inspectorApi": {
                    "replicas": 1,
                    "ingress": {
                        "fqdn": "inspector-api.domain.example",
                        "port": 2342,
                        "ingressClassName": "foo-ingress-class",
                    },
                    "resources": testutils.generate_resources_dict(
                        "inspectorApi.ironic-inspector-api",
                        "inspectorApi.ssl-terminator",
                        "inspectorApi.ssl-terminator-external",
                        "inspectorApi.service-reload",
                        "inspectorApi.service-reload-external",
                    ),
                },
                "issuerRef": {
                    "name": "ca-issuer",
                },
                "database": {
                    "ironic": {
                        "replicas": 2,
                        "proxy": {
                            "replicas": 1,
                            "resources": testutils.generate_db_proxy_resources(
                                "database.ironic.proxy"),
                        },
                        "storageSize": "8Gi",
                        "storageClassName": "db1-storage-class",
                        "backup": {},
                        "resources": testutils.generate_db_resources(
                            "database.ironic"),
                    },
                    "inspector": {
                        "replicas": 2,
                        "proxy": {
                            "replicas": 1,
                            "resources": testutils.generate_db_proxy_resources(
                                "database.inspector.proxy"),
                        },
                        "storageSize": "8Gi",
                        "storageClassName": "db2-storage-class",
                        "backup": {},
                        "resources": testutils.generate_db_resources(
                            "database.inspector"),
                    },
                },
                "pxe": {
                    "listenNetwork": "10.2.3.0/24",
                    "dhcp": [{
                        "dhcpRange": "10.2.3.100,10.2.3.254"
                        },
                        {
                        "dhcpRange": "192.168.255.10,"
                                     "192.168.255.20,255.255.255.0",
                        "defaultGateway": "192.168.255.1"
                        }]
                },
                "dnsmasq": {
                    "storageClassName": "dns-storage-class",
                    "storageSize": "100Mi",
                    "resources": testutils.generate_resources_dict(
                        "dnsmasq.dnsmasq",
                    ),
                },
                "conductor": {
                    "resources": testutils.generate_resources_dict(
                        "conductor.conductor",
                    ),
                },
                "ipa": {
                    "pxeAppendKernelParams": "ipa-collect-lldp=1 kernel-param=26"  # noqa:E501
                },
                "targetRelease": "train",
                "keystoneRef": {
                    "kind": "KeystoneDeployment",
                    "name": self._keystone_name,
                },
                "region": {
                    "name": "SomeRegion",
                    "parent": "SomeParentRegion",
                },
                "jobResources": testutils.generate_resources_dict(
                    "job.infra-ironic-db-sync-job",
                    "job.infra-ironic-inspector-db-sync-job",
                ),
                "serviceMonitor": {
                    "additionalLabels": {
                        "mykey": "mylabel",
                    },
                },
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            infra_ironic.cr.InfrastructureIronicDeployment,
            self._get_infrastructure_ironic_deployment_yaml(),
        )
        self._make_all_databases_ready_immediately()

    async def test_creates_data_volume(self):
        await self.cr.sm.ensure(self.ctx)

        pvcs = interfaces.persistent_volume_claim_interface(self.api_client)

        pvc, = await pvcs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "data_volume",
            },
        )

        self.assertEqual(
            pvc.spec.resources.requests["storage"],
            "8Gi",
        )
        self.assertEqual(
            pvc.spec.storage_class_name,
            "foo-storage-class",
        )

    async def test_creates_main_database_and_user(self):
        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db",
            },
        )
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "db1-storage-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_inspector_database_and_user(self):
        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db",
            },
        )
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "db2-storage-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_config_with_database_uri_for_ironic(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user_password",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
                context.LABEL_PARENT_NAME: db["metadata"]["name"],
            },
        )
        db_name = db["spec"]["database"]
        ironic_conf = config.data["ironic.conf"]
        cfg = testutils._parse_config(ironic_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_creates_config_with_database_uri_for_inspector(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db_api_user_password",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_db",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
                context.LABEL_PARENT_NAME: db["metadata"]["name"],
            },
        )
        db_name = db["spec"]["database"]
        ironic_conf = config.data["ironic_inspector.conf"]
        cfg = testutils._parse_config(ironic_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_api_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_ca_certs",
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "ironic-api",
        )

        ironic_conf = config.data["ironic.conf"]
        cfg = testutils._parse_config(ironic_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_inspector_api_database_uri_refers_to_mounted_ca_bundle(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "inspector_config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_ca_certs",
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "inspector_api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "ironic-inspector-api",
        )

        ironic_conf = config.data["ironic_inspector.conf"]
        cfg = testutils._parse_config(ironic_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_configuration_points_at_data_volume(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)
        pvcs = interfaces.persistent_volume_claim_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "config",
            },
        )

        pvc, = await pvcs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "data_volume",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        data_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_pvc_volume(
                api.spec.template.spec,
                pvc.metadata.name,
            ),
            "ironic-api",
        )

        ironic_conf = config.data["ironic.conf"]
        cfg = testutils._parse_config(ironic_conf, decode=True)

        self.assertEqual(
            data_mountpoint,
            cfg.get("deploy", "http_root"),
        )
        self.assertEqual(
            f"{data_mountpoint}/master_images",
            cfg.get("pxe", "tftp_master_path"),
        )

    async def test_dnsmasq_offers_records_for_ingress_endpoints(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)

        config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "dnsmasq_config",
            },
        )

        self.assertIn(
            "\nhost-record=ironic-image-server.domain.example,10.2.3.4\n",
            config.data["dnsmasq.conf"],
        )
        self.assertIn(
            "\nhost-record=ironic-api.domain.example,10.2.3.4\n",
            config.data["dnsmasq.conf"],
        )
        self.assertIn(
            "\nhost-record=inspector-api.domain.example,10.2.3.4\n",
            config.data["dnsmasq.conf"],
        )

    async def test_dnsmasq_points_ipxe_at_image_server(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)

        config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "dnsmasq_config",
            },
        )

        self.assertIn(
            "\ndhcp-boot=tag:ipxe,"
            "http://ironic-image-server.domain.example:2342/boot.ipxe\n",
            config.data["dnsmasq.conf"],
        )

    async def test_dnsmasq_has_listen_address_and_dhcp_range_set(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)

        config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "infrastructureironicdeployments",
                context.LABEL_COMPONENT: "dnsmasq_config",
            },
        )
        self.assertIn(
            "\ndhcp-range=set:10_2_3_100,10.2.3.100,10.2.3.254\n",
            config.data["dnsmasq.conf"],
        )
        self.assertIn(
            "\ndhcp-range=set:192_168_255_10,"
            "192.168.255.10,192.168.255.20,255.255.255.0\n",
            config.data["dnsmasq.conf"],
        )
        self.assertIn(
            "\ndhcp-option=tag:192_168_255_10,3,192.168.255.1\n",
            config.data["dnsmasq.conf"],
        )

    async def test_keystone_users_match_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        for user in await users.list_(
                    NAMESPACE,
                    label_selector={
                        context.LABEL_PARENT_PLURAL:
                            "infrastructureironicdeployments",
                    },
                ):
            self.assertEqual(user["spec"]["keystoneRef"]["name"],
                             self._keystone_name)
            self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                             "KeystoneDeployment")

    async def test_keystone_endpoints_match_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        for endpoint in await endpoints.list_(
                    NAMESPACE,
                    label_selector={
                        context.LABEL_PARENT_PLURAL:
                            "infrastructureironicdeployments",
                    },
                ):
            self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                             self._keystone_name)
            self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                             "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = self._get_infrastructure_ironic_deployment_yaml()
        self._configure_cr(infra_ironic.cr.InfrastructureIronicDeployment,
                           deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = self._get_infrastructure_ironic_deployment_yaml()
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(infra_ironic.cr.InfrastructureIronicDeployment,
                           deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_inspector_keystone_endpoint_is_created(self):
        deployment_yaml = self._get_infrastructure_ironic_deployment_yaml()
        self._configure_cr(infra_ironic.cr.InfrastructureIronicDeployment,
                           deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "inspector_api_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_inspector_keystone_endpoint_is_not_created(self):
        deployment_yaml = self._get_infrastructure_ironic_deployment_yaml()
        deployment_yaml["spec"]["inspectorApi"]["publishEndpoint"] = False
        self._configure_cr(infra_ironic.cr.InfrastructureIronicDeployment,
                           deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "inspector_api_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoints_match_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        for endpoint in await endpoints.list_(
                    NAMESPACE,
                    label_selector={
                        context.LABEL_PARENT_PLURAL:
                            "infrastructureironicdeployments",
                    },
                ):
            self.assertEqual(endpoint["spec"]["region"]["name"],
                             "SomeRegion")
            self.assertEqual(endpoint["spec"]["region"]["parent"],
                             "SomeParentRegion")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["ironic.conf"], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_forces_use_of_public_endpoint_for_service_catalog_queries(self):  # noqa:E501
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        cfg = testutils._parse_config(config.data["ironic.conf"], decode=True)

        self.assertEqual(cfg.get("service_catalog", "valid_interfaces"),
                         "public")
        self.assertTrue(
            # check that we're not cheating by using the public keystone
            # endpoint
            cfg.get("service_catalog", "auth_url").endswith(":5000/v3"),
        )

    async def test_created_inspector_config_matches_keystone_user(self):
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "inspector_config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    "inspector_api_keystone_user",
            },
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(
            config.data["ironic_inspector.conf"],
            decode=True,
        )

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_imagesrv_ingress_no_ssl_backend(self):
        await self.cr.sm.ensure(self.ctx)

        ingress = interfaces.ingress_interface(self.api_client)

        imgsrv_ingress, = await ingress.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "image_server_ingress",
            }
        )

        self.assertNotEqual(imgsrv_ingress.metadata.annotations.get(
                            'nginx.ingress.kubernetes.io/backend-protocol'),
                            'HTTPS')

    async def test_bootscripts_contain_appended_kernel_params(self):
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)

        bootscripts_configmap, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "bootscripts",
            }
        )

        self.assertIn("ipa-collect-lldp=1 kernel-param=26",
                      bootscripts_configmap.data['default.ipxe'])

    async def test_creates_containers_with_resources(self):

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        api_deploy, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        inspector_deploy, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "inspector_api_deployment"}
        )
        image_server, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "image_server"}
        )
        conductor_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "conductor_sts"}
        )
        dnsmasq_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "dnsmasq_sts"}
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )
        inspector_db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "inspector_db_sync"}
        )

        self.assertEqual(
            testutils.container_resources(api_deploy, 0),
            testutils.unique_resources("api.ironic-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deploy, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deploy, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deploy, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deploy, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(inspector_deploy, 0),
            testutils.unique_resources("inspectorApi.ironic-inspector-api")
        )
        self.assertEqual(
            testutils.container_resources(inspector_deploy, 1),
            testutils.unique_resources("inspectorApi.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(inspector_deploy, 2),
            testutils.unique_resources("inspectorApi.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(inspector_deploy, 3),
            testutils.unique_resources("inspectorApi.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(inspector_deploy, 4),
            testutils.unique_resources("inspectorApi.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(image_server, 0, is_init=True),
            testutils.unique_resources("imageServer.bootstrap-cfg")
        )
        self.assertEqual(
            testutils.container_resources(image_server, 0),
            testutils.unique_resources("imageServer.httpd")
        )
        self.assertEqual(
            testutils.container_resources(dnsmasq_sts, 0),
            testutils.unique_resources("dnsmasq.dnsmasq")
        )
        self.assertEqual(
            testutils.container_resources(conductor_sts, 0),
            testutils.unique_resources("conductor.conductor")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.infra-ironic-db-sync-job")
        )
        self.assertEqual(
            testutils.container_resources(inspector_db_sync_job, 0),
            testutils.unique_resources(
                "job.infra-ironic-inspector-db-sync-job")
        )
