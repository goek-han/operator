#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest.mock
import uuid
import os

import yaook.op.common as op_common
import yaook.op.neutron_l3 as neutron_l3
import yaook.statemachine as sm
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "node2"
CONFIG_FILE_NAME = "neutron.conf"
CONFIG_PATH = "/etc/neutron"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "context_is_admin"
POLICY_RULE_VALUE = "role:admin"
NEUTRON_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}


class TestNeutronL3AgentDeployments(testutils.CustomL2UseResourceTestCase):
    # Needs to be disabled as the neutron-l3-operator relies on SSA to
    # make patches to its own resource. Since our testutils do not implement
    # that this test can not pass.
    run_needs_update_test = False
    run_topology_spread_test = False
    # Needs to be disabled as the ApiState modifies the context
    run_context_modification_test = False

    async def _prepare_message_queue(self):
        self._make_all_mqs_succeed_immediately()

        amqpservers = sm.amqpserver_interface(self.api_client)
        await amqpservers.create(
            NAMESPACE,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "AMQPServer",
                "metadata": {
                    "name": "amqp-server",
                },
                "spec": {
                    "replicas": 1,
                },
                "status": {"phase": context.Phase.UPDATED.value}
            },
            "myfieldmanager"
        )

    def _get_neutron_l3_agent_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "novaRef": {
                    "name": "nova",
                },
                "neutronConfig": [],
                "neutronL3AgentConfig": [],
                "neutronMetadataAgentConfig": [],
                "startupLimitMinutes": 1,
                "caConfigMapName": self.ca_config_map_name,
                "messageQueue": {
                    "amqpServerRef": {
                        "name": "amqp-server",
                    },
                },
                "state": "Enabled",
                "region": {
                    "name": "RegionOne",
                },
                "resources": testutils.generate_resources_dict(
                    "neutron-l3-agent",
                    "neutron-metadata-agent",
                    "l3-evict-job",
                ),
            },
            "status": {
                "state": "Creating",
            },
        }

    def setUp(self):
        super().setUp()
        self._env_backup = os.environ.pop(
            "YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE", None
        )
        os.environ["YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE"] = "dummy-image"
        # scheduling_key is needed, so testutils.CustomL2UseResourceTestCase.
        # test_delete_removes_annotation knows for wich key to filter the
        # nodes.
        self.scheduling_key = \
            op_common.SchedulingKey.NETWORK_NEUTRON_L3_AGENT.value

        self.l3_status_mock = unittest.mock.Mock([])
        self.l3_status_mock.return_value = resource.ResourceStatus(
            up=True,
            enabled=True,
            disable_reason=None,
        )
        self.__patches = [
            unittest.mock.patch(
                "yaook.op.neutron_l3.resources.L3StateResource"
                "._get_status",
                new=self.l3_status_mock,
            ),
            unittest.mock.patch(
                "yaook.op.neutron_l3.resources.L3StateResource"
                "._update_status",
                new=self.l3_status_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.base.write_ca_certificates"
            ),
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        self.__l2lock_patches = [
            unittest.mock.patch(
                "yaook.statemachine.resources.orchestration.L2Lock.reconcile",
            ),
        ]

        for p in self.__patches:
            p.start()
        self.start_l2lock_patches()

    def start_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.start()

    def stop_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.stop()

    def tearDown(self):
        self.stop_l2lock_patches()
        for p in self.__patches:
            p.stop()
        if self._env_backup is not None:
            os.environ["YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE"] = \
                self._env_backup
        else:
            del os.environ["YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE"]
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_l3 = {
            self.scheduling_key: str(uuid.uuid4()),
        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_l3,
        }
        self.l3_nodes = ["node2"]
        self._keystone_name = self._provide_keystone(NAMESPACE)

        self.ca_config_map_name = str(uuid.uuid4())
        self._configure_cr(
            neutron_l3.NeutronL3Agent,
            self._get_neutron_l3_agent_deployment_yaml(),
        )

        self.metadata_pw = f"foobar2342-{uuid.uuid4()}"

        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, "magic-metadata-secret",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "magic-metadata-secret",
                    "labels": {
                        context.LABEL_COMPONENT:
                            op_common.NOVA_METADATA_SECRET_COMPONENT,
                        context.LABEL_PARENT_PLURAL: "novadeployments",
                        context.LABEL_PARENT_GROUP: "yaook.cloud",
                        context.LABEL_PARENT_NAME: "nova",
                    },
                },
                "data": sm.api_utils.encode_secret_data({
                    "password": self.metadata_pw,
                }),
            },
        )

        await self._prepare_message_queue()

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_l3_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_l3_user_passwords",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 1)

    async def test_creates_config_with_transport_url(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "l3_config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_l3_user_passwords",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        agent_conf = config.data["neutron_l3_agent.conf"]
        cfg = testutils._parse_config(agent_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://l3-{NAME}:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_l3_sfs_requires_l2(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        sfsi = interfaces.stateful_set_interface(self.api_client)

        l3_sfs, = await sfsi.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_agent"},
        )

        self.assertEqual(
            l3_sfs.spec.template.spec.affinity.pod_affinity.
            required_during_scheduling_ignored_during_execution[0].
            label_selector.match_labels[op_common.LABEL_NETWORK_L2_PROVIDER],
            "true",
        )

    async def test_l3_stateful_set_pinned_to_node(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulset = sm.stateful_set_interface(self.api_client)

        l3_sfs, = await statefulset.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_agent"},
        )

        self.assertEqual(
            l3_sfs.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.
            node_selector_terms[0].match_fields[0].to_dict(),
            {
                "key": "metadata.name",
                "operator": "In",
                "values": [NAME],
            },
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_service"}
        )

        self.assertIsNotNone(service)

    async def test_create_l3_sfs(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "l3_agent",
            },
        )

        self.assertEqual(
            sfs.spec.selector.match_labels["state.yaook.cloud/parent-name"],
            "node2"
        )

    async def test_configures_l3_sfs(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "l3_config",
            },
        )

        agent_cfg = testutils._parse_config(
            cfg.data["neutron_l3_agent.conf"],
            decode=True,
        )

        self.assertTrue(agent_cfg.getboolean("DEFAULT", "use_stderr"))

        self.assertTrue(
            agent_cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            agent_cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/pki/tls/certs/ca-bundle.crt",
        )

        self.assertEqual(agent_cfg.get("DEFAULT", "interface_driver"),
                         "openvswitch")

    async def test_configures_metadata_secret(self):
        self._make_all_mq_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "l3_config",
            },
        )
        agent_cfg = testutils._parse_config(
            cfg.data["neutron_metadata_agent.conf"],
            decode=True,
        )
        agent_secret = agent_cfg.get("DEFAULT", "metadata_proxy_shared_secret")

        self.assertEqual(agent_secret, self.metadata_pw)

    async def test_job_role_binding(self):
        await self.cr.sm.ensure(self.ctx)

        service_interface = interfaces.service_interface(self.api_client)
        role_inteface = interfaces.role_interface(self.api_client)
        rb_interface = interfaces.role_binding_interface(self.api_client)

        service, = await service_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "l3_service",
            },
        )

        role, = await role_inteface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "job_role",
            },
        )

        rolebinding, = await rb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "job_role_binding",
            },
        )

        self.assertIsNotNone(rolebinding)
        self.assertEqual(rolebinding.subjects[0].name,
                         service.metadata.name+"-job")
        self.assertEqual(rolebinding.role_ref.name, role.metadata.name)

    async def test_update(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, {})
        agent_interface = \
            interfaces.neutron_l3_agent_interface(self.api_client)
        agent = await agent_interface.list_(
            NAMESPACE,
        )
        await self.cr.sm.ensure(self.ctx)
        agent, = await agent_interface.list_(
            NAMESPACE,
        )
        self.l3_status_mock.assert_called_with(
            self.ctx,
            await self.cr.api_state.get_openstack_connection_info(self.ctx),
            True
        )
        self.assertEqual(agent['status']['state'], 'Enabled')

    async def test_apistate_has_no_l3_dependency(self):
        await self.cr.sm.ensure(self.ctx)
        for deps in self.cr.api_state._dependencies:
            if isinstance(
                    deps,
                    resource.TemplatedRecreatingStatefulSet):
                self.fail(
                    "APIState depends on a TemplatedRecreatingStatefulSet. It"
                    " should have no dependencies to it.")

    async def test_creates_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulsets = interfaces.stateful_set_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        agent_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_agent"}
        )
        evict_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_state"}
        )

        self.assertEqual(
            testutils.container_resources(agent_sts, 0),
            testutils.unique_resources("neutron-l3-agent")
        )
        self.assertEqual(
            testutils.container_resources(agent_sts, 1),
            testutils.unique_resources("neutron-metadata-agent")
        )
        self.assertEqual(
            testutils.container_resources(evict_job, 0),
            testutils.unique_resources("l3-evict-job")
        )

    async def test_evict_not_running_as_root(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        evict_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_state"},
        )
        security_context = evict_job._spec._template._spec._containers[0] \
            .security_context

        self.assertEqual(
            security_context.run_as_group, 2020
        )
        self.assertEqual(
            security_context.run_as_user, 2020
        )
