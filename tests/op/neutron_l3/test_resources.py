#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel
from datetime import datetime

import kubernetes_asyncio.client

import yaook.common.config
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.op.neutron_l3.resources as resources
import yaook.op.neutron_l3.cr as cr


# BIG TODO: generalize this whole test class
# we basically copy pasted and ctrl+f and replaced stuff
class TestSpecSequenceLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ssl = cr.L3SpecSequenceLayer()

    async def test_get_layer_extracts_configs_from_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "neutronConfig": [sentinel.cfg1, sentinel.cfg2],
            "neutronL3AgentConfig": [sentinel.cfg2, sentinel.cfg3],
            "neutronMetadataAgentConfig": [sentinel.cfg4, sentinel.cfg5],
        }
        result = await self.ssl.get_layer(ctx)
        self.assertEqual(
            result,
            {
                "neutron_l3_agent": yaook.common.config.OSLO_CONFIG.declare(
                    [sentinel.cfg1, sentinel.cfg2, sentinel.cfg2,
                     sentinel.cfg3],
                ),
                "neutron_metadata_agent":
                yaook.common.config.OSLO_CONFIG.declare(
                    [sentinel.cfg4, sentinel.cfg5],
                ),
            }
        )


class TestL3StateResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.csr = resources.L3StateResource(
            scheduling_keys=["foo", "bar"],
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            job_endpoint_config=sentinel.job_endpoint_config,
            finalizer="test.yaook.cloud",
            eviction_job_template="some-job-template.yaml",
        )

        self.node_name = str(uuid.uuid4())

        self.core_v1 = unittest.mock.Mock([])
        self.core_v1.read_node = unittest.mock.AsyncMock()
        self.core_v1.read_node.return_value = kubernetes_asyncio.client.V1Node(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                name=self.node_name,
            ),
            spec=kubernetes_asyncio.client.V1NodeSpec(
                taints=[
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.foo,
                        effect="NoSchedule",
                    ),
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.bar,
                        effect="NoExecute",
                    )
                ]
            ),
            status=kubernetes_asyncio.client.V1NodeStatus(),
        )
        self.CoreV1Api = unittest.mock.Mock([])
        self.CoreV1Api.return_value = self.core_v1

        self.background_job = unittest.mock.Mock([])
        self.background_job.is_ready = unittest.mock.AsyncMock()
        self.background_job.is_ready.return_value = True

        self.__state_patches = [
            unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
                new=self.CoreV1Api,
            ),
            unittest.mock.patch.object(
                self.csr, "_background_job",
                new=self.background_job,
            ),
        ]
        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_state_patches(self):
        for p in self.__state_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        ctx.creation_timestamp = datetime.strptime(
            ctx.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")
        return ctx

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_non_if_not_exists(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["serivces"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agents.return_value = []

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-l3-agent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_correct_agent(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agent1 = unittest.mock.Mock()
        agent1.binary = "neutron-l3-agent"
        agent1.host = self.node_name
        agent1.last_heartbeat_at = "2022-01-03 11:22:33"
        agent1.is_alive = True
        agent1.is_admin_state_up = True
        agent2 = unittest.mock.Mock()
        agent2.binary = "neutron-l3-agent"
        agent2.host = "someothernode"
        agent2.last_heartbeat_at = "2022-01-03 11:22:33"
        agent2.is_alive = False
        agent2.is_admin_state_up = False
        agents.return_value = [agent1, agent2]

        self.assertEqual(
            resource.ResourceStatus(
                True, True, None),
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-l3-agent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_ignores_agent_if_too_old(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agent1 = unittest.mock.Mock()
        agent1.binary = "neutron-l3-agent"
        agent1.host = self.node_name
        agent1.last_heartbeat_at = "2021-01-01 00:00:00"
        agent1.is_alive = True
        agent1.is_admin_state_up = True
        agents.return_value = [agent1]

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-l3-agent",
            host=self.node_name)
