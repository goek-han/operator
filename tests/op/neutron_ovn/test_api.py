#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import unittest.mock
import uuid

import kubernetes_asyncio.client as kclient

import yaook.op.common as op_common
import yaook.op.neutron_ovn as neutron_ovn
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.resources.openstack as resource

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "node2"


class TestNeutronOVNAgentDeployments(testutils.CustomResourceTestCase):
    # Needs to be disabled as the neutron-ovn-operator relies on SSA to
    # make patches to its own resource. Since our testutils do not implement
    # that this test can not pass.
    run_needs_update_test = False
    run_topology_spread_test = False

    def _get_neutron_ovn_gateway_agent_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "novaRef": {
                    "name": "nova",
                },
                "neutronOvnMetadataAgentConfig": [],
                "bridgeConfig": [
                    {
                        "bridgeName": "br-ex",
                        "uplinkDevice": "eth1",
                        "openstackPhysicalNetwork": "physnet",
                    },
                ],
                "deployedOnComputeNode": False,
                "southboundServers": ["ssl:1.1.1.1:6642",
                                      "ssl:1.1.1.2:6642"],
                "issuerRef": "ca-issuer",
                "caConfigMapName": self.ca_config_map_name,
                "region": {
                    "name": "RegionOne",
                },
                "resources": testutils.generate_resources_dict(
                    "ovn-controller",
                    "ovn-controller-setup",
                    "ovs-vswitchd",
                    "ovsdb-server",
                ),
            },
            "status": {
                "state": "Creating",
            },
        }

    def setUp(self):
        super().setUp()

        self.fetch_status_mock = unittest.mock.Mock([])
        self.fetch_status_mock.return_value = resource.ResourceStatus(
            up=True,
            enabled=True,
            disable_reason=None,
        )
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.resources.base.write_ca_certificates"
            ),
            unittest.mock.patch(
                "yaook.op.neutron_ovn.resources.OVNStateResource"
                "._fetch_status",
                new=self.fetch_status_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="yoga")
            ),
        ]

        for p in self.__patches:
            p.start()

        self._node_patch = [
            unittest.mock.patch(
                "yaook.op.neutron_ovn.resources.OVNStateResource"
                "._maintenance_required_label"
            ),
        ]
        self._start_node_patch()

    def _start_node_patch(self):
        for p in self._node_patch:
            p.start()

    def _tear_down_node_patch(self):
        for p in self._node_patch:
            p.stop()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        self._tear_down_node_patch()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_ovn = {
            op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value:
                str(uuid.uuid4()),
            op_common.SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value:
                str(uuid.uuid4()),

        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_ovn,
        }
        self._keystone_name = self._provide_keystone(NAMESPACE)

        self.ca_config_map_name = str(uuid.uuid4())
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            self._get_neutron_ovn_gateway_agent_deployment_yaml(),
        )

        self.metadata_pw = f"foobar2342-{uuid.uuid4()}"

        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, "magic-metadata-secret",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "magic-metadata-secret",
                    "labels": {
                        context.LABEL_COMPONENT:
                            op_common.NOVA_METADATA_SECRET_COMPONENT,
                        context.LABEL_PARENT_PLURAL: "novadeployments",
                        context.LABEL_PARENT_GROUP: "yaook.cloud",
                        context.LABEL_PARENT_NAME: "nova",
                    },
                },
                "data": sm.api_utils.encode_secret_data({
                    "password": self.metadata_pw,
                }),
            },
        )

        self.client_mock.put_object(
            "", "v1", "services",
            NAMESPACE, "ovn1",
            {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "ovn1",
                    "labels": {
                        context.LABEL_COMPONENT:
                        op_common.OVSDB_DATABASE_SERVICE_COMPONENT,
                        context.LABEL_PARENT_GROUP:
                        "infra.yaook.cloud",
                        context.LABEL_PARENT_NAME: "neutron-ovn",
                        context.LABEL_PARENT_PLURAL: "ovsdbservices",
                        testutils.LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "spec": {
                    "clusterIP": "10.0.0.1",
                    "selector": {},
                    "ports": {},
                },
            },
        )

        self.client_mock.put_object(
            "", "v1", "services",
            NAMESPACE, "ovn2",
            {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "ovn2",
                    "labels": {
                        context.LABEL_COMPONENT:
                        op_common.OVSDB_DATABASE_SERVICE_COMPONENT,
                        context.LABEL_PARENT_GROUP:
                        "infra.yaook.cloud",
                        context.LABEL_PARENT_NAME: "neutron-ovn",
                        context.LABEL_PARENT_PLURAL: "ovsdbservices",
                        testutils.LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "spec": {
                    "clusterIP": "10.0.0.2",
                    "selector": {},
                    "ports": {},
                },
            },
        )

    async def test_create_ovn_sfs(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_statefulsets_ready_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)
        sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_controller",
            },
        )

        self.assertEqual(
            sfs.spec.selector.match_labels["state.yaook.cloud/parent-name"],
            "node2"
        )

    async def test_configures_ovn_sfs(self):
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_config",
            },
        )
        mapping = base64.b64decode(
            cfg.data['bridge_mappings'].encode("ascii")
            ).decode("utf-8")

        self.assertEqual(mapping, 'br-ex;eth1;physnet\n')

    async def test_configures_ovn_ovs_sfs(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)
        sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovs_vswitchd"},
        )

        self.assertEqual(
            sfs.spec.selector.match_labels["state.yaook.cloud/parent-name"],
            "node2"
        )

    async def test_configures_ovn_ovsdb_sfs(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)
        sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovsdb_server"},
        )

        self.assertEqual(
            sfs.spec.selector.match_labels["state.yaook.cloud/parent-name"],
            "node2"
        )

    async def test_ovn_sfs_requires_ovs(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_statefulsets_ready_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)
        LABEL_NETWORK_OVS_PROVIDER = "state.yaook.cloud/component"

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertEqual(
            ovn_sfs.spec.template.spec.affinity.pod_affinity.
            required_during_scheduling_ignored_during_execution[0].
            label_selector.match_labels[LABEL_NETWORK_OVS_PROVIDER],
            "ovs_vswitchd",
        )

    async def test_ovn_stateful_set_pinned_to_node(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_statefulsets_ready_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = sm.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertEqual(
            ovn_sfs.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.
            node_selector_terms[0].match_fields[0].to_dict(),
            {
                "key": "metadata.name",
                "operator": "In",
                "values": [NAME],
            },
        )

    # nova-compute pod want's to run only on hosts where a pod is providing l2
    # so we need to check, if we set this label.
    async def test_ovn_sfs_provides_l2(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_statefulsets_ready_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertEqual(
            ovn_sfs.spec.template.metadata.labels
            [op_common.LABEL_NETWORK_L2_PROVIDER],
            "true",
        )

    async def test_ovn_sfs_env_set(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertIn(
            kclient.V1EnvVar("SOUTHBOUND_SERVERS",
                             "ssl:1.1.1.1:6642," +
                             "ssl:1.1.1.2:6642"),
            ovn_sfs.spec.template.spec.init_containers[0].env,
        )
        self.assertIn(
            kclient.V1EnvVar("NETWORK_NODE", "true"),
            ovn_sfs.spec.template.spec.init_containers[0].env,
        )
        value_from = {'config_map_key_ref': None,
                      'field_ref': {'api_version': None,
                                    'field_path': 'status.hostIP'},
                      'resource_field_ref': None,
                      'secret_key_ref': None}
        self.assertIn(
            kclient.V1EnvVar("OVERLAY_IP_ADDRESS", value_from=value_from),
            ovn_sfs.spec.template.spec.init_containers[0].env,
        )

    async def test_ovn_sfs_env_set_with_old_southboundAddress(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        self._mock_labelled_nodes(
            self.default_node_setup,
            {"l2-lock.maintenance.yaook.cloud/iwantcake": "yes"})
        yml = self._get_neutron_ovn_gateway_agent_deployment_yaml()
        yml["spec"]["southboundServers"] = [
            "ovn1.test-namespace.svc:6642",
            "ovn2.test-namespace.svc:6642"
        ]
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            yml,
        )

        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertIn(
            kclient.V1EnvVar("SOUTHBOUND_SERVERS",
                             "ssl:ovn1.test-namespace.svc:6642," +
                             "ssl:ovn2.test-namespace.svc:6642"),
            ovn_sfs.spec.template.spec.init_containers[0].env,
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_service"}
        )

        self.assertIsNotNone(service)

    async def test_node_has_ovn_label(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        self._tear_down_node_patch()
        await self.cr.sm.ensure(self.ctx)

        v1 = kclient.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value  # noqa: E501
        )

        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, None
            ) == 'False'
        )
        self._start_node_patch()

    async def test_sets_require_migration_label(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(
            self.default_node_setup,
            {"l2-lock.maintenance.yaook.cloud/iwantcake": "yes"})
        self._tear_down_node_patch()

        yml = self._get_neutron_ovn_gateway_agent_deployment_yaml()
        yml["status"]["state"] = "Enabled"
        yml["metadata"]["deletionTimestamp"] = "now"
        yml["metadata"]["finalizers"] = [
            "network.yaook.cloud/neutron-ovn-agent"
        ]
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            yml,
        )

        await self.cr.sm.ensure(self.ctx)

        v1 = kclient.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value  # noqa: E501
        )
        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, None
            ) == 'True'
        )
        self._start_node_patch()

    async def test_keeps_require_migration_label_if_agent_down(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        self.fetch_status_mock.return_value = resource.ResourceStatus(
            up=False,
            enabled=True,
            disable_reason=None,
        )
        self._tear_down_node_patch()
        await self.cr.sm.ensure(self.ctx)

        v1 = kclient.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value  # noqa: E501
        )
        require_migration_label = (nodes.items[0].metadata.labels or {}).get(
            context.LABEL_L2_REQUIRE_MIGRATION, None
            )
        self.assertTrue(
            require_migration_label is None or
            require_migration_label == 'True'
        )
        self._start_node_patch()

    async def test_status_state_enabled(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        ovniface = interfaces.neutron_ovn_agent_interface(self.api_client)
        ovn_agent, = await ovniface.list_(NAMESPACE)

        self.assertEqual(ovn_agent["status"]["state"], "Enabled")

    async def test_status_state_evicting(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(
            self.default_node_setup,
            {"l2-lock.maintenance.yaook.cloud/iwantcake": "yes"})
        yml = self._get_neutron_ovn_gateway_agent_deployment_yaml()
        yml["status"]["state"] = "Enabled"
        yml["metadata"]["deletionTimestamp"] = "now"
        yml["metadata"]["finalizers"] = [
            "network.yaook.cloud/neutron-ovn-agent"
        ]
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            yml,
        )

        await self.cr.sm.ensure(self.ctx)

        ovniface = interfaces.neutron_ovn_agent_interface(self.api_client)
        ovn_agent, = await ovniface.list_(NAMESPACE)
        self.assertEqual(ovn_agent["status"]["state"], "Evicting")

    async def test_creates_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        agent_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"}
        )
        ovs_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovs_vswitchd"}
        )
        ovsdb_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovsdb_server"}
        )

        self.assertEqual(
            testutils.container_resources(agent_sts, 0, is_init=True),
            testutils.unique_resources("ovn-controller-setup")
        )
        self.assertEqual(
            testutils.container_resources(agent_sts, 0),
            testutils.unique_resources("ovn-controller")
        )
        self.assertEqual(
            testutils.container_resources(ovs_sts, 0),
            testutils.unique_resources("ovs-vswitchd")
        )
        self.assertEqual(
            testutils.container_resources(ovsdb_sts, 0),
            testutils.unique_resources("ovsdb-server")
        )


class TestNeutronOVNAgentComputeDeployments(testutils.CustomResourceTestCase):
    # Needs to be disabled as the neutron-ovn-operator relies on SSA to
    # make patches to its own resource. Since our testutils do not implement
    # that this test can not pass.
    run_needs_update_test = False
    run_topology_spread_test = False

    def _get_neutron_ovn_compute_agent_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "novaRef": {
                    "name": "nova",
                },
                "neutronOvnMetadataAgentConfig": [],
                "deployedOnComputeNode": True,
                "bridgeConfig": [],
                "southboundServers": ["ssl:10.0.0.1:6642",
                                      "ssl:10.0.0.2:6642"],
                "issuerRef": "ca-issuer",
                "caConfigMapName": self.ca_config_map_name,
                "region": {
                    "name": "RegionOne",
                },
                "resources": testutils.generate_resources_dict(
                    "ovn-controller",
                    "ovn-controller-setup",
                    "ovs-vswitchd",
                    "ovsdb-server",
                ),
            },
            "status": {
                "state": "Creating",
            },
        }

    def setUp(self):
        super().setUp()

        self.fetch_status_mock = unittest.mock.Mock([])
        self.fetch_status_mock.return_value = resource.ResourceStatus(
            up=True,
            enabled=True,
            disable_reason=None,
        )

        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.resources.base.write_ca_certificates"
            ),
            unittest.mock.patch(
                "yaook.op.neutron_ovn.resources.OVNStateResource"
                "._fetch_status",
                new=self.fetch_status_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="yoga")
            ),
        ]

        for p in self.__patches:
            p.start()

        self._node_patch = [
            unittest.mock.patch(
                "yaook.op.neutron_ovn.resources.OVNStateResource"
                "._maintenance_required_label"
            ),
        ]
        self._start_node_patch()

    def _start_node_patch(self):
        for p in self._node_patch:
            p.start()

    def _tear_down_node_patch(self):
        for p in self._node_patch:
            p.stop()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        self._tear_down_node_patch()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_ovn = {
            op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value:
                str(uuid.uuid4()),
            op_common.SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value:
                str(uuid.uuid4()),

        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_ovn,
        }
        self._keystone_name = self._provide_keystone(NAMESPACE)

        self.ca_config_map_name = str(uuid.uuid4())
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            self._get_neutron_ovn_compute_agent_deployment_yaml(),
        )

        self.metadata_pw = f"foobar2342-{uuid.uuid4()}"

        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, "magic-metadata-secret",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "magic-metadata-secret",
                    "labels": {
                        context.LABEL_COMPONENT:
                            op_common.NOVA_METADATA_SECRET_COMPONENT,
                        context.LABEL_PARENT_PLURAL: "novadeployments",
                        context.LABEL_PARENT_GROUP: "yaook.cloud",
                        context.LABEL_PARENT_NAME: "nova",
                    },
                },
                "data": sm.api_utils.encode_secret_data({
                    "password": self.metadata_pw,
                }),
            },
        )

        self.client_mock.put_object(
            "", "v1", "services",
            NAMESPACE, "ovn1",
            {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "ovn1",
                    "labels": {
                        context.LABEL_COMPONENT:
                        op_common.OVSDB_DATABASE_SERVICE_COMPONENT,
                        context.LABEL_PARENT_GROUP:
                        "infra.yaook.cloud",
                        context.LABEL_PARENT_NAME: "neutron-ovn",
                        context.LABEL_PARENT_PLURAL: "ovsdbservices",
                        testutils.LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "spec": {
                    "clusterIP": "10.0.0.1",
                    "selector": {},
                    "ports": {},
                },
            },
        )

        self.client_mock.put_object(
            "", "v1", "services",
            NAMESPACE, "ovn2",
            {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "ovn2",
                    "labels": {
                        context.LABEL_COMPONENT:
                        op_common.OVSDB_DATABASE_SERVICE_COMPONENT,
                        context.LABEL_PARENT_GROUP:
                        "infra.yaook.cloud",
                        context.LABEL_PARENT_NAME: "neutron-ovn",
                        context.LABEL_PARENT_PLURAL: "ovsdbservices",
                        testutils.LABEL_IGNORE_DURING_TESTS: "true",
                    },
                },
                "spec": {
                    "clusterIP": "10.0.0.2",
                    "selector": {},
                    "ports": {},
                },
            },
        )

    async def test_configures_metadata_secret(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_metadata_config",
            },
        )
        agent_cfg = testutils._parse_config(
            cfg.data["neutron_ovn_metadata_agent.conf"],
            decode=True,
        )
        agent_secret = agent_cfg.get("DEFAULT", "metadata_proxy_shared_secret")

        self.assertEqual(agent_secret, self.metadata_pw)

    async def test_configures_metadata_secret_with_southboundservers(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_metadata_config",
            },
        )
        agent_cfg = testutils._parse_config(
            cfg.data["neutron_ovn_metadata_agent.conf"],
            decode=True,
        )
        agent_secret = agent_cfg.get("DEFAULT", "metadata_proxy_shared_secret")

        self.assertEqual(agent_secret, self.metadata_pw)
        self.assertEqual(agent_cfg.get('ovn', 'ovn_sb_connection'),
                         'ssl:10.0.0.1:6642,ssl:10.0.0.2:6642')

    async def test_configures_metadata_secret_old_form_southboundservers(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        yml = self._get_neutron_ovn_compute_agent_deployment_yaml()
        yml["spec"]["southboundServers"] = [
            "ovn1.test-namespace.svc:6642",
            "ovn2.test-namespace.svc:6642"
        ]
        self._configure_cr(
            neutron_ovn.NeutronOVNAgent,
            yml,
        )

        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        cfg, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_metadata_config",
            },
        )
        agent_cfg = testutils._parse_config(
            cfg.data["neutron_ovn_metadata_agent.conf"],
            decode=True,
        )
        agent_secret = agent_cfg.get("DEFAULT", "metadata_proxy_shared_secret")

        self.assertEqual(agent_secret, self.metadata_pw)
        self.assertEqual(agent_cfg.get('ovn', 'ovn_sb_connection'),
                         'ssl:10.0.0.1:6642,ssl:10.0.0.2:6642')

    async def test_ovn_sfs_gateway_node_env_is_set_false(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        ovn_sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_controller"},
        )

        self.assertIn(
            kclient.V1EnvVar("NETWORK_NODE", "false"),
            ovn_sfs.spec.template.spec.init_containers[0].env,
        )
