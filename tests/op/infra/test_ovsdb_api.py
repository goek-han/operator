#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import yaook.op.infra.ovsdb_cr as ovsdb_cr
import yaook.statemachine as sm
import yaook.op.common as cm

from ... import testutils


NAME = "ovsdb-northbound-xyz"
NAMESPACE = "infra"
ISSUER_REF_NAME = "ovn-central-ca-issuer"


class TestOVSDBService(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            ovsdb_cr.OVSDBService,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "OVSDBService",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "dbSchema": "northbound",
                    "imageRef": "example.com/ovsdb:1.2.3",
                    "issuerRef": {
                        "name": ISSUER_REF_NAME,
                    },
                    "backup": {
                        "schedule": "0 * * * *",
                    },
                    "replicas": 5,
                    "storageSize": "32Gi",
                    "storageClassName": "foobar",
                    "resources": testutils.generate_resources_dict(
                        "setup-ovsdb",
                        "ovsdb",
                        "backup-creator",
                        "backup-shifter",
                    ),
                    "serviceMonitor": {
                        "additionalLabels": {
                            "mykey": "mylabel",
                        },
                    },
                },
            }
        )
        self.run_podspec_tests = False

    async def test_creates_headless_service_for_statefulset(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        ovsdb_sts, = await statefulsets.list_(NAMESPACE)

        headless_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "headless_service",
            },
        )

        self.assertTrue(
            headless_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            headless_service.spec.selector,
            ovsdb_sts.spec.template.metadata.labels,
        )
        self.assertEqual(
            ovsdb_sts.spec.service_name,
            headless_service.metadata.name,
        )

    async def test_creates_service_for_each_statefulset_pod(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        ovsdb_sts, = await statefulsets.list_(NAMESPACE)

        pod_services = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    cm.OVSDB_DATABASE_SERVICE_COMPONENT
            },
        )

        self.assertEqual(5, len(pod_services))

        for i in range(ovsdb_sts.spec.replicas):
            pod_name = f"{ovsdb_sts.metadata.name}-{i}"
            expected_labels = dict(ovsdb_sts.spec.template.metadata.labels)
            expected_labels["statefulset.kubernetes.io/pod-name"] = pod_name
            for service in pod_services:
                if service.spec.selector == expected_labels:
                    break
            else:
                self.fail(f"Could not find service for pod {pod_name}")

    async def test_creates_ovsdb_certificate(self):
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            ISSUER_REF_NAME,
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_ovsdb_statefulset(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_deployments_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        ovsdb, = await statefulsets.list_(NAMESPACE)

        ovsdb_container = ovsdb.spec.template.spec.containers[0]

        # dict comprehension to unpack the env object list into a smaller dict
        simple_env_map = {envdef.name: envdef.value
                          for envdef in ovsdb_container.env
                          if envdef.value is not None}

        self.assertEqual(ovsdb_container.image, "example.com/ovsdb:1.2.3",)
        self.assertEqual(simple_env_map["DBNAME"], "OVN_Northbound",)
        self.assertEqual(simple_env_map["GLOBAL"], "NB_Global",)

        volume_claim_template, = ovsdb.spec.volume_claim_templates

        self.assertEqual(
            volume_claim_template.spec.storage_class_name,
            "foobar",
        )

        self.assertEqual(
            volume_claim_template.spec.resources.requests["storage"],
            "32Gi",
        )

    async def test_ovsdb_statefulset_exposes_ports_ovsdb(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        ovsdb, = await statefulsets.list_(NAMESPACE)

        ovsdb_container = ovsdb.spec.template.spec.containers[0]

        port_map = {port.name: (port.container_port, port.protocol)
                    for port in ovsdb_container.ports}

        self.assertDictEqual(
            {
                "ovsdb": (6641, "TCP"),
                "raft": (6643, "TCP"),
            },
            port_map,
        )

    async def test_ovsdb_sts_container_env_has_port_and_database_name(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        ovsdb, = await statefulsets.list_(NAMESPACE)

        d = {e.name: e.value for e
             in ovsdb.spec.template.spec.containers[0].env
             if e.value is not None}

        self.assertEqual(int(d["PORT"]), 6641,)
        self.assertEqual(d["DBNAME"], "OVN_Northbound",)

    async def test_creates_containers_with_resource(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        ovsdb_sts, = await statefulsets.list_(NAMESPACE)

        self.assertEqual(
            testutils.container_resources(ovsdb_sts, 0, is_init=True),
            testutils.unique_resources("setup-ovsdb")
        )
        self.assertEqual(
            testutils.container_resources(ovsdb_sts, 0),
            testutils.unique_resources("ovsdb")
        )

    async def test_database_statefulset_creates_backup_pods(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        containers = db.spec.template.spec.containers

        self.assertEqual(5, len(containers))

        envs = {
            container.name: {
                envdef.name: envdef.value
                for envdef in container.env
            } for container in containers
        }

        self.assertEqual(
            envs["ovsdb"]["DBNAME"],
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_OVSDB_DATABASE"]
        )
        self.assertEqual(
            "ssl:ovsdb-northbound-xyz-ovsdb-0.ovsdb-northbound-xyz-ovsdb" +
            ".infra.svc.test.dns.domain:6641," +
            "ssl:ovsdb-northbound-xyz-ovsdb-1.ovsdb-northbound-xyz-ovsdb" +
            ".infra.svc.test.dns.domain:6641," +
            "ssl:ovsdb-northbound-xyz-ovsdb-2.ovsdb-northbound-xyz-ovsdb" +
            ".infra.svc.test.dns.domain:6641," +
            "ssl:ovsdb-northbound-xyz-ovsdb-3.ovsdb-northbound-xyz-ovsdb" +
            ".infra.svc.test.dns.domain:6641," +
            "ssl:ovsdb-northbound-xyz-ovsdb-4.ovsdb-northbound-xyz-ovsdb" +
            ".infra.svc.test.dns.domain:6641",
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_OVSDB_SOCKET"]
        )
        self.assertEqual(
            "/backup/new",
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_OUT_PATH"]
        )

        self.assertEqual(
            "/backup",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_WORK_DIR"]
        )

        self.assertEqual(
            "dumpinfo",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_SHIFTERS"]
        )

    async def test_service_monitor_is_labeled_and_has_service_and_secret(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(NAMESPACE)

        self.assertEqual("mylabel", sms["metadata"]["labels"]["mykey"])

        self.assertEqual("prometheus", sms["spec"]["endpoints"][0]["port"])

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        headless_service, = await services.list_(
            NAMESPACE,
            label_selector={sm.context.LABEL_COMPONENT: "headless_service", },
        )
        self.assertEqual(servicelabels, headless_service.metadata.labels)

        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "certificate_secret",
            }
        )
        self.assertEqual(certificatename, cert_secret.metadata.name)
