#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pprint

import yaook.op.common as op_common
import yaook.op.horizon as horizon
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "horizon"


class TestHorizonDeployments(testutils.ReleaseAwareCustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            horizon.Horizon,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "ingress": {
                        "fqdn": "horizon-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                        "externalCertificateSecretRef": {
                            "name": "some-secret",
                        },
                    },
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "replicas": 2,
                    "targetRelease": "queens",
                    "resources": testutils.generate_resources_dict(
                        "horizon",
                        "ssl-terminator-external",
                        "service-reload-external",
                    ),
                },
            },
        )

    async def test_creates_deployment_with_replica_spec(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)

        deployment, = await deployment_interface.list_(NAMESPACE)
        self.assertEqual(deployment.spec.replicas, 2)

    async def test_creates_containers_with_resource(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("horizon")
        )
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("service-reload-external")
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "horizon")

        self.assertIsNotNone(service)

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "horizon-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "horizon")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "horizon-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "horizon")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "horizon")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(NAMESPACE)
        deployment, = await deployments.list_(NAMESPACE)

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_references_keystone_endpoint_config_in_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)

        deployment, = await deployments.list_(NAMESPACE)

        endpoint_config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    op_common.KEYSTONE_INTERNAL_API_COMPONENT,
            },
        )

        container_spec = deployment.spec.template.spec.containers[0]
        envmap = {
            var.name: var
            for var in container_spec.env
        }

        keystone_url_ref = envmap["YAOOK_HORIZON_KEYSTONE_URL"].value_from

        self.assertEqual(
            keystone_url_ref.config_map_key_ref.name,
            endpoint_config.metadata.name,
        )
        self.assertEqual(
            keystone_url_ref.config_map_key_ref.key,
            "OS_AUTH_URL",
        )

        interface_ref = envmap["YAOOK_HORIZON_INTERFACE"].value_from

        self.assertEqual(
            interface_ref.config_map_key_ref.name,
            endpoint_config.metadata.name,
        )
        self.assertEqual(
            interface_ref.config_map_key_ref.key,
            "OS_INTERFACE",
        )

    async def test_applies_scheduling_key_to_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.DASHBOARD.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.DASHBOARD.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_forwards_external_tls_secret(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)

        volume_spec, = [
            volume for volume in deployment.spec.template.spec.volumes
            if volume.name == "tls-secret-external"
        ]

        self.assertEqual(
            volume_spec.secret.secret_name,
            "some-secret",
        )
