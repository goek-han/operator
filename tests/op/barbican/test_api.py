#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import base64
import contextlib
import json
import os
import pprint
import unittest
import unittest.mock

import ddt

import yaook.op.common as op_common
import yaook.op.barbican as barbican
from yaook.op.barbican import generate_kek
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from ... import testutils


NAMESPACE = "test-namespace"
NAME = "barbican"
CONFIG_FILE_NAME = "barbican.conf"
CONFIG_PATH = "/etc/barbican"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "observer"
POLICY_RULE_VALUE = "role:observer"
BARBICAN_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


def _get_barbican_deployment_yaml(keystone_name):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
            "api": {
                "ingress": {
                    "fqdn": "barbican-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 2,
                "resources": testutils.generate_resources_dict(
                    "api.barbican-api",
                    "api.barbican-worker",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            "keystoneListener": {
                "replicas": 20,
                "resources": testutils.generate_resources_dict(
                    "keystoneListener.barbican-keystone-listener",
                ),
            },
            "database": {
                "replicas": 21,
                "storageSize": "8Gi",
                "storageClassName": "foo-class",
                "proxy": {
                    "replicas": 1,
                    "resources": testutils.generate_db_proxy_resources(),
                },
                "backup": {
                    "schedule": "0 * * * *"
                },
                "resources": testutils.generate_db_resources(),
            },
            "messageQueue": {
                "replicas": 1,
                "storageSize": "2Gi",
                "storageClassName": "bar-class",
                "resources": testutils.generate_amqp_resources(),
            },
            "issuerRef": {
                "name": "issuername"
            },
            "barbicanConfig": {},
            "barbicanSecrets": [
                {
                    "secretName": CONFIG_SECRET_NAME,
                    "items": [{
                        "key": CONFIG_SECRET_KEY,
                        "path": "/DEFAULT/mytestsecret",
                    }],
                },
            ],
            "targetRelease": "queens",
            "jobResources": testutils.generate_resources_dict(
                "job.barbican-db-sync-job",
            ),
        },
    }


@ddt.ddt
class TestBarbicanDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            barbican.Barbican,
            _get_barbican_deployment_yaml(self._keystone_name),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = _get_barbican_deployment_yaml(self._keystone_name)
        self._configure_cr(barbican.Barbican, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_barbican_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(barbican.Barbican, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(
            config.data[CONFIG_FILE_NAME], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "barbican-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_creates_db_sync_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 2)

    async def test_creates_listener_deployment_with_replicas(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_listener"}
        )
        self.assertEqual(deployment.spec.replicas, 20)

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            self.assertEqual(
                job.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
                str(job.metadata),
            )

    async def test_creates_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 21)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"], "foo-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_config_with_database_uri(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        barbican_conf = config.data[CONFIG_FILE_NAME]
        cfg = testutils._parse_config(barbican_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "sql_connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "barbicandeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "barbican-api",
        )

        barbican_conf = config.data["barbican.conf"]
        cfg = testutils._parse_config(barbican_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("DEFAULT", "sql_connection"),
        )

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 1)
        self.assertEqual(mq["spec"]["storageSize"], "2Gi")
        self.assertEqual(mq["spec"]["storageClassName"], "bar-class")

    async def test_creates_config_with_transport_url(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        barbican_conf = config.data[CONFIG_FILE_NAME]
        cfg = testutils._parse_config(barbican_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://api:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )

    async def test_amqp_server_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        amqpserver = interfaces.amqpserver_interface(self.api_client)

        amqp, = await amqpserver.list_(
            NAMESPACE,
        )

        self.assertEqual(
            "issuername",
            amqp["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "barbican-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "barbican-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://barbican-ingress:8080",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "barbican")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "barbican-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "barbican")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "barbican-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_PARENT_PLURAL: "barbicandeployments"}
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        self._make_all_dependencies_complete_immediately()
        blocking = await self.cr.sm.ensure(self.ctx)
        self.assertEquals(len(blocking), 0, blocking)
        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                KEY_MANAGER_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.KEY_MANAGER_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        self.assertEquals(len(jobs), 1)

        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_BARBICAN.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_BARBICAN.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        barbican_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            barbican_conf_content.get("DEFAULT", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    expected_api_depl_volume_indexes = {
        "config": 0,
        "policy": 1,
        "ca_certs": 2,
        "tls": 3,
        "ext_cert": 4,
        "terminator-config": 5,
        "terminator-external-config": 6
    }
    expected_api_depl_volume_mount_indexes = {
        "barb_container": {
            "config": 0,
            "policy": 1,
            "ca_certs": 2,
        },
        "ssl_term_container": {
            "config": 0,
            "tls": 1,
            "ca_certs": 2,
        },
        "reload_container": {
            "config": 0,
            "tls": 1,
        }
    }

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_api_deployment_with_volumes(self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        tls_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "certificate_secret"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "barbican_policy"},
        )
        ca_cert_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_NAME: "barbican"
            },
        )

        # assert volumes are correct
        observed_volumes = api_deployment.spec.template.spec.volumes
        self.assertEqual(len(observed_volumes),
                         len(self.expected_api_depl_volume_indexes))

        # assert config volume
        observed_config_volume = \
            observed_volumes[self.expected_api_depl_volume_indexes["config"]]
        await self.assert_config_volume(config_secret, observed_config_volume)

        # assert policy volume
        observed_policy_volume = \
            observed_volumes[self.expected_api_depl_volume_indexes["policy"]]
        await self.assert_policy_volume(
            policy_config_map, observed_policy_volume)

        # assert ca-cert volume
        observed_ca_cert_volume = \
            observed_volumes[self.expected_api_depl_volume_indexes["ca_certs"]]
        await self.assert_ca_cert_volume(ca_cert_config_map,
                                         observed_ca_cert_volume)

        # assert tls-secret volume
        observed_tls_volume = \
            observed_volumes[self.expected_api_depl_volume_indexes["tls"]]
        self.assertEqual(
            observed_tls_volume.secret.secret_name,
            tls_secret.metadata.name,
        )
        self.assertEqual(observed_tls_volume.secret.items, None)

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_listener_deployment_with_volumes(self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        listener_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_listener"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "barbican_policy"},
        )
        ca_cert_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_NAME: "barbican"
            },
        )

        # assert volumes are correct
        observed_volumes = listener_deployment.spec.template.spec.volumes
        self.assertEqual(len(observed_volumes), 3)

        await self.assert_config_volume(config_secret, observed_volumes[0])
        await self.assert_policy_volume(policy_config_map, observed_volumes[1])
        await self.assert_ca_cert_volume(
            ca_cert_config_map, observed_volumes[2])

    async def assert_config_volume(
            self, config_secret, observed_config_volume):
        self.assertEqual(
            observed_config_volume.secret.secret_name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(observed_config_volume.secret.items), 1)
        self.assertEqual(
            observed_config_volume.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            observed_config_volume.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

    async def assert_policy_volume(
            self, policy_config_map, observed_policy_volume):
        self.assertEqual(
            observed_policy_volume.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(observed_policy_volume.config_map.items, None)

    async def assert_ca_cert_volume(self, ca_cert_config_map,
                                    observed_ca_cert_volume):
        self.assertEqual(
            observed_ca_cert_volume.config_map.name,
            ca_cert_config_map.metadata.name,
        )
        self.assertEqual(observed_ca_cert_volume.config_map.items, None)

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_listener_deployment_with_volume_mounts(
            self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        keystone_listener_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_listener"},
        )
        container = \
            keystone_listener_deployment.spec.template.spec.containers[0]
        await self.assert_barbican_container_volume_mounts(
            keystone_listener_deployment, container)

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_api_deployment_with_barb_containers_volume_mounts(
            self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        # assert each of the barbican containers has correct volume mounts
        for container_index in range(2):
            container = \
                api_deployment.spec.template.spec.containers[container_index]
            await self.assert_barbican_container_volume_mounts(
                api_deployment, container)

    async def assert_barbican_container_volume_mounts(
            self, deployment, container):
        # assert that the container has the correct number of volume mounts
        expected_container_volume_mounts = \
            self.expected_api_depl_volume_mount_indexes["barb_container"]
        self.assertEqual(
            len(container.volume_mounts),
            len(expected_container_volume_mounts)
        )
        # assert container mounts config file correctly
        observed_config_volume = \
            deployment.spec.template.spec.volumes[
                self.expected_api_depl_volume_indexes["config"]
            ]
        observed_config_volume_mount = \
            container.volume_mounts[
                expected_container_volume_mounts["config"]
            ]
        self.assertEqual(
            observed_config_volume_mount.name,
            observed_config_volume.name,
        )
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, CONFIG_FILE_NAME),
        )
        self.assertEqual(
            observed_config_volume_mount.sub_path,
            CONFIG_FILE_NAME,
        )
        # assert container mounting policy file correctly
        observed_policy_volume = \
            deployment.spec.template.spec.volumes[1]
        observed_policy_volume_mount = container.volume_mounts[
            expected_container_volume_mounts["policy"]
        ]
        self.assertEqual(
            observed_policy_volume_mount.name,
            observed_policy_volume.name,
        )
        self.assertEqual(
            observed_policy_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, POLICY_FILE_NAME),
        )
        self.assertEqual(
            observed_policy_volume_mount.sub_path,
            POLICY_FILE_NAME,
        )
        # assert container mounting certs correctly
        observed_certs_volume = \
            deployment.spec.template.spec.volumes[2]
        observed_certs_volume_mount = container.volume_mounts[
            expected_container_volume_mounts["ca_certs"]
        ]
        self.assertEqual(
            observed_certs_volume_mount.name,
            observed_certs_volume.name,
        )
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/pki/tls/certs",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            None,
        )

    async def test_creates_api_deployment_with_term_containers_volume_mounts(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        # assert container has correct number of volume mounts
        ssl_terminator_container_spec = \
            api_deployment.spec.template.spec.containers[3]
        expected_ssl_terminator_container_volume_mounts = \
            self.expected_api_depl_volume_mount_indexes["ssl_term_container"]
        self.assertEqual(
            len(ssl_terminator_container_spec.volume_mounts),
            len(expected_ssl_terminator_container_volume_mounts)
        )

        # assert container mounting certs correctly
        observed_certs_volume = \
            api_deployment.spec.template.spec.volumes[2]
        observed_certs_volume_mount = \
            ssl_terminator_container_spec.volume_mounts[
                expected_ssl_terminator_container_volume_mounts["ca_certs"]
            ]
        self.assertEqual(
            observed_certs_volume_mount.name,
            observed_certs_volume.name,
        )
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/ssl/certs/ca-certificates.crt",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            "ca-bundle.crt",
        )

    async def test_creates_api_deployment_with_reload_containers_volume_mounts(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        # assert container has correct number of volume mounts
        reload_container_spec = \
            api_deployment.spec.template.spec.containers[4]
        expected_reload_container_volume_mounts = \
            self.expected_api_depl_volume_mount_indexes["reload_container"]
        self.assertEqual(
            len(reload_container_spec.volume_mounts),
            len(expected_reload_container_volume_mounts)
        )

        # assert container mounting tls-secret correctly
        observed_tls_secret_volume = \
            api_deployment.spec.template.spec.volumes[3]
        observed_tls_secret_volume_mount = \
            reload_container_spec.volume_mounts[
                expected_reload_container_volume_mounts["tls"]
            ]
        self.assertEqual(
            observed_tls_secret_volume_mount.name,
            observed_tls_secret_volume.name,
        )
        self.assertEqual(
            observed_tls_secret_volume_mount.mount_path,
            "/data",
        )
        self.assertEqual(
            observed_tls_secret_volume_mount.sub_path,
            None,
        )

    async def test_creates_api_deployment_with_correct_api_container_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        api_container_spec = api_deployment.spec.template.spec.containers[0]
        self.assertEqual(len(api_container_spec.env), 2)
        local_port_env_var = api_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")

    async def test_creates_api_deployment_with_correct_ssl_term_container_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        ssl_term_container_spec = \
            api_deployment.spec.template.spec.containers[2]
        self.assertEqual(len(ssl_term_container_spec.env), 4)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "9311")
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9090")

    async def test_create_api_deploy_verify_ext_ssl_term_container_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        external_ssl_term_container_spec = \
            api_deployment.spec.template.spec.containers[3]
        self.assertEqual(len(external_ssl_term_container_spec.env), 4)
        service_port_env_var = external_ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "9312")
        local_port_env_var = external_ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = external_ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9091")

    async def test_creates_containers_with_resources(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        listener_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_listener"},
        )
        jobs = interfaces.job_interface(self.api_client)
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"},
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.barbican-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.barbican-worker")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 5),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(listener_deployment, 0),
            testutils.unique_resources(
                "keystoneListener.barbican-keystone-listener")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.barbican-db-sync-job")
        )

    @ddt.data({}, BARBICAN_POLICY)
    async def test_creates_policy_configmap(self, policy):
        barbican_deployment_yaml = _get_barbican_deployment_yaml(
            self._keystone_name,
        )
        barbican_deployment_yaml["spec"].update(policy)
        self._configure_cr(barbican.Barbican, barbican_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        barbican_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "barbican_policy"},
        )

        self.assertEqual(barbican_policy.kind, "ConfigMap")
        self.assertTrue(
            barbican_policy.metadata.name.startswith("barbican-policy"))

        barbican_policy_decoded = json.loads(
            barbican_policy.data[POLICY_FILE_NAME])

        for rule, value in policy.get("policy", {}).items():
            self.assertEqual(
                barbican_policy_decoded[rule],
                value
            )

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config"},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)
        lines = decoded["barbican.conf"].splitlines()

        self.assertIn(f"mytestsecret = {CONFIG_SECRET_VALUE}", lines)

    async def test_not_creates_db_upgrade_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"},
        )

        self.assertEqual(0, len(jobs))


class Testgenerate_kek(unittest.TestCase):
    def test_defaults_to_32_bytes_of_entropy(self):
        with contextlib.ExitStack() as stack:
            urandom = stack.enter_context(unittest.mock.patch("os.urandom"))
            urandom.return_value = b"random bytes"

            generate_kek()

        urandom.assert_called_once_with(32)

    def test_generates_kek(self):
        expected_kek_byte_length = 32
        random_bytes = b"random bytes"
        expected_kek = base64.b64encode(random_bytes).decode("utf-8")

        with contextlib.ExitStack() as stack:
            urandom = stack.enter_context(unittest.mock.patch("os.urandom"))
            urandom.return_value = random_bytes

            result = generate_kek()

        urandom.assert_called_once_with(expected_kek_byte_length)
        self.assertEqual(result, expected_kek)


class TestBarbicanDeploymentsUpgrade(
        testutils.ReleaseAwareCustomResourceTestCase):
    async def test_sets_deployedopenstackversion_on_success(self):
        raise unittest.SkipTest("not relevant")

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        cr = _get_barbican_deployment_yaml(self._keystone_name)
        cr["metadata"]["generation"] = 1
        cr["spec"]["imagePullSecrets"] = [
            {
                "name": "testpullsecret",
            }
        ]
        self._configure_cr(
            barbican.Barbican,
            cr,
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )
        self._make_all_databases_ready_immediately()
        hooks = copy.deepcopy(self.client_mock._hooks)
        self._make_all_dependencies_complete_immediately()
        await self.cr.reconcile(self.ctx, 0)
        self.client_mock._hooks = hooks

        cr = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME,
        )
        cr["spec"]["targetRelease"] = "rocky"
        cr["metadata"]["generation"] = 2
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME, cr,
        )
        self.ctx = self._make_context()  # needed to set the updated spec

    async def test_does_upgrade(self):
        self._make_all_dependencies_complete_immediately()
        barbican = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("queens", barbican["status"]["installedRelease"])

        await self.cr.reconcile(self.ctx, 0)
        barbican = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("rocky", barbican["status"]["installedRelease"])

    async def test_replicas_scaled_down_after_db_upgrade_job_started(self):
        self._make_all_deployments_ready_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"},
        )

        self.assertEqual(1, len(jobs))
        self.assertEqual(0, deployment.status.replicas)

    async def test_replicas_scaled_up_after_db_upgrade_job_completion(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"},
        )
        self.assertEqual(1, len(jobs))

        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(2, deployment.status.replicas)

    async def test_not_creates_db_sync_jobs(self):
        self._make_all_dependencies_complete_immediately()

        # We need to remove the job from the initial queens run
        job_interface = interfaces.job_interface(self.api_client)
        for jobname in ["db_sync"]:
            await job_interface.delete_collection(
                NAMESPACE,
                label_selector=sm.api_utils.LabelSelector(
                    match_labels={context.LABEL_COMPONENT: jobname},
                ).as_api_selector())

        await self.cr.sm.ensure(self.ctx)

        for jobname in ["db_sync"]:
            jobs = await job_interface.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: jobname},
            )
            self.assertEqual(0, len(jobs))
