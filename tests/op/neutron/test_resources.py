#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
from unittest.mock import sentinel

import ddt

import yaook.op.neutron.resources as n_resources
from yaook.statemachine.exceptions import ConfigurationInvalid
from ...statemachine.resources.utils import SingleObjectMock


@ddt.ddt
class TestNeutronDHCPAgent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.neutron_dhcp_agent = n_resources.TemplatedNeutronDHCPAgent(
            template=sentinel.template,
        )

    @ddt.data(
            "keystoneRef",
            "novaRef",
            "neutronConfig",
            "neutronDHCPAgentConfig",
            "neutronMetadataAgentConfig",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "targetRelease",
        )
    def test_needs_update_true_if_relevant_key_changes(self, key):
        self.assertTrue(self.neutron_dhcp_agent._needs_update(
            {"spec": {key: sentinel.foo}},
            {"spec": {key: sentinel.bar}}
        ))

    def test_needs_update_false_if_nothing_relevant_changes(self):
        self.assertFalse(self.neutron_dhcp_agent._needs_update(
            {"spec": {"cake": sentinel.foo}},
            {"spec": {"cake": sentinel.bar}}
        ))

    def test_needs_update_true_if_label_changes(self):
        self.assertTrue(self.neutron_dhcp_agent._needs_update(
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.foo
                        }
                    },
                "spec": {}
            },
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.bar
                        }
                    },
                "spec": {}
            },
        ))


@ddt.ddt
class TestNeutronL2Agent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.neutron_l2_agent = n_resources.TemplatedNeutronL2Agent(
            template=sentinel.template,
        )

    @ddt.data(
            "keystoneRef",
            "neutronConfig",
            "bridgeConfig",
            "neutronOpenvSwitchAgentConfig",
            "overlayNetworkConfig",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "targetRelease",
        )
    def test_needs_update_true_if_relevant_key_changes(self, key):
        self.assertTrue(self.neutron_l2_agent._needs_update(
            {"spec": {key: sentinel.foo}},
            {"spec": {key: sentinel.bar}}
        ))

    def test_needs_update_false_if_nothing_relevant_changes(self):
        self.assertFalse(self.neutron_l2_agent._needs_update(
            {"spec": {"cake": sentinel.foo}},
            {"spec": {"cake": sentinel.bar}}
        ))

    def test_needs_update_true_if_label_changes(self):
        self.assertTrue(self.neutron_l2_agent._needs_update(
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.foo
                        }
                    },
                "spec": {}
            },
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.bar
                        }
                    },
                "spec": {}
            },
        ))


@ddt.ddt
class TestNeutronL3Agent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.neutron_l3_agent = n_resources.TemplatedNeutronL3Agent(
            template=sentinel.template,
        )

    @ddt.data(
            "keystoneRef",
            "novaRef",
            "neutronConfig",
            "neutronL3AgentConfig",
            "neutronMetadataAgentConfig",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "targetRelease",
            "startupLimitMinutes",
         )
    def test_needs_update_true_if_relevant_key_changes(self, key):
        self.assertTrue(self.neutron_l3_agent._needs_update(
            {"spec": {key: sentinel.foo}},
            {"spec": {key: sentinel.bar}}
        ))

    def test_needs_update_false_if_nothing_relevant_changes(self):
        self.assertFalse(self.neutron_l3_agent._needs_update(
            {"spec": {"cake": sentinel.foo}},
            {"spec": {"cake": sentinel.bar}}
        ))

    def test_needs_update_true_if_label_changes(self):
        self.assertTrue(self.neutron_l3_agent._needs_update(
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.foo
                        }
                    },
                "spec": {}
            },
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.bar
                        }
                    },
                "spec": {}
            },
        ))


@ddt.ddt
class TestNeutronBGPDRAgent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.neutron_bgpdr_agent = n_resources.TemplatedNeutronBGPDRAgent(
            template=sentinel.template,
        )
        self.neutron_bgpdr_agents = n_resources.NeutronBGPDRAgents(
            scheduling_keys="foo",
            wrapped_state=self.neutron_bgpdr_agent,
        )

    @ddt.data(
            "hostname",
            "keystoneRef",
            "novaRef",
            "neutronConfig",
            "neutronBGPDRAgentConfig",
            "bgpInterfaceMapping",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "targetRelease",
        )
    def test_needs_update_true_if_relevant_key_changes(self, key):
        self.assertTrue(self.neutron_bgpdr_agent._needs_update(
            {"spec": {key: sentinel.foo}},
            {"spec": {key: sentinel.bar}}
        ))

    def test_needs_update_false_if_nothing_relevant_changes(self):
        self.assertFalse(self.neutron_bgpdr_agent._needs_update(
            {"spec": {"cake": sentinel.foo}},
            {"spec": {"cake": sentinel.bar}}
        ))

    def test_needs_update_true_if_label_changes(self):
        self.assertTrue(self.neutron_bgpdr_agent._needs_update(
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.foo
                        }
                    },
                "spec": {}
            },
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.bar
                        }
                    },
                "spec": {}
            },
        ))

    @unittest.mock.patch("yaook.op.neutron.resources._bgp_spec_configkeys")
    @unittest.mock.patch("yaook.op.neutron.resources.NeutronBGPDRAgents._get_target_nodes")   # noqa: E501
    async def test_get_target_instances_bad_config_keys(
            self,
            mock_get_target_nodes,
            mock_bgp_spec_configkeys):
        node1 = unittest.mock.Mock(['V1Node'])
        node1.metadata = unittest.mock.Mock([])
        node1.metadata.name = "X"
        node2 = unittest.mock.Mock(['V1Node'])
        node2.metadata = unittest.mock.Mock([])
        node2.metadata.name = "Y"
        mock_get_target_nodes.return_value = [node1, node2]
        bgp_keys = set()
        # keys differ after 59 characters which is bad and cannot be handled
        # since we split the key at the 59th char
        bgp_keys.add("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab")  # noqa: E501
        bgp_keys.add("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaac")  # noqa: E501
        mock_bgp_spec_configkeys.return_value = bgp_keys

        with self.assertRaisesRegex(
                ConfigurationInvalid,
                "The configkeys for the bgpdr-agents are equal till the 59th "
                "character and cannot be created. Please use names that "
                "differ in the first 59 characters."):
            await self.neutron_bgpdr_agents.get_target_instances("foo")

    @unittest.mock.patch("yaook.op.neutron.resources._bgp_spec_configkeys")
    @unittest.mock.patch("yaook.op.neutron.resources.NeutronBGPDRAgents._get_target_nodes")   # noqa: E501
    async def test_get_target_instances_invalid_name_gets_stripped(
            self,
            mock_get_target_nodes,
            mock_bgp_spec_configkeys):
        node1 = unittest.mock.Mock(['V1Node'])
        node1.metadata = unittest.mock.Mock([])
        node1.metadata.name = "nodeX.foo"
        mock_get_target_nodes.return_value = [node1]
        bgp_keys = set()
        # the key is 56 charactes long
        key = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        bgp_keys.add(key)  # noqa: E501
        mock_bgp_spec_configkeys.return_value = bgp_keys
        # key + '.' = 57 chars + len(nodeX.foo) = 66
        # stripped to 63: aaaaaa.nodeX.
        bgp_key = (key+'.'+node1.metadata.name)[:63]

        expected = {}
        # expected behaviour: '.' at the end gets stripped automatically
        expected[bgp_key[:-1]] = {
            "node": node1.metadata.name,
            "configkey": key,
            "lockName": "bgp-"+key,
        }

        self.assertEquals(
            await self.neutron_bgpdr_agents.get_target_instances("foo"),
            expected
        )

    @unittest.mock.patch("yaook.op.neutron.resources._bgp_spec_configkeys")
    @unittest.mock.patch("yaook.op.neutron.resources.NeutronBGPDRAgents._get_target_nodes")   # noqa: E501
    async def test_get_target_instances_invalid_configkey(
            self,
            mock_get_target_nodes,
            mock_bgp_spec_configkeys):
        node1 = unittest.mock.Mock(['V1Node'])
        node1.metadata = unittest.mock.Mock([])
        node1.metadata.name = "node.foo"
        mock_get_target_nodes.return_value = [node1]
        bgp_keys = set()
        key = "-"
        lock = "bgp-" + key.rstrip("-_.")
        bgp_keys.add(key)
        mock_bgp_spec_configkeys.return_value = bgp_keys

        with self.assertRaisesRegex(
                ConfigurationInvalid,
                f"The lockName {lock} does not match the k8s regex "
                "validation"):
            await self.neutron_bgpdr_agents.get_target_instances("foo")

    @unittest.mock.patch("yaook.op.neutron.resources._bgp_spec_configkeys")
    @unittest.mock.patch("yaook.op.neutron.resources.NeutronBGPDRAgents._get_target_nodes")   # noqa: E501
    async def test_get_target_instances_invalid_name(
            self,
            mock_get_target_nodes,
            mock_bgp_spec_configkeys):
        node1 = unittest.mock.Mock(['V1Node'])
        node1.metadata = unittest.mock.Mock([])
        node1.metadata.name = "node!.foo"
        mock_get_target_nodes.return_value = [node1]
        bgp_keys = set()
        # the key is 56 charactes long
        key = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        bgp_keys.add(key)
        mock_bgp_spec_configkeys.return_value = bgp_keys
        # key + '.' = 57 chars + len(node!.foo) = 66
        # stripped to 63: aaa[...]aaa.node!.
        bgp_key = (key+'.'+node1.metadata.name)[:63].rstrip("-_.")

        with self.assertRaisesRegex(
                ConfigurationInvalid,
                "The bgp agent name shortened to 63 characters "
                "does not match the k8s regex. Wrong name: "
                f"{bgp_key[:63]}"):
            await self.neutron_bgpdr_agents.get_target_instances("foo")

    @unittest.mock.patch("yaook.op.neutron.resources._bgp_spec_configkeys")
    @unittest.mock.patch("yaook.op.neutron.resources.NeutronBGPDRAgents._get_target_nodes")   # noqa: E501
    async def test_get_target_instances_bad_combination(
            self,
            mock_get_target_nodes,
            mock_bgp_spec_configkeys):
        node1 = unittest.mock.Mock(['V1Node'])
        node1.metadata = unittest.mock.Mock([])
        node1.metadata.name = "some.long.node.name.1"
        node2 = unittest.mock.Mock(['V1Node'])
        node2.metadata = unittest.mock.Mock([])
        node2.metadata.name = "some.long.node.name.2"
        mock_get_target_nodes.return_value = [node1, node2]
        bgp_keys = set()

        # the key is 56 charactes long
        key = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        bgp_keys.add(key)
        mock_bgp_spec_configkeys.return_value = bgp_keys

        # key and node name combined will not differ in the first 63
        # characters

        with self.assertRaisesRegex(
                ConfigurationInvalid,
                "The configkeys and the node names do not differ when put "
                "together with a point in between. Use shorter keys so "
                "when combining the config keys with your node names they "
                "differ in the first 64 characters."):
            await self.neutron_bgpdr_agents.get_target_instances("foo")


class TestNeutronBGPDRAgents(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.wrapped = SingleObjectMock()
        self.csr = n_resources.NeutronBGPDRAgents(
            scheduling_keys=["foo", "bar"],
            wrapped_state=self.wrapped,
            component=unittest.mock.sentinel.component,
        )

    def test__does_node_require_maintenance_returns_false(self):
        class MockCtxVars:
            def get(self):
                return set(['node1', 'node3'])

        ctx_mock = MockCtxVars()

        ctx = unittest.mock.Mock()
        ctx.instance_data = {"node": "node2"}

        with unittest.mock.patch(
            "yaook.statemachine.resources.openstack.nodes_in_maintenance",
            new=ctx_mock
        ):
            self.assertEqual(
                False,
                self.csr._does_node_require_maintenance(ctx)
            )

    def test__does_node_require_maintenance_returns_true(self):
        class MockCtxVars:
            def get(self):
                return set(['node1', 'node2', 'node3'])

        ctx_mock = MockCtxVars()

        ctx = unittest.mock.Mock()
        ctx.instance_data = {"node": "node2"}

        with unittest.mock.patch(
            "yaook.statemachine.resources.openstack.nodes_in_maintenance",
            new=ctx_mock
        ):
            self.assertEqual(
                True,
                self.csr._does_node_require_maintenance(ctx)
            )


@ddt.ddt
class TestNeutronOVNAgent(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.neutron_ovn_agent = n_resources.TemplatedNeutronOVNAgent(
            template=sentinel.template,
        )

    @ddt.data(
            "bridgeConfig",
            "southboundServers",
            "imagePullSecrets",
            "resources",
            "issuerRef",
        )
    def test_needs_update_true_if_relevant_key_changes(self, key):
        self.assertTrue(self.neutron_ovn_agent._needs_update(
            {"spec": {key: sentinel.foo}},
            {"spec": {key: sentinel.bar}}
        ))

    def test_needs_update_false_if_nothing_relevant_changes(self):
        self.assertFalse(self.neutron_ovn_agent._needs_update(
            {"spec": {"cake": sentinel.foo}},
            {"spec": {"cake": sentinel.bar}}
        ))

    def test_needs_update_true_if_label_changes(self):
        self.assertTrue(self.neutron_ovn_agent._needs_update(
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.foo
                        }
                    },
                "spec": {}
            },
            {
                "metadata": {
                    "labels": {
                        "foo": sentinel.bar
                        }
                    },
                "spec": {}
            },
        ))
