#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail
resourcetype="$1"
shift
IFS=$'\n'
kubectl "$@" get "$resourcetype" -o name | while read -r name; do
    # name includes the resource type with -o name
    echo "Stripping finalizers of and deleting $name ..."
    kubectl "$@" delete "$name" --wait=false || true
    kubectl "$@" patch "$name" --patch '{"metadata": {"finalizers": []}}' --type merge || true
done
