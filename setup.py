#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from setuptools import setup, find_namespace_packages

setup(
    name="yaook-operators",
    version="0.0.1",
    packages=find_namespace_packages(include=["yaook.*"]),
    install_requires=[
        "kubernetes-asyncio==24.2.2",
        "jsonpatch==1.32",
        "Jinja2==3.1.2",
        "cryptography==38.0.4",
        "environ-config==22.1.0",
        "nose==1.3.7",
        "ddt==1.6.0",
        "openstacksdk==0.103.0",
        "oslo_config==9.0.0",
        "oslo_policy==4.0.0",
        "pyOpenSSL==22.1.0",
        "python-dxf==8.0.1",
        "semver==2.13.0",
        "pymysql==1.0.2",
        "pyyaml==6.0",
        "python-novaclient==18.2.0",
    ],
    include_package_data=True,
)
