.. _implementation_details.openstack_upgrades:

Openstack Upgrades
##################

Openstack upgrades are implemented as part of the normal operator statemachine.
Using special guard resources, individual resources can be configured to be skipped during upgrades or to be only executed during upgrades.

The reason for this implementation way is that we assume for now that most parts of the statemachine are actually the same across versions and during upgrades.
In case an upgrade procedure or a new openstack version has a completely different way of working this idea might not make sense.

Release definitions
===================

Each operator needs to define the releases it supports in a ``RELEASES`` array.
It contains the supported releases in an ordered way.
This ordering is also used to determine if a given ``targetRelease`` is a valid upgrade target.

Upgrade guard resources
=======================

The following two methods can be used as ``condition=`` parameter for ``yaook.statemachine.Optional`` and ``yaook.statemachine.OptionalKubernetesReference`` as a wrapper guard for the wrapped resources in case of upgrades.

yaook.statemachine.optional_only_upgrade
    Only executes when currently an upgrade is in progress. This will probably be mostly use for database migration jobs

yaook.statemachine.optional_non_upgrade
    Only executes when no upgrade is in progress. This is probably used for the initial db sync

To ensure appropriate ordering of running e.g. db migration jobs you will need to mark the above metioned resources as dependencies on appropriate places.
E.g. the db-premigration should be a dependency of the service api.
This will ensure that only after the premigration actually finishes the api pods are updated.

Docker image versions
=====================

In order to use different docker images based on the release of the service you can use a ``ReleaseAwareVersionedDependency``.
It maps the release name to a inner ``VersionedDependency``.

For an example you can check the keystone operator.
