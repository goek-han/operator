Containers
==========

User IDs
--------

In order to run unprivileged containers we need to assign uid's and gid's to the users.
To be able to use the users also across e.g. NFS shares the id's need to be stable/unique.
Services provided by OS packages (e.g. Apache) use their normal IDs for now.
The following table is used for the uid's and gid's we define.

===============     =======
User-/Groupname     ID
===============     =======
operator            2500000
keystone            2500001
ceilometer          2500002
cinder              2500003
glance              2500004
gnocchi             2500005
horizon             2500006
neutron             2500007
nova                2500008
libvirt             2500009
mariadb (*)         2500010
rabbitmq (*)        2500011
barbican            2500012
heat                2500013
tempest             2500014
ssl-terminator      2500015
ovn                 2500016
===============     =======

- (*): MariaDB and RabbitMQ might migrate to a random per-cluster assignment
  from a to-be-defined range in the future.


API-Container Naming
--------------------

To ease the generation and parsing of logs we use a standardized naming scheme for all api-container hosting an openstack api (within the api-pod).

The syntax is ``<api-name>-api``. Example for the Keystone-API is ``keystone-api``.
