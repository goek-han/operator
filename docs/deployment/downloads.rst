Downloads
=========

The direct downloads have been removed in favour of the Helm registry. The
official Yaook operator Helm charts are available at
https://charts.yaook.cloud/operator/stable/.

To add the repository to your Helm setup, run:

.. code-block::

    helm repo add yaook https://charts.yaook.cloud/operator/stable/

You can then install the Yaook CRDs using:

.. code-block::

    helm install -n default yaook-crds yaook/crds

.. note::

    CRDs are installed separately in order to be able to uninstall/reinstall
    operators without affecting all resources associated with them.

    In addition, this is not using the standard Helm mechanism to manage CRDs.
    However, the CRDs will not be deleted when the crd Helm chart is
    uninstalled, to avoid accidentally blowing up your entire cluster.

Further operators can be installed by their name (e.g.
``yaook/keystone-operator``).
