Custom Resource Status
======================

The status of the custom resources describes its current state and what the operators are currently doing.
It is a very valuable tool in troubleshooting.

The following is an example of a status indicating success:

.. code-block:: yaml

    status:
        conditions:
        - lastTransitionTime: "2021-09-02T06:08:14Z"
            lastUpdateTime: "2021-09-02T06:08:14Z"
            message: ""
            reason: Success
            status: "True"
            type: Converged
        - lastTransitionTime: "2021-08-30T11:48:39Z"
            lastUpdateTime: "2021-09-02T06:43:37Z"
            message: 0 resources in use
            reason: Success
            status: "True"
            type: GarbageCollected
        installedRelease: queens
        observedGeneration: 1
        phase: Updated

.. _custom_resource_status_phases:

Phases
------

Phases are the most generic description of the status of the resource.
It should be your first point when checking for issues.
The following phases exist:

Created
    The custom resource has been created and the operator has just become aware of it.
    This is an internal state and is never visible from kubernetes.

Updating
    The operator is currently running the reconcile loop for this resource.

WaitingForDependency
    The operator is waiting for some subresource to be ready (e.g. waiting for a certificate to be issued or a job to finish).
    This is a temporary state and the operator will go back to `Updating` once the resource is ready.
    The `message` in the status does contain the name of the component the operator is waiting for.

Updated
    The custom resource has been successfully reconciled.

BackingOff
    The operator encountered an issue (e.g. some part of the code raised an exception).
    It will retry the reconcilation with an exponential backoff.
    If the operator does not recover from this state take a look at the operator logs.

InvalidConfiguration
    The user has specified a configuration that is in some way invalid.
    The `reason` and `message` part of the status have more information.
    The user will need to update the requested configuration to solve this issue.

.. note:: Resources will not exhibit a clear error message but instead stay in `Updating` if the operator is killed or crashes hard while updating a resource.

.. _custom_resource_status_conditions:

Conditions
----------

Conditions are a more detailed view into individual parts of the operator.
Each resource generally has two conditions associated with it.
They can be separated by the `type` field.

Converged
    Describes the status of the reconcile loop of the operator.
    If a Phase from above indicates some issue this will contain more detailed information.

GarbageCollected
    The operator is regularly removing resources that are no longer needed (e.g. old configmaps).
    The status of this operation can be seen here

.. _custom_resource_status_observed_generation:

Observed Generation
-------------------

The `observedGeneration` contains the last generation (taken from `metadata`) the operator acted on.
This can be useful to check if a operator picked up a change to a resource at all.

.. _custom_resource_status_installed_release:

Installed Release
-----------------

Custom resources that have different releases available (e.g. Keystone with different openstack versions) have an additional status field.
`installedRelease` contains the last release that was reconciled successfully.
This means that the field will only be set when the resource reaches the `Updated` Phase.
