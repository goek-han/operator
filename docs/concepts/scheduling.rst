.. _concepts.scheduling:

Scheduling
##########

The scheduling of services spawned by Yaook is handled by the Kubernetes
Scheduler using Affinities and Tolerations. All services come with a set of
Affinities and Tolerations which allows fine-grained control over the placement
of the components.

To each scheduleable component a `Scheduling Key` is assigned. The Key is used as
key for both tolerations and labels. If a component has the key ``foo/bar``, it
will tolerate both ``NoExecute`` and ``NoSchedule`` taints *with that key* and
it will only run on nodes with that as label.

A notable exception is the Neutron Layer 2 Agent, which will schedule itself
based on other criteria (see :ref:`concepts.scheduling.neutron-l2-agent`
below).

In addition, a component may also listen to a `Scheduling Group Key`. In that
case, it will tolerate that key and it will also run on nodes which just have
a label matching that key.

.. note::

    This means that, on a fresh Kubernetes cluster, no services will be
    scheduled at all.

For details on the specific scheduling keys, please see
:class:`yaook.op.common.SchedulingKey`.

Neutron Metadata Agent
======================

The metadata agent is tightly coupled to both, L3 and DHCP. It also can run on
a single physical node multiple times without damage, provided proper
namespacing. Hence, it is included as a sidecar container in L3 and DHCP
Pods and not scheduled separately.

.. _concepts.scheduling.neutron-l2-agent:

Neutron L2 Agent
================

Neutron's Layer 2 Agent is a strict dependency of several other services, including
Nova Compute. In contrast to the Metadata agent, however, it can only run at
most once on each Kubernetes node, as it manages node-wide resources (network
interfaces).

Thus, it will tolerate *all* the keys of the following services (in addition to
the :attr:`.SchedulingKey.NETWORK_NEUTRON_L2_AGENT`) and
it will be spawned on all nodes matching any of their keys:

- Neutron L3 Agent: :attr:`.SchedulingKey.NETWORK_NEUTRON_L3_AGENT`
- Neutron DHCP Agent: :attr:`.SchedulingKey.NETWORK_NEUTRON_DHCP_AGENT`
- Nova Compute: :attr:`.SchedulingKey.COMPUTE_HYPERVISOR`

The use of a `ConfiguredDaemonSet` ensures that there exists at most one L2
Agent Pod per Kubernetes node.

Agents needing Neutron L2 (bgp, dhcp, l3, compute)
==================================================

Besides the `Scheduling Key` each component has (see
:class:`yaook.op.common.SchedulingKey`)
components that depend on l2 agent have another scheduling dependency.

The neutron-l2-operator sets an label to the node, that no l2 maintenance
is required. If this label is missing or set to maintenance required,
the agents/services won't be scheduled on or will be deleted from the node.

More details about this:
.. _concepts.safe-eviction.implementation.neutron-l2-agent:

Neutron BGP (DR)Agent
=====================

This agent is optional. To schedule it, the neutron deployment needs to have
the bgp section defined with one configname (will be used for naming the agent)
and configTemplate (can be empty nodeSelector).
