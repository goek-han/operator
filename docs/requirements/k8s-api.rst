.. _requirements.k8s-api:

Kubernetes API Requirements
###########################

This document describes the requirements Yaook imposes on the Kubernetes API
of a cluster. In contrast to :ref:`requirements.k8s-cluster`, this document is
focused on the Kubernetes API itself and discusses feature flags and version
requirements. For requirements on the services offered inside the Kubernetes
cluster, such as Ingress controllers, please see
:ref:`requirements.k8s-cluster` instead.

Kubernetes Versions
===================

Yaook supports all Kubernetes versions between 1.19 and 1.23.
Yaook is automatically tested with Kubernetes 1.23.

API Features
============

Pod security policies
---------------------

Pod security policies are NOT supported. They MUST NOT be enabled in
a cluster `in order for Yaook to work <https://gitlab.com/yaook/operator/-/issues/202>`_.


NodeRestriction admission controller
------------------------------------

The `NodeRestriction <https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#noderestriction>`_
admission controller SHOULD be enabled for all Yaook clusters and is REQUIRED
for SecuStack clusters.

Yaook relies on the ``NodeRestriction`` admission controller in order to
control the distribution of secrets.


Immutable Secrets and ConfigMaps
--------------------------------

The `ImmutableEphemeralVolumes <https://kubernetes.io/docs/concepts/configuration/secret/#secret-immutable>`_ feature gate MUST be enabled, as the operators make extensive use of immutable data.

It is enabled by default starting with Kubernetes 1.19.
