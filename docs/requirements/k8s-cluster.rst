.. _requirements.k8s-cluster:

Kubernetes Cluster Requirements
###############################

This document describes the requirements yaook/operator imposes on a Kubernetes Cluster.
In contrast to :ref:`requirements.k8s-api`, this document is focused on the
services and resources which need to be defined inside a cluster. For
requirements on the Kubernetes deployment itself, such as feature flags or
Kubernetes versions, please see :ref:`requirements.k8s-api` instead.

.. note::

    If your underlying kubernetes cluster will be deployed with yaook/k8s, we recommend configuring yaook/k8s to include all of the desired features of your cluster. This includes the following features, which are required by yaook/operator:
        1. `prometheus <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#prometheus-based-monitoring-configuration>`_
        #. `cert-manager <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#cert-manager-configuration>`_
        #. `ingress controller <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#nginx-ingress-controller-configuration>`_
        #. `calico plugin <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#network-configuration>`_

    and the following features, which are optional to yaook/operator:
        1. `ceph <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#rook-configuration>`_
        #. `load-balancing <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#configuring-load-balancing>`_


Nodes
=====

Hostname
--------

The hostname (including the domain part) of each node (as reported by ``hostname -f``) MUST be the same
as the name of the node object in kubernetes for the respective node.

Size
----

When deploying all yaook operators on a kubernetes cluster, it is recommended to have at least 4 worker nodes with 4 (v)cores and 8192 MiB RAM each and 3 master nodes with 2 (v)cores and 2048 MiB RAM each.


.. _ceph_node_requirements:

Node Software Requirements for Ceph
------------------------------------

If `ceph OSDs <https://rook.io/docs/rook/v1.6/ceph-osd-mgmt.html#ceph-osd-management>`_ should be deployed on a kubernetes node, ``lvm2`` needs to be installed on it.


Storage Classes
===============

The Kubernetes cluster MUST have a default storage class set. This storage
class MUST allow moving volumes between nodes.

.. note::

    In an upcoming release, the requirements will change to:

    - There MUST be a storage class which is host local (e.g. provided by the
      local-storage controller).
    - There MUST be a storage class which is not host local and can be moved
      between different nodes.


Networking
==========

CNI
---

The networking SHOULD be provided by Calico. Other network layers may work, but
are not officially supported and you are on your own.

.. note::

    See `yaook/k8s Network Plugin Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#network-configuration>`_.

Ingress
-------

The ``networking.k8s.io/v1`` API MUST be implemented by a proper ingress
controller.

.. note::

    See `yaook/k8s Ingress Controller Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#nginx-ingress-controller-configuration>`_.

Load Balancer
-------------

Yaook itself does not require an external load balancer, but it is likely that
your ingress controller (see above) will.

.. note::

    See `yaook/k8s Load-Balancer Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#configuring-load-balancing>`_.

Certificates
============

The ``cert-manager.io/v1`` API MUST be implemented.

.. note::

    See `yaook/k8s Cert-Manager Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#cert-manager-configuration>`_.

Monitoring
==========

The ``monitoring.coreos.com/v1`` Custom Resources MUST exist.
The usage of these resources (e.g. by the Prometheus Operator) is optional.

.. note::

    See `yaook/k8s Monitoring Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#prometheus-based-monitoring-configuration>`_.

Storage Provider
================

The following details additional storage requirements for individual services.

Cinder
------

Cinder supports Ceph and/or Netapp as a block storage backend.

Glance
------

Glance supports Ceph or PVCs (via the file backend) as a storage backend.
PVCs need to support ``ReadWriteMany`` as access method.

Gnocchi
-------

Gnocchi supports Ceph or S3 as a storage backend.

Ceph
----

If Ceph should be used with Yaook it is RECOMMENDED to deploy the ceph
cluster using `Rook.io <https://rook.io>`_. It is OPTIONAL to run it in
the same Kubernetes cluster as Yaook itself.
Furthermore, :ref:`these <ceph_node_requirements>` are the requirements
that Ceph is posing on the kubernetes nodes.

.. note::

    See `yaook/k8s Rook Configuration <https://yaook.gitlab.io/k8s/usage/cluster-configuration.html#rook-configuration>`_.

.. note::

    If your underlying cluster is created by yaook/k8s, we recommend to use yaook/k8s to install rook, since it automatically configures rook to create and attach volumes for rook to use as storage devices.
