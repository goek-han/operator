Glance
======

.. literalinclude:: glance.yaml

Glance using a file based backend
---------------------------------

Note that your selected Storageclass muss support `ReadWriteMany` Volumes

.. literalinclude:: glance_file.yaml

Specifying secrets in the configuration
---------------------------------------

To include confidential values in the configuration you can use `glanceSecrets`.
This allows use to reference an external secret which is included at a specific path in the configuration

.. literalinclude:: glance_config_secret.yaml
