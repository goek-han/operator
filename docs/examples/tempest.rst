Tempest
=======

With the following you can request a tempest run.
In this example it is just testing glance.

.. literalinclude:: tempest.yaml

Specifiying a test regex
------------------------

Alternatively you can specify a test regex yourself.

.. literalinclude:: tempest_regex.yaml

Specifying a list of tests to exclude
-------------------------------------

You may also additionally specify individual test names or regexes to exclude
by specifying them using the ``exclude`` list parameter.

.. literalinclude:: tempest_with_excludes.yaml
