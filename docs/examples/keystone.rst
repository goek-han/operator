Keystone
========

Configuration specifics
-----------------------

Note that for keystone there is a subtle connection between fernet key validity and the key rotation.

When issuing a new keystone token it is signed by the currently active fernet key.
To validate that token later the fernet key must still be in the list of active or old fernet keys.
Otherwise keystone will be unable to validate the token.
This may cause unexpected errors on the users of keystone if their token is invalid before its expiration date.

All of this can be adjusted by 3 parameters:
    * ``keystoneConfig.token.expiration`` sets the time in seconds that tokens issues by keystone are valid
    * ``keystoneConfig.fernet_tokens.max_active_keys`` the count of staged, active and old fernet keys before they are deleted.
    * ``keyRotationSchedule`` the schedule when the fernet key rotation takes place

The following inequality must hold to ensure no token expires before its designated date.
``keyRotationSeconds`` refers to the time in seconds between ``keyRotationSchedule`` invocations.

.. math::
    \frac{expiration}{keyRotationSeconds} \leq maxActiveKeys - 1


(We need to subtract one from ``max_active_keys`` as we can not count in the staged key)

.. note:: If you manually trigger the key rotation you need to adjust accordingly.
          We therefor recommend to choose ``max_active_keys`` larger then needed.

Example keystone deployment
---------------------------

.. literalinclude:: keystone.yaml

Specifying a custom public certificate
--------------------------------------

If you want to use a custom certificate for the public ingress you can set it as in the example here.

.. literalinclude:: keystone_cert_public.yaml

This works also for all other operators in the same way.

.. _examples_keystone_secret_config:

Specifying a secret in the configuration
----------------------------------------

To include confidential values in the configuration you can use `keystoneSecrets`.
This allows use to reference an external secret which is included at a specific path in the configuration

.. literalinclude:: keystone_config_secret.yaml

Referencing an external Keystone
--------------------------------

You can also reference an external Keystone deployment that is not created by the Keystone Operator.
This is especially usefully if you want to integrate with an existing deployment or share a Keystone between deployments.

.. literalinclude:: keystone_external.yaml

To reference an external Keystone in another custom resource use the following `keystoneRef`

.. code-block::

    keystoneRef:
        name: keystone-external
        kind: ExternalKeystoneDeployment
