.. _examples.neutron:

Neutron using ovs
=================

.. literalinclude:: neutron-ovs.yaml


Neutron using ovn
=================

.. literalinclude:: neutron-ovn.yaml

Further parameters
------------------

``spec:setup:ovs:l2:configTemplates:nodeSelectors[]:overlayNetworkConfig:ovs_local_ip_subnet``

The IP address configured via ``[ovs]/local_ip`` is used for the local
overlay (tunnel) network endpoint by the Neutron Open vSwitch agent. If not
specified in the ``NeutronDeployment`` resource the Kubernetes node IP address
is used. By specifying a subnet via
``spec:setup:ovs:l2:configTemplates:nodeSelectors[]:overlayNetworkConfig:ovs_local_ip_subnet``
the IP address of the corresponding interface of the node which has an IP
address for that subnet is used for ``[ovs]/local_ip`` instead.
