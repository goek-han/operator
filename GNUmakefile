# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DEPLOY_FILES=docs/examples/barbican.yaml docs/examples/ceilometer.yaml docs/examples/cinder.yaml docs/examples/glance.yaml docs/examples/gnocchi.yaml docs/examples/horizon.yaml docs/examples/neutron.yaml docs/examples/nova.yaml

helm_charts_all_in=$(patsubst %/values-template.yaml.j2,%,$(wildcard yaook/helm_builder/Charts/*/values-template.yaml.j2))
helm_charts_non_crds_in=$(filter-out yaook/helm_builder/Charts/crds,$(helm_charts_all_in))
helm_charts_all_out=$(patsubst %,%/values.yaml,$(helm_charts_all_in))
helm_charts_non_crds_out=$(patsubst %,%/values.yaml,$(helm_charts_non_crds_out))

cue_schema_dsts=$(patsubst %,yaook/op/cue/pkg/yaook.cloud/%_template,barbican ceilometer cinder glance heat keystone neutron neutron_bgp_dragent neutron_ml2 neutron_dhcp_agent neutron_openvswitch_agent neutron_l3_agent neutron_metadata_agent neutron_ovn_metadata_agent nova ironic ironic_inspector placement)

all: cue-templates helm-charts

cue-templates: $(cue_schema_dsts)

helm-charts: $(helm_charts_all_out)

$(helm_charts_all_out): %:
	python3 -m yaook.helm_builder -vvv $$(pwd)/$(patsubst %/values.yaml,%,$@)

k8s_deploy: k8s_apply
	kubectl -n "${YAOOK_OP_NAMESPACE}" apply -f deploy/selfsigned-issuer.yaml
	kubectl -n "${YAOOK_OP_NAMESPACE}" apply -f tools/mysqlclient.yaml
	kubectl -n "${YAOOK_OP_NAMESPACE}" apply -f tools/openstackclient.yaml

k8s_apply: k8s_helm_install_crds k8s_helm_install k8s_apply_examples

k8s_helm_install_crds: yaook/helm_builder/Charts/crds/values.yaml
	# This is kept separate to install/uninstall CRDs separately, as managing them may be
	# destructive.
	# force namespace to a constant because CRDs are exclusively global
	helm upgrade --install --namespace kube-system yaook-crds ./yaook/helm_builder/Charts/crds

k8s_helm_install: helm-charts
	for i in $(helm_charts_non_crds_in); do helm upgrade --install --namespace "${YAOOK_OP_NAMESPACE}" --set operator.image.tag=devel --set operator.image.pullPolicy=Always $$(basename $$i) $$(pwd)/$$i; done

k8s_apply_examples: | k8s_helm_install k8s_helm_install_crds
	for i in ${DEPLOY_FILES}; do kubectl -n "${YAOOK_OP_NAMESPACE}" apply -f $$i; done

k8s_clean: k8s_helm_uninstall k8s_helm_uninstall_crds k8s_clean_examples
	for i in amqpservers amqpusers barbicandeployments ceilometerdeployments cinderdeployments configureddaemonsets externalkeystonedeployments glancedeployments gnocchideployments heatdeployments horizondeployments keystonedeployments keystoneendpoints keystoneusers mysqlservices mysqlusers neutrondeployments novacomputenodes novadeployments sshidentities neutrondhcpagents neutronl2agents neutronl3agents neutronbgpdragents; do \
		./tools/strip-finalizers.sh "$$i" -n "${YAOOK_OP_NAMESPACE}"; \
	done
	kubectl -n "${YAOOK_OP_NAMESPACE}" delete -f tools/mysqlclient.yaml --ignore-not-found=true
	kubectl -n "${YAOOK_OP_NAMESPACE}" delete -f tools/openstackclient.yaml --ignore-not-found=true

k8s_helm_uninstall:
	for i in $(helm_charts_non_crds_in); do helm uninstall --namespace "${YAOOK_OP_NAMESPACE}" $$(basename $$i) || true; done

k8s_helm_uninstall_crds:
	helm uninstall --namespace kube-system yaook-crds || true

k8s_clean_examples:
	for i in ${DEPLOY_FILES}; do kubectl -n "${YAOOK_OP_NAMESPACE}" delete --ignore-not-found=true -f $$i; done

clean:
	rm -rf $(cue_schema_dsts)
	for i in $(helm_charts_in); do dir="./yaook/helm_builder/Charts/$$i"; rm -f -- "$dir"/values.yaml "$dir"/Chart.yaml "$dir"/templates/gen-*.yaml; done

$(cue_schema_dsts): yaook/op/cue/pkg/yaook.cloud/%_template:
	python3 ./buildcue.py $(patsubst yaook/op/cue/pkg/yaook.cloud/%_template,%,$@)

dump:
	@echo $(helm_charts_in)
	@echo $(helm_charts_out)
	@echo $(cue_schema_dsts)

.PHONY: clean dump $(helm_charts_all_out) k8s_helm_install k8s_helm_install_crds k8s_helm_uninstall k8s_helm_uninstall_crds helm-charts k8s_clean_examples k8s_apply_examples k8s_deploy k8s_apply k8s_clean
