#!/bin/bash
MATCH=$(for i in $(find yaook/op/ -mindepth 1 -maxdepth 1 -type d | cut -d / -f 3); do grep -R "^import yaook.op.\|^from yaook.op." "yaook/op/$i" | grep -E -v "(import|from) (yaook\.op\.common|yaook\.op\.tasks)"; done)
if [[ -n "$MATCH" ]]; then
    echo "The following issues where found:"
    echo ""
    echo "$MATCH"
    echo ""
    exit 1
else
    echo "No issues found"
fi