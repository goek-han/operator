#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import io
import sys
import oslo_config.generator
from oslo_config import types
import pathlib
import subprocess  # nosemgrep

CONFIG_NAMESPACES = {
    "barbican": [
        "barbican.certificate.plugin",
        "barbican.certificate.plugin.snakeoil",
        "barbican.common.config",
        "barbican.plugin.crypto",
        "barbican.plugin.crypto.p11",
        "barbican.plugin.crypto.simple",
        "barbican.plugin.dogtag",
        "barbican.plugin.secret_store",
        "keystonemiddleware.auth_token",
        "oslo.log",
        "oslo.messaging",
        "oslo.middleware.cors",
        "oslo.middleware.http_proxy_to_wsgi",
        "oslo.policy",
        "oslo.service.periodic_task",
        "oslo.service.sslutils",
        "oslo.service.wsgi",
    ],
    "cinder": [
        "castellan.config",
        "cinder",
        "keystonemiddleware.auth_token",
        "osprofiler",
        "oslo.config",
        "oslo.concurrency",
        "oslo.db",
        "oslo.log",
        "oslo.messaging",
        "oslo.middleware",
        "oslo.policy",
        "oslo.privsep",
        "oslo.reports",
        "oslo.service.periodic_task",
        "oslo.service.service",
        "oslo.service.sslutils",
        "oslo.service.wsgi",
        "oslo.versionedobjects",
    ],
    "glance": [
        "glance.api",
        "glance.store",
        "oslo.concurrency",
        "oslo.messaging",
        "oslo.db",
        "oslo.db.concurrency",
        "oslo.policy",
        "keystonemiddleware.auth_token",
        "oslo.log",
        "oslo.middleware.cors",
        "oslo.middleware.http_proxy_to_wsgi",
    ],
    "heat": [
        "heat.common.config",
        "heat.common.context",
        "heat.common.crypt",
        "heat.engine.clients.os.keystone.heat_keystoneclient",
        "heat.common.wsgi",
        "heat.engine.clients",
        "heat.engine.notification",
        "heat.engine.resources",
        "heat.api.aws.ec2token",
        "keystonemiddleware.auth_token",
        "oslo.messaging",
        "oslo.middleware",
        "oslo.cache",
        "oslo.log",
        "oslo.policy",
        "oslo.service.service",
        "oslo.service.periodic_task",
        "oslo.service.sslutils",
    ],
    "neutron": [
        "neutron",
        "neutron.agent",
        "neutron.db",
        "neutron.extensions",
        "nova.auth",
        "ironic.auth",
        "oslo.log",
        "oslo.db",
        "oslo.policy",
        "oslo.privsep",
        "oslo.concurrency",
        "oslo.messaging",
        "oslo.middleware.cors",
        "oslo.middleware.http_proxy_to_wsgi",
        "oslo.service.sslutils",
        "oslo.service.wsgi",
        "keystonemiddleware.auth_token",
    ],
    "neutron_ml2": [
        "neutron.ml2",
        "oslo.log",
    ],
    "neutron_metadata_agent": [
        "neutron.metadata.agent",
        "oslo.log",
        "oslo.cache",
    ],
    "neutron_ovn_metadata_agent": [
        "neutron.ovn.metadata.agent",
        "oslo.log",
        "oslo.cache",
    ],
    "neutron_openvswitch_agent": [
        "neutron.ml2.ovs.agent",
        "oslo.log"
    ],
    "neutron_bgp_dragent": [
        "bgp.agent",
    ],
    "neutron_dhcp_agent": [
        "neutron.az.agent",
        "neutron.base.agent",
        "neutron.dhcp.agent",
        "oslo.log"
    ],
    "neutron_l3_agent": [
        "neutron.az.agent",
        "neutron.base.agent",
        "neutron.l3.agent",
        "oslo.log"
    ],
    "nova": [
        "nova.conf",
        "oslo.log",
        "oslo.messaging",
        "oslo.policy",
        "oslo.service.periodic_task",
        "oslo.service.service",
        "oslo.db",
        "oslo.db.concurrency",
        "oslo.middleware",
        "oslo.concurrency",
        "keystonemiddleware.auth_token",
    ],
    "placement": [
        "placement.conf",
        "oslo.log",
        "oslo.policy",
        "oslo.middleware",
        "keystonemiddleware.auth_token",
    ],
    "keystone": [
        "keystone",
        "oslo.cache",
        "oslo.log",
        "oslo.messaging",
        "oslo.policy",
        "oslo.db",
        "oslo.middleware",
        "osprofiler"
    ],
    "ceilometer": [
        "ceilometer",
        "ceilometer-auth",
        "oslo.concurrency",
        "oslo.log",
        "oslo.messaging",
        "oslo.service.service",
    ],
    "ironic": [
        "ironic",
        "ironic_lib.disk_utils",
        "ironic_lib.disk_partitioner",
        "ironic_lib.exception",
        "ironic_lib.json_rpc",
        "ironic_lib.mdns",
        "ironic_lib.metrics",
        "ironic_lib.metrics_statsd",
        "ironic_lib.utils",
        "oslo.db",
        "oslo.messaging",
        "oslo.middleware.cors",
        "oslo.middleware.healthcheck",
        "oslo.middleware.http_proxy_to_wsgi",
        "oslo.concurrency",
        "oslo.policy",
        "oslo.log",
        "oslo.reports",
        "oslo.service.service",
        "oslo.service.periodic_task",
        "oslo.service.sslutils",
        "osprofiler",
        "keystonemiddleware.auth_token",
    ],
    "ironic_inspector": [
        "ironic_inspector",
        "ironic_lib.mdns",
        "keystonemiddleware.auth_token",
        "oslo.db",
        "oslo.log",
        "oslo.messaging",
        "oslo.middleware.cors",
        "oslo.middleware.healthcheck",
        "oslo.policy",
        "oslo.service.service",
        "oslo.service.sslutils",
        "oslo.service.wsgi",
    ],
}


def _flatmap(list):
    return [item for innerlist in list for item in innerlist]


def _genoptions(modulename: str):
    ns = CONFIG_NAMESPACES[modulename]
    groups = oslo_config.generator._get_groups(
        oslo_config.generator._list_opts(ns))
    out = {}
    for groupname, group in sorted(groups.items()):
        out[groupname] = []
        for options in group.values():
            if options and not isinstance(options, oslo_config.cfg.OptGroup):
                for option_source in options:
                    out[groupname].append(option_source[1])
        out[groupname] = _flatmap(out[groupname])
    return out


def _get_cue_type(groupname, optionname, oslotype, multi=False):
    if isinstance(oslotype, types.List):
        needs_global, type_ = _get_cue_type(
            groupname, optionname, oslotype.item_type)
        return needs_global, "[...%s]" % type_
    elif multi:
        # This needs to be before all other types, as it will otherwise never
        # match first
        needs_global, type_ = _get_cue_type(
            groupname, optionname, oslotype)
        return needs_global, "[...%s]" % type_
    elif isinstance(
            oslotype,
            types.String) or isinstance(
            oslotype,
            str.__class__):
        return False, "string"
    elif isinstance(oslotype, types.Boolean):
        return False, "bool"
    elif isinstance(oslotype, types.Integer):
        suffix = "int"
        if oslotype.min:
            suffix = "%s & >= %s" % (suffix, oslotype.min)
        if oslotype.max:
            suffix = "%s & <= %s" % (suffix, oslotype.max)
        return False, suffix
    elif isinstance(oslotype, types.Float):
        suffix = "(float | int)"
        if oslotype.min:
            suffix = "%s & >= %s" % (suffix, oslotype.min)
        if oslotype.max:
            suffix = "%s & <= %s" % (suffix, oslotype.max)
        return False, suffix
    elif isinstance(oslotype, types.HostAddress):
        return True, "global.#HostAddress"
    elif isinstance(oslotype, types.URI):
        return True, "global.#URI"
    elif isinstance(oslotype, types.IPAddress):
        return True, "global.#IPAddress"
    else:
        print(dir(oslotype))
        print(
            "WARNING: Option of type %s unsupported; group: %s; "
            "option: %s; option will be skipped..." %
            (oslotype, groupname, optionname))
        return False, "_"


def _writeoptions(options):
    output = io.StringIO()
    needs_global = False
    for groupname, group in sorted(options.items()):
        output.write('\t"%s"?: {\n' % groupname)
        for option in group:
            line = '"%s"' % option.name
            if not option.required:
                line = "%s?" % line
            line = "%s:\t" % line
            opt_needs_global, suffix = _get_cue_type(
                groupname,
                option.name, option.type, option.multi)
            needs_global = needs_global or opt_needs_global
            output.write("\t\t%s%s\n" % (line, suffix))
        output.write("\t}\n")
    return needs_global, output.getvalue()


def _writefile(modulename: str, options):
    basepath = pathlib.Path(
        f"yaook/op/cue/pkg/yaook.cloud/{modulename}_template"
    )
    basepath.mkdir(parents=True, exist_ok=True)
    with open(basepath / f"openstack_default_{modulename}.cue", "w") as fout:
        fout.write("package %s_template\n" % modulename)
        needs_global, opts = _writeoptions(options)
        if needs_global:
            fout.write("import (\"yaook.cloud/global\")\n")
        fout.write("%s_template_conf_spec: {\n" % modulename)
        fout.write(opts)
        fout.write("}\n")


def buildcue(modulename: str):
    options = _genoptions(modulename)
    _writefile(modulename, options)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        for module in CONFIG_NAMESPACES.keys():
            # This is to work around oslo config that does not expect to have
            # multiple overlaping configs (e.g. glance and nova) in the same
            # python process. Without this the same config key is defined
            # multiple times (from an oslo.config perspecitve)
            subprocess.run(  # nosemgrep
                [sys.executable, "buildcue.py", module])
    else:
        print("Generating config for: %s" % sys.argv[1])
        buildcue(sys.argv[1])
