#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -ex

TEST_DIR=$(dirname ${0})

# Releases to run tests on as an array
readarray -t RELEASES < <("${TEST_DIR}"/os_services.py upgradepath --initial "${INITIAL_RELEASE:?}" --target "${TARGET_RELEASE}")

# Openstack Services to install, also include lower versions so we have a full stack
INSTALL_SERVICES=$("${TEST_DIR}"/os_services.py installables --release "${INITIAL_RELEASE:?}" --include-lower-versions)

function retry {
  local max=3;
  local delay=5;
  for i in $(seq $max); do
    "$@" && return 0;
    if [[ $i -lt $max ]]; then
      echo "Command failed. Attempt $i/$max:";
      sleep $delay;
    fi
  done
  echo "The command has failed after $i attempts.";
  return 1;
}

helm repo add stable https://charts.helm.sh/stable 2>&1
helm repo add rook-release https://charts.rook.io/release 2>&1
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx 2>&1
helm repo add jetstack https://charts.jetstack.io 2>&1
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 2>&1
helm repo update

retry helm upgrade --install --namespace $NAMESPACE cert-manager\
  --set "installCRDs=true"\
  --version 1.9.1 \
  jetstack/cert-manager
retry helm upgrade --install --force --namespace $NAMESPACE rook-ceph\
  --set "csi.enableRbdDriver=false,csi.enableCephfsDriver=false"\
  --version v1.9.8 \
  rook-release/rook-ceph
retry helm upgrade --install --namespace $NAMESPACE ingress-nginx\
  --set "controller.extraArgs.enable-ssl-passthrough=true"\
  --version 4.2.0 \
  ingress-nginx/ingress-nginx
retry helm upgrade --install --namespace $NAMESPACE prometheus-operator\
  --set "alertmanager.enabled=false,grafana.enabled=false"\
  prometheus-community/kube-prometheus-stack

${TEST_DIR}/label-nodes.sh
# The networknodes must fit to the nodes selected in label-nodes.sh
network_nodes="$(kubectl get node -l 'network.yaook.cloud/neutron-bgp-dragent=true' -o 'jsonpath={ range .items[*] }{ .metadata.name }{ "\n" }{ end }')"
for node in $network_nodes; do
  sed "s/NODENAME/$node/" ${TEST_DIR}/setup-interfaces-job.yml | kubectl apply -n $NAMESPACE -f -
done

HELMFLAGS="--namespace $NAMESPACE --set operator.pythonOptimize=false --set operator.image.tag=$YAOOK_BUILD_VERSION --set operator.image.repository=$CI_REGISTRY_IMAGE/test"
# Install Helm Charts as generated previously
retry helm install --namespace $NAMESPACE yaook-crds ./Charts/crds-*.tgz
retry helm install $HELMFLAGS infra-operator ./Charts/infra-operator-*.tgz
retry helm install $HELMFLAGS cds-operator ./Charts/cds-operator-*.tgz
retry helm install $HELMFLAGS keystone-operator ./Charts/keystone-operator-*.tgz
retry helm install $HELMFLAGS keystone-resources-operator ./Charts/keystone-resources-operator-*.tgz
retry helm install $HELMFLAGS barbican-operator ./Charts/barbican-operator-*.tgz
retry helm install $HELMFLAGS ceilometer-operator ./Charts/ceilometer-operator-*.tgz
retry helm install $HELMFLAGS cinder-operator ./Charts/cinder-operator-*.tgz
retry helm install $HELMFLAGS glance-operator ./Charts/glance-operator-*.tgz
retry helm install $HELMFLAGS gnocchi-operator ./Charts/gnocchi-operator-*.tgz
retry helm install $HELMFLAGS heat-operator ./Charts/heat-operator-*.tgz
retry helm install $HELMFLAGS horizon-operator ./Charts/horizon-operator-*.tgz
retry helm install $HELMFLAGS neutron-operator ./Charts/neutron-operator-*.tgz
retry helm install $HELMFLAGS neutron-dhcp-operator ./Charts/neutron-dhcp-operator-*.tgz
retry helm install $HELMFLAGS neutron-l2-operator ./Charts/neutron-l2-operator-*.tgz
retry helm install $HELMFLAGS neutron-l3-operator ./Charts/neutron-l3-operator-*.tgz
retry helm install $HELMFLAGS neutron-bgp-operator ./Charts/neutron-bgp-operator-*.tgz
retry helm install $HELMFLAGS neutron-ovn-operator ./Charts/neutron-ovn-operator-*.tgz
retry helm install $HELMFLAGS nova-operator ./Charts/nova-operator-*.tgz
retry helm install $HELMFLAGS nova-compute-operator ./Charts/nova-compute-operator-*.tgz

for i in deploy/*.yaml; do
    retry kubectl apply -n $NAMESPACE -f "$i"
done

# Wait for Cert-Manager to be ready
echo "Waiting for cert-manager"
while kubectl get pods -n "$NAMESPACE" --selector=app.kubernetes.io/instance=cert-manager -o 'jsonpath={ range .items[*] }{ .metadata.name }: { .status.phase } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | grep False; do
  sleep 5
done

# Create a CA for the cert-manager
openssl genrsa -out ca.key 2048
openssl req -x509 -new -nodes -key ca.key -sha256 -days 3650 -out ca.crt -subj "/CN=YAOOK-CA"
retry kubectl -n $NAMESPACE create secret tls root-ca --key ca.key --cert ca.crt
retry kubectl -n $NAMESPACE apply -f docs/getting_started/ca-issuer.yaml
cp ca.crt "/etc/ssl/certs/$(openssl x509 -noout -hash -in ca.crt).0"
cat ca.crt >> /usr/local/lib/python3.10/site-packages/certifi/cacert.pem

# Deploy all custom resources
for i in realtime-hack rook-cluster rook-resources yaookdisruptionbudget; do
  kubectl apply -n $NAMESPACE -f ci/devel_integration_tests/deploy/${i}.yaml
done

# Deploy Openstack Services
while IFS="," read -r service release
do
  if [ -f ci/devel_integration_tests/deploy/${service}_${release}.yaml ]; then
    filename="ci/devel_integration_tests/deploy/${service}_${release}.yaml"
  else
    filename="ci/devel_integration_tests/deploy/${service}.yaml"
  fi
  sed "s/TARGET_RELEASE/${release}/" "$filename" | kubectl apply -n $NAMESPACE -f -
done <<< ${INSTALL_SERVICES}

pip3 install openstackclient python-openstackclient
openstack --version
openstack configuration show

# Download cirros Image for Glance Tests
cirros_version="0.5.1"
cirros_image="cirros-${cirros_version}-x86_64-disk.img"
cirros_url="http://download.cirros-cloud.net/${cirros_version}/${cirros_image}"
retry wget -ncv "${cirros_url}"

set +x

function eval_status_lines() {
    updating_allowed="${1:-0}"
    has_any=0
    is_ok=1
    # We do not `break` out of the loop but use flags in order to get the full
    # output to stdout always.
    while IFS= read -r line; do
        has_any=1
        # If there is a False in the line, it’s a problem for sure -> mark as
        # erroneous
        if ( ! test "$updating_allowed" = '1' || ! grep Updating <<<"$line" >/dev/null) && grep False <<<"$line" >/dev/null; then
            is_ok=0
        fi
        # If there is no status at all after the colon, the operator has not
        # yet picked up the resource, which is also not ok.
        if ! grep -P '^[^:]+: \w+' <<<"$line" >/dev/null; then
            is_ok=0
        fi
        # The generation is not yet updated, probably the operator did not yet
        # pick up the change.
        generation=$(echo $line | awk '{print $3}')
        observedGeneration=$(echo $line | awk '{print $4}')
        if [ "$generation" != "$observedGeneration" ]; then
            is_ok=0
        fi
        # Echo the input to stdout for debugging
        echo "$line"
    done
    if [ "$has_any" = 0 ] || [ "$is_ok" = 0 ]; then
        return 1
    fi
    return 0
}

function _test_cinder() {
  openstack volume service list
  id=$(openstack volume create --size 10 testvolume -f value -c id)
  ok=false
  for i in $(seq 1 60); do
    status="$(openstack volume show "$id" -f value -c status)"
    if [[ "$status" = 'available' ]]; then
      ok=true
      break
    fi
    if [[ "$status" = 'error' ]]; then
      echo "volume failed"
      openstack volume show "$id"
      exit 1
    fi
    echo "$status"
    sleep 1
  done
  if [[ "$ok" != 'true' ]]; then
    echo "volume timed out"
    openstack volume show "$id"
    exit 1
  fi
}

function _test_glance() {
  # Glance Image Upload Test
  glance import-info
  openstack image create cirros-file-upload --progress --min-ram 512 --public --disk-format qcow2 --min-disk 1 --file "${cirros_image}"
}

function wait_and_test() {
  for cr in keystonedeployments glancedeployments novadeployments barbicandeployments cinderdeployments neutrondeployments horizondeployments keystoneusers keystoneendpoints ceilometerdeployments gnocchideployments novacomputenodes bgpdragents ovnagents heatdeployments; do
      # skip if a deployment is not installed
      [[ $(kubectl -n $NAMESPACE get $cr -o json | jq '.items | length') -lt 1 ]] && continue

      echo
      echo "Waiting for $cr to get ready ..."
      echo
      updating_allowed=0
      if [ "$cr" = 'novacomputenodes' ]; then
          # the nova-compute-operator is in what looks like a loop in the CI
          # cluster because the watches get terminated quicker than it can finish
          # updating all nodes (also because of other watches getting terminated)
          updating_allowed=1
      fi
      max_wait=$((60 * 20))
      if [ "$cr" = 'keystonedeployments' ]; then
          # The MySQLService needs about 3-4 minutes per container during the
          # upgrade from (10.2 -> 10.6). For Keystone we use 3 replicas.
          # With 3 replicas this is 3 * 4 = 12 minutes per step by step update
          # therefore for Keystone we need 4 * 12 minutes = 48 minutes
          # minimum plus some buffer we take 60 minutes.
          max_wait=$((60 * 60))
      fi
      SECONDS=0
      while ! kubectl get "$cr" -n "$NAMESPACE" -o 'jsonpath={ range .items[*] }{ .metadata.name }: { .status.phase } { .metadata.generation } { .status.observedGeneration } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | eval_status_lines $updating_allowed; do
          sleep 5
          if [[ $SECONDS -gt $max_wait ]]; then
            echo "Time Limit reached waiting for $cr."
            exit 1
          fi
      done
  done

  echo
  echo "Waiting for Pods to get ready ..."
  echo
  while kubectl get pods -n "$NAMESPACE" -o 'jsonpath={ range .items[?(@.metadata.ownerReferences[*].kind!="Job")] }{ .metadata.name }: { .status.phase } { .status.conditions[*].status } { .status.conditions[*].message }{ "\n" }{ end }' | grep False; do
      sleep 5
  done

  ingress_ip="";
  while [ -z $ingress_ip ]; do
    echo "Waiting for public ip of ingress...";
    ingress_ip=$(kubectl -n $NAMESPACE get svc ingress-nginx-controller --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}");
    [ -z "$ingress_ip" ] && sleep 10
  done
  echo "Ingress ready at ${ingress_ip}"
  for i in keystone barbican nova glance cinder neutron gnocchi horizon heat vnc; do
    echo "${ingress_ip} ${i}" >> /etc/hosts
    echo "${ingress_ip} ${i}.yaook.cloud" >> /etc/hosts
  done

  eval "$(./tools/download-os-env.sh public -n $NAMESPACE)"

  while ! curl -v "$OS_AUTH_URL"; do echo "waiting for keystone api to come up"; sleep 15; done

  kubectl -n $NAMESPACE get pods

  openstack token issue -vvv
  openstack endpoint list -vvv

  endpoints=$(openstack endpoint list -f value --interface public -c "Service Name")

  # Loop through endpoints and run basic tests
  for e in $endpoints
  do
    case $e in
      barbican)
        openstack secret list -vvv
        ;;
      glance)
        openstack image list -vvv
        _test_glance
        ;;
      neutron)
        openstack network list -vvv
        openstack network agent list -f json | jq .
        # check that some agent is up - this is needed to see, if ovn-controller are up
        while ! openstack network agent list -f json | jq -e .[0].State | grep -Eiq "(UP|true)"; do echo "waiting for network agents to come up"; sleep 15; done
        if [ ${RELEASES[@]} == "yoga" ]; then
          # check to also see the propagation of information from the ovn-controllers to the neutron-api work
          while ! openstack network agent list | grep -Eiq "OVN Controller Gateway agent"; do echo "waiting for ovn gateways to come up"; sleep 15; done
        else
          while ! openstack network agent list --agent-type bgp | grep -q UP; do echo "waiting for bgpdragents to come up"; sleep 15; done
        fi
        ;;
      heat)
        openstack stack list -vvv
        ;;
      nova)
        openstack server list -vvv
        openstack hypervisor list
        while ! openstack hypervisor list | grep -q up > /dev/null; do echo "waiting for hypervisors to come up"; sleep 15; done
        openstack hypervisor list
        openstack compute service list
        retry curl https://vnc.yaook.cloud -v
        ;;
      cinderv3)
        openstack volume list -vvv
        _test_cinder
        ;;
      gnocchi)
        gnocchi resource list
        ;;
      keystone)
        ;;
      *)
        echo "Test for $e not implemented"
    esac
  done

  # Check if mysqlservice backups are working
  SECONDS=0
  while [[ ! $(kubectl exec -n $NAMESPACE $(kubectl get po -n $NAMESPACE --selector=state.yaook.cloud/component=database -o json | jq -r '.items | .[] | .metadata | select(.name|test(".*keystone.*-db-0.*")) | .name') --container backup-creator -- ls /backup/warm) ]] ; do
    sleep 10
    if [[ $SECONDS -gt 360 ]]; then
      echo 'No mysqlservice backups are present. Please check if backups are still working'
      exit 1
    fi
  done

  # Checking if yaookdisruptionbudgets are updated correctly
  kubectl get yaookdisruptionbudgets -n $NAMESPACE test-disruption-budget -o json | jq -e ".status.nodes[]"
  if kubectl get yaookdisruptionbudgets -n $NAMESPACE test-disruption-budget -o json | jq -e ".status.nodes[] | .type" | grep -q NeutronDeployment-l2_agents; then
    disruptionbudgettypes=("NovaDeployment-compute_nodes" "NeutronDeployment-l2_agents" "NeutronDeployment-l3_agents" "NeutronDeployment-dhcp_agents");
  else
    disruptionbudgettypes=("NovaDeployment-compute_nodes" "NeutronDeployment-ovn_agents");
  fi
  for type in "${disruptionbudgettypes[@]}"; do
    echo $type
    kubectl get yaookdisruptionbudgets -n $NAMESPACE test-disruption-budget -o json | jq -e ".status.nodes[] | select(.type == \"$type\") | .configuredInstances > 0"
  done

  [[ $(kubectl -n $NAMESPACE get horizondeployment -o json | jq '.items | length') -gt 0 ]] && retry curl https://horizon.yaook.cloud -v
}

function upgrade_deployments() {
  release=${1}
  echo "$(date --iso-8601=seconds) - NOW RUNNING UPGRADE TO ${release} + TESTS"
  for service in $(${TEST_DIR}/os_services.py upgradables --release ${release}) ; do
    kubectl -n $NAMESPACE patch ${service}deployments ${service} --type='json' --patch '[{"op":"replace", "path":"/spec/targetRelease", "value":"'${release}'"}]'
  done
}

# Wait for and test initial deployments
wait_and_test

# Now perform and test upgrades of the deployments.
for next_release in "${RELEASES[@]:1}"; do
  upgrade_deployments $next_release
  wait_and_test
done
