#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -xe

# Since spawning a new cluster takes a while, we will most likely not be
# using the cluster we spawn here. Instead, we later claim a cluster that
# was already spawned by a previous run.
"$(dirname "$0")/spawn-cluster.sh"

pip install -e .
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

export KUBECONFIG=${GARDENER_ACCESS_KUBECONFIG}

CLAIMED_CLUSTER=""
while [ -z "${CLAIMED_CLUSTER}" ]; do
    # Check if any ready and unclaimed cluster exists
    shoots=$(kubectl get shoots -l 'shoot.gardener.cloud/status=healthy,!yaook.cloud/integrationtest' -o json | jq -r '.items[] | select(.status.lastOperation.progress == 100) | .metadata.name')
    if [ -z "${shoots}" ]; then
        # no ready and unclaimed cluster. sleep and retry
        echo "No available clusters, retrying in 30s ..."
        sleep 30
        continue
    fi

    # Try to claim a random free cluster
    shootname=$(echo "${shoots}" | shuf | head -1)
    echo "Trying to claim shoot ${shootname} ..."
    if ! kubectl label shoots "${shootname}" "yaook.cloud/integrationtest=${CI_JOB_ID}"; then
        echo "Lost race for shoot ${shootname}, trying again later."
        continue
    fi
    while true; do
        label_value="$(kubectl get shoot ${shootname} -o jsonpath="{ .metadata.labels['yaook\.cloud/integrationtest'] }" || echo 'failed to fetch')"
        if [ "x$label_value" = "x${CI_JOB_ID}" ]; then
            CLAIMED_CLUSTER="${shootname}"
            echo "Successfully claimed shoot ${CLAIMED_CLUSTER}"
            break
        elif [ -z "$label_value" ]; then
            echo "Waiting for label to appear on ${shootname} ..."
            sleep 1
        else
            echo "Failed to claim cluster: claim label reads as ${label_value}"
            break
        fi
    done
    if [ -z "${CLAIMED_CLUSTER}" ]; then
        echo "Lost race for cluster, retrying in 15s ..."
        sleep 15
        continue
    fi
    kubectl label --overwrite shoots "${CLAIMED_CLUSTER}" "yaook.cloud/testtime=$(date +%s)"
done

# Save CLAIMED_CLUSTER so we can cleanup later
echo "${CLAIMED_CLUSTER}" > /tmp/claimed_cluster

# Build kubeconfig for the claimed cluster
export CLAIMED_CLUSTER
kubectl get secret "${CLAIMED_CLUSTER}.kubeconfig" --template={{.data.kubeconfig}} | base64 -d > /tmp/claimed.kubeconfig
export KUBECONFIG=/tmp/claimed.kubeconfig

timeout 90m "$(dirname "$0")/run-tests.sh"
