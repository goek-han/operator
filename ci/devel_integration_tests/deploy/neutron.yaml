##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: yaook.cloud/v1
kind: NeutronDeployment
metadata:
  name: neutron
spec:
  keystoneRef:
    name: keystone
  region:
    name: YaookRegion
  novaRef:
    name: nova
  database:
    replicas: 1
    storageClassName: premium-perf4-stackit
    storageSize: 8Gi
    proxy: {}
    backup:
      schedule: "0 0 * * *"
  api:
    replicas: 1
    ingress:
      fqdn: "neutron.yaook.cloud"
      port: 443
  neutronConfig:
    DEFAULT:
      debug: True
  neutronML2Config:
    ml2_type_flat:
      flat_networks:
        - physnet1
  messageQueue:
    # We use 3 replicas here to test rabbitmq clustering
    replicas: 3
    storageClassName: premium-perf4-stackit
    storageSize: 5Gi
  policy:
    "context_is_admin": "role:admin"
  issuerRef:
    name: "ca-issuer"
  setup:
    ovs:
      bgp:
        test-bgp:
          configTemplates:
          - bgpInterfaceMapping:
              bridgeName: br-bgp
            neutronBGPDRAgentConfig:
              DEFAULT:
                debug: true
                use_json: false
            nodeSelectors:
            - matchLabels:
                network.yaook.cloud/neutron-l3-agent: "true"
      l2:
        configTemplates:
          - nodeSelectors:
            - matchLabels: {}  # matches all nodes
            neutronOpenvSwitchAgentConfig:
              DEFAULT:
                debug: True
          - bridgeConfig:
            - bridgeName: br-bgp
              uplinkDevice: fake1
            nodeSelectors:
            - matchLabels:
                network.yaook.cloud/neutron-l3-agent: "true"
        resources:
          ovs-vswitchd:
            requests:
              memory: 100Mi
              cpu: 100m
      dhcp:
        configTemplates:
          - nodeSelectors:
            - matchLabels: {}  # matches all nodes
            neutronDHCPAgentConfig:
              DEFAULT:
                debug: True
        resources:
          neutron-dhcp-agent:
            requests:
              memory: 100Mi
              cpu: 100m
          neutron-metadata-agent:
            requests:
              memory: 100Mi
              cpu: 100m
      l3:
        configTemplates:
          - nodeSelectors:
            - matchLabels: {}  # matches all nodes
            neutronL3AgentConfig:
              DEFAULT:
                debug: True
              agent:
                debug_iptables_rules: True
  targetRelease: TARGET_RELEASE
