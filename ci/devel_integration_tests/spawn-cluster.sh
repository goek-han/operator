#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -xe
apt-get update
# debianutils: which
apt-get install -o Apt::InstallRecommends=0 -y apt-transport-https curl jq openssl ca-certificates debianutils
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubectl jq

export KUBECONFIG=${GARDENER_ACCESS_KUBECONFIG}
# Hack: use the last 5 digits of the job id as a arbitrary and hopefully unique
# id for this cluster. This cluster has NO special relation to this CI job.
NEW_CLUSTERNAME="${CI_JOB_ID:(-5)}"
cat "$(dirname "$0")/shootcluster.yml" | sed -e "s/replacethisname/${NEW_CLUSTERNAME}/" | sed -e "s/replacethisnumber/$(($RANDOM % 3 + 1))/" | kubectl apply -f -
