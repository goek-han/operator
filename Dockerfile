##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM golang:1.19@sha256:dc76ef03e54c34a00dcdca81e55c242d24b34d231637776c4bb5c1a8e8514253 AS cue-builder
WORKDIR /build
RUN GOPATH=/build go install cuelang.org/go/cmd/cue@v0.4.3

FROM debian:11.5-slim@sha256:98de137b613dfc97f6b1eaa2f2d0a167eec4c5b72e2a34fd215aa51c3dcc3a86 AS python
RUN \
	apt-get update && \
	apt-get install python3-minimal python3-pip wget --no-install-recommends -y && \
	apt-get clean -y

FROM python:3.10 AS cue-default-builder
RUN \
	apt-get update && \
	apt-get install python3-dev gcc make --no-install-recommends -y
WORKDIR /build
COPY requirements-build.txt /build/
RUN pip3 install -r requirements-build.txt
# TODO: When we support deploying other openstack releases than
# queens, comment in the following two lines to generate the
# default policy files.
#COPY generate_default_policies.py .
#RUN python3 generate_default_policies.py
COPY buildcue.py GNUmakefile /build/
RUN make cue-templates

FROM python:3.10
ARG userid=2020
RUN \
	groupadd -g $userid yaook-operator && \
	useradd -u $userid -g $userid -d /tmp/yaook-operator -m yaook-operator
RUN \
	apt-get update && \
	apt-get install iputils-ping --no-install-recommends -y && \
	apt-get clean -y
COPY yaook /app/yaook
COPY setup.py MANIFEST.in /app/
RUN set -eux; cd /tmp ;\
    wget -O helm.tar.gz https://get.helm.sh/helm-v3.5.0-linux-amd64.tar.gz ;\
    echo '3fff0354d5fba4c73ebd5db59a59db72f8a5bbe1117a0b355b0c2983e98db95b helm.tar.gz' > helm.tar.gz.sha256sum ;\
    sha256sum -c helm.tar.gz.sha256sum ;\
    tar -xf helm.tar.gz ;\
    mv linux-amd64/helm /bin/helm ;\
    rm -f helm.tar.gz helm.tar.gz.sha256sum
COPY --from=cue-builder /build/bin/cue /bin/cue
COPY --from=cue-default-builder /build/yaook/op /app/yaook/op
RUN cd /app && pip install . && rm -rf -- /root/.cache

ENTRYPOINT ["python3", "-m", "yaook.op"]
