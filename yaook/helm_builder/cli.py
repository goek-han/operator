#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from yaook.helm_builder import helmutils
from yaook.common import configure_logging
import argparse
import os
import pathlib
import typing
import logging
import subprocess
import sys

import yaml

import yaook.common.config


GENERATED_ROLES_FILENAME = "gen-roles.yaml"


def generate_roles(
        logger: logging.Logger,
        spec: typing.Mapping[str, typing.Any],
        outfile: pathlib.Path) -> None:
    operators = spec["operators"]
    name_template = spec["name_template"]

    roles = []
    for op in operators:
        logger.debug("Generating Role and ClusterRole for %s", op)
        op_roles = subprocess.check_output(
            [sys.executable, "-m", "yaook.op", op, "generate-roles"],
            encoding="utf-8",
        )
        op_roles = op_roles.replace(
            f"name: {op}-operator",
            # not using .format() here to not make things painful with
            # including go templates in that string
            f"name: {name_template.replace('{op}', op)}",
        )
        roles.append(op_roles)

    logger.info("Writing roles for %s to %s", operators, outfile)
    with outfile.open("w") as f:
        f.writelines(roles)


def build_cue_templates(
        logger: logging.Logger,
        chartpath: pathlib.Path) -> None:
    outdir = chartpath / "templates"
    for entry in outdir.iterdir():
        if not entry.is_file():
            continue
        if not entry.name.startswith("gen-cue-"):
            continue
        if not entry.name.endswith(".yaml"):
            continue
        logger.debug("Deleting old generated file %s", entry)
        entry.unlink()

    tpldir = chartpath / "cue-templates"
    if not tpldir.is_dir():
        logger.info("No CUE templates for this chart, skipping CUE generation")
        return

    ntemplates = 0
    for tpl in tpldir.iterdir():
        if not tpl.is_file():
            continue
        if not tpl.name.endswith(".cue"):
            continue
        outname = outdir / f"gen-cue-{tpl.name}.yaml"
        logger.debug(
            "Rendering CUE template %s -> %s",
            tpl,
            outname,
        )
        data = yaook.common.config.render_cue_template(
            tpl,
            pathlib.Path(__file__).parent /
            "cue" / "pkg",
        )
        with outname.open("w") as f:
            yaml.dump(data, f)
        ntemplates += 1

    logger.info("Rendered %d CUE templates", ntemplates)


def build_chart(logger: logging.Logger,
                path: pathlib.Path,
                operator_version: str,
                image_versions: typing.Mapping[str, str],
                ) -> typing.Optional[typing.Tuple[pathlib.Path, str]]:
    chartname = path.name
    logger.info(f"Creating Helm-Chart {chartname}")

    try:
        with (path / "MANIFEST.yaml").open("r") as f:
            manifest = yaml.safe_load(f)
    except FileNotFoundError:
        manifest = {}

    if not manifest.get("build", True):
        logger.info("Chart %s is not to be built", chartname)
        return None

    generated_roles_file = path / "templates" / GENERATED_ROLES_FILENAME
    try:
        generate_roles_spec = manifest["generate_roles"]
    except KeyError:
        try:
            generated_roles_file.unlink()
        except FileNotFoundError:
            pass
    else:
        generate_roles(logger, generate_roles_spec, generated_roles_file)

    build_cue_templates(logger, path)

    helmutils.create_helm_chart_yaml_file(
        path,
        {
            "version": operator_version,
        },
    )
    helmutils.create_helm_values_yaml_file(
        path,
        {
            "version": operator_version,
            "images": image_versions,
        },
    )

    return helmutils.create_helm_archive(
        path,
        chartname,
        operator_version,
    )


def get_ci_repo_path() -> str:
    return (
        "https://gitlab.com/api/v4/projects/"
        f"{os.environ['CI_PROJECT_ID']}/packages/"
        "helm/api/stable/charts"
    )


def get_ci_auth() -> str:
    project_id = os.getenv('CI_PROJECT_ID')
    if not project_id:
        raise ValueError("Upload requested, but CI_PROJECT_ID is not set")

    username = os.getenv("GITLAB_USERNAME")
    token = os.getenv("CI_JOB_TOKEN")

    if username and token:
        return f"{username}:{token}"

    raise RuntimeError(
        "Upload requested, but no credentials available!"
        " Set CI_JOB_TOKEN and optionally GITLAB_USERNAME"
    )


def build_charts(
        chart_dirs: typing.Collection[pathlib.Path],
        upload: bool,
        ) -> None:

    logger = logging.getLogger(__name__)
    logger.debug(f"Running {__name__}")

    if upload:
        auth = get_ci_auth()
        logger.info("Found gitlab_username and gitlab_token. "
                    "Using username:token for authentication")
        repo_path = get_ci_repo_path()
        logger.debug(f"Helm registry used: {repo_path}")
    else:
        auth = None

    image_versions = helmutils.get_container_image_version()
    operator_version = helmutils.get_operator_repo_version()

    for helm_chart in chart_dirs:
        file_info = build_chart(
            logger,
            helm_chart,
            operator_version,
            image_versions,
        )
        if file_info is None:
            # TODO: would be nice to throw this as an error if the user
            # explicitly requested this chart to be built
            continue

        if auth is not None:
            helmutils.upload_helm_chart(file_info[0],
                                        file_info[1],
                                        repo_path,
                                        auth)


def chartpath(s: str) -> pathlib.Path:
    return helmutils.get_chart_root() / pathlib.Path(s)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        dest="verbosity",
        action="count",
        default=0,
        help="Increase verbosity (up to -vvv)"
    )
    parser.add_argument(
        "--debug-libraries",
        action="store_true",
        default=False,
        help="Let verbosity also affect third-party libraries",
    )

    parser.add_argument(
        "--ci-upload", "-U",
        dest="upload",
        action="store_const",
        const="ci",
        default=None,
        help="Upload charts to the remote repository using CI credentials",
    )
    parser.add_argument(
        "--all", "-a",
        dest="do_all",
        action="store_true",
        default=False,
        help="Process all charts",
    )
    parser.add_argument(
        "charts",
        nargs="*",
        metavar="PATH",
        help="The path of the charts to process",
        type=chartpath,
    )

    args = parser.parse_args()

    configure_logging(args.verbosity, args.debug_libraries)

    if args.do_all:
        args.charts.extend(helmutils.get_helm_chart_directories())
        # deduplicate
        args.charts = list(sorted(set(args.charts)))

    if not args.charts:
        print(
            "no charts given! either specify a list of charts or run with "
            "-a to build all charts",
            file=sys.stderr,
        )
        sys.exit(2)

    build_charts(args.charts, args.upload is not None)
