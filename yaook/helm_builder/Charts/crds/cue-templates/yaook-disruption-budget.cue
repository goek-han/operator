import (
	"yaook.cloud/crd"
)

crd.#crd
{
	#group:    "yaook.cloud"
	#kind:     "YaookDisruptionBudget"
	#plural:   "yaookdisruptionbudgets"
	#singular: "yaookdisruptionbudget"
	#shortnames: ["ydb", "ydbs"]
	#schema: properties: spec: {
		type: "object"
		required: ["nodeSelectors", "maxUnavailable"]
		properties: {
			nodeSelectors: {
				type: "array"
				items: {
					type: "object"
					required: ["matchLabels"]
					properties: matchLabels: {
						type: "object"
						additionalProperties: type: "string"
					}
				}
			}
			maxUnavailable: {
				"x-kubernetes-int-or-string": true
				pattern:                      "^(100|[0-9]{1,2})%$"
				minimum:                      0
			}
		}
	}
	#schema: properties: status: {
		type: "object"
		properties: {
			nodes: {
				type:                     "array"
				"x-kubernetes-list-type": "map"
				"x-kubernetes-list-map-keys": ["type"]
				items: {
					type: "object"
					required: [
						"configuredInstances",
						"existingInstances",
						"updatedInstances",
						"readyInstances",
						"availableInstances",
						"lastUpdateTime",
						"type",
					]
					properties: {
						configuredInstances: type: "integer"
						existingInstances: type:   "integer"
						updatedInstances: type:    "integer"
						readyInstances: type:      "integer"
						availableInstances: type:  "integer"
						lastUpdateTime: {
							type:    "string"
							pattern: "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z"
						}
						type: {
							type: "string"
						}
					}
				}
			}
		}
	}
}
