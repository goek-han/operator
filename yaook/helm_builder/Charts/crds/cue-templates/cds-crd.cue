// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#operatorcrd
{
	#group:    "apps.yaook.cloud"
	#kind:     "ConfiguredDaemonSet"
	#plural:   "configureddaemonsets"
	#singular: "configureddaemonset"
	#shortnames: ["cds"]
	#schema: properties: spec: {
		required: [
			"volumeTemplates",
			"targetNodes",
			"template",
		]
		properties: {
			volumeTemplates: {
				type: "array"
				items: {
					type: "object"
					required: ["nodeMap", "volumeName"]
					properties: {
						volumeName: type: "string"
						nodeMap: {
							type: "object"
							additionalProperties: {
								type: "object"
								required: ["template"]
								properties: template: crd.#anyobj
							}
						}
						default: {
							type: "object"
							required: ["template"]
							properties: template: crd.#anyobj
						}
					}
				}
			}
			targetNodes: {
				type: "array"
				items: type: "string"
			}
			template: {
				type: "object"
				required: ["spec"]
				properties: {
					metadata: crd.#anyobj
					spec:     crd.#anyobj
				}
			}
			updateStrategy: {
				type: "object"
				properties: {
					type: {
						type: "string"
						enum: ["rollingUpdate"]
						default: "rollingUpdate"
					}
					rollingUpdate: {
						type: "object"
						properties: maxUnavailable: {
							type:    "integer"
							minimum: 1
							default: 1
						}
					}
				}
			}
		}
	}
	#schema: properties: status: properties: {
		currentNumberScheduled: type: "integer"
		numberAvailable: type:        "integer"
		numberReady: type:            "integer"
		updatedNumberScheduled: type: "integer"
	}
	#additionalprintercolumns: [
		{
			name:        "Up-to-date"
			type:        "integer"
			description: "Number of scheduled up-to-date pods"
			jsonPath:    ".status.updatedNumberScheduled"
		},
		{
			name:        "Ready"
			type:        "integer"
			description: "Number of ready pods"
			jsonPath:    ".status.numberReady"
		},
		{
			name:        "Scheduled"
			type:        "integer"
			description: "Number of scheduled pods"
			jsonPath:    ".status.currentNumberScheduled"
		},
		{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "Message"
			type:        "string"
			description: "Informative messages"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].message"
		},
	]
}
