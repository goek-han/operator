// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#region

crd.#openstackcrd
{
	#group:    "compute.yaook.cloud"
	#kind:     "NovaComputeNode"
	#plural:   "novacomputenodes"
	#singular: "novacomputenode"
	#shortnames: ["computenode", "computenodes"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"novaConfig",
			"cephBackend",
			"publicKeysSecretRef",
			"caConfigMapName",
			"messageQueue",
			"targetRelease",
		]
		properties: {
			keystoneRef:         crd.#keystoneref
			publicKeysSecretRef: crd.#ref
			caConfigMapName: type: "string"
			novaConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			messageQueue: {
				type: "object"
				required: ["amqpServerRef"]
				properties: {
					amqpServerRef: crd.#ref
				}
			}

			cephBackend: {
				type: "object"
				required: ["enabled"]
				properties: {
					enabled: type:           "boolean"
					user: type:              "string"
					keyringSecretName: type: "string"
					uuid: {
						type:    "string"
						pattern: "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"
					}
					cephConfig: crd.#anyconfig
				}
			}
			state: {
				type:    "string"
				default: "Enabled"
				enum: [
					"Enabled",
					"Disabled",
					"DisabledAndCleared",
				]
			}

			ids: {
				type: "object"
				properties: {
					cinderGid: type: "integer"
				}
			}
			vnc: {
				type: "object"
				required: ["issuerRef", "baseUrl"]
				properties: {
					issuerRef: crd.#issuerspec
					baseUrl: type: "string"
				}
			}
			resources: {
				type: "object"
				properties: {
					"keygen":            crd.#containerresources
					"chown-nova":        crd.#containerresources
					"nova-compute":      crd.#containerresources
					"nova-compute-ssh":  crd.#containerresources
					"libvirtd":          crd.#containerresources
					"compute-evict-job": crd.#containerresources
				}
			}
			evictPollMigrationSpeedLocalDisk: {
				type:    "integer"
				default: 30
			}
			hostAggregates: {
				type: "array"
				items: {
					type: "string"
				}
			}
		}
	}
	#schema: properties: status: properties: {
		conditions: items: properties: type: enum: [
			"Converged",
			"GarbageCollected",
			"Evicted",
			"Enabled",
			"BoundToNode",
			"RequiresRecreation",
		]
		eviction: {
			type:     "object"
			nullable: true
			required: ["reason"]
			properties: reason: type: "string"
		}
		state: {
			type:    "string"
			default: "Creating"
			enum: [
				"Creating",
				"Enabled",
				"Disabled",
				"Evicting",
				"DisabledAndCleared",
			]
		}
	}
	#additionalprintercolumns: [
		{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "State"
			type:        "string"
			description: "The state of the service"
			jsonPath:    ".status.state"
		},
		{
			name:        "Evicting"
			type:        "string"
			description: "Eviction status"
			jsonPath:    ".status.eviction.mode"
		},
		{
			name:        "Enabled"
			type:        "string"
			description: "Enabled status"
			jsonPath:    ".status.conditions[?(@.type==\"Enabled\")].status"
		},
		{
			name:        "Requires Recreation"
			type:        "string"
			description: "Requires Recreation status"
			jsonPath:    ".status.conditions[?(@.type==\"RequiresRecreation\")].status"
		},
	]
}
