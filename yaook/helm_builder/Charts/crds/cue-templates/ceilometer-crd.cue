// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#messagequeue
crd.#issuer
{
	#kind:     "CeilometerDeployment"
	#plural:   "ceilometerdeployments"
	#singular: "ceilometerdeployment"
	#shortnames: ["ceilometerd", "ceilometerds"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"central",
			"notification",
			"ceilometerConfig",
			"ceilometerCompute",
			"messageQueue",
			"targetRelease",
			"issuerRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			central: crd.replicated
			central: properties: replicas: default: 1
			notification: crd.replicated
			notification: properties: replicas: default: 1

			ceilometerConfig:  crd.#anyconfig
			ceilometerSecrets: crd.#configsecret

			ceilometerCompute: {
				crd.pernodeconfig
				properties: configTemplates: items: properties: ceilometerComputeConfig: crd.#anyconfig
			}

			additionalHosts: {
				type: "array"
				items: {
					type: "object"
					required: ["hostnames", "ip"]
					properties: {
						hostnames: {
							type: "array"
							items: type: "string"
						}
						ip: type: "string"
					}
				}
			}
			ceilometerEventDefinitions: {
				type: "array"
				items: {
					type: "object"
					required: ["event_type", "traits"]
					properties: {
						event_type: {
							type: "array"
							items: type: "string"
						}
						traits: {
							type: "object"
							additionalProperties: {
								type:                                   "object"
								"x-kubernetes-preserve-unknown-fields": true
							}
						}
					}
				}
			}
			ceilometerEventPipeline: crd.#anyobj
			ceilometerPipeline:      crd.#anyobj
			pankoConfig:             crd.#configsecret

			central: properties: resources: {
				type: "object"
				properties: "ceilometer-agent-central": crd.#containerresources
			}
			notification: properties: resources: {
				type: "object"
				properties: "ceilometer-agent-notification": crd.#containerresources
			}
			ceilometerCompute: properties: resources: {
				type: "object"
				properties: "ceilometer-compute-agent": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: "ceilometer-upgrade-job": crd.#containerresources
			}
			skip_gnocchi_bootstrap: {
				type:    "boolean"
				default: false
			}
		}
	}
}
