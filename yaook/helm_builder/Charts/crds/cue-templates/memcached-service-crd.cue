// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#cacertificatecrd
crd.#imagepullsecretcrd
{
	#group:    "infra.yaook.cloud"
	#kind:     "MemcachedService"
	#plural:   "memcachedservices"
	#singular: "memcachedservice"
	#shortnames: ["memcachedsvcs", "memcachedsvc", "memsvc"]
	#schema: properties: spec: {
		crd.replicated
		required: [
			"imageRef",
		]
		properties: {
			imageRef: type: "string"
			memory: {
				type:    "integer"
				default: 512
			}
			connections: {
				type:    "integer"
				default: 1024
			}
			resources: {
				type: "object"
				properties: memcached: crd.#containerresources
			}
		}
	}
	#schema: properties: status: properties: replicas: type: "integer"
}
