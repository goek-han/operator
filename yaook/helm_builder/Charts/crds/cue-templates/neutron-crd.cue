// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "NeutronDeployment"
	#plural:   "neutrondeployments"
	#singular: "neutrondeployment"
	#shortnames: ["neutrond", "neutronds"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"novaRef",
			"keystoneRef",
			"api",
			"targetRelease",
			"neutronConfig",
			"neutronML2Config",
			"setup",
			"database",
			"messageQueue",
			"region",
			"issuerRef",
		]
		properties: {
			novaRef:     crd.#ref
			keystoneRef: crd.#keystoneref

			api:              crd.apiendpoint
			serviceMonitor:   crd.#servicemonitor
			neutronConfig:    crd.#anyconfig
			neutronSecrets:   crd.#configsecret
			neutronML2Config: crd.#anyconfig
			messageQueue:     crd.#messagequeuesnip

			setup: {
				type:        "object"
				description: "The type of neutron setup you want. You can choose between 'ovs' and 'ovn'"
				oneOf: [
					{required: ["ovs"]},
					{required: ["ovn"]},
				]
				properties: {
					ovn: {
						type: "object"
						required: [
							"controller",
							"northboundOVSDB",
							"northd",
							"southboundOVSDB",
						]
						properties: {
							controller: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										bridgeConfig: {
											type: "array"
											items: {
												type: "object"
												required: ["bridgeName", "uplinkDevice", "openstackPhysicalNetwork"]
												properties: {
													bridgeName: type:   "string"
													uplinkDevice: type: "string"
													openstackPhysicalNetwork: {
														type:        "string"
														description: "This must be the same as `--provider-physical-network`, when setting up the provider network"
													}
												}
											}
										}
									}
								}
							}
							northboundOVSDB: {
								type: "object"
								crd.replicated
								crd.storageconfig
								required: ["backup"]
								properties: {
									backup: {
										type: "object"
										crd.backupspec
									}
									resources: {
										type: "object"
										properties: {
											"setup-ovsdb": crd.#containerresources
											"ovsdb":       crd.#containerresources
										}
									}
								}
							}
							southboundOVSDB: {
								type: "object"
								crd.replicated
								crd.storageconfig
								required: ["backup"]
								properties: {
									backup: {
										type: "object"
										crd.backupspec
									}
									resources: {
										type: "object"
										properties: {
											"setup-ovsdb": crd.#containerresources
											"ovsdb":       crd.#containerresources
										}
									}
								}
							}
							northd: {
								type: "object"
								crd.replicated
								properties: {
									resources: {
										type: "object"
										properties: {
											"northd": crd.#containerresources
										}
									}
								}
							}
						}
					}
					ovs: {
						type: "object"
						required: [
							"l2",
							"l3",
							"dhcp",
						]
						properties: {
							l2: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:                 crd.#anyconfig
										neutronOpenvSwitchAgentConfig: crd.#anyconfig
										bridgeConfig: {
											type: "array"
											items: {
												type: "object"
												required: ["bridgeName", "uplinkDevice"]
												properties: {
													bridgeName: type:   "string"
													uplinkDevice: type: "string"
												}
											}
										}
										overlayNetworkConfig: {
											type: "object"
											properties: {
												ovs_local_ip_subnet: type: "string"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-ovs-bridge-setup":  crd.#containerresources
											"neutron-openvswitch-agent": crd.#containerresources
											"ovs-vswitchd":              crd.#containerresources
											"ovsdb-server":              crd.#containerresources
										}
									}
								}
							}
							l3: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:              crd.#anyconfig
										neutronL3AgentConfig:       crd.#anyconfig
										neutronMetadataAgentConfig: crd.#anyconfig
									}
									startupLimitMinutes: {
										type:    "integer"
										default: 60
									}
									evictor: {
										type:        "object"
										description: "Configuration for the L3 eviction job"
										properties: {
											allowFallback: {
												type:        "boolean"
												default:     true
												description: "This setting allows the evict job to disable the agent if not all routers could be migrated because no agent was available for them. This violates redundancy and is unsafe"
											}
											pollInterval: {
												type:        "integer"
												default:     5
												description: "Defines in seconds how long to wait between iterates until the next poll of the API."
											}
											maxParallelMigrations: {
												type:        "integer"
												default:     15
												description: "Defines how many routers may be evacuated in parallel per iteration"
											}
											respectAvailabilityZones: {
												type:        "boolean"
												default:     false
												description: "If enabled, excludes all agents where the router is already scheduled except the availibility zone of the agent being evicted."
											}
											verifySeconds: {
												type:        "integer"
												default:     0
												description: "If the value is greater than 0, allows the user to specify how long the API should be queried until the router is active, if this did not work in the defined time period an error is logged"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-l3-agent":       crd.#containerresources
											"neutron-metadata-agent": crd.#containerresources
											"l3-evict-job":           crd.#containerresources
										}
									}
								}
							}
							dhcp: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:              crd.#anyconfig
										neutronDHCPAgentConfig:     crd.#anyconfig
										neutronMetadataAgentConfig: crd.#anyconfig
									}
									evictor: {
										type:        "object"
										description: "Configuration for the DHCP eviction job"
										properties: {
											pollInterval: {
												type:        "integer"
												default:     5
												description: "Defines in seconds how long to wait between iterates until the next poll of the API"
											}
											maxParallelMigrations: {
												type:        "integer"
												default:     5
												description: "Defines how many networks may be evacuated in parallel per iteration"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-dhcp-agent":     crd.#containerresources
											"neutron-metadata-agent": crd.#containerresources
											"dhcp-evict-job":         crd.#containerresources
										}
									}
								}
							}
							bgp: {
								type: "object"
								additionalProperties: {
									type: "object"
									crd.pernodeconfig
									properties: {
										configTemplates: items: properties: {
											neutronConfig:           crd.#anyconfig
											neutronBGPDRAgentConfig: crd.#anyconfig
											bgpInterfaceMapping: {
												type: "object"
												required: ["bridgeName"]
												properties: {
													bridgeName: type: "string"
												}
											}
										}
										resources: {
											type: "object"
											properties: {
												"neutron-bgp-interface-setup": crd.#containerresources
												"neutron-bgp-dragent":         crd.#containerresources
												"bgp-evict-job":               crd.#containerresources
											}
										}
									}
								}
							}
						}
					}
				}
			}

			api: properties: resources: {
				type: "object"
				properties: {
					"neutron-api":             crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}

			jobResources: {
				type: "object"
				properties: {
					"neutron-db-sync-job": crd.#containerresources
				}
			}
		}
	}
}
