// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#cacertificatecrd
crd.#operatorcrd
{
	#kind:     "ExternalKeystoneDeployment"
	#plural:   "externalkeystonedeployments"
	#singular: "externalkeystonedeployment"
	#shortnames: ["ekd", "ekds"]
	#schema: properties: spec: {
		required: [
			"username",
			"password",
			"authURL",
			"memcachedServers",
		]
		properties: {
			username: type: "string"
			password: crd.#ref
			authURL: type: "string"
			memcachedServers: {
				type: "array"
				items: {
					type:  "string"
					regex: "^[^\\:]+:[0-9]{1,5}$"
				}
			}

			projectName: {
				type:    "string"
				default: "admin"
			}

			authType: {
				type:    "string"
				default: "password"
			}

			userDomainName: {
				type:    "string"
				default: "default"
			}
			projectDomainName: {
				type:    "string"
				default: "default"
			}
		}
	}
}
