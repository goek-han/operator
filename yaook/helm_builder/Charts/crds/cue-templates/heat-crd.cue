// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#messagequeue
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "HeatDeployment"
	#plural:   "heatdeployments"
	#singular: "heatdeployment"
	#shortnames: ["heatd", "heatds"]
	#releases: ["queens", "train"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"apiCfn",
			"engine",
			"targetRelease",
			"heatConfig",
			"database",
			"messageQueue",
			"region",
			"issuerRef",
		]
		properties: {
			keystoneRef:    crd.#keystoneref
			serviceMonitor: crd.#servicemonitor
			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			apiCfn:         crd.apiendpoint
			engine:         crd.replicated

			heatConfig:  crd.#anyconfig
			heatSecrets: crd.#configsecret

			api: properties: resources: {
				type: "object"
				properties: {
					"heat-api":                crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}
			apiCfn: properties: resources: {
				type: "object"
				properties: {
					"heat-api-cfn":            crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}
			engine: properties: resources: {
				type: "object"
				properties: "heat-engine": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: "heat-db-sync-job": crd.#containerresources
			}
		}
	}
}
