// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#messagequeue
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "CinderDeployment"
	#plural:   "cinderdeployments"
	#singular: "cinderdeployment"
	#shortnames: ["cinderd", "cinderds"]
	#releases: ["queens", "rocky", "stein", "train", "ussuri", "victoria", "wallaby", "xena", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"scheduler",
			"targetRelease",
			"cinderConfig",
			"backends",
			"database",
			"messageQueue",
			"region",
			"issuerRef",
			"backup",
			"databaseCleanup",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			scheduler:      crd.replicated
			cinderConfig:   crd.#anyconfig
			cinderSecrets:  crd.#configsecret

			databaseCleanup: {
				type: "object"
				required: ["schedule", "deletionTimeRange"]
				properties: {
					schedule: crd.#cronschedulesnip
					deletionTimeRange: {
						type:    "integer"
						default: 60
					}
				}
			}

			backends: {
				type: "object"
				additionalProperties: {
					type: "object"
					required: ["volume"]
					oneOf: [
						{required: ["rbd"]},
						{required: ["netapp"]},
					]
					properties: {
						cinderSecrets: crd.#configsecret
						volume: {
							crd.replicated
							properties: replicas: {
								default: 1
								minimum: 0
								maximum: 1
							}
							properties: resources: {
								type: "object"
								properties: "cinder-volume": crd.#containerresources
							}
						}
						rbd: {
							type: "object"
							required: [
								"keyringReference",
								"keyringUsername"]
							properties: {
								backendConfig: {
									type: "object"
									additionalProperties: {
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
								keyringReference: type: "string"
								keyringUsername: type:  "string"
								cephConfig: {
									type: "object"
									additionalProperties: {
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
							}
						}
						netapp: {
							type: "object"
							required: [
								"login",
								"passwordReference",
								"server",
								"vserver",
								"shares",
							]
							properties: {
								backendConfig: {
									type: "object"
									additionalProperties: {
										type:                                   "string"
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
								login: type:             "string"
								passwordReference: type: "string"
								server: type:            "string"
								vserver: type:           "string"
								copyoffloadConfigMap: crd.#ref
								shares: {
									type: "array"
									items: type: "string"
								}
							}
						}
					}
				}
			}

			backup: {
				// We use an object here as we need to support multiple different cinder-backup services.
				// This is required as a single cinder-backup can not handle multiple availability zones.
				// We could also use an array, but that would have the issue that we can not easily track the individual entries if one would be removed.
				type: "object"
				additionalProperties: {
					type: "object"
					required: ["cinderConfig"]
					crd.replicated
					properties: {
						cinderConfig:  crd.#anyconfig
						cinderSecrets: crd.#configsecret
						terminationGracePeriod: {
							type:    "integer"
							default: 3600
						}
					}
				}
			}

			ids: {
				type: "object"
				anyOf: [
					{required: ["uid", "gid"]},
				]
				properties: {
					uid: type: "integer"
					gid: type: "integer"
				}
			}

			api: properties: resources: {
				type: "object"
				properties: {
					"cinder-api":              crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}
			scheduler: properties: resources: {
				type: "object"
				properties: "cinder-scheduler": crd.#containerresources
			}
			backup: additionalProperties: properties: resources: {
				type: "object"
				properties: "cinder-backup": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: {
					"cinder-db-sync-job":         crd.#containerresources
					"cinder-db-upgrade-pre-job":  crd.#containerresources
					"cinder-db-upgrade-post-job": crd.#containerresources
					"cinder-db-cleanup-cronjob":  crd.#containerresources
				}
			}
			conversionVolume: {
				type: "object"
				required: ["emptyDir"]
				properties: {
					emptyDir: {
						type:        "object"
						description: "EmptyDir represents a temporary directory that shares a pod's lifetime. More info: https://kubernetes.io/docs/concepts/storage/volumes#emptydir"
						properties: {
							medium: {
								type:        "string"
								pattern:     "^$|Memory"
								default:     ""
								description: "What type of storage medium should back this directory."
							}
							sizeLimit: {
								type:        "string"
								pattern:     crd.quantilepattern
								description: "Total amount of local storage required for this EmptyDir volume."
							}
						}
					}
				}
			}
		}
	}
}
