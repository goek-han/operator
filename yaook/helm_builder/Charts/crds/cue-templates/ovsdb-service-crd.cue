// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#cacertificatecrd
crd.#imagepullsecretcrd
{
	#group:    "infra.yaook.cloud"
	#kind:     "OVSDBService"
	#plural:   "ovsdbservices"
	#singular: "ovsdbservice"
	#shortnames: ["ovsdbsvcs", "ovsdbsvc"]
	#schema: properties: spec: {
		crd.replicated
		crd.storageconfig
		required: [
			"dbSchema",
			"imageRef",
			"issuerRef",
			"storageSize",
			"backup",
		]
		properties: {
			dbSchema: {
				type: "string"
				enum: [
					"northbound",
					"southbound",
				]

			}
			imageRef: type: "string"
			backup: crd.backupspec

			issuerRef: crd.#ref

			serviceMonitor: crd.#servicemonitor

			resources: {
				type: "object"
				properties: {
					"ovsdb":       crd.#containerresources
					"setup-ovsdb": crd.#containerresources
				}
			}
		}
	}
	#schema: properties: status: properties: dbSchema: type: "string"
}
