// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "KeystoneDeployment"
	#plural:   "keystonedeployments"
	#singular: "keystonedeployment"
	#shortnames: ["keystoned", "keystoneds"]
	#releases: ["train", "ussuri", "victoria", "wallaby", "xena", "yoga"]
	#schema: properties: spec: {
		required: [
			"api",
			"database",
			"memcached",
			"targetRelease",
			"keystoneConfig",
			"region",
			"issuerRef",
		]
		properties: {
			api: {
				type: "object"
				required: ["ingress"]
				crd.replicated
				properties: {
					ingress: crd.#ingress
					wsgiProcesses: type: "integer"
				}
			}
			serviceMonitor:      crd.#servicemonitor
			keystoneConfig:      crd.#anyconfig
			keystoneSecrets:     crd.#configsecret
			keyRotationSchedule: crd.#cronschedulesnip

			api: properties: resources: {
				type: "object"
				properties: {
					"keystone":                crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}
			jobResources: {
				type: "object"
				properties: {
					"keystone-bootstrap-job":                   crd.#containerresources
					"keystone-credential-setup-job":            crd.#containerresources
					"keystone-db-sync-job":                     crd.#containerresources
					"keystone-db-upgrade-post-job":             crd.#containerresources
					"keystone-db-upgrade-pre-job":              crd.#containerresources
					"keystone-fernet-setup-job":                crd.#containerresources
					"keystone-key-rotation-fernet-cronjob":     crd.#containerresources
					"keystone-key-rotation-credential-cronjob": crd.#containerresources
				}
			}
		}
	}
}
