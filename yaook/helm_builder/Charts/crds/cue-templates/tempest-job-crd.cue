// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#releaseawarecrd
crd.#oneshotoperatorcrd
crd.#imagepullsecretcrd
{
	#kind:     "TempestJob"
	#plural:   "tempestjobs"
	#singular: "tempestjob"
	#shortnames: ["tempestj"]
	#releases: ["27.0.0", "32.0.0"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"target",
		]
		properties: {
			keystoneRef: crd.#keystoneref
			target: {
				type: "object"
				oneOf: [
					{required: ["service"]},
					{required: ["regex"]},
				]
				properties: {
					service: {
						type: "string"
						enum: [
							"cinder",
							"glance",
							"keystone",
							"nova",
							"neutron",
							"all",
						]
					}
					regex: {
						type: "string"
					}
				}
			}
			serial: {
				type:    "boolean"
				default: false
			}
			exclude: {
				type: "array"
				items: {
					type: "string"
				}
			}
			tempestConfig: crd.#anyconfig
			region: {
				type:    "string"
				default: "RegionOne"
			}
			resources: {
				type: "object"
				properties: {
					"tempest-job": crd.#containerresources
				}
			}
		}
	}
}
