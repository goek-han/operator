import (
	"yaook.cloud/crd"
)

crd.#crd
{
	#group:    "compute.yaook.cloud"
	#kind:     "SSHIdentity"
	#plural:   "sshidentities"
	#singular: "sshidentity"
	#schema: properties: status: {
		type: "object"
		required: ["keys"]
		properties: keys: {
			type: "object"
			required: ["host", "user"]
			properties: {
				host: {
					type:                     "array"
					"x-kubernetes-list-type": "set"
					items: {
						type:  "string"
						regex: "^ssh-\\S+\\s+[a-zA-Z0-9/+]+(\\s+\\S+)?$"
					}
				}
				user: {
					type:                     "array"
					"x-kubernetes-list-type": "set"
					items: {
						type:  "string"
						regex: "^ssh-\\S+\\s+[a-zA-Z0-9/+]+(\\s+\\S+)?$"
					}
				}
			}
		}
	}
}
