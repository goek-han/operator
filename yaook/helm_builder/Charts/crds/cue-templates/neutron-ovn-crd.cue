// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#region

crd.#releaseawarecrd
crd.#operatorcrd
crd.#imagepullsecretcrd
{
	#group:  "network.yaook.cloud"
	#kind:   "NeutronOVNAgent"
	#plural: "neutronovnagents"
	#releases: ["yoga"]
	#singular: "neutronovnagent"
	#shortnames: ["ovnagent", "ovnagents"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"novaRef",
			"caConfigMapName",
			"deployedOnComputeNode",
			"southboundServers",
			"issuerRef",
			"targetRelease",
		]
		properties: {
			keystoneRef: crd.#keystoneref
			novaRef:     crd.#ref
			caConfigMapName: type: "string"
			bridgeConfig: {
				type: "array"
				items: {
					type: "object"
					required: ["bridgeName", "uplinkDevice", "openstackPhysicalNetwork"]
					properties: {
						bridgeName: type:   "string"
						uplinkDevice: type: "string"
						openstackPhysicalNetwork: {
							type:        "string"
							description: "This must be the same as `--provider-physical-network`, when setting up the provider network"
						}
					}
				}
			}
			deployedOnComputeNode: {
				type: "boolean"
			}
			southboundServers: {
				type: "array"
				items: {
					type: "string"
					// Update me to "^[A-Za-z]{3,9}:[^\\:]+:[0-9]{1,5}$" after
					// deprecation of the old format
					// Old version ['neutron-ovsdb-0-pod.yaook.svc.cluster.local:6642',...]
					// New version ['ssl:10.101.161.234:6642',...]
					// Deprecation date: End of January 2023
					regex: "^([A-Za-z]{3,9})*:[^\\:]+:[0-9]{1,5}$"
				}
			}
			resources: {
				type: "object"
				properties: {
					"ovs-vswitchd": crd.#containerresources
					"ovsdb-server": crd.#containerresources
				}
			}
			issuerRef: crd.#ref
		}
	}
	#schema: properties: status: properties: {
		conditions: items: properties: type: enum: [
			"Converged",
			"GarbageCollected",
			"Evicted",
			"Enabled",
			"BoundToNode",
			"RequiresRecreation",
		]
		eviction: {
			type:     "object"
			nullable: true
			required: ["reason"]
			properties: reason: type: "string"
		}
		state: {
			type:    "string"
			default: "Creating"
			enum: [
				"Creating",
				"Enabled",
				"Disabled",
				"Evicting",
				"DisabledAndCleared",
			]
		}
	}
	#additionalprintercolumns: [
		{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "State"
			type:        "string"
			description: "The state of the service"
			jsonPath:    ".status.state"
		},
		{
			name:        "Requires Recreation"
			type:        "string"
			description: "Requires Recreation status"
			jsonPath:    ".status.conditions[?(@.type==\"RequiresRecreation\")].status"
		},
	]
}
