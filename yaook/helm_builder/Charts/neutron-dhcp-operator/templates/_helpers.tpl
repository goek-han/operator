{{- define "yaook.operator.name" -}}
neutron_dhcp
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_NEUTRON_DHCP_AGENT_OP_JOB_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
