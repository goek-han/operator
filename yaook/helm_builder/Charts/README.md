# Generated Helm Charts

This directory holds the templates for Helm charts released by the Yaook Operator project.

## `values.yaml` and `Chart.yaml` templates

Each Helm Chart in this directory must have a `values-template.yaml.j2` and a `Chart-template.yaml.j2`. These are Jinja2 templates (like the templates used for k8s resources by Yaook Operator) which have the following variables available to them:

- `version`: The Yaook Operator version for which the Charts are being built.
- `images`: The contents of the image version pin file (`yaook/assets/pinned_versions.yaml`. This is only available in `values-template.yaml.j2`.

These templates are rendered to `values.yaml` and `Chart.yaml`, respectively, and then included in the Helm Chart.

The intent behind this is to simply include values related to the specific Operator version into the template. Doing complex templating operations is explicitly discouraged.

## `cue-templates` folder

Each Chart may have a `cue-templates` folder containing `.cue` files. Each of these files will be rendered, with `/deploy/cuelang/pkg` in its include path. The intent is to allow rendering CRDs.

The rendered structs are written as YAML into the `templates` folder and included in the generated Helm Chart.

## `MANIFEST.yaml` file

This optional file contains additional instructions to the helm builder. Currently, the following keys are supported:

- `generate_roles`: Trigger generation of Operator roles and inclusion into the `templates`.

    - `name_template`: Must be a valid Helm resource template string which will be used as the *name* of both the ClusterRole and the Role generated for the operators. Within this string, `{op}` is replaced with the name of the operator for which the roles were rendered.

        A typical value will be `{{ include "yaook.operator-lib.fullname" . }}` if roles are only built for a single operator and `{{ include "yaook.operator-lib.fullname" . }}-{op}` if multiple operators are included.

    - `operators`: A list of operator names for which roles are to be generated.

    The generated roles are concatenated, written to `templates/gen-roles.yaml` and included in the Helm Chart.

- `build` (bool, default: true): If set to false, this chart is never built. This is useful for library charts included via symlinks into the other charts.
