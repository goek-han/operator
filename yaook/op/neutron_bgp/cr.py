#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import ipaddress
import typing

import kubernetes_asyncio.client as kclient

import yaook.common
import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
from yaook.statemachine import version_utils
from . import resources


class BGPInterfaceMapping(typing.NamedTuple):
    bridge_name: str
    interface_name: str
    interface_ip: str = ''


class BGPConfigState(sm.CueSecret):
    def _extract_interface_mappings(
            self,
            ctx: sm.Context,
            node_info: kclient.models.v1_node.V1Node,
            ) -> BGPInterfaceMapping:
        bgp_mapping = ctx.parent_spec["bgpInterfaceMapping"]
        bgp_interface = 'bgp-' + ctx.parent_uid[:11]
        ip_annotation = sm.context.ANNOTATION_BGP_INTERFACE_IP + \
            ctx.parent_spec["configKey"]
        ip = node_info.metadata.annotations.get(ip_annotation)
        try:
            ipaddress.ip_interface(ip)
        except ValueError:
            raise sm.ConfigurationInvalid(
                f"The given IP '{str(ip)}' via annotation "
                f"'{str(ip_annotation)}' is not a valid interface IP."
            )
        if len(bgp_interface) > 15:
            raise sm.ConfigurationInvalid(
                f"Interface name is not allowed to be longer than "
                f"15 character. '{bgp_interface}' is too long.")
        return BGPInterfaceMapping(
            bridge_name=bgp_mapping["bridgeName"],
            interface_name=bgp_interface,
            interface_ip=ip
        )

    async def _render_cue_config(
            self,
            ctx: sm.Context,
            ) -> sm.ResourceBody:
        config = await super()._render_cue_config(ctx)
        v1 = kclient.CoreV1Api(ctx.api_client)
        node_info = await v1.read_node(ctx.parent_spec['hostname'])
        bgp_interface_mapping = self._extract_interface_mappings(
            ctx,
            node_info,
        )
        interface_mapping = [
            "BRIDGE=%s" % bgp_interface_mapping.bridge_name,
            "BGP_INTERFACE=%s" % bgp_interface_mapping.interface_name,
            "BGP_IP=%s" % bgp_interface_mapping.interface_ip,
        ]
        config["bgp_interface_mapping"] = '\n'.join(interface_mapping)
        for value in config.values():
            if 'use_json = true' in value and \
                    'debug = true' in value:
                raise sm.ConfigurationInvalid(
                    'BGP agent will fail with use_json=true and debug=true')
        return config


AMQP_USERNAME_FORMAT = "bgp-{name}"
USERNAME_FORMAT = "{resource}-{name}-"


class BGPSpecSequenceLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        bgp_driver_class = 'ryu.driver.RyuBgpDriver' \
            if version_utils.get_target_release(ctx) in ['queens', 'rocky'] \
            else 'os_ken.driver.OsKenBgpDriver'
        return {
            "neutron_bgp_dragent": yaook.common.config.OSLO_CONFIG.declare(
                ctx.parent_spec.get("neutronConfig", []) +
                ctx.parent_spec["neutronBGPDRAgentConfig"] +
                [{"DEFAULT": {"host": ctx.parent_name},
                  "bgp": {"bgp_speaker_driver": f"neutron_dynamic_routing"
                          f".services.bgp.agent.driver.{bgp_driver_class}"
                          },
                  }],
            ),
        }


@environ.config(prefix="YAOOK_NEUTRON_BGP_DRAGENT_OP")
class OpNeutronBGPDRAgentConfig:
    interface = environ.var("internal")
    job_image = environ.var()


class NeutronBGPDRAgent(sm.CustomResource):
    API_GROUP = "network.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "neutronbgpdragents"
    KIND = "NeutronBGPDRAgent"
    ADDITIONAL_PERMISSIONS = (
        # The jobs management is hidden behind the DHCPStateResource and
        # thus cannot be auto-discovered by the CLI tool.
        (False, "batch", "jobs", {"create", "list", "watch", "get", "delete",
                                  "patch", "update", "deletecollection"}),
        # We need to pass a few permissions to the job itself
        (False, "network.yaook.cloud", "neutronbgpdragents/status",
         {"get", "patch", "replace", "update"}),
        # Required to read the node state for judging the next steps in
        # DHCPStateResource
        (True, "", "nodes", {"get", "patch"}),
    )
    RELEASES = [
        "queens",
        "train",
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    neutron_l2_lock = resources.BGPL2Lock()

    neutron_bgp_dragent_docker_image = yaook.op.common.image_dependencies(
        "neutron-bgp-dragent/neutron-bgp-dragent-{release}",
        RELEASES,
    )
    neutron_agent_docker_image = yaook.op.common.image_dependencies(
        "neutron-agent/neutron-agent-{release}",
        RELEASES,
    )

    nova = sm.NovaReference()
    nova_metadata_proxy_secret = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=nova,
        foreign_component=yaook.op.common.NOVA_METADATA_SECRET_COMPONENT,
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )
    keystone_operator_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    user = sm.StaticKeystoneUserWithParent(
        username="neutron-bgp",
        keystone=keystone
    )
    user_password = yaook.op.common.keystone_user_credentials_reference(
        user
    )

    mq = sm.AMQPServerRef()
    mq_service = sm.ForeignResourceDependency(
        foreign_resource=mq,
        foreign_component=yaook.op.common.AMQP_SERVER_SERVICE_COMPONENT,
        resource_interface_factory=sm.service_interface,
    )
    mq_bgp_user_passwords = sm.AutoGeneratedPassword(
        metadata=("neutron-bgp-mq-user-", True),
    )

    mq_bgp_user = sm.SimpleAMQPUser(
        metadata=("neutron-bgp-", True),
        server=mq,
        username_format=AMQP_USERNAME_FORMAT,
        password_secret=mq_bgp_user_passwords,
    )

    bgp_config = BGPConfigState(
        metadata=(lambda ctx: (USERNAME_FORMAT.format(
            resource="neutron-bgp-config", name=ctx.parent_name), True)),
        copy_on_write=True,
        add_cue_layers=[
            BGPSpecSequenceLayer(),
            sm.AMQPTransportLayer(
                target="neutron_bgp_dragent",
                service=mq_service,
                username=lambda ctx: AMQP_USERNAME_FORMAT.format(
                    name=ctx.parent_name),
                password_secret=mq_bgp_user_passwords,
            ),
        ],
        add_dependencies=[
            mq_bgp_user,
        ],
    )

    bgp_service = sm.TemplatedService(
        template="bgp-service.yaml",
    )

    job_role = sm.TemplatedRole(
        component="job_role",
        template="job-role.yaml",
        params={
            "password_secret_name":
            yaook.op.common.NEUTRON_BGP_OPERATOR_USER_SECRET,
        },
    )

    job_service_account = sm.TemplatedServiceAccount(
        component="job_service_account",
        template="serviceaccount.yaml",
        params={
            "suffix": "job",
        },
        add_dependencies=[bgp_service],
    )

    job_role_binding = sm.TemplatedRoleBinding(
        template="rolebinding.yaml",
        copy_on_write=True,
        params={
            "role": job_role.component,
            "service_account": job_service_account.component,
            "name_prefix": "bgp-job-",
        },
        add_dependencies=[job_role, job_service_account],
    )

    bgp_dragent = sm.TemplatedRecreatingStatefulSet(
        template="bgp-dragent-statefulset.yaml",
        add_dependencies=[
            bgp_config,
            bgp_service,
            neutron_l2_lock,
        ],
        versioned_dependencies=[
            neutron_bgp_dragent_docker_image,
            neutron_agent_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                yaook.op.common.BGP_SCHEDULING_KEYS,
            )),
        },
    )

    bgp_dragent_pdb = sm.DisallowedPodDisruptionBudget(
         metadata=("neutron-bgp-dragent-pdb-", True),
         replicated=bgp_dragent,
    )

    api_state = resources.BGPStateResource(
        finalizer="network.yaook.cloud/neutron-bgp-dragent",
        endpoint_config=keystone_operator_api,
        job_endpoint_config=keystone_internal_api,
        credentials_secret=user_password,
        scheduling_keys=[
            yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
            yaook.op.common.SchedulingKey.OPERATOR_NEUTRON.value,
        ],
        eviction_job_template="eviction-job.yaml",
        add_dependencies=[bgp_dragent, job_service_account],
    )

    # We need to restart the bgp-agent after it is enabled (again), otherwise
    # it won't start the speaker. Speaker is not started, when enabling the
    # agent...
    restart_bgp_dragent = sm.TriggerRollingRestart(
        controller_ref=bgp_dragent,
        restart_id=lambda ctx: (
            "restart-after-enabling-"
            f"{ctx.parent['metadata']['generation']}"
        ),
        add_dependencies=[
            bgp_dragent,
            api_state,
        ],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        config = environ.to_config(OpNeutronBGPDRAgentConfig)
        self.keystone_operator_api._foreign_component = {
            "internal": yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
            "public": yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        }[config.interface]
        self.api_state._params["job_image"] = config.job_image


sm.register(NeutronBGPDRAgent)
