##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['bgp_service'].resource_name() }}
spec:
  serviceName: {{ dependencies['bgp_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
    spec:
      automountServiceAccountToken: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronBGPDRAgent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ crd_spec.hostname }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                "network.yaook.cloud/provides-l2": "true"
            topologyKey: kubernetes.io/hostname
      dnsPolicy: ClusterFirstWithHostNet
      initContainers:
        - name: neutron-bgp-ovs-setup
          image:  {{ versioned_dependencies['neutron_bgp_dragent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/add-bgp-interface.sh"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: bgp-interface-mapping-config-volume
              mountPath: /etc/neutron/bgp_mapping
              subPath: bgp_interface_mapping
            - name: host-proc
              mountPath: /host/proc
              mountPropagation: Bidirectional
            - name: run-openvswitch
              mountPath: /run/openvswitch
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: DEBUG
              value: "1"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          resources: {{ crd_spec | resources('neutron-bgp-interface-setup') }}
      containers:
        - name: "neutron-bgp-dragent"
          image:  {{ versioned_dependencies['neutron_bgp_dragent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/neutron-bgp-dragent-runner.sh"]
          volumeMounts:
            - name: neutron-bgp-config-volume
              mountPath: /etc/neutron/neutron.conf
              subPath: neutron.conf
            - name: bgp-interface-mapping-config-volume
              mountPath: /etc/neutron/bgp_mapping
              subPath: bgp_interface_mapping
            - name: run-openvswitch
              mountPath: /run/openvswitch
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/neutron/bgp_dragent.ini
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/neutron/bgp_dragent.ini
          resources: {{ crd_spec | resources('neutron-bgp-dragent') }}
      volumes:
        - name: bgp-interface-mapping-config-volume
          secret:
            secretName: {{ dependencies['bgp_config'].resource_name() }}
            items:
            - key: bgp_interface_mapping
              path: bgp_interface_mapping
        - name: neutron-bgp-config-volume
          secret:
            secretName: {{ dependencies['bgp_config'].resource_name() }}
            items:
            - key: neutron_bgp_dragent.conf
              path: neutron.conf
        - name: host-proc
          hostPath:
            path: /proc
        - name: run-openvswitch
          hostPath:
            path: /run/openvswitch
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
