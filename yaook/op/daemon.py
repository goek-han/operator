#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import datetime
import enum
import functools
import io
import logging
import random
import signal
import typing
import yaml

import aiohttp

import kubernetes_asyncio.client
import kubernetes_asyncio.config
import kubernetes_asyncio.watch

import yaook.statemachine as sm
from yaook.statemachine import customresource
from yaook.statemachine import api_utils
import yaook.statemachine.context as context
import yaook.statemachine.watcher as watcher

from . import tasks


T = typing.TypeVar("T")


class WriterLimitExceeded(OSError):
    pass


class LimitedWriter(io.StringIO):
    def __init__(self, limit: int = 65535):
        super().__init__()
        self.__limit = limit
        self.__written = 0

    def write(self, data: str) -> int:
        to_write = len(data)
        max_write = min(self.__limit - self.__written, to_write)
        if max_write < to_write:
            raise WriterLimitExceeded("write limit exceeded")
        written = super().write(data)
        self.__written += written
        return written


def limited_yaml_dump_lines(data: typing.Any, limit: int) -> typing.List[str]:
    buf = LimitedWriter(limit=limit)
    yaml.dump(data, buf, indent=2)
    return buf.getvalue().split("\n")


class lazy_event_diff(typing.Generic[T]):
    """
    Helper class to lazily generate extensive diff information from a watch
    event.

    By not generating the diff when the instance is created but only when
    it is stringified, we ensure that the diff is not computed if the user
    (generally a logger) does not need it (e.g. because of loglevel).
    """

    def __init__(self, ev: watcher.StatefulWatchEvent[T]):
        super().__init__()
        self._ev = ev

    @staticmethod
    def _indent(lines: typing.Iterable[str]) -> typing.Iterator[str]:
        for line in lines:
            yield "  " + line

    def __str__(self) -> str:
        ev = self._ev
        ref = ev.reference
        parts = [
            f"Event referring to {ref.namespace}/{ref.name} of type "
            f"{ref.kind} {ref.api_version} "
        ]
        try:
            new_yaml = limited_yaml_dump_lines(self._ev.raw_object, 65535)
            if self._ev.old_raw_object:
                old_yaml = limited_yaml_dump_lines(self._ev.old_raw_object,
                                                   65535)
            else:
                old_yaml = []
        except WriterLimitExceeded:
            parts.append("<object is larger than 64k, refusing to diff>")
            return "".join(parts)

        parts.append(f"  Type: {ev.type_}")
        if ev.type_ == watcher.EventType.ADDED:
            parts.extend(self._indent(new_yaml))
        elif ev.type_ == watcher.EventType.DELETED:
            parts.extend(self._indent(new_yaml))
        else:
            import difflib
            parts.extend(self._indent(new_yaml))
            parts.append("  --- BEGIN OF DIFF ---")
            parts.extend(
                "  " + line
                for line in difflib.unified_diff(old_yaml, new_yaml,
                                                 lineterm="")
            )
            parts.append("  --- END OF DIFF ---")
        return "\n".join(parts)


class PatchingApiClient(kubernetes_asyncio.client.ApiClient):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__bufsize_patched = False

    def call_api(self, resource_path, method,
                 path_params=None,
                 query_params=None,
                 header_params=None,
                 body=None, *args, **kwargs):
        if method == "PATCH":
            if isinstance(body, list):
                header_params["Content-Type"] = "application/json-patch+json"
            elif isinstance(body, bytes):
                header_params["Content-Type"] = "application/apply-patch+yaml"
        return super().call_api(resource_path, method,
                                path_params,
                                query_params,
                                header_params,
                                body,
                                *args,
                                **kwargs)

    def select_header_content_type(self, content_type, method, body):
        if "application/apply-patch+yaml" in content_type:
            result = "application/apply-patch+yaml"
        else:
            result = super().select_header_content_type(
                content_type, method, body)
        return result

    async def __aenter__(self):
        if self.__bufsize_patched:
            raise RuntimeError("entered twice")
        self.__bufsize_patched = True
        connector = self.rest_client.pool_manager.connector
        # ugly...
        self.rest_client.pool_manager._connector_owner = False
        await self.rest_client.pool_manager.close()
        self.rest_client.pool_manager = aiohttp.ClientSession(
            connector=connector,
            # 1 MiB
            read_bufsize=2**20,
        )
        return await super().__aenter__()


def make_builtin_stream_factory(client_class,
                                unnamespaced_func_name,
                                namespaced_func_name=None):
    # This is a no-op in order to raise AttributeError early if the user has
    # a typo in `namespaced_func_name` / `unnamespaced_func_name`.
    getattr(client_class, unnamespaced_func_name)
    if namespaced_func_name is None:
        def make_stream(api_client, namespace=None):
            api = client_class(api_client)
            func = getattr(api, unnamespaced_func_name)
            return func

    else:
        # See above.
        getattr(client_class, namespaced_func_name)

        def make_stream(api_client, namespace=None):
            api = client_class(api_client)

            if namespace is None:
                return getattr(api, unnamespaced_func_name)

            func = getattr(api, namespaced_func_name)
            return functools.wraps(func)(
                functools.partial(func, namespace=namespace),
            )

    def make_stream_factory(namespace=None):
        def wrapped_make_stream(api_client):
            return make_stream(api_client, namespace=namespace)
        return wrapped_make_stream

    return make_stream_factory


def make_cr_stream_factory(api_group, version, plural):
    def make_stream(api_client, namespace=None):
        cr_api = kubernetes_asyncio.client.CustomObjectsApi(api_client)

        if namespace is None:
            return functools.wraps(
                cr_api.list_cluster_custom_object
            )(functools.partial(
                cr_api.list_cluster_custom_object,
                api_group, version, plural,
            ))

        return functools.wraps(
            cr_api.list_namespaced_custom_object
        )(functools.partial(
            cr_api.list_namespaced_custom_object,
            api_group, version, namespace, plural,
        ))

    def make_stream_factory(namespace=None):
        def wrapped_make_stream(api_client):
            return make_stream(api_client, namespace=namespace)
        return wrapped_make_stream

    return make_stream_factory


def ref_matches_cr_obj(
        ref: sm.ResourceReference,
        cr_obj: sm.CustomResource) -> bool:
    api_version = sm.join_api_version(cr_obj.API_GROUP,
                                      cr_obj.API_GROUP_VERSION)
    if api_version != ref.api_version:
        return False
    if cr_obj.PLURAL.lower() != ref.plural.lower():
        return False
    return True


def get_cr_logger(
        base: logging.Logger,
        cr_obj: sm.CustomResource,
        namespace: str,
        name: str) -> logging.Logger:
    result = base.getChild(
        sm.join_api_version(cr_obj.API_GROUP, cr_obj.API_GROUP_VERSION)
    ).getChild(
        f"{cr_obj.PLURAL}",
    ).getChild(namespace).getChild(name)
    return result


def get_cr_field_manager(
        cr_obj: sm.CustomResource,
        ) -> str:
    return f"operator.yaook.cloud:{cr_obj.PLURAL}.{cr_obj.API_GROUP}"


EventKey = typing.Tuple[str, str, str]

# This magic mapping is required because the kubernetes API is not
# consistent in its method naming. We thus have to configure, for each
# object type we want to watch, how to watch for it.
AVAILABLE_WATCHERS: typing.Mapping[EventKey, typing.Callable] = {
    ('yaook.cloud', 'v1', 'keystonedeployments'):
        make_cr_stream_factory(
            'yaook.cloud', 'v1', 'keystonedeployments',
        ),
    ('yaook.cloud', 'v1', 'externalkeystonedeployments'):
        make_cr_stream_factory(
            'yaook.cloud', 'v1', 'externalkeystonedeployments',
        ),
    ('yaook.cloud', 'v1', 'keystoneusers'):
        make_cr_stream_factory(
            'yaook.cloud', 'v1', 'keystoneusers',
        ),
    ('yaook.cloud', 'v1', 'keystoneendpoints'):
        make_cr_stream_factory(
            'yaook.cloud', 'v1', 'keystoneendpoints',
        ),
    ('compute.yaook.cloud', 'v1', 'novacomputenodes'):
        make_cr_stream_factory(
            'compute.yaook.cloud', 'v1', 'novacomputenodes',
        ),
    ('compute.yaook.cloud', 'v1', 'novahostaggregates'):
        make_cr_stream_factory(
            'compute.yaook.cloud', 'v1', 'novahostaggregates',
        ),
    ('compute.yaook.cloud', 'v1', 'sshidentities'):
        make_cr_stream_factory(
            'compute.yaook.cloud', 'v1', 'sshidentities',
        ),
    ('infra.yaook.cloud', 'v1', 'amqpservers'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'amqpservers',
        ),
    ('infra.yaook.cloud', 'v1', 'amqpusers'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'amqpusers',
        ),
    ('infra.yaook.cloud', 'v1', 'mysqlservices'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'mysqlservices',
        ),
    ('infra.yaook.cloud', 'v1', 'mysqlusers'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'mysqlusers',
        ),
    ('infra.yaook.cloud', 'v1', 'memcachedservices'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'memcachedservices',
        ),
    ('infra.yaook.cloud', 'v1', 'ovsdbservices'):
        make_cr_stream_factory(
            'infra.yaook.cloud', 'v1', 'ovsdbservices',
        ),
    ('network.yaook.cloud', 'v1', 'neutrondhcpagents'):
        make_cr_stream_factory(
            'network.yaook.cloud', 'v1', 'neutrondhcpagents',
        ),
    ('network.yaook.cloud', 'v1', 'neutronl2agents'):
        make_cr_stream_factory(
            'network.yaook.cloud', 'v1', 'neutronl2agents',
        ),
    ('network.yaook.cloud', 'v1', 'neutronl3agents'):
        make_cr_stream_factory(
            'network.yaook.cloud', 'v1', 'neutronl3agents',
        ),
    ('network.yaook.cloud', 'v1', 'neutronovnagents'):
        make_cr_stream_factory(
            'network.yaook.cloud', 'v1', 'neutronovnagents',
        ),
    ('network.yaook.cloud', 'v1', 'neutronbgpdragents'):
        make_cr_stream_factory(
            'network.yaook.cloud', 'v1', 'neutronbgpdragents',
        ),
    ('cert-manager.io', 'v1', 'certificates'):
        make_cr_stream_factory(
            'cert-manager.io', 'v1', 'certificates',
        ),
    ('cert-manager.io', 'v1', 'issuers'):
        make_cr_stream_factory(
            'cert-manager.io', 'v1', 'issuers',
        ),
    ('batch', 'v1', 'jobs'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.BatchV1Api,
            "list_job_for_all_namespaces",
            "list_namespaced_job",
        ),
    ('', 'v1', 'pods'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.CoreV1Api,
            "list_pod_for_all_namespaces",
            "list_namespaced_pod",
        ),
    ('', 'v1', 'nodes'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.CoreV1Api,
            "list_node",
        ),
    ('', 'v1', 'secrets'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.CoreV1Api,
            "list_secret_for_all_namespaces",
            "list_namespaced_secret",
        ),
    ('', 'v1', 'configmaps'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.CoreV1Api,
            "list_config_map_for_all_namespaces",
            "list_namespaced_config_map",
        ),
    ('', 'v1', 'services'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.CoreV1Api,
            "list_service_for_all_namespaces",
            "list_namespaced_service",
        ),
    ('apps', 'v1', 'statefulsets'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.AppsV1Api,
            "list_stateful_set_for_all_namespaces",
            "list_namespaced_stateful_set",
        ),
    ('apps', 'v1', 'deployments'):
        make_builtin_stream_factory(
            kubernetes_asyncio.client.AppsV1Api,
            "list_deployment_for_all_namespaces",
            "list_namespaced_deployment",
        ),
}


class ReconcileReason(enum.Enum):
    OBJECT_ADDED = "ObjectAdded"
    OBJECT_MODIFIED = "ObjectModified"
    LISTENER = "Listener"
    RETRY = "Retry"
    INTERVAL = "Interval"


class OperatorDaemon:
    """
    Run a Yaook operator.

    This class is the main point of contact with the Kubernetes API. It
    initialises the API clients used when reconciling resources and watches
    the custom resources themselves as well as their dependencies (as declared
    by :meth:`~.Resource.get_listener`) and triggers reconciliation runs as
    required.
    """
    def __init__(self,
                 config: kubernetes_asyncio.client.Configuration,
                 namespace: str,
                 runner_count: int):
        self.logger = logging.getLogger(__name__)
        self._namespace = namespace
        self._task_queue = tasks.TaskQueue()
        self._task_queue_runners = [
            tasks.RestartingTask(
                self._task_queue.run,
                logger=self.logger.getChild(f"runner{runner + 1}"),
            )
            for runner in range(runner_count)
        ]

        self._config = config

        # Structure to store the listeners declared by custom resources. The
        # event key is a tuple consisting of the API group, its version and
        # the plural of the resource.
        self._listeners: \
            typing.Dict[
                EventKey,
                typing.List[typing.Tuple[
                    sm.CustomResource, sm.KubernetesListener]]
                ] \
            = {}

        # Structure to store the external listeners declared by custom
        # resources.
        self._external_listeners: \
            typing.Dict[
                watcher.ExternalWatcher,
                typing.List[typing.Tuple[
                    sm.CustomResource, sm.ListenerCallback]]
                ] \
            = {}

        # Structure to store the custom resources this operator manages.
        self._crs: typing.Dict[EventKey, sm.CustomResource] = {}

        # K8s watchers for the custom resources.
        self._cr_watches: \
            typing.Dict[
                sm.CustomResource,
                watcher.StatefulWatcher[typing.Mapping],
            ] \
            = {}

        # K8s watchers for all other resources.
        self._builtin_watches: \
            typing.Dict[
                EventKey,
                watcher.StatefulWatcher[typing.Mapping],
            ] \
            = {}

        # Bookkeeping for the reasons a reconcile was triggered. To avoid
        # reconcile loops if a resource changes itself (e.g. because of status
        # updates), we need to know why a reconcile was triggered when it is
        # scheduled in order to decide whether to actually execute it.
        #
        # See :meth:`_reconcile_cr`.
        self._reconcile_reasons: \
            typing.Dict[
                typing.Tuple[sm.CustomResource, str, str],
                typing.Set[ReconcileReason],
            ] \
            = {}

    def _add_reconcile_reason(
            self,
            cr_obj: sm.CustomResource,
            namespace: str,
            name: str,
            reason: ReconcileReason) -> None:
        """
        Add a reconciliation reason for a custom resource instance to the
        bookkeeping.
        """
        self._reconcile_reasons.setdefault(
            (cr_obj, namespace, name), set(),
        ).add(reason)

    def _pop_reconcile_reasons(
            self,
            cr_obj: sm.CustomResource,
            namespace: str,
            name: str) -> typing.Set[ReconcileReason]:
        """
        Atomically remove and return the current reconciliation reasons for a
        given custom resource instance.

        Returns an empty set if no reasons have been added.
        """
        return self._reconcile_reasons.pop((cr_obj, namespace, name), set())

    def _make_client(self) -> kubernetes_asyncio.client.ApiClient:
        """
        Create an appropriate Kubernetes API client.

        .. note::

            The API client needs to be "enabled" using an `async with` block,
            otherwise it will leak resources.
        """
        return PatchingApiClient(self._config)

    async def _schedule_periodic_reconcile(self):
        """
        Infinite loop to regularly trigger reconciles for all known resources.

        This method regularly enqueues a reconcile operation
        for all custom resources currently known to the operator that did
        not reconcile recently.

        For more details see :meth:`_do_schedule_periodic_reconcile`
        """
        while True:
            await self._do_schedule_periodic_reconcile()

    async def _do_schedule_periodic_reconcile(self):
        """
        After sleeping for 3500-3700s (randomized), this method enqueues
        a reconcile operation for all custom resources currently known
        to the operator that did not reconcile recently.

        This uses the cached information from the watcher.
        """
        delay = random.randint(3500, 3700)  # nosemgrep as the random values
        # are not for cryptgraphic usage
        self.logger.debug(
            "scheduling next periodic reconcile in %d seconds", delay,
        )
        await asyncio.sleep(delay)
        for cr_obj, cr_watch in self._cr_watches.items():
            instances = list(cr_watch.get_all())
            for raw_object, _ in instances:
                namespace = raw_object["metadata"]["namespace"]
                name = raw_object["metadata"]["name"]
                self.logger.debug(
                    "triggering periodic reconcile for %s/%s of %s",
                    namespace, name, cr_obj,
                )
                self._add_reconcile_reason(cr_obj, namespace, name,
                                           ReconcileReason.INTERVAL)
                self._task_queue.push(
                    self._reconcile_cr,
                    (cr_obj, namespace, name),
                )
            self.logger.info(
                "triggered periodic reconciles for %d instances of %s",
                len(instances), cr_obj,
            )

    def _needs_self_reconcile(
            self,
            new_raw_object: typing.Mapping,
            ) -> bool:
        """
        Test whether the resource needs a reconcile due to changes to itself.

        This uses the `observedGeneration` field included in all of the
        resources managed by :class:`~.CustomResource` to determine whether
        the spec of the resource has changed since the last successful
        reconcile.

        Another criterium for returning true is if the last reconcile was not
        successful.
        """
        new_generation = new_raw_object.get(
            "metadata", {},
        ).get("generation", 0)
        status = new_raw_object.get("status", {})
        if not status:
            return True
        phase = status.get("phase")
        if phase not in [context.Phase.UPDATED.value,
                         context.Phase.WAITING_FOR_DEPENDENCY.value]:
            return True
        observed_generation = status.get("observedGeneration")
        if observed_generation != new_generation:
            return True
        return False

    def _needs_interval_reconcile(
            self,
            new_raw_object: typing.Mapping,
            ) -> bool:
        """
        Test whether the resource needs a reconcile due to it not having
        reconciled recently.
        """
        conditions = new_raw_object.get("status", {}).get("conditions", [])
        if not conditions:
            return True
        for condition in conditions:
            if (condition["type"] ==
                    customresource.CommonConditionTypes.CONVERGED.value):
                last_update_time = condition["lastUpdateTime"]
                timestamp = api_utils.parse_timestamp(last_update_time)
                return timestamp < (datetime.datetime.utcnow() -
                                    datetime.timedelta(minutes=30))
        return True

    def _build_cr_context(
            self,
            api_client: kubernetes_asyncio.client.ApiClient,
            cr_obj: sm.CustomResource,
            namespace: str,
            instance: typing.Mapping,
            ) -> sm.Context:
        """
        Construct a context for a given custom resource instance.
        """
        name = instance["metadata"]["name"]
        return sm.Context(
            parent=instance,
            parent_intf=cr_obj.get_resource_interface(api_client),
            namespace=namespace,
            api_client=api_client,
            instance=None,
            instance_data=None,
            logger=get_cr_logger(self.logger, cr_obj, namespace, name),
            field_manager=get_cr_field_manager(cr_obj),
        )

    async def _reconcile_cr(
            self,
            cr_obj: sm.CustomResource,
            namespace: str,
            name: str) -> None:
        """
        Execute reconciliation of a custom resource instance, conditionally.

        After establishing the context and obtaining the current version of the
        instance (from the API), this method validates whether a reconcile is
        appropriate.

        The reconcile is executed if and only if:

        - The resource generation has changed since the last reconcile
          (hence, observedGeneration not equal to generation).
        - The last reconciliation run was not successful
        - A listener has requested a reconciliation run since the last
          reconciliation run has started.
        - The resource was newly added (or reported as newly added because of
          a restarted watch).
        - The interval for regular reconciles expired

        Most notably, no reconciliation is executed if only the `status` of
        the resource has changed (in which case the k8s API does not increase
        the generation number) to avoid reconciliation loops.
        """
        # we need to fetch the reasons first so that we catch any new reasons
        # in a new run
        reasons = self._pop_reconcile_reasons(cr_obj, namespace, name)
        async with self._make_client() as api:
            intf = cr_obj.get_resource_interface(api)
            try:
                obj = await intf.read(namespace, name)
            except kubernetes_asyncio.client.rest.ApiException as exc:
                if exc.status == 404:
                    self.logger.info("%s %s/%s vanished, discarding task",
                                     cr_obj, namespace, name)
                    return
                raise

            if (context.ANNOTATION_PAUSE in
                    obj["metadata"].get("annotations", {})):
                self.logger.info(
                    "requested to reconcile %s/%s (because of %s), but it has "
                    "the %s annotation set. doing nothing.",
                    namespace,
                    name,
                    reasons,
                    context.ANNOTATION_PAUSE,
                )
                return

            ctx = self._build_cr_context(api, cr_obj, namespace, obj)
            generation = obj["metadata"]["generation"]
            if not reasons:
                self.logger.warning(
                    "internal state issue: no reasons for reconcile for %s/%s "
                    "of %s, but I am going to reconcile! making one up to be "
                    "sure...",
                    namespace, name, cr_obj,
                )
                # make a reason which cannot be discarded again
                reasons.add(typing.cast(ReconcileReason, object()))

            # If the resource itself does not have a reason to reconcile
            # (observedGeneration == generation and last run successful /
            # waiting for dependency) then we pretend that we did not get
            # notified about a change to prevent reconcile loops.
            if not self._needs_self_reconcile(obj):
                reasons.discard(ReconcileReason.OBJECT_MODIFIED)

            # If the resource has been reconciled recently we do not need to
            # reconcile it due to the interval.
            if not self._needs_interval_reconcile(obj):
                reasons.discard(ReconcileReason.INTERVAL)

            if not reasons:
                ctx.logger.info(
                    "skipping reconcile; it was triggered by a CR change "
                    "event or by a timer, but the CR was successfully "
                    "reconciled in the meantime"
                )
                return

            ctx.logger.debug("reconciling because of %r", reasons)

            try:
                await cr_obj.reconcile(ctx, generation)
            except sm.TriggerReconcile as exc:
                ctx.logger.debug(
                    "CR triggered a reconcile because of %s", exc.message
                )
                self._add_reconcile_reason(cr_obj, namespace, name,
                                           ReconcileReason.RETRY)
                self._task_queue.push(
                    self._reconcile_cr,
                    (cr_obj, namespace, name),
                )
            except Exception:
                # Make sure we always re-reconcile even on no changes after an
                # error in case it is transient.
                self._add_reconcile_reason(cr_obj, namespace, name,
                                           ReconcileReason.RETRY)
                raise

    def _judge_cr_watch_event(
            self,
            cr_obj: sm.CustomResource,
            ev: watcher.StatefulWatchEvent[typing.Mapping],
            ) -> typing.Optional[ReconcileReason]:
        """
        Return a reconcile reason, if any, for the given event on a
        custom resource watch.

        If the resource was deleted, we do not reconcile (because there is
        nothing left to reconcile).

        If the resource was added, a reconcile is triggered with
        :attr:`ReconcileReason.OBJECT_ADDED`.

        If the resource was modified, a reconcile is triggered with
        :attr:`ReconcileReason.OBJECT_MODIFIED`, but only if one of the
        following conditions is true:

        - The ``generation`` has changed
        - The ``deletionTimestamp`` has changed

        If the resource was modified, but the old object is not present on
        the event, a reconcile is triggered with
        :attr:`ReconcileReason.OBJECT_ADDED`, to force it to happen even if all
        code agrees that the object may not have been modified. This is to
        ensure that no false-negative judgement is made because of missing
        data.
        """
        if ev.type_ == watcher.EventType.ADDED:
            return ReconcileReason.OBJECT_ADDED
        if ev.type_ == watcher.EventType.DELETED:
            return None
        if ev.old_object is None:
            return ReconcileReason.OBJECT_ADDED

        # use object() here to get two values which will for sure compaer
        # unequal in case both generations are missing
        prev_generation = ev.old_raw_object["metadata"].get(
            "generation", object()
        )
        curr_generation = ev.raw_object["metadata"].get("generation", object())
        if prev_generation != curr_generation:
            return ReconcileReason.OBJECT_MODIFIED

        prev_deletion_timestamp = ev.old_raw_object["metadata"].get(
            "deletionTimestamp",
        )
        curr_deletion_timestamp = ev.raw_object["metadata"].get(
            "deletionTimestamp",
        )
        if prev_deletion_timestamp != curr_deletion_timestamp:
            return ReconcileReason.OBJECT_MODIFIED

        prev_paused = context.ANNOTATION_PAUSE in \
            ev.old_raw_object["metadata"].get("annotations", {})
        curr_paused = context.ANNOTATION_PAUSE in \
            ev.raw_object["metadata"].get("annotations", {})
        if prev_paused and not curr_paused:
            # using OBJECT_ADDED here because it is slightly stronger than
            # OBJECT_MODIFIED. We have to assume that during pause, someone
            # messed with *something* related to the object, so we should force
            # a reconcile.
            return ReconcileReason.OBJECT_ADDED
        return None

    async def _watch_cr(self, cr_obj: sm.CustomResource) -> bool:
        """
        Watch a custom resource for changes and trigger reconciles.

        This method triggers reconciliation runs with the appropriate
        ReconcileReasons whenever a custom resource instance has been added or
        modified.

        We do not reconcile on deletion, because there’s nothing left to
        reconcile.

        It is the job of :meth:`_reconcile_cr` and the task queue to
        deduplicate reconciliation requests.

        This method always returns true in order to get automatically restarted
        by a RestartingTask.
        """
        self.logger.info("setting up watcher for %r", cr_obj)
        watch = self._cr_watches[cr_obj]
        async with self._make_client() as api:
            async for ev in watch.stream_events(api):
                reason = self._judge_cr_watch_event(cr_obj, ev)
                if reason is None:
                    continue

                self._add_reconcile_reason(
                    cr_obj, ev.reference.namespace, ev.reference.name,
                    reason,
                )
                self._task_queue.push(
                    self._reconcile_cr,
                    (cr_obj, ev.reference.namespace, ev.reference.name)
                )
            # this happens when the server closes the watch. we log this as
            # debug, but we restart the watch immediately
            self.logger.debug("server closed watch for %r, restarting",
                              cr_obj)
        return True

    async def _watch_builtin(
            self,
            ev_key: EventKey,
            stream_factory: typing.Callable,
            ) -> bool:
        """
        Watch a dependency resource using the kubernetes API.

        Whenever a change to that resource occurs, it is delivered
        synchronously and blockingly to :meth:`_dispatch_event`.

        This method always returns true in order to get automatically restarted
        by a RestartingTask.
        """
        ev_name = "{}.{}/{}".format(ev_key[2], ev_key[0], ev_key[1])
        self.logger.info("setting up watcher for %s", ev_name)
        try:
            w = self._builtin_watches[ev_key]
        except KeyError:
            w = self._builtin_watches[ev_key] = watcher.StatefulWatcher[T](
                stream_factory(namespace=self._namespace),
                restore_state_on_restart=True,
            )
        async with self._make_client() as api:
            async for ev in w.stream_events(api):
                self._dispatch_event(api, ev_key, ev)
            # this happens when the server closes the watch. we log this
            # as debug, but we restart the watch immediately
            self.logger.debug("server closed watch for %s, restarting",
                              ev_name)
        return True

    async def _watch_external(
            self,
            watcher: watcher.ExternalWatcher,
            ) -> bool:
        """
        Watch a dependency resource outside of Kubernetes.

        Whenever a change to that resource occurs, it is delivered
        synchronously and blockingly.

        This method always returns true in order to get automatically restarted
        by a RestartingTask.
        """
        self.logger.info("setting up watcher for %s", watcher.__class__)
        async with self._make_client() as api:
            async for ev in watcher.stream_events():
                self._dispatch_external_event(api, watcher, ev)
        return True

    def _deliver_event(
            self,
            func: sm.ListenerCallback[T],
            cr_obj: sm.CustomResource,
            ctx: sm.Context,
            event: watcher.StatefulWatchEvent[T],
            ) -> None:
        """
        Deliver an event to a single listener.

        If the listener returns true, a reconcile run is triggered with an
        appropriate reconcile reason in order to force it to execute. This
        is required for tracking dependent resources.
        """
        trigger_reconcile = func(ctx, event)
        if trigger_reconcile:
            self.logger.debug(
                "triggering reconcile of %s %s/%s because of listener %s",
                cr_obj,
                ctx.namespace, ctx.parent_name,
                func,
            )
            self.logger.debug(
                "involved diff: %s",
                lazy_event_diff(event),
            )
            self._add_reconcile_reason(
                cr_obj, ctx.namespace, ctx.parent_name,
                ReconcileReason.LISTENER,
            )
            self._task_queue.push(
                self._reconcile_cr,
                (cr_obj, ctx.namespace, ctx.parent_name),
            )

    def _broadcast_event(
            self,
            listener_func: sm.ListenerCallback[T],
            api_client: kubernetes_asyncio.client.ApiClient,
            cr_obj: sm.CustomResource,
            event: watcher.StatefulWatchEvent[T]) -> None:
        """
        Broadcast an event to a listener on all custom resource instances of
        a given type.
        """
        watch = self._cr_watches[cr_obj]
        for _, cr_body in watch.get_all():
            ctx = sm.Context(
                api_client=api_client,
                logger=self.logger,
                namespace=cr_body["metadata"]["namespace"],
                parent=cr_body,
                parent_intf=cr_obj.get_resource_interface(api_client),
                field_manager=get_cr_field_manager(cr_obj),
                instance=None,
                instance_data=None,
            )

            self.logger.debug(
                "event on object %s/%s dispatched to listener %s for %r %s/%s",
                event.reference.namespace,
                event.reference.name,
                listener_func,
                cr_obj,
                ctx.namespace,
                ctx.parent_name,
            )
            self._deliver_event(
                listener_func,
                cr_obj,
                ctx,
                event,
            )

    def _dispatch_event(
            self,
            api_client: kubernetes_asyncio.client.ApiClient,
            ev_key: EventKey,
            event: watcher.StatefulWatchEvent[T]) -> None:
        """
        Dispatch an event on a dependency resource to the corresponding
        listener.

        For broadcast listeners, the event is delivered to all custom resource
        instances which have such a broadcast listener registered.

        For non-broadcast listeners, the event is delivered only to those
        custom resource instances whose context labels are present on the
        object where the event occured.

        Event dispatching happens synchronously and blockingly. API requests
        are not allowed during event processing. Hence this method bases its
        operations solely on watched and resource data.
        """

        # even though we could use the raw_object metadata here, we use
        # extract metadata to exploit the guarantees it provides us with.
        obj_metadata = sm.extract_metadata(event.object_)
        obj_name: str = event.reference.name
        obj_namespace: typing.Optional[str] = event.reference.namespace
        obj_component = obj_metadata["labels"].get(sm.context.LABEL_COMPONENT)

        self.logger.debug(
            "dispatching %s event on object %s/%s of type %s.%s/%s",
            event.type_,
            obj_namespace,
            obj_name,
            ev_key[2], ev_key[0], ev_key[1],
        )
        recovered_ref = None
        try:
            recovered_ref, instance = sm.recover_parent_reference(obj_metadata)
        except ValueError:
            self.logger.debug("object %s/%s is not directly managed",
                              obj_namespace, obj_name)

        for cr_obj, listener in self._listeners.get(ev_key, []):
            if listener.broadcast:
                if not listener.broadcast_filter or \
                        listener.broadcast_filter.items() <= \
                        obj_metadata.get("labels", {}).items():
                    # broadcast listeners recover their context from the
                    # cached resources instead of from the object which had
                    # the event.
                    self.logger.debug("event on object %s/%s will be "
                                      "broadcast to listener %s for all "
                                      "instances of %r", obj_namespace,
                                      obj_name, listener.listener, cr_obj)
                    self._broadcast_event(
                        listener.listener,
                        api_client,
                        cr_obj,
                        event,
                    )
                else:
                    self.logger.debug("event on object %s/%s won't be "
                                      "broadcasted as the filter does not "
                                      "match.", obj_namespace, obj_name)
                continue

            # If we were not able to recover a custom resource reference for
            # the affected object, we have to skip all non-broadcast listeners
            # since we won’t be able to deliver it to any.
            if recovered_ref is None:
                continue

            # Only deliver to instances which match the labels on the object.
            if not ref_matches_cr_obj(recovered_ref, cr_obj):
                # event does not concern this listener
                continue

            # If the listener restricts to a specific component, ensure that
            # this restriction is respected.
            if (listener.component is not None and
                    obj_component != listener.component):
                self.logger.debug(
                    "object %s/%s does not match component for listener with "
                    "the component %s (!= object component %s)",
                    obj_namespace, obj_name, listener.component, obj_component,
                )
                continue

            # We have to recover the custom resource body from the CR watcher
            # as we cannot execute API calls.
            watch = self._cr_watches[cr_obj]
            try:
                body, _ = watch.get_by_name(
                    recovered_ref.namespace,
                    recovered_ref.name,
                )
            except KeyError:
                self.logger.debug(
                    "event triggered by object %s/%s dropped because the "
                    "resource %s %s/%s it belongs to is not known",
                    obj_namespace, obj_name,
                    cr_obj,
                    recovered_ref.namespace,
                    recovered_ref.name,
                )
                continue

            # Finally, we can assemble the complete context and deliver the
            # event.
            ctx = sm.Context(
                api_client=api_client,
                logger=self.logger,
                namespace=typing.cast(str, recovered_ref.namespace),
                parent=body,
                parent_intf=cr_obj.get_resource_interface(api_client),
                field_manager=get_cr_field_manager(cr_obj),
                instance=instance,
                instance_data=None,
            )

            self._deliver_event(
                listener.listener,
                cr_obj,
                ctx,
                event,
            )

    def _dispatch_external_event(
            self,
            api_client: kubernetes_asyncio.client.ApiClient,
            watcher: watcher.ExternalWatcher,
            event: watcher.StatefulWatchEvent[T]) -> None:
        """
        Dispatch an external event on a dependency resource to the
        corresponding lsiteners.

        Event dispatching happens synchronously and blockingly. API requests
        are not allowed during event processing. Hence this method bases its
        operations solely on watched and resource data.
        """

        self.logger.debug(
            "dispatching external %s event on object %s of type %s",
            event.type_,
            event.object_,
            watcher.__class__,
        )

        for cr_obj, listener in self._external_listeners.get(watcher, []):
            # We have to recover the custom resource body from the CR watcher
            # as we cannot execute API calls.
            watch = self._cr_watches[cr_obj]
            try:
                body, _ = watch.get_by_name(
                    event.reference.namespace,
                    event.reference.name,
                )
            except KeyError:
                self.logger.debug(
                    "external event dropped because the "
                    "resource %s %s/%s it belongs to is not known",
                    cr_obj,
                    event.reference.namespace,
                    event.reference.name,
                )
                continue

            # Finally, we can assemble the complete context and deliver the
            # event.
            ctx = sm.Context(
                api_client=api_client,
                logger=self.logger,
                namespace=body["metadata"]["namespace"],
                parent=body,
                parent_intf=cr_obj.get_resource_interface(api_client),
                field_manager=get_cr_field_manager(cr_obj),
                instance=None,
                instance_data=None,
            )

            self._deliver_event(
                listener,
                cr_obj,
                ctx,
                event,
            )

    async def run(self) -> None:
        """
        Start background tasks to implement the operator and wait for a signal
        to terminate.

        A proper shutdown can be initiated by cancelling this coroutine or
        sending SIGTERM or SIGINT signals to the process.

        For each custom resource registered in this process, watches are
        established and reconciliation including cleanup schedules are
        installed. For all listeners of all custom resources watches are
        installed in order to track them.

        The task queue is initialised and its execution is started.
        """
        stop_signal = asyncio.Event()
        loop = asyncio.get_event_loop()
        loop.add_signal_handler(signal.SIGTERM, stop_signal.set)
        loop.add_signal_handler(signal.SIGINT, stop_signal.set)

        task_list = self._task_queue_runners + [
            tasks.RestartingTask(
                self._schedule_periodic_reconcile,
                logger=self.logger,
            ),
        ]

        try:
            for plural, group, version, cr_cls in \
                    sm.registry.registry.values():
                cr_obj = cr_cls(logger=self.logger.getChild(plural))
                ev_key = group, version, plural
                self._crs[ev_key] = cr_obj
                self._cr_watches[cr_obj] = watcher.StatefulWatcher[
                    typing.Mapping
                ](make_cr_stream_factory(group, version, plural)(None),
                  restore_state_on_restart=True)

                for listener in cr_obj.get_listeners():
                    if isinstance(listener, context.KubernetesListener):
                        self._listeners.setdefault(
                            listener.object_key, []).append(
                                (cr_obj, listener)
                        )
                    elif isinstance(listener, context.ExternalListener):
                        self._external_listeners.setdefault(
                            listener.watcher, []).append(
                                (cr_obj, listener.listener)
                        )
                    else:
                        raise TypeError(
                            f"The listener {listener} is neither a Kubernetes-"
                            "nor an ExternalListener."
                        )

                task_list.append(tasks.RestartingTask(
                    functools.partial(self._watch_cr, cr_obj),
                ))

            for listen_key, listeners in self._listeners.items():
                if not listeners:
                    continue

                try:
                    stream_builder = AVAILABLE_WATCHERS[listen_key]
                except KeyError:
                    raise RuntimeError(
                        "I don’t know how to watch for {}".format(listen_key)
                    )

                task_list.append(
                    tasks.RestartingTask(
                        functools.partial(self._watch_builtin,
                                          listen_key,
                                          stream_builder)
                    )
                )

            for externalwatcher, ext_listeners in \
                    self._external_listeners.items():
                if not ext_listeners:
                    continue

                task_list.append(
                    tasks.RestartingTask(
                        functools.partial(self._watch_external,
                                          externalwatcher)
                    )
                )

            for task in task_list:
                task.start()

            await stop_signal.wait()
        finally:
            for task in task_list:
                task.stop()
            # wait_for_termination should never raise; if it raises, that is an
            # implementation problem which needs to be noticed
            await asyncio.gather(*(
                task.wait_for_termination()
                for task in task_list
            ))
