#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import hashlib
import json
import typing

import kubernetes_asyncio.client

import yaook.statemachine as sm
from yaook.statemachine.context import LABEL_CDS_CONTROLLER_UID
import yaook.statemachine.customresource as customresource
import yaook.statemachine.resources.instancing as instancing
import yaook.statemachine.watcher as watcher


HASH_VERSION = 2
CRITICAL_KEYS = ["metadata", "spec"]
HASH_ANNOTATION = "apps.yaook.cloud/configureddaemonset-hash"


def hash_pod(spec: typing.Mapping) -> str:
    """
    Calculate a hash over the "critical" keys of the Pod spec and return it as
    string.

    The hash is intended to be different whenever a part of the Pod spec
    changes which may require re-creating the Pod.
    """
    partial_spec = {
        k: spec[k]
        for k in CRITICAL_KEYS
        if k in spec
    }
    fun = hashlib.sha256()
    fun.update(json.dumps(partial_spec, sort_keys=True).encode("utf-8"))
    return "{}.{}".format(HASH_VERSION, fun.hexdigest())


def _add_tolerations(
        tolerations: typing.MutableSequence[typing.Mapping[str, str]],
        ) -> None:
    """
    Add tolerations which avoid evicting a CDS Pod.

    The tolerations are mostly stolen from a normal DaemonSet.
    """
    tolerations.append({
        "effect": "NoExecute",
        "key": "node.kubernetes.io/not-ready",
        "operator": "Exists",
    })
    tolerations.append({
        "effect": "NoExecute",
        "key": "node.kubernetes.io/unreachable",
        "operator": "Exists",
    })
    tolerations.append({
        "effect": "NoSchedule",
        "key": "node.kubernetes.io/disk-pressure",
        "operator": "Exists",
    })
    tolerations.append({
        "effect": "NoSchedule",
        "key": "node.kubernetes.io/memory-pressure",
        "operator": "Exists",
    })
    tolerations.append({
        "effect": "NoSchedule",
        "key": "node.kubernetes.io/pid-pressure",
        "operator": "Exists",
    })
    tolerations.append({
        "effect": "NoSchedule",
        "key": "node.kubernetes.io/unschedulable",
        "operator": "Exists",
    })


def _set_node_affinity(
        nodeSelectorTerms: typing.MutableSequence[
            typing.Mapping[str, typing.Any],
        ],
        nodename: str,
        ) -> None:
    nodeSelectorTerms.clear()
    nodeSelectorTerms.append({
        "matchFields": [
            {
                "key": "metadata.name",
                "operator": "In",
                "values": [nodename],
            }
        ]
    })


def _update_volumes(
        pod_volumes: typing.Sequence[typing.Mapping],
        volume_templates: typing.Sequence[typing.Mapping],
        node: str) -> typing.List[typing.Mapping]:
    """
    Append the instantiated per-Node volumes of a CDS Pod to a list of volumes
    in-place.
    """
    result = list(pod_volumes)
    existing_volumes = set(v["name"] for v in result)
    added_volumes = set()

    for template in volume_templates:
        volume_name = template["volumeName"]
        if volume_name in existing_volumes:
            raise ValueError(
                f"volume {volume_name!r} exists already inside pod",
            )
        if volume_name in added_volumes:
            raise ValueError(
                f"volume {volume_name!r} is declared multiple times",
            )

        cfg = template.get("nodeMap", {}).get(
            node,
            template.get("default"),
        )
        if cfg is None:
            raise ValueError(
                f"no volume template for volume {volume_name!r} on {node!r}"
            )

        instance = copy.deepcopy(cfg["template"])
        instance["name"] = volume_name
        result.append(instance)
        added_volumes.add(volume_name)

    return result


def make_pod(cds_spec: typing.Mapping, node: str) -> sm.ResourceBody:
    """
    Instantiate a Pod from a `cds_spec` for a `node`.

    Besides instantiating the volume configuration for the given node, this
    also adds the necessary tolerations and affinity rules for CDS Pods.
    """
    pod_instance = copy.deepcopy(cds_spec["template"])
    pod_instance["apiVersion"] = "v1"
    pod_instance["kind"] = "Pod"
    _add_tolerations(pod_instance["spec"].setdefault("tolerations", []))
    _set_node_affinity(
        pod_instance["spec"].setdefault(
            "affinity", {},
        ).setdefault(
            "nodeAffinity", {},
        ).setdefault(
            "requiredDuringSchedulingIgnoredDuringExecution", {},
        ).setdefault("nodeSelectorTerms", []),
        node,
    )
    pod_instance["spec"]["volumes"] = _update_volumes(
        pod_instance["spec"].get("volumes", []),
        cds_spec["volumeTemplates"],
        node,
    )
    return pod_instance


def get_pod_target_node(
        pod: kubernetes_asyncio.client.V1Pod,
        ) -> typing.Optional[str]:
    """
    Determine the node on which a Pod is supposed to run.

    The node is extracted from the Pod affinity.
    """
    if not pod.spec.affinity:
        return None
    node_affinity = pod.spec.affinity.node_affinity
    if not node_affinity:
        return None
    req = node_affinity.required_during_scheduling_ignored_during_execution
    if not req:
        return None
    terms = req.node_selector_terms
    if len(terms) != 1:
        return None
    fields = terms[0].match_fields
    if not fields:
        return None
    if len(fields) != 1:
        return None
    field = fields[0]
    if field.key != "metadata.name":
        return None
    if field.operator != "In":
        return None
    names = field.values
    if not names:
        return None
    if len(names) != 1:
        return None
    return names[0]


def get_max_unavailable(cds_spec: typing.Mapping) -> int:
    return cds_spec.get("updateStrategy", {}).get("rollingUpdate", {}).get(
        "maxUnavailable", 1
    )


class CDSPodResource(sm.SingleObject[kubernetes_asyncio.client.V1Pod]):
    def _create_resource_interface(
            self,
            api_client: kubernetes_asyncio.client.ApiClient,
            ) -> sm.ResourceInterface[kubernetes_asyncio.client.V1Pod]:
        return sm.pod_interface(api_client)

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> sm.ResourceBody:
        if ctx.instance is None:
            raise RuntimeError("cannot create pod without instance")
        return make_pod(ctx.parent_spec, ctx.instance)

    def labels(self, ctx: sm.Context) -> typing.Mapping[str, str]:
        return {
            LABEL_CDS_CONTROLLER_UID: ctx.parent["metadata"]["uid"],
        }

    async def adopt_object(
            self,
            ctx: sm.Context,
            body: sm.ResourceBody,
            ) -> None:
        pod_hash = hash_pod(body)
        annotations = body.setdefault("metadata", {}).setdefault(
            "annotations", {},
        )
        annotations[HASH_ANNOTATION] = pod_hash
        await super().adopt_object(ctx, body)
        body["metadata"]["generateName"] = f"{ctx.parent_name}-"
        body["metadata"].pop("name", None)

    def _needs_update(
            self,
            old: kubernetes_asyncio.client.V1Pod,
            new: typing.Mapping,
            ) -> bool:
        return super()._needs_update(old, new)


class CDSPods(instancing.StatefulInstancedResource[
        kubernetes_asyncio.client.V1Pod]):
    def __init__(self, **kwargs):
        super().__init__(
            wrapped_state=CDSPodResource(),
            **kwargs,
        )

    def _get_max_unavailable(
            self,
            ctx: sm.Context,
            ) -> int:
        return get_max_unavailable(ctx.parent_spec)

    def get_listeners(self) -> typing.List[sm.Listener]:
        return super().get_listeners() + [
            sm.KubernetesListener(
                api_group="", version="v1", plural="pods",
                listener=self._handle_pod_event,
                broadcast=True),
        ]

    def _handle_pod_event(
            self,
            ctx: sm.Context,
            event: watcher.StatefulWatchEvent[
                kubernetes_asyncio.client.V1Pod,
            ]) -> bool:
        controller_uid = (event.object_.metadata.labels or {}).get(
            LABEL_CDS_CONTROLLER_UID, None
        )
        if controller_uid == ctx.parent_uid:
            return True
        return False

    async def get_target_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Any]:
        return {x: None for x in ctx.parent_spec["targetNodes"]}

    async def _get_current_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Tuple[
                typing.Mapping[
                    str,
                    typing.Collection[
                        instancing.ResourceInfo[
                            kubernetes_asyncio.client.V1Pod
                        ]
                    ]
                ],
                typing.Collection[
                    instancing.ResourceInfo[kubernetes_asyncio.client.V1Pod]
                ],
            ]:
        raw_assignment, raw_leftovers = await super()._get_current_instances(
            ctx
        )
        all_instances = [
            instance
            for instances in raw_assignment.values()
            for instance in instances
        ]
        all_instances.extend(raw_leftovers)
        del raw_assignment, raw_leftovers

        assignment: typing.Dict[
            str, typing.List[
                instancing.ResourceInfo[kubernetes_asyncio.client.V1Pod]
            ]
        ] = {}
        leftovers: typing.List[
            instancing.ResourceInfo[kubernetes_asyncio.client.V1Pod]
        ] = []
        for instance in all_instances:
            target_node = get_pod_target_node(instance.body)
            if target_node is None:
                leftovers.append(instance)
            else:
                assignment.setdefault(target_node, []).append(instance)

        return assignment, leftovers

    def _get_spec_state(
            self,
            ctx: sm.Context,
            intent: typing.Mapping,
            instance: kubernetes_asyncio.client.V1Pod,
            ) -> instancing.ResourceSpecState:
        if (instance.metadata is None or
                not instance.metadata.annotations):
            return instancing.ResourceSpecState.STALE

        try:
            current_hash = instance.metadata.annotations[HASH_ANNOTATION]
        except KeyError:
            return instancing.ResourceSpecState.STALE

        new_hash = intent["metadata"]["annotations"][HASH_ANNOTATION]
        if current_hash != new_hash:
            ctx.logger.debug(
                "pod %s is stale: %s != %s",
                instance.metadata.name,
                current_hash, new_hash,
            )
            return instancing.ResourceSpecState.STALE

        return instancing.ResourceSpecState.UP_TO_DATE

    def _get_run_state(
            self,
            ctx: sm.Context,
            instance: kubernetes_asyncio.client.V1Pod,
            ) -> instancing.ResourceRunState:
        target_node = get_pod_target_node(instance)
        if target_node is None:
            # For whatever reason this pod does not have any target specified.
            # Probably it has a pod selector that just randomly matched ours.
            # We therefor delete this pod as it should not be here.
            return instancing.ResourceRunState.DELETION_REQUIRED

        current_node = instance.spec.node_name
        if current_node is None:
            # Pod is not yet scheduled to a node. As we do not want to get to a
            # infinite create/delete loop we just assume it will eventually end
            # up there and treat it as Starting.
            # In case the target node is no longer supposed to have any pods
            # that will be fixed in the reconile method itself.
            return instancing.ResourceRunState.STARTING

        if current_node is not None and current_node != target_node:
            # The pod is scheduled, but not on the node we originally intended
            # it to be on -> kill it.
            return instancing.ResourceRunState.DELETION_REQUIRED

        # If the pod is deleting, it is also definitely shutting down.
        if instance.metadata.deletion_timestamp is not None:
            return instancing.ResourceRunState.SHUTTING_DOWN

        # According to the k8s documentation, there are five possible values
        # for Pod phases (see:
        # https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/
        # )
        # Quoting:
        # - Pending: The Pod has been accepted by the Kubernetes cluster, but
        #   one or more of the containers has not been set up and made ready to
        #   run. This includes time a Pod spends waiting to be scheduled as
        #   well as the time spent downloading container images over the
        #   network.
        # - Running: The Pod has been bound to a node, and all of the
        #   containers have been created. At least one container is still
        #   running, or is in the process of starting or restarting.
        # - Succeeded: All containers in the Pod have terminated in success,
        #   and will not be restarted.
        # - Failed: All containers in the Pod have terminated, and at least one
        #   container has terminated in failure. That is, the container either
        #   exited with non-zero status or was terminated by the system.
        # - Unknown: For some reason the state of the Pod could not be
        #   obtained. This phase typically occurs due to an error in
        #   communicating with the node where the Pod should be running.
        #
        # Of these, only Unknown and Failed are clear cut: We treat them as
        # Broken. In addition, we treat Succeeded as Ready, as we do not want
        # to get in a fun loop.
        #
        # For Pending, we have to investigate whether it is a state where
        # containers are stuck (for example, failing to pull images) or whether
        # it is a state where they are making progress. It may not be possible
        # to distinguish those.
        #
        # As a first iteration, we treat Pending as Starting.
        #
        # Running can either be Starting (if the containers are not Ready yet)
        # or Ready (otherwise).

        phase = instance.status.phase
        if phase == "Failed":
            return instancing.ResourceRunState.DELETION_REQUIRED
        elif phase == "Unknown":
            return instancing.ResourceRunState.UNKNOWN
        elif phase == "Succeeded":
            return instancing.ResourceRunState.READY
        elif phase == "Pending":
            return instancing.ResourceRunState.STARTING
        elif phase == "Running":
            ready_conditions = [
                condition
                for condition in instance.status.conditions
                if condition.type == "Ready"
            ]
            if ready_conditions and ready_conditions[0].status == "True":
                return instancing.ResourceRunState.READY
            else:
                return instancing.ResourceRunState.STARTING

        raise ValueError(f"pod status not understood: {phase}")

    async def _update_stats(
            self,
            ctx: sm.Context,
            groups: typing.List[instancing.InstanceGroup],
            ) -> None:
        await super()._update_stats(ctx, groups)
        stats = typing.cast(instancing.InstanceStatistics, groups[0].stats)
        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            additional_patch={
                "currentNumberScheduled": (
                    stats.nexisting_instances +
                    stats.ncreated
                ),
                "updatedNumberScheduled": stats.nupdated,
                "numberAvailable": stats.navailable,
                "numberReady": stats.nready,
            },
        )
