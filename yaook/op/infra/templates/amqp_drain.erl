%% -*- erlang -*-
%% %CopyrightBegin%
%%
%% Copyright (c) 2021 The Yaook Authors.
%%
%% This file is part of Yaook.
%% See https://yaook.cloud for further info.
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% %CopyrightEnd%

% This is erlang syntax, so comments start with %.
% This file is commented extensively as we do not suppose that most people know erlang.
% Also most probably this is not how real erlang people would do it, but it works.
%
% The file and its commands exists in order to allow us to cleanly drain mirrored queues away from
% a rabbitmq node. This is used during the pod stop hook to ensure the restarting of a pod is as
% cleanly as possible. As the drain operation takes a few seconds per queue we need to do this
% in parallel as it otherwise takes a long time in larger environments.

% We define our module here (just like a python module). This must match the filename
-module(amqp_drain).

% We define the public functions of the module here.
% Each item in the list defines the function name. The number after the / is the argument count.
% For a description of the function see below.
-export([
    failover_mirrored_queues/1,
    failover_mirrored_queues_worker/2
]).

% This is the main entrypoint for the failover process. It is expected to be called using
% `rabbitmqctl eval`. `Parallel` is a number signifying the amount of parallel workers
failover_mirrored_queues(Parallel) ->
    rabbit_log:info("Starting failover with ~b processes in parallel", [Parallel]), % ~b is == %d in python

    % this gives us a list of all mirrored queues where the current node is master
    Queues = rabbit_amqqueue:list_local_mirrored_classic_queues(),
    % this gives us a list of all nodes that can host queue master (e.g. that are not in maintenance mode)
    TargetNodes = rabbit_maintenance:primary_replica_transfer_candidate_nodes(),
    rabbit_log:info("~b mirrored queues will now failover to other nodes",
                    [length(Queues)]),

    % this distributes all elements of `Queues` over `Parallel` amount of sublists.
    % so if Queues = ['a', 'b', 'c', 'd'] and Parallel = 2 then we get
    % QueuesBatches = [['a', 'b'], ['c', 'd']]
    QueuesBatches = divide(Queues, Parallel) ,

    % read this as `[spawn_monitor(xxxx) for QueuesBatch in QueuesBatches]`
    [begin
        % creates a subprocess running the `failover_mirrored_queues_worker` function with the `QueuesBatch` and `TargetNodes` arguments.
        % `spawn_monitor` causes our "inbox" (see below) to receive messages when a process finishes/dies.
        spawn_monitor(amqp_drain, failover_mirrored_queues_worker, [QueuesBatch, TargetNodes])
    end || QueuesBatch <- QueuesBatches],

    % we wait until all subprocesses are done with the failover.
    % `min` is needed here if we have length(Queues) < Parallel
    wait_for_failover(min(Parallel, length(QueuesBatches))),
    rabbit_log:info("Finished failover of all mirrored queues").

% hello erlang fun. You can define a function multiple times. They are matched top to bottom.
% The first match is used.
% This one is just used if the parameter is 0. In this case we do nothing, as nothing does not exist we just return ok.
wait_for_failover(0) ->
    ok; % btw the semicolon is important if you define a function multiple times

% this is used if the parameter is not (yet) 0.
wait_for_failover(Left) ->
    % receive gets the first message from our "inbox".
    receive
        % this tries to match the received message to the parameters. If you know Rust then read this as a `match` statement.
        % `'DOWN'` is the first value from a monitor (that we spawned above). If the last value is `normal` then the process just
        % finished its normal execution (reached the end of the code).
        {'DOWN', _, _, _, normal} ->
            rabbit_log:info("One failover process finished normally. ~b processes remaining.", [Left - 1]),
            % erlang does not have loops so we just use recursion instead (no this can not trigger a stack overflow).
            wait_for_failover(Left - 1);
        % If the above expression did not match (because it was not `normal`) we just log the outcome here and continue.
        % We can not really do something about it here anyway. If you match this case you hopefully have a stacktrace for a bug report :)
        {'DOWN', _, _, _, ExitReason} ->
            rabbit_log:warning("One failover process crashed because of ~s, continuing with the others. ~b processes remaining.",
                               [ExitReason, Left - 1]),
            wait_for_failover(Left - 1)
    end.

% This is now the actuall function that migrates a list of queues.
% It is mostly taking from the rabbitmq source code in `rabbit_maintenance.erl` as available at
% https://github.com/rabbitmq/rabbitmq-server/blob/master/deps/rabbit/src/rabbit_maintenance.erl.
% It is exported above because `spawn***` expects the target function to be exported.
failover_mirrored_queues_worker(Queues, TargetNodes) ->
    % [... for Q in Queues]
    [begin
        Name = amqqueue:get_name(Q),
        % Gets all nodes currently hosting the queue (ignores unsynchronized mirrors).
        ExistingReplicaNodes = [node(Pid) || Pid <- amqqueue:get_sync_slave_pids(Q)],
        % takes a random node that hosts the queue and is available as a target at all
        case rabbit_maintenance:random_primary_replica_transfer_candidate_node(TargetNodes, ExistingReplicaNodes) of
            % we found one
            {ok, Pick} ->
                rabbit_log:debug("Will transfer leadership of local ~s to node ~s",
                        [rabbit_misc:rs(Name), Pick]),
                % this will migrate the queue to the new master node. I have no idea how it actually makes sure that it is
                % migrated to that specific node and not somewhere else. But at least we are no longer master.
                % it also waits until the master status is really transfered
                case rabbit_mirror_queue_misc:migrate_leadership_to_existing_replica(Q, Pick) of
                    {migrated, _} ->
                        rabbit_log:debug("Successfully transferred leadership of queue ~s to node ~s",
                                        [rabbit_misc:rs(Name), Pick]);
                    Other ->
                        rabbit_log:warning("Could not transfer leadership of queue ~s to node ~s: ~p",
                                        [rabbit_misc:rs(Name), Pick, Other])
                end;
            undefined ->
                rabbit_log:warning("Could not transfer leadership of queue ~s: no suitable candidates?",
                                [Name])
        end
    end || Q <- Queues],
    rabbit_log:info("Finished failover batch of mirrored queues").

% The following splits a list `L` into `N` sublists.
% Idea stolen from https://stackoverflow.com/a/2135920 and rewritten to erlang
divide(L, N) ->
    % this is len(L) / N but truncated to an integer.
    K = length(L) div N,
    % this is the remainder of len(L) / N.
    M = length(L) rem N,
    % we do list comprehension
    [
        % each element is a sublist of L second parameter is the start offset
        % and third parameter is the length of the sublist
        % yes the `min() - min()` is ugly.
        lists:sublist(L, I*K+min(I, M)+1, K+min(I+1, M)-min(I, M))
    || I <- lists:seq(0, N-1)].
