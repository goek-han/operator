#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import kubernetes_asyncio.client as kclient

import typing

import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources as resources
import yaook.statemachine.registry
from yaook.statemachine.resources.k8s_service import TemplatedService

T = typing.TypeVar("T")


class OVSDBStatefulSet(sm.TemplatedStatefulSet):
    def __init__(
            self,
            *,
            certificate: sm.Certificate,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.certificate = certificate

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        param["db_service_namespace"] = ctx.namespace
        param["vars"]["cert_not_before"] = (
            await self.certificate._get_current(ctx)
            )["status"]["notBefore"]

        dns_domain = sm.api_utils.get_cluster_domain()
        headless_service_dep = typing.cast(
            sm.Service,
            dependencies["headless_service"],
        )
        service_names = []
        headless_svc_ref = await headless_service_dep.get(ctx)
        for r in range(ctx.parent_spec["replicas"]):
            dns_name = ".".join([
                f"{ctx.parent_name}-ovsdb-{r}",
                headless_svc_ref.name,
                headless_svc_ref.namespace,
                "svc",
                dns_domain
                ])
            if "northbound" in ctx.parent_name:
                port = 6641
            else:
                port = 6642
            service_names.append(":".join([
                "ssl",
                dns_name,
                str(port)
                ]))
        param["service_names"] = ",".join(service_names)
        return param


class PerReplicasInCR(sm.StatelessInstancedResource[T]):
    async def get_target_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Any]:
        return {f"{ctx.parent_name}-ovsdb-{i}": None
                for i in range(ctx.parent_spec["replicas"])}


class TemplatedCertificateWithServices(sm.TemplatedCertificate):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        v1 = kclient.CoreV1Api(ctx.api_client)
        dns_domain = sm.api_utils.get_cluster_domain()

        dns_names = []

        services_per_replica_dep = typing.cast(
            sm.Service,
            dependencies["services_per_replica"],
        )

        headless_service_dep = typing.cast(
            sm.Service,
            dependencies["headless_service"],
        )

        svc_per_replica_ref = await services_per_replica_dep.get_all(ctx)

        for k8s_svc in svc_per_replica_ref.values():
            fqsn = f"{k8s_svc.name}.{k8s_svc.namespace}.svc.{dns_domain}"
            dns_names.append(fqsn)

            k8s_has_svc_now = await v1.read_namespaced_service(
                k8s_svc.name,
                k8s_svc.namespace,
                )
            dns_names.append(k8s_has_svc_now.spec.cluster_ip)

        headless_svc_ref = await headless_service_dep.get(ctx)

        for r in range(ctx.parent_spec["replicas"]):
            dns_names.append(".".join([
                f"{ctx.parent_name}-ovsdb-{r}",
                headless_svc_ref.name,
                headless_svc_ref.namespace,
                "svc",
                dns_domain
                ]))

        param["dns_names"] = dns_names
        return param


class OVSDBService(sm.CustomResource):
    API_GROUP = "infra.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "ovsdbservices"
    KIND = "OVSDBService"

    # The DNS names of the pods of this headless service
    # are used for the raft names of the cluster.
    headless_service = TemplatedService(
        template="ovsdb-headless-service.yaml",
        params={
            "database_component": "ovsdb",
        },
    )

    # The neutron operator will find the ClusterIP
    # from all these k8s services (one per ovsdb-server pod)
    # and construct the list of tcp:<ClusterIP>:<664x>.
    services_per_replica = PerReplicasInCR(
        wrapped_state=TemplatedService(
            component=yaook.op.common.OVSDB_DATABASE_SERVICE_COMPONENT,
            template="ovsdb-per-pod-service.yaml",
            params={
                "ovsdb_component": "ovsdb",
            },
        ),
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("ovsdb-server-certificate-", True),
    )

    # Creates a signable certificate to be used by all pods.
    # Add all ClusterIPs from the services per pod,
    # FQDNs of services per pod and all constructed FQDNs
    # of the pods beneath the single headless service as DNS names.
    certificate = TemplatedCertificateWithServices(
        template="ovsdb-server-certificate.yaml",
        add_dependencies=[
            headless_service,
            services_per_replica,
            certificate_secret,
        ],
    )

    # This waits for cert-manager to sign our cert.
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )

    backup_creator_image = sm.ConfigurableVersionedDockerImage(
        'backup-creator/backup-creator',
        sm.YaookSemVerSelector(),
    )

    backup_shifter_image = sm.ConfigurableVersionedDockerImage(
        'backup-shifter/backup-shifter',
        sm.YaookSemVerSelector(),
    )

    nb_cfg_monitoring_image = sm.ConfigurableVersionedDockerImage(
        'openvswitch/ovsdbapp',
        sm.YaookSemVerSelector(),
    )

    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )

    ca_certs = sm.CAConfigMap(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ]
    )

    ovsdb = OVSDBStatefulSet(
        component="ovsdb",
        template="ovsdb-statefulset.yaml",
        add_dependencies=[
            headless_service,
            ready_certificate_secret,
            ca_certs,
        ],
        certificate=certificate,
        scheduling_keys=[
            yaook.op.common.SchedulingKey.INFRA_OVSDB_CLUSTER.value,
            yaook.op.common.SchedulingKey.ANY_INFRA.value,
        ],
        versioned_dependencies=[
             backup_creator_image,
             backup_shifter_image,
             nb_cfg_monitoring_image,
             ssl_terminator_image,
        ]
    )

    ovsdb_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-db-pdb",
        replicated=ovsdb,
    )

    ovsdb_service_monitor = sm.GeneratedStatefulsetServiceMonitor(
        metadata=lambda ctx: (f"{ctx.parent_name}-ovsdb-service-monitor-",
                              True),
        service=headless_service,
        certificate=ready_certificate_secret,
        endpoints=["prometheus"],)

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


yaook.statemachine.registry.register(OVSDBService)
