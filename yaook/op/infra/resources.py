#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import base64
import contextlib
import dataclasses
import re
import ssl
import subprocess  # nosemgrep there are no useful helm bindings unfortunately
import tempfile
import typing
import urllib.parse

import pymysql

import jinja2

import kubernetes_asyncio.client as kclient

import aiohttp

import yaook.statemachine as sm
import yaook.statemachine.resources as resources


# Components are not passwords so we need to exclude these :)
MYSQL_DATABASE_COMPONENT_PASSWORDS = "passwords"  # nosemgrep
AMQP_SERVER_COMPONENT_PASSWORDS = "cluster_credentials"  # nosemgrep

MAINTENANCE_USER = "yaook-sys-maint"
_PRIVILEGE_RE = re.compile(r"[A-Z ]")
_UNQUOTED_SCHEMA_OBJECT_NAME_RE = re.compile(
    # the same as used in the OpenAPI schema
    r"[a-zA-Z$_\u0080-\uffff]"
    r"([0-9a-zA-Z$_\u0080-\uffff ]+[0-9a-zA-Z$_\u0080-\uffff])?"
)


@contextlib.contextmanager
def temporary_file(contents: bytes) -> typing.Iterator[str]:
    with tempfile.NamedTemporaryFile(mode="wb") as f:
        f.write(contents)
        f.flush()
        yield f.name


def validate_privilege(s: str) -> str:
    if not _PRIVILEGE_RE.match(s):
        raise ValueError(f"invalid mysql/mariadb privilege: {s!r}")
    return s


@dataclasses.dataclass(repr=False, frozen=True)
class MySQLConnectionTemplate:
    hostname: str
    user: str
    password: str
    ca_cert: bytes


@contextlib.contextmanager
def connect_mysql(
        template: MySQLConnectionTemplate,
        ) -> typing.Generator[pymysql.Connection, typing.Any, None]:
    with temporary_file(template.ca_cert) as ca_filename:
        conn = pymysql.connect(
            host=template.hostname,
            user=template.user,
            password=template.password,
            charset="utf8mb4",
            ssl={
                "ca": ca_filename,
            },
        )
        try:
            yield conn
        finally:
            conn.close()


def validate_unquoted_schema_object_name(s: str) -> str:
    if not _UNQUOTED_SCHEMA_OBJECT_NAME_RE.match(s):
        raise ValueError(f"invalid mysql/mariadb schema object name: {s!r}")
    return s


def manage_mysql_user(
        conn: pymysql.Connection,
        username: str,
        password: str,
        database: str,
        database_privileges: typing.Collection[str],
        global_privileges: typing.Collection[str]) -> None:
    # validate all privileges to avoid fancy SQL injections \o/
    database_privileges = list(map(validate_privilege, database_privileges))
    global_privileges = list(map(validate_privilege, global_privileges))
    with contextlib.closing(conn.cursor()) as c:
        # create user initially
        c.execute(
            "CREATE USER IF NOT EXISTS %s IDENTIFIED BY %s",
            (username, password),
        )
        # update password if user does not exist
        c.execute(
            "ALTER USER %s IDENTIFIED BY %s",
            (username, password),
        )
        # finally, update all privileges
        c.execute(
            "REVOKE ALL PRIVILEGES, GRANT OPTION FROM %s",
            (username,),
        )
        if database_privileges:
            c.execute(
                "GRANT {} ON {}.* TO %s".format(
                    ", ".join(database_privileges),
                    validate_unquoted_schema_object_name(database),
                ),
                (username,),
            )
        if global_privileges:
            c.execute(
                "GRANT {} ON *.* TO %s".format(
                    ", ".join(global_privileges),
                ),
                (username,),
            )
        c.execute("FLUSH PRIVILEGES")


def delete_mysql_user(
        conn: pymysql.Connection,
        username: str) -> None:
    with contextlib.closing(conn.cursor()) as c:
        c.execute(
            "DROP USER IF EXISTS %s",
            (username, ),
        )
        c.execute("FLUSH PRIVILEGES")


class RawTemplateConfigMap(sm.resources.DefaultTemplateParamsMixin,
                           sm.ConfigMap):
    def __init__(self,
                 *,
                 metadata: sm.MetadataProvider,
                 template_map: typing.Mapping[str, str],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata
        self._template_map = template_map

    def _get_jinja_environment(
            self,
            loader: jinja2.BaseLoader,
            ) -> jinja2.Environment:
        env = jinja2.Environment(  # nosemgrep as autoescape is only designed
                                   # for HTML/XML which we dont use here.
            loader=loader,
            enable_async=True,
        )
        self._add_filters(env)
        return env

    async def _render_raw_template(
            self,
            template: str,
            data: sm.resources.TemplateParameters,
            ) -> str:
        tpl = self.jinja_env.get_template(template)
        return await tpl.render_async(**data)

    async def _make_body(self,
                         ctx: sm.Context,
                         dependencies: sm.DependencyMap,
                         ) -> sm.ResourceBody:
        data = {
            filename: await self._render_raw_template(
                template,
                await self._get_template_parameters(ctx, dependencies),
            )
            for filename, template in self._template_map.items()
        }

        return {
            "apiVersion": "v1",
            "kind": "ConfigMap",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "data": data,
        }


class HaproxyTemplateConfigMap(RawTemplateConfigMap):
    def __init__(self,
                 *,
                 database_pod_services: sm.PerStatefulSetPod[
                     kclient.V1Service],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._database_pod_services = database_pod_services

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        services = await self._database_pod_services.get_all(ctx)
        params["services"] = {
            instance: ref.name for instance, ref in services.items()}
        return params


class MySQLUser(sm.ExternalResource):
    def __init__(
            self,
            *,
            database_service: sm.KubernetesReference[kclient.V1Service],
            database_credentials: sm.KubernetesReference[kclient.V1Secret],
            tls_secret: sm.KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._database_service = database_service
        self._database_credentials = database_credentials
        self._tls_secret = tls_secret

    async def _get_root_credentials(
            self,
            ctx: sm.Context) -> typing.Tuple[MySQLConnectionTemplate, str]:
        service_ref = await self._database_service.get(ctx)
        secret_ref = await self._database_credentials.get(ctx)
        tls_secret_ref = await self._tls_secret.get(ctx)

        secrets = sm.secret_interface(ctx.api_client)
        secret = await secrets.read(secret_ref.namespace, secret_ref.name)
        tls_secret = await secrets.read(tls_secret_ref.namespace,
                                        tls_secret_ref.name)

        return (
            MySQLConnectionTemplate(
                hostname=f"{service_ref.name}.{service_ref.namespace}",
                user=MAINTENANCE_USER,
                password=base64.b64decode(
                    secret.data["mariadb-root-password"],
                ).decode("ascii"),
                ca_cert=base64.b64decode(tls_secret.data["ca.crt"]),
            ),
            validate_unquoted_schema_object_name(
                base64.b64decode(secret.data["databaseName"]).decode("utf-8"),
            ),
        )

    def _update_user(
            self,
            connection_template: MySQLConnectionTemplate,
            username: str,
            password: str,
            database: str,
            database_privileges: typing.Collection[str],
            global_privileges: typing.Collection[str],
            ) -> None:
        with connect_mysql(connection_template) as conn:
            manage_mysql_user(
                conn,
                username, password,
                database,
                database_privileges,
                global_privileges,
            )

    def _delete_user(
            self,
            connection_template: MySQLConnectionTemplate,
            username: str,
            ) -> None:
        with connect_mysql(connection_template) as conn:
            delete_mysql_user(
                conn,
                username,
            )

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        try:
            connection_template, database = \
                await self._get_root_credentials(ctx)
        except sm.DependencyNotReady:
            return

        username = ctx.parent_spec["user"]
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(
            None,
            self._delete_user,
            connection_template,
            username,
        )

    async def update(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        connection_template, database = await self._get_root_credentials(ctx)
        username = ctx.parent_spec["user"]
        database_privileges = ctx.parent_spec["databasePrivileges"]
        global_privileges = ctx.parent_spec["globalPrivileges"]
        password = await sm.extract_password(
            ctx,
            ctx.parent_spec["passwordSecretKeyRef"]["name"],
            key=ctx.parent_spec["passwordSecretKeyRef"]["key"],
        )

        loop = asyncio.get_event_loop()
        await loop.run_in_executor(
            None,
            self._update_user,
            connection_template,
            username, password, database,
            database_privileges,
            global_privileges,
        )


async def declare_amqp_policy(
        session: aiohttp.ClientSession,
        api_url: str,
        *,
        vhost: str,
        name: str,
        pattern: str,
        definition: dict,
        priority: int,
        applyTo: str) -> None:
    vhost_quoted = urllib.parse.quote(vhost, safe="")
    name_quoted = urllib.parse.quote(name, safe="")
    json = {
                "pattern": pattern,
                "definition": definition,
                "priority": priority,
                "apply-to": applyTo
                }
    async with session.put(
            f"{api_url}/policies/{vhost_quoted}/{name_quoted}",
            json=json) as resp:
        resp.raise_for_status()


async def get_amqp_policies(
        session: aiohttp.ClientSession, api_url: str) -> typing.Dict:
    async with session.get(
            f"{api_url}/policies/") as resp:
        resp.raise_for_status()
        return await resp.json()


async def delete_amqp_policy(
        session: aiohttp.ClientSession,
        api_url: str,
        vhost: str,
        name: str) -> None:
    vhost_quoted = urllib.parse.quote(vhost, safe="")
    name_quoted = urllib.parse.quote(name, safe="")
    async with session.delete(
            f"{api_url}/policies/{vhost_quoted}/{name_quoted}") as resp:
        resp.raise_for_status()


async def declare_amqp_user(
        session: aiohttp.ClientSession,
        api_url: str,
        *,
        user: str,
        password: str,
        tags: str) -> None:
    user_quoted = urllib.parse.quote(user, safe="")
    async with session.put(
            f"{api_url}/users/{user_quoted}",
            json={"password": password, "tags": tags}) as resp:
        resp.raise_for_status()


async def declare_amqp_user_permissions(
        session: aiohttp.ClientSession,
        api_url: str,
        *,
        vhost: str,
        user: str,
        configure: str,
        read: str,
        write: str) -> None:
    user_quoted = urllib.parse.quote(user, safe="")
    vhost_quoted = urllib.parse.quote(vhost, safe="")
    async with session.put(
            f"{api_url}/permissions/{vhost_quoted}/{user_quoted}",
            json={"configure": configure,
                  "read": read,
                  "write": write}) as resp:
        resp.raise_for_status()


async def delete_amqp_user(
        session: aiohttp.ClientSession,
        api_url: str,
        user: str) -> None:
    user_quoted = urllib.parse.quote(user, safe="")
    async with session.delete(
            f"{api_url}/users/{user_quoted}") as resp:
        resp.raise_for_status()


class AMQPApiResource(sm.ExternalResource):
    def __init__(
            self,
            *,
            amqp_credentials: sm.KubernetesReference[kclient.V1Secret],
            amqp_service: sm.KubernetesReference[kclient.V1Service],
            amqp_frontend_certificate: sm.KubernetesReference[
                kclient.V1Secret
            ],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._amqp_credentials = amqp_credentials
        self._amqp_service = amqp_service
        self._amqp_frontend_certificate = amqp_frontend_certificate

    async def _get_root_credentials(
            self,
            ctx: sm.Context) -> typing.Tuple[str, str, str, bytes]:
        secret_ref = await self._amqp_credentials.get(ctx)
        cert_secret_ref = await self._amqp_frontend_certificate.get(ctx)
        service_ref = await self._amqp_service.get(ctx)

        secrets = sm.secret_interface(ctx.api_client)
        secret = await secrets.read(
            secret_ref.namespace,
            secret_ref.name,
        )
        cert_secret = await secrets.read(
            cert_secret_ref.namespace,
            cert_secret_ref.name,
        )

        return (
            f"https://{service_ref.name}.{service_ref.namespace}:15671/api",
            MAINTENANCE_USER,
            base64.b64decode(secret.data["rabbitmq-password"]).decode(
                "ascii",
            ),
            base64.b64decode(cert_secret.data['ca.crt']),
        )

    @contextlib.asynccontextmanager
    async def _api_session(
            self,
            rootuser: str,
            rootpassword: str,
            cacert: bytes,
            ) -> typing.AsyncGenerator[aiohttp.ClientSession, typing.Any]:
        with temporary_file(cacert) as ca_filename:
            context = ssl.create_default_context(cafile=ca_filename)
            connector = aiohttp.TCPConnector(ssl=context)
            auth = aiohttp.BasicAuth(login=rootuser, password=rootpassword,
                                     encoding="utf-8")
            async with aiohttp.ClientSession(auth=auth,
                                             connector=connector) as session:
                yield session


class AMQPPolicies(AMQPApiResource):
    async def update(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        api_url, rootuser, rootpassword, cacert = \
            await self._get_root_credentials(ctx)
        policies = ctx.parent_spec.get("policies")

        if policies is None:
            policies = {
                "/": {
                    "openstack-default": {
                        "pattern": "^(?!(amq\\.)|(.*_fanout_)|(reply_)).*",
                        "definition": {
                            "expires": 3600000,
                            "ha-mode": "all",
                            "ha-promote-on-failure": "always",
                            "ha-promote-on-shutdown": "always",
                            "ha-sync-mode": "manual",
                            "message-ttl": 600000,
                            "queue-master-locator": "client-local"
                        },
                        "priority": 1,
                        "applyto": "all"
                    }
                }
            }

        async with self._api_session(rootuser, rootpassword,
                                     cacert) as session:
            existing_policies = await get_amqp_policies(session, api_url)
            for existing_policy in existing_policies:
                if existing_policy["name"] not in policies.get(
                        existing_policy["vhost"], {}):
                    await delete_amqp_policy(
                        session,
                        api_url,
                        existing_policy["vhost"],
                        existing_policy["name"]
                    )

            for vhost in policies:
                for name, policy in policies[vhost].items():
                    policy_json = {
                        'vhost': vhost,
                        'name': name,
                        'apply-to': policy["applyto"],
                        **policy
                    }
                    del policy_json["applyto"]
                    if policy_json in existing_policies:
                        continue

                    await declare_amqp_policy(
                        session=session,
                        api_url=api_url,
                        vhost=vhost,
                        name=name,
                        pattern=policy["pattern"],
                        definition=policy["definition"],
                        priority=policy["priority"],
                        applyTo=policy["applyto"])

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        pass


class AMQPUser(AMQPApiResource):
    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        try:
            api_url, rootuser, rootpassword, cacert = \
                await self._get_root_credentials(ctx)
        except sm.DependencyNotReady:
            return
        user = ctx.parent_spec["user"]

        async with self._api_session(rootuser, rootpassword,
                                     cacert) as session:
            await delete_amqp_user(
                session,
                api_url,
                user,
            )

    async def update(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        api_url, rootuser, rootpassword, cacert = \
            await self._get_root_credentials(ctx)
        password = await sm.extract_password(
            ctx,
            ctx.parent_spec["passwordSecretKeyRef"]["name"],
            key=ctx.parent_spec["passwordSecretKeyRef"]["key"],
        )
        user = ctx.parent_spec["user"]

        async with self._api_session(rootuser, rootpassword,
                                     cacert) as session:
            await declare_amqp_user(
                session,
                api_url,
                user=user,
                password=password,
                tags="",
            )
            await declare_amqp_user_permissions(
                session,
                api_url,
                vhost="/",
                user=user,
                configure=".*",
                read=".*",
                write=".*",
            )


def helm_repo_update():
    subprocess.check_call(  # nosemgrep statically known command
        ["helm", "repo", "update"])


def helm_repo_add(name, url, *, update=True):
    subprocess.check_call([  # nosemgrep only statically known inputs
        "helm", "repo", "add", name, url,
    ])
    if update:
        helm_repo_update()
