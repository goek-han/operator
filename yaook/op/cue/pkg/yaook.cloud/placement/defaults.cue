// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package placement

import (
	"yaook.cloud/placement_template"
)

placement_conf_spec: placement_template.placement_template_conf_spec
placement_conf_spec: {
	placement_database: {
		#connection_host:        *"mysql-placement-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"placement" | string
		#connection_password:    string
		#connection_database:    *"placement" | string
		#connection_ssl_ca_path: *"/etc/ssl/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the nova operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	DEFAULT: {
		use_stderr:  *true | bool
		policy_file: "/etc/placement/policy.json"
	}
	keystone_authtoken: {
		auth_url:            string
		cafile:              "/etc/ssl/certs/ca-bundle.crt"
		"auth-url":          auth_url
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
}
