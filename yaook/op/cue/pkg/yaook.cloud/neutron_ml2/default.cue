// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neutron_ml2

import (
	"yaook.cloud/neutron_ml2_template"
)

neutron_ml2_conf_spec: neutron_ml2_template.neutron_ml2_template_conf_spec
neutron_ml2_conf_spec: {
	ml2: {
		extension_drivers: ["port_security"]
	}
}

ml2_config_ovs: {
	ml2: {
		type_drivers:         *["flat", "vlan", "vxlan"] | [...string]
		tenant_network_types: *["vxlan"] | [...string]
		mechanism_drivers: ["openvswitch", "l2population"]
	}
	ml2_type_vxlan: {
		vni_ranges: *["1:1000"] | [...string]
	}
}

ml2_config_ovn: {
	ml2: {
		type_drivers:         *["flat", "vlan", "geneve"] | [...string]
		tenant_network_types: *["geneve"] | [...string]
		mechanism_drivers: ["ovn"]
	}
	ml2_type_geneve: {
		vni_ranges:      *["1:65536"] | [...string]
		max_header_size: 38
	}
	ovn: {
		ovn_l3_scheduler:     *"leastloaded" | string
		ovn_nb_private_key:   *"/etc/ssl/ml2-plugin/tls.key" | string
		ovn_nb_certificate:   *"/etc/ssl/ml2-plugin/tls.crt" | string
		ovn_nb_ca_cert:       *"/etc/ssl/ml2-plugin/ca.crt" | string
		ovn_sb_private_key:   *"/etc/ssl/ml2-plugin/tls.key" | string
		ovn_sb_certificate:   *"/etc/ssl/ml2-plugin/tls.crt" | string
		ovn_sb_ca_cert:       *"/etc/ssl/ml2-plugin/ca.crt" | string
		ovn_metadata_enabled: *true | bool
	}
}
