// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tempest

tempest_conf_spec: {
	DEFAULT: {
		use_stderr: *true | bool
		debug:      *false | bool
	}
	oslo_concurrency: {
		lock_path: "tempest-lock"
	}
	auth: {
		admin_username:            keystone_authtoken.username
		admin_project_name:        keystone_authtoken.project_name
		admin_password:            keystone_authtoken.password
		admin_user_domain_name:    keystone_authtoken.user_domain_name
		admin_project_domain_name: keystone_authtoken.project_domain_name
	}
	identity: {
		uri_v3:               keystone_authtoken.auth_url
		auth_version:         "v\( keystone_authtoken.auth_version )"
		v3_endpoint_type:     keystone_authtoken.valid_interfaces
		ca_certificates_file: "/etc/ssl/certs/ca-certificates.crt"
	}
	keystone_authtoken: {
		username:            string
		project_name:        string
		password:            string
		user_domain_name:    string
		project_domain_name: string
		auth_url:            string
		auth_version:        string
	}
}
