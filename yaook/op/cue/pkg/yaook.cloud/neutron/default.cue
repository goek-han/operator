// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neutron

import (
	"strings"
	"yaook.cloud/neutron_template"
)

neutron_conf_spec: neutron_template.neutron_template_conf_spec
neutron_conf_spec: {
	database: {
		#connection_host:        *"mysql-neutron-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"neutron" | string
		#connection_password:    string
		#connection_database:    *"neutron" | string
		#connection_ssl_ca_path: *"/etc/ssl/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the neutron operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	DEFAULT: {
		bind_host:               "127.0.0.1"
		bind_port:               8080
		api_paste_config:        "/usr/local/etc/neutron/api-paste.ini"
		#transport_url_hosts:    *["rabbitmq-neutron"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: *"" | string
		#transport_url_vhost:    *"" | string
		#transport_url_parts:    *[ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}] | string
		if #transport_url_password != "" {
			transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		}
		use_stderr:      *true | bool
		use_json:        *true | bool
		core_plugin:     "ml2"
		service_plugins: *[
					"neutron.services.l3_router.l3_router_plugin.L3RouterPlugin",
					"neutron_dynamic_routing.services.bgp.bgp_plugin.BgpPlugin",
		] | [...string]
		allow_overlapping_ips:              *true | bool
		notify_nova_on_port_status_changes: true
		notify_nova_on_port_data_changes:   true
		l3_ha:                              *true | bool
		l3_ha_network_type:                 *"vxlan" | string
		network_auto_schedule:              *true | bool
		router_auto_schedule:               *true | bool
		if DEFAULT.#setup == "ovn" {
			dhcp_agent_notification: *false | bool
			service_plugins: [ "ovn-router"]
		}
	}
	nova: {
		auth_url:       string
		"auth-url":     auth_url
		endpoint_type:  *"internal" | string
		cafile:         keystone_authtoken.cafile
		region_name:    keystone_authtoken.region_name
		os_region_name: region_name
	}
	placement: {
		cafile:         keystone_authtoken.cafile
		region_name:    keystone_authtoken.region_name
		os_region_name: region_name
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		"auth-url":          auth_url
		cafile:              "/etc/ssl/certs/ca-bundle.crt"
		auth_type:           *"password" | string
		project_domain_name: *"default" | string
		user_domain_name:    *"default" | string
		project_name:        *"service" | string
		username:            *"neutron" | string
		password:            string
	}
	oslo_concurrency: {
		"lock_path": "/var/lib/neutron/tmp"
	}
	oslo_policy: {
		policy_file: "/etc/neutron/policy.json"
	}
	if DEFAULT.#transport_url_password != "" {
		oslo_messaging_rabbit: {
			ssl:                 true
			ssl_ca_file:         "/etc/ssl/certs/ca-bundle.crt"
			amqp_durable_queues: true
			rabbit_quorum_queue: true
		}
	}

	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
}
