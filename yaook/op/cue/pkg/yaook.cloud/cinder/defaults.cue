// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cinder

import (
	"strings"
	"yaook.cloud/cinder_template"
)

cinder_conf_spec: cinder_template.cinder_template_conf_spec
cinder_conf_spec: {
	database: {
		#connection_host:        *"mysql-cinder-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"cinder" | string
		#connection_password:    string
		#connection_database:    *"cinder" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the cinder operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	DEFAULT: {
		osapi_volume_listen:      "127.0.0.1"
		osapi_volume_listen_port: 8080
		use_stderr:               *true | bool
		use_json:                 *true | bool
		#transport_url_hosts:     *["rabbitmq-cinder"] | [...string]
		#transport_url_port:      *5671 | int
		#transport_url_username:  *"admin" | string
		#transport_url_password:  string
		#transport_url_vhost:     *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url:               "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		api_paste_config:            "/usr/local/etc/cinder/api-paste.ini"
		default_volume_type:         *"__DEFAULT__" | string
		enabled_backends:            *["ceph"] | [string, ...]
		glance_api_version:          2
		glance_catalog_info:         *"image:glance:internalURL" | string
		glance_ca_certificates_file: keystone_authtoken.cafile
		rootwrap_config:             "/usr/local/etc/cinder/rootwrap.conf"
		resource_query_filters_file: *"/usr/local/etc/cinder/resource_filters.json" | string
	}
	oslo_policy: {
		policy_file: "/etc/cinder/policy.json"
	}
	oslo_concurrency: {
		lock_path: "/var/lib/cinder/tmp"
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
	cinder_sys_admin: {
		helper_command: "sudo /usr/local/bin/cinder-rootwrap /usr/local/etc/cinder/rootwrap.conf privsep-helper --config-file /etc/cinder/cinder.conf"
	}
	privsep_osbrick: {
		helper_command: cinder_sys_admin.helper_command
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/pki/tls/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	nova: {
		interface:      *"internal" | string
		cafile:         keystone_authtoken.cafile
		region_name:    keystone_authtoken.region_name
		os_region_name: region_name
	}
	barbican: {
		barbican_endpoint_type: *"internal" | string
	}
}

cinder_backend_ceph: {
	backend_host:                     *"ceph-\(rbd_pool)" | string
	volume_driver:                    "cinder.volume.drivers.rbd.RBDDriver"
	volume_backend_name:              *"ceph" | string
	rbd_pool:                         *"cinder-pool" | string
	rbd_ceph_conf:                    "/etc/ceph/ceph.conf"
	rbd_flatten_volume_from_snapshot: *false | bool
	rbd_max_clone_depth:              5
	rbd_store_chunk_size:             4
	rados_connect_timeout:            -1
	rbd_exclusive_cinder_pool:        true
	rbd_user:                         string
}

cinder_backend_netapp: {
	backend_host:                 *"\(netapp_server_hostname)" | string
	netapp_login:                 string
	netapp_vserver:               string
	netapp_server_port:           *443 | int
	nfs_shares_config:            *"/etc/cinder/nfs_shares_\( netapp_vserver )" | string
	volume_driver:                "cinder.volume.drivers.netapp.common.NetAppDriver"
	netapp_storage_family:        "ontap_cluster"
	volume_backend_name:          *netapp_vserver | string
	nfs_mount_options:            *"vers=4.1,lookupcache=pos" | string
	netapp_server_hostname:       string
	netapp_transport_type:        *"https" | "http"
	netapp_password:              string
	netapp_webservice_path:       "/api/v2"
	netapp_lun_space_reservation: "disabled"
	nas_secure_file_permissions:  true
	nas_secure_file_operations:   true
	image_volume_cache_enabled:   false
	netapp_storage_protocol:      "nfs"
}
