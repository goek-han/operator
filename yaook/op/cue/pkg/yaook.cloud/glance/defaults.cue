// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package glance

import (
	"yaook.cloud/glance_template"
)

glance_conf_spec: glance_template.glance_template_conf_spec
glance_conf_spec: {
	database: {
		#connection_host:        *"mysql-glance-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"glance" | string
		#connection_password:    string
		#connection_database:    *"glance" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the glance operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	DEFAULT: {
		bind_host:             "127.0.0.1"
		bind_port:             8080
		use_stderr:            *true | bool
		use_json:              *true | bool
		show_image_direct_url: *true | bool
		enable_v1_api:         false
		enable_v2_api:         true
		image_size_cap:        *5368709120 | int
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
	paste_deploy: {
		flavor:      *"keystone" | string
		config_file: "/usr/local/etc/glance/glance-api-paste.ini"
	}
	oslo_policy: {
		policy_file: "/etc/glance/policy.json"
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	barbican: {
		barbican_endpoint_type: *"internal" | string
	}
}

// Template to be included in "glance_store" if we want to use ceph
glance_store_ceph_spec: {
	stores:               ["rbd", ...] & [...string]
	default_store:        *"rbd" | string
	rbd_store_chunk_size: *8 | int
	rbd_store_pool:       string
	rbd_store_user:       string
	rbd_store_ceph_conf:  "/etc/ceph/ceph.conf"
}

// Template to be included in "glance_store" if we want to use file based storage
glance_store_file_spec: {
	stores:        ["file", ...] & [...string]
	default_store: *"file" | string
}
