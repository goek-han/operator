// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package barbican

import (
	"strings"
	"yaook.cloud/barbican_template"
)

barbican_conf_spec: barbican_template.barbican_template_conf_spec
barbican_conf_spec: {
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-barbican"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url:           "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		#connection_host:        *"mysql-barbican-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"barbican" | string
		#connection_password:    string
		#connection_database:    *"barbican" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		sql_connection:          "mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )"
		use_stderr:              *true | bool
		use_json:                *true | bool
		api_paste_config:        "/etc/barbican/api-paste.ini"
		policy_file:             "/etc/barbican/policy.json"
		db_auto_create:          false

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the barbican operator. sql_idle_timeout
		// should always be ~10% smaller than the haproxy timeoutClient.
		sql_idle_timeout: 280
	}
	keystone_notifications: {
		enable: true
		topic:  "barbican_notifications"
	}
	secretstore: {
		namespace:                   "barbican.secretstore.plugin"
		enabled_secretstore_plugins: *["store_crypto"] | [...string]
	}
	crypto: {
		namespace: "barbican.crypto.plugin"
		enabled_crypto_plugins: ["simple_crypto"]
	}
	simple_crypto_plugin: {
		kek: =~"^[A-Za-z0-9+\/]{42}[AEIMQUYcgkosw048]=$"
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/pki/tls/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	keystone_authtoken: {
		www_authenticate_uri: *"https://keystone:5000/v3" | string
		auth_url:             *"https://keystone:5000/v3" | string
		cafile:               "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:            "password"
		project_domain_name:  string
		user_domain_name:     string
		project_name:         string
		username:             string
		password:             string
	}
}
