// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ironic

import (
	"yaook.cloud/ironic_template"
)

#standalone_ironic_conf_template: ironic_template.ironic_template_conf_spec
#standalone_ironic_conf_template: {
	// NOTE: The sections in this template must be included manually in the
	// yaook/op/infra_ironic/cr.py due to restrictions in our cue handling.
	// (normally, you'd want to include #standalone_ironic_conf_template as
	// another definition to form the config, but we don't have any means to
	// do that)
	DEFAULT: {
		rpc_transport: "json-rpc"
		enabled_network_interfaces: ["noop"]
		enabled_inspect_interfaces: ["inspector", "no-inspect"]
		enabled_power_interfaces:      *["ipmitool"] | [...string]
		enabled_management_interfaces: *["ipmitool"] | [...string]
		enabled_hardware_types:        *["ipmi"] | [...string]
	}
	pxe: {
		#basedir:                 "/usr/venv-ironic/lib/python3.6/site-packages/ironic"
		pxe_append_params:        *"ipa-insecure=1" | string
		pxe_config_template:      "\( #basedir )/drivers/modules/ipxe_config.template"
		uefi_pxe_config_template: "\( #basedir )/drivers/modules/ipxe_config.template"
		tftp_root:                "/tftpboot"
		tftp_master_path:         "/httpboot/master_images"
		pxe_bootfile_name:        "undionly.kpxe"
		ipxe_enabled:             true
		ipxe_boot_script:         "/etc/ironic/boot.ipxe"
		enable_netboot_fallback:  true
	}
	deploy: {
		http_root:           "/httpboot"
		default_boot_option: "local"
	}
	conductor: {
		#image_server_url: string
		automated_clean:   true
		deploy_kernel:     "\( #image_server_url )/ipa.kernel"
		deploy_ramdisk:    "\( #image_server_url )/ipa.initramfs"
		rescue_kernel:     "\( #image_server_url )/ipa.kernel"
		rescue_ramdisk:    "\( #image_server_url )/ipa.initramfs"
	}
	dhcp: {
		dhcp_provider: "none"
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
}

ironic_conf_spec: ironic_template.ironic_template_conf_spec
ironic_conf_spec: {
	api: {
		host_ip: "127.0.0.1"
		port:    8080
	}
	database: {
		#connection_host:        *"mysql-keystone-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"keystone" | string
		#connection_password:    string
		#connection_database:    *"keystone" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the keystone operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	service_catalog: {
		auth_url:   string
		"auth-url": auth_url
	}
}
