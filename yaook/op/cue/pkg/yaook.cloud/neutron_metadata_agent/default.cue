// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neutron_metadata_agent

import (
	"yaook.cloud/neutron_metadata_agent_template"
	"yaook.cloud/neutron_agent"
)

neutron_metadata_agent_conf_spec: neutron_metadata_agent_template.neutron_metadata_agent_template_conf_spec
neutron_metadata_agent_conf_spec: neutron_agent.neutron_agent_conf_spec
neutron_metadata_agent_conf_spec: {
	DEFAULT: {
		"metadata_proxy_socket":      "/run/neutron/metadata_proxy_socket"
		"nova_metadata_host":         "nova-metadata"
		metadata_proxy_shared_secret: string
		nova_metadata_protocol:       "https"
		auth_ca_cert:                 "/etc/pki/tls/certs/ca-bundle.crt"
	}
}
