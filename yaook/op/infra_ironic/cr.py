#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

import yaook.op.common
import yaook.statemachine as sm
from yaook.op.common import SchedulingKey


CORE_DATABASE_NAME = "ironic"
INSPECTOR_DATABASE_NAME = "inspector"
API_SVC_USERNAME = "api"
INSPECTOR_API_SVC_USERNAME = "inspector-api"

JOB_SCHEDULING_KEYS = [
    SchedulingKey.OPERATOR_INFRA_IRONIC.value,
    SchedulingKey.OPERATOR_ANY.value,
]


def create_dnsmasq_uid(dhcprange: str) -> str:
    return dhcprange.split(',')[0].replace('.', '_')


def create_dhcpconfig(dhcprange: str) -> str:
    uid = create_dnsmasq_uid(dhcprange)
    return f"dhcp-range=set:{uid},{dhcprange}"


def create_dhcpgwoption(dhcprange: str, gateway: str) -> str:
    uid = create_dnsmasq_uid(dhcprange)
    return f"dhcp-option=tag:{uid},3,{gateway}"


class StandaloneIronicLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        cue_prefix = "ironic.#standalone_ironic_conf_template"
        # this would get way too large as an f-string
        image_server_external_url = "http://{fqdn}:{port}".format(
            fqdn=ctx.parent_spec['imageServer']['ingress']['fqdn'],
            port=ctx.parent_spec['imageServer']['ingress']['port'],
        )
        return {
            "ironic": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "DEFAULT": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.DEFAULT",
                    ),
                    "pxe": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.pxe",
                    ),
                    "deploy": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.deploy",
                    ),
                    "conductor": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.conductor",
                    ),
                    "dhcp": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.dhcp",
                    ),
                },
                {
                    "deploy": {
                        "http_url": image_server_external_url,
                    },
                    "conductor": {
                        "#image_server_url": image_server_external_url,
                    },
                    "json_rpc": {
                        "auth-url": "\\( auth_url )",
                    },
                }
            ]),
        }


class StandaloneIronicInspectorLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        cue_prefix = "ironic_inspector.#standalone_ironic_conf_template"
        return {
            "ironic_inspector": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "pxe_filter": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.pxe_filter",
                    ),
                    "processing": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.processing",
                    ),
                    "discovery": yaook.common.config.CueConfigReference(
                        f"{cue_prefix}.discovery",
                    ),
                },
            ]),
        }


class DnsmasqConfigmapTemplate(sm.TemplatedConfigMap):
    def __init__(
            self,
            **kwargs: typing.Any):
        super().__init__(**kwargs)

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap
            ) -> yaook.statemachine.resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        params["vars"]["dhcpconfig"] = []
        for x in ctx.parent_spec["pxe"]["dhcp"]:
            dhcprange = x["dhcpRange"]
            cfg = create_dhcpconfig(dhcprange)
            params["vars"]["dhcpconfig"].append(cfg)

            if 'defaultGateway' in x:
                cfg = create_dhcpgwoption(dhcprange,
                                          x['defaultGateway'])
                params["vars"]["dhcpconfig"].append(cfg)

        return params


class InfrastructureIronicDeployment(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "infrastructureironicdeployments"
    KIND = "InfrastructureIronicDeployment"
    RELEASES = ["train"]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    nginx_image = sm.VersionedDockerImage(
        "library/nginx",
        sm.SemVerSelector([
            ">=1.21.3",
        ]),
    )
    ironic_image = sm.ReleaseAwareVersionedDependency({
        "train": sm.ConfigurableVersionedDockerImage(
            'infra-ironic/infra-ironic-train',
            sm.YaookSemVerSelector(),
        ),
    },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx)
    )
    ironic_inspector_image = sm.ReleaseAwareVersionedDependency({
        "train": sm.ConfigurableVersionedDockerImage(
            'infra-ironic-inspector/infra-ironic-inspector-train',
            sm.YaookSemVerSelector(),
        ),
    },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx)
    )
    dnsmasq_image = sm.ConfigurableVersionedDockerImage(
        'infra-ironic-dnsmasq/infra-ironic-dnsmasq',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            **{release: "10.2" for release in ["queens", "rocky", "stein"]},
            **{
                release: "10.6"
                for release in [
                    "train",
                    "ussuri",
                    "victoria",
                    "wallaby",
                    "xena",
                    "yoga",
                ]
            },
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    data_volume = sm.TemplatedPersistentVolumeClaim(
        template="http-data.yaml",
    )

    # here, it's a bit more complex, because we need to have the services
    # available to write the config files, as we do not have the Keystone API
    # as shared storage for endpoints

    image_server_service = sm.TemplatedService(
        template="image-server-svc.yaml",
        params={
            "component": "image_server",
        },
    )
    image_server_ingress = sm.TemplatedIngress(
        template="image-server-ingress.yaml",
        params={
            "suffix": "imgserv",
            "crd_key": "imageServer",
            "service": "image_server_service",
            "port": 80,
        },
        add_dependencies=[
            image_server_service,
        ]
    )

    bootscripts = sm.TemplatedConfigMap(
        template="bootscripts.yaml",
        copy_on_write=True,
    )

    image_server = sm.TemplatedDeployment(
        template="image-server-deployment.yaml",
        add_dependencies=[data_volume, bootscripts],
        versioned_dependencies=[nginx_image, service_reload_image],
        scheduling_keys=[
            SchedulingKey.INFRA_BARE_METAL_API.value,
            SchedulingKey.ANY_API.value,
        ],
    )
    image_server_pdb = sm.QuorumPodDisruptionBudget(
        metadata=("infra-ironic-imgserv-pdb-", True),
        replicated=image_server,
    )

    api_certificate_secret = sm.EmptyTlsSecret(
        metadata=("infra-ironic-api-certificate-", True),
    )
    api_certificate = sm.TemplatedCertificate(
        template="api-certificate.yaml",
        add_dependencies=[api_certificate_secret],
    )
    api_ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=api_certificate,
    )
    api_ca_certs = sm.CAConfigMap(
        metadata=("infra-ironic-api-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            api_ready_certificate_secret,
        ],
    )

    api_keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="ironic",
    )
    api_keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            api_keystone_user,
        )

    api_db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "database_name": CORE_DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )

    api_db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=api_db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    api_db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("infra-ironic-api-db-api-user-", True),
        copy_on_write=True,
    )
    api_db_api_user = sm.SimpleMySQLUser(
        metadata=("infra-ironic-api-api-", True),
        database=api_db,
        username=API_SVC_USERNAME,
        password_secret=api_db_api_user_password,
    )

    config = sm.CueSecret(
        metadata=("infra-ironic-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="ironic",
                accessor="ironicConfig",
            ),
            sm.DatabaseConnectionLayer(
                target="ironic",
                config_section="database",
                service=api_db_service,
                database_name=CORE_DATABASE_NAME,
                username=API_SVC_USERNAME,
                password_secret=api_db_api_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="ironic",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.KeystoneAuthLayer(
                target="ironic",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="json_rpc",
            ),
            sm.KeystoneAuthLayer(
                target="ironic",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="service_catalog",
                interface_override="public",
            ),
            StandaloneIronicLayer(),
        ],
    )

    db_sync = sm.TemplatedJob(
        template="db-sync.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, api_ca_certs],
        versioned_dependencies=[ironic_image],
    )

    api_external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
        ctx.parent_spec["api"]["ingress"].get("externalCertificateSecretRef",
                                              {}).get("name"),
        secret_reference=api_ready_certificate_secret
    )

    api_deployment = sm.TemplatedDeployment(
        template="api-deployment.yaml",
        scheduling_keys=[
            SchedulingKey.INFRA_BARE_METAL_API.value,
            SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            data_volume,
            config,
            db_sync,
            api_ca_certs,
            api_ready_certificate_secret,
            api_external_certificate_secret,
        ],
        versioned_dependencies=[
            ironic_image,
            service_reload_image,
            ssl_terminator_image,
        ],
    )
    api_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("infra-ironic-api-pdb-", True),
         replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="api-svc.yaml",
        add_dependencies=[
            api_deployment,
        ],
    )

    api_ssl_terminator_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=api_ready_certificate_secret,
        endpoints=[
            "internal-ssl-terminator-prometheus",
        ],
    )

    api_ready_external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=api_external_certificate_secret,
        server_name_provider=lambda ctx: ctx.parent_spec["api"]["ingress"][
            "fqdn"],
        endpoints=["external-ssl-terminator-prometheus"],
    )

    api_ingress = sm.TemplatedIngress(
        template="ingress.yaml",
        params={
            "suffix": "api",
            "crd_key": "api",
            "service": "api_service",
            "port": 6386,
        },
        add_dependencies=[
            api_service,
        ]
    )
    api_endpoint = sm.Optional(
        condition=yaook.op.common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="endpoint.yaml",
            params={
                "suffix": "api",
                "crd_key": "api",
                "service": "api_service",
                "port": 6385,
                "type": "baremetal",
                "name": "ironic",
                "description": "OpenStack Bare Metal",
            },
            add_dependencies=[
                keystone,
                api_service,
            ],
        )
    )

    inspector_api_keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="ironic-inspector",
    )
    inspector_api_keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            inspector_api_keystone_user,
        )

    inspector_api_certificate_secret = sm.EmptyTlsSecret(
        metadata=("infra-ironic-inspector-certificate-", True),
    )
    inspector_api_certificate = sm.TemplatedCertificate(
        template="inspector-api-certificate.yaml",
        add_dependencies=[inspector_api_certificate_secret],
    )
    inspector_api_ready_certificate_secret = \
        sm.ReadyCertificateSecretReference(
            certificate_reference=inspector_api_certificate,
        )
    inspector_api_ca_certs = sm.CAConfigMap(
        metadata=("infra-ironic-inspector-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            inspector_api_ready_certificate_secret,
        ],
    )

    inspector_api_db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "database_name": INSPECTOR_DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )

    inspector_api_db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=inspector_api_db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    inspector_api_db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("infra-ironic-inspector-api-db-inspector-api-user-", True),
        copy_on_write=True,
    )
    inspector_api_db_api_user = sm.SimpleMySQLUser(
        metadata=("infra-ironic-inspector-api-inspector-api-", True),
        database=inspector_api_db,
        username=INSPECTOR_API_SVC_USERNAME,
        password_secret=inspector_api_db_api_user_password,
    )

    inspector_config = sm.CueSecret(
        metadata=("infra-ironic-inspector-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="ironic_inspector",
                accessor="inspectorConfig",
            ),
            sm.DatabaseConnectionLayer(
                target="ironic_inspector",
                config_section="database",
                service=inspector_api_db_service,
                database_name=INSPECTOR_DATABASE_NAME,
                username=INSPECTOR_API_SVC_USERNAME,
                password_secret=inspector_api_db_api_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="ironic_inspector",
                credentials_secret=inspector_api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.KeystoneAuthLayer(
                target="ironic_inspector",
                credentials_secret=inspector_api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="ironic",
            ),
            StandaloneIronicInspectorLayer(),
        ],
    )

    inspector_api_external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name")),
        secret_reference=inspector_api_ready_certificate_secret
    )

    inspector_db_sync = sm.TemplatedJob(
        template="inspector-db-sync.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[inspector_config, inspector_api_ca_certs],
        versioned_dependencies=[ironic_inspector_image],
    )

    inspector_api_deployment = sm.TemplatedDeployment(
        template="inspector-api-deployment.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.INFRA_BARE_METAL_API.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            inspector_config,
            inspector_db_sync,
            inspector_api_ca_certs,
            inspector_api_ready_certificate_secret,
            inspector_api_external_certificate_secret,
        ],
        versioned_dependencies=[
            ironic_inspector_image,
            service_reload_image,
            ssl_terminator_image,
        ],
    )
    inspector_api_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("infra-ironic-inspect-api-pdb-", True),
         replicated=inspector_api_deployment,
    )

    inspector_api_service = sm.TemplatedService(
        template="inspector-api-svc.yaml",
        add_dependencies=[
            inspector_api_deployment,
        ],
    )

    inspector_ssl_terminator_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=inspector_api_service,
        certificate=inspector_api_ready_certificate_secret,
        endpoints=[
            "internal-ssl-terminator-prometheus",
        ],
    )

    inspector_api_external_ssl_service_monitor = \
        sm.GeneratedServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-external-ssl-service-monitor-",
                True),
            service=inspector_api_service,
            certificate=inspector_api_external_certificate_secret,
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["ingress"]["fqdn"]),
            endpoints=["external-ssl-terminator-prometheus"],
        )

    inspector_api_ingress = sm.TemplatedIngress(
        template="ingress.yaml",
        params={
            "suffix": "inspect",
            "crd_key": "inspectorApi",
            "service": "inspector_api_service",
            "port": 5051,
        },
        add_dependencies=[
            inspector_api_service,
        ]
    )
    inspector_api_endpoint = sm.Optional(
        condition=lambda ctx: ctx.parent_spec.get('inspectorApi', {}).get(
            'publishEndpoint', True),
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="endpoint.yaml",
            params={
                "suffix": "inspect",
                "crd_key": "inspectorApi",
                "service": "inspector_api_service",
                "port": 5050,
                "type": "baremetal-introspection",
                "name": "ironic-inspector",
                "description": "OpenStack Bare Metal Introspection",
            },
            add_dependencies=[
                keystone,
                inspector_api_service,
            ],
        )
    )

    dnsmasq_config = DnsmasqConfigmapTemplate(
        template="dnsmasq-config.yaml",
        copy_on_write=True,
    )

    dnsmasq_service = sm.TemplatedService(
        template="dnsmasq-svc.yaml",
        params={
            "component": "dnsmasq_sts",
        },
    )

    dnsmasq_sts = sm.TemplatedStatefulSet(
        template="dnsmasq-sts.yaml",
        add_dependencies=[
            dnsmasq_service,
            dnsmasq_config,
        ],
        versioned_dependencies=[
            dnsmasq_image,
        ],
    )
    dnsmasq_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("infra-ironic-dnsmasq-pdb-", True),
         replicated=dnsmasq_sts,
    )

    conductor_service = sm.TemplatedService(
        template="conductor-svc.yaml",
        params={
            "component": "conductor_sts",
        },
    )

    conductor_sts = sm.TemplatedStatefulSet(
        template="conductor-sts.yaml",
        scheduling_keys=[
            SchedulingKey.INFRA_BARE_METAL_IRONIC_CONDUCTOR.value,
            SchedulingKey.INFRA_BARE_METAL_IRONIC_ANY_SERVICE.value,
        ],
        add_dependencies=[
            conductor_service,
            config,
            bootscripts,
            db_sync,
            data_volume,
            api_ca_certs,
        ],
        versioned_dependencies=[
            ironic_image,
        ],
    )
    conductor_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("infra-ironic-conductor-pdb-", True),
         replicated=conductor_sts,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(InfrastructureIronicDeployment)
