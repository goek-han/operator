##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ "%s-inspector-api" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  replicas: {{ crd_spec.api.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
      - name: ironic-inspector-api
        image: {{ versioned_dependencies["ironic_inspector_image"] }}
        command:
        - ironic-inspector
        volumeMounts:
        - name: ironic-inspector
          mountPath: /etc/ironic-inspector
        - name: ca-certs
          mountPath: /etc/pki/tls/certs
        env:
        - name: REQUESTS_CA_BUNDLE
          value: /etc/pki/tls/certs/ca-bundle.crt
        ports: []
        livenessProbe:
          exec:
            command:
              - curl
              - localhost:8080
        readinessProbe:
          exec:
            command:
              - curl
              - localhost:8080
        resources: {{ crd_spec | resources('inspectorApi.ironic-inspector-api') }}
      - name: "ssl-terminator"
        image: {{ versioned_dependencies['ssl_terminator_image'] }}
        imagePullPolicy: Always
        env:
          - name: SERVICE_PORT
            value: "5050"
          - name: LOCAL_PORT
            value: "8080"
          - name: METRICS_PORT
            value: "9090"
          - name: REQUESTS_CA_BUNDLE
            value: /etc/ssl/certs/ca-certificates.crt
        volumeMounts:
          - name: ssl-terminator-config
            mountPath: /config
          - name: tls-secret
            mountPath: /data
          - name: ca-certs
            mountPath: /etc/ssl/certs/ca-certificates.crt
            subPath: ca-bundle.crt
        livenessProbe:
          httpGet:
            path: /.yaook.cloud/ssl-terminator-healthcheck
            port: 5050
            scheme: HTTPS
        readinessProbe:
          httpGet:
            path: /
            port: 5050
            scheme: HTTPS
        resources: {{ crd_spec | resources('inspectorApi.ssl-terminator') }}
      - name: "ssl-terminator-external"
        image: {{ versioned_dependencies['ssl_terminator_image'] }}
        imagePullPolicy: Always
        env:
          - name: SERVICE_PORT
            value: "5051"
          - name: LOCAL_PORT
            value: "8080"
          - name: METRICS_PORT
            value: "9091"
          - name: REQUESTS_CA_BUNDLE
            value: /etc/ssl/certs/ca-certificates.crt
        volumeMounts:
          - name: ssl-terminator-external-config
            mountPath: /config
          - name: tls-secret-external
            mountPath: /data
          - name: ca-certs
            mountPath: /etc/ssl/certs/ca-certificates.crt
            subPath: ca-bundle.crt
        livenessProbe:
          httpGet:
            path: /.yaook.cloud/ssl-terminator-healthcheck
            port: 5051
            scheme: HTTPS
        readinessProbe:
          httpGet:
            path: /
            port: 5051
            scheme: HTTPS
        resources: {{ crd_spec | resources('inspectorApi.ssl-terminator-external') }}
      - name: "service-reload"
        image: {{ versioned_dependencies['service_reload_image'] }}
        imagePullPolicy: Always
        volumeMounts:
          - name: ssl-terminator-config
            mountPath: /config
          - name: tls-secret
            mountPath: /data
        env:
          - name: YAOOK_SERVICE_RELOAD_MODULE
            value: traefik
        args:
          - /data/
        resources: {{ crd_spec | resources('inspectorApi.service-reload') }}
      - name: "service-reload-external"
        image: {{ versioned_dependencies['service_reload_image'] }}
        imagePullPolicy: Always
        volumeMounts:
          - name: ssl-terminator-external-config
            mountPath: /config
          - name: tls-secret-external
            mountPath: /data
        env:
          - name: YAOOK_SERVICE_RELOAD_MODULE
            value: traefik
        args:
          - /data/
        resources: {{ crd_spec | resources('inspectorApi.service-reload-external') }}
      volumes:
      - name: ironic-inspector
        projected:
          sources:
          - secret:
              name: {{ dependencies['inspector_config'].resource_name() }}
              items:
              - key: ironic_inspector.conf
                path: ironic-inspector.conf
      - name: tls-secret
        secret:
          secretName: {{ dependencies['inspector_api_ready_certificate_secret'].resource_name() }}
      - name: tls-secret-external
        secret:
          secretName: {{ dependencies['inspector_api_external_certificate_secret'].resource_name() }}
      - name: ca-certs
        configMap:
          name: {{ dependencies['inspector_api_ca_certs'].resource_name() }}
      - name: ssl-terminator-config
        emptyDir: {}
      - name: ssl-terminator-external-config
        emptyDir: {}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
