#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import yaook.op.common as common
import yaook.statemachine as sm

import typing


DATABASE_NAME = "heat"
API_SVC_USERNAME = "api"
API_CFN_SVC_USERNAME = "api-cfn"
ENGINE_SVC_USERNAME = "engine"


class Heat(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "heatdeployments"
    KIND = "HeatDeployment"
    RELEASES = [
        "queens",
        "train",
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    # Container image dependencies
    heat_docker_image = common.image_dependencies(
        "heat/heat-{release}",
        RELEASES,
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            **{release: "10.2" for release in ["queens", "rocky", "stein"]},
            **{
                release: "10.6"
                for release in [
                    "train",
                    "ussuri",
                    "victoria",
                    "wallaby",
                    "xena",
                    "yoga",
                ]
            },
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )
    rabbitmq_image = sm.VersionedDockerImage(
        "library/rabbitmq",
        sm.SemVerSelector([">=3.8.0", "<4.0.0"], suffix="-management"),
    )

    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )

    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )

    # General database setup
    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "db_name": DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )
    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    # General message queue setup
    mq = sm.TemplatedAMQPServer(
        template="amqp.yaml",
        versioned_dependencies=[rabbitmq_image],
    )
    mq_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=mq,
        foreign_component=common.AMQP_SERVER_SERVICE_COMPONENT,
    )

    # Keystone user and endpoint setup
    keystone = sm.KeystoneReference()
    keystone_internal_api = common.keystone_api_config_reference(keystone)

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="heat",
    )
    keystone_user_credentials = \
        common.keystone_user_credentials_reference(keystone_user)
    keystone_endpoint = sm.Optional(
        condition=common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="heat-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    # Certificate and key setup
    # For API
    certificate_secret_api = sm.EmptyTlsSecret(
        metadata=("heat-api-certificate-", True),
    )
    certificate_api = sm.TemplatedCertificate(
        template="heat-api-certificate.yaml",
        add_dependencies=[certificate_secret_api],
    )
    ready_certificate_secret_api = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate_api,
    )
    # For API CFN
    certificate_secret_api_cfn = sm.EmptyTlsSecret(
        metadata=("heat-api-cfn-certificate-", True),
    )
    certificate_api_cfn = sm.TemplatedCertificate(
        template="heat-api-cfn-certificate.yaml",
        add_dependencies=[certificate_secret_api_cfn],
    )
    ready_certificate_secret_api_cfn = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate_api_cfn,
    )
    # CA
    ca_certs = sm.CAConfigMap(
        metadata=("heat-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret_api,
            ready_certificate_secret_api_cfn,
        ],
    )

    # Policy setup
    heat_policy = sm.PolicyConfigMap(
        metadata=("heat-policy-", True),
        policy_spec_key="policy",
        copy_on_write=True,
    )

    # Heat API and DB sync job setup
    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-api-db-user-", True),
        copy_on_write=True,
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("heat-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )

    mq_api_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-api-mq-user-", True),
        copy_on_write=True,
    )
    mq_api_user = sm.SimpleAMQPUser(
        metadata=("heat-api-", True),
        server=mq,
        username_format=API_SVC_USERNAME,
        password_secret=mq_api_user_password,
    )

    config_api = sm.CueSecret(
        metadata=("heat-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="heat",
                accessor="heatConfig",
            ),
            sm.SecretInjectionLayer(
                target="heat",
                accessor=lambda ctx: ctx.parent_spec.get("heatSecrets", []),
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.DatabaseConnectionLayer(
                target="heat",
                service=db_service,
                database_name=DATABASE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="DEFAULT",
            ),
            sm.AMQPTransportLayer(
                target="heat",
                service=mq_service,
                username=API_SVC_USERNAME,
                password_secret=mq_api_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="trustee"
            )
        ],
    )

    db_sync = sm.TemplatedJob(
        template="heat-job-db-sync.yaml",
        scheduling_keys=[
            common.SchedulingKey.OPERATOR_HEAT.value,
            common.SchedulingKey.OPERATOR_ANY.value,
        ],
        add_dependencies=[config_api, ca_certs],
        versioned_dependencies=[heat_docker_image],
    )

    api_external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret_api
    )

    api_deployment = sm.TemplatedDeployment(
        template="heat-api-deployment.yaml",
        scheduling_keys=[
            common.SchedulingKey.HEAT_API.value,
            common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config_api, db_sync, ca_certs,
            ready_certificate_secret_api,
            heat_policy,
            api_external_certificate_secret
        ],
        versioned_dependencies=[
            heat_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )

    heat_api_pdb = sm.QuorumPodDisruptionBudget(
        metadata=("heat-api-pdb-", True),
        replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="heat-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    api_internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret_api,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    api_external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",

            True),
        service=api_service,
        certificate=api_external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
        ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    api_ingress = sm.TemplatedIngress(
        template="heat-api-ingress.yaml",
        add_dependencies=[api_service],
    )

    # Heat API CFN (AWS CloudFormation compatible API)
    db_api_cfn_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-api-cfn-db-user-", True),
        copy_on_write=True,
    )
    db_api_cfn_user = sm.SimpleMySQLUser(
        metadata=("heat-api-cfn-", True),
        database=db,
        username=API_CFN_SVC_USERNAME,
        password_secret=db_api_cfn_user_password,
    )

    mq_api_cfn_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-api-cfn-mq-user-", True),
        copy_on_write=True,
    )
    mq_api_cfn_user = sm.SimpleAMQPUser(
        metadata=("heat-api-cfn-", True),
        server=mq,
        username_format=API_CFN_SVC_USERNAME,
        password_secret=mq_api_cfn_user_password,
    )

    config_api_cfn = sm.CueSecret(
        metadata=("heat-api-cfn-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="heat",
                accessor="heatConfig",
            ),
            sm.SecretInjectionLayer(
                target="heat",
                accessor=lambda ctx: ctx.parent_spec.get("heatSecrets", []),
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.DatabaseConnectionLayer(
                target="heat",
                service=db_service,
                database_name=DATABASE_NAME,
                username=API_CFN_SVC_USERNAME,
                password_secret=db_api_cfn_user_password,
                config_section="DEFAULT",
            ),
            sm.AMQPTransportLayer(
                target="heat",
                service=mq_service,
                username=API_CFN_SVC_USERNAME,
                password_secret=mq_api_cfn_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="trustee"
            )
        ],
    )

    api_cfn_external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
        ctx.parent_spec["api"]["ingress"].get("externalCertificateSecretRef",
                                              {}).get("name"),
        secret_reference=ready_certificate_secret_api_cfn
    )

    api_cfn_deployment = sm.TemplatedDeployment(
        template="heat-api-cfn-deployment.yaml",
        scheduling_keys=[
            common.SchedulingKey.HEAT_API_CFN.value,
            common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config_api_cfn, db_sync, ca_certs,
            ready_certificate_secret_api_cfn,
            api_cfn_external_certificate_secret,
            heat_policy,
        ],
        versioned_dependencies=[
            heat_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )

    heat_api_cfn_pdb = sm.QuorumPodDisruptionBudget(
        metadata=("heat-api-cfn-pdb-", True),
        replicated=api_cfn_deployment,
    )

    keystone_cfn_endpoint = sm.Optional(
        condition=lambda ctx: ctx.parent_spec.get('apiCfn', {}).get(
            'publishEndpoint', True),
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="heat-cfn-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    api_cfn_service = sm.TemplatedService(
        template="heat-api-cfn-service.yaml",
        add_dependencies=[api_cfn_deployment],
    )

    api_cfn_ssl_terminator_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_cfn_service,
        certificate=ready_certificate_secret_api_cfn,
        endpoints=["external-ssl-terminator-prometheus"],
    )

    api_cfn_ssl_external_terminator_service_monitor = \
        sm.GeneratedServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-external-ssl-service-monitor-",
                True),
            service=api_cfn_service,
            certificate=api_cfn_external_certificate_secret,
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["ingress"]["fqdn"]
            ),
            endpoints=["internal-ssl-terminator-prometheus"],
        )

    api_cfn_ingress = sm.TemplatedIngress(
        template="heat-api-cfn-ingress.yaml",
        add_dependencies=[api_cfn_service],
    )

    # Heat engine setup
    db_engine_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-engine-db-user-", True),
        copy_on_write=True,
    )
    db_engine_user = sm.SimpleMySQLUser(
        metadata=("heat-engine-", True),
        database=db,
        username=ENGINE_SVC_USERNAME,
        password_secret=db_engine_user_password,
    )

    mq_engine_user_password = sm.AutoGeneratedPassword(
        metadata=("heat-engine-mq-user-", True),
        copy_on_write=True,
    )
    mq_engine_user = sm.SimpleAMQPUser(
            metadata=("heat-engine-", True),
            server=mq,
            username_format=ENGINE_SVC_USERNAME,
            password_secret=mq_engine_user_password,
    )

    config_engine = sm.CueSecret(
        metadata=("heat-engine-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="heat",
                accessor="heatConfig",
            ),
            sm.SecretInjectionLayer(
                target="heat",
                accessor=lambda ctx: ctx.parent_spec.get("heatSecrets", []),
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.DatabaseConnectionLayer(
                target="heat",
                service=db_service,
                database_name=DATABASE_NAME,
                username=ENGINE_SVC_USERNAME,
                password_secret=db_engine_user_password,
                config_section="DEFAULT",
            ),
            sm.AMQPTransportLayer(
                target="heat",
                service=mq_service,
                username=ENGINE_SVC_USERNAME,
                password_secret=mq_engine_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="heat",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="trustee"
            )
        ],
    )

    engine = sm.TemplatedStatefulSet(
        template="heat-engine-statefulset.yaml",
        scheduling_keys=[
            common.SchedulingKey.HEAT_ENGINE.value,
        ],
        add_dependencies=[
            config_engine, ca_certs,
            heat_policy,
        ],
        versioned_dependencies=[
            heat_docker_image,
        ],
    )

    heat_engine_pdb = sm.QuorumPodDisruptionBudget(
        metadata=("heat-engine-pdb-", True),
        replicated=engine,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(Heat)
