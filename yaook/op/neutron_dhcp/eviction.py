#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import dataclasses
import enum
import random
import typing

import openstack

import yaook.statemachine.commoneviction as eviction
import yaook.statemachine.resources.openstack as os_resource


@dataclasses.dataclass(frozen=True)
class EvictionStatus():
    migrated_networks: int
    migratable_networks: int
    pending_networks: int
    unhandleable_networks: int
    unhandleable_agent: bool

    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        return {
            "migrated": self.migrated_networks,
            "migratable": self.migratable_networks,
            "pending": self.pending_networks,
            "unhandleable": self.unhandleable_networks,
            "unhandleable_agent": self.unhandleable_agent,
        }


class NetworkStatusClass(enum.Enum):
    MIGRATABLE = "migratable"
    PENDING = "pending"
    UNHANDLEABLE = "unhandleable"


ALL_OPENSTACK_STATUSES = [
    "ACTIVE",
    "BUILD",
    "DOWN",
    "ERROR"
]

STATUS_MAP = {
    # Obvious, do a migration
    "ACTIVE": NetworkStatusClass.MIGRATABLE,
    # Wait until it has completed building
    "BUILD": NetworkStatusClass.PENDING,
    # We don't know the reason for error, but should try to migrate the
    # network.
    "ERROR": NetworkStatusClass.MIGRATABLE,
    # No idea, what happens, if a network is in this state. We try to migrate
    # it. If it fails, we loop forever and a human needs to have a look.
    "DOWN": NetworkStatusClass.MIGRATABLE,
}


NetworkStatusMap = typing.Mapping[
    NetworkStatusClass,
    typing.List[openstack.network.v2.network.Network],
]


class RawActionProtocol(typing.Protocol):
    def __call__(
            self,
            client: openstack.connection.Connection,
            network_id: str,
            new_agent: str,
            old_agent: str,
            ) -> None:
        ...


class ActionProtocol(typing.Hashable, typing.Protocol):
    __name__: str

    def __call__(
            self,
            client: openstack.connection.Connection,
            network_id: str,
            new_agent: typing.Optional[str],
            old_agent: str,
            ) -> None:
        ...


def action(f: RawActionProtocol) -> ActionProtocol:
    return typing.cast(ActionProtocol, f)


@action
def delete_network_from_agent(
        client: openstack.connection.Connection,
        network_id: str,
        new_agent: typing.Optional[str],
        old_agent: str,
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.network.delete(
            f"/agents/{old_agent}/dhcp-networks/{network_id}",
        ),
    )
    return


@action
def migrate_network(
        client: openstack.connection.Connection,
        network_id: str,
        new_agent: str,
        old_agent: str,
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.network.post(
            f"/agents/{new_agent}/dhcp-networks",
            json={
                "network_id": network_id,
            },
        ),
    )
    # we may don't care, if removing network worked or the network may is
    # already removed
    delete_network_from_agent(client, network_id, None, old_agent)
    return


class ActionSpec(typing.NamedTuple):
    network_id: str
    new_agent: typing.Optional[str]
    old_agent: str
    action: ActionProtocol


class Evictor(eviction.Evictor[EvictionStatus]):
    def __init__(
            self,
            *,
            max_parallel_migrations: int,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._max_parallel_migrations = max_parallel_migrations

    def _connect(self) -> openstack.connection.Connection:
        client = openstack.connect(**self._connection_info)
        return client

    def _collect_network_status(
            self,
            client: openstack.connection.Connection,
            current_agent: openstack.network.v2.agent.Agent,
            status_map: typing.Mapping[str, NetworkStatusClass],
            ) -> NetworkStatusMap:
        result: typing.Dict[
            NetworkStatusClass,
            typing.List[openstack.network.v2.network.Network],
        ] = {
            class_: []
            for class_ in NetworkStatusClass
        }
        for network in client.network.dhcp_agent_hosting_networks(
                agent=current_agent.id,
                ):
            class_ = status_map.get(
                network.status,
                NetworkStatusClass.UNHANDLEABLE,
            )
            result[class_].append(network)

        return result

    def _compute_next_agent(
            self,
            client: openstack.connection.Connection,
            current_agent: openstack.network.v2.agent.Agent,
            status: NetworkStatusMap,
            ) -> typing.Tuple[
                typing.List[ActionSpec],
                typing.List[openstack.network.v2.network.Network]
            ]:
        result: typing.List[ActionSpec] = []
        unhandleable: typing.List[openstack.network.v2.network.Network] = []

        agents = list(client.network.agents(binary="neutron-dhcp-agent"))
        for network in status[NetworkStatusClass.MIGRATABLE]:
            sourceagent = current_agent.id
            targetagents = set()
            networkid = network.id
            for agent in agents:
                if agent.id != sourceagent and agent.is_admin_state_up and \
                        agent.is_alive:
                    targetagents.add(agent.id)

            for subnet_id in network.subnet_ids:
                subnet = client.network.get_subnet(subnet=subnet_id)
                if subnet.is_dhcp_enabled:
                    break
            else:
                self._logger.debug(
                    f"Skipping Network {networkid} as it does not have dhcp "
                    "enabled")
                continue
            hosting_ids = set([x.id for x in
                               client.network.network_hosting_dhcp_agents(
                                   network=networkid,
                               )])
            possibletargets = (targetagents - hosting_ids)
            if len(possibletargets) == 0:
                self._logger.warning(
                    f"no possible target for network {networkid} found. "
                    "Skipping.")
                if len(hosting_ids) > 2:
                    # the network is already on two agents plus the one we want
                    # to remove. So it is save to disable the agent.
                    result.append(ActionSpec(networkid, None, sourceagent,
                                             delete_network_from_agent))
                    continue
                unhandleable.append(network)
            else:
                new_agent = random.SystemRandom().choice(
                    list(possibletargets))
                result.append(ActionSpec(networkid, new_agent, sourceagent,
                                         migrate_network))
        return result, unhandleable

    def _start_actions(
            self,
            client: openstack.connection.Connection,
            actions: typing.Sequence[ActionSpec],
            ) -> typing.Tuple[int, int]:
        migrated = 0
        failing = 0
        count = 0
        max_migrations = self._max_parallel_migrations
        for action in actions:
            count += 1
            if count > max_migrations:
                break
            self._logger.debug("starting %s for network %s; "
                               "parralel_migrations left = %s",
                               action.action.__name__,
                               action.network_id,
                               str(max_migrations - count),
                               )
            attempt_key = action.network_id, action.action
            self._attempt_counter[attempt_key] += 1
            nattempt = self._attempt_counter[attempt_key]
            if nattempt % 3 == 0:
                self._logger.warning(
                    "%s: is failing to be moved (this is attempt #%d)",
                    action.network_id,
                    nattempt,
                )
            try:
                action.action(client, action.network_id, action.new_agent,
                              action.old_agent)
                migrated += 1
            except openstack.exceptions.HttpException as exc:
                self._logger.warning(
                    "failed to start action %s for instance %s: %s",
                    action.action.__name__, action.network_id, exc,
                )
                failing += 1
                continue
        return migrated, failing

    def _dump_status(
            self,
            status: NetworkStatusMap,
            ) -> None:
        if not any(status.values()):
            self._logger.debug("no networks in network status map")
            return

        self._logger.debug("network status map (empty sets omitted):")
        for k, networks in status.items():
            if not networks:
                continue
            self._logger.debug(
                "  %s: %s",
                k.name,
                ", ".join(f"{network.id} ({network.status})"
                          for network in networks),
            )

    def _iterate(
            self,
            client: openstack.connection.Connection,
            current_agent: openstack.network.v2.agent.Agent,
            status_map: typing.Mapping[str, NetworkStatusClass],
            ) -> EvictionStatus:
        status = self._collect_network_status(client, current_agent,
                                              status_map)
        self._dump_status(status)

        actions, unhandleable_list = self._compute_next_agent(
            client, current_agent, status)
        status[NetworkStatusClass.UNHANDLEABLE].extend(unhandleable_list)
        migrated, failing = self._start_actions(client, actions)

        unhandleable = status[NetworkStatusClass.UNHANDLEABLE]
        if unhandleable:
            self._logger.warning(
                "%d network(s) are not handleable: %s",
                len(unhandleable),
                ", ".join(f"{network.id} ({network.status})"
                          for network in unhandleable),
            )

        return EvictionStatus(
            migrated_networks=migrated,
            migratable_networks=len(actions) - migrated,
            pending_networks=len(status[NetworkStatusClass.PENDING]),
            unhandleable_networks=(
                len(status[NetworkStatusClass.UNHANDLEABLE]) +
                failing),
            unhandleable_agent=False,
        )

    def _os_iterate(self) -> EvictionStatus:
        client = self._connect()

        is_disabled: bool
        is_down: bool
        try:
            agent = os_resource.get_network_agent(
                client.network,
                host=self._node_name,
                binary="neutron-dhcp-agent",
            )
        except LookupError:
            self._logger.error(
                "dhcp agent %s has vanished ... we can't do anything, as we "
                "can't get the list of networks on this agent.",
                self._node_name,
            )
            # This is a very unpleasant situation; we can't get a list of
            # networks on this agent.
            # is_disabled = True
            # is_down = True
            return EvictionStatus(
                migrated_networks=0,
                migratable_networks=0,
                pending_networks=0,
                unhandleable_networks=0,
                unhandleable_agent=True,
            )
        else:
            # We only need to know if the agent is disabled, to not disable it
            # again. As long as we get agent info from the API, we can remove
            # networks. The process is the same for all states.
            is_disabled = not agent.is_admin_state_up
            is_down = not agent.is_alive
            self._logger.info(
                "dhcp agent %s: is_down=%s, is_disabled=%s",
                agent.id, is_down, is_disabled,
            )

        if not is_disabled:
            os_resource.disable_network_agent(
                client.network,
                agent,
            )

        result = self._iterate(client, agent, STATUS_MAP)

        return result

    async def is_migration_done(
            self,
            status: EvictionStatus,
            ) -> bool:
        # We don't look on migrated_networks, due to they are already done
        # and migration takes no time.
        # Everything that fails is at unhandleable_networks.
        return (status.migratable_networks + status.pending_networks +
                status.unhandleable_networks) == 0 and \
                not status.unhandleable_agent

    def start_eviction_log(
            self,
            poll_interval: int,
            ) -> str:
        return (f"initiating eviction of node {self._node_name} with reason "
                f"{self._reason}.up to {self._max_parallel_migrations} in "
                f"parallel, poll interval is {poll_interval}s")
