#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import dataclasses
import logging
import typing

import keystoneauth1.exceptions
import openstack
import openstack.exceptions

import kubernetes_asyncio.client as kclient

import yaook.statemachine as sm


def get_current_project(
        client: openstack.connection.Connection,
        ) -> openstack.identity.v3.project.Project:
    return client.identity.get_project(client.identity.get_project_id())


def create_or_update_project(
        client: openstack.connection.Connection,
        project_name: str,
        domain_id: str,
        ) -> openstack.identity.v3.project.Project:
    projects = list(client.identity.projects(
        name=project_name,
        domain_id=domain_id,
    ))
    if projects:
        return projects[0]
    return client.identity.create_project(
        name=project_name,
        domain_id=domain_id,
    )


def create_qualified_username(username: str, namespace: str) -> str:
    cluster_domain = sm.api_utils.get_cluster_domain()
    return f"{username}.{namespace}.{cluster_domain}"


def create_or_update_user(
        client: openstack.connection.Connection,
        user_client: openstack.connection.Connection,
        logger: logging.Logger,
        username: str,
        domain_id: str,
        password: str,
        ) -> openstack.identity.v3.user.User:
    users = list(client.identity.users(
        name=username,
        domain_id=domain_id,
    ))
    if not users:
        return client.identity.create_user(
            name=username,
            domain_id=domain_id,
            password=password,
        )

    user = users[0]

    try:
        user_client.identity.get_token()
    except keystoneauth1.exceptions.Unauthorized:
        logger.warn("The password for the user %s has been altered, "
                    "resetting it to the intended value", username)
        client.identity.update_user(
            user.id,
            password=password,
        )

    return user


def delete_user(
        client: openstack.connection.Connection,
        username: str,
        domain_id: str,
        ) -> None:
    users = list(client.identity.users(
        name=username,
        domain_id=domain_id,
    ))
    if not users:
        return

    client.identity.delete_user(users[0].id)


def create_or_update_project_membership(
        client: openstack.connection.Connection,
        user: openstack.identity.v3.user.User,
        project: openstack.identity.v3.project.Project,
        role: str,
        ) -> None:
    try:
        role_object = next(iter(client.identity.roles(name=role)))
    except StopIteration:
        raise ValueError(f"role not found by name: {role}") from None

    project.assign_role_to_user(
        client.identity,
        user=user,
        role=role_object,
    )


def create_or_update_service(
        client: openstack.connection.Connection,
        type_: str,
        name: str,
        description: str,
        ) -> openstack.identity.v3.service.Service:
    services = list(
        filter(lambda x: x.name == name, client.identity.services(type=type_))
    )
    if not services:
        return client.identity.create_service(
            type=type_,
            name=name,
            description=description,
        )

    service = services[0]
    client.identity.update_service(
        service.id,
        description=description,
    )
    return service


def create_or_update_region(
        client: openstack.connection.Connection,
        region_name: str,
        parent_region_id: typing.Optional[str],
        ) -> None:
    if parent_region_id:
        create_or_update_region(client, parent_region_id, None)
    try:
        region = client.identity.get_region(region_name)
        if parent_region_id and region.parent_region_id != parent_region_id:
            client.identity.update_region(region_name,
                                          parent_region_id=parent_region_id)
    except openstack.exceptions.NotFoundException:
        region = client.identity.create_region(
            id=region_name, parent_region_id=parent_region_id)


def reconcile_endpoints(
        client: openstack.connection.Connection,
        service: openstack.identity.v3.service.Service,
        region_id: str,
        parent_region_id: typing.Optional[str],
        existing_endpoints: typing.Mapping[
            str, openstack.identity.v3.endpoint.Endpoint,
        ],
        requested_endpoints: typing.Mapping[
            str, str,
        ],
        ) -> None:

    existing_keys = set(existing_endpoints.keys())
    requested_keys = set(requested_endpoints.keys())
    to_be_deleted = existing_keys - requested_keys
    to_be_created = requested_keys - existing_keys
    to_be_updated = requested_keys & existing_keys

    for interface in to_be_created:
        create_or_update_region(client, region_id, parent_region_id)
        url = requested_endpoints[interface]
        client.identity.create_endpoint(
            service_id=service.id,
            region_id=region_id,
            interface=interface,
            url=url,
        )

    for key in to_be_deleted:
        endpoint = existing_endpoints[key]
        client.identity.delete_endpoint(endpoint.id)

    for key in to_be_updated:
        endpoint = existing_endpoints[key]
        new_url = requested_endpoints[key]
        if endpoint.url == new_url:
            continue
        client.identity.update_endpoint(
            endpoint.id,
            url=new_url,
            region_id=region_id,
        )


def create_or_update_endpoints(
        client: openstack.connection.Connection,
        service: openstack.identity.v3.service.Service,
        endpoint_map: typing.Mapping[
            str, str,
        ],
        region_id: str,
        parent_region_id: typing.Optional[str],
        ) -> None:
    existing_endpoints: typing.Dict[
        str,
        openstack.identity.v3.endpoint.Endpoint,
    ] = {}

    for ep in client.identity.endpoints(service_id=service.id):
        if ep.region_id == region_id:
            existing_endpoints[ep.interface] = ep

    reconcile_endpoints(
        client,
        service,
        region_id,
        parent_region_id,
        existing_endpoints,
        endpoint_map,
    )


def get_service(
        client: openstack.connection.Connection,
        servicetype: str,
        ) -> typing.Optional[openstack.identity.v3.service.Service]:
    services = list(
        client.identity.services(type=servicetype)
    )

    if not services:
        return None
    return services[0]


def delete_endpoints(
        client: openstack.connection.Connection,
        service: openstack.identity.v3.service.Service,
        region_id: str,
        ) -> None:
    endpoints = list(
        filter(lambda x: x.region_id == region_id,
               client.identity.endpoints(service_id=service.id))
    )
    if not endpoints:
        return
    for endpoint in endpoints:
        client.identity.delete_endpoint(endpoint.id)


def delete_service(
        client: openstack.connection.Connection,
        service: openstack.identity.v3.service.Service,
        logger: logging.Logger,
        ) -> None:

    endpoints = list(client.identity.endpoints(service_id=service.id))
    if endpoints:
        logger.info("There are endpoints in other regions registered for this "
                    "service. Not deleting the service.")
        return
    client.identity.delete_service(service.id)


class KeystoneUser(sm.resources.KeystoneResource):
    def __init__(
            self,
            *,
            admin_credentials: sm.KubernetesReference[kclient.V1Secret],
            endpoint_config: sm.KubernetesReference[kclient.V1ConfigMap],
            user_credentials: sm.KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any):
        super().__init__(
            admin_credentials=admin_credentials,
            endpoint_config=endpoint_config,
            **kwargs)
        self._user_credentials = user_credentials
        self._declare_dependencies(user_credentials)

    def _reconcile_user(
            self,
            logger: logging.Logger,
            connection_parameters: sm.resources._OpaqueParameters,
            user_parameters: sm.resources._OpaqueParameters,
            ) -> None:
        username = user_parameters.params["username"]
        password = user_parameters.params["password"]
        project_name = user_parameters.params["project_name"]

        client = openstack.connect(**connection_parameters.params)

        user_parameters = dataclasses.replace(connection_parameters)
        user_parameters.params["auth"]["password"] = password
        user_parameters.params["auth"]["username"] = username
        user_parameters.params["auth"]["project_name"] = project_name
        userclient = openstack.connect(**user_parameters.params)

        auth_project = get_current_project(client)
        domain_id = auth_project.domain_id
        logger.debug("connected to OpenStack with %s", auth_project)

        project = create_or_update_project(
            client,
            project_name=project_name,
            domain_id=domain_id,
        )
        logger.debug("using project %s for user %s",
                     project.id, username)

        user = create_or_update_user(
            client,
            userclient,
            logger,
            username=username,
            domain_id=domain_id,
            password=password,
        )
        logger.debug("created user %s for user %s",
                     user.id, username)

        create_or_update_project_membership(
            client,
            user=user,
            project=project,
            role="admin",
        )
        logger.debug("updated role assignment")
        logger.info("updated user %s: user_id=%s, project_id=%s",
                    username, user.id, project.id)

    def _delete_user(
            self,
            logger: logging.Logger,
            connection_parameters: sm.resources._OpaqueParameters,
            username: str,
            ) -> None:
        client = openstack.connect(**connection_parameters.params)
        auth_project = get_current_project(client)
        domain_id = auth_project.domain_id
        logger.debug("connected to OpenStack with %s", auth_project)

        delete_user(
            client,
            username=username,
            domain_id=domain_id,
        )
        logger.info("deleted user")

    async def _get_user_credentials(
            self,
            ctx: sm.Context) -> sm.resources._OpaqueParameters:
        secret_ref = await self._user_credentials.get(ctx)
        secrets = sm.secret_interface(ctx.api_client)
        secret = await secrets.read(secret_ref.namespace, secret_ref.name)
        data = sm.api_utils.decode_secret_data(secret.data)
        return sm.resources._OpaqueParameters({
            "username": data["OS_USERNAME"],
            "project_name": data["OS_PROJECT_NAME"],
            "password": data["OS_PASSWORD"],
        })

    async def update(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        connection_parameters = await self._get_connection_params(ctx)
        user_parameters = await self._get_user_credentials(ctx)

        try:
            await asyncio.get_event_loop().run_in_executor(
                None,
                self._reconcile_user,
                ctx.logger,
                connection_parameters,
                user_parameters,
            )
        except keystoneauth1.exceptions.ConnectFailure as exc:
            # stripping off the context here because it’s like a gazillion
            # lines without any use
            raise exc from None

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> None:
        connection_parameters = await self._get_connection_params(ctx)
        username = create_qualified_username(ctx.parent_name,
                                             ctx.namespace)

        try:
            await asyncio.get_event_loop().run_in_executor(
                None,
                self._delete_user,
                ctx.logger,
                connection_parameters,
                username,
            )
        except keystoneauth1.exceptions.ConnectFailure as exc:
            # stripping off the context here because it’s like a gazillion
            # lines without any use
            raise exc from None


class KeystoneEndpoint(sm.resources.KeystoneResource):
    def _reconcile_service(
            self,
            logger: logging.Logger,
            connection_parameters: sm.resources._OpaqueParameters,
            parent_spec: typing.Mapping[str, typing.Any],
            ) -> None:
        client = openstack.connect(**connection_parameters.params)

        service = create_or_update_service(
            client,
            type_=parent_spec["servicetype"],
            name=parent_spec["servicename"],
            description=parent_spec.get("description", ""),
        )
        logger.debug("created service %s", service.id)

        create_or_update_endpoints(
            client,
            service,
            {
                interface: url
                for interface, url in parent_spec["endpoints"].items()
            },
            parent_spec["region"]["name"],
            parent_spec["region"].get("parent"),
        )
        logger.info("updated: service_id=%s", service.id)

    def _delete_service(
            self,
            logger: logging.Logger,
            connection_parameters: sm.resources._OpaqueParameters,
            parent_spec: typing.Mapping[str, typing.Any],
            ) -> None:
        client = openstack.connect(**connection_parameters.params)

        service = get_service(client, parent_spec["servicetype"])

        if not service:
            logger.warn("no service for type %s exists. "
                        "Don't need to do anything",
                        parent_spec["servicetype"])
            return

        delete_endpoints(
            client,
            service,
            parent_spec["region"]["name"],
        )

        delete_service(
            client,
            service,
            logger,
        )

    async def update(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        connection_parameters = await self._get_connection_params(ctx)

        try:
            await asyncio.get_event_loop().run_in_executor(
                None,
                self._reconcile_service,
                ctx.logger,
                connection_parameters,
                ctx.parent_spec,
            )
        except keystoneauth1.exceptions.ConnectFailure as exc:
            # stripping off the context here because it’s like a gazillion
            # lines without any use
            raise exc from None

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> None:
        connection_parameters = await self._get_connection_params(ctx)

        try:
            await asyncio.get_event_loop().run_in_executor(
                None,
                self._delete_service,
                ctx.logger,
                connection_parameters,
                ctx.parent_spec
            )
        except keystoneauth1.exceptions.ConnectFailure as exc:
            # stripping off the context here because it’s like a gazillion
            # lines without any use
            raise exc from None
