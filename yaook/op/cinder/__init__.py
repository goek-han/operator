#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

import yaook
import yaook.common.config
import yaook.op.common
import yaook.op.tasks
import yaook.statemachine as sm
from yaook.statemachine import resources

from yaook.statemachine.cue import CueInput


async def _backup_spec_accessor(ctx: sm.Context) -> typing.Set[str]:
    return ctx.parent_spec.get("backup", {}).keys()


async def _backend_spec_accessor(ctx: sm.Context) -> typing.Set[str]:
    return ctx.parent_spec.get("backends", {}).keys()


def _copyoffload_configmap(backendspec: typing.Mapping[str,
                           typing.Mapping[str, typing.Any]]) -> \
                           typing.Optional[str]:
    copyoffload_config_maps = {
        backend.get("netapp", {}).get("copyoffloadConfigMap", {}).get("name")
        for backend in backendspec.values()
        if "netapp" in backend
        and backend.get("netapp", {}).get("copyoffloadConfigMap")
        }
    if len(copyoffload_config_maps) > 1:
        raise sm.ConfigurationInvalid("Multiple diffrent configmap for the "
                                      "netapp copyoffload defined.")
    if len(copyoffload_config_maps) == 0:
        return None
    return copyoffload_config_maps.pop()


class CephLayer(sm.CueLayer):
    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> sm.cue.CueInput:
        backends = ctx.parent_spec["backends"]

        out_cinder: typing.List[typing.Mapping[str, typing.Any]] = []
        out_ceph: typing.List[typing.Mapping[str, typing.Any]] = []

        backend_name = ctx.instance
        if backend_name is None:
            return {}
        backend = backends.get(backend_name, {})

        if "rbd" not in backend:
            return {}

        rbdconfig = backend["rbd"]
        username = rbdconfig["keyringUsername"]

        out_cinder.append(
            {
                backend_name: yaook.common.config.CueConfigReference(
                    "cinder.cinder_backend_ceph"
                ),
            }
        )
        out_cinder.append(
            {
                backend_name: {
                    "rbd_user": username,
                },
            },
        )
        if "backendConfig" in rbdconfig:
            out_cinder.append(
                {
                    backend_name: rbdconfig["backendConfig"]
                }
            )
        out_ceph.append(
            {
                f"client.{username}": rbdconfig.get("cephConfig", {})
            }
        )
        out_ceph.append(
            {
                f"client.{username}": {
                    "keyfile": f"/etc/ceph/{username}"
                },
            },
        )
        return {
            "cinder": yaook.common.config.OSLO_CONFIG.declare(out_cinder),
            "ceph": yaook.common.config.CEPH_CONFIG.declare(out_ceph)
        }


class NetappLayer(sm.CueLayer):
    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> sm.cue.CueInput:
        backends = ctx.parent_spec["backends"]

        out_cinder: typing.List[typing.Mapping[str, typing.Any]] = []

        secrets = sm.interfaces.secret_interface(ctx.api_client)

        backend_name = ctx.instance
        backend = backends.get(backend_name, {})

        if backend_name is None:
            return {}

        if "netapp" not in backend:
            return {}

        netappconfig = backend["netapp"]

        credentials = sm.api_utils.decode_secret_data(
            (await secrets.read(ctx.namespace,
                                netappconfig["passwordReference"])).data
        )

        out_cinder.append(
            {
                backend_name: yaook.common.config.CueConfigReference(
                    "cinder.cinder_backend_netapp"
                ),
            }
        )
        out_cinder.append(
            {
                backend_name: {
                    "netapp_login": netappconfig["login"],
                    "netapp_password": credentials["password"],
                    "netapp_server_hostname": netappconfig["server"],
                    "netapp_vserver": netappconfig["vserver"],
                },
            },
        )
        if "backendConfig" in netappconfig:
            out_cinder.append(
                {
                    backend_name: netappconfig["backendConfig"]
                }
            )
        if "copyoffloadConfigMap" in netappconfig:
            out_cinder.append(
                {
                    backend_name: {
                        "netapp_copyoffload_tool_path":
                        "/na_copyoffload/na_copyoffload_64"
                    },
                },
            )
        return {
            "cinder": yaook.common.config.OSLO_CONFIG.declare(out_cinder)
        }


class EnabledBackendLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> CueInput:
        return {
            "cinder": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "DEFAULT": {
                        "enabled_backends": [ctx.instance]
                    }
                }
            ]),
        }


class Config(sm.CueSecret):
    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.ResourceBody:
        body = await super()._make_body(ctx, dependencies)
        backends = ctx.parent_spec["backends"]

        if ctx.instance is not None:
            backend = backends[ctx.instance]
            if "netapp" in backend:
                netappdata = {}
                filename = f"nfs_shares_{backend['netapp']['vserver']}"
                netappdata[filename] = '\n'.join(backend['netapp']
                                                 ['shares'])

                encdata = sm.api_utils.encode_secret_data(netappdata,
                                                          encoding="utf-8")
                body["data"] = {**body["data"], **encdata}
        return body


class Deployment(sm.TemplatedDeployment):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        backends = ctx.parent_spec["backends"]
        param["copyoffload_configmap"] = _copyoffload_configmap(backends)
        return param


class StatefulSet(sm.TemplatedStatefulSet):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        backends = ctx.parent_spec["backends"]
        param["copyoffload_configmap"] = _copyoffload_configmap(backends)
        return param


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_CINDER.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]

BACKUP_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.BLOCK_STORAGE_CINDER_BACKUP.value,
    yaook.op.common.SchedulingKey.BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
]


DATABASE_NAME = "cinder"
API_SVC_USERNAME = "api"


class Cinder(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "cinderdeployments"
    KIND = "CinderDeployment"
    RELEASES = [
        "queens",
        "rocky",
        "stein",
        "train",
        "ussuri",
        "victoria",
        "wallaby",
        "xena",
        "yoga",
    ]
    VALID_UPGRADE_TARGETS = [
        "rocky",
        "stein",
        "train",
        "ussuri",
    ]

    ADDITIONAL_PERMISSIONS = (
        # To send signals to the pods of the statefulsets so that they reload
        # their config.
        (False, "", "pods/exec", {"create", "get"}),
    )

    cinder_docker_image = yaook.op.common.image_dependencies(
        "cinder/cinder-{release}",
        RELEASES,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            **{release: "10.2" for release in ["queens", "rocky", "stein"]},
            **{
                release: "10.6"
                for release in [
                    "train",
                    "ussuri",
                    "victoria",
                    "wallaby",
                    "xena",
                    "yoga",
                ]
            },
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )
    rabbitmq_image = sm.VersionedDockerImage(
        "library/rabbitmq",
        sm.SemVerSelector([">=3.8.0", "<4.0.0"], suffix="-management"),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone
    )

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "db_name": DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )
    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    mq = sm.TemplatedAMQPServer(
        template="amqp.yaml",
        versioned_dependencies=[rabbitmq_image],
    )
    mq_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=mq,
        foreign_component=yaook.op.common.AMQP_SERVER_SERVICE_COMPONENT,
    )

    mq_api_user_password = sm.AutoGeneratedPassword(
        metadata=("cinder-api-mq-user-", True),
        copy_on_write=True,
    )
    mq_api_user = sm.SimpleAMQPUser(
        metadata=("cinder-api-", True),
        server=mq,
        username_format=API_SVC_USERNAME,
        password_secret=mq_api_user_password,
    )

    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("cinder-api-db-user-", True),
        copy_on_write=True,
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("cinder-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="cinder",
    )
    keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            keystone_user
        )
    keystone_endpoint = sm.Optional(
        condition=yaook.op.common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="cinder-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("cinder-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="cinder-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("cinder-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
    )

    config = Config(
        metadata=("cinder-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="cinder",
                accessor="cinderConfig",
            ),
            sm.SecretInjectionLayer(
                target="cinder",
                accessor=lambda ctx: ctx.parent_spec.get("cinderSecrets", []),
            ),
            sm.KeystoneAuthLayer(
                target="cinder",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.DatabaseConnectionLayer(
                target="cinder",
                service=db_service,
                database_name=DATABASE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="database",
            ),
            sm.AMQPTransportLayer(
                target="cinder",
                service=mq_service,
                username=API_SVC_USERNAME,
                password_secret=mq_api_user_password,
            ),
            sm.RegionNameConfigLayer(
                target="cinder",
            ),
        ],
    )

    db_sync = sm.Optional(
        condition=sm.optional_non_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="cinder-job-db-sync.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[cinder_docker_image],
        )
    )
    db_upgrade_pre = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="cinder-job-db-upgrade-pre.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[cinder_docker_image],
        )
    )

    cinder_policy = sm.PolicyConfigMap(
        metadata=("cinder-policy-", True),
        policy_spec_key="policy",
        copy_on_write=True,
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name")
        ),
        secret_reference=ready_certificate_secret
    )

    api_deployment = Deployment(
        template="cinder-deployment-api.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.BLOCK_STORAGE_API.value,
            yaook.op.common.SchedulingKey.
            BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config, db_sync, ca_certs, cinder_policy,
            ready_certificate_secret,
            external_certificate_secret,
            db_upgrade_pre,
        ],
        versioned_dependencies=[
            cinder_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )
    api_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("cinder-api-pdb-", True),
         replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="cinder-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
            ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    api_ingress = sm.TemplatedIngress(
        template="cinder-api-ingress.yaml",
        add_dependencies=[api_service],
    )

    db_cleanup = sm.TemplatedCronJob(
        template="cinder-cronjob-db-cleanup.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs, api_deployment],
        versioned_dependencies=[cinder_docker_image],
    )

    scheduler = StatefulSet(
        template="cinder-statefulset-scheduler.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.BLOCK_STORAGE_CINDER_SCHEDULER.value,
            yaook.op.common.SchedulingKey.
            BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
        ],
        add_dependencies=[config, db_sync, ca_certs, db_upgrade_pre],
        versioned_dependencies=[cinder_docker_image],
    )
    scheduler_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("cinder-scheduler-pdb-", True),
         replicated=scheduler,
    )

    # volume services
    db_volume_user_password = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.AutoGeneratedPassword(
            metadata=("cinder-volume-db-user-", True),
            copy_on_write=True,
        ),
    )
    db_volume_user = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.SimpleMySQLUser(
            metadata=("cinder-volume-", True),
            database=db,
            username="volume-{instance}",
            password_secret=db_volume_user_password,
        ),
    )

    mq_volume_user_passwords = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.AutoGeneratedPassword(
            metadata=("cinder-volume-mq-user-", True),
        ),
    )
    mq_volume_user = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.SimpleAMQPUser(
            metadata=("cinder-volume-", True),
            server=mq,
            username_format="volume-{instance}",
            password_secret=mq_volume_user_passwords,
        ),
    )

    volume_keystone_user = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.InstancedKeystoneUser(
            prefix="cinder-volume",
            keystone=keystone,
        )
    )
    volume_keystone_user_passwords = \
        yaook.op.common.keystone_user_credentials_reference(
            volume_keystone_user,
        )

    volume_config = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=Config(
            metadata=("cinder-volume-config-", True),
            copy_on_write=True,
            add_cue_layers=[
                sm.SpecLayer(
                    target="cinder",
                    accessor="cinderConfig",
                ),
                sm.KeystoneAuthLayer(
                    target="cinder",
                    credentials_secret=volume_keystone_user_passwords,
                    endpoint_config=keystone_internal_api,
                ),
                sm.KeystoneAuthLayer(
                    target="cinder",
                    config_section="nova",
                    credentials_secret=volume_keystone_user_passwords,
                    endpoint_config=keystone_internal_api,
                ),
                sm.AMQPTransportLayer(
                    target="cinder",
                    service=mq_service,
                    username=lambda ctx: f"volume-{ctx.instance}",
                    password_secret=mq_volume_user_passwords,
                ),
                sm.DatabaseConnectionLayer(
                    target="cinder",
                    service=db_service,
                    database_name=DATABASE_NAME,
                    username=lambda ctx: f"volume-{ctx.instance}",
                    password_secret=db_volume_user_password,
                    config_section="database",
                ),
                sm.SecretInjectionLayer(
                    target="cinder",
                    accessor=lambda ctx: ctx.parent_spec[
                        "backends"][ctx.instance].get("cinderSecrets", {}),
                ),
                CephLayer(),
                NetappLayer(),
                EnabledBackendLayer(),
                sm.RegionNameConfigLayer(
                    target="cinder",
                ),
            ],
        )
    )

    volume_deployment = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=Deployment(
            template="cinder-deployment-volume.yaml",
            scheduling_keys=[
                yaook.op.common.SchedulingKey
                .BLOCK_STORAGE_CINDER_VOLUME.value,
                yaook.op.common.SchedulingKey.
                BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
            ],
            add_dependencies=[volume_config, db_sync, ca_certs,
                              db_upgrade_pre],
            versioned_dependencies=[cinder_docker_image],
        )
    )
    volume_pdb = sm.PerSetEntry(
        accessor=_backend_spec_accessor,
        wrapped_state=sm.QuorumPodDisruptionBudget(
         metadata=("cinder-volume-pdb-", True),
         replicated=volume_deployment,
        )
    )

    # backup services
    db_backup_user_password = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.AutoGeneratedPassword(
            metadata=("cinder-backup-db-user-", True),
            copy_on_write=True,
        ),
    )
    db_backup_user = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.SimpleMySQLUser(
            metadata=("cinder-backup-", True),
            database=db,
            username="backup-{instance}",
            password_secret=db_backup_user_password,
        ),
    )

    mq_backup_user_passwords = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.AutoGeneratedPassword(
            metadata=("cinder-backup-mq-user-", True),
        ),
    )
    mq_backup_user = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.SimpleAMQPUser(
            metadata=("cinder-backup-", True),
            server=mq,
            username_format="backup-{instance}",
            password_secret=mq_backup_user_passwords,
        ),
    )

    backup_keystone_users = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.InstancedKeystoneUser(
            prefix="cinder-backup",
            keystone=keystone,
        )
    )
    backup_keystone_user_passwords = \
        yaook.op.common.keystone_user_credentials_reference(
            backup_keystone_users,
        )

    backup_configs = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.CueSecret(
            metadata=("cinder-backup-config-", True),
            copy_on_write=True,
            add_cue_layers=[
                sm.KeystoneAuthLayer(
                    target="cinder",
                    credentials_secret=backup_keystone_user_passwords,
                    endpoint_config=keystone_internal_api,
                ),
                sm.AMQPTransportLayer(
                    target="cinder",
                    service=mq_service,
                    username=lambda ctx: f"backup-{ctx.instance}",
                    password_secret=mq_backup_user_passwords,
                ),
                sm.DatabaseConnectionLayer(
                    target="cinder",
                    service=db_service,
                    database_name=DATABASE_NAME,
                    username=lambda ctx: f"backup-{ctx.instance}",
                    password_secret=db_backup_user_password,
                    config_section="database",
                ),
                sm.SpecLayer(
                    target="cinder",
                    accessor=lambda ctx: ctx.parent_spec[
                        "backup"][ctx.instance]["cinderConfig"],
                ),
                sm.SecretInjectionLayer(
                    target="cinder",
                    accessor=lambda ctx: ctx.parent_spec[
                        "backup"][ctx.instance].get("cinderSecrets", {}),
                ),
                sm.RegionNameConfigLayer(
                    target="cinder",
                ),
            ],
        ),
    )

    backup_agent_statefulsets = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.TemplatedStatefulSet(
            template="cinder-statefulset-backup.yaml",
            scheduling_keys=BACKUP_SCHEDULING_KEYS,
            add_dependencies=[db_sync, backup_configs, ca_certs,
                              db_upgrade_pre],
            versioned_dependencies=[
                cinder_docker_image,
            ],
        ),
    )
    backup_agent_pdb = sm.PerSetEntry(
        accessor=_backup_spec_accessor,
        wrapped_state=sm.QuorumPodDisruptionBudget(
            metadata=("cinder-backup-pdb-", True),
            replicated=backup_agent_statefulsets,
        ),
    )

    upgrade_barrier = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.Barrier(
            add_dependencies=[
                backup_agent_statefulsets,
                volume_deployment,
                scheduler,
                api_deployment,
            ],
        ),
    )

    sighup_backup = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.SendSignal(
            ref=backup_agent_statefulsets,
            container="cinder-backup",
            process_name="cinder-backup",
            signal="HUP",
            add_dependencies=[
                upgrade_barrier,
            ],
        ),
    )

    sighup_volume = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.SendSignal(
            ref=volume_deployment,
            container="cinder-volume",
            process_name="cinder-volume",
            signal="HUP",
            add_dependencies=[
                upgrade_barrier,
            ],
        ),
    )

    sighup_scheduler = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.SendSignal(
            ref=scheduler,
            container="cinder-scheduler",
            process_name="cinder-scheduler",
            signal="HUP",
            add_dependencies=[
                upgrade_barrier,
            ],
        ),
    )

    restart_api = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TriggerRollingRestart(
            controller_ref=api_deployment,
            restart_id=lambda ctx: (
                f"{ctx.parent_spec['targetRelease']}-"
                f"{ctx.parent['metadata']['generation']}"
            ),
            add_dependencies=[
                upgrade_barrier,
                sighup_scheduler,
                sighup_volume,
                sighup_backup,
            ],
        )
    )

    db_upgrade_post = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="cinder-job-db-upgrade-post.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[
                config, ca_certs,
                upgrade_barrier,
                restart_api,
                sighup_backup,
                sighup_volume,
                sighup_scheduler,
            ],
            versioned_dependencies=[cinder_docker_image],
        )
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)

    # just exists for transion period remove after 2023-02-28 if still present
    async def _validate_config(self, ctx: sm.Context) -> None:
        await super()._validate_config(ctx)
        if "volume" in ctx.parent_spec.keys():
            raise sm.ConfigurationInvalid(
                "Volume section are not longer supported directly under spec."
                " Use the volume in backends section instead."
            )


sm.register(Cinder)
