#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

import yaook
import yaook.common.config
import yaook.op.common
import yaook.op.tasks
import yaook.statemachine as sm
from yaook.statemachine import resources


class RegionLayer(sm.CueLayer):
    async def get_layer(
            self,
            ctx: sm.Context) -> sm.cue.CueInput:
        return {
            "tempest": yaook.common.config.OSLO_CONFIG.declare([{
                "identity": {
                    "region": ctx.parent_spec["region"],
                },
            }])
        }


class TempestJob(sm.FinalTemplatedJob):
    def _get_tempest_suffix_for_service(self, service: str) -> str:
        if service == 'keystone':
            return "tempest.api.identity"
        if service == 'nova':
            return "tempest.api.compute"
        if service == 'glance':
            return "tempest.api.image"
        if service == 'cinder':
            return "tempest.api.volume"
        if service == 'neutron':
            return "tempest.api.network"
        if service == 'swift':
            return "tempest.api.object-storage"
        if service == 'all':
            return "tempest.api"
        raise yaook.common.config.ConfigValidationError(
            "service %s is unkown. Therefor no tempest run can be scheduled"
            % service
        )

    async def _get_template_parameters(self,
                                       ctx: sm.Context,
                                       dependencies: resources.DependencyMap
                                       ) -> resources.TemplateParameters:
        parameters = await super()._get_template_parameters(ctx, dependencies)
        if "service" in ctx.parent_spec["target"]:
            parameters["tempest_regex"] = self._get_tempest_suffix_for_service(
                ctx.parent_spec["target"]["service"]
            )
        else:
            parameters["tempest_regex"] = ctx.parent_spec["target"]["regex"]
        return parameters


class ExcludeListFile(sm.ConfigMap):
    def __init__(self,
                 *,
                 metadata: sm.MetadataProvider,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:

        entry_list = ctx.parent_spec.get("exclude", [])
        content = "\n".join(entry_list)

        return {
            "apiVersion": "v1",
            "kind": "ConfigMap",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "data": {
                "exclude.list": content,
            }
        }


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_TEMPEST.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]


class Tempest(sm.OneshotCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "tempestjobs"
    KIND = "TempestJob"
    RELEASES = [
        "27.0.0",
        "32.0.0"
    ]
    tempest_docker_image = yaook.op.common.image_dependencies(
        "tempest/tempest-{release}",
        RELEASES,
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
        component=yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT
    )

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="tempest",
    )
    keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            keystone_user,
        )

    ca_certs = sm.ForeignResourceDependency(
        resource_interface_factory=sm.config_map_interface,
        foreign_component=yaook.op.common.KEYSTONE_CA_CERTIFICATES_COMPONENT,
        foreign_resource=keystone
    )

    config = sm.CueSecret(
        metadata=("tempest-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="tempest",
                accessor="tempestConfig",
            ),
            RegionLayer(),
            sm.KeystoneAuthLayer(
                target="tempest",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
        ],
    )

    exclude_list = ExcludeListFile(
        metadata=("tempest-excludelist-", True),
    )

    tempest_job = TempestJob(
        template="tempest-job.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            config,
            ca_certs,
            exclude_list,
        ],
        versioned_dependencies=[tempest_docker_image],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        # This needs to be done here to avoid issues with `__set_name__`
        self.FINAL_STATE = self.tempest_job


sm.register(Tempest)
