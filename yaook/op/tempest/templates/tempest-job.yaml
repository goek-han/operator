##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: {{ "tempest-%s-" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  # In case of tempest fail, container should not be restarted,
  # as error is not caused by container
  backoffLimit: 0
  template:
    spec:
      restartPolicy: Never
      securityContext:
        runAsNonRoot: true
        runAsUser: 2500014
        runAsGroup: 2500014
        fsGroup: 2500014
      automountServiceAccountToken: false
      imagePullSecrets:
        - name: regcred
      containers:
        - name: tempest
          image: {{ versioned_dependencies['tempest_docker_image'] }}
          imagePullPolicy: Always
          command:
            - tempest
            - run
{% if crd_spec.serial | default(False) %}
            - "--serial"
{% endif %}
            - "--config-file"
            - /home/tempest/etc/tempest.conf
            - "--blacklist-file"
            - /home/tempest/exclude.list
            - "--regex"
            - {{ tempest_regex }}
          workingDir: /home/tempest
          securityContext:
            runAsUser: 2500014
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
            - name: tempest-conf-volume
              mountPath: /home/tempest/etc/
              readOnly: true
            - name: tempest-excludelist-volume
              mountPath: /home/tempest/exclude.list
              subPath: exclude.list
          resources: {{ crd_spec | resources('tempest-job') }}
      volumes:
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tempest-conf-volume
          secret:
             secretName: {{ dependencies['config'].resource_name() }}
             defaultMode: {{ "u=r" | chmod }}
        - name: tempest-excludelist-volume
          configMap:
             name: {{ dependencies['exclude_list'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
