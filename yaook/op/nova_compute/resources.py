#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
from datetime import datetime
import typing

import openstack
import keystoneauth1

import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.statemachine as sm
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.watcher as watcher


class SpecSequenceLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        return {
            "novacompute": yaook.common.config.OSLO_CONFIG.declare(
                list(ctx.parent_spec["novaConfig"]),
            ),
        }


class VNCLayer(sm.CueLayer):
    def __init__(
            self, **kwargs: typing.Any):
        super().__init__(**kwargs)

    async def get_layer(
            self,
            ctx: sm.Context) -> sm.cue.CueInput:
        return {
            "novacompute": yaook.common.config.OSLO_CONFIG.declare([{
                "vnc": {
                    "novncproxy_base_url": ctx.parent_spec["vnc"]["baseUrl"],
                },
            }]),
        }


class CephConfigLayer(sm.CueLayer):
    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> sm.cue.CueInput:
        if not ctx.parent_spec["cephBackend"]["enabled"]:
            return {}

        ceph_backend_cfg = ctx.parent_spec["cephBackend"]
        return {
            "ceph": yaook.common.config.CEPH_CONFIG.declare([
                ceph_backend_cfg.get("cephConfig", {}),
                {
                    f"client.{ceph_backend_cfg['user']}": {
                        "keyfile": "/etc/ceph/keyfile",
                    },
                },
            ])
        }


class CephConfigResource(sm.CueSecret):
    async def _render_cue_config(
            self,
            ctx: sm.Context,
            ) -> sm.ResourceBody:
        result = await super()._render_cue_config(ctx)

        if ctx.parent_spec["cephBackend"]["enabled"]:
            ceph_backend_cfg = ctx.parent_spec["cephBackend"]
            result["idmap"] = f"{ceph_backend_cfg['uuid']};/etc/ceph/keyfile"

        return result


def get_compute_service(
        client: openstack.connection.Connection,
        host: str,
        ) -> typing.Optional[openstack.compute.v2.service.Service]:
    services = [
        svc for svc in client.compute.services()
        if svc.binary == "nova-compute" and svc.host == host
    ]
    if not services:
        return None
    return services[0]


class ComputeStateResource(resource.APIStateResource):
    def __init__(self, **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.novaComputeWatch: watcher.ExternalWatcher[
            openstack.compute.v2.service.Service] = NovaComputeWatcher()

    def _get_resource(
            self,
            client: openstack.connection.Connection,
            ctx: sm.Context,
            ) -> typing.Optional[resource.ResourceStatus]:
        service = get_compute_service(client, ctx.parent_name)
        if service is None:
            return None

        # If the nova service was last time updated before we where created
        # it is probably an old instance that has not been cleaned up.
        # In this case we just behave as if the service was not known to nova.
        # The used format string is taken from the nova repository here
        # https://opendev.org/openstack/nova/src/commit/755aa11e0c9a59dc1cdb6de5c71f4f9249b37741/nova/utils.py#L880  # noqa: E501
        updated_at = datetime.strptime(service.updated_at,
                                       "%Y-%m-%dT%H:%M:%S.%f")
        created_at = ctx.creation_timestamp
        if updated_at < created_at:
            ctx.logger.warn(
                "nova service for %s was updated before this resource "
                "was created. Treating it as non existent", ctx.parent_name)
            return None

        return resource.ResourceStatus(
            up=service.state == "up",
            enabled=service.status == "enabled",
            disable_reason=service.disabled_reason or None,
        )

    def _update_status(
            self,
            ctx: sm.Context,
            connection_info: typing.Mapping[str, typing.Any],
            enabled: bool,
            ) -> typing.Optional[resource.ResourceStatus]:
        client = openstack.connect(**connection_info)
        service = get_compute_service(client, ctx.parent_name)
        hypervisor = client.compute.find_hypervisor(self._get_node_name(ctx))

        # As we did not raise by now the connection parameters seem to be valid
        self.novaComputeWatch.connection_parameters = connection_info
        self.novaComputeWatch.namespace = ctx.namespace

        if service is None:
            return None

        if hypervisor is None:
            return None

        # We add this extra check in order to avoid clearing out a disable
        # reason set by another entity.
        is_enabled = (service.status == "enabled")
        ctx.logger.debug("is_enabled = %r, want enabled = %r",
                         is_enabled, enabled)
        if is_enabled != enabled:
            if enabled:
                ctx.logger.debug("enabling compute service %s %s",
                                 service.binary, service.host)
                service.enable(client.compute, service.host, service.binary)
            else:
                ctx.logger.debug("disabling compute service %s %s",
                                 service.binary, service.host)
                service.disable(client.compute, service.host, service.binary)

        if enabled:
            for aggregate_name in ctx.parent_spec.get("hostAggregates", []):
                aggregate = client.compute.find_aggregate(aggregate_name)
                if aggregate is None:
                    ctx.logger.warning(
                        f"The set aggregate {aggregate_name} was not found. "
                        "Ignored."
                    )
                    continue

                if hypervisor.name not in set(aggregate.hosts):
                    client.compute.add_host_to_aggregate(
                        aggregate=aggregate,
                        host=hypervisor.name,
                    )
                    ctx.logger.debug(f"Added hypervisor {hypervisor.name} "
                                     f"to aggregate {aggregate.name}")

        service = get_compute_service(client, ctx.parent_name)

        if service is None:
            return None
        return resource.ResourceStatus(
            up=service.state == "up",
            enabled=service.status == "enabled",
            disable_reason=service.disabled_reason or None,
        )

    def _needs_l2_agent(self) -> bool:
        return True

    def get_listeners(self) -> typing.List[sm.Listener]:
        return super().get_listeners() + [
            sm.ExternalListener(
                watcher=self.novaComputeWatch,
                listener=self._handle_nova_event,
            ),
        ]

    def _handle_nova_event(
            self,
            ctx: sm.Context,
            event: watcher.StatefulWatchEvent[
                openstack.compute.v2.service.Service],
            ) -> bool:
        if (event.type_ == watcher.EventType.ADDED or
                event.type_ == watcher.EventType.DELETED or
                event.old_object is None):
            return True
        if (datetime.strptime(
                    event.old_object.updated_at, "%Y-%m-%dT%H:%M:%S.%f") <=
                ctx.creation_timestamp and
                datetime.strptime(
                    event.object_.updated_at, "%Y-%m-%dT%H:%M:%S.%f") >
                ctx.creation_timestamp):
            return True
        return (
            event.old_object.state != event.object_.state or
            event.old_object.status != event.object_.status
        )

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> None:
        conn_info = await self.get_openstack_connection_info(ctx)
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(
            None,
            self.delete_nodes_from_hostaggreate,
            ctx,
            conn_info
        )
        return await super().delete(
            ctx=ctx,
            dependencies=dependencies
        )

    def delete_nodes_from_hostaggreate(self, ctx, conn_info):
        client = openstack.connect(**conn_info)

        hypervisor = client.compute.find_hypervisor(self._get_node_name(ctx))
        if hypervisor is not None:
            for aggregate_name in ctx.parent_spec.get("hostAggregates", []):
                aggregate = client.compute.find_aggregate(aggregate_name)
                if aggregate is None:
                    continue

                if hypervisor.name in set(aggregate.hosts):
                    client.compute.remove_host_from_aggregate(
                        aggregate=aggregate,
                        host=hypervisor.name,
                    )
                    ctx.logger.debug(f"Removed hypervisor {hypervisor.name} "
                                     f"from aggregate {aggregate.name}")


class NovaComputeWatcher(
        watcher.ExternalWatcher[openstack.compute.v2.service.Service]):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def to_kubernetes_reference(self, name: str) -> kclient.V1ObjectReference:
        if self.namespace:
            return kclient.V1ObjectReference(
                api_version="compute.yaook.cloud/v1",
                kind="NovaComputeNode",
                name=name,
                namespace=self.namespace,
            )
        raise ValueError("Trying to generate a object reference while we "
                         "do not know the namespace yet.")

    def _get_state(self) -> typing.Mapping[
            str, openstack.compute.v2.service.Service]:
        if self.connection_parameters:
            try:
                client = openstack.connect(**self.connection_parameters)
                agents = client.compute.services(binary="nova-compute")
                return {a.host: a for a in agents}
            except keystoneauth1.exceptions.http.Unauthorized:
                self.connection_parameters = None
        return {}

    async def get_state(self) -> typing.Mapping[
            str, openstack.compute.v2.service.Service]:
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(
            None,
            self._get_state,
        )

    def to_dict(self, obj: openstack.compute.v2.service.Service
                ) -> typing.Mapping[str, typing.Any]:
        return obj.to_dict()
