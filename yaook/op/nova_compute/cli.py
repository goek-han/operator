#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import argparse
import types
import typing

import environ

import kubernetes_asyncio.client

import yaook.statemachine as sm

from . import eviction


@environ.config(prefix="YAOOK_NOVA_COMPUTE_EVICT")
class EvictionConfig:
    node_name = environ.var()
    node_namespace = environ.var()
    poll_interval = environ.var(15, converter=int)
    reason = environ.var()
    max_parallel_migrations = environ.var(2, converter=int)
    release = environ.var()


async def do_evacuate(args: types.SimpleNamespace) -> int:
    try:
        kubernetes_asyncio.config.load_incluster_config()
    except Exception:
        await kubernetes_asyncio.config.load_kube_config()

    config = environ.to_config(EvictionConfig)
    async with kubernetes_asyncio.client.ApiClient() as api_client:
        evictor = eviction.Evictor(
            interface=sm.nova_compute_node_interface(api_client),
            namespace=config.node_namespace,
            node_name=config.node_name,
            # do not pass any connection info, it should be sourced from the
            # environment by the OpenStack SDK :)
            connection_info={},
            reason=config.reason,
            max_parallel_migrations=config.max_parallel_migrations,
            k8s_api_client=api_client,
            release=config.release
        )
        await evictor.run(poll_interval=config.poll_interval)

    return 0


async def command(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    subparser = subparsers.add_parser("evict")
    subparser.set_defaults(func=do_evacuate)

    args = parser.parse_args(argv[1:])

    return await args.func(args)
