##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

{% set service_account = dependencies['compute_service_account'].resource_name() %}
{% set public_keys_secret = dependencies['public_keys'].resource_name() %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['compute_service'].resource_name() }}
spec:
  serviceName: {{ dependencies['compute_service'].resource_name() }}
  podManagementPolicy: Parallel
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
    spec:
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NovaComputeNode resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                "network.yaook.cloud/provides-l2": "true"
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      hostPID: true
      hostIPC: true
      dnsPolicy: ClusterFirstWithHostNet
      serviceAccountName: {{ service_account }}
      initContainers:
      - name: keygen
        image: {{ versioned_dependencies['nova_compute_docker_image'] }}
        securityContext:
          runAsUser: 2500008
        imagePullPolicy: Always
        command: ["/keygen-runner.sh"]
        env:
        - name: NODE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: SSH_PORT
          value: "8022"
        - name: YAOOK_NOVA_COMPUTE_PUBLIC_KEYS
          value: {{ public_keys_secret }}
        - name: REQUESTS_CA_BUNDLE
          value: /etc/ssl/certs/ca-bundle.crt
        volumeMounts:
        - name: client-ssh-home
          mountPath: /mnt/client-ssh-home
        - name: hostkeys
          mountPath: /etc/ssh/hostkeys
        - name: ca-certs
          mountPath: /etc/ssl/certs
        resources: {{ crd_spec | resources('keygen') }}
      - name: chown-nova
        image: {{ versioned_dependencies['nova_compute_docker_image'] }}
        imagePullPolicy: Always
        command: ["sh", "-ec"]
        args:
          # change the permissions of all files in /var/lib/nova to nova:nova
          # except for /var/lib/nova/mnt and subfiles/-directories since these are nfs mounts
{% if target_release == "yoga" %}
          ["find /var/lib/nova -name 'mnt' -prune -o -exec chown nova:nova {} + && chown nova:nova /run/openvswitch/db.sock"]
{% else %}
          - find /var/lib/nova -name 'mnt' -prune -o -exec chown nova:nova {} +
{% endif %}
        env:
          - name: REQUESTS_CA_BUNDLE
            value: /etc/ssl/certs/ca-bundle.crt
        securityContext:
          runAsUser: 0
        volumeMounts:
          - name: varlibnova
            mountPath: /var/lib/nova
          - name: ca-certs
            mountPath: /etc/ssl/certs
          - name: run
            mountPath: /run
        resources: {{ crd_spec | resources('chown-nova') }}
      containers:
        - name: nova-compute
          image: {{ versioned_dependencies['nova_compute_docker_image'] }}
          imagePullPolicy: Always
          command: ["/nova-compute-runner.sh"]
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
{% if crd_spec.ids | default(False) and crd_spec.ids.cinderGid | default(False) %}
            - name: CINDER_GID
              value: {{ crd_spec.ids.cinderGid | string }}
{% endif %}
          securityContext:
            # see docs/implementation_details/nova/nova_privileged.rst
            privileged: true
            runAsUser: 0
          volumeMounts:
            - name: nova-config-volume
              mountPath: /etc/nova/nova.conf
              subPath: nova.conf
            - name: varlibnova
              mountPath: /var/lib/nova
              mountPropagation: Bidirectional
            - name: varliblibvirt
              mountPath: /var/lib/libvirt
              mountPropagation: Bidirectional
            - name: varrunlibvirt
              mountPath: /var/run/libvirt
            - name: dev
              mountPath: /dev
            - name: sysclass
              mountPath: /sys/class
            - name: run
              mountPath: /run
            - name: machine-id
              mountPath: /etc/machine-id
              readOnly: true
            - name: client-ssh-home
              mountPath: /home/nova/.ssh
            - name: public-key-material
              mountPath: /publickeys
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: vnc-certs
              mountPath: /etc/pki/libvirt-vnc
{% if target_release in ["victoria", "wallaby", "xena", "yoga"] %}
            - name: varlibswtpmlocalca
              mountPath: /var/lib/swtpm-localca
{% endif %}
          livenessProbe:
            # This checks if nova-compute has a open connection to the libvirt unix socket
            # Can be tested by killing the libvirt process mutliple times in a row
            exec:
              command:
              - sh
              - -c
              - ss -xapn | grep -F -- "$(ss -xap | grep nova-compute | awk '{print $6}')" | grep -q /run/libvirt
            failureThreshold: 3
            initialDelaySeconds: 30
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ss -xapn | grep -F -- "$(ss -xap | grep nova-compute | awk '{print $6}')" | grep -q /run/libvirt
            failureThreshold: 1
          resources: {{ crd_spec | resources('nova-compute') }}
        - name: nova-compute-ssh
          image: {{ versioned_dependencies['nova_compute_docker_image'] }}
          imagePullPolicy: Always
          command: ["/sshd-runner.sh"]
          securityContext:
            # see docs/implementation_details/nova/nova_privileged.rst
            privileged: true
            runAsUser: 0
          ports:
            - containerPort: 8022
              protocol: TCP
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
{% if crd_spec.ids | default(False) and crd_spec.ids.cinderGid | default(False) %}
            - name: CINDER_GID
              value: {{ crd_spec.ids.cinderGid | string }}
{% endif %}
          volumeMounts:
            - name: varlibnova
              mountPath: /var/lib/nova
            - name: varliblibvirt
              mountPath: /var/lib/libvirt
            - name: varrunlibvirt
              mountPath: /var/run/libvirt
            - name: server-ssh-home
              mountPath: /home/nova/.ssh
            - name: public-key-material
              mountPath: /publickeys
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: hostkeys
              mountPath: /etc/ssh/hostkeys
            - name: vnc-certs
              mountPath: /etc/pki/libvirt-vnc
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - ssh 127.0.0.1 -p 8022 -o stricthostkeychecking=no 2>&1 | grep Permission
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ssh 127.0.0.1 -p 8022 -o stricthostkeychecking=no 2>&1 | grep Permission
          resources: {{ crd_spec | resources('nova-compute-ssh') }}
        - name: "libvirtd"
          image: {{ versioned_dependencies['nova_compute_docker_image'] }}
          imagePullPolicy: Always
          command: ["/libvirtd-runner.sh"]
          securityContext:
            privileged: true
            runAsUser: 0
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
{% if crd_spec.ids | default(False) and crd_spec.ids.cinderGid | default(False) %}
            - name: CINDER_GID
              value: {{ crd_spec.ids.cinderGid | string }}
{% endif %}
          volumeMounts:
            - name: run
              mountPath: /run
            - name: dev
              mountPath: /dev
            - name: sysbus
              mountPath: /sys/bus
            - name: varrunlibvirt
              mountPath: /run/libvirt
            - name: varrunlibvirt
              mountPath: /var/run/libvirt
            - name: varlibnova
              mountPath: /var/lib/nova
              mountPropagation: Bidirectional
            - name: varliblibvirt
              mountPath: /var/lib/libvirt
              mountPropagation: Bidirectional
            - name: etc-libvirt-qemu
              mountPath: /etc/libvirt/qemu
            - name: cgroup
              mountPath: /sys/fs/cgroup
            - name: client-ssh-home
              mountPath: /root/.ssh
            - name: public-key-material
              mountPath: /publickeys
            - name: ceph-configs
              mountPath: /etc/ceph
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: vnc-certs
              mountPath: /etc/pki/libvirt-vnc
{% if target_release in ["victoria", "wallaby", "xena", "yoga"] %}
            - name: varlibswtpmlocalca
              mountPath: /var/lib/swtpm-localca
{% endif %}
          lifecycle:
            preStop:
              exec:
                command:
                  - bash
                  - -c
                  - |-
                    kill $(cat /var/run/libvirtd.pid)
          livenessProbe:
            exec:
              command:
              - virsh
              - list
          readinessProbe:
            exec:
              command:
              - virsh
              - list
          resources: {{ crd_spec | resources('libvirtd') }}
      volumes:
        - name: public-key-material
          secret:
            secretName: {{ crd_spec.publicKeysSecretRef.name }}
        - name: client-ssh-home
          emptyDir: {}
        - name: server-ssh-home
          emptyDir: {}
        - name: hostkeys
          emptyDir: {}
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
        - name: vnc-certs
          secret:
            secretName: {{ dependencies['ready_vnc_backend_certificate_secret'].resource_name() }}
            items:
              - key: ca.crt
                path: ca-cert.pem
              - key: tls.key
                path: server-key.pem
              - key: tls.crt
                path: server-cert.pem
        - name: varlibnova
          hostPath:
            path: /var/lib/nova
        - name: varliblibvirt
          hostPath:
            path: /var/lib/libvirt
        - name: varrunlibvirt
          hostPath:
            path: /var/run/libvirt
        - name: machine-id
          hostPath:
            path: /etc/machine-id
{% if target_release in ["victoria", "wallaby", "xena", "yoga"] %}
        - name: varlibswtpmlocalca
          hostPath:
            path: /var/lib/swtpm-localca
{% endif %}
        - name: run
          hostPath:
            path: /run
        - name: dev
          hostPath:
            path: /dev
        - name: sysclass
          hostPath:
            path: /sys/class
        - name: sysbus
          hostPath:
            path: /sys/bus
        - name: etc-libvirt-qemu
          hostPath:
            path: /etc/libvirt/qemu
        - name: cgroup
          hostPath:
            path: /sys/fs/cgroup
        - name: nova-config-volume
          secret:
            secretName: {{ dependencies['compute_config'].resource_name() }}
            items:
            - key: novacompute.conf
              path: nova.conf
        - name: ceph-configs
          projected:
{% if crd_spec.cephBackend.enabled %}
            sources:
            - secret:
                name: {{ crd_spec.cephBackend.keyringSecretName }}
                items:
                - key: {{ crd_spec.cephBackend.user }}
                  path: keyfile
            - secret:
                name: {{ dependencies['ceph_config'].resource_name() }}
                items:
                - key: ceph.conf
                  path: ceph.conf
                - key: idmap
                  path: idmap
{% else %}
            sources: []
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
