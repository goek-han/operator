##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: {{ "compute-evict-%s-" | format(vars.node_name) }}
spec:
  parallelism: 1
  activeDeadlineSeconds: 86400
  template:
    metadata:
      labels: {{ labels }}
    spec:
      restartPolicy: OnFailure
      serviceAccountName: {{ dependencies['job_service_account'].resource_name() }}
      containers:
      - name: evict
        image: {{ params.job_image }}
        imagePullPolicy: Always
        securityContext:
          runAsUser: 2020
          runAsGroup: 2020
        args:
        - nova_compute
        - command
        - evict
        envFrom:
        - configMapRef:
            name: {{ params.endpoint_config }}
        - secretRef:
            name: {{ params.credentials_secret }}
        env:
        - name: YAOOK_NOVA_COMPUTE_EVICT_RELEASE
          value: {{ crd_spec.targetRelease }}
        - name: YAOOK_NOVA_COMPUTE_EVICT_POLL_MIGRATION_SPEED_LOCAL_DISK
          value: {{ crd_spec.evictPollMigrationSpeedLocalDisk | string }}
        - name: YAOOK_NOVA_COMPUTE_EVICT_NODE_NAME
          value: {{ vars.node_name }}
        - name: YAOOK_NOVA_COMPUTE_EVICT_NODE_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_OP_VERBOSITY
          value: "3"
        - name: YAOOK_OP_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_NOVA_COMPUTE_EVICT_REASON
          value: {{ vars.reason }}
        - name: REQUESTS_CA_BUNDLE
          value: /etc/ssl/certs/ca-bundle.crt
# if clause needed for the introduction of region.name field for novacomputenodes
{% if crd_spec.region is defined %}
        - name: OS_REGION_NAME
          value: {{ crd_spec.region.name }}
{% endif %}
        volumeMounts:
          - name: ca-certs
            mountPath: /etc/ssl/certs
        resources: {{ crd_spec | resources('compute-evict-job') }}
      volumes:
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}