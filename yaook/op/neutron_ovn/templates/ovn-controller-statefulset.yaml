##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = {} %}
{% set _ = pod_labels.update(labels) %}
# nova-compute pod wants to run only on hosts where a pod is providing l2
# so we need to set this label.
{% set _ = pod_labels.update({"network.yaook.cloud/provides-l2": "true"}) %}

{% set southbound_servers = [] %}
{% for server in crd_spec.southboundServers %}
{%   if server.startswith('ssl:') %}
{%     set _ = southbound_servers.append(server) %}
{%   else %}
{%     set _ = southbound_servers.append("ssl:" + server) %}
{%   endif %}
{% endfor %}

# Hack incoming: If we have bridge mappings set we can safely assume that the node is a network node
{% set network_node = {'value': False} %}
{% if crd_spec.bridgeConfig | default(False) and crd_spec.bridgeConfig|length > 0 %}
{%   set _ = network_node.update({'value': True }) %}
{% endif %}

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['ovn_service'].resource_name() }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  serviceName: {{ dependencies['ovn_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronOVNAgent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                state.yaook.cloud/component: ovs_vswitchd
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      initContainers:
        - name: ovn-controller-setup
          image:  {{ versioned_dependencies['ovn_controller_docker_image'] }}
          imagePullPolicy: Always
          command: ["/ovn_controller_setup.sh"]
          securityContext:
            capabilities:
              add:
              - NET_ADMIN
          volumeMounts:
            - name: run
              mountPath: /run
            - name: tls-secret
              mountPath: /etc/ssl/private
            - name: neutron-bridge-mappings-config-volume
              mountPath: /etc/neutron/bridge_mappings
              subPath: bridge_mappings
          env:
            - name: NETWORK_NODE
              value: '{{ network_node.value }}'
            - name: SOUTHBOUND_SERVERS
              value: {{ southbound_servers | join(',') }}
            - name: OVERLAY_IP_ADDRESS
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
          resources: {{ crd_spec | resources('ovn-controller-setup') }}
      containers:
        - name: "ovn-controller"
          image:  {{ versioned_dependencies['ovn_controller_docker_image'] }}
          imagePullPolicy: Always
          command:
            - bash
            - -ec
            - |
              set -ex
              ovn-controller --pidfile unix:/run/openvswitch/db.sock \
              --private-key /etc/ssl/private/tls.key \
              --certificate /etc/ssl/private/tls.crt \
              --ca-cert /etc/ssl/private/ca.crt \
              --log-file=/dev/stdout
          volumeMounts:
            - name: run
              mountPath: /run
            - name: tls-secret
              mountPath: /etc/ssl/private
            - name: neutron-bridge-mappings-config-volume
              mountPath: /etc/neutron/bridge_mappings
              subPath: bridge_mappings
          lifecycle:
            preStop:
              exec:
                command:
                - sh
                - -c
                - ovn-appctl exit && while pgrep ovn-controller; do sleep 1; done
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - 'ovs-vsctl show && [ "$(ovs-appctl -t /var/run/ovn/ovn-controller.1.ctl connection-status)" = "connected" ]'
          resources: {{ crd_spec | resources('ovn-controller') }}
{% if crd_spec.deployedOnComputeNode %}
        - name: "neutron-ovn-metadata-agent"
          image: {{ versioned_dependencies['neutron_agent_docker_image'] }}
          imagePullPolicy: Always
          command:
            - bash
            - -ec
            - |
              set -ex
              neutron-ovn-metadata-agent --config-file /etc/neutron/neutron.conf
          livenessProbe:
            exec:
              command:
              - bash
              - -c
              - 'curl -H "X-Ovn-Network-Id: I-am-the-liveness-probe" --unix-socket /run/neutron/metadata_proxy_socket http://'
            initialDelaySeconds: 30
          readinessProbe:
            exec:
              command:
              - bash
              - -c
              - 'curl -H "X-Ovn-Network-Id: I-am-the-readiness-probe" --unix-socket /run/neutron/metadata_proxy_socket http://'
          resources: {{ crd_spec | resources('neutron-ovn-metadata-agent') }}
          securityContext:
            privileged: true
          volumeMounts:
            - name: run
              mountPath: /run
            - name: tls-secret
              mountPath: /etc/ssl/private
            - mountPath: /run/netns
              mountPropagation: Bidirectional
              name: run-netns
            - mountPath: /run/xtables.lock
              name: iptables-lockfile
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
            - name: neutron-config-vol
              mountPath: /etc/neutron
{% endif %}
      terminationGracePeriodSeconds: 240
      volumes:
        - name: run
          hostPath:
            path: /run
        - hostPath:
            path: /run/netns
          name: run-netns
        - hostPath:
            path: /run/xtables.lock
          name: iptables-lockfile
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
        - name: neutron-bridge-mappings-config-volume
          secret:
            secretName: {{ dependencies['ovn_config'].resource_name() }}
            items:
            - key: bridge_mappings
              path: bridge_mappings
{% if crd_spec.deployedOnComputeNode %}
        - name: neutron-config-vol
          secret:
            secretName: {{ dependencies['ovn_metadata_config'].resource_name() }}
            items:
            - key: neutron_ovn_metadata_agent.conf
              path: neutron.conf
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
