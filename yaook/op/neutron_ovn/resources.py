#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import typing

import openstack

from yaook.statemachine import (
    context,
    customresource,
    watcher,
)
import yaook.statemachine.resources.openstack as resource
from yaook.statemachine.resources.base import DependencyMap


class OVNStateResource(resource.L2ProvidingAgentStateResource):
    def __init__(
            self,
            *,
            scheduling_keys: typing.Collection[str],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.neutronAgentWatch: watcher.ExternalWatcher[
                openstack.network.v2.agent.Agent] = resource.\
            NeutronAgentWatcher("NeutronOVNAgent", "ovn-controller")

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[typing.Mapping](
                "", "v1", "nodes",
                self._handle_event,
                broadcast=True,
            ),
        ] + [
            context.ExternalListener(
                watcher=self.neutronAgentWatch,
                listener=self._handle_agent_event,
            ),
        ]

    def _get_agent(
            self,
            network_client: openstack.network.v2._proxy.Proxy,
            host: str,
            ) -> typing.Optional[openstack.network.v2.agent.Agent]:
        try:
            agent = resource.get_network_agent(network_client, host,
                                               "ovn-controller")
        except LookupError:
            return None
        return agent

    def _fetch_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            ) -> typing.Optional[resource.ResourceStatus]:
        client = openstack.connect(**connection_info)
        agent = self._check_agent(ctx, client)
        if agent is None:
            return None
        is_state_up = agent.is_admin_state_up
        ctx.logger.debug("is_state_up = %r", is_state_up)

        # As we did not raise by now the connection parameters seem to be valid
        self.neutronAgentWatch.connection_parameters = connection_info
        self.neutronAgentWatch.namespace = ctx.namespace

        return resource.ResourceStatus(
            up=agent.is_alive,
            enabled=agent.is_admin_state_up,
            disable_reason=None,
        )

    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        conn_info = await self.get_openstack_connection_info(ctx)
        loop = asyncio.get_event_loop()

        status = await loop.run_in_executor(
            None,
            self._fetch_status,
            ctx,
            conn_info,
        )
        ctx.logger.debug("read status from OpenStack: %s",
                         status)

        next_state = resource.ResourceStatusState.ENABLED
        if status is None or not status.up:
            next_state = resource.ResourceStatusState.CREATING
        else:
            await self._maintenance_required_label(
                ctx,
                "add",
                False,
                )

        additional_patch = {
            "state": next_state.value,
        }

        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            conditions=[
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    reason="Unknown",
                    message="",
                    status="ovn-controller is always enabled"
                ),
            ],
            additional_patch=additional_patch,
        )
