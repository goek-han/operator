##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "ovn-northd"
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  replicas: {{ crd_spec.setup.ovn.northd.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
      securityContext:
        fsGroup: 2500016
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: northd
          image: {{ versioned_dependencies['ovn_image'] }}
          imagePullPolicy: Always
          command:
            - "bash"
            - -ec
            - |
              ovn-northd \
              --ovnnb-db={{ ovsdb_nb }} \
              --ovnsb-db={{ ovsdb_sb }} \
              --private-key=/etc/ssl/private/tls.key \
              --certificate=/etc/ssl/private/tls.crt \
              --ca-cert=/etc/ssl/private/ca.crt \
              --pidfile=/run/ovn/ovn-northd.pid \
              --log-file=/dev/stdout
          env:
            - name: MY_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: MY_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
          readinessProbe:
            exec:
              command:
                - "bash"
                - -ec
                - |
                  /northd_livenessprobe.sh {{ ovsdb_nb }} {{ ovsdb_sb }}
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          startupProbe:
            exec:
              command:
                - "bash"
                - -ec
                - |
                  /northd_livenessprobe.sh {{ ovsdb_nb }} {{ ovsdb_sb }}
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 60
          volumeMounts:
            - name: run-ovn
              mountPath: /run/ovn
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: tls-secret
              mountPath: /etc/ssl/private
          resources: {{ crd_spec | resources('setup.ovn.northd.northd') }}
      volumes:
        - name: run-ovn
          emptyDir: {}
        # I actually think, we don't need this, but it makes the pipeline
        # happy and don't hurt, I guess
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_northd_certificate_secret'].resource_name() }}
