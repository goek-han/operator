#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

import yaook.common.config
import yaook.op.common
import yaook.op.tasks
import yaook.statemachine as sm
from yaook.statemachine import resources


def _use_ceph(ctx: sm.Context) -> bool:
    return "ceph" in ctx.parent_spec.get("backends", {})


def _use_s3(ctx: sm.Context) -> bool:
    return "s3" in ctx.parent_spec.get("backends", {})


class CephLayer(sm.CueLayer):
    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> sm.cue.CueInput:
        if not _use_ceph(ctx):
            return {}

        cephconfig = ctx.parent_spec["backends"]["ceph"]
        return {
            "gnocchi": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "storage": yaook.common.config.CueConfigReference(
                        "gnocchi.gnocchi_backend_ceph"
                    ),
                },
            ]),
            "ceph": yaook.common.config.CEPH_CONFIG.declare([
                cephconfig.get("cephConfig", {}),
                {
                    f"client.{cephconfig['keyringUsername']}": {
                        "keyfile": "/etc/ceph/keyfile"
                    },
                },
            ]),
        }


class S3ConfigLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        if not _use_s3(ctx):
            return {}

        if _use_ceph(ctx):
            raise sm.ConfigurationInvalid(
                "Choose either 'ceph' or 's3' as a storage backend. "
                "Using both is not supported.")

        secret_name = (ctx.parent_spec["backends"]["s3"]
                                      ["credentialRef"]["name"])
        s3_access_key = await sm.extract_password(
            ctx,
            secret_name,
            key="access"
        )
        s3_access_secret = await sm.extract_password(
            ctx,
            secret_name,
            key="secret"
        )

        return {
            "gnocchi": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "storage": yaook.common.config.CueConfigReference(
                        "gnocchi.gnocchi_backend_s3"
                    ),
                },
                {
                    "storage": {
                        "s3_endpoint_url":
                            ctx.parent_spec["backends"]["s3"]["endpoint"],
                        "s3_access_key_id": s3_access_key,
                        "s3_secret_access_key": s3_access_secret,
                        "s3_bucket_prefix":
                            ctx.parent_spec["backends"]["s3"]["bucketPrefix"],
                    }
                },
            ]),
        }


class Deployment(sm.TemplatedDeployment):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        param["use_ceph"] = _use_ceph(ctx)
        return param


class Upgrade(sm.TemplatedJob):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        param["use_ceph"] = _use_ceph(ctx)
        return param


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_GNOCCHI.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]

DATABASE_NAME = "gnocchi"
API_SVC_USERNAME = "api"


class Gnocchi(sm.CustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "gnocchideployments"
    KIND = "GnocchiDeployment"
    RELEASES = [
        "queens",
        "train",
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    gnocchi_docker_image = yaook.op.common.image_dependencies(
        "gnocchi/gnocchi-{release}",
        RELEASES,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            **{release: "10.2" for release in ["queens", "rocky", "stein"]},
            **{
                release: "10.6"
                for release in [
                    "train",
                    "ussuri",
                    "victoria",
                    "wallaby",
                    "xena",
                    "yoga",
                ]
            },
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "db_name": DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )
    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("gnocchi-db-api-user-", True),
        copy_on_write=True,
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("gnocchi-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="gnocchi",
    )
    keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            keystone_user
        )
    keystone_endpoint = sm.Optional(
        condition=yaook.op.common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="gnocchi-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("gnocchi-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="gnocchi-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("gnocchi-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
    )

    config = sm.CueSecret(
        metadata=("gnocchi-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="gnocchi",
                accessor="gnocchiConfig",
            ),
            sm.KeystoneAuthLayer(
                target="gnocchi",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.DatabaseConnectionLayer(
                target="gnocchi",
                service=db_service,
                database_name=DATABASE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="indexer",
            ),
            CephLayer(),
            S3ConfigLayer(),
        ],
    )

    upgrade = Upgrade(
        template="gnocchi-job-upgrade.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs],
        versioned_dependencies=[gnocchi_docker_image],
    )

    gnocchi_policy = sm.PolicyConfigMap(
        metadata=("gnocchi-policy-", True),
        policy_spec_key="policy",
        copy_on_write=True,
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
        ctx.parent_spec["api"]["ingress"].get("externalCertificateSecretRef",
                                              {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    api_deployment = Deployment(
        template="gnocchi-deployment-api.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.GNOCCHI_API.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config,
            upgrade,
            ca_certs,
            ready_certificate_secret,
            external_certificate_secret,
            gnocchi_policy,
        ],
        versioned_dependencies=[
            gnocchi_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )
    api_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("gnocchi-api-pdb-", True),
         replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="gnocchi-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    internal_ssl_terminator_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    external_ssl_terminator_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
            ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    api_ingress = sm.TemplatedIngress(
        template="gnocchi-api-ingress.yaml",
        add_dependencies=[api_service],
    )
    metricd_deployment = Deployment(
        template="gnocchi-deployment-metricd.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.GNOCCHI_METRICD.value,
        ],
        add_dependencies=[config, upgrade, ca_certs],
        versioned_dependencies=[gnocchi_docker_image],
    )
    metricd_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("gnocchi-metricd-pdb-", True),
         replicated=metricd_deployment,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(Gnocchi)
