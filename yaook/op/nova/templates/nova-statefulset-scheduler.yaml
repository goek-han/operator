##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-scheduler" | format(labels["state.yaook.cloud/parent-name"]) }}
spec:
  podManagementPolicy: "Parallel"
  replicas:  {{ crd_spec.scheduler.replicas }}
  serviceName: "nova-scheduler"
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "nova-scheduler"
          image: {{ versioned_dependencies['nova_docker_image'] }}
          command: ["nova-scheduler"]
          imagePullPolicy: Always
          volumeMounts:
            - name: nova-config-volume
              mountPath: /etc/nova/nova.conf
              subPath: nova.conf
            - name: ca-certs
              mountPath: /etc/ssl/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          resources: {{ crd_spec | resources('scheduler.nova-scheduler') }}
      volumes:
        - name: nova-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: nova.conf
                path: nova.conf
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
