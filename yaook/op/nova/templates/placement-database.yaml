##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: infra.yaook.cloud/v1
kind: MySQLService
metadata:
  generateName: {{ "placement-%s-" | format(params.database_name) }}
spec:
  database: {{ params.database_name }}
  targetRelease: {{ versioned_dependencies['mariadb_version'] }}
  replicas: {{ crd_spec.database[params.database_name].replicas }}
  proxy:
    replicas: {{ crd_spec.database[params.database_name].proxy.replicas }}
    # This needs to be changed in lockstep with the connection_recycle_time in
    # the nova config. The haproxy timeout should always be ~10% larger
    # than the connection_recycle_time.
    timeoutClient: 300
  mysqlConfig: {{ crd_spec.database[params.database_name].mysqlConfig | default({}) }}
  frontendIssuerRef:
    name: {{ crd_spec.issuerRef.name }}
  backendCAIssuerRef:
    name: selfsigned-issuer
  storageSize: {{ crd_spec.database[params.database_name].storageSize }}
{% if crd_spec.database[params.database_name].storageClassName | default(False) %}
  storageClassName: {{ crd_spec.database[params.database_name].storageClassName }}
{% endif %}
  backup: {{ crd_spec.database[params.database_name].backup }}
{% if crd_spec.imagePullSecrets | default(False) %}
  imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
{% if crd_spec.caCertificates | default(False) %}
  caCertificates: {{ crd_spec.caCertificates }}
{% endif %}
{% if crd_spec.serviceMonitor | default(False) and crd_spec.serviceMonitor.additionalLabels | default(False) %}
  serviceMonitor:
    additionalLabels:
      {{ crd_spec.serviceMonitor.additionalLabels }}
{% endif %}
