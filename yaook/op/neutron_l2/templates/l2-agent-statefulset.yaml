##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = {} %}
{% set _ = pod_labels.update(labels) %}
# TODO: we need to add somehow somewhere the info about the labels
{% set _ = pod_labels.update({"network.yaook.cloud/provides-l2": "true"}) %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['l2_service'].resource_name() }}
spec:
  serviceName: {{ dependencies['l2_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronL2Agent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                state.yaook.cloud/component: l2_ovs_vswitchd
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      hostPID: true
      dnsPolicy: ClusterFirstWithHostNet
      initContainers:
        - name: neutron-ovs-bridge-setup
          image:  {{ versioned_dependencies['neutron_agent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/map-bridges.sh"]
          securityContext:
            capabilities:
              add:
              - NET_ADMIN
          volumeMounts:
            - name: neutron-bridge-mappings-config-volume
              mountPath: /etc/neutron/bridge_mappings
              subPath: bridge_mappings
            - name: run
              mountPath: /run
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          resources: {{ crd_spec | resources('neutron-ovs-bridge-setup') }}
      containers:
        - name: "neutron-openvswitch-agent"
          image:  {{ versioned_dependencies['neutron_agent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/neutron-openvswitch-agent-runner.sh"]
          securityContext:
            privileged: true
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
{% if crd_spec['overlayNetworkConfig']['ovs_local_ip_subnet'] %}
            - name: NEUTRON_OVS_LOCAL_IP_SUBNET
              value: {{ crd_spec['overlayNetworkConfig']['ovs_local_ip_subnet'] }}
{% endif %}
          volumeMounts:
            - name: neutron-ovs-config-volume
              mountPath: /etc/neutron/neutron.conf
              subPath: neutron.conf
            - name: run
              mountPath: /run
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          readinessProbe:
            # Can be tested by rapidly restarting the neutron-server
            exec:
              command:
              - grep
              - "True"
              - /tmp/last_iteration
            periodSeconds: 5
            failureThreshold: 1
          startupProbe:
            # Can be tested by having rabbitmq be inaccessible
            exec:
              command:
              - cat
              - /tmp/initial_iteration
            periodSeconds: 5
            failureThreshold: 540 # 45 min
          resources: {{ crd_spec | resources('neutron-openvswitch-agent') }}
      volumes:
        - name: neutron-bridge-mappings-config-volume
          secret:
            secretName: {{ dependencies['l2_config'].resource_name() }}
            items:
            - key: bridge_mappings
              path: bridge_mappings
        - name: neutron-ovs-config-volume
          secret:
            secretName: {{ dependencies['l2_config'].resource_name() }}
            items:
            - key: neutron_openvswitch_agent.conf
              path: neutron.conf
        - name: run
          hostPath:
            path: /run
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
