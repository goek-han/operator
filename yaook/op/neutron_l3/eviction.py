#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations
import asyncio

import dataclasses
import enum
import json
import typing

import openstack

import yaook.statemachine.commoneviction as eviction
import yaook.statemachine.resources.openstack as os_resource


class EvictResult(enum.Enum):
    MIGRATED = 0
    PENDING = 1


@dataclasses.dataclass(frozen=True)
class EvictionStatus:
    migrated_router: int
    pending_router: int
    unhandleable_router: int
    unhandleable_agent: bool

    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        return {
            "migrated": self.migrated_router,
            "pending": self.pending_router,
            "unhandleable": self.unhandleable_router,
            "unhandleable_agent": self.unhandleable_agent,
        }


class HashableAgent:
    def __init__(self, agent: openstack.network.v2.agent.Agent) -> None:
        self.agent = agent

    def __hash__(self):
        return hash(self.agent.id)

    def __eq__(self, other):
        return self.agent.id == other.agent.id


class NeutronAPIError(Exception):
    def __init__(self, error_type: str, message: str) -> None:
        self.error_type = error_type
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"{self.error_type}: {self.message}"


def raise_neutron_error_if_any(
    error_data: typing.Optional[typing.Dict],
) -> None:
    if error_data and isinstance(error_data, dict):
        neutron_error = error_data.get("NeutronError")
        if neutron_error:
            error_type = neutron_error.get("type")
            error_msg = neutron_error.get("message")
            raise NeutronAPIError(error_type=error_type, message=error_msg)

        raise NeutronAPIError(
            error_type="Unknown", message=json.dumps(error_data)
        )


class Evictor(eviction.Evictor[EvictionStatus]):
    def __init__(
        self,
        max_parallel_migrations: int,
        respect_azs: bool,
        verify_seconds: int,
        handle_non_migrated_routers_as_unhandleable: bool,
        **kwargs: typing.Any,
    ):
        super().__init__(**kwargs)
        self.max_parallel_migrations = max_parallel_migrations
        self.respect_azs = respect_azs
        self.verify_seconds = verify_seconds
        self.handle_non_migrated_routers_as_unhandleable = (
            handle_non_migrated_routers_as_unhandleable
        )
        self._recheck = False

    def _connect(self) -> openstack.connection.Connection:
        client = openstack.connect(**self._connection_info)
        return client

    def _collect_agents_and_routers(
        self, client: openstack.connection.Connection
    ) -> typing.Dict[HashableAgent, typing.Set[str]]:
        agents: typing.Dict[HashableAgent, typing.Set[str]] = dict()
        for agent in client.network.agents(binary="neutron-l3-agent"):
            # We don't use a defaultdict here, as we also want empty L3 agents
            # in the dictionary which wouldn't be created if the agent doesnt
            # have routers
            h_agent = HashableAgent(agent)
            agents[h_agent] = set()
            for router in client.network.agent_hosted_routers(
                agent, is_ha=True
            ):
                agents[h_agent].add(router.id)

        return agents

    def _select_agent(
        self,
        router: openstack.network.v2.router.Router,
        agents: typing.Dict[HashableAgent, typing.Set[str]],
        respect_azs: bool,
    ) -> typing.List[HashableAgent]:
        """
        Returns a sorted list of agents, best agent first.
        """
        # Get all Agents hosting this router
        agents_hosting_this_resource = set(
            [
                h_agent
                for h_agent, routers_on_this_agent in agents.items()
                if router.id in routers_on_this_agent
            ]
        )

        # Get all availability zones where there router is already schedulded
        # and exclude them
        excluded_availability_zones = (
            set(
                [
                    h_agent.agent.availability_zone
                    for h_agent in agents_hosting_this_resource
                ]
            )
            if respect_azs
            else None
        )

        possible_agents = []
        for h_agent, routers_on_this_agent in agents.items():
            if not (
                h_agent.agent.is_alive and h_agent.agent.is_admin_state_up
            ):
                continue

            if router.id in routers_on_this_agent:
                # Already on this agent
                continue

            if (
                excluded_availability_zones
                and h_agent.agent.availability_zone
                in excluded_availability_zones
            ):
                # Availability Zone is excluded
                continue

            possible_agents.append((h_agent, len(routers_on_this_agent)))

        # Sorted in ascending order based on the number of resources per agent
        return [
            agent
            for agent, _ in sorted(possible_agents, key=lambda tup: tup[1])
        ]

    async def os_remove_router_from_agent(
        self,
        client: openstack.connection.Connection,
        router: openstack.network.v2.router.Router,
        agent: openstack.network.v2.agent.Agent,
    ) -> typing.Optional[typing.Dict[str, typing.Any]]:
        return await asyncio.get_event_loop().run_in_executor(
            None,
            client.network.remove_router_from_agent,
            agent,
            router,
        )

    async def os_add_router_to_agent(
        self,
        client: openstack.connection.Connection,
        router: openstack.network.v2.router.Router,
        agent: openstack.network.v2.agent.Agent,
    ) -> typing.Optional[typing.Dict[str, typing.Any]]:
        return await asyncio.get_event_loop().run_in_executor(
            None,
            client.network.add_router_to_agent,
            agent,
            router,
        )

    async def _router_is_not_active_on_agent(
        self,
        router: openstack.network.v2.router.Router,
        agent: openstack.network.v2.agent.Agent,
    ) -> EvictResult:
        client = self._connect()

        if self._is_agent_in_list(
            agent, self._get_active_agents_for_router(client, router)
        ):
            self._logger.debug(
                f"Router {router.id} is still active on the agent. Wait until"
                " it is in standby..."
            )
            return EvictResult.PENDING

        return EvictResult.MIGRATED

    async def _move_router(
        self,
        router: openstack.network.v2.router.Router,
        src_agent: openstack.network.v2.agent.Agent,
        dst_agent: openstack.network.v2.agent.Agent,
        verify_seconds: int,
    ) -> EvictResult:
        client = self._connect()

        # We get all active agents, check if the agent we want to remove is
        # Active, if it is and verify_seconds is set, we then check if the
        # router has become active on another agent.
        verify_active = (
            self._is_agent_in_list(
                src_agent, self._get_active_agents_for_router(client, router)
            )
            if verify_seconds > 0
            else False
        )

        raise_neutron_error_if_any(
            await self.os_add_router_to_agent(client, router, dst_agent)
        )
        raise_neutron_error_if_any(
            await self.os_remove_router_from_agent(client, router, src_agent)
        )

        # Verify that the router got active anywhere.
        if verify_active:
            for _ in range(verify_seconds):
                await asyncio.sleep(1.0)
                active_agents = self._get_active_agents_for_router(
                    client, router
                )
                if active_agents:
                    self._logger.debug(
                        f"Router {router.id} is active on"
                        f" {', '.join([agent.id for agent in active_agents])}"
                    )
                    return EvictResult.MIGRATED

            self._logger.error(
                f"The router {router.id} is not active on any agent after"
                f" {verify_seconds} seconds."
            )

        return EvictResult.MIGRATED

    def _is_agent_in_list(
        self,
        agent: openstack.network.v2.agent.Agent,
        agents: typing.List[openstack.network.v2.agent.Agent],
    ) -> bool:
        return len(list(filter(lambda x: x.id == agent.id, agents))) > 0

    def _get_active_agents_for_router(
        self,
        client: openstack.connection.Connection,
        router: openstack.network.v2.router.Router,
    ) -> typing.List[openstack.network.v2.agent.Agent]:
        return list(
            filter(
                lambda agent: agent.ha_state == "active",
                client.network.routers_hosting_l3_agents(router),
            )
        )

    def _get_agent(
        self,
        all_agents: typing.List[HashableAgent],
        hostname: str,
    ) -> typing.Optional[HashableAgent]:
        agents = list(
            filter(lambda h_agent: h_agent.agent.host == hostname, all_agents)
        )
        if not agents:
            return None
        return agents[0]

    async def iterate(self) -> EvictionStatus:
        client = self._connect()

        migrated_router = 0
        routers_total = 0
        unhandleable_router = 0

        all_agents = self._collect_agents_and_routers(client)

        h_current_agent = self._get_agent(
            all_agents=list(all_agents.keys()),
            hostname=self._node_name,
        )
        if h_current_agent is None:
            self._logger.error(
                "l3 agent %s has vanished ... we can't do anything, as we "
                "can't get the list of networks on this agent.",
                self._node_name,
            )
            return EvictionStatus(
                migrated_router=0,
                pending_router=0,
                unhandleable_router=0,
                unhandleable_agent=True,
            )

        tasks = []
        # select a targetagent and create asyncio tasks
        routers = all_agents.pop(h_current_agent)

        routers_total = len(routers)

        routers_without_target_agent = list()

        for router_id in routers:
            router = client.network.find_router(router_id)
            if router is None:
                migrated_router += 1
                self._logger.warning(f"The router {router_id} has vanished.")
                continue

            possible_agents = self._select_agent(
                router=router, agents=all_agents, respect_azs=self.respect_azs
            )
            if not possible_agents:
                if self.handle_non_migrated_routers_as_unhandleable:
                    self._logger.error(
                        f"There are no agents to move the router {router_id}."
                        " Manual intervention required."
                    )
                routers_without_target_agent.append(router)
                continue

            new_agent = possible_agents[0]
            all_agents[new_agent].add(router_id)

            tasks.append(
                asyncio.create_task(
                    self._move_router(
                        router=router,
                        src_agent=h_current_agent.agent,
                        dst_agent=new_agent.agent,
                        verify_seconds=self.verify_seconds,
                    ),
                )
            )

            if len(tasks) >= self.max_parallel_migrations:
                break

        if (
            not self.handle_non_migrated_routers_as_unhandleable
            and len(tasks) < self.max_parallel_migrations
        ):
            # We still have space left in our tasks list

            # This is a corner case which would block us forever.
            # If we would have more routers in routers_without_target_agent
            # than max_parallel_migrations, its impossible for us to check all
            # routers as we will always check the same and even if those are
            # active somewhere else we can't check the other ones.
            # Therefore we go through all routers when all remaining routers
            # are in routers_without_target_agent. If the above loop
            # didnt add a single task we only have routers in
            # routers_without_target_agent
            check_all = len(tasks) == 0

            for router in routers_without_target_agent:
                tasks.append(
                    asyncio.create_task(
                        self._router_is_not_active_on_agent(
                            router=router,
                            agent=h_current_agent.agent,
                        ),
                    )
                )
                if (
                    not check_all
                    and len(tasks) >= self.max_parallel_migrations
                ):
                    break

        elif self.handle_non_migrated_routers_as_unhandleable:
            unhandleable_router += len(routers_without_target_agent)

        results = await asyncio.gather(*tasks, return_exceptions=True)
        for res in results:
            if isinstance(res, EvictResult):
                if res == EvictResult.MIGRATED:
                    migrated_router += 1
                continue
            if isinstance(res, NeutronAPIError):
                self._logger.error(f"{res}")
                unhandleable_router += 1
            elif isinstance(res, Exception):
                raise res

        if (routers_total <= migrated_router) or (
            not self.handle_non_migrated_routers_as_unhandleable
            and routers_without_target_agent
        ):
            # Disable the agent, and do one more iteration loop
            is_disabled = not h_current_agent.agent.is_admin_state_up
            is_down = not h_current_agent.agent.is_alive
            self._logger.info(
                "l3 agent %s: is_down=%s, is_disabled=%s",
                h_current_agent.agent.id,
                is_down,
                is_disabled,
            )

            if not is_disabled:
                os_resource.disable_network_agent(
                    client.network,
                    h_current_agent.agent,
                )
                self._logger.info(
                    "Disabled l3 agent %s",
                    h_current_agent.agent.id,
                )
                self._recheck = True
            else:
                self._recheck = False
        else:
            self._recheck = False

        return EvictionStatus(
            migrated_router=migrated_router,
            pending_router=routers_total
            - (migrated_router + unhandleable_router),
            unhandleable_router=unhandleable_router,
            unhandleable_agent=False,
        )

    async def is_migration_done(
        self,
        status: EvictionStatus,
    ) -> bool:
        # Everything that fails is at unhandleable_router.
        return (
            (status.pending_router + status.unhandleable_router) == 0
            and not status.unhandleable_agent
            and not self._recheck
        )

    def start_eviction_log(
        self,
        poll_interval: int,
    ) -> str:
        return (
            f"initiating eviction of node {self._node_name} with reason"
            f" {self._reason}. Up to {self.max_parallel_migrations} routers"
            f" in parallel, poll interval is {poll_interval}s"
        )

    def _os_iterate(self) -> EvictionStatus:
        # just to make mypy happy. this function is not called.
        pass
