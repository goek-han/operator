#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import typing

import openstack

import yaook.statemachine as sm
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.watcher as watcher


def get_l3_agent(
        client: openstack.connection.Connection,
        host: str,
        ) -> typing.Optional[openstack.network.v2.agent.Agent]:
    agents = list(client.network.agents(
        binary="neutron-l3-agent",
        host=host)
                  )
    if not agents:
        return None
    return agents[0]


class L3StateResource(resource.APIStateResource):
    def __init__(self, **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.neutronL3Watch: watcher.ExternalWatcher[
                openstack.network.v2.agent.Agent] = resource.\
            NeutronAgentWatcher("NeutronL3Agent", "neutron-l3-agent")

    def _get_resource(
            self,
            client: openstack.connection.Connection,
            ctx: sm.Context,
            ) -> typing.Optional[resource.ResourceStatus]:
        agent = get_l3_agent(client, ctx.parent_name)
        if agent is None:
            return None

        # If the l3 agent was last time updated before we where created
        # it is probably an old instance that has not been cleaned up.
        # In this case we just behave as if the agent was not known to neutron.
        # The used format string is taken from the neutron repository here,
        # but for reasons, the agent object returns a space instead of 'T'
        # between date and time.
        # https://opendev.org/openstack/neutron/src/branch/master/neutron/services/timestamp/timestamp_db.py#L25  # noqa: E501
        updated_at = datetime.datetime.strptime(agent.last_heartbeat_at,
                                                "%Y-%m-%d %H:%M:%S")
        created_at = ctx.creation_timestamp
        if updated_at < created_at:
            ctx.logger.warn(
                "l3 agent for %s was updated before this resource "
                "was created. Treating it as non existent", ctx.parent_name)
            return None

        return resource.ResourceStatus(
            up=agent.is_alive,
            enabled=agent.is_admin_state_up,
            disable_reason=None,
        )

    def _update_status(
            self,
            ctx: sm.Context,
            connection_info: typing.Mapping[str, typing.Any],
            state_up: bool,
            ) -> typing.Optional[resource.ResourceStatus]:
        client = openstack.connect(**connection_info)
        agent = get_l3_agent(client, ctx.parent_name)
        if agent is None:
            return None

        # As we did not raise by now the connection parameters seem to be valid
        self.neutronL3Watch.connection_parameters = connection_info
        self.neutronL3Watch.namespace = ctx.namespace

        is_state_up = agent.is_admin_state_up
        ctx.logger.debug("is_state_up = %r, want state_up = %r",
                         is_state_up, state_up)

        if is_state_up != state_up:
            if state_up:
                ctx.logger.debug("enabling l3 agent %s %s",
                                 agent.binary, agent.host)
                client.network.update_agent(
                    agent=agent.id,
                    admin_state_up=True
                )
            else:
                ctx.logger.debug("disabling l3 agent %s %s",
                                 agent.binary, agent.host)
                client.network.update_agent(
                    agent=agent.id,
                    admin_state_up=False
                )

        agent = get_l3_agent(client, ctx.parent_name)
        if agent is None:
            return None
        return resource.ResourceStatus(
            up=agent.is_alive,
            enabled=agent.is_admin_state_up,
            disable_reason=None,
        )

    def _needs_l2_agent(self) -> bool:
        return True

    def get_listeners(self) -> typing.List[sm.Listener]:
        return super().get_listeners() + [
            sm.ExternalListener(
                watcher=self.neutronL3Watch,
                listener=self._handle_neutron_event,
            ),
        ]
