#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import copy
import inspect
import itertools
import json
import logging
import importlib
import sys
import types
import typing
import yaml

import environ

import kubernetes_asyncio

import yaook.common
import yaook.statemachine
from yaook.statemachine.versioneddependencies import MappedVersionedDependency

from . import daemon


@environ.config(prefix="YAOOK_OP")
class OpCommonConfig:
    verbosity = environ.var(0, converter=int)


@environ.config(prefix="YAOOK_OP")
class OpRunConfig:
    namespace = environ.var()
    runner_count = environ.var(default=3)


async def amain(namespace, runner_count):
    try:
        kubernetes_asyncio.config.load_incluster_config()
    except Exception:
        await kubernetes_asyncio.config.load_kube_config()
    config = kubernetes_asyncio.client.Configuration.get_default_copy()
    operator = daemon.OperatorDaemon(config, namespace, runner_count)
    await operator.run()


def do_run(common_config, args, module):
    run_config = environ.to_config(OpRunConfig)

    if hasattr(module, "main"):
        return asyncio.get_event_loop().run_until_complete(module.main(
            namespace=run_config.namespace or None
        ))
    else:
        return asyncio.run(amain(run_config.namespace or None,
                                 int(run_config.runner_count)))


async def generate_roles(role_name, namespace):
    import yaook.statemachine.registry

    classes = list(yaook.statemachine.registry.registry.values())
    if not classes:
        print("this operator does not support role generation",
              file=sys.stderr)
        return 1

    MANAGE_PERMISSIONS = {
        "create", "list", "watch", "get", "delete", "patch", "update",
        "deletecollection",
    }
    IMPLEMENT_PERMISSIONS = {
        "list", "get", "watch", "patch",
    }
    IMPLEMENT_STATUS_PERMISSIONS = {
        "get", "patch", "update",
    }
    WATCH_PERMISSIONS = {
        "list", "get", "watch",
    }
    READ_PERMISSIONS = {
        "list", "get"
    }
    CREATE_PERMISSIONS = {"create"}

    async with kubernetes_asyncio.client.ApiClient() as api_client:
        ctx = yaook.statemachine.Context(
            api_client=api_client,
            parent=None,
            parent_intf=None,
            namespace=None,
            instance=None,
            instance_data=None,
            logger=None,
            field_manager=None,
        )

        requested_permissions = {
            (False, "", "events"): CREATE_PERMISSIONS,
            (False, "events.k8s.io", "events"): CREATE_PERMISSIONS,
            (False, "", "pods"): READ_PERMISSIONS,  # gather resource usage
        }

        def add_permissions(cluster_wide, api_group, plural, permissions):
            key = cluster_wide, api_group, plural
            requested_permissions.setdefault(key, set()).update(permissions)

        for _, _, _, cls in classes:
            add_permissions(True, cls.API_GROUP, cls.PLURAL,
                            IMPLEMENT_PERMISSIONS)
            add_permissions(True, cls.API_GROUP,
                            f"{cls.PLURAL}/status",
                            IMPLEMENT_STATUS_PERMISSIONS)

            for (cluster_wide,
                 api_group, plural,
                 verbs) in cls.ADDITIONAL_PERMISSIONS:
                add_permissions(cluster_wide, api_group, plural,
                                set(verbs))

            for attr in dir(cls):
                if attr.startswith("_"):
                    continue

                obj_ = getattr(cls, attr)
                if not isinstance(obj_, yaook.statemachine.Resource):
                    continue

                for listener in obj_.get_listeners():
                    if not isinstance(listener,
                                      yaook.statemachine.KubernetesListener):
                        continue

                    add_permissions(
                        False,
                        listener.api_group, listener.plural,
                        WATCH_PERMISSIONS,
                    )
                    add_permissions(
                        False,
                        listener.api_group, f"{listener.plural}/status",
                        READ_PERMISSIONS,
                    )

                # Optional may wrap a KubernetesResource, but it doesn't expose
                # that directly, so we may have to unwrap it.
                if isinstance(obj_, yaook.statemachine.Optional):
                    # not using get_inner here because we need to always get
                    # the inner state and we don't even have the context :)
                    obj_ = obj_.wrapped_state

                if not isinstance(obj_, yaook.statemachine.KubernetesResource):
                    continue

                if isinstance(obj_,
                              yaook.statemachine.instancing.PerNodeMixin):
                    add_permissions(True, "", "nodes", WATCH_PERMISSIONS)
                    obj_ = obj_._wrapped_state

                if hasattr(obj_, "get_resource_interface"):
                    # duck type, wooho
                    ri = obj_.get_resource_interface(ctx)
                    api_group, _ = yaook.statemachine.split_api_version(
                        ri.api_version,
                    )
                    add_permissions(False, api_group, ri.plural,
                                    MANAGE_PERMISSIONS)

    role_template = {
        "apiVersion": "rbac.authorization.k8s.io/v1",
        "metadata": {
            "name": role_name,
        },
        "rules": [],
    }
    cluster_role = copy.deepcopy(role_template)
    cluster_role["kind"] = "ClusterRole"
    role = copy.deepcopy(role_template)
    role["kind"] = "Role"
    if namespace is not None:
        role["metadata"]["namespace"] = namespace

    sorted_permissions = sorted(requested_permissions.items(),
                                key=lambda x: x[0])
    grouped_permissions = itertools.groupby(
        sorted_permissions,
        key=lambda x: (x[0][0], x[0][1], x[1]),
    )
    for (cluster_wide, api_group, permissions), items in grouped_permissions:
        plurals = [
            plural for (_, _, plural), _ in items
        ]
        target = cluster_role if cluster_wide else role
        target["rules"].append({
            "apiGroups": [api_group],
            "resources": plurals,
            "verbs": sorted(permissions),
        })

    import yaml
    print("# THESE ROLES WERE AUTOGENERATED USING THE yaook.op CLI")
    print("# DO NOT MODIFY")
    yaml.dump_all([cluster_role, role], sys.stdout, explicit_start=True)


def do_generate_roles(common_config, args, module):
    role_name = args.role_name or f"{args.operator}-operator"
    return asyncio.run(generate_roles(role_name, args.namespace))


async def build_structure(
        classes: typing.List[typing.Type[yaook.statemachine.CustomResource]],
        ) -> typing.Mapping:
    structure: typing.Dict[str, typing.Any] = {
        "crs": []
    }
    crs = structure["crs"]

    async with kubernetes_asyncio.client.ApiClient() as api_client:
        for cr_cls in sorted(classes, key=lambda x: x.KIND):
            states: typing.List[typing.Mapping] = []
            instance = cr_cls(logger=typing.cast(logging.Logger, None))
            for state_obj in sorted(instance.sm.states,
                                    key=lambda x: x.component):
                state_info: typing.Dict[str, typing.Any] = {
                    "component": state_obj.component,
                }

                # Optional may wrap a KubernetesResource, but it doesn't expose
                # that directly, so we may have to unwrap it.
                if isinstance(state_obj, yaook.statemachine.Optional):
                    # not using get_inner here because we need to always get
                    # the inner state and we don't even have the context :)
                    inner_obj = state_obj.wrapped_state
                else:
                    inner_obj = state_obj

                if isinstance(
                       inner_obj,
                       (yaook.statemachine.ForeignReference,
                        yaook.statemachine.ForeignResourceDependency,  # noqa: E501
                        yaook.statemachine.ReadyCertificateSecretReference)):  # noqa: E501
                    # All of these are references. Therefor they can never
                    # cause dangling resources.
                    continue
                if isinstance(inner_obj,
                              yaook.statemachine.KubernetesResource):
                    # evil
                    intf = inner_obj._create_resource_interface(api_client)
                    state_info["endpoint"] = {
                        "apiVersion": intf.api_version,
                        "plural": intf.plural,
                    }
                else:
                    state_info["info"] = repr(inner_obj)

                state_info["dependencies"] = [
                    dep.component
                    for dep in instance.sm._dependencies[state_obj]
                ]
                state_info["dependencies"].sort()

                states.append(state_info)

            crs.append({
                "endpoint": {
                    "kind": cr_cls.KIND,
                    "plural": cr_cls.PLURAL,
                    "apiVersion": yaook.statemachine.join_api_version(
                        cr_cls.API_GROUP,
                        cr_cls.API_GROUP_VERSION,
                    ),
                },
                "states": states,
            })

    return structure


async def dump_structure(format: str, graphvizcr: typing.Optional[str]) -> int:
    import yaook.statemachine.registry

    classes = [
        cr_obj
        for _, _, _, cr_obj in yaook.statemachine.registry.registry.values()
    ]
    if not classes:
        print("this operator does not support introspection",
              file=sys.stderr)
        return 1

    structure = await build_structure(classes)
    if format == "json":
        json.dump(structure, sys.stdout, indent=4)
        print()
    elif format == "graphviz":
        for cr in structure["crs"]:
            if not cr['endpoint']['kind'] == graphvizcr:
                continue
            print(f"digraph {cr['endpoint']['kind']} {{")
            print("rankdir=\"LR\"")
            for state in cr["states"]:
                print(f"{state['component']}")
                for dep in state["dependencies"]:
                    print(f"{dep} -> {state['component']}")
            print("}")
    return 0


def do_dump_structure(
        common_config: OpCommonConfig,
        args: types.SimpleNamespace,
        module: typing.Any) -> int:
    return asyncio.run(dump_structure(args.format, args.graphvizcr))


class IndexedResource(typing.NamedTuple):
    name: typing.Union[str, typing.Tuple[str, str]]
    dependencies: typing.Sequence[str]


IndexedCRStructure = typing.Mapping[str, IndexedResource]


IndexedOperatorStructure = typing.Mapping[
    typing.Tuple[str, str, str],
    IndexedCRStructure,
]


def index_structure(
        crs: typing.List[typing.Mapping[str, typing.Any]],
        ) -> IndexedOperatorStructure:
    result: typing.Dict[
        typing.Tuple[str, str, str],
        IndexedCRStructure,
    ] = {}
    for cr in crs:
        endpoint: typing.Mapping[str, str] = cr["endpoint"]
        key = (endpoint["apiVersion"], endpoint["plural"], endpoint["kind"])
        states: typing.Dict[str, IndexedResource] = {}
        for state in cr["states"]:
            data: IndexedResource
            try:
                state_endpoint: typing.Mapping[str, str] = state["endpoint"]
            except KeyError:
                data = IndexedResource(state["info"], state["dependencies"])
            else:
                data = IndexedResource(
                    (state_endpoint["apiVersion"], state_endpoint["plural"]),
                    state["dependencies"],
                )
            states[state["component"]] = data
        result[key] = states
    return result


class OperatorDiffConfig(typing.NamedTuple):
    diff_dependencies: bool = False


def check_cr_diff(
        logger: logging.Logger,
        cfg: OperatorDiffConfig,
        old_cr: IndexedCRStructure,
        new_cr: IndexedCRStructure,
        ) -> bool:
    is_ok = True
    old_components = set(old_cr.keys())
    new_components = set(new_cr.keys())
    missing = old_components - new_components
    for component in missing:
        logger.error("missing component: %s", component)
        is_ok = False

    introduced = new_components - old_components
    for component in introduced:
        logger.info("new component: %s using %s",
                    component, new_cr[component])

    shared = old_components & new_components
    for component in shared:
        old_component = old_cr[component]
        new_component = new_cr[component]
        if old_component.name != new_component.name:
            logger.error("mismatch: component %s used to use %s, "
                         "but uses %s now",
                         component, old_component.name, new_component.name)
            is_ok = False

        if cfg.diff_dependencies:
            old_deps = set(old_component.dependencies)
            new_deps = set(new_component.dependencies)

            missing_in_new = old_deps - new_deps
            missing_in_old = new_deps - old_deps

            if missing_in_new:
                logger.error("depedency change: %s no longer depends on: %s",
                             component, ', '.join(sorted(missing_in_new)))
                is_ok = False

            if missing_in_old:
                logger.error("depedency change: %s newly depends on: %s",
                             component, ', '.join(sorted(missing_in_old)))
                is_ok = False

    return is_ok


def check_structure_diff(
        logger: logging.Logger,
        cfg: OperatorDiffConfig,
        old_version: IndexedOperatorStructure,
        new_version: IndexedOperatorStructure,
        ) -> bool:
    is_ok = True
    old_keys = set(old_version.keys())
    new_keys = set(new_version.keys())
    missing = old_keys - new_keys
    for api_version, plural, kind in missing:
        logger.error("missing CR for %s/%s (kind: %s)",
                     api_version, plural, kind)
        is_ok = False

    introduced = new_keys - old_keys
    for api_version, plural, kind in introduced:
        logger.info("new CR for %s/%s (kind: %s)",
                    api_version, plural, kind)

    shared = old_keys & new_keys
    for key in shared:
        api_version, plural, kind = key
        logger.debug("found CR for %s/%s (kind: %s)",
                     api_version, plural, kind)
        old_cr = old_version[key]
        new_cr = new_version[key]
        is_ok = check_cr_diff(
            logger.getChild(f"{api_version}/{plural}"), cfg,
            old_cr, new_cr,
        ) and is_ok

    return is_ok


def do_diff_structure(
        common_config: OpCommonConfig,
        args: types.SimpleNamespace,
        module: typing.Any) -> int:
    with args.old_version as f:
        old_version_unstructured = json.load(f)
    if args.new_version is None:
        new_version_unstructured = asyncio.run(build_structure(
            [
                cr_obj
                for _, _, _, cr_obj
                in yaook.statemachine.registry.registry.values()
            ]
        ))
    else:
        with args.new_version as f:
            new_version_unstructured = json.load(f)

    old_version = index_structure(old_version_unstructured["crs"])
    new_version = index_structure(new_version_unstructured["crs"])

    logger = logging.getLogger(__name__).getChild("diff-structure")
    diff_ok = check_structure_diff(
        logger,
        OperatorDiffConfig(
            diff_dependencies=args.diff_dependencies
        ),
        old_version, new_version,
    )

    if diff_ok:
        logger.info("no issues found in %d old and %d new CRs",
                    len(old_version),
                    len(new_version))
    else:
        logger.critical("issues found, check logs")

    return (0 if diff_ok else 1)


async def pin_dependencies():
    import yaook.statemachine.registry
    import yaook.statemachine.context as context
    import yaook.statemachine.versioneddependencies as versioneddependencies
    pin_file = versioneddependencies.PIN_FILE

    classes = [
        cr_obj
        for _, _, _, cr_obj in yaook.statemachine.registry.registry.values()
    ]

    try:
        with open(pin_file) as f:
            version_pins = yaml.safe_load(f)
        if version_pins is None:
            version_pins = {}
    except FileNotFoundError:
        version_pins = {}

    async def add_version_pin(
        dependency: versioneddependencies.VersionedDependency,
    ) -> None:
        url = dependency.pin_key
        version = await dependency.get_latest_version(
            typing.cast(context.Context, None), ignore_pin=True
        )
        version_pins[url] = version

    for cls in classes:
        for dependency in cls.get_versioned_dependency():
            if isinstance(
                dependency,
                MappedVersionedDependency
            ):
                continue
            if isinstance(
                    dependency,
                    versioneddependencies.ReleaseAwareVersionedDependency):
                for dep in dependency.dependencies.values():
                    await add_version_pin(dep)
            else:
                await add_version_pin(dependency)
    with open(pin_file, "w") as f:
        yaml.safe_dump(version_pins, f, explicit_start=True, explicit_end=True)


def do_pin_dependencies(
        common_config: OpCommonConfig,
        args: types.SimpleNamespace,
        module: typing.Any) -> int:
    return asyncio.run(pin_dependencies())


def do_command(common_config: OpCommonConfig,
               args: types.SimpleNamespace,
               module: typing.Any) -> int:
    try:
        command_func = module.command
    except AttributeError:
        command_func = None
    if not inspect.iscoroutinefunction(command_func):
        print(f"operator {args.operator} does not support any commands",
              file=sys.stderr)
        return 2

    return asyncio.run(
        command_func([args.operator] + args.args),
    )


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "operator",
        help="Name of the operator module to load",
    )
    parser.add_argument(
        "-v",
        dest="verbosity",
        action="count",
        default=0,
        help="Increase verbosity (up to -vvv)"
    )
    parser.add_argument(
        "--debug-libraries",
        action="store_true",
        default=False,
        help="Let verbosity also affect third-party libraries",
    )

    subparsers = parser.add_subparsers()

    subparser = subparsers.add_parser("run")
    subparser.set_defaults(func=do_run)

    subparser = subparsers.add_parser("generate-roles")
    subparser.set_defaults(func=do_generate_roles)
    subparser.add_argument(
        "--role-name",
        default=None,
        help="Name of the roles to generate",
    )
    subparser.add_argument(
        "-n", "--namespace",
        default=None,
        help="Namespace of the Role"
    )

    subparser = subparsers.add_parser("dump-structure")
    subparser.set_defaults(func=do_dump_structure)
    subparser.add_argument(
        "-f", "--format",
        choices=["graphviz", "json"],
        default="json"
    )
    subparser.add_argument(
        "--graphvizcr",
        default=None,
        help="Name of the CR to dump in graphviz format"
    )

    subparser = subparsers.add_parser("diff-structure")
    subparser.set_defaults(func=do_diff_structure)
    subparser.add_argument(
        "old_version",
        metavar="OLD",
        type=argparse.FileType("r"),
    )
    subparser.add_argument(
        "-n", "--new",
        dest="new_version",
        metavar="NEW",
        default=None,
        type=argparse.FileType("r"),
    )
    subparser.add_argument(
        "--diff-dependencies",
        default=False,
        action="store_true",
        help="Also diff the dependencies of the states"
    )

    subparser = subparsers.add_parser("pin-dependencies")
    subparser.set_defaults(func=do_pin_dependencies)

    subparser = subparsers.add_parser("command")
    subparser.set_defaults(func=do_command)
    subparser.add_argument(
        "args",
        nargs="*",
    )

    args = parser.parse_args()

    common_config = environ.to_config(OpCommonConfig)

    configured_verbosity = max(args.verbosity, common_config.verbosity)
    yaook.common.configure_logging(configured_verbosity, args.debug_libraries)

    if not hasattr(args, "func"):
        parser.print_help()
        return 2

    operator_module = "{}.{}".format(__package__, args.operator)
    module = importlib.import_module(operator_module)

    return args.func(common_config, args, module) or 0
