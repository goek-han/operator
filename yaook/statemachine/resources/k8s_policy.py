#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Policy resources
###########################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: PodDisruptionBudget

.. autoclass:: GeneratedPodDisruptionBudget

.. autoclass:: QuorumPodDisruptionBudget

.. autoclass:: DisallowedPodDisruptionBudget

"""
import typing

import kubernetes_asyncio.client as kclient

from .. import api_utils, context, interfaces
from .base import (
    MetadataProvider,
    ResourceBody,
    evaluate_metadata,
)
from .k8s import (
    DependencyMap,
    KubernetesResource,
    SingleObject,
)
from .orchestration import (
    OptionalKubernetesReference,
)


class PodDisruptionBudget(
        SingleObject[kclient.V1beta1PodDisruptionBudget]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1beta1PodDisruptionBudget]:
        return interfaces.pod_disruption_budget_interface(api_client)

    def _needs_update(self,
                      current: kclient.
                      V1beta1PodDisruptionBudget,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["spec"], new["spec"]))


class GeneratedPodDisruptionBudget(PodDisruptionBudget):
    """
    Generate a PodDisruptionBudget from an existing Deployment or Statefulset.
    You will need to override :meth:`_get_min_available` to return the
    amount of replicas that must be available at all times.
    """

    def __init__(
            self,
            metadata: MetadataProvider,
            replicated: typing.Union[
                KubernetesResource[kclient.V1StatefulSet],
                OptionalKubernetesReference[kclient.V1StatefulSet],
                KubernetesResource[kclient.V1Deployment],
                OptionalKubernetesReference[kclient.V1Deployment],
                KubernetesResource[typing.Mapping],
            ],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata
        self.replicated = replicated
        self._declare_dependencies(self.replicated)

    def _get_min_available(self,
                           replicated: typing.Mapping
                           ) -> typing.Union[str, int]:
        """
        Return the minimum number of replicas that must be available at all
        times.

        :param replicated: The resource we are generating the pdb for.
        :return: The minimum number of replicas that must be available.
            Can also be a string to return percentages.
        """
        raise NotImplementedError()

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:

        repliacted_ref = await self.replicated.get(ctx)
        replicated = await self.replicated.get_resource_interface(ctx).read(
            repliacted_ref.namespace, repliacted_ref.name)
        if not isinstance(replicated, dict):
            replicated = api_utils.k8s_obj_to_yaml_data(replicated)

        min_available = self._get_min_available(replicated)

        selector = replicated["spec"]["template"]["metadata"]["labels"]

        pdb = {
            "apiVersion": "policy/v1beta1",
            "kind": "PodDisruptionBudget",
            "metadata": evaluate_metadata(ctx, self._metadata),
            "spec": {
                "minAvailable": min_available,
                "selector": {
                    "matchLabels": selector,
                },
            },
        }
        return pdb


class QuorumPodDisruptionBudget(GeneratedPodDisruptionBudget):
    """
    Generate a PodDisruptionBudget from an existing Deployment or Statefulset.
    The PodDisruptionBudget will guarantee that at least a quorum of pods is
    available.
    If there is only one replica in the Deployment or Statefulset then the
    PodDisruptionBudget will not guarantee anything
    """

    def __init__(
            self,
            metadata: MetadataProvider,
            replicated: typing.Union[
                KubernetesResource[kclient.V1Deployment],
                KubernetesResource[kclient.V1StatefulSet],
                OptionalKubernetesReference[kclient.V1StatefulSet],
                OptionalKubernetesReference[kclient.V1Deployment],
            ],
            **kwargs: typing.Any):
        super().__init__(
            metadata=metadata,
            replicated=replicated,
            **kwargs
        )

    def _get_min_available(self,
                           replicated: typing.Mapping
                           ) -> typing.Union[str, int]:
        replicas = replicated["spec"]["replicas"]
        if replicas <= 1:
            return 0
        return int(replicas / 2) + 1


class DisallowedPodDisruptionBudget(GeneratedPodDisruptionBudget):
    """
    Generate a PodDisruptionBudget from an existing CDS.
    The PodDisruptionBudget will guarantee that no pod is voluntarily
    disrupted.
    """

    def _get_min_available(self,
                           replicated: typing.Mapping
                           ) -> typing.Union[str, int]:
        return "100%"
