#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Base classes and mixins
#######################

.. currentmodule:: yaook.statemachine.resources

Mixins
------

.. autoclass:: TemplateMixin

Fundamental base classes
------------------------

.. autoclass:: Resource

Miscellaneous
=============

.. autofunction:: write_ca_certificates

Helpers for Handling Secrets
============================

.. autofunction:: extract_password

.. autofunction:: get_injected_secrets

.. autofunction:: get_multivalued_injected_secrets

"""

import abc
import base64
import dataclasses
import functools
import shlex
import tempfile
import typing
import os.path

import yaml

import kubernetes_asyncio.client as kclient

import jinja2

import jsonpatch

import yaook.common as common
from .. import api_utils, context, exceptions, interfaces, templating


@dataclasses.dataclass(frozen=True)
class ReadyInfo:
    is_ready: bool
    message: typing.Optional[str]

    @classmethod
    def cast(
            cls,
            other: typing.Union[bool, "ReadyInfo",
                                exceptions.ContinueableError],
    ) -> "ReadyInfo":
        if isinstance(other, bool):
            return ReadyInfo(is_ready=other, message=None)
        if isinstance(other, exceptions.ContinueableError):
            return ReadyInfo(is_ready=other.ready, message=other.msg)
        return other

    def __bool__(self) -> bool:
        return self.is_ready


DependencyMap = typing.Mapping[str, "Resource"]
ResourceBody = typing.MutableMapping[str, typing.Any]
TemplateParameters = typing.MutableMapping[str, typing.Any]
ReadyResult = typing.Union[bool, ReadyInfo]


T = typing.TypeVar("T")


def _generate_cr_patch(old: typing.Mapping,
                       new: typing.Mapping) -> typing.Iterable:
    PROTECTED_FIELDS = [
        "/metadata/uid",
        "/metadata/selfLink",
        "/metadata/managedFields",
        "/metadata/name",
        "/metadata/resourceVersion",
        "/metadata/generation",
        "/metadata/namespace",
        "/metadata/creationTimestamp",
        "/metadata/deletionTimestamp",
    ]
    for patch in jsonpatch.make_patch(old, new):
        if patch["path"] in PROTECTED_FIELDS:
            continue
        yield patch


def _scheduling_keys_to_selectors(
        scheduling_keys: typing.Iterable[str],
        ) -> typing.Iterable[api_utils.LabelSelector]:
    return [
        api_utils.LabelSelector(match_expressions=(
            api_utils.LabelExpression(
                key=key,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            ),
        ))
        for key in scheduling_keys
    ]


MetadataProvider = typing.Union[
    str,
    typing.Tuple[str, bool],
    typing.Callable[[context.Context], str],
    typing.Callable[[context.Context], typing.Tuple[str, bool]],
    typing.Callable[[context.Context], ResourceBody],
]


def evaluate_metadata(
        ctx: context.Context,
        provider: MetadataProvider,
        ) -> typing.Mapping[str, typing.Any]:
    value: typing.Union[str, typing.Tuple[str, bool], ResourceBody]
    if isinstance(provider, (str, tuple)):
        value = provider
    else:
        value = provider(ctx)
    if isinstance(value, str):
        return {"name": value}
    elif isinstance(value, tuple):
        value, generate_name = value
        if generate_name:
            return {"generateName": value}
        else:
            return {"name": value}
    else:
        return value


class Resource(metaclass=abc.ABCMeta):
    """
    Abstract base class to represent a resource.

    :param component: Unique (within a custom resource) identifier to identify
        this resource.
    :param add_dependencies: Additional resources this state depends on.

    This class defines the common interface of all resource managers which can
    be used to implement a :class:`~.CustomResource`.

    **User documentation**

    If `component` is not given, the :class:`Resource` must be assigned as a
    class attribute to the :class:`~.CustomResource`, in which case the
    attribute name will be used as `component`.

    .. warning::

        The `component` of a logical resource MUST NOT change. If the
        `component` is changed (e.g. because the attribute is renamed in the
        code) and the operator is restarted, data which belongs to the old name
        will not be found by the instance with the new name.

        Thus, if you have to rename the attribute, you are strongly advised to
        make use of the `component` argument (which takes precedence over the
        attribute assignment).

    .. seealso::

        Instantiating a :class:`~.Resource` directly is not possible. You have
        to use on of the subclasses. TODO(resource-refactor): make nice class
        hierarchy overview.

    **Public interface**

    .. autoattribute:: component

    .. automethod:: get_listeners

    .. automethod:: get_used_resources

    .. automethod:: is_ready

    .. automethod:: reconcile

    .. automethod:: delete

    .. automethod:: cleanup_orphans

    **Documentation for subclasses**

    The following methods and attributes are meant for implementing subclasses
    only.

    .. automethod:: _set_component

    .. automethod:: _set_owner

    .. automethod:: _declare_dependencies

    """

    def __init__(self, component: typing.Optional[str] = None, *,
                 add_dependencies: typing.Sequence["Resource"] = []):
        super().__init__()
        self._owned_by: typing.Optional[typing.Type] = None
        self._component: typing.Optional[str] = None
        self._dependencies: typing.Sequence["Resource"] = ()
        if component is not None:
            self._set_component(component)
        self._declare_dependencies(*add_dependencies)

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        if self._owned_by is not None:
            raise ValueError("Resource used on two different parent objects")
        self._owned_by = owner
        if self._component is None:
            self._set_component(name)
        self._set_owner(owner)

    def _declare_dependencies(
            self,
            *dependencies: "Resource"
    ) -> None:
        """
        Declare other resources as dependencies of this resource.

        :param dependencies: The resources we dependend on.

        This must only be called from constructors of subclasses,
        to register some of their arguments as resources they depend on.
        """
        self._dependencies = tuple(self._dependencies) + tuple(dependencies)

    @property
    def component(self) -> str:
        """
        The unique identifier of the logical component implemented by this
        resource.

        .. seealso::

            :class:`~.Resource` for a discussion of the significance of the
            :attr:`component`.
        """
        if self._component is None:
            raise AttributeError("component has not been set")
        return self._component

    def get_listeners(self) -> typing.List[context.Listener]:
        """
        Return a list of listener callbacks for this state.

        The machinery around the :class:`~.CustomResource` calls this method
        in order to set up watches for Kubernetes objects. This allows the
        resources to trigger additional reconcile runs based on changes to
        resources which are different than the :class:`~.CustomResource` to
        which they belong (changes to the non-status of the custom resource
        always trigger a reconcile).
        """
        return []

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Iterable[api_utils.ResourceReference]:
        """
        Obtain an iterable of resources used by this resource.

        The resulting set may include resources not managed by Yaook or by this
        operator. It also reflects the current state of the cluster and may be
        volatile.
        """
        return []

    async def is_ready(self, ctx: context.Context) -> ReadyResult:
        """
        Test whether the managed resource is ready to be used by its
        dependents.

        :param ctx: The context of the operation.
        :return: True if the resource is ready to be used, false otherwise.

        Note that returning false here does not necessarily imply that a
        reconcile run is needed right away. It is also possible that a
        long-running external process needs to finish first.

        In that case, the :class:`~.Resource` is responsible for registering
        a listener (see :meth:`get_listeners`) which is able to trigger a
        reconcile once the resource is ready to be used in order to bring the
        dependents in shape.

        The default implementation returns :data:`True`.
        """
        return True

    @abc.abstractmethod
    async def reconcile(self,
                        ctx: context.Context,
                        *,
                        dependencies: DependencyMap,
                        ) -> None:
        """
        Reconcile the state.

        :param ctx: The context of the current operation.
        :type ctx: :class:`Context`
        :param dependencies: The dependencies which are injected into this
            State.

        `dependencies` must be a dictionary mapping the component name to a
        :class:`Resource` instance, for each component declared as dependency
        by this class.
        """

    @abc.abstractmethod
    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
            ) -> None:
        """
        Delete the resource or mark it for later deletion.

        :param ctx: The context of the current operation.
        :type ctx: :class:`Context`
        :param dependencies: The dependencies which are injected into this
            State.

        This is destructive.

        This method is idempotent and a no-op if no resources are present.
        """

    @abc.abstractmethod
    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Mapping[
                typing.Tuple[str, str],
                typing.Collection[typing.Tuple[str, str]],
            ]) -> None:
        """
        Delete any old instances of this resource which are not in use anymore.

        :param ctx: The context to use.
        :param protect: A collection of resources to keep.

        `protect` must be a mapping which maps `(apiVersion, plural)` to a
        collection (preferably a set) of namespace-name-pairs of resources to
        keep. No resources from those sets will be deleted.
        """

    def _set_component(
            self,
            component: str) -> None:
        """
        Initialise the :attr:`component`.

        This is called exactly once by the machinery in :class:`~.Resource` to
        allow subclasses to initialise attributes based on the `component`
        value.
        """
        self._component = component

    def _set_owner(
            self,
            owner: typing.Type) -> None:
        """
        Set the owner of the :class:`~.Resource`.

        This is called up to once by the machinery in :class:`~.Resource` when
        the resource instance is assigned to a :class:`~.CustomResource` class
        attribute.

        (An attempt to assign the same instance to different classes will be
        greeted with :class:`ValueError`.)
        """

    def __repr__(self) -> str:
        return "<{}.{} component={!r}>".format(
            type(self).__module__,
            type(self).__qualname__,
            self._component,
        )


class KubernetesReference(Resource, typing.Generic[T], metaclass=abc.ABCMeta):
    """
    Reference to a kubernetes resource.

    This type can be used in interfaces to require a :class:`Resource`
    subclass which exposes access to a kubernetes resource of a specific type.

    **Public interface:**

    .. automethod:: get

    .. automethod:: get_all

    .. automethod:: get_resource_interface

    **Subclass interface:**

    .. automethod:: _create_resource_interface
    """

    # NOTE: We do not implement `delete` and `cleanup_orphans` here, because
    # subclasses differ massively and we want to avoid a stub implementation
    # here shadowing the lack of an override.

    @abc.abstractmethod
    async def get(
            self,
            ctx: context.Context,
            ) -> kclient.V1ObjectReference:
        """
        Retrieve the reference to the object of which the State is managed.

        :param ctx: The context of the current operation.
        :type ctx: :class:`Context`
        :raises ResourceNotPresent: If the resource is currently non-existent.
        :raises AmbiguousRequest: If there are multiple resources matching the
            request.
        :rtype: :class:`kclient.V1ObjectReference`
        :return: A reference containing the namespace and name of the object.

        Both error conditions inherit from :class:`KeyError`. In general, both
        error conditions should eventually be rectified by repeated calls to
        :meth:`reconcile`.

        That means it should be safe to propagate them to the outer reconcile
        loop.

        .. note::

            This function may be expensive, as it may require round-trips to
            the kubernetes API.

        """

    @abc.abstractmethod
    async def get_all(self,
                      ctx: context.Context) -> typing.Mapping[
                          typing.Union[str, None],
                          kclient.V1ObjectReference]:
        """
        Retrieve a mapping containing all instances of which the state is
        managed relating to the current context.

        :param ctx: The context of the current operation.
        :type ctx: :class:`Context`

        The mapping is keyed based on the `instance` string of the individual
        objects. If no objects exist, an empty mapping is returned. If this
        state only manages a single resource, the key is None instead of a
        string.
        """

    @abc.abstractmethod
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        """
        Construct a fresh resource interface to access the resources.

        :param api_client: The Kubernetes API client to use to access the
            resources.
        :return: A fresh resource interface to access the resources.`

        The result of this method may be cached, keyed on `api_client`.
        """

    @functools.lru_cache(maxsize=1)
    def _autocreate_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        return self._create_resource_interface(api_client)

    def get_resource_interface(
            self,
            ctx: context.Context,
            ) -> interfaces.ResourceInterface[T]:
        """
        Return a resource interface to access resources of the type managed
        by this object.

        :param ctx: The context to use to access the resources.
        :return: A resource interface to access the resources.
        """
        return self._autocreate_resource_interface(ctx.api_client)


class FinalResource(typing.Protocol):
    """
    Represents the resource of a oneshot that determines the final outcome
    of the oneshot. :meth:`is_successful` is used by the custom resource to
    determine if the oneshot was successful or not
    """

    async def is_successful(self,
                            ctx: context.Context,
                            ) -> bool:
        """
        Returns true if the final resource completed successfuly (e.g. if a
        job completed without errors).
        """
        ...


# This is a fun hack in order to allow type checking of methods such as
# `_set_owner`, which reference both methods from the fictitious base class
# as well as from the mixin.
if typing.TYPE_CHECKING:
    _TemplateMixinBase = Resource
else:
    _TemplateMixinBase = object


class TemplateMixin(_TemplateMixinBase):
    """
    A mixin to provide Jinja2 templating facilities in a resource.

    :param basedir: The base directory where the jinja2 instance will look
        for templates.
    :param params: A dictionary with additional parameters which may be passed
        to templates.

    If `basedir` is not set, it can be implicitly set by setting the
    :attr:`TEMPLATE_DIR` attribute on the :class:`~.CustomResource` subclass
    on which the resource is used, provided that the resource is used as a
    descriptor.

    The following functions are made available in the templating context:

    - ``chmod``: :func:`.parse_chmod`
    - ``flat_merge``: :func:`.flat_merge_dict`
    - ``shellescape``: :func:`shlex.quote`
    """

    def __init__(self, *,
                 templatedir: typing.Optional[str] = None,
                 params: typing.Optional[typing.Mapping] = None,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._jinja_env = None
        if templatedir is not None:
            self._setup_loader(jinja2.FileSystemLoader(templatedir))
        self._params = {} if params is None else params

    def _setup_loader(self, loader):
        if self._jinja_env is not None:
            return
        self._jinja_env = self._get_jinja_environment(loader)

    @property
    def jinja_env(self):
        if self._jinja_env is None:
            raise RuntimeError(f"{self!r} not initialised with templatedir=")
        return self._jinja_env

    def _set_owner(self, owner: typing.Type) -> None:
        super()._set_owner(owner)
        loader: jinja2.BaseLoader
        if hasattr(owner, "TEMPLATE_DIR"):
            loader = jinja2.FileSystemLoader(owner.TEMPLATE_DIR)
        else:
            mod_loader = jinja2.PackageLoader(owner.__module__)
            common_loader = jinja2.PackageLoader(common.__name__)
            loader = jinja2.ChoiceLoader([mod_loader, common_loader])
        self._setup_loader(loader)

    def _add_filters(self, env: jinja2.Environment) -> None:
        env.filters['chmod'] = templating.parse_chmod
        env.filters['shellescape'] = shlex.quote
        env.filters['flat_merge'] = templating.flat_merge_dict
        env.filters['resources'] = templating.container_resources
        env.filters['resources_dict'] = templating.container_resources_dict
        env.filters['db_proxy_resources'] = templating.db_proxy_resources
        env.filters['db_resources'] = templating.db_resources
        env.filters['amqp_resources'] = templating.amqp_resources
        env.filters['memcached_resources'] = templating.memcached_resources
        env.filters['ovsdb_resources'] = templating.ovsdb_resources

    def _get_jinja_environment(self, loader):
        env = jinja2.Environment(  # nosemgrep as autoescape is only designed
                                   # for HTML/XML which we dont use here.
                                   # Escaping variable to be valid yaml is
                                   # done by the extension below.
            loader=loader,
            extensions=(templating.YAMLAutoescapeExtension,),
            enable_async=True,
        )
        self._add_filters(env)
        return env

    async def _render_template(
            self,
            template: str,
            data: TemplateParameters) -> ResourceBody:
        tpl = self.jinja_env.get_template(template)
        text = await tpl.render_async(**data)
        return yaml.safe_load(text)


# TODO(resource-refactor): should probably be moved to k8s_storage or somesuch,
# that would allow moving KubernetesReference to k8s.
async def extract_password(
        ctx: context.Context,
        state: typing.Union[
            KubernetesReference[kclient.V1Secret],
            str,
        ],
        *,
        key: str = "password") -> str:
    v1 = kclient.CoreV1Api(ctx.api_client)
    if isinstance(state, str):
        secret_name = state
        secret_namespace = ctx.namespace
    else:
        secret_ref = await state.get(ctx)
        secret_name = secret_ref.name
        secret_namespace = secret_ref.namespace
    secret = await v1.read_namespaced_secret(
        secret_name,
        secret_namespace,
    )
    encpw = secret.data[key]
    return base64.b64decode(encpw).decode('utf-8')


SecretInjectionSpec = typing.Iterable[typing.Mapping[str, typing.Any]]


async def get_injected_secrets(
        ctx: context.Context,
        injection_spec: SecretInjectionSpec) -> typing.List[typing.Any]:
    """
    Construct a configuration parts from k8s secrets according
    to the specification given in the attribute *injection_spec*.

    :param ctx: The context of the current operation.
    :type ctx: :class:`~.Context`
    :param injection_spec: The injection specification.
    :type SecretInjectionSpec:
    :returns: The constructed configuration parts

    This is intended to be called from implementations of
    :meth:`CueSecret._get_config_inputs` and the return
    value should be concatenated with the other configuration
    inputs.

    An example injection specification (serialized as YAML):

    .. code-block:: yaml

      - secretName: example-secret
        items:
        - key: foo
          path: /DEFAULT/foo
        - key: bar
          path: /DEFAULT/bar

    With this spec, this method will take the contents of the keys `foo`
    and `bar` in the secret `example-secret`, say `$foo` and `$bar`,
    and return::

      [{"DEFAULT": {"foo": "$foo"}}. {"DEFAULT": {"bar": "$bar"}}]

    The path is a JSON Pointer following :rfc:`6901`. All
    referenced non-terminal keys are assumed to be objects
    and created on demand.

    Merging the configuration parts is the job of the caller,
    for cuelang you can just serialize the parts independently
    and let cuelang do the merging and verfication of consistency.
    """
    secrets = interfaces.secret_interface(ctx.api_client)
    injected_secrets = []
    for secret_source in injection_spec:
        secret_name = secret_source["secretName"]
        secret = (await secrets.read(ctx.namespace, secret_name)).data
        for secret_item in secret_source["items"]:
            attribute_path = jsonpatch.JsonPointer(secret_item["path"]).parts
            secret_key = secret_item["key"]
            try:
                value = base64.b64decode(
                    secret[secret_key]).decode('utf-8')
            except KeyError:
                raise exceptions.ConfigurationInvalid(
                    f"Secret {secret_name} has no key {secret_key}")
            cur: typing.Any = value
            for part in reversed(attribute_path):
                cur = {part: cur}
            injected_secrets.append(cur)
    return injected_secrets


async def get_multivalued_injected_secrets(
        ctx: context.Context,
        injection_spec: SecretInjectionSpec) -> typing.List[typing.Any]:
    """
    Construct a configuration parts from k8s secrets according
    to the specification given in the attribute *injection_spec*.

    This method should be used when multple items in the  *injection_spec*
    have the same path.

    :param ctx: The context of the current operation.
    :type ctx: :class:`~.Context`
    :param injection_spec: The injection specification.
    :type SecretInjectionSpec:
    :returns: The constructed configuration parts

    This is intended to be called from implementations of
    :meth:`CueSecret._get_config_inputs` and the return
    value should be concatenated with the other configuration
    inputs.

    An example injection specification (serialized as YAML):

    .. code-block:: yaml

      - secretName: example-secret
        items:
        - key: foo
          path: /DEFAULT/foobar
        - key: bar
          path: /DEFAULT/foobar

    With this spec, this method will take the contents of the keys `foo`
    and `bar` in the secret `example-secret`, say `$foo` and `$bar`,
    and return::

        [{"DEFAULT": {"foobar": "$foo"}}. {"DEFAULT": {"foobar": "$bar"}}]

    The path is a JSON Pointer following :rfc:`6901`. All
    referenced non-terminal keys are assumed to be objects
    and created on demand.

    Merging the configuration parts is the job of the caller,
    for cuelang you can just serialize the parts independently
    and let cuelang do the merging and verfication of consistency.

    This method could be merged with get_injected_secrets() by using the
    multivalued knowledge from common/config.py.
    """
    secrets = interfaces.secret_interface(ctx.api_client)
    injected_secrets = []
    # This contains all values for the same path in one list, e.g
    # {'DEFAULT/foo': ['valueA', 'valueB']}
    multivalued_items: typing.Dict[str, typing.List[str]] = {}
    for secret_source in injection_spec:
        secret_name = secret_source["secretName"]
        secret = (await secrets.read(ctx.namespace, secret_name)).data
        for secret_item in secret_source["items"]:
            path = secret_item["path"]
            secret_key = secret_item["key"]
            try:
                value = base64.b64decode(
                    secret[secret_key]).decode('utf-8')
            except KeyError:
                raise exceptions.ConfigurationInvalid(
                    f"Secret {secret_name} has no key {secret_key}")
            if path in multivalued_items.keys():
                multivalued_items[path].append(value)
            else:
                multivalued_items[path] = [value]
    for path, value_list in multivalued_items.items():
        attribute_path = jsonpatch.JsonPointer(path).parts
        cur: typing.Any = value_list
        for part in reversed(attribute_path):
            cur = {part: cur}
        injected_secrets.append(cur)
    return injected_secrets


async def make_db_config_overlay(
        ctx: context.Context,
        db_service: KubernetesReference[kclient.V1Service],
        db_name: str,
        db_username: str,
        db_user_password_secret: KubernetesReference[kclient.V1Secret],
        *,
        config_section: str = "database",
        ) -> typing.MutableMapping[str, typing.Any]:
    service_ref = await db_service.get(ctx)
    host = f"{service_ref.name}.{service_ref.namespace}"
    password = await extract_password(
        ctx,
        db_user_password_secret,
    )
    return {
        config_section: {
            "#connection_username": db_username,
            "#connection_database": db_name,
            "#connection_password": password,
            "#connection_host": host,
        }
    }


async def make_mq_config_overlay(
        ctx: context.Context,
        mq_service: KubernetesReference[kclient.V1Service],
        mq_username: str,
        mq_user_password_secret: KubernetesReference[kclient.V1Secret],
        ) -> typing.MutableMapping[str, typing.Any]:
    service_ref = await mq_service.get(ctx)
    host = f"{service_ref.name}.{service_ref.namespace}"
    password = await extract_password(
        ctx,
        mq_user_password_secret,
    )
    return {
        "DEFAULT": {
            "#transport_url_hosts": [host],
            "#transport_url_username": mq_username,
            "#transport_url_password": password,
            "#transport_url_vhost": "",
        }
    }


async def write_ca_certificates(
        ca_config: kclient.V1ObjectReference,
        ctx: context.Context,
        ) -> str:
    """
    Write the CA Certificates from the passed ca_config to a local file and
    return the filename.

    This method is using the Context to choose an appropriate file name. Calls
    to this method with the same Context might overwrite the old file.

    :param ca_config: A reference to the Configmap holding the ca certificates.
    :param ctx: the Context.
    :return: The path to the ca-bundle file.
    """
    configmaps = interfaces.config_map_interface(ctx.api_client)
    ca_configmap = await configmaps.read(
        ca_config.namespace, ca_config.name)
    ca_bundle = ca_configmap.data["ca-bundle.crt"]

    tmppath = os.path.join(tempfile.gettempdir(), f"{ctx.parent_uid}.crt")
    with open(tmppath, "w") as f:
        f.write(ca_bundle)
    return tmppath
