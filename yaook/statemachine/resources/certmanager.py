#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
cert-manager.io resources
#########################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Certificate

.. autoclass:: EmptyTlsSecret

.. autoclass:: TemplatedCertificate

"""
import typing

import kubernetes_asyncio.client as kclient

from .. import context, exceptions, interfaces, watcher
from .base import (
    DependencyMap,
    MetadataProvider,
    ResourceBody,
    evaluate_metadata,
)
from .k8s import (
    BodyTemplateMixin,
    KubernetesReference,
    SingleObject,
)
from .k8s_storage import Secret


class EmptyTlsSecret(Secret):
    """
    Generates an empty tls secret which is never updated.
    This can be used as a backing secret for :class:`~.CertificateState`
    """
    def __init__(self,
                 metadata: MetadataProvider,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:

        config = {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": evaluate_metadata(ctx, self._metadata),
            "type": "kubernetes.io/tls",
            "data": {"tls.crt": "", "tls.key": ""},
        }
        return config

    def _needs_update(self,
                      current: kclient.V1Secret,
                      new: typing.Mapping) -> bool:
        return False


class Certificate(SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.certificates_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        return (super()._needs_update(current, new) or
                current.get("spec", {}) != new.get("spec", {}))

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener(
                "cert-manager.io", "v1", "certificates",
                listener=self._handle_certificate_event,
            )
        ]

    @staticmethod
    def _has_ready_condition_set_to_true(body: typing.Mapping) -> bool:
        conditions = body.get("status", {}).get("conditions", [])
        for condition in conditions:
            if condition.get("type") == "Ready":
                return condition.get("status", "False") == "True"
        return False

    def _handle_certificate_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[typing.Mapping],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            # if the thing was deleted, we need to do something about it asap
            return True

        old_ready = self._has_ready_condition_set_to_true(
            event.old_object or {},
        )
        old_not_before = (event.old_object or {}
                          ).get("status", {}).get("notBefore")
        new_ready = self._has_ready_condition_set_to_true(
            event.object_ or {},
        )
        new_not_before = event.object_.get("status", {}).get("notBefore")
        return (old_ready != new_ready or
                old_not_before != new_not_before) and new_ready

    async def is_ready(self, ctx: context.Context) -> bool:
        """
        Return true if and only if the certificate as been issued.
        """
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        return self._has_ready_condition_set_to_true(instance)


class Issuer(SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.issuer_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        return (super()._needs_update(current, new) or
                current.get("spec", {}) != new.get("spec", {}))

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener(
                "cert-manager.io", "v1", "issuers",
                listener=self._handle_issuer_event,
            )
        ]

    @staticmethod
    def _has_ready_condition_set_to_true(body: typing.Mapping) -> bool:
        conditions = body.get("status", {}).get("conditions", [])
        for condition in conditions:
            if condition.get("type") == "Ready":
                return condition.get("status", "False") == "True"
        return False

    def _handle_issuer_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[typing.Mapping],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            # if the thing was deleted, we need to do something about it asap
            return True

        old_ready = self._has_ready_condition_set_to_true(
            event.old_object or {},
        )
        new_ready = self._has_ready_condition_set_to_true(
            event.object_ or {},
        )
        return old_ready != new_ready and new_ready

    async def is_ready(self, ctx: context.Context) -> bool:
        """
        Return true if and only if the certificate as been issued.
        """
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        return self._has_ready_condition_set_to_true(instance)


class TemplatedCertificate(BodyTemplateMixin, Certificate):
    """
    Manage a jinja2-templated CertificateState.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.CertificateState`:
            for the original state.
    """


class TemplatedIssuer(BodyTemplateMixin, Issuer):
    """
    Manage a jinja2-templated IssuerState.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.IssuerState`:
            for the original state.
    """


class ReadyCertificateSecretReference(KubernetesReference[kclient.V1Secret]):
    """
    Represent a reference to the secret of an issued Certificate.

    While a Certificate requires an EmptyTlsSecretState and thus a reference to
    the secret is generally available, the Secret is not populated until
    cert-manager got around to actually issue the certificate.

    If the Issuer is slow, broken or overloaded, it may take a while for the
    Secret to become populated. In some cases, services require that the
    certificate data is present and do not handle absence gracefully.

    This reference state will only report ready if the keys `tls.crt`,
    `tls.key` an `ca.crt` of the secret referenced by the Certificate are
    present and not empty.
    """

    def __init__(
            self,
            *,
            certificate_reference: KubernetesReference[typing.Mapping],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(certificate_reference)
        self._certificate_reference = certificate_reference

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[kclient.V1Secret]:
        return interfaces.secret_interface(api_client)

    async def get(self,
                  ctx: context.Context,
                  ) -> kclient.V1ObjectReference:
        certificate_ref = await self._certificate_reference.get(ctx)
        intf = interfaces.certificates_interface(ctx.api_client)
        certificate = await intf.read(
            certificate_ref.namespace,
            certificate_ref.name,
        )
        secret_name = certificate["spec"]["secretName"]
        return kclient.V1ObjectReference(
            namespace=certificate_ref.namespace,
            name=secret_name,
        )

    async def get_all(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference,
            ]:
        # NOTE: this makes it unusable for instancing, but we don’t need that
        # at this moment.
        return {
            None: await self.get(ctx),
        }

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        return

    async def is_ready(
            self,
            ctx: context.Context,
            ) -> bool:
        # NOTE: despite this presence of a non-default is_ready, we do not need
        # to implement a watch.
        # This is because the CertificateState already has a watch and it gets
        # notified when the secret is completed.
        secret_ref = await self.get(ctx)
        intf = self.get_resource_interface(ctx)
        secret = await intf.read(secret_ref.namespace, secret_ref.name)
        return bool(secret.data.get("tls.key") and
                    secret.data.get("tls.crt") and
                    secret.data.get("ca.crt"))

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """
