#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.resources` – Classes representing resources managed by an operator
############################################################################################


Permissions
===========

The Service Account under which an operator is running needs the following
permissions for any resource it manages:

- create
- delete
- list
- watch
- patch
- get

In addition, if any resource is modified with :class:`PerNode`, the operator
will also need the following resources on v1 Node resources:

- get
- list
- watch

For the custom resources implemented by the operator, the following permissions
are required:

- get
- list
- watch
- patch

To report concerning conditions, the operators need the following permissions
on event objects:

- create

.. automodule:: yaook.statemachine.resources.k8s_authn

.. automodule:: yaook.statemachine.resources.k8s_authz

.. automodule:: yaook.statemachine.resources.k8s_policy

.. automodule:: yaook.statemachine.resources.k8s_service

.. automodule:: yaook.statemachine.resources.k8s_storage

.. automodule:: yaook.statemachine.resources.k8s_workload

.. automodule:: yaook.statemachine.resources.yaook_cds

.. automodule:: yaook.statemachine.resources.yaook_infra

.. automodule:: yaook.statemachine.resources.yaook_keystone

.. automodule:: yaook.statemachine.resources.yaook_nova

.. automodule:: yaook.statemachine.resources.certmanager

.. automodule:: yaook.statemachine.resources.prometheus

.. automodule:: yaook.statemachine.resources.helm

.. automodule:: yaook.statemachine.resources.dependencies

.. automodule:: yaook.statemachine.resources.instancing

.. automodule:: yaook.statemachine.resources.external

.. automodule:: yaook.statemachine.resources.orchestration

.. automodule:: yaook.statemachine.resources.k8s

.. automodule:: yaook.statemachine.resources.yaook

.. automodule:: yaook.statemachine.resources.base

"""  # noqa:E501

from .base import (  # noqa:F401
    DependencyMap,
    FinalResource,
    MetadataProvider,
    ReadyInfo,
    ReadyResult,
    Resource,
    ResourceBody,
    SecretInjectionSpec,
    TemplateMixin,
    TemplateParameters,
    evaluate_metadata,
    extract_password,
    get_injected_secrets,
    get_multivalued_injected_secrets,
    make_db_config_overlay,
    make_mq_config_overlay,
    write_ca_certificates,
)

from .certmanager import (  # noqa:F401
    Certificate,
    EmptyTlsSecret,
    Issuer,
    TemplatedCertificate,
    TemplatedIssuer,
    ReadyCertificateSecretReference,
)

from .dependencies import (  # noqa:F401
    AMQPServerRef,
    DependencyNotReady,
    ForeignReference,
    ForeignResourceDependency,
    ExternalSecretReference,
    KeystoneReference,
    NovaReference,
    ReadyForeignReference,
)

from .external import (  # noqa:F401
    ExternalResource,
)

from .helm import (  # noqa:F401
    HelmRelease,
)

from .instancing import (  # noqa:F401
    InstancedResource,
    PerNode,
    PerNodeMixin,
    PerSetEntry,
    PerStatefulSetPod,
    ResourceRunState,
    ResourceSpecState,
    StatefulInstancedResource,
    StatefulPerNode,
    StatelessInstancedResource,
)

from .k8s import (  # noqa:F401
    BodyTemplateMixin,
    DefaultTemplateParamsMixin,
    KubernetesReference,
    KubernetesResource,
    Orphan,
    SingleObject,
)

from .k8s_authn import (  # noqa:F401
    ServiceAccount,
    TemplatedServiceAccount,
)

from .k8s_authz import (  # noqa:F401
    Role,
    ClusterRole,
    RoleBinding,
    ClusterRoleBinding,
    TemplatedRole,
    TemplatedClusterRole,
    TemplatedRoleBinding,
    TemplatedClusterRoleBinding,
)

from .k8s_policy import (  # noqa:F401
    DisallowedPodDisruptionBudget,
    GeneratedPodDisruptionBudget,
    PodDisruptionBudget,
    QuorumPodDisruptionBudget,
)

from .k8s_service import (  # noqa:F401
    Ingress,
    Service,
    TemplatedIngress,
    TemplatedService,
)

from .k8s_storage import (  # noqa:F401
    AutoGeneratedPassword,
    CAConfigMap,
    ConfigMap,
    PersistentVolumeClaim,
    PolicyConfigMap,
    ReleaseAwarePolicyConfigMap,
    ReadyPolicyConfigMapReference,
    PolicyValidationScriptConfigMap,
    Secret,
    TemplatedConfigMap,
    TemplatedPersistentVolumeClaim,
    TemplatedSecret,
    generate_password,
)

from .k8s_workload import (  # noqa:F401
    CronJob,
    Deployment,
    FinalTemplatedJob,
    Job,
    StatefulSet,
    TemplatedCronJob,
    TemplatedDeployment,
    TemplatedJob,
    TemplatedStatefulSet,
)

from .openstack import (  # noqa:F401
    APIStateResource,
    BackgroundJob,
    BackgroundJobController,
    L2AwareStatefulAgentResource,
    OpenStackMixin,
    PolicyValidator,
    StatefulAgentResource,
    TemplatedRecreatingStatefulSet,
    KeystoneResource,
    _OpaqueParameters,
)

from .orchestration import (  # noqa:F401
    Barrier,
    Optional,
    OptionalKubernetesReference,
    L2Lock,
    SendSignal,
    TriggerRollingRestart,
    optional_not_in_releases,
    optional_only_in_releases,
    optional_non_upgrade,
    optional_only_upgrade,
    Upgrade,
)

from .prometheus import (  # noqa:F401
    GeneratedServiceMonitor,
    ServiceMonitor,
    GeneratedStatefulsetServiceMonitor,
)

from .yaook import (  # noqa:F401
    YaookReadyResource,
    YaookResourceReadinessMixin,
)

from .yaook_cds import (  # noqa:F401
    ConfiguredDaemonSet,
    TemplatedConfiguredDaemonSet,
)

from .yaook_infra import (  # noqa:F401
    AMQPServer,
    AMQPUser,
    MemcachedService,
    MySQLService,
    MySQLUser,
    OVSDBService,
    SimpleAMQPUser,
    SimpleMySQLUser,
    TemplatedAMQPServer,
    TemplatedAMQPUser,
    TemplatedMemcachedService,
    TemplatedMySQLService,
    TemplatedMySQLUser,
    TemplatedOVSDBService,
)

from .yaook_keystone import (  # noqa:F401
    KeystoneUser,
    StaticKeystoneUser,
    StaticKeystoneUserWithParent,
    KeystoneEndpoint,
    InstancedKeystoneUser,
    TemplatedKeystoneEndpoint,
)

from .yaook_nova import (  # noqa:F401
    SSHIdentity,
)
