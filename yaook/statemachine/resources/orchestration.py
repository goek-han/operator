#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`yaook.statemachine.orchestration` -- Tools for (upgrade) orchestration
############################################################################

This module contains resource subclasses which are not really resources. They
are instead actions which are always taken during a reconciliation run.
Generally, doing these things *always* isn't really useful -- instead, one
might often want to guard them using :class:`~.statemachine.Optional` with
:meth:`~.statemachine.optional_only_upgrade` or similar.

.. autoclass:: L2Lock

.. autoclass:: TriggerRollingRestart

.. autoclass:: SendSignal

.. autoclass:: Barrier

.. autoclass:: Optional

.. autoclass:: OptionalKubernetesReference

.. autofunction:: optional_non_upgrade

.. autofunction:: optional_not_in_releases

.. autofunction:: optional_only_upgrade
"""
import asyncio
import functools
import json
import jsonpatch
import subprocess
import typing

import kubernetes_asyncio.client as kclient
import kubernetes_asyncio.stream as kstream

from .. import api_utils, context, interfaces, version_utils, watcher
from .base import (
    DependencyMap,
    ReadyResult,
    Resource,
)
from .k8s import KubernetesReference, KubernetesResource
from .k8s_workload import (
    Deployment,
    StatefulSet,
)


LOCK_NAME_FORMAT = context.ANNOTATION_L2_MIGRATION_LOCK + "{kind}"


T = typing.TypeVar("T")


class L2Lock(Resource):
    """
    This class is used to set an annotation to the node, so l2-operator knows
    the l2 agent must not be deleted (yet).

    .. seealso::

        :class:`~.Resource`, :class:`~.Resource`
            for supported arguments.
    """

    # This two functions can be overwritten by other operators, e.g. bgp,
    # so the annotation and node name is right.
    def _get_annotation_name(self, ctx: context.Context) -> str:
        return LOCK_NAME_FORMAT.format(kind=ctx.parent_kind)

    def _get_patch_node_name(self, ctx: context.Context) -> str:
        return ctx.parent_name

    async def _remove_old_annotation(self, ctx: context.Context) -> None:
        pass

    async def reconcile(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
            ) -> None:
        v1 = kclient.CoreV1Api(ctx.api_client)
        for _ in range(10):
            node = await v1.read_node(self._get_patch_node_name(ctx))
            if (node.metadata.annotations or {}).get(
                    self._get_annotation_name(ctx)) is not None:
                return
            if (node.metadata.labels or {}).get(
                        context.LABEL_L2_REQUIRE_MIGRATION, "True"
                    ) == "True" and \
                    ctx.parent['metadata'].get(
                        'deletionTimestamp', None) is None:
                raise Exception(
                    'There is an LABEL_L2_REQUIRE_MIGRATION at this ' +
                    'node, so no agent needing l2 should be scheduled here ' +
                    'currently. node: ' + self._get_patch_node_name(ctx) +
                    ' agent type: ' + ctx.parent_kind
                )
            try:
                # we need to set the resourceVersion (and use field_manager for
                # SSA and to not change an updated node. We need to be sure, no
                # migration annotation was made in the meantime.
                await v1.patch_node(
                    self._get_patch_node_name(ctx),
                    [{
                        "op": "add",
                        "path": jsonpatch.JsonPointer.from_parts([
                            "metadata", "annotations",
                            self._get_annotation_name(ctx)
                        ]).path,
                        "value": "",
                    }, {
                        "op": "add",
                        "path": jsonpatch.JsonPointer.from_parts([
                            "metadata", "resourceVersion"]).path,
                        "value": node.metadata.resource_version,
                    }],
                    field_manager=ctx.field_manager,
                )
                await self._remove_old_annotation(ctx)
                return
            except kclient.ApiException as exc:
                if exc.status != 409:
                    raise

        raise Exception("Could not patch node as we were unable to "
                        "generate a new version.")

    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
            ) -> None:
        pass

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Mapping[
                typing.Tuple[str, str],
                typing.Collection[typing.Tuple[str, str]],
            ]) -> None:
        pass


class OptionalError(ValueError):
    """
    Error that is thrown when calling a method on a state that currently should
    not run. This is normally an implementation error.
    """
    pass


class Optional(Resource):
    """
    Wraps a state and only activates it under certain conditions.

    :param condition: Function that evaluates to :data:`True` when the
        wrapped_state should be run
    :param wrapped_state: The state that is run if the condition is satisfied

    .. seealso::

        The following ready-made predicates exist:

        - :func:`~.optional_only_upgrade`
        - :func:`~.optional_non_upgrade`
        - :func:`~.optional_not_in_releases`

    .. seealso::

        :class:`~.KubernetesResource`, :class:`~.Resource`
            for more supported arguments.
    """
    def __init__(self, *,
                 condition: typing.Callable[[context.Context], bool],
                 wrapped_state: Resource,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.condition = condition
        self.wrapped_state = wrapped_state
        self._declare_dependencies(*wrapped_state._dependencies)

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        super().__set_name__(owner, name)
        self.wrapped_state.__set_name__(owner, name)

    def _enforce_condition(self, ctx: context.Context) -> None:
        if not self.condition(ctx):
            raise OptionalError()

    def get_listeners(self) -> typing.List[context.Listener]:
        return self.wrapped_state.get_listeners()

    def get_inner(self, ctx: context.Context) -> typing.Optional[Resource]:
        if self.condition(ctx):
            return self.wrapped_state
        return None

    async def reconcile(self,
                        ctx: context.Context,
                        *,
                        dependencies: DependencyMap,
                        ) -> None:
        if not self.condition(ctx):
            await self.wrapped_state.delete(
                ctx,
                dependencies=dependencies,
            )
            return
        await self.wrapped_state.reconcile(
            ctx,
            dependencies=dependencies
        )

    async def is_ready(self, ctx: context.Context) -> ReadyResult:
        if not self.condition(ctx):
            return True
        return await self.wrapped_state.is_ready(ctx)

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Mapping[
                typing.Tuple[str, str],
                typing.Collection[typing.Tuple[str, str]],
            ]) -> None:
        return await self.wrapped_state.cleanup_orphans(ctx, protect)

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Iterable[api_utils.ResourceReference]:
        return await self.wrapped_state.get_used_resources(ctx)

    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
    ) -> None:
        return await self.wrapped_state.delete(
            ctx,
            dependencies=dependencies,
        )


class OptionalKubernetesReference(Optional, KubernetesReference[T]):
    """
    Wraps a KubernetesReference state and only activates it under certain
    conditions.
    Modifies the listeners of the KubernetesReference to do nothing in case
    the condition is not satisfied.
    Also proxies the methods of the wrapped KubernetesReference.

    :param condition: Function that evaluates to :data:`True` when the
        wrapped_state should be run
    :param wrapped_state: The KubernetesReference state that is run if the
        condition is satisfied

    .. seealso::

        :class:`~.KubernetesResource`, :class:`~.Resource`
            for more supported arguments.
    """
    def __init__(self, *,
                 condition: typing.Callable[[context.Context], bool],
                 wrapped_state: KubernetesReference[T],
                 **kwargs: typing.Any):
        super().__init__(condition=condition,
                         wrapped_state=wrapped_state, **kwargs)

    def _to_conditional_listener(
            self, listener: context.Listener) -> context.Listener:
        """
        Clones a given Listener so that its callback is modified in a way that
        is aware of the conditional and doesn't trigger any actions in case
        the conditional is not met, i.e. the state wrapped by this Optional
        is inactive.
        """

        if not isinstance(listener, context.KubernetesListener):
            raise TypeError("OptionalKubernetesReference only supports "
                            "KubernetesListeners as inner listeners.")

        def optional_callback(
                listener_func: typing.Callable
                ) -> typing.Callable:
            @functools.wraps(listener_func)
            def wrapper(ctx: context.Context,
                        event: watcher.StatefulWatchEvent) -> bool:
                """
                Calls the callback function only if the Optional's condition is
                actually met. Otherwise simply returns False.
                """
                if not self.condition(ctx):
                    return False
                return listener_func(ctx, event)
            return wrapper

        return context.KubernetesListener(
            api_group=listener.api_group, version=listener.version,
            plural=listener.plural,
            listener=optional_callback(listener.listener),
            broadcast=listener.broadcast, component=listener.component
        )

    def get_listeners(self) -> typing.List[context.Listener]:
        return list(map(
            self._to_conditional_listener,
            super().get_listeners() + self.wrapped_state.get_listeners()
        ))

    async def get(
            self,
            ctx: context.Context,
            ) -> kclient.V1ObjectReference:
        # casting the type here because the type checker doesn't realize that
        # the inherited self.wrapped is locked to KubernetesReference[T] by the
        # constructor instead of being a Resource as in the parent class
        wrapped_state = typing.cast(KubernetesReference[T], self.wrapped_state)
        return await wrapped_state.get(ctx)

    async def get_all(self,
                      ctx: context.Context) -> typing.Mapping[
                          typing.Union[str, None],
                          kclient.V1ObjectReference]:
        # casting the type here because the type checker doesn't realize that
        # the inherited self.wrapped is locked to KubernetesReference[T] by the
        # constructor instead of being a Resource as in the parent class
        wrapped_state = typing.cast(KubernetesReference[T], self.wrapped_state)
        return await wrapped_state.get_all(ctx)

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        wrapped_state = typing.cast(KubernetesReference[T], self.wrapped_state)
        return wrapped_state._create_resource_interface(api_client)

    def get_resource_interface(
            self,
            ctx: context.Context,
            ) -> interfaces.ResourceInterface[T]:
        # casting the type here because the type checker doesn't realize that
        # the inherited self.wrapped is locked to KubernetesReference[T] by the
        # constructor instead of being a Resource as in the parent class
        wrapped_state = typing.cast(KubernetesReference[T], self.wrapped_state)
        return wrapped_state.get_resource_interface(ctx)


class TriggerRollingRestart(Resource):
    """
    Trigger a rolling restart on a statefulset or deployment resource.

    This operates by setting an annotation on the pod template, hence forcing
    the kubernetes controller to controlledly restart the pods.

    After triggering the rolling restart, it depends on the backing controller
    resource whether it waits for completion. Consult that resource'
    ``is_ready`` method.
    """

    def __init__(
            self,
            *,
            controller_ref: typing.Union[StatefulSet, Deployment],
            restart_id: typing.Callable[[context.Context], str],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self.controller_ref = controller_ref
        self.restart_id = restart_id

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        restart_id = self.restart_id(ctx)
        await self.controller_ref.rolling_restart(ctx, restart_id)

    async def is_ready(self, ctx: context.Context) -> bool:
        return await self.controller_ref.is_ready(ctx)

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """


async def kubernetes_run(
        ws_client: kstream.WsApiClient,
        *,
        namespace: str,
        pod: str,
        container: typing.Optional[str] = None,
        command: typing.Sequence[str],
) -> subprocess.CompletedProcess:
    """
    Run a command in a container in a Kubernetes pod and return the
    result as :class:`subprocess.CompletedProcess`.

    :param namespace: The namespace of the pod.
    :param pod: The name of the pod.
    :param container: The name of the container within the pod.
    :param command: The command to execute.

    Standard output and standard error are captured by default.
    """

    ws_core_v1 = kclient.CoreV1Api(ws_client)
    websock = await ws_core_v1.connect_get_namespaced_pod_exec(
        pod,
        namespace,
        container=container,
        command=command,
        tty=False,
        stdin=False,
        stdout=True,
        stderr=True,
        _preload_content=False,
    )
    stdout = bytearray()
    stderr = bytearray()
    while True:
        msg = await websock.receive_bytes()
        if msg[0] == 0x01:
            # stdout
            stdout.extend(msg[1:])
        elif msg[0] == 0x02:
            # stderr
            stderr.extend(msg[1:])
        elif msg[0] == 0x03:
            # end of command
            break
        else:
            raise RuntimeError(
                "exec websocket protocol violation: unknown message "
                f"type: {msg[0]!r}",
            )

    info = json.loads(msg[1:].decode("utf-8"))
    if info["status"] == "Failure":
        if info["reason"] == "NonZeroExitCode":
            try:
                returncode, = [
                    int(cause["message"])
                    for cause in info["details"]["causes"]
                ]
            except (ValueError, TypeError) as exc:
                raise ValueError(
                    f"failed to process response JSON {info!r}: "
                    f"failed to read returncode: {exc}"
                )
        else:
            raise RuntimeError(
                f"failed to execute command via kubernetes: {info!r}",
            )
    elif info["status"] == "Success":
        returncode = 0
    else:
        raise RuntimeError(
            f"unexpected response JSON: {info!r}",
        )
    await websock.close()
    return subprocess.CompletedProcess(
        args=command,
        returncode=returncode,
        stdout=bytes(stdout),
        stderr=bytes(stderr),
    )


async def pkill_container(
        api_client: kclient.ApiClient,
        namespace: str,
        pod: str,
        container: str,
        process: str,
        signal: typing.Union[str, int],
) -> None:
    """
    Execute ``/bin/pkill`` in a container of a pod.
    """
    async with kstream.WsApiClient(api_client.configuration) as ws_client:
        proc = await kubernetes_run(
            ws_client,
            namespace=namespace,
            pod=pod,
            container=container,
            command=["/bin/pkill", "--signal", str(signal), "-f", process],
        )

    proc.check_returncode()


class SendSignal(Resource):
    """
    Send a signal to all pods matching the labels of a given resource.

    .. warning::

        This finds the pods belonging to the resource by looking for pods
        matching the labels of that resource. This works only if the labels are
        set on the pods, which is the responsibility of the ``_make_body``
        function associated with the corresponding resource.

        If the ``_make_body`` function (or the template backing it) do not
        adhere to this, the :class:`SendSignal` action will log a warning, but
        otherwise pass.

    .. note::

        This requires that `pkill` is installed in the container image,
        otherwise it will fail (loudly).
    """

    def __init__(
            self,
            *,
            ref: KubernetesResource,
            container: str,
            signal: typing.Union[str, int],
            process_name: str,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self.ref = ref
        self.container = container
        self.signal = signal
        self.process_name = process_name

    async def _find_pods(
            self,
            ctx: context.Context,
    ) -> typing.Sequence[kclient.V1ObjectReference]:
        intf = interfaces.pod_interface(ctx.api_client)
        labels = self.ref.labels(ctx)
        pods = await intf.list_(
            ctx.namespace,
            label_selector=labels,
        )
        refs = []
        for pod in pods:
            refs.append(kclient.V1ObjectReference(
                namespace=pod.metadata.namespace,
                name=pod.metadata.name,
            ))
        if not refs:
            ctx.logger.warning(
                "no pods matched labels %r; not sending any signals",
                labels,
            )
        return refs

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        pods = await self._find_pods(ctx)
        tasks = []
        for pod in pods:
            tasks.append(asyncio.create_task(pkill_container(
                ctx.api_client,
                pod.namespace,
                pod.name,
                self.container,
                self.process_name,
                self.signal,
            )))
        await asyncio.gather(*tasks)
        ctx.logger.info("sent signals to %d pods", len(pods))

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """


class Barrier(Resource):
    """
    Virtual resource which does nothing.

    This is intended to be used as a
    `barrier, in the concurrent programming sense
    <https://en.wikipedia.org/wiki/Barrier_(computer_science)>`_. As the
    statemachine will flatten the state graph in an undefined order, only
    dependencies between nodes (resources) can control the order in which
    things execute.

    Sometimes, it is required that multiple things have finished before
    multiple other things have started. While it is possible to express that by
    adding the prerequisites as dependencies to all subsequent tasks, this is
    error-prone if the list of dependencies changes: it needs to be updated in
    all places.

    The Barrier resource can be used to group dependencies together by adding
    the dependencies to the Barrier and then adding the barrier as dependency
    to subsequent states. As the barrier will only ever become ready after all
    of its dependencies have become ready, it effectively groups these
    dependencies together under a single name.
    """

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        pass

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        pass

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        pass


def optional_not_in_releases(releases: typing.List[str]) -> typing.Callable:
    """
    Condition function for Optional and its subclasses that only triggers
    when the target release is not part of the given list of releases.

    :param releases: List of releases that the condition checks

    .. seealso::

        :class:`~.Optional` for more predicates.
    """
    return lambda ctx: version_utils.get_target_release(ctx) not in releases


def optional_only_in_releases(releases: typing.List[str]) -> typing.Callable:
    """
    Condition function for Optional and its subclasses that only triggers
    when the target release is part of the given list of releases.

    :param releases: List of releases that the condition checks

    .. seealso::

        :class:`~.Optional` for more predicates.
    """
    return lambda ctx: version_utils.get_target_release(ctx) in releases


def optional_non_upgrade() -> typing.Callable:
    """
    Condition function for Optional and its subclasses that only triggers
    when we are not upgrading.

    .. seealso::

        :class:`~.Optional` for more predicates.
    """
    return lambda ctx: not version_utils.is_upgrading(ctx)


def optional_only_upgrade() -> typing.Callable:
    """
    Condition function for Optional and its subclasses that only triggers
    if we are upgrading.

    .. seealso::

        :class:`~.Optional` for more predicates.
    """
    return lambda ctx: version_utils.is_upgrading(ctx)


class Upgrade(Resource):
    """
    Wraps a resource and containing reference to the deployment to be managed.

    :param controller_ref: Reference to the Deployment to be managed
    :param wrapped: The resource, which needs to be reconciled

    This operates by setting a number of replicas to zero in spec of the
    deployment, hence forcing the kubernetes controller to downscale the
    deployment. After that reconcile wrapped resource until it is ready and
    then scale up the managed deployment back.
    """
    def __init__(
            self,
            *,
            wrapped: Resource,
            controller_ref: typing.Optional[Deployment] = None,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._declare_dependencies(*wrapped._dependencies)
        if controller_ref:
            self.controller_ref = controller_ref
        self.wrapped = wrapped
        self.zero_replicas = 0

    def get_listeners(self) -> typing.List[context.Listener]:
        return self.wrapped.get_listeners()

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        super().__set_name__(owner, name)
        self.wrapped.__set_name__(owner, name)

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        if not await self.wrapped.is_ready(ctx):
            ctx.logger.debug(
                "%s is scaling down to zero",
                self.controller_ref,
            )
            await self.controller_ref.scale(ctx, self.zero_replicas)

            if await self.controller_ref.deployment_has_zero_replicas(ctx):
                ctx.logger.debug(
                    "%s replicas are set to zero and %s is reconciling",
                    self.controller_ref, self.wrapped
                )
                await self.wrapped.reconcile(ctx, dependencies=dependencies)

    async def is_ready(self, ctx: context.Context) -> ReadyResult:
        return await self.wrapped.is_ready(ctx)

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a oneshot action.
        """
