#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import abc
import typing
import dataclasses

import kubernetes_asyncio.client as kclient

from .. import (
    api_utils,
    context,
    exceptions,
    interfaces,
    watcher,
)
from .base import (
    DependencyMap,
)
from .k8s import KubernetesReference
from .yaook import YaookResourceReadinessMixin


T = typing.TypeVar("T")
U = typing.TypeVar("U")


class DependencyNotReady(exceptions.ResourceNotPresent):
    pass


class ForeignReference(KubernetesReference[T], metaclass=abc.ABCMeta):
    """
    Reference a kubernetes resource which is not managed by this operator.

    :param resource_interface_factory: Factory to obtain a resource interface
        for the resource type.

    .. note::

        Currently, this class does not support :meth:`get_all` and raises
        :class:`NotImplementedError`, as there is no efficient Implementation
        and we need to find use cases for it before sinking time into making
        it ok.

    This class is abstract. Implementations must overwrite the following
    method:

    .. automethod:: get_resource_references
    """

    def __init__(
            self,
            *,
            resource_interface_factory: typing.Callable[
                [kclient.ApiClient],
                interfaces.ResourceInterface[T],
            ],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._resource_interface_factory = resource_interface_factory

    @abc.abstractmethod
    def get_resource_references(
            self,
            ctx: context.Context,
            ) -> typing.Collection[kclient.V1ObjectReference]:
        """
        Obtain the references to access the foreign resources.

        :param ctx: The context of the operation.
        :raises ResourceNotPresent: If the tracked resource cannot be located.
        :return: A collection of references to the foreign resources.

        The references must have the ``namespace`` and ``name`` attributes set.
        All other attributes are ignored.

        .. note::

            Returning an empty collection is semantically different from
            raising :class:`.ResourceNotPresent`. Returning an empty collection
            is valid for use cases which need :meth:`get_all`, but do not care
            about the specific amount of resources, while raising
            :class:`.ResourceNotPresent` would make those use cases fail.

        """

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        return

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        return self._resource_interface_factory(api_client)

    async def get(
            self,
            ctx: context.Context,
            ) -> kclient.V1ObjectReference:
        references = self.get_resource_references(ctx)
        if not references:
            raise exceptions.ResourceNotPresent(self.component, ctx)
        if len(references) > 1:
            raise exceptions.AmbiguousRequest(self.component, ctx)
        return next(iter(references))

    async def get_all(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference,
            ]:
        raise NotImplementedError

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """


class ReadyForeignReference(
        YaookResourceReadinessMixin,
        ForeignReference[T],
        metaclass=abc.ABCMeta):

    """
    Reference a operator managed custom resource which is not managed by this
    operator. Thie Reference will only return ready once the remote resource
    has been sucessfully reconciled.

    For more details please see :class:`ForeignReference`.
    """

    def __init__(
            self,
            *,
            resource_interface_factory: typing.Callable[
                [kclient.ApiClient],
                interfaces.ResourceInterfaceWithStatus[T],
            ],
            **kwargs: typing.Any,
            ):
        super().__init__(
            resource_interface_factory=resource_interface_factory,
            **kwargs)

    def get_listeners(self) -> typing.List[context.Listener]:
        """
        As a ReadyForeignReference, we need to change all KubernetesListeners
        to broadcast so that we get events despite the fact that our
        CustomResource context labels do not match those of the object that
        created the event.
        """
        listeners = []
        for listener in super().get_listeners():
            if isinstance(listener, context.KubernetesListener):
                listener = dataclasses.replace(listener, broadcast=True)
            listeners.append(listener)
        return listeners


class ForeignResourceDependency(KubernetesReference[T], typing.Generic[T, U]):
    """
    Track a component of a foreign Yaook resource as a dependency.

    :param resource_interface_factory: A factory function returning the
        interface to access tdependency resource.
    :param component: The component name of the dependency to watch.
    :param foreign_resource: The resource reference to the resource to which
        the component belongs.

    The :meth:`reconcile` of this resource does nothing; instead, it checks
    in :meth:`is_ready` whether the dependency is present.

    The dependency is selected based on the labels of the parent
    `foreign_resource` and the given `component`.
    """
    def __init__(
            self,
            *,
            resource_interface_factory: typing.Callable[
                [kclient.ApiClient],
                interfaces.ResourceInterface[T],
            ],
            foreign_component: str,
            foreign_resource: KubernetesReference[U],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._declare_dependencies(foreign_resource)
        self._foreign_resource = foreign_resource
        self._foreign_component = foreign_component
        self._resource_interface_factory = resource_interface_factory

    def _static_labels(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, str]:
        foreign_intf = self._foreign_resource.get_resource_interface(ctx)
        return {
            context.LABEL_PARENT_GROUP: foreign_intf.group,
            context.LABEL_PARENT_PLURAL: foreign_intf.plural,
            context.LABEL_COMPONENT: self._foreign_component,
        }

    async def _labels(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, str]:
        """
        Find the current labels which need to be used to obtain instances of
        the resource.

        .. note::

            In contrast to :meth:`.KubernetesResource.labels`, this method is
            both private and a coroutine.

            This is because it needs to access the Kubernetes API to find the
            current instance of the `foreign_resource`.

        """
        labels: typing.Dict[str, str] = {}
        labels.update(self._static_labels(ctx))
        ref = await self._foreign_resource.get(ctx)
        labels[context.LABEL_PARENT_NAME] = ref.name
        return labels

    async def _get_all(
            self,
            ctx: context.Context,
            ) -> interfaces.ListWrapper[T]:
        try:
            labels = await self._labels(ctx)
        except exceptions.ResourceNotPresent as exc:
            # If the foreign resource is not present (anymore? yet?) we can not
            # (and should not) try to find its product. Hence, treat it as not
            # ready.
            ctx.logger.info(
                "dependency resource not present yet: %s",
                exc,
            )
            raise DependencyNotReady(self.component, ctx)

        intf = self.get_resource_interface(ctx)
        ctx.logger.debug(
            "finding resources of type %s %s by %s",
            intf.plural, intf.api_version,
            labels,
        )
        return await intf.list_(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=labels,
            ).as_api_selector(),
        )

    async def get(
            self,
            ctx: context.Context,
            ) -> kclient.V1ObjectReference:
        items = await self._get_all(ctx)
        if not items:
            raise DependencyNotReady(
                self.component,
                ctx,
            )
        if len(items) > 1:
            raise exceptions.AmbiguousRequest(
                self.component,
                ctx,
            )
        metadata = api_utils.extract_metadata(items[0])
        return kclient.V1ObjectReference(
            name=metadata["name"],
            namespace=metadata["namespace"],
        )

    async def get_all(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference,
            ]:
        items = await self._get_all(ctx)
        item_metadata = list(map(api_utils.extract_metadata, items))
        return {
            metadata.get("labels", {}).get(context.LABEL_INSTANCE, None):
                kclient.V1ObjectReference(
                    name=metadata["name"],
                    namespace=metadata["namespace"],
                )
            for metadata in item_metadata
        }

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        return

    async def is_ready(self, ctx: context.Context) -> bool:
        try:
            await self.get(ctx)
        except exceptions.AmbiguousRequest:
            pass
        except DependencyNotReady:
            return False
        return True

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        return self._resource_interface_factory(api_client)

    def get_broadcast_filter(self) -> typing.Mapping[str, str]:
        if isinstance(self._foreign_resource, KeystoneReference):
            keystone_intf = interfaces.keystonedeployment_interface(None)
            return {
                context.LABEL_PARENT_GROUP: keystone_intf.group,
                context.LABEL_COMPONENT: self._foreign_component,
            }
        foreign_intf = self._foreign_resource._autocreate_resource_interface(
            None)
        return {
            context.LABEL_PARENT_GROUP: foreign_intf.group,
            context.LABEL_PARENT_PLURAL: foreign_intf.plural,
            context.LABEL_COMPONENT: self._foreign_component,
        }

    def get_listeners(
            self,
            ) -> typing.List[context.Listener]:
        # ugh. the api client isn’t needed until you actually do something
        # with the interface, but this is still fugly.
        # we need to convert the functions into classes so that the api
        # endpoint info is reliably available without a client.
        ri = self._resource_interface_factory(typing.cast(
            kclient.ApiClient,
            None,
        ))
        return super().get_listeners() + [
            context.KubernetesListener(
                ri.group, ri.version, ri.plural,
                listener=self._handle_foreign_component_event,
                broadcast=True,
                broadcast_filter=self.get_broadcast_filter()
            ),
        ]

    def _handle_foreign_component_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[T],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            return False
        selector = api_utils.LabelSelector(
            match_labels=self._static_labels(ctx),
        )
        labels = api_utils.extract_metadata(event.object_).get("labels", {})
        return selector.object_matches(labels)

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        """
        This is a no-op, as this class only represents a reference and a
        reference cannot be deleted.
        """


class KeystoneReference(ReadyForeignReference[typing.Mapping]):
    """
    Reference to the default keystone deployment.

    .. note::

        In the future, this will be changed to use a ``keystoneRef`` field in
        the parent spec.
    """

    def __init__(self, **kwargs):
        super().__init__(
            resource_interface_factory=None,
        )

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        raise NotImplementedError()

    def get_resource_interface(
            self,
            ctx: context.Context,
            ) -> interfaces.ResourceInterfaceWithStatus[typing.Mapping]:
        if ctx.parent_spec["keystoneRef"]["kind"] == "KeystoneDeployment":
            return interfaces.keystonedeployment_interface(ctx.api_client)
        elif (ctx.parent_spec["keystoneRef"]["kind"] ==
                "ExternalKeystoneDeployment"):
            return interfaces.externalkeystonedeployment_interface(
                ctx.api_client)
        else:
            raise ValueError(
                "Kind %s is not supported as a keystone deployment" %
                ctx.parent_spec["keystoneRef"]["kind"])

    def get_listeners(
            self,
            ) -> typing.List[context.Listener]:
        # since we have two potential interfaces we need to register both of
        # them here manually and skip the `get_listeners` in
        # `ReadyForeignReference`.
        ri_keystone = interfaces.keystonedeployment_interface(typing.cast(
            kclient.ApiClient,
            None,
        ))
        ri_extkeystone = interfaces.externalkeystonedeployment_interface(
            typing.cast(kclient.ApiClient, None)
        )
        return super(YaookResourceReadinessMixin, self).get_listeners() + [
                context.KubernetesListener(
                    ri_keystone.group, ri_keystone.version, ri_keystone.plural,
                    listener=self._handle_dependency_event,
                    broadcast=True,
                ),
                context.KubernetesListener(
                    ri_extkeystone.group, ri_extkeystone.version,
                    ri_extkeystone.plural,
                    listener=self._handle_dependency_event,
                    broadcast=True,
                ),
        ]

    def get_resource_references(
            self,
            ctx: context.Context,
            ) -> typing.Collection[
                kclient.V1ObjectReference
            ]:
        return [kclient.V1ObjectReference(
            name=ctx.parent_spec["keystoneRef"]["name"],
            namespace=ctx.namespace,
        )]


class NovaReference(ForeignReference):
    def __init__(self, **kwargs):
        super().__init__(
            resource_interface_factory=interfaces.novadeployment_interface,
            **kwargs,
        )

    def get_resource_references(
            self,
            ctx: context.Context,
            ) -> typing.Collection[
                kclient.V1ObjectReference
            ]:
        return [kclient.V1ObjectReference(
            name=ctx.parent_spec["novaRef"]["name"],
            namespace=ctx.namespace,
        )]


class AMQPServerRef(ReadyForeignReference[typing.Mapping]):
    def __init__(self, **kwargs):
        super().__init__(
            resource_interface_factory=interfaces.amqpserver_interface,
            **kwargs
        )

    def get_resource_references(
            self,
            ctx: context.Context) -> typing.Collection[
                kclient.V1ObjectReference
            ]:
        return [
            kclient.V1ObjectReference(
                namespace=ctx.namespace,
                name=ctx.parent_spec["messageQueue"]["amqpServerRef"]["name"],
            ),
        ]


class ExternalSecretReference(KubernetesReference[kclient.V1Secret]):
    """
    Represent a reference to the external defined secret.

    If no external secret is defined, the secret reference will be used.
    This can be used for external optional certificate that can be specified
    in the CRD.
    """

    def __init__(
            self,
            *,
            external_secret: typing.Callable[
                [context.Context], typing.Optional[str]],
            secret_reference: KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(secret_reference)
        self.external_secret = external_secret
        self._secret_reference = secret_reference

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[kclient.V1Secret]:
        return interfaces.secret_interface(api_client)

    async def get(self,
                  ctx: context.Context,
                  ) -> kclient.V1ObjectReference:
        external_certificate = self.external_secret(ctx)
        if external_certificate:
            return kclient.V1ObjectReference(
                namespace=ctx.namespace,
                name=external_certificate,
            )
        else:
            return await self._secret_reference.get(ctx)

    async def get_all(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference,
            ]:
        # NOTE: this makes it unusable for instancing, but we don’t need that
        # at this moment.
        return {
            None: await self.get(ctx),
        }

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        return

    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
    ) -> None:
        pass

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any) -> None:
        pass
