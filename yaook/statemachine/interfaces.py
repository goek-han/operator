#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import enum
import functools
import json
import typing

import asyncio
import random

import kubernetes_asyncio.client as kclient

from . import api_utils


rng = random.SystemRandom()
# Stolen from rand.go in the Kubernetes source code.
ALPHANUMS = "bcdfghjklmnpqrstvwxz2456789"


@api_utils.cached
async def get_kubernetes_version(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> kclient.VersionInfo:
    version_api = kclient.VersionApi(api_client)
    return await version_api.get_code()


def generate_name(body: typing.Mapping[str, typing.Any]) -> None:
    """
    Update the object body to contain a `metadata.name` field.
    If the field is already present the function just returns.

    If the field is missing it is generated based on `metadata.generateName`
    and a random suffix.
    The generated name is then written back to `metadata.name`.

    `metadata.generateName` is truncated to 58 characters to ensure the
    resulting name does not exceed 63 characters
    (This is consistent with the Kubernetes implementation).
    """
    metadata = body["metadata"]
    if "name" not in metadata:
        if "generateName" not in metadata:
            raise ValueError("name or generateName must be set")
        metadata["name"] = metadata["generateName"][:58]
        for _ in range(5):
            metadata["name"] += rng.choice(ALPHANUMS)


T = typing.TypeVar("T")


class ListWrapper(list, typing.MutableSequence[T]):
    resource_version: str

    def __init__(self, init: typing.Iterable[T] = [], *,
                 resource_version: str):
        super().__init__(init)
        self.resource_version = resource_version


def standard_list(
        f: typing.Callable[..., typing.Any],
        ) -> typing.Callable[...,
                             typing.Coroutine[typing.Any, None, ListWrapper]]:
    """
    Wrap a function returning a kubernetes V1List to extract the python list
    of objects.
    """

    @functools.wraps(f)
    async def wrapped(*args: typing.Any,
                      **kwargs: typing.Any) -> ListWrapper:
        listing = (await f(*args, **kwargs))
        return ListWrapper(
            listing.items,
            resource_version=listing.metadata.resource_version,
        )
    return wrapped


def emulated_delete_collection(
        listfn: typing.Callable,
        deletefn: typing.Callable) -> typing.Callable:
    async def delete_collection(
            namespace: str,
            *,
            label_selector: typing.Optional[str] = None) -> None:
        for item in await listfn(namespace, label_selector=label_selector):
            metadata = api_utils.extract_metadata(item)
            await deletefn(metadata["name"], metadata["namespace"])

    return delete_collection


class DeletionPropagationPolicy(enum.Enum):
    """
    The k8s propagation policy to use during the deletion of a resource

    .. autoattribute:: BACKGROUND

    .. autoattribute:: FOREGROUND

    .. note::
        We explicitly do not provide the `Orphan` policy as we do not want to
        keep unused resources around.

    """

    BACKGROUND = "Background"
    """
    Allow the garbage collector to delete the dependents in the background.
    This means that this resource is directly deleted and the dependents are
    cleaned up automatically later.
    """

    FOREGROUND = "Foreground"
    """
    Delete all the dependents in the foreground.
    This means that this resource will only be deleted once all dependents that
    have `blockOwnerDeletion` set are fully removed.
    """


class ResourceInterface(typing.Generic[T]):
    """
    Generic interface to conveniently access kubernetes resources of type `T`.

    :param api_version: The `apiVersion` of the resource to manage.
    :type api_version: :class:`str`
    :param plural: The `plural` of the resource to manage.
    :type plural: :class:`str`

    .. seealso::

        To conveniently create a useful :class:`ResourceInterface`, please
        see:

        .. autosummary::

            config_map_interface
            secret_interface
            pod_interface
            job_interface
            deployment_interface
            stateful_set_interface
            role_interface
            clusterrole_interface
            role_binding_interface
            clusterrole_binding_interface
            service_account_interface
            service_interface
            custom_resource_interface

        For additional methods available if a status subresource exists, see
        :class:`ResourceInterfaceWithStatus`.

    .. automethod:: create

    .. automethod:: read

    .. automethod:: list_

    .. automethod:: patch

    .. automethod:: replace

    .. automethod:: delete

    .. automethod:: delete_collection
    """

    def __init__(self, api_version: str, plural: str,
                 *,
                 createfn: typing.Callable,
                 readfn: typing.Callable,
                 listfn: typing.Callable,
                 patchfn: typing.Callable,
                 replacefn: typing.Callable,
                 deletefn: typing.Callable,
                 deletecollfn: typing.Optional[typing.Callable] = None):
        super().__init__()
        self._api_version = api_version
        self._plural = plural
        api_group, version = api_utils.split_api_version(api_version)
        self._group = api_group
        self._version = version
        if api_group == "":
            self._fqrn = "{}.{}".format(plural, api_version)
        else:
            self._fqrn = "{}/{}".format(plural, api_version)
        self._create = createfn
        self._read = readfn
        self._list = listfn
        self._patch = patchfn
        self._delete = deletefn
        self._replace = replacefn
        if deletecollfn is None:
            deletecollfn = emulated_delete_collection(listfn, deletefn)
        self._deletecollection = deletecollfn

    @property
    def api_version(self) -> str:
        return self._api_version

    @property
    def plural(self) -> str:
        return self._plural

    @property
    def group(self) -> str:
        return self._group

    @property
    def version(self) -> str:
        return self._version

    async def create(self,
                     namespace: str,
                     body: typing.Mapping,
                     field_manager: str,
                     ) -> T:
        """
        Create a resource while taking full Server-Side Apply ownership of all
        initial fields.

        :param intf: The resource interface to use to manage the resource.
        :param namespace: The namespace to create the resource in.
        :param body: The full initial intent for creating the resource.
        :param field_manager: The field manager identity to use for
            Server-Side Apply.
        :return: The fully set up object.

        .. seealso::

            `Server-Side Apply <https://kubernetes.io/docs/reference/using-api/server-side-apply/>`_
            in the Kubernetes documentation.

        We need to use a PATCH call to actually gain `Apply` here.
        (In this case PATCH also serves to create the resource).
        Otherwise we would also get the `Update` ownership of all fields which
        would prevent them from actually being updated by a normal PATCH call.

        As PATCH requires a resource name we can not rely on `generateName` on
        the server side. Therfor we just implement a similar logic as kubernets
        and send already the new name to the server.
        We therfor also need a local guard to ensure we do not accidentially
        overwrite some other object.

        .. note::

            The returned object may include additional fields set *after* creation
            by other controllers.
        """  # NOQA

        for _ in range(10):
            patch_body = copy.deepcopy(body)
            generate_name(patch_body)
            name = patch_body["metadata"]["name"]

            resources = await self.list_(
                namespace,
                field_selector={"metadata.name": name}
            )
            if len(resources) == 0:
                try:
                    patch_body["metadata"]["resourceVersion"] = \
                        resources.resource_version
                    return await self.patch(
                        namespace,
                        name,
                        _encode_body(patch_body),
                        field_manager=field_manager,
                        force=True,
                    )
                except kclient.ApiException as exc:
                    if exc.status != 409:
                        raise
            else:
                # If we are here the name is already in use. If we did specify
                # a `name` in the original body we do not need to retry at all.
                if body.get("metadata", {}).get("name"):
                    raise kclient.ApiException(
                        status=409,
                        reason="resource name %s is already in use" % name)
            # do not hard busy loop this
            await asyncio.sleep(0.1)
        raise Exception("Could not create resource as we were unable to "
                        "generate a unique name")

    async def read(self,
                   namespace: str,
                   name: str) -> T:
        return await self._read(name, namespace)

    async def list_(
            self,
            namespace: str,
            label_selector: typing.Union[
                typing.Mapping[str, str], str, None] = None,
            field_selector: typing.Optional[typing.Mapping[str, str]] = None,
            ) -> ListWrapper[T]:
        kwargs = {}
        if label_selector is not None:
            kwargs["label_selector"] = api_utils.build_selector(
                label_selector
            )
        if field_selector is not None:
            kwargs["field_selector"] = api_utils.build_selector(
                field_selector
            )

        return await self._list(
            namespace,
            **kwargs,
        )

    async def patch(self,
                    namespace: str,
                    name: str,
                    patch: typing.Union[bytes, typing.Mapping, typing.List],
                    **kwargs: typing.Any,
                    ) -> T:
        return await self._patch(name, namespace, patch, **kwargs)

    async def replace(
            self,
            namespace: str,
            name: str,
            body: typing.Union[T, typing.Mapping]) -> T:
        return await self._replace(
            name,
            namespace,
            body,
        )

    async def delete(self,
                     namespace: str,
                     name: str,
                     propagation_policy: DeletionPropagationPolicy =
                     DeletionPropagationPolicy.BACKGROUND) -> None:
        await self._delete(name, namespace,
                           propagation_policy=propagation_policy.value)

    async def delete_collection(
            self,
            namespace: str,
            *,
            label_selector: typing.Union[typing.Mapping[str, str], str, None],
            ) -> None:

        await self._deletecollection(
            namespace,
            label_selector=label_selector,
        )

    def __repr__(self) -> str:
        return "<ResourceInterface {}>".format(self._fqrn)

    async def _clean_managed_fields(self,
                                    current: typing.Any,
                                    namespace: str,
                                    field_manager: str) -> None:
        """
        Get rid of any potential managed field by our fieldmanager for the
        `Update` operation. This is a transition function from our previously
        wrong implementation of `ssa_create` and a workaround for Services.

        :param current: The current state of the resource.
        :param namespace: The namespace of the resource.
        :param field_manager: The field manager we use
        """
        metadata: typing.Mapping[str, typing.Any]
        if isinstance(current, dict):
            metadata = current.get("metadata", {})
        else:
            metadata = api_utils.k8s_obj_to_yaml_data(current.metadata)
        managed_fields = metadata.get("managedFields")
        needs_cleaning_index = None
        if not managed_fields:
            return
        for i, field in enumerate(managed_fields):
            if field["manager"] == field_manager and \
                    field["operation"] == "Update":
                needs_cleaning_index = i
                break
        else:
            return

        name = metadata["name"]
        # Yes this is ugly
        # It also does not conform to the explanation in the docs here:
        # https://kubernetes.io/docs/reference/using-api/server-side-apply/#clearing-managedfields  # noqa: E501
        # but we actually only need to get rid of the managedFields
        # ownership for this specific fieldmanager and operation.
        # If we use the advertised method we would get a new fieldmanager
        # named `before-first-apply` for the Update operation and therefor
        # would not have acceived any difference
        await self.patch(
            namespace,
            name,
            [{"op": "remove",
                "path": f"/metadata/managedFields/{needs_cleaning_index}",
              }],
        )


class ClusterResourceInterface(ResourceInterface[T]):
    """
    Extension of :class:`ResourceInterface`. For global Interfaces
    the namespaces should not be returned.

    .. automethod:: list_

    .. automethod:: patch

    .. automethod:: delete_collection
    """
    async def list_(
            self,
            namespace: str,
            label_selector: typing.Union[
                typing.Mapping[str, str], str, None] = None,
            field_selector: typing.Optional[typing.Mapping[str, str]] = None,
            ) -> ListWrapper[T]:
        kwargs = {}
        if label_selector is not None:
            kwargs["label_selector"] = api_utils.build_selector(
                label_selector
            )
        if field_selector is not None:
            kwargs["field_selector"] = api_utils.build_selector(
                field_selector
            )

        return await self._list(**kwargs,)

    async def patch(self,
                    namespace: str,
                    name: str,
                    patch: typing.Union[bytes, typing.Mapping, typing.List],
                    **kwargs: typing.Any,
                    ) -> T:
        return await self._patch(name, patch, **kwargs)

    async def delete_collection(
            self,
            namespace: str,
            *,
            label_selector: typing.Union[typing.Mapping[str, str], str, None],
            ) -> None:

        await self._deletecollection(
                label_selector=label_selector,
            )


class ResourceInterfaceWithStatus(ResourceInterface[T]):
    """
    Extension of :class:`ResourceInterface` with the ``/status`` subresource.

    .. automethod:: read_status

    .. automethod:: patch_status

    .. automethod:: replace_status
    """
    def __init__(self,
                 api_version, plural,
                 *,
                 readstatusfn, patchstatusfn, replacestatusfn,
                 **kwargs):
        super().__init__(api_version, plural, **kwargs)
        self._readstatus = readstatusfn
        self._patchstatus = patchstatusfn
        self._replacestatus = replacestatusfn

    async def read_status(
            self,
            namespace: str,
            name: str) -> T:
        return await self._readstatus(
            name,
            namespace,
        )

    async def patch_status(
            self,
            namespace: str,
            name: str,
            patch: typing.Union[bytes, typing.Mapping, typing.List],
            **kwargs: typing.Any
            ) -> typing.Mapping:
        return await self._patchstatus(name, namespace, patch, **kwargs)

    async def replace_status(
            self,
            namespace: str,
            name: str,
            body: typing.Union[T, typing.Mapping]) -> T:
        return await self._replacestatus(
            name,
            namespace,
            body,
        )


def config_map_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[kclient.V1ConfigMap]:
    """
    Create interface to access configmaps.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1ConfigMap`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CoreV1Api(api_client)

    return ResourceInterface(
        "v1", "configmaps",
        createfn=api.create_namespaced_config_map,
        readfn=api.read_namespaced_config_map,
        listfn=standard_list(api.list_namespaced_config_map),
        patchfn=api.patch_namespaced_config_map,
        replacefn=api.replace_namespaced_config_map,
        deletefn=api.delete_namespaced_config_map,
        deletecollfn=api.delete_collection_namespaced_config_map,
    )


def secret_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[kclient.V1Secret]:
    """
    Create interface to access secrets.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Secret`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CoreV1Api(api_client)

    return ResourceInterface(
        "v1", "secrets",
        createfn=api.create_namespaced_secret,
        readfn=api.read_namespaced_secret,
        listfn=standard_list(api.list_namespaced_secret),
        patchfn=api.patch_namespaced_secret,
        replacefn=api.replace_namespaced_secret,
        deletefn=api.delete_namespaced_secret,
        deletecollfn=api.delete_collection_namespaced_secret,
    )


def pod_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[kclient.V1Pod]:
    """
    Create interface to access pods.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Pod`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CoreV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "v1", "pods",
        createfn=api.create_namespaced_pod,
        readfn=api.read_namespaced_pod,
        listfn=standard_list(api.list_namespaced_pod),
        patchfn=api.patch_namespaced_pod,
        replacefn=api.replace_namespaced_pod,
        deletefn=api.delete_namespaced_pod,
        deletecollfn=api.delete_collection_namespaced_pod,
        readstatusfn=api.read_namespaced_pod_status,
        patchstatusfn=api.patch_namespaced_pod_status,
        replacestatusfn=api.replace_namespaced_pod_status,
    )


def role_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[kclient.V1Role]:
    """
    Create interface to access roles.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Role`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.RbacAuthorizationV1Api(api_client)

    return ResourceInterface(
        "rbac.authorization.k8s.io/v1", "roles",
        createfn=api.create_namespaced_role,
        readfn=api.read_namespaced_role,
        listfn=standard_list(api.list_namespaced_role),
        patchfn=api.patch_namespaced_role,
        replacefn=api.replace_namespaced_role,
        deletefn=api.delete_namespaced_role,
        deletecollfn=api.delete_collection_namespaced_role,
    )


def clusterrole_interface(
    api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ClusterResourceInterface[kclient.V1ClusterRole]:
    """
    Create interface to access cluster roles.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1ClusterRole`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.RbacAuthorizationV1Api(api_client)

    return ClusterResourceInterface(
        "rbac.authorization.k8s.io/v1", "clusterroles",
        createfn=api.create_cluster_role,
        readfn=api.read_cluster_role,
        listfn=standard_list(api.list_cluster_role),
        patchfn=api.patch_cluster_role,
        replacefn=api.replace_cluster_role,
        deletefn=api.delete_cluster_role,
        deletecollfn=api.delete_collection_cluster_role,
    )


def role_binding_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[kclient.V1RoleBinding]:
    """
    Create interface to access role bindings.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1RoleBinding`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.RbacAuthorizationV1Api(api_client)

    return ResourceInterface(
        "rbac.authorization.k8s.io/v1", "rolebindings",
        createfn=api.create_namespaced_role_binding,
        readfn=api.read_namespaced_role_binding,
        listfn=standard_list(api.list_namespaced_role_binding),
        patchfn=api.patch_namespaced_role_binding,
        replacefn=api.replace_namespaced_role_binding,
        deletefn=api.delete_namespaced_role_binding,
        deletecollfn=api.delete_collection_namespaced_role_binding,
    )


def clusterrole_binding_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ClusterResourceInterface[kclient.V1ClusterRoleBinding]:
    """
    Create interface to access clusterrole bindings.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1ClusterRoleBinding`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.RbacAuthorizationV1Api(api_client)

    return ClusterResourceInterface(
        "rbac.authorization.k8s.io/v1", "clusterrolebindings",
        createfn=api.create_cluster_role_binding,
        readfn=api.read_cluster_role_binding,
        listfn=standard_list(api.list_cluster_role_binding),
        patchfn=api.patch_cluster_role_binding,
        replacefn=api.replace_cluster_role_binding,
        deletefn=api.delete_cluster_role_binding,
        deletecollfn=api.delete_collection_cluster_role_binding,
    )


def service_account_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[kclient.V1ServiceAccount]:
    """
    Create interface to access service accounts.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1ServiceAccount`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CoreV1Api(api_client)

    return ResourceInterface(
        "v1", "serviceaccounts",
        createfn=api.create_namespaced_service_account,
        readfn=api.read_namespaced_service_account,
        listfn=standard_list(api.list_namespaced_service_account),
        patchfn=api.patch_namespaced_service_account,
        replacefn=api.replace_namespaced_service_account,
        deletefn=api.delete_namespaced_service_account,
        deletecollfn=api.delete_collection_namespaced_service_account,
    )


class ServiceInterface(ResourceInterfaceWithStatus):
    """
    Custom interface for services.
    For details please see :meth:`service_interface`
    """
    def __init__(self, api_version, plural, api_client, **kwargs):
        super().__init__(api_version, plural,  **kwargs)
        self._api_client = api_client

    async def create(self,
                     namespace: str,
                     body: typing.Mapping,
                     field_manager: str,
                     ) -> T:
        k8s_version = await get_kubernetes_version(self._api_client)
        if int(k8s_version.minor) >= 23:
            return await super().create(namespace, body, field_manager)

        # The following is the original ssa_create implementation
        # (including its issues)
        created = await self._create(namespace, body,
                                     field_manager=field_manager)
        metadata = api_utils.extract_metadata(created)
        name = metadata["name"]
        uid = metadata["uid"]
        while True:
            patch_body = copy.deepcopy(body)
            patch_body["metadata"]["resourceVersion"] = \
                metadata["resourceVersion"]
            patch_body["metadata"]["name"] = name
            try:
                await self.patch(
                    namespace,
                    name,
                    _encode_body(patch_body),
                    field_manager=field_manager,
                    force=True,
                )
                break
            except kclient.ApiException as exc:
                if exc.status != 409:
                    raise
                pass
            # do not hard busy loop this
            await asyncio.sleep(0.1)
            metadata = api_utils.extract_metadata(
                await self.read(namespace, name)
            )
            if metadata["uid"] != uid:
                raise kclient.ApiException(
                    status=409,
                    reason="create/delete conflict: "
                    "got different uid during readback",
                )

        # We now fix the issue of the original ssa_create implementation
        # by patching out the Update managed Field
        current = await self.read(namespace, name)
        await self._clean_managed_fields(
            current, namespace, field_manager)

        return await self.read(namespace, name)


def service_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[kclient.V1Service]:
    """
    Create interface to access services.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Service`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.

    .. note::
        Because of https://github.com/kubernetes/kubernetes/issues/106745 we
        need to handle the creation of services differently before k8s 1.23.
        This special case can be removed once our minimum supported kubernetes
        version is 1.23.
    """
    api = kclient.CoreV1Api(api_client)

    return ServiceInterface(
        "v1", "services",
        createfn=api.create_namespaced_service,
        readfn=api.read_namespaced_service,
        listfn=standard_list(api.list_namespaced_service),
        replacefn=api.replace_namespaced_service,
        patchfn=api.patch_namespaced_service,
        deletefn=api.delete_namespaced_service,
        readstatusfn=api.read_namespaced_service_status,
        patchstatusfn=api.patch_namespaced_service_status,
        replacestatusfn=api.replace_namespaced_service_status,
        api_client=api_client,
    )


def job_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[kclient.V1Job]:
    """
    Create interface to access jobs.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Job`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.BatchV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "batch/v1", "jobs",
        createfn=api.create_namespaced_job,
        readfn=api.read_namespaced_job,
        listfn=standard_list(api.list_namespaced_job),
        patchfn=api.patch_namespaced_job,
        replacefn=api.replace_namespaced_job,
        deletefn=api.delete_namespaced_job,
        deletecollfn=api.delete_collection_namespaced_job,
        readstatusfn=api.read_namespaced_job_status,
        patchstatusfn=api.patch_namespaced_job_status,
        replacestatusfn=api.replace_namespaced_job_status,
    )


def cronjob_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.V1beta1CronJob
            ]:
    """
    Create interface to access cronjobs.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1beta1CronJob`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.BatchV1beta1Api(api_client)

    return ResourceInterfaceWithStatus(
        "batch/v1beta1", "cronjobs",
        createfn=api.create_namespaced_cron_job,
        readfn=api.read_namespaced_cron_job,
        listfn=standard_list(api.list_namespaced_cron_job),
        patchfn=api.patch_namespaced_cron_job,
        replacefn=api.replace_namespaced_cron_job,
        deletefn=api.delete_namespaced_cron_job,
        deletecollfn=api.delete_collection_namespaced_cron_job,
        readstatusfn=api.read_namespaced_cron_job_status,
        patchstatusfn=api.patch_namespaced_cron_job_status,
        replacestatusfn=api.replace_namespaced_cron_job_status,
    )


def deployment_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.V1Deployment]:
    """
    Create interface to access deployments.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1Deployment`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.AppsV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "apps/v1", "deployments",
        createfn=api.create_namespaced_deployment,
        readfn=api.read_namespaced_deployment,
        listfn=standard_list(api.list_namespaced_deployment),
        patchfn=api.patch_namespaced_deployment,
        replacefn=api.replace_namespaced_deployment,
        deletefn=api.delete_namespaced_deployment,
        deletecollfn=api.delete_collection_namespaced_deployment,
        readstatusfn=api.read_namespaced_deployment_status,
        patchstatusfn=api.patch_namespaced_deployment_status,
        replacestatusfn=api.replace_namespaced_deployment_status,
    )


def stateful_set_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.V1StatefulSet]:
    """
    Create interface to access stateful sets.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1StatefulSet`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.AppsV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "apps/v1", "statefulsets",
        createfn=api.create_namespaced_stateful_set,
        readfn=api.read_namespaced_stateful_set,
        listfn=standard_list(api.list_namespaced_stateful_set),
        patchfn=api.patch_namespaced_stateful_set,
        replacefn=api.replace_namespaced_stateful_set,
        deletefn=api.delete_namespaced_stateful_set,
        deletecollfn=api.delete_collection_namespaced_stateful_set,
        readstatusfn=api.read_namespaced_stateful_set_status,
        patchstatusfn=api.patch_namespaced_stateful_set_status,
        replacestatusfn=api.replace_namespaced_stateful_set_status,
    )


def ingress_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.NetworkingV1Api]:
    """
    Create interface to access ingresses.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.NetworkingV1Api`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.NetworkingV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "networking.k8s.io/v1", "ingresses",
        createfn=api.create_namespaced_ingress,
        readfn=api.read_namespaced_ingress,
        listfn=standard_list(api.list_namespaced_ingress),
        patchfn=api.patch_namespaced_ingress,
        replacefn=api.replace_namespaced_ingress,
        deletefn=api.delete_namespaced_ingress,
        deletecollfn=api.delete_collection_namespaced_ingress,
        readstatusfn=api.read_namespaced_ingress_status,
        patchstatusfn=api.patch_namespaced_ingress_status,
        replacestatusfn=api.replace_namespaced_ingress_status,
    )


def persistent_volume_claim_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.V1PersistentVolumeClaim]:
    """
    Create interface to access persistentvolumeclaims.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1PersistentVolumeClaim`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CoreV1Api(api_client)

    return ResourceInterfaceWithStatus(
        "v1", "persistentvolumeclaims",
        createfn=api.create_namespaced_persistent_volume_claim,
        readfn=api.read_namespaced_persistent_volume_claim,
        listfn=standard_list(api.list_namespaced_persistent_volume_claim),
        patchfn=api.patch_namespaced_persistent_volume_claim,
        replacefn=api.replace_namespaced_persistent_volume_claim,
        deletefn=api.delete_namespaced_persistent_volume_claim,
        deletecollfn=api.delete_collection_namespaced_persistent_volume_claim,
        readstatusfn=api.read_namespaced_persistent_volume_claim_status,
        patchstatusfn=api.patch_namespaced_persistent_volume_claim_status,
        replacestatusfn=api.replace_namespaced_persistent_volume_claim_status,
    )


def pod_disruption_budget_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[
            kclient.V1beta1PodDisruptionBudget]:
    """
    Create interface to access persistentvolumeclaims.

    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for
        :class:`kubernetes_asyncio.client.V1beta1PodDisruptionBudget`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.PolicyV1beta1Api(api_client)

    return ResourceInterfaceWithStatus(
        "policy/v1beta1", "poddisruptionbudgets",
        createfn=api.create_namespaced_pod_disruption_budget,
        readfn=api.read_namespaced_pod_disruption_budget,
        listfn=standard_list(api.list_namespaced_pod_disruption_budget),
        patchfn=api.patch_namespaced_pod_disruption_budget,
        replacefn=api.replace_namespaced_pod_disruption_budget,
        deletefn=api.delete_namespaced_pod_disruption_budget,
        deletecollfn=api.delete_collection_namespaced_pod_disruption_budget,
        readstatusfn=api.read_namespaced_pod_disruption_budget_status,
        patchstatusfn=api.patch_namespaced_pod_disruption_budget_status,
        replacestatusfn=api.replace_namespaced_pod_disruption_budget_status,
    )


def custom_resource_interface(
        api_group: str,
        api_version: str,
        plural: str,
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    """
    Create interface to access custom objects.

    :param api_group: The API group of the custom resource.
    :type api_group: :class:`str`
    :param api_version: The API version of the custom resource.
    :type api_version: :class:`str`
    :param plural: The plural of the custom resource.
    :type plural: :class:`str`
    :param api_client: The client to use.
    :type api_client: :class:`kubernetes_asyncio.client.ApiClient` or
        :data:`None`
    :rtype: :class:`ResourceInterface` for :class:`dict`

    If the `api_client` is :data:`None`, a new client with the default
    global configuration is used.
    """
    api = kclient.CustomObjectsApi(api_client)

    @functools.wraps(api.create_namespaced_custom_object)
    async def create(namespace: str, body: typing.Mapping,
                     **kwargs: typing.Any) -> typing.Mapping:
        return await api.create_namespaced_custom_object(
            api_group, api_version, namespace, plural, body,
            **kwargs,
        )

    @functools.wraps(api.get_namespaced_custom_object)
    async def read(name: str, namespace: str,
                   **kwargs: typing.Any) -> typing.Mapping:
        return await api.get_namespaced_custom_object(
            api_group, api_version, namespace, plural, name,
            **kwargs,
        )

    @functools.wraps(api.list_namespaced_custom_object)
    async def list_(namespace: str,
                    **kwargs: typing.Any) -> ListWrapper:
        listing = await api.list_namespaced_custom_object(
            api_group, api_version, namespace, plural,
            **kwargs,
        )
        return ListWrapper(
            listing["items"],
            resource_version=listing["metadata"]["resourceVersion"],
        )

    @functools.wraps(api.patch_namespaced_custom_object)
    async def patch(name: str, namespace: str,
                    patch: typing.Union[typing.Mapping, typing.Sequence],
                    **kwargs: typing.Any) -> typing.Mapping:
        return await api.patch_namespaced_custom_object(
            api_group, api_version, namespace, plural,
            name, patch, **kwargs,
        )

    @functools.wraps(api.replace_namespaced_custom_object)
    async def replace(name: str, namespace: str,
                      body: typing.Mapping,
                      **kwargs: typing.Any) -> typing.Mapping:
        return await api.replace_namespaced_custom_object(
            api_group, api_version, namespace, plural,
            name, body, **kwargs,
        )

    @functools.wraps(api.delete_namespaced_custom_object)
    async def delete(name: str, namespace: str,
                     **kwargs: typing.Any) -> None:
        return await api.delete_namespaced_custom_object(
            api_group, api_version, namespace, plural,
            name, **kwargs,
        )

    @functools.wraps(api.get_namespaced_custom_object_status)
    async def readstatus(name, namespace, **kwargs):
        return await api.get_namespaced_custom_object_status(
            api_group, api_version, namespace, plural, name,
            **kwargs,
        )

    @functools.wraps(api.patch_namespaced_custom_object_status)
    async def patchstatus(name, namespace, patch, **kwargs):
        return await api.patch_namespaced_custom_object_status(
            api_group, api_version, namespace, plural,
            name, patch, **kwargs,
        )

    @functools.wraps(api.replace_namespaced_custom_object_status)
    async def replacestatus(name, namespace, body, **kwargs):
        return await api.replace_namespaced_custom_object_status(
            api_group, api_version, namespace, plural,
            name, body, **kwargs,
        )

    # The k8s-API does not support delete_collection_* with label_selector for
    # custom objects, thus we need to use the emulation by not passing a
    # deletecollfn to the ResourceInterface ctor.
    return ResourceInterfaceWithStatus(
        api_utils.join_api_version(api_group, api_version),
        plural,
        createfn=create,
        readfn=read,
        listfn=list_,
        patchfn=patch,
        replacefn=replace,
        deletefn=delete,
        readstatusfn=readstatus,
        patchstatusfn=patchstatus,
        replacestatusfn=replacestatus,
    )


def configured_daemon_set_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[typing.Mapping]:
    return custom_resource_interface(
        "apps.yaook.cloud", "v1", "configureddaemonsets",
        api_client,
    )


def keystonedeployment_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "keystonedeployments",
        api_client,
    )


def externalkeystonedeployment_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "externalkeystonedeployments",
        api_client,
    )


def keystoneuser_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "keystoneusers",
        api_client,
    )


def keystoneendpoint_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "keystoneendpoints",
        api_client,
    )


def neutron_dhcp_agent_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "network.yaook.cloud", "v1", "neutrondhcpagents",
        api_client,
    )


def neutron_l2_agent_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "network.yaook.cloud", "v1", "neutronl2agents",
        api_client,
    )


def neutron_l3_agent_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "network.yaook.cloud", "v1", "neutronl3agents",
        api_client,
    )


def neutron_ovn_agent_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "network.yaook.cloud", "v1", "neutronovnagents",
        api_client,
    )


def neutron_bgp_dragent_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "network.yaook.cloud", "v1", "neutronbgpdragents",
        api_client,
    )


def novadeployment_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "novadeployments",
        api_client,
    )


def nova_compute_node_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "compute.yaook.cloud", "v1", "novacomputenodes",
        api_client,
    )


def nova_compute_host_aggregate_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "compute.yaook.cloud", "v1", "novahostaggregates",
        api_client,
    )


def mysqlservice_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "mysqlservices",
        api_client,
    )


def mysqluser_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "mysqlusers",
        api_client,
    )


def amqpserver_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "amqpservers",
        api_client,
    )


def amqpuser_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "amqpusers",
        api_client,
    )


def memcachedservice_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "memcachedservices",
        api_client,
    )


def ovsdbservice_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "infra.yaook.cloud", "v1", "ovsdbservices",
        api_client,
    )


def certificates_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "cert-manager.io", "v1", "certificates",
        api_client,
    )


def issuer_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "cert-manager.io", "v1", "issuers",
        api_client,
    )


def sshidentity_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterface[typing.Mapping]:
    return custom_resource_interface(
        "compute.yaook.cloud", "v1", "sshidentities",
        api_client,
    )


def gnocchideployment_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "gnocchideployments",
        api_client,
    )


def servicemonitor_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "monitoring.coreos.com", "v1", "servicemonitors",
        api_client,
    )


def yaookdisruptionbudget_interface(
        api_client: typing.Optional[kclient.ApiClient] = None
        ) -> ResourceInterfaceWithStatus[typing.Mapping]:
    return custom_resource_interface(
        "yaook.cloud", "v1", "yaookdisruptionbudgets",
        api_client,
    )


async def get_selected_nodes_union(
        api_client: kclient.ApiClient,
        selectors: typing.Iterable[api_utils.LabelSelector],
        ) -> typing.Sequence[kclient.V1Node]:
    v1 = kclient.CoreV1Api(api_client)
    node_set = {}
    node: kclient.V1Node
    async for node in api_utils.multi_selector_list(
            standard_list(v1.list_node),
            selectors):
        node_set[node.metadata.name] = node
    return list(node_set.values())


def _encode_body(body: typing.Mapping[str, typing.Any]) -> bytes:
    return json.dumps(body).encode("utf-8")
