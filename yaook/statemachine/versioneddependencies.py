#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.versioneddependencies` – Manage the versions
    of external dependencies
##################################################################

This module contains all code necessary to manage externaly versioned
dependencies. This includes our self-built docker images but also external
things like Helm Charts or possibly even Kubernetes version.
"""

import abc
import asyncio
import base64
import functools
import environ
import json
import typing
import pathlib
import yaml
import yaml.scanner

import dxf
import semver

from . import context


YAOOK_IMAGES_REGISTRY = "registry.gitlab.com/yaook/images"
PIN_FILE = (pathlib.Path(__file__).parent.parent / "assets" /
            "pinned_version.yml")


@functools.lru_cache(maxsize=1)
def _load_pin_file() -> typing.Mapping[str, str]:
    try:
        f = open(PIN_FILE)
    except (IsADirectoryError, FileNotFoundError):
        return {}
    with f:
        return yaml.safe_load(f) or {}


@functools.lru_cache(maxsize=1)
def _get_override_versions() -> typing.Mapping[str, str]:
    """
    Return: a dictionary described by YAOOK_OP_VERSIONS_OVERRIDE.
    """
    config = environ.to_config(VersionsConfig)
    overrides: typing.Dict[str, str] = {}
    if not config.override:
        return overrides
    try:
        overrides = yaml.safe_load(config.override)
        if not isinstance(overrides, dict):
            raise ValueError(
                "provided version override environment "
                "variable is not a valid dictionary"
            )
    except yaml.scanner.ScannerError as e:
        raise ValueError(
            "Provided version override environment variable is not "
            "valid yaml: %s" % e
            )
    return overrides


@functools.lru_cache(maxsize=1)
def _get_registry_overrides() -> typing.Mapping[str, str]:
    """
    Return: a dictionary described by YAOOK_OP_DOCKER_REGISTRY_OVERRIDE.
    """
    config = environ.to_config(DockerConfig)
    overrides: typing.Dict[str, str] = {}
    if not config.registry_override:
        return overrides
    try:
        overrides = yaml.safe_load(config.registry_override)
        if not isinstance(overrides, dict):
            raise ValueError(
                "provided image registry override environment "
                "variable is not a valid dictionary"
            )
    except yaml.scanner.ScannerError as e:
        raise ValueError(
            "Provided image registry override environment "
            "variable is not valid yaml: %s" % e
            )

    return overrides


T = typing.TypeVar("T")


class VersionFormat(typing.Generic[T], metaclass=abc.ABCMeta):
    """
    Parse and format a specific kind of version number.
    """

    @abc.abstractmethod
    def parse(self, version: str) -> T:
        pass

    @abc.abstractmethod
    def serialize(self, value: T) -> str:
        pass


class SemVer(VersionFormat[semver.VersionInfo]):
    def __init__(
            self,
            prefix: typing.Optional[str] = None,
            suffix: typing.Optional[str] = None):
        self.prefix = prefix
        self.suffix = suffix

    def parse(self, version: str) -> T:
        if self.prefix:
            if version.startswith(self.prefix):
                version = version[len(self.prefix):]
            else:
                raise ValueError("Missing prefix")
        if self.suffix:
            if version.endswith(self.suffix):
                version = version[:-len(self.suffix)]
            else:
                raise ValueError("Missing suffix")
        return semver.VersionInfo.parse(version)

    def serialize(self, value: semver.VersionInfo) -> str:
        parts = [str(value)]
        if self.prefix:
            parts.insert(0, self.prefix)
        if self.suffix:
            parts.append(self.suffix)
        return "".join(parts)


SoftwareBuildVersionWrapper = typing.Tuple[semver.VersionInfo,
                                           semver.VersionInfo]


class BitnamiVersion(VersionFormat[SoftwareBuildVersionWrapper]):
    def parse(self, version: str) -> SoftwareBuildVersionWrapper:
        semver_s, os, os_version_s, image_release_s = version.split("-")
        if os != "debian":
            raise ValueError("only debian is supported")
        if not image_release_s.startswith("r"):
            raise ValueError("image release must start mit r")
        image_release = int(image_release_s[1:])
        os_version = int(os_version_s)
        semver_software = semver.VersionInfo.parse(semver_s)
        return semver_software, semver.VersionInfo(os_version, image_release)

    def serialize(self, value: SoftwareBuildVersionWrapper) -> str:
        semver_software, semver_os = value
        return f"{semver_software}-debian-{semver_os.major}-r{semver_os.minor}"


class OVNVersion(VersionFormat[SoftwareBuildVersionWrapper]):
    def parse(self, version: str) -> SoftwareBuildVersionWrapper:
        # feature branch versions have a "-" in image_release_s, so split only
        # at first "-". But they will still get ignored, as thats no valid
        # semver.VersionInfo.
        semver_s, image_release_s = version.split("-", 1)
        semver_software = semver.VersionInfo(*semver_s.strip("v").split("."))
        image_release = semver.VersionInfo.parse(image_release_s)
        return semver_software, image_release

    def serialize(self, value: SoftwareBuildVersionWrapper) -> str:
        semver_software, image_release = value
        minor = semver_software.minor
        if semver_software.minor < 10:
            minor = f"0{semver_software.minor}"
        return f"v{semver_software.major}.{minor}.{semver_software.patch}" \
            f"-{image_release}"


class VersionSelector(typing.Generic[T], metaclass=abc.ABCMeta):
    """
    Class which defines a selector for versions of different kinds.
    """
    version_format: VersionFormat

    @abc.abstractmethod
    def selects_version(self, version: T) -> bool:
        pass

    def filter(self, versions: typing.Iterable[T]) -> typing.Iterable[T]:
        return filter(self.selects_version, versions)


class SemVerSelector(VersionSelector[semver.VersionInfo]):
    def __init__(self, requirements: typing.Collection[str] = [],
                 prefix: typing.Optional[str] = None,
                 suffix: typing.Optional[str] = None):
        super().__init__()
        self._requirements = list(requirements)
        self.version_format = SemVer(prefix=prefix, suffix=suffix)

    def selects_version(self, version: semver.VersionInfo) -> bool:
        if version.prerelease:
            return False
        return all(version.match(req) for req in self._requirements)


@environ.config(prefix="YAOOK_OP_VERSIONS")
class VersionsConfig:
    use_alpha = environ.bool_var(False)
    use_rolling = environ.bool_var(False)
    override = environ.var("")


class YaookSemVerSelector(VersionSelector[semver.VersionInfo]):
    def __init__(self, requirements: typing.Collection[str] = []):
        super().__init__()
        self._requirements = list(requirements)
        self.version_format = SemVer()

    def selects_version(self, version: semver.VersionInfo) -> bool:
        if version.prerelease:
            versionsconfig = environ.to_config(VersionsConfig)
            if version.prerelease.startswith("alpha"):
                if not versionsconfig.use_alpha:
                    return False
            elif version.prerelease.startswith("rolling"):
                if not versionsconfig.use_rolling:
                    return False
            else:
                return False

        return all(version.match(req) for req in self._requirements)


class BitnamiVersionSelector(VersionSelector[SoftwareBuildVersionWrapper]):
    def __init__(
            self,
            requirements: typing.Collection[
                typing.Tuple[typing.Collection[str], typing.Collection[str]]
            ],
            ):
        self._requirements = list(
            (list(software_reqs), list(osimage_reqs))
            for software_reqs, osimage_reqs in requirements
        )
        self.version_format = BitnamiVersion()

    def selects_version(self, version: SoftwareBuildVersionWrapper) -> bool:
        semver_software, semver_osimage = version
        return any(
            all(semver_software.match(software_req)
                for software_req in software_reqs) and
            all(semver_osimage.match(osimage_req)
                for osimage_req in osimage_reqs)
            for software_reqs, osimage_reqs in self._requirements
        )


class OVNVersionSelector(VersionSelector[SoftwareBuildVersionWrapper]):
    def __init__(
            self,
            requirements: typing.Tuple[
                typing.Collection[str], typing.Collection[str]
            ] = ([], []),
            ):
        software_reqs, osimage_reqs = requirements
        self._requirements = (list(software_reqs), list(osimage_reqs))
        self.version_format = OVNVersion()

    def selects_version(self, version: SoftwareBuildVersionWrapper) -> bool:
        semver_software, image_release = version
        software_reqs, image_reqs = self._requirements

        return (
            all(semver_software.match(software_req)
                for software_req in software_reqs) and
            all(image_release.match(image_req)
                for image_req in image_reqs)
        )


class VersionedDependency(typing.Generic[T], metaclass=abc.ABCMeta):
    """
    Abstract base class to represent a versioned and possibly external
    dependency.

    :param url: Unique, identifying URL for the external dependency.
    :param version_selector: Selector to find appropriate versions.

    The logic of the version selection is delegated to `version_selector`,
    which is a :class:`VersionSelector` instance.

    This class is able to automatically detect and load pinning files for
    the dependencies. If such a file is present it overwrites automatic
    version logic. This is used stay at a specific version during development
    and operations.

    In addition `VersionsConfig.override` may contain a valid yaml file.
    This environmentvariable-yaml allows the user to override the entire URL.
    This is required in scenarios when a specific image has a different
    version or is living in another registry.

    .. automethod:: get_latest_version

    .. automethod:: get_versioned_url

    **Interface for subclasses**:

    .. automethod:: _get_available_versions

    .. automethod:: _get_versioned_url
    """

    def __init__(self,
                 url: str,
                 version_selector: VersionSelector[T]):
        self.url = url
        self.version_selector = version_selector
        self.pinned_version: typing.Optional[str] = \
            _load_pin_file().get(self.url)

    @property
    def pin_key(self):
        """
        The name to use to pin the version in pinned_version.yaml.
        """
        return self.url

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        self.name = name

    async def _get_valid_versions(self) -> typing.List[T]:
        available_versions = await self._get_available_versions()
        versions = []
        for version_s in available_versions:
            try:  # nosemgrep this is intended
                version = self.version_selector.version_format.parse(version_s)
            except ValueError:
                continue
            versions.append(version)
        return versions

    async def get_latest_version(self, ctx: context.Context,
                                 ignore_pin: bool = False) -> str:
        """
        Gets the latest version of this dependency. If the version is
        pinned this method will return the pinned version.
        If `version_requirements` are specified in the constructor they will
        be validated here. After the version discovery the version will be
        pinned for the lifetime of the program.

        :param ctx: The context of the current request

        :param ignore_pin: If set to true then the version will be
            autodiscovered even if it would be pinned normally.
            Setting this will also overwrite the currently pinned version with
            whatever version is the latest right now.

        :raises KeyError: If no version matched the requirements specified
            in the constructor.
        """
        if self.pinned_version is not None and not ignore_pin:
            return self.pinned_version

        versions = await self._get_valid_versions()

        versions.sort(reverse=True)

        for version in self.version_selector.filter(versions):
            version_s = self.version_selector.version_format.serialize(
                version,
            )
            self.pinned_version = version_s
            return version_s

        raise KeyError(
            f"No version matching requirements for '{self.url}' found")

    @abc.abstractmethod
    async def _get_available_versions(self) -> typing.List[str]:
        """
        Return: A list of version strings available for the `self.url`
            resource. This list should contain all possible versions
            even if some of them don't conform to SemVer. The non-conforming
            ones are simply removed later.
        """
        raise NotImplementedError()

    def _get_override_version(self) -> typing.Optional[str]:
        """
        Return: a string to be returned by `get_versioned_url` or None.
        """
        overrides = _get_override_versions()
        return overrides.get(self.url)

    async def get_versioned_url(self, ctx: context.Context) -> str:
        """
        Get a useable versioned url that can be used to access whatever this
        dependency is refering to.

        :param ctx: The context of the current request
        """
        override = self._get_override_version()
        if override is not None:
            return override
        return await self._get_versioned_url(ctx)

    @abc.abstractclassmethod
    async def _get_versioned_url(self, ctx: context.Context) -> str:
        """
        Return: A string of the dependency along with it's version that is
            usefull for further processing (e.g. `self.url:version` for docker
            images).
        """
        raise NotImplementedError()


class ReleaseAwareVersionedDependency(VersionedDependency):
    """
    Class that allows selecting a versioned dependency based on the
    current release we are running.
    """

    def __init__(self,
                 dependencies: typing.Dict[str, VersionedDependency],
                 targetfn: typing.Callable[[context.Context], typing.Any],
                 ):
        self.dependencies = dependencies
        self.targetfn = targetfn

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        self.name = name

    async def get_latest_version(self, ctx: context.Context,
                                 ignore_pin: bool = False) -> str:
        ver = self.targetfn(ctx)
        dep = self.dependencies[ver]
        return await dep.get_latest_version(ctx, ignore_pin=ignore_pin)

    async def get_versioned_url(self, ctx: context.Context) -> str:
        ver = self.targetfn(ctx)
        dep = self.dependencies[ver]
        return await dep.get_versioned_url(ctx)

    async def _get_versioned_url(self, ctx: context.Context) -> str:
        ver = self.targetfn(ctx)
        dep = self.dependencies[ver]
        return await dep._get_versioned_url(ctx)

    async def _get_valid_versions(self) -> typing.List[T]:
        raise NotImplementedError()

    async def _get_available_versions(self) -> typing.List[str]:
        raise NotImplementedError()

    def _get_override_version(self) -> typing.Optional[str]:
        raise NotImplementedError()


class MappedVersionedDependency(VersionedDependency):
    """
    Class that allows you to set the return value from a dictionary
    where the targetfn function is the key of the dictionary.
    """
    def __init__(self,
                 mapping: typing.Dict[typing.Any, str],
                 targetfn: typing.Callable[[context.Context], typing.Any],
                 ):
        self.mapping = mapping
        self.targetfn = targetfn

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        self.name = name

    async def get_latest_version(self, ctx: context.Context,
                                 ignore_pin: bool = False) -> str:
        ver = self.targetfn(ctx)
        return self.mapping[ver]

    async def get_versioned_url(self, ctx: context.Context) -> str:
        ver = self.targetfn(ctx)
        return self.mapping[ver]

    async def _get_versioned_url(self, ctx: context.Context) -> str:
        ver = self.targetfn(ctx)
        return self.mapping[ver]

    async def _get_valid_versions(self) -> typing.List[T]:
        raise NotImplementedError()

    async def _get_available_versions(self) -> typing.List[str]:
        raise NotImplementedError()

    def _get_override_version(self) -> typing.Optional[str]:
        raise NotImplementedError()


_YAOOK_OP_DOCKER_PREFIX = "YAOOK_OP_DOCKER"


@environ.config(prefix=_YAOOK_OP_DOCKER_PREFIX)
class DockerConfig:
    config = environ.var(default=None)
    registry_override = environ.var(default="")


class VersionedDockerImage(VersionedDependency):
    """
    Represents a versioned Docker image.
    """
    def __init__(self,
                 url: str,
                 version_selector: VersionSelector[T]):
        super().__init__(url, version_selector)
        # We need to touch the URL after the constructor of the super class has
        # been called in order to apply the image version from the version pin
        # file even if image registry was overwritten.
        self.url = self._get_registry_override_url(self.url)

    def _get_credentials(self, domain: str) -> typing.Tuple[str, str]:
        dockerconfig = environ.to_config(DockerConfig)
        if not dockerconfig.config:
            raise KeyError("Not dockerconfig available")
        with open(dockerconfig.config) as f:
            cfg = json.load(f)
        cred = cfg.get("auths", {}).get(domain)
        if cred:
            auth = cred["auth"]
            auth_dec = base64.b64decode(auth).decode('ascii')
            username, password = auth_dec.split(":", 1)
            return username, password
        raise KeyError("Docker config missing for %s" % domain)

    def _get_image_versions(self, auth: typing.Callable) -> typing.List[str]:
        hostname, path = self.url.split("/", 1)
        if "." not in hostname:
            path = self.url
            hostname = "docker.io"
        if hostname == "docker.io":
            hostname = "registry-1.docker.io"
        repo = dxf.DXF(hostname, path, auth=auth)
        return repo.list_aliases()

    async def _get_available_versions(self) -> typing.List[str]:
        username: typing.Optional[str] = None
        password: typing.Optional[str] = None
        try:  # nosemgrep also fine if we dont have user/pw
            username, password = self._get_credentials(self.url.split("/")[0])
        except KeyError:
            pass

        def auth(d, response):
            d.authenticate(username, password,
                           response=response)

        return await asyncio.get_event_loop().run_in_executor(
            None, self._get_image_versions, auth)

    async def _get_versioned_url(self, ctx: context.Context) -> str:
        version = await self.get_latest_version(ctx)
        return f"{self.url}:{version}"

    def _get_registry_override_url(self, url):
        """
        Returns a container image URL with an alternative container image
        registry name if an override is specified in the dict given by the
        environement variable YAOOK_OP_DOCKER_REGISTRY_OVERRIDE.

        :param url: A container image URL may prefixed by a image registry host
            name.

        :return: The given container image URL with a overriden container
            registry host name or the given URL as is.
        """
        dis_url = url.split("/", 1)

        if len(dis_url) < 2:
            registry = ""
            path = dis_url[0]
        else:
            registry = dis_url[0]
            path = dis_url[1]

        if "." not in registry:
            registry = "DEFAULT_REGISTRY"

        registry_override = _get_registry_overrides().get(registry)

        if registry_override is not None:
            url = f"{registry_override}/{url}" \
                if registry == "DEFAULT_REGISTRY" else \
                f"{registry_override}/{path}"

        return url


BitnamiMajorMinorReleaseVersion = str


class SuffixedVersionedDockerImage(VersionedDockerImage):
    """
    This class allows you to locally suffix a Image in a registry.

    e.g.:
    registry/my-image:1.3: 1.3.59    ->  registry/my-image: 1.3.59
    registry/my-image:1.4: 1.4.10    ->  registry/my-image: 1.4.10
    registry/my-image:1.7: 1.7.9     ->  registry/my-image: 1.7.9
    registry/my-image:devel: 1.9.21  ->  registry/my-image: 1.9.21

    get_versioned_url will for example return registry/my-image:3.1.9 if you
    requested "registry/my-image:3". This allows you to pin multiple
    sub-releases of the same image locally with an alias in
    pinned_version.yml.
    """
    def __init__(self,
                 url: str,
                 version_selector: VersionSelector[T]):
        self.suffixed_url = url
        real_url, self.suffix = url.split(":", 1)
        super().__init__(real_url, version_selector)
        self.pinned_version: typing.Optional[str] = \
            _load_pin_file().get(self.suffixed_url)

    @property
    def pin_key(self):
        return self.suffixed_url


class BitnamiVersionedDockerImage(SuffixedVersionedDockerImage):
    def __init__(self,
                 url: str,
                 release: BitnamiMajorMinorReleaseVersion):
        major, minor = release.split(".")
        min_ver = f"{major}.{minor}"
        max_ver = f"{major}.{int(minor) + 1}"
        version_selector = BitnamiVersionSelector([
            ([f">={min_ver}.0", f"<{max_ver}.0"], [])
        ])
        url = f"{url}:{min_ver}"
        super().__init__(url, version_selector)


@environ.config(prefix=_YAOOK_OP_DOCKER_PREFIX)
class RegistryConfig:
    registry_base_path = environ.var(
        default=YAOOK_IMAGES_REGISTRY
    )


class ConfigurableVersionedDockerImage(VersionedDockerImage):
    """
    Represents a versioned Docker image, which can be configured.

    Configurable attributes:
        Docker Registry Base Path:
            YAOOK_OP_DOCKER_REGISTRY_BASE_PATH: the docker registry from which
                to pull the image selected by the image_selector parameter.
                Default value: YAOOK_IMAGES_REGISTRY

            Example: If image_selector == "repo_name/image_name", the env var
                YAOOK_OP_DOCKER_REGISTRY_BASE_PATH is set to "foo_registry" and
                get_latest_version() returns "1.0.1", get_versioned_url() will
                return "foo_registry/repo_name/image_name:1.0.1"

    :param image_selector: unique string identifying the image and its
        repository within the registry YAOOK_OP_DOCKER_REGISTRY_BASE_PATH.
    :type image_selector: str
    :param version_selector: Selector to find appropriate versions.
    :type version_selector: VersionSelector[T]

    Raises:
        ValueError: if the value of the environment variable
            YAOOK_OP_DOCKER_REGISTRY_BASE_PATH starts or end with a '/'.
    """

    def __init__(
            self,
            image_selector: str,
            version_selector: VersionSelector[T]):
        registry_config = environ.to_config(RegistryConfig)
        registry_base_path = registry_config.registry_base_path.strip()
        if len(registry_base_path) > len(registry_base_path.strip("/")):
            env_var_name = "_".join([
                _YAOOK_OP_DOCKER_PREFIX,
                RegistryConfig.registry_base_path.__name__.upper()
            ])
            msg = f"The value of the environment variable {env_var_name} " \
                  f"'{registry_base_path}' is not valid."
            raise ValueError(msg)
        sep = "/" if registry_base_path else ""
        url = f"{registry_base_path}{sep}{image_selector}"
        super().__init__(url, version_selector)
