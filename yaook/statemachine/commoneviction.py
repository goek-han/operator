#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta, abstractmethod
import asyncio
import collections
import itertools
import json
import logging
import typing

import yaook.statemachine as sm
import yaook.statemachine.customresource as customresource
import yaook.statemachine.resources.openstack as resource

# TODO(resource-refactor): clean up imports


class AsJsonObject(typing.Protocol):
    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        pass


T = typing.TypeVar('T', bound=AsJsonObject)


class Evictor(typing.Generic[T], metaclass=ABCMeta):
    def __init__(
            self,
            *,
            interface:
            sm.ResourceInterfaceWithStatus[typing.Mapping],
            namespace: str,
            node_name: str,
            connection_info: typing.Mapping,
            reason: str,
            ):
        self._interface = interface
        self._namespace = namespace
        self._node_name = node_name
        self._connection_info = connection_info
        self._reason = reason
        self._logger = logging.getLogger(
            __name__ + "." + namespace + "." + node_name,
        )

        self._attempt_counter: typing.Counter[
            typing.Tuple[str, typing.Hashable],
        ] = collections.Counter()

    @abstractmethod
    def _os_iterate(self) -> T:
        """
        start evtion of service/agent

        :return: current state of eviction.
        """
        raise NotImplementedError

    @abstractmethod
    async def is_migration_done(
            self,
            status: T,
            ) -> bool:
        """
        get status of eviction and check for pending and unhandleable resource
        objects or agents/services.
        It is a termination condition for migration loop

        :return: bool if no objects are pending or unhandleable.
        """
        raise NotImplementedError

    @abstractmethod
    def start_eviction_log(
            self,
            poll_interval: int,
            ) -> str:
        """
        prints status of eviction, e.g. if a compute node has all instances
        migrated away.

        :return: str with agent/service specific logmessage
        """
        raise NotImplementedError

    async def iterate(self) -> T:
        return await asyncio.get_event_loop().run_in_executor(
            None,
            self._os_iterate,
        )

    async def post_status(self, status: T) -> None:
        status_str = json.dumps(status.as_json_object())
        await customresource.update_status(
            self._interface,
            self._namespace,
            self._node_name,
            conditions=[
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.EVICTED.value,
                    status=resource.ResourceEvictingStatus.EVICTING.value,
                    reason=self._reason,
                    message=status_str,
                ),
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    status=str(False),
                    reason=self._reason,
                    message="Eviction in progress",
                ),
            ],
        )

    async def run(self, poll_interval: int) -> None:
        self._logger.info(self.start_eviction_log(poll_interval))
        for i in itertools.count():
            status = await self.iterate()
            self._logger.info("iteration %d complete: %s", i, status)
            await self.post_status(status)
            if await self.is_migration_done(status):
                break
            self._logger.debug("sleeping %ds until next iteration",
                               poll_interval)
            await asyncio.sleep(poll_interval)
