#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import abc
import asyncio
import dataclasses
import enum
import typing

import aiohttp.client_exceptions

import kubernetes_asyncio.client as kclient
import kubernetes_asyncio.watch


class EventType(enum.Enum):
    ADDED = 'ADDED'
    MODIFIED = 'MODIFIED'
    DELETED = 'DELETED'


T = typing.TypeVar("T")


@dataclasses.dataclass
class WatchEvent(typing.Generic[T]):
    type_: EventType
    """
    The type of the event.
    """

    reference: kclient.V1ObjectReference
    """
    Reference to the object. The following fields are populated:

    - ``uid``
    - ``name``
    - ``namespace``
    - ``kind``
    - ``api_version``

    Specifically, the ``uid`` can be used to ensure that a deferred processing
    of the event still refers to the same instance of an object and that the
    object has not been replaced in the meantime.
    """

    raw_object: typing.Mapping[str, typing.Any]
    """
    Unparsed representation of the new version of the object.
    """

    object_: T
    """
    Object wrapper (for k8s objects Kubernetes OpenAPI) representation of the
    new version of the object.

    If the watched resource type is a custom resource, this is the same as
    `raw_object`.
    """

    @classmethod
    def from_api_watch_event(cls: typing.Type[WatchEvent[T]],
                             ev: typing.Mapping) -> WatchEvent[T]:
        raw_object = ev["raw_object"]
        return cls(
            type_=EventType(ev["type"]),
            raw_object=raw_object,
            object_=typing.cast(T, ev["object"]),
            reference=kclient.V1ObjectReference(
                api_version=raw_object["apiVersion"],
                kind=raw_object["kind"],
                namespace=raw_object["metadata"].get("namespace"),
                name=raw_object["metadata"]["name"],
                uid=raw_object["metadata"]["uid"],
            ),
        )


@dataclasses.dataclass
class StatefulWatchEvent(WatchEvent[T]):
    old_raw_object: typing.Mapping[str, typing.Any]
    """
    Unparsed representation of the old version of the object.

    If the event type is :attr:`.EventType.ADDED`, this is the empty dict.
    """

    old_object: typing.Optional[T]
    """
    Object wrapper (for k8s objects Kubernetes OpenAPI) representation of the
    old version of the object.

    If the watched resource type is a custom resource, this is the same as
    `old_raw_object`.

    If the event type is :attr:`.EventType.ADDED`, this is :data:`None` (even
    if the resource type is a custom resource).
    """

    @classmethod
    def from_watch_event(cls,
                         ev: WatchEvent[T],
                         old_raw_object: typing.Mapping[str, typing.Any],
                         old_object: typing.Optional[T],
                         ) -> StatefulWatchEvent:
        return StatefulWatchEvent(
            type_=ev.type_,
            reference=ev.reference,
            raw_object=ev.raw_object,
            object_=ev.object_,
            old_raw_object=old_raw_object,
            old_object=old_object,
        )


async def watch_events(
        api_client: kclient.ApiClient,
        stream_factory: typing.Callable[..., typing.Any],
        ) -> typing.AsyncGenerator[WatchEvent[T], None]:
    """
    Convert a Kubernetes watch event stream into :class:`WatchEvent` objects.
    """
    watch = kubernetes_asyncio.watch.Watch()
    try:  # nosemgrep When we exit here we will just restart the watcher
        #   so just `pass`ing is fine
        async for ev in watch.stream(stream_factory(api_client)):
            if not isinstance(ev, dict):
                raise RuntimeError(
                    f"Kubernetes watch replied with non-object: {ev!r}",
                )
            if ev.get("type", "").lower() == "error":
                raise RuntimeError(
                    f"Kubernetes watch replied with error: {ev!r}",
                )
            yield WatchEvent[T].from_api_watch_event(ev)
    except aiohttp.client_exceptions.ClientPayloadError:
        pass
    except kclient.exceptions.ApiException as e:
        if e.status == 410:
            return
        raise e


@dataclasses.dataclass
class _ObjectState(typing.Generic[T]):
    uid: str
    namespace: typing.Optional[str]
    name: str
    raw_object: typing.Mapping[str, typing.Any]
    object_: T


class CorruptionAvoided(RuntimeError):
    pass


class StatefulWatcher(typing.Generic[T]):
    """
    Watch a kubernetes resource while keeping the last known state of the
    objects around.

    :param stream_factory: A factory function to create a stream which can
        be passed to :meth:`kubernetes_asyncio.watch.Watch.stream`.
    :param restore_state_on_reconnect: Whether to approximate diffs during
        a restart of the watch.

    In addition to the featuers offered by normal kubernetes API watches, this
    gives us the possibility to look at the differences which caused the watch
    event to be fired.

    If `restore_state_on_restart` is true, the object state is not discarded
    when the watch exits. Instead, it is stashed away for use when the watch
    is restarted.

    Upon a restart with `restore_state_on_restart` enabled, the
    :attr:`~.EventType.ADDED` events which refer to objects which are already
    known in the stash (identified by their UID) will be converted to
    :attr:`~.EventType.MODIFIED` events with the version from the stash as the
    old version.

    As there is no reliable way to detect the end of the initial
    synchronization with :attr:`~.EventType.ADDED`, the first non-ADDED event
    which occurs will be used to purge the remainder of the stash and also emit
    :attr:`~.EventType.DELETED` events for all UIDs which have not been
    reobserved so far.

    .. warning::

        This means that for some usages, the :attr:`~.EventType.DELETED` events
        may be delayed indefinitely, which is why this option is not enabled by
        default.

    """

    def __init__(
            self,
            stream_factory: typing.Callable[..., typing.Any],
            *,
            restore_state_on_restart: bool = False):
        super().__init__()
        self._stream_factory = stream_factory
        self._objects_by_uid: typing.Dict[str, _ObjectState[T]] = {}
        self._objects_by_name: \
            typing.Dict[typing.Tuple[typing.Optional[str], str],
                        _ObjectState[T]] \
            = {}
        self._stashed_objects_by_uid: typing.Dict[str, _ObjectState[T]] = {}
        self._running = False
        self._running_lock = asyncio.Lock()
        self._restore_state_on_restart = restore_state_on_restart

    def get_by_name(
            self,
            namespace: typing.Optional[str],
            name: str,
            ) -> typing.Tuple[typing.Mapping[str, typing.Any], T]:
        state = self._objects_by_name[namespace, name]
        return state.raw_object, state.object_

    def get_by_uid(
            self,
            uid: str,
            ) -> typing.Tuple[typing.Mapping[str, typing.Any], T]:
        state = self._objects_by_uid[uid]
        return state.raw_object, state.object_

    def get_all(
            self,
            ) -> typing.Iterable[
                typing.Tuple[typing.Mapping[str, typing.Any], T]
            ]:
        return [
            (state.raw_object, state.object_)
            for state in self._objects_by_uid.values()
        ]

    def _purge_stash(self) -> typing.Iterable[StatefulWatchEvent[T]]:
        for state in self._stashed_objects_by_uid.values():
            raw_object = state.raw_object
            yield StatefulWatchEvent(
                type_=EventType.DELETED,
                reference=kclient.V1ObjectReference(
                    name=state.name,
                    namespace=state.namespace,
                    uid=raw_object["metadata"]["uid"],
                    api_version=raw_object["apiVersion"],
                    kind=raw_object["kind"],
                ),
                object_=state.object_,
                raw_object=raw_object,
                old_object=state.object_,
                old_raw_object=raw_object,
            )
        self._stashed_objects_by_uid.clear()

    async def stream_events(
            self,
            api_client: kclient.ApiClient,
            ) -> typing.AsyncGenerator[StatefulWatchEvent[T], None]:
        """
        Stream events from the Kubernetes API as :class:`StatefulWatchEvent`.

        This asynchronous generator yields the events received from the
        Kubernetes API as :class:`StatefulWatchEvent` instances. Please see
        that class for documentation about the information available.

        While this generator is running, the methods :meth:`get_by_name` and
        :meth:`get_by_uid` can be used.

        .. warning::

            This generator cannot be run multiple times in parallel! Attempting
            to do so will raise :class:`CorruptionAvoided`.
        """
        async with self._running_lock:
            if self._running:
                raise CorruptionAvoided()
            self._running = True
        try:
            ev_: WatchEvent
            async for ev_ in watch_events(api_client, self._stream_factory):
                # evil, but we cannot infer the type of watch_events, generic
                # functions don’t work quite that well.
                ev = typing.cast(WatchEvent[T], ev_)
                ref = ev.reference

                old_object: typing.Optional[T]
                old_raw_object: typing.Mapping[str, typing.Any]

                if (ev.type_ != EventType.ADDED and
                        self._stashed_objects_by_uid):
                    # If we had objects stashed and there are still any left on
                    # the first non-ADDED event, we have to emit DELETE events
                    # to let the user catch up to that.
                    for synthesized_event in self._purge_stash():
                        yield synthesized_event

                # First try to recover the stashed state, if any is available.
                # Note that recovering stashed state is only valid on ADDED
                # events, but just above we dropped the entire stash if this
                # is a non-ADDED event -> the lookup will always fail in that
                # case.
                try:  # nosemgrep we just access a cache here so there is no
                    #   problem if we dont hit it
                    stashed_state = self._stashed_objects_by_uid.pop(ref.uid)
                except KeyError:
                    pass
                else:
                    # If a stashed state was found, we need to rewrite the
                    # event by changing the type and then injecting the stashed
                    # state into our current state data structures.
                    ev = dataclasses.replace(ev, type_=EventType.MODIFIED)
                    self._objects_by_uid[ref.uid] = stashed_state
                    self._objects_by_name.setdefault(
                        (ref.namespace, ref.name),
                        stashed_state,
                    )
                    if stashed_state.raw_object == ev.raw_object:
                        # raw object equality means we can skip this event
                        continue
                    del stashed_state

                try:
                    state = self._objects_by_uid[ref.uid]
                except KeyError:
                    state = _ObjectState(
                        uid=ref.uid,
                        namespace=ref.namespace,
                        name=ref.name,
                        raw_object=ev.raw_object,
                        object_=ev.object_,
                    )
                    old_object = None
                    old_raw_object = {}

                    # note that we always fill in _objects_by_uid and
                    # _objects_by_name, even if the event is a DELETED one, to
                    # allow the consistency check below to be accurate.
                    self._objects_by_uid[ref.uid] = state
                    # use setdefault here to cause the lookup below to
                    # return a possibly old version of the object in
                    # order to cause a nice error
                    self._objects_by_name.setdefault(
                        (ref.namespace, ref.name),
                        state,
                    )
                else:
                    old_object = state.object_
                    old_raw_object = state.raw_object

                state_by_name: typing.Optional[_ObjectState]
                state_by_name = self._objects_by_name.get(
                    (ref.namespace, ref.name),
                )
                if state_by_name is not state:
                    raise RuntimeError(
                        "internal state corruption: "
                        "UIDs and namespace/name pairs do not refer to "
                        "the same object. "
                        "Object by UID: {}. "
                        "Object by Name: {}.".format(state, state_by_name)
                    )
                del state_by_name

                if ev.type_ == EventType.DELETED:
                    self._objects_by_uid.pop(ref.uid, None)
                    self._objects_by_name.pop((ref.namespace, ref.name), None)

                state.raw_object = ev.raw_object
                state.object_ = ev.object_

                yield StatefulWatchEvent.from_watch_event(
                    ev,
                    old_raw_object=old_raw_object,
                    old_object=old_object,
                )
        except BaseException:
            # prevent stashing if there was an error
            self._objects_by_uid.clear()
            raise
        finally:
            self._objects_by_name.clear()
            if self._restore_state_on_restart:
                self._stashed_objects_by_uid = self._objects_by_uid
                self._objects_by_uid = {}
            else:
                self._objects_by_uid.clear()
            async with self._running_lock:
                self._running = False


class ExternalWatcher(typing.Generic[T], metaclass=abc.ABCMeta):
    """
    Watch a out-of-kubernetes resource while keeping the last known state of
    the objects around.
    """

    def __init__(
            self,
            connection_parameters: typing.Optional[
                typing.Mapping[str, typing.Any]] = None,
            namespace: typing.Optional[str] = None,
            interval: int = 60):
        super().__init__()
        self._objects_by_resource_name: typing.Dict[str, T] = {}
        self._running = False
        self._running_lock = asyncio.Lock()
        self._interval = interval
        self.connection_parameters = connection_parameters
        self.namespace = namespace

    @abc.abstractmethod
    async def get_state(self) -> typing.Mapping[str, T]:
        """
        Get the current state of the watched resource.
        This method is called at a regular interval.

        :return: A mapping of a kubernetes resource name to the current state
        for this resource.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def to_dict(self, obj: T) -> typing.Mapping[str, typing.Any]:
        """
        Transfor the current state object into a dict representation of it.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def to_kubernetes_reference(self, name: str) -> kclient.V1ObjectReference:
        """
        Create a kubernetes reference out of a resource name.
        The implementation must ensure that at least name and namespace are
        set.
        """
        raise NotImplementedError()

    async def stream_events(
            self) -> typing.AsyncGenerator[StatefulWatchEvent[T], None]:
        """
        Stream events from the External API as :class:`StatefulWatchEvent`.

        This asynchronous generator yields the events generated for the state
        of the external api.

        .. warning::

            This generator cannot be run multiple times in parallel! Attempting
            to do so will raise :class:`CorruptionAvoided`.
        """
        async with self._running_lock:
            if self._running:
                raise CorruptionAvoided()
            self._running = True
        try:
            while True:
                state = await self.get_state()
                for name, data in state.items():
                    if name not in self._objects_by_resource_name:
                        self._objects_by_resource_name[name] = data
                        yield StatefulWatchEvent(
                            type_=EventType.ADDED,
                            reference=self.to_kubernetes_reference(name),
                            raw_object=self.to_dict(data),
                            object_=data,
                            old_raw_object={},
                            old_object=None,
                        )
                    else:
                        old = self._objects_by_resource_name[name]
                        if old != data:
                            self._objects_by_resource_name[name] = data
                            yield StatefulWatchEvent(
                                type_=EventType.MODIFIED,
                                reference=self.to_kubernetes_reference(name),
                                raw_object=self.to_dict(data),
                                object_=data,
                                old_raw_object=self.to_dict(old),
                                old_object=old,
                            )
                deleted_keys = self._objects_by_resource_name.keys() -\
                    state.keys()
                for deleted in deleted_keys:
                    old = self._objects_by_resource_name[deleted]
                    yield StatefulWatchEvent(
                        type_=EventType.DELETED,
                        reference=self.to_kubernetes_reference(name),
                        raw_object={},
                        object_=typing.cast(T, None),
                        old_raw_object=self.to_dict(old),
                        old_object=old,
                    )
                    del self._objects_by_resource_name[deleted]

                await asyncio.sleep(self._interval)
        except BaseException:
            # prevent keeping data if there was an error
            self._objects_by_resource_name.clear()
            raise
        finally:
            async with self._running_lock:
                self._running = False
