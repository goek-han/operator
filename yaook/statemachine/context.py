#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.context` – Operative Context
######################################################

.. currentmodule:: yaook.statemachine

.. autoclass:: Context

.. _yaook.statemachine.context.LabelsAndAnnotations:

Labels and Annotations
======================

.. currentmodule:: yaook.statemachine.context

.. autodata:: LABEL_PARENT_GROUP

.. autodata:: LABEL_PARENT_VERSION

.. autodata:: LABEL_PARENT_PLURAL

.. autodata:: LABEL_PARENT_NAME

.. autodata:: LABEL_COMPONENT

.. autodata:: LABEL_INSTANCE

.. autodata:: LABEL_ORPHANED

.. autodata:: LABEL_L2_REQUIRE_MIGRATION

.. autodata:: ANNOTATION_BGP_INTERFACE_IP

.. autodata:: ANNOTATION_LAST_UPDATE

.. autodata:: ANNOTATION_L2_MIGRATION_LOCK

.. autodata:: ANNOTATION_RESTART_ID

.. autodata:: ANNOTATION_PAUSE
"""
from __future__ import annotations

import dataclasses
import datetime
import enum
import logging
import typing

import kubernetes_asyncio.client

from . import api_utils, interfaces, watcher


class Phase(enum.Enum):
    CREATED = "Created"
    UPDATING = "Updating"
    WAITING_FOR_DEPENDENCY = "WaitingForDependency"
    UPDATED = "Updated"
    BACKING_OFF = "BackingOff"
    INVALID_CONFIGURATION = "InvalidConfiguration"
    COMPLETED = "Completed"
    FAILED = "Failed"


LABEL_PARENT_PLURAL = "state.yaook.cloud/parent-plural"
"""
The Kubernetes API plural of the Custom Resource to which a managed object
belongs.
"""

LABEL_PARENT_GROUP = "state.yaook.cloud/parent-group"
"""
The Kubernetes API group of the Custom Resource to which a managed object
belongs. This is generally the string before the slash in the `apiVersion`
value of the object.
"""

LABEL_PARENT_VERSION = "state.yaook.cloud/parent-version"
"""
The Kubernetes API version of the Custom Resource to which a managed object
belongs. This is generally the string behind the slash in the `apiVersion`
value of the object.
"""

LABEL_PARENT_NAME = "state.yaook.cloud/parent-name"
"""
The name of the Custom Resource object to which a managed object belongs.
"""

LABEL_COMPONENT = "state.yaook.cloud/component"
"""
The name of the component represented by the object in the context of the
custom resource. This is a freeform string assigned by the
:class:`.CustomResource` instance.
"""

LABEL_INSTANCE = "state.yaook.cloud/instance"
"""
If multiple instances of a component exist, for example because of the use of
:class:`.PerNode`, this label is used to distinguish the instances.
"""

LABEL_ORPHANED = "state.yaook.cloud/orphaned"
"""
If an object has been orhpaned by the state machine, it will be given this
label.
"""

LABEL_CDS_CONTROLLER_UID = \
    "configureddaemonsets.apps.yaook.cloud/controller-uid"
"""
The uid of the cds controller
"""

LABEL_L2_REQUIRE_MIGRATION = \
    "maintenance.yaook.cloud/maintenance-required-l2-agent"
"""
Mark the node for needing l2 agent maintenance (restart/remove), so other
agents needing l2 will be removed by their operators
"""

ANNOTATION_BGP_INTERFACE_IP = "bgp-interface-ip.network.yaook.cloud/"
"""
Define an IP with subnet for a interface created for bgp.
This will be used by bgp agent init container to set the IP to the bgp
interface.
This can be used to let bgp dragent advertise routes via the external network.

Name is the config_key of the specific bgp section.

Format: IP/Prefixlen

Example: bgp-interface-ip.network.yaook.cloud/bgpdr-agent: 10.2.4.42/24
"""

ANNOTATION_LAST_UPDATE = "state.yaook.cloud/last-update-timestamp"
"""
Mark the extended timestamp of the last update to the resource by the state
machine.
"""

ANNOTATION_L2_MIGRATION_LOCK = "l2-lock.maintenance.yaook.cloud/"
"""
Lock the node, so l2 agend on the node will not be restarted/updated/removed
"""

ANNOTATION_RESTART_ID = "kubectl.kubernetes.io/restartedAt"
"""
Annotation set on pod templates when the statemachine triggers a rolling
restart of a resource. The existence and value of this annotation is ignored
for the purpose of determining whether a resource needs updating.

Note that the name of this annotation is compatible with the kubectl
annotation, with the nice side-benefit that we don't accidentally re-concile
resources just because someone did a rolling restart on them.
"""

ANNOTATION_PAUSE = "state.yaook.cloud/pause"
"""
If this annotation is present on a custom resource, it will *not* be converged
by the corresponding operator.
"""

TEST_ANNOTATION_PREFIX = "test.yaook.cloud"
"""
A prefix used for annotations which are evaluated in unittests. Future code may
remove those annotations from deployed objects.
"""

ANNOTATION_VERSIONED_DEPENDENCY = "state.yaook.cloud/versioned_dependencies"
"""
An annotation used to get the versioned_dependency for resources that don't
have an image but are dependant on a version (e.g. policies).
"""

ANNOTATION_POLICY = "state.yaook.cloud/policies"
"""
An anotation used to provide the policies that need to be validated.
"""


@dataclasses.dataclass(frozen=True)
class Context:
    """
    Represent the context in which a state machine operation takes place.

    It is comprised of:

    - Type and instance information about the parent custom resource
    - The namespace in which the operation takes place
    - An API client to use
    - A logger to use
    - And, if present, the instance of the component which is being worked on

    .. attribute:: namespace

        The namespace in which all affected objects reside.

        .. note::

            This should only be used for listings. When a specific object is
            being looked for, you’ll usually have both its namespace and its
            name at your disposal, and then you should use the more specific
            source of the namespace value.

    .. attribute:: parent

        The full body of the parent object.

    .. attribute:: parent_intf

        The resource interface to access the parent resource.

    .. autoattribute:: parent_kind

    .. autoattribute:: parent_plural

    .. autoattribute:: parent_api_version

    .. autoattribute:: parent_name

    .. autoattribute:: parent_uid

    .. autoattribute:: parent_spec

    .. attribute:: instance

        If the current operation happens in the context of a instanced
        resource, for example due to :class:`PerNode`, this field contains
        the string identifier of the instance.

    .. attribute:: instance_data

        If the current operation happens in the context of a instanced
        resource, for example due to :class:`PerNode`, this field contains
        additional data of the instance.

    .. attribute:: field_manager

        The ``fieldManager`` value to use when creating and updating Kubernetes
        API objects.

        .. seealso::

            `Server-Side Apply <https://kubernetes.io/docs/reference/using-api/server-side-apply/>`_
            in the Kubernetes documentation.

    .. automethod:: base_label_match

    .. automethod:: with_instance
    """  # NOQA

    namespace: str
    parent: typing.Mapping[str, typing.Any]
    parent_intf: interfaces.ResourceInterfaceWithStatus[typing.Mapping]
    instance: typing.Optional[str]
    instance_data: typing.Optional[typing.Mapping]
    api_client: kubernetes_asyncio.client.ApiClient
    logger: logging.Logger
    field_manager: str

    def base_label_match(self) -> typing.MutableMapping[str, str]:
        group, version = api_utils.split_api_version(self.parent_api_version)
        label_match = {
            LABEL_PARENT_PLURAL: self.parent_plural,
            LABEL_PARENT_GROUP: group,
            LABEL_PARENT_VERSION: version,
            LABEL_PARENT_NAME: self.parent_name,
        }
        if self.instance is not None:
            label_match[LABEL_INSTANCE] = self.instance
        return label_match

    def with_instance(self,
                      instance: str,
                      data: typing.Optional[typing.Any] = None) -> Context:
        """
        Create a child context for the given instance.

        :param instance: The instance value to use.
        :type instance: :class:`str`
        :param data: Additional data of the instance.
        :raises ValueError: if the context already has an `instance` set.
        :return: A copy of the context with the :attr:`instance` field set to
            `instance`.

        When attempting to call this method on a context which already has an
        instance set, :class:`ValueError` is raised. This is because that usage
        indicates nested usage of state wrappers such as :class:`PerNode` which
        is at this time not supported.
        """

        if self.instance is not None:
            raise ValueError(
                "context {!r} already has an instance".format(
                    self
                )
            )
        if len(instance) > 63:
            raise ValueError(
                "Instance is longer than 63 characters and k8s api calls wont"
                f" work. Instance: {instance}, Length: {len(instance)}"
            )
        return dataclasses.replace(self, instance=instance, instance_data=data)

    def without_instance(self) -> Context:
        return dataclasses.replace(self, instance=None, instance_data=None)

    @property
    def parent_api_version(self) -> str:
        """
        The `apiVersion` of the parent custom resource.
        """
        return self.parent["apiVersion"]

    @property
    def parent_plural(self) -> str:
        """
        The `plural` of the parent custom resource.
        """
        return self.parent_intf.plural

    @property
    def parent_kind(self) -> str:
        """
        The `kind` of the parent custom resource.
        """
        return self.parent["kind"]

    @property
    def parent_spec(self) -> typing.Mapping[str, typing.Any]:
        """
        The JSON data representing the complete parent spec.
        """
        return self.parent["spec"]

    @property
    def parent_uid(self) -> str:
        """
        The `metadata.uid` of the parent custom resource.
        """
        return self.parent["metadata"]["uid"]

    @property
    def parent_name(self) -> str:
        """
        The `metadata.name` of the parent custom resoucre.
        """
        return self.parent["metadata"]["name"]

    @property
    def creation_timestamp(self) -> datetime.datetime:
        """
        The `metadata.creationTimestamp` of the parent custom resource.
        """
        return datetime.datetime.strptime(
            self.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")


T = typing.TypeVar("T")


class ListenerCallback(typing.Protocol[T]):
    def __call__(self,
                 ctx: Context,
                 event: watcher.StatefulWatchEvent[T]) -> bool:
        ...


@dataclasses.dataclass(frozen=True)
class KubernetesListener(typing.Generic[T]):
    """
    Represent a listener for Kubernetes events.

    .. attribute:: api_group

        The `apiVersion`, without the version suffix, of the object to listen
        for. For example, to listen for jobs, this would be `batch`.

    .. attribute:: version

        The versioning part of the `apiVersion` of the object to listen for.
        For example, to listen for the latest jobs object type, this would be
        `v1`.

    .. attribute:: plural

        The plural name of the object to listen for. To listen for jobs, this
        would be `jobs`.

    .. attribute:: listener

        The listener callback. The callback must be a callable accepting the
        following arugments:

        - `ctx`: The :class:`Context` in which the operation takes place
        - `event` (:class:`watcher.StatefulWatcherEvent[T]`): The event
          object.

        .. note::

            It is possible that the object changes or gets deleted between the
            time of the notification and the time the event is delivered.

            The handler needs to be prepared to deal with such events
            gracefully and it must eventually return without an exception.

        The handler may return :data:`True` to enqueue a reconcile of the
        parent custom resource.

    .. attribute:: broadcast

        If true, the listener registers for events for all objects of the named
        API endpoint, not only events for objects which somehow relate to the
        custom resource it belongs to.

        This is useful to listen for changes in the kubernetes environment to
        which one must react, such as added or removed nodes.

    .. attribute:: component

        If given, the listener will only receive events for objects matching
        the given component. This is not compatible with setting `broadcast`
        to :data:`True` (it is ignored in that case).

    """

    api_group: str
    version: str
    plural: str
    listener: ListenerCallback
    broadcast: bool = dataclasses.field(default=False)
    component: typing.Optional[str] = dataclasses.field(default=None)
    broadcast_filter: typing.Optional[typing.Mapping[str, str]] = \
        dataclasses.field(default=None)

    @property
    def object_key(self) -> typing.Tuple[str, str, str]:
        return (self.api_group, self.version, self.plural)


@dataclasses.dataclass(frozen=True)
class ExternalListener:
    """
    Represent a listener for Out-of-Kubernetes events.

    .. attribute:: watcher

        The watcher class.

    .. attribute:: listener

        The listener callback. The callback must be a callable accepting the
        following arugments:

        - `ctx`: The :class:`Context` in which the operation takes place
        - `event` (:class:`watcher.StatefulWatcherEvent[T]`): The event
          object.

        .. note::

            It is possible that the object changes or gets deleted between the
            time of the notification and the time the event is delivered.

            The handler needs to be prepared to deal with such events
            gracefully and it must eventually return without an exception.

        The handler may return :data:`True` to enqueue a reconcile of the
        parent custom resource.

    """

    watcher: watcher.ExternalWatcher[typing.Any]
    listener: ListenerCallback


Listener = typing.Union[KubernetesListener, ExternalListener]


def recover_parent_reference(
        metadata: typing.Mapping[str, typing.Any],
        ) -> typing.Tuple[api_utils.ResourceReference, typing.Optional[str]]:
    labels = typing.cast(typing.Mapping[str, str], metadata.get("labels"))
    try:
        parent_plural = labels[LABEL_PARENT_PLURAL]
        parent_group = labels[LABEL_PARENT_GROUP]
        parent_version = labels[LABEL_PARENT_VERSION]
        parent_name = labels[LABEL_PARENT_NAME]
        instance = labels.get(LABEL_INSTANCE, None)
        namespace = metadata["namespace"]
    except KeyError:
        raise ValueError("does not belong to the statemachine")

    return api_utils.ResourceReference(
        api_version=api_utils.join_api_version(parent_group, parent_version),
        plural=parent_plural,
        name=parent_name,
        namespace=namespace,
    ), instance
