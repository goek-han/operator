#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`yaook.statemachine`
#########################

Each :class:`Resource` class is responsible for *one* specific way to manage
*one* type of Kubernetes resource (though it may manage multiple instances)
per instance of a Custom Resource which uses that State.

Each :class:`CustomResource` instantiates one or more instances of the
specific :class:`State` subclasses it needs. During instantiation,
final parametrisation (such as paths to templates and the COMPONENT key)
are configured.

.. automodule:: yaook.statemachine.resources

.. automodule:: yaook.statemachine.cue

.. automodule:: yaook.statemachine.context

.. automodule:: yaook.statemachine.templating

.. automodule:: yaook.statemachine.api_utils

"""
from .api_utils import (  # noqa: F401
    build_selector,
    match_to_selector,
    extract_metadata,
    generate_update_timestamp,
    deep_has_changes,
    matches_labels,
    LabelSelector,
    LabelExpression,
    SelectorOperator,
    ResourceReference,
    split_api_version,
    join_api_version,
    matches_node_selector,
    parse_timestamp,
    format_timestamp,
)

from .exceptions import (  # noqa: F401
    ResourceNotPresent,
    AmbiguousRequest,
    ContinueableError,
    ConfigurationInvalid,
    TriggerReconcile,
)

from .context import (  # noqa: F401
    Context,
    KubernetesListener,
    ListenerCallback,
    ExternalListener,
    Listener,
    recover_parent_reference,
)

from .interfaces import (  # noqa: F401
    ResourceInterface,
    ResourceInterfaceWithStatus,
    config_map_interface,
    secret_interface,
    role_interface,
    role_binding_interface,
    service_account_interface,
    service_interface,
    job_interface,
    deployment_interface,
    stateful_set_interface,
    custom_resource_interface,
    neutron_dhcp_agent_interface,
    neutron_l2_agent_interface,
    neutron_l3_agent_interface,
    neutron_bgp_dragent_interface,
    neutron_ovn_agent_interface,
    nova_compute_node_interface,
    nova_compute_host_aggregate_interface,
    novadeployment_interface,
    mysqlservice_interface,
    mysqluser_interface,
    amqpserver_interface,
    amqpuser_interface,
    pod_interface,
    keystonedeployment_interface,
    sshidentity_interface,
    gnocchideployment_interface,
    certificates_interface,
    issuer_interface,
    servicemonitor_interface,
    keystoneuser_interface,
)

from .resources import *  # noqa: F401,F403

from .cue import (  # noqa: F401
    CueLayer,
    CueConfigMap,
    CueSecret,
    AMQPTransportLayer,
    DatabaseConnectionLayer,
    KeystoneAuthLayer,
    MetadataSecretLayer,
    NodeLabelSelectedLayer,
    NodeLabelSelectedSecretInjectionLayer,
    SecretInjectionLayer,
    MultivaluedSecretInjectionLayer,
    SpecLayer,
    ConfigSecretLayer,
    RegionNameConfigLayer,
)

from .customresource import (  # noqa: F401
    CustomResource,
    ReleaseAwareCustomResource,
    UpgradeStrategy,
    OneshotCustomResource,
)

from .registry import (  # noqa: F401
    register,
)

from .versioneddependencies import (  # noqa: F401
    SuffixedVersionedDockerImage,
    BitnamiVersionedDockerImage,
    MappedVersionedDependency,
    ConfigurableVersionedDockerImage,
    VersionedDockerImage,
    SemVerSelector,
    YaookSemVerSelector,
    BitnamiVersionSelector,
    BitnamiVersion,
    OVNVersionSelector,
    OVNVersion,
    ReleaseAwareVersionedDependency,
)
