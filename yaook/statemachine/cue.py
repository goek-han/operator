#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.cue` – Objects containing config generated with cuelang
#################################################################################

.. currentmodule:: yaook.statemachine

Concept
=======

For general information on cuelang, please visit
`the cuelang website <https://cuelang.org/>`_.

In the context of Yaook, cuelang is used to generate and prevalidate
configuration for OpenStack and other services. The key advantage with using
cuelang is (simplified) that it enforces that if you see an input value
for an option somewhere, it will also be the output value; if conflicting
values for the same option come from different sources, cuelang rejects the
input.

This means that even though Yaook supports multiple inputs for most
configuration files (for example, snippets which are injected based on the
labels of the target nodes), there will never be any surprises, because those
inputs cannot override each other.

The classes in this document support working with cuelang by allowing the user
to make most of those cuelang properties.

Layers
------

The cuelang generation uses the concept of *layers* or *overlays*. A single
cuelang generated configuration is composed from multiple *layers*. Each layer
is represented by a :class:`.CueLayer` object, while the composed configuration
is managed by a class using the :class:`.LayeredCueMixin`.

Generally, each layer contains a single aspect of the configuration, such as
a single database or rabbitmq connection. As described, each layer needs to
merge with the other layers without errors for rendering with cuelang to
succeed.

Quick reference
===============

.. _yaook.statemachine.cue.ConfigurationClasses:

Configuration classes
---------------------

To provide configuration to services in Kubernetes, secrets or config maps can
be generated using cuelang:

.. autosummary::
    :nosignatures:

    CueConfigMap
    CueSecret

Provided layers
---------------

.. autosummary::
    :nosignatures:

    AMQPTransportLayer
    DatabaseConnectionLayer
    KeystoneAuthLayer
    NodeLabelSelectedLayer
    NodeLabelSelectedSecretInjectionLayer
    SecretInjectionLayer
    SpecLayer
    ConfigSecretLayer
    RegionNameConfigLayer

Custom layers
-------------

To implement a layer, derive from the following class and implement the methods
described in the documentation:

.. autosummary::
    :nosignatures:

    CueLayer

Full reference
==============

Configuration class details
---------------------------

.. autoclass:: CueConfigMap(*[, metadata][, add_cue_layers=[]][, copy_on_write=False])

.. autoclass:: CueSecret(*[, metadata][, add_cue_layers=[]][, copy_on_write=False])

Layer details
-------------

.. autoclass:: AMQPTransportLayer

.. autoclass:: DatabaseConnectionLayer

.. autoclass:: KeystoneAuthLayer

.. autoclass:: NodeLabelSelectedLayer

.. autoclass:: NodeLabelSelectedSecretInjectionLayer

.. autoclass:: SecretInjectionLayer

.. autoclass:: SpecLayer

.. autoclass:: ConfigSecretLayer

.. autoclass:: CueLayer

.. autoclass:: RegionNameConfigLayer

.. currentmodule:: yaook.statemachine.cue

Implementation details
----------------------

.. autoclass:: CueMixin

.. autoclass:: LayeredCueMixin

"""  # noqa:E501
from __future__ import annotations

import abc
import copy
import dataclasses
import typing

import kubernetes_asyncio.client as kclient

import yaook.common.config

from . import api_utils, context, exceptions, interfaces, resources

if typing.TYPE_CHECKING:
    _MixinBase = resources.Resource
else:
    _MixinBase = object


CueInput = yaook.common.config.MutableConfigSpec


ContextString = typing.Callable[[context.Context], str]
DynamicString = typing.Union[str, ContextString]


def _compile_string(s: DynamicString) -> ContextString:
    if isinstance(s, str):
        return lambda _: typing.cast(str, s)
    return s


class CueMixin(_MixinBase, metaclass=abc.ABCMeta):
    """
    Mixin to generate cuelang-based configuration.

    Subclasses must implement the following method:

    .. automethod:: _get_cue_inputs

    The following method is provided to subclasess for consumption and is not
    part of the public interface:

    .. automethod:: _render_cue_config
    """

    def __init__(
            self,
            **kwargs):
        super().__init__(**kwargs)

    @abc.abstractmethod
    async def _get_cue_inputs(
            self,
            ctx: context.Context,
            ) -> CueInput:
        """
        Generate the cuelang input for the cuelang config generator.
        """
        return {}

    async def _render_cue_config(
            self,
            ctx: context.Context,
            ) -> resources.ResourceBody:
        """
        Return the rendered cue configuration.

        :raises .ContinueableError: If the configuration is invalid.

        Internally, this uses :meth:`_get_cue_inputs` to obtain the inputs
        to cuelang.
        """
        try:
            return yaook.common.config.build_config(
                await self._get_cue_inputs(ctx)
            )
        except yaook.common.config.ConfigValidationError as e:
            raise exceptions.ConfigurationInvalid(str(e))


class CueLayer(metaclass=abc.ABCMeta):
    """
    Base class to implement custom configuration overlays.

    The following interface is public:

    .. automethod:: get_dependencies

    The following method must be implemented by subclasses and is also public:

    .. automethod:: get_layer

    The following interface is offered only to subclasses:

    .. automethod:: _declare_dependencies

    Instances of (subclasses of) this class can be used with
    :ref:`yaook.statemachine.cue.ConfigurationClasses` to extend the generated
    output safely.
    """
    def __init__(
            self,
            *,
            add_dependencies: typing.Collection[resources.Resource] = []):
        super().__init__()
        self._dependencies: typing.List[resources.Resource] = \
            list(add_dependencies)

    def _declare_dependencies(self, *deps: resources.Resource) -> None:
        """
        Declare resources as dependencies of this layer.

        :param deps: Resources to declare as dependency.

        The declared resources will be available through
        :meth:`get_dependencies` to users of this class. Specifically, the
        default :ref:`yaook.statemachine.cue.ConfigurationClasses` export the
        dependency information to the state machine for dependency resolution.
        """
        self._dependencies.extend(deps)

    def get_dependencies(self) -> typing.Collection[resources.Resource]:
        """
        Return the dependencies of this layer.
        """
        return self._dependencies

    @abc.abstractmethod
    async def get_layer(
            self,
            ctx: context.Context,
            ) -> CueInput:
        """
        Compose the configuration overlay.

        :param ctx: The context of the operation.
        :return: The configuration overlay.

        The format of the return value is a dictionary. The dictionary maps
        "targets" (cuelang package names) to lists of configuration inputs.

        Each configuration input is typically a dictionary which maps string
        keys or configuration sections to configuration data.
        """
        return {}


class LayeredCueMixin(CueMixin):
    """
    Mixin providing the layering functionality on top of :class:`CueMixin`.

    :param add_cue_layers: Collection of layers to add during initialisation.

    All layers which are passed via `add_cue_layers` will be added to the
    configuration as if by calling :meth:`add_layer` for each item.

    This mixin is used to form the
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.

    .. automethod:: add_layer
    """

    def __init__(
            self,
            *,
            add_cue_layers: typing.Collection[CueLayer] = [],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._cue_layers = list(add_cue_layers)
        for layer in self._cue_layers:
            self._declare_dependencies(*layer.get_dependencies())

    def add_layer(self, layer: CueLayer) -> None:
        """
        Add an overlay to the configuration.

        :param layer: The layer to add.

        The dependencies of the layer will be reflected in this resource’s
        dependencies.
        """
        self._cue_layers.append(layer)
        self._declare_dependencies(*layer.get_dependencies())

    async def _get_cue_inputs(self, ctx: context.Context) -> CueInput:
        result = await super()._get_cue_inputs(ctx)
        for layer in self._cue_layers:
            for k, v in (await layer.get_layer(ctx)).items():
                try:
                    decl = result[k]
                except KeyError:
                    decl = yaook.common.config.ConfigDeclaration(
                        flavor=v.flavor,
                        contents=list(v.contents),
                    )
                    result[k] = decl
                    continue

                if decl.flavor != v.flavor:
                    raise ValueError(
                        "mixing different configuration flavors in a single "
                        "file is not supported: got different flavors "
                        f"{decl.flavor!r} and {v.flavor!r} from cue layers for"
                        f" {k!r}"
                    )

                result[k] = yaook.common.config.ConfigDeclaration(
                    flavor=decl.flavor,
                    contents=list(decl.contents) + list(v.contents),
                )
        return result


class CueConfigMap(LayeredCueMixin, resources.ConfigMap):
    """
    Manage a ConfigMap whose contents are generated using cuelang.

    :param metadata: Description of the metadata to generate for the resource.
    :param add_cue_layers: Collection of layers to add during initialisation.
    :param copy_on_write: If true, create a new resource on changes instead of
        updating in-place.

    All layers which are passed via `add_cue_layers` will be added to the
    configuration as if by calling :meth:`add_layer` for each item.

    .. seealso::

        :func:`~.evaluate_metadata`
            for details on the accepted values for `metadata`.

        :class:`~.SingleObject`
            for details on `copy_on_write`

        :class:`CueSecret`
            for a usage example

    .. automethod:: add_layer
    """

    def __init__(
            self,
            *,
            metadata: resources.MetadataProvider,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.ResourceBody:
        return {
            "apiVersion": "v1",
            "kind": "ConfigMap",
            "metadata": resources.evaluate_metadata(ctx, self._metadata),
            "data": await self._render_cue_config(ctx),
        }


class CueSecret(LayeredCueMixin, resources.Secret):
    """
    Manage a Secret whose contents are generated using cuelang.

    :param metadata: Description of the metadata to generate for the resource.
    :param add_cue_layers: Collection of layers to add during initialisation.
    :param copy_on_write: If true, create a new resource on changes instead of
        updating in-place.

    All layers which are passed via `add_cue_layers` will be added to the
    configuration as if by calling :meth:`add_layer` for each item.

    The configuration generated by cuelang is encoded as UTF-8 and wrapped in
    base64 in order to transport it into a Kubernetes Secret.

    Example use:

    .. code-block:: python

        class FancyResource(sm.CustomResource):
            config = sm.CueSecret(
                metadata=("my-config-", True),
                copy_on_write=True,
                add_cue_layers=[
                    sm.DatabaseConnectionLayer(...),
                ]
            )

    .. seealso::

        :func:`~.evaluate_metadata`
            for details on the accepted values for `metadata`.

        :class:`~.SingleObject`
            for details on `copy_on_write`

    .. automethod:: add_layer
    """
    def __init__(
            self,
            *,
            metadata: resources.MetadataProvider,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.ResourceBody:
        raw_data = await self._render_cue_config(ctx)
        encoded_data = api_utils.encode_secret_data(raw_data, encoding="utf-8")
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": resources.evaluate_metadata(ctx, self._metadata),
            "type": "Opaque",
            "data": encoded_data,
        }


class AMQPTransportLayer(CueLayer):
    """
    Cue layer for a OpenStack AMQP transport URI.

    :param target: The cuelang package to target.
    :param service: Reference to the Kubernetes Service under which the
        AMQP server is reachable.
    :param username: Username to use for authenticating with the AMQP service.
    :param password_secret: Reference to the Kubernetes Secret which holds the
        password to use for authenticating with the AMQP service.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            target: str,
            service: resources.KubernetesReference[kclient.V1Service],
            username: DynamicString,
            password_secret: resources.KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(service, password_secret)
        self._target = target
        self._service = service
        self._username = _compile_string(username)
        self._password_secret = password_secret

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.OSLO_CONFIG.declare([
                await resources.make_mq_config_overlay(
                    ctx,
                    mq_service=self._service,
                    mq_username=self._username(ctx),
                    mq_user_password_secret=self._password_secret,
                ),
            ])
        }


class DatabaseConnectionLayer(CueLayer):
    """
    Cue layer for a OpenStack database connection URI.

    :param target: The cuelang package to target.
    :param service: Reference to the Kubernetes Service under which the
        database is reachable.
    :param database_name: Name of the database.
    :param username: Username to use for authenticating with the database
        service.
    :param password_secret: Reference to the Kubernetes Secret which holds the
        password to use for authenticating with the database service.
    :param config_section: The configuration section to inject the database
        credentials into.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            target: str,
            service: resources.KubernetesReference[kclient.V1Service],
            database_name: str,
            username: DynamicString,
            password_secret: resources.KubernetesReference[kclient.V1Secret],
            config_section: str,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(service, password_secret)
        self._target = target
        self._service = service
        self._database_name = database_name
        self._username = _compile_string(username)
        self._password_secret = password_secret
        self._config_section = config_section

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.OSLO_CONFIG.declare([
                await resources.make_db_config_overlay(
                    ctx,
                    db_service=self._service,
                    db_name=self._database_name,
                    db_username=self._username(ctx),
                    db_user_password_secret=self._password_secret,
                    config_section=self._config_section,
                ),
            ])
        }


class KeystoneAuthLayer(CueLayer):
    """
    Cue layer for OpenStack keystone credentials.

    :param target: The cuelang package to target.
    :param credentials: Reference to the Kubernetes Secret which holds the
        OpenStack environment variables to access keystone.
    :param config_section: The configuration section to inject the database
        credentials into.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            target: str,
            credentials_secret: resources.KubernetesReference[
                kclient.V1Secret
            ],
            endpoint_config: resources.KubernetesReference[
                kclient.V1ConfigMap
            ],
            config_section: str = "keystone_authtoken",
            interface_override: typing.Optional[str] = None,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(credentials_secret, endpoint_config)
        self._target = target
        self._credentials_secret = credentials_secret
        self._config_section = config_section
        self._endpoint_config = endpoint_config
        self._interface_override = interface_override

    def _map_credentials(
            self,
            secret_data: typing.Mapping[str, str],
            ) -> resources.ResourceBody:
        """
        Convert a secret with an OpenStack environment to a config snippet.

        .. note::

            Currently, only ``password`` authentication is supported.
        """
        auth_type = secret_data["OS_AUTH_TYPE"]
        if auth_type != "password":
            raise ValueError(
                f"unsupported OS_AUTH_TYPE for KeystoneAuthLayer: {auth_type}"
            )

        return {
            "auth_type": auth_type,
            "username": secret_data["OS_USERNAME"],
            "password": secret_data["OS_PASSWORD"],
            "project_name": secret_data["OS_PROJECT_NAME"],
            "project_domain_name": secret_data["OS_PROJECT_DOMAIN_NAME"],
            "user_domain_name": secret_data["OS_USER_DOMAIN_NAME"],
        }

    def _map_endpoint_config(
            self,
            configmap_data: typing.Mapping[str, str],
            ) -> resources.ResourceBody:
        data = {
            "valid_interfaces": [self._interface_override or
                                 configmap_data["OS_INTERFACE"]],
            "auth_url": configmap_data["OS_AUTH_URL"],
            "auth_version": configmap_data["OS_IDENTITY_API_VERSION"],
            "memcached_servers": configmap_data["MEMCACHED_SERVERS"].split(',')
        }
        if self._config_section == "keystone_authtoken":
            data["interface"] = (self._interface_override or
                                 configmap_data["OS_INTERFACE"])
        return data

    async def get_layer(self, ctx: context.Context) -> CueInput:
        secrets = interfaces.secret_interface(ctx.api_client)
        secret_ref = await self._credentials_secret.get(ctx)
        credentials = api_utils.decode_secret_data(
            (await secrets.read(secret_ref.namespace, secret_ref.name)).data
        )

        configmaps = interfaces.config_map_interface(ctx.api_client)
        config_ref = await self._endpoint_config.get(ctx)
        endpoint_config = (await configmaps.read(
            config_ref.namespace,
            config_ref.name,
        )).data

        return {
            self._target: yaook.common.config.OSLO_CONFIG.declare([
                {
                    self._config_section: self._map_credentials(credentials),
                },
                {
                    self._config_section: self._map_endpoint_config(
                        endpoint_config,
                    ),
                },
            ]),
        }


@dataclasses.dataclass(frozen=True)
class LabelledOverlay:
    """
    Configuration snippets with an associated label selector.

    .. attribute:: selector

        The label selector to use when a decision whether to apply the
        configuration has to be made.

    .. attribute:: configs

        The configuration snippets themselves.
    """
    selectors: typing.Collection[api_utils.LabelSelector]
    configs: typing.Mapping[str, typing.Any]


class LabelSelectedLayer(CueLayer):
    """
    Abstract cue layer to combine multiple configuration snippets based on
    label selectors.

    :param target_map: A mapping which maps configuration keys from the
        templates to cuelang packages.

    Subclasses must implement the following methods to fully specify the
    behaviour of this layer:

    .. automethod:: _get_target_labels

    .. automethod:: _get_config_templates

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """

    def __init__(
            self,
            *,
            target_map: typing.Mapping[str, str],
            flavor: yaook.common.config.ConfigFlavor =
            yaook.common.config.OSLO_CONFIG,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._target_map = target_map
        self._flavor = flavor

    @abc.abstractmethod
    async def _get_target_labels(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, str]:
        """
        Return the labels which the target instance has.

        :param ctx: The context of the operation.
        :return: The label set of the target instance.

        The labels returned by this method are used to select the matching
        configuration templates from :meth:`_get_config_templates`.
        """

    @abc.abstractmethod
    async def _get_config_templates(
            self,
            ctx: context.Context,
            ) -> typing.Collection[LabelledOverlay]:
        """
        Return the configuration templates to use.

        :param ctx: The context of the operation.
        :return: A collection of overlays with their corresponding label
            selectors.
        """

    async def get_layer(
            self,
            ctx: context.Context,
            ) -> CueInput:
        labels = await self._get_target_labels(ctx)
        templates = await self._get_config_templates(ctx)

        result: CueInput = {
            cue_target: yaook.common.config.ConfigDeclaration(
                flavor=yaook.common.config.OSLO_CONFIG,
                contents=[],
            )
            for cue_target in self._target_map.values()
        }
        for template in templates:
            if not any(selector.object_matches(labels)
                       for selector in template.selectors):
                continue
            for config_key, cue_target in self._target_map.items():
                try:  # nosemgrep on keyerror this section is not relevant
                    value = template.configs[config_key]
                except KeyError:
                    continue
                typing.cast(typing.List,
                            result[cue_target].contents).append(value)

        return result


class MetadataSecretLayer(CueLayer):
    def __init__(
            self,
            *,
            proxy_secret: resources.KubernetesReference[kclient.V1Secret],
            metadata_key: str = "neutron_metadata_agent",
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(proxy_secret)
        self._proxy_secret = proxy_secret
        self._metadata_key = metadata_key

    async def get_layer(self, ctx: context.Context) -> CueInput:
        metadata_secret = await resources.extract_password(
            ctx,
            self._proxy_secret,
        )
        return {
            self._metadata_key: yaook.common.config.OSLO_CONFIG.declare([
                {
                    "DEFAULT": {
                        "metadata_proxy_shared_secret": metadata_secret
                    },
                }
            ]),
        }


class NodeLabelSelectedLayer(LabelSelectedLayer):
    """
    Cue layer to select configuration snippets based on the labels of the
    context’s node.

    :param target_map: A mapping which maps configuration keys from the
        templates to cuelang packages.
    :param accessor: A function which extracts the configuration template
        collection from the parent resource’ spec.

    When generating the layer, the labels of the node whose name matches the
    :attr:`~.Context.instance` of the execution context are used to select the
    configuration snippets.

    The snippets returned by `accessor` are expected to be dictionaries of the
    following format (in YAML):

    .. code-block:: yaml

        nodeSelectors:
        - matchLabels:
            ...
        - ...
        key1: ...
        key2: ...

    where `nodeSelector` is an array of label match definitions similar to
    those typically found in the k8s API. See :meth:`~.LabelSelector.from_dict`
    for the supported format. The remaining keys of the dictionary are taken as
    configuration snippets. The `accessor` receives the complete
    :class:`~.Context` of the operation as only argument.

    The configuration snippets will be associated to a cuelang package via the
    `target_map`. The keys of the `target_map` correspond to the keys from the
    above dictionary while the values are the corresponding cuelang packages.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            target_map: typing.Mapping[str, str],
            accessor: typing.Callable[
                [context.Context],
                typing.Collection[resources.ResourceBody],
            ],
            **kwargs: typing.Any):
        super().__init__(target_map=target_map, **kwargs)
        self._accessor = accessor

    async def _get_target_labels(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, str]:
        core_client = kclient.CoreV1Api(ctx.api_client)
        node = await core_client.read_node(ctx.instance)
        return node.metadata.labels or {}

    async def _get_config_overlay(
            self,
            ctx: context.Context,
            item: typing.Mapping[str, typing.Any]
            ) -> typing.Sequence[typing.Mapping[str, typing.Any]]:
        return [item]

    async def _get_config_templates(
            self,
            ctx: context.Context,
            ) -> typing.Collection[LabelledOverlay]:
        result = []
        try:
            configs = self._accessor(ctx)
        except Exception as e:
            raise exceptions.ConfigurationInvalid(str(e))
        for item in configs:
            item = dict(item)
            node_selectors = item.pop("nodeSelectors")
            item = {
                k: v for
                k, v in item.items()
                if k in self._target_map.keys()
            }
            overlays = await self._get_config_overlay(ctx, item)
            for overlay in overlays:
                result.append(LabelledOverlay(
                    selectors=[
                        api_utils.LabelSelector.from_dict(node_selector)
                        for node_selector in node_selectors
                    ],
                    configs=overlay,
                ))
        return result


class NodeLabelSelectedSecretInjectionLayer(NodeLabelSelectedLayer):
    """
    Cue layer to inject secrets based on the labels of the
    context’s node.

    This works in the same way as :class:`SecretInjectionLayer` but using node
    labels as in :class:`NodeLabelSelectedLayer`.
    """
    async def _get_config_overlay(
            self,
            ctx: context.Context,
            item: typing.Mapping[str, typing.Any]
            ) -> typing.Sequence[typing.Mapping[str, typing.Any]]:
        ret = []
        for configKey, secretInjects in item.items():
            secretValues = await resources.get_injected_secrets(
                ctx,
                secretInjects
            )
            ret.extend([{configKey: val} for val in secretValues])
        return ret


SpecAccessor = typing.Union[
    str,
    typing.Callable[[context.Context], typing.Any]
]


def _compile_accessor(
        accessor: SpecAccessor
        ) -> typing.Callable[[context.Context], typing.Any]:
    if isinstance(accessor, str):
        str_accessor = accessor

        def new_accessor(ctx: context.Context) -> typing.Any:
            return ctx.parent_spec.get(str_accessor, {})

        return new_accessor
    return accessor


class SecretInjectionLayer(CueLayer):
    """
    Cue layer to inject Kubernetes Secrets referenced from the parent resource
    spec into the configuration.

    :param target: The cuelang package to target.
    :param accessor: A string key or a function to extract the secret injection
        spec from the parent resource.

    This creates a configuration layer out secrets extracted from Kubernetes
    according to the secret injection specification referenced in the parent
    resource spec by `accessor`.

    If `accessor` is a string, the injection spec is taken from
    ``parent["spec"].get(accessor, {})``. Otherwise, the it is taken from
    ``accessor(ctx)``.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """

    def __init__(
            self,
            *,
            target: str,
            accessor: SpecAccessor,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._target = target
        self._accessor = _compile_accessor(accessor)

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.ConfigDeclaration(
                flavor=yaook.common.config.OSLO_CONFIG,
                contents=await resources.get_injected_secrets(
                    ctx,
                    self._accessor(ctx),
                ),
            ),
        }


class MultivaluedSecretInjectionLayer(SecretInjectionLayer):
    """
    Cue layer to inject Kubernetes Secrets referenced from the parent resource
    spec into the configuration with multivaled keys. So one key can occour
    multiple times.

    TODO: copy from SecretInjectionLayer
    """

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.ConfigDeclaration(
                flavor=yaook.common.config.OSLO_CONFIG,
                contents=await resources.get_multivalued_injected_secrets(
                    ctx,
                    self._accessor(ctx),
                ),
            ),
        }


class SpecLayer(CueLayer):
    """
    Cue layer to copy parts of the parent resource spec into the configuration.

    :param target: The cuelang package to target.
    :param accessor: A string key or a function to access the configuration
        to inject.

    This creates a configuration layer out of an attribute of the resource
    spec of the parent resource. If `accessor` is a string, the configuration
    input is taken from ``parent["spec"].get(accessor, {})``. Otherwise, the
    input is taken from ``accessor(ctx)``.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """

    def __init__(
            self,
            *,
            target: str,
            accessor: SpecAccessor,
            flavor: yaook.common.config.ConfigFlavor = yaook.common.config.OSLO_CONFIG,  # noqa:E501
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._target = target
        self._accessor = _compile_accessor(accessor)
        self._flavor = flavor

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: self._flavor.declare([
                copy.deepcopy(self._accessor(ctx))
            ]),
        }


class ConfigSecretLayer(CueLayer):
    """
    Cue layer to set a configuration option to a string.

    The value of the configuration option `config_option_name` in the
    configuration section `config_group` will set to `config_option_value`.

    :param target: The cuelang package to target.
    :param config_section: The configuration section into which
        the secret should be injected
    :param config_option_name: The name of the configuration option under which
        the secret should be injected
    :param secret: The secret to inject
    :param secret_key: the key in the secret's data, whose value should be
        injected
    :param config_flavor: configuration flavor. The default value is
        yaook.common.config.OSLO_CONFIG

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """

    def __init__(
            self,
            *,
            target: str,
            config_section: str,
            config_option_name: str,
            secret: resources.Secret,
            secret_key: str = "password",
            flavor: yaook.common.config.ConfigFlavor = yaook.common.config.OSLO_CONFIG,  # noqa:E501
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._target = target
        self._config_path = f"/{config_section}/{config_option_name}"
        self._secret = secret
        self._flavor = flavor
        self._secret_key = secret_key

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.ConfigDeclaration(
                flavor=yaook.common.config.OSLO_CONFIG,
                contents=await resources.get_injected_secrets(
                    ctx,
                    await self.secret_accessor(ctx),
                ),
            ),
        }

    async def secret_accessor(
            self, ctx: context.Context
            ) -> typing.Iterable[typing.Mapping[str, typing.Any]]:
        secret_ref = await self._secret.get(ctx)
        return [
            {
                "secretName": secret_ref.name,
                "items": [
                    {
                        "key": self._secret_key,
                        "path": self._config_path,
                    },
                ],
            },
        ]


class RegionNameConfigLayer(CueLayer):
    """
    Cue layer to set a configuration option of region name to a placement.

    :param target: The cuelang package to target.
    :param config_section: The configuration section to inject the region
        name into.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            target: str,
            config_section: str = "keystone_authtoken",
            allow_not_defined: bool = False,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._target = target
        self.config_section = config_section
        self.allow_not_defined = allow_not_defined

    async def get_layer(self, ctx: context.Context) -> CueInput:
        return {
            self._target: yaook.common.config.OSLO_CONFIG.declare([
                self.make_region_name_config_overlay(ctx)
            ])
        }

    def make_region_name_config_overlay(
            self,
            ctx: context.Context,
            ) -> typing.MutableMapping[str, typing.Any]:
        try:
            return {
                self.config_section: {
                    "region_name": ctx.parent_spec["region"]["name"],
                    "os_region_name": ctx.parent_spec["region"]["name"],
                }
            }
        except KeyError as e:
            if self.allow_not_defined:
                return {}
            else:
                raise e
