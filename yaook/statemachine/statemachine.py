#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

from . import context, exceptions, resources


class StateMachine:
    def __init__(self,
                 customresource: "customresource.CustomResource"):
        self.cr = customresource
        self._dependencies: typing.Dict[
            resources.Resource,
            typing.Set[resources.Resource],
        ] = {}
        self._components: typing.Dict[str, resources.Resource] = {}

    def add_state(
            self,
            state: resources.Resource,
            dependencies: typing.Optional[
                typing.Iterable[resources.Resource]] = None) -> None:
        if state.component in self._components:
            existing_state = self._components[state.component]
            raise ValueError(
                f"{state!r} defines the component {state.component!r} which is"
                f" already defined by {existing_state!r}"
            )

        if not isinstance(state.component, str):
            raise TypeError(
                f"class {state!r} has component {state.component!r}, which is "
                "not a string"
            )

        self._components[state.component] = state
        self._dependencies[state] = set(dependencies or [])

    def get_listeners(self) -> typing.List[context.Listener]:
        result = []
        for st in self._dependencies.keys():
            result.extend(st.get_listeners())
        return result

    async def _ensure_state(self,
                            state: resources.Resource,
                            ctx: context.Context) -> resources.ReadyInfo:
        try:
            # TODO: filter external event here or in reconcile?
            dependencies_by_component = {
                dep_state.component: dep_state
                for dep_state in self._dependencies[state]
            }
            await state.reconcile(
                ctx,
                dependencies=dependencies_by_component
            )

            return resources.ReadyInfo.cast(await state.is_ready(ctx))
        except exceptions.ContinueableError as e:
            ctx.logger.warn(
                "State %s could not complete: %s",
                state,
                e.msg
            )
            return resources.ReadyInfo.cast(e)
        except Exception:
            # TODO: maybe simply wrap this in another exception type and let
            # the caller log it properly?
            ctx.logger.error(
                "failed to reconcile state %s",
                state,
                exc_info=True,
            )
            raise

    async def ensure(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[resources.Resource, typing.Optional[str]]:
        ctx.logger.debug("reconciling states for %s: %r",
                         self.cr, list(self._dependencies.keys()))
        pending = {
            state: frozenset(dependencies)
            for state, dependencies in self._dependencies.items()
        }
        stalled: typing.Set[resources.Resource] = set()
        blocking: typing.Dict[resources.Resource, typing.Optional[str]] = {}
        done: typing.Set[resources.Resource] = set()

        while pending:
            new_pending = {}
            for state, dependencies in pending.items():
                if dependencies - set(pending):
                    raise RuntimeError(
                        "state machine cannot complete: "
                        f"state {state!r} has (remaining) dependencies "
                        f"{dependencies!r} which are not a subset of the "
                        "remaining states to converge: "
                        f"{list(pending.keys())!r}"
                    )
                dependencies = dependencies - done
                stalled_dependencies = dependencies & stalled

                if stalled_dependencies:
                    ctx.logger.info(
                        "skipping state %s in this reconciliation run because "
                        "the following dependencies are not ready yet: %s",
                        state,
                        stalled_dependencies,
                    )
                    stalled.add(state)
                    continue

                if dependencies:
                    # skip state in this iteration.
                    new_pending[state] = dependencies
                    continue

                ctx.logger.info(
                    "reconciling [%3.0f%% (%3d+%3d/%3d)] %s",
                    (len(done)+len(stalled))/len(self._dependencies)*100,
                    len(done), len(stalled),
                    len(self._dependencies),
                    state,
                )
                ready = await self._ensure_state(state, ctx)
                if not ready:
                    stalled.add(state)
                    blocking[state] = ready.message
                    continue
                done.add(state)
            pending = new_pending

        return blocking

    @property
    def states(self) -> typing.Iterable[resources.Resource]:
        return self._dependencies.keys()


if typing.TYPE_CHECKING:
    from . import customresource
