#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

from . import customresource


registry: typing.Dict[str,
                      typing.Tuple[
                        str,
                        str,
                        str,
                        typing.Type[customresource.CustomResource]]] = {}


def register(cr_cls: typing.Type[customresource.CustomResource]) -> None:
    registry[cr_cls.PLURAL] = (cr_cls.PLURAL,
                               cr_cls.API_GROUP,
                               cr_cls.API_GROUP_VERSION,
                               cr_cls)
