#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.api_utils`: Utilities for the k8s api
===============================================================

.. currentmodule:: yaook.statemachine

.. autofunction:: match_to_selector

.. autofunction:: build_selector

.. autofunction:: extract_metadata

.. autofunction:: generate_update_timestamp

.. autofunction:: deep_has_changes

.. autofunction:: matches_labels

.. autoclass:: LabelSelector

.. autoclass:: LabelExpression

.. autoclass:: SelectorOperator

.. autoclass:: ResourceReference

.. autofunction:: split_api_version

.. autofunction:: join_api_version

.. currentmodule:: yaook.statemachine.api_utils

.. autofunction:: multi_selector_list

.. autofunction:: inject_scheduling_keys

.. autofunction:: extract_pod_references

.. autofunction:: extract_cds_references

.. autofunction:: get_cluster_domain

.. autofunction:: k8s_obj_to_yaml_data

.. autofunction:: matches_node_selector

.. autofunction:: parse_timestamp

.. autofunction:: format_timestamp
"""
from __future__ import annotations

import asyncio
import base64
import copy
import dataclasses
import enum
import functools
import itertools
import json
import os
import re
import secrets
import socket
import types
import typing

from datetime import datetime

import kubernetes_asyncio.client as kclient


def cached(
        f: typing.Callable[..., typing.Awaitable[T]],
        ) -> typing.Callable[..., typing.Awaitable[T]]:
    """
    Decorator to deduplicate and cache the result of a coroutine function.

    This works by wrapping the coroutine function in a normal function which
    invokes the original coroutine function. The coroutine is then wrapped into
    a future and the future is LRU cached.
    """
    @functools.lru_cache(maxsize=32)
    @functools.wraps(f)
    def wrapper(
            *args: typing.Any,
            **kwargs: typing.Any,
            ) -> typing.Awaitable[T]:
        return asyncio.ensure_future(f(*args, **kwargs))
    return wrapper


def match_to_selector(match: typing.Mapping[str, str]) -> str:
    """
    Convert a match mapping to a selector string.
    Can be used for label or field selectors.

    :param match: Dictionary mapping keys to respective values.
    :return: selector string which filters for the given keys and
        values.
    """
    return ",".join(
        "{}={}".format(k, v)
        for k, v in match.items()
    )


def build_selector(
        selector: typing.Union[typing.Mapping[str, str], str]
        ) -> str:
    """
    Convert a selector into a proper string for the API.

    :param selector: Either a label/field match dictionary or a string which
        already is a proper selector.
    :return: The resulting label/field selector.
    """
    if isinstance(selector, str):
        return selector
    return match_to_selector(selector)


def extract_metadata(obj: typing.Any) -> typing.Dict[str, typing.Any]:
    """
    Extract the metadata dictionary from any kubernetes API object.

    :param obj: An object returned by the k8s API.
    :return: The metadata dictionary of that object.

    This extract the following metadata:

    - `annotations`
    - `creationTimestamp`
    - `deletionTimestamp`
    - `labels`
    - `name`
    - `namespace`
    - `resourceVersion`
    - `uid`

    If `obj` is a dictionary, this function returns the `"metadata"` key from
    that dictionary.

    Otherwise, the metadata is extracted from the `metadata` attribute of `obj`
    and converted into a dictionary.

    In addition, the following guarantees are made:

    - The `labels` key is never absent or :data:`None`. Unless the input data
      contains a different type, it will always be a mapping.
    """

    # NOTE: DO NOT EDIT THIS UNLESS YOU KNOW WHAT YOU ARE DOING.
    # The problem here is that the to_dict() function of the OpenAPI wrappers
    # returns the python-side name of the attributes. For example, it uses the
    # `deletion_timestamp` key instead of `deletionTimestamp`. That means that
    # if we want to use the results of this function exchangably ne matter
    # whether an OpenAPI wrapper was involved or not, we have to reverse this
    # mapping.
    # Currently, the reversal is only implemented for the first layer of
    # attributes (see below). That means that any attribute which has a nested
    # data structure may be garbled (such as managedFields).
    # Hence, we use a allowlist here in order to avoid confusing behaviour when
    # the results of this function are used on both custom resources and known
    # resources; and we enforce the allowlist even for custom resources to be
    # consistent.
    SUPPORTED_FIELDS = [
        "annotations",
        "creationTimestamp",
        "deletionTimestamp",
        "labels",
        "name",
        "namespace",
        "uid",
        "resourceVersion",
    ]

    if isinstance(obj, dict):
        raw_metadata = copy.deepcopy(obj["metadata"])
    else:
        raw_metadata = obj.metadata.to_dict()
        attr_map = kclient.V1ObjectMeta.attribute_map
        for py_name, api_name in attr_map.items():
            try:  # nosemgrep we do not guarantee that all fields will be
                #   present in the return value
                value = raw_metadata.pop(py_name)
            except KeyError:
                continue
            raw_metadata[api_name] = value

    result = {
        k: v for k, v in raw_metadata.items()
        if k in SUPPORTED_FIELDS
    }
    # specifically using .get() here to catch both cases: when "labels" is
    # missing and when it’s None
    if result.get("labels") is None:
        result["labels"] = {}
    return result


def generate_update_timestamp() -> str:
    """
    Generate a unique timestamp.

    :return: A string containing the current time as well as a random value to
        ensure uniqueness even in a distributed system.

    The resulting value should be treated as opaque. Its internal structure may
    change at any time, however, it is supposed to be useful for human
    observers to reason about causality.
    """
    random = secrets.token_urlsafe(8)
    return "{:%Y-%m-%dT%H:%M:%S.%f}Z+{}".format(
        datetime.utcnow(),
        random,
    )


def k8s_obj_to_yaml_data(obj: object) -> typing.Mapping[str, typing.Any]:
    """
    Convert a kubernetes object to a dict while adjusting key names
    according to the attribute map.
    (Original implementation from the kubernetes_asyncio lib)
    """
    result = {}
    for attr, _ in obj.openapi_types.items():  # type: ignore
        key = obj.attribute_map[attr]  # type: ignore
        value = getattr(obj, attr)
        if hasattr(value, "to_dict"):
            result[key] = k8s_obj_to_yaml_data(value)
        elif isinstance(value, list):
            result[key] = list(map(  # type: ignore
                lambda x: k8s_obj_to_yaml_data(x)
                if hasattr(x, "to_dict") else x,
                value
            ))
        elif isinstance(value, dict):
            result[key] = dict(map(
                lambda item: (item[0], k8s_obj_to_yaml_data(item[1]))
                if hasattr(item[1], "to_dict") else item,
                value.items()
            ))
        else:
            if value is not None:
                result[key] = value
    return result


# The following is a dictionary of all kubernetes resource keys that represent
# lists that are internally treated as maps (common example is environment
# variables). We need to take special care for these lists as we must not
# compare the order of their elements.
# The key in this dictionary is the name of the list attribute as used in k8s.
# The value is the key inside the list value that k8s treats as the dictionary
# key.
#
# Example for environment variables (how we define them in k8s):
#
# env:
#   - name: REQUESTS_CA_BUNDLE
#     value: /etc/pki/tls/certs/ca-bundle.crt
#   - name: WSGI_PROCESSES
#     value: "15"
#
# How they are internally treated:
#
# env:
#   REQUESTS_CA_BUNDLE: /etc/pki/tls/certs/ca-bundle.crt
#   WSGI_PROCESSES: "15"
#
# To have the same intelligence in our code we need the mapping below that
# allows us to convert the list to the map representation.
#
_K8S_LISTS_THAT_ARE_MAPS = {
    "volumeMounts": "mountPath",
    "env": "name",
    "volumes": "name",
}


def _copy_poped(d: typing.Dict, key: str) -> typing.Dict:
    """
    Copy a dictonary and remove a key from the copy.

    :param d: Dictionary to copy.
    :param key: Key to pop.
    :return: The copied dictionary without the key.
    """
    new = copy.deepcopy(d)
    new.pop(key)
    return new


def deep_has_changes(old: typing.Any, new: typing.Any,
                     key: typing.Optional[str] = None) -> bool:
    """
    Check if `new` would change something in `old` when used in a patch.

    :param old: The current state of an object.
    :param new: Input to a kubernetes PATCH API call.
    :param key: used internally to check if a list is actually a map

    This recursively compares `old` and `new`, returning :data:`True` if and
    only if `new` changes or overwrites any keys in `old`.
    """
    if isinstance(old, dict) and isinstance(new, dict):
        for kn, vn in new.items():
            try:
                vo = old[kn]
            except KeyError:
                try:
                    if vn is None or len(vn) == 0:
                        # We are not actually missing something useful
                        return False
                except TypeError:
                    # This might happen if `vn` is an int. In that case we
                    # we definately have a difference.
                    return True
                return True
            if deep_has_changes(vo, vn, kn):
                return True
        return False
    elif isinstance(old, list) and isinstance(new, list):
        if len(new) != len(old):
            return True
        if key and key in _K8S_LISTS_THAT_ARE_MAPS:
            map_key = _K8S_LISTS_THAT_ARE_MAPS[key]
            old_map = {x[map_key]: _copy_poped(x, map_key) for x in old}
            new_map = {x[map_key]: _copy_poped(x, map_key) for x in new}
            return deep_has_changes(old_map, new_map)
        for i in range(len(new)):
            if deep_has_changes(old[i], new[i]):
                return True
        return False
    return old != new


def matches_labels(
        object_labels: typing.Mapping[str, str],
        match_labels: typing.Mapping[str, str]) -> bool:
    """
    Check if the labels of an object match the given labels.

    :param object_labels: The label dictionary of the object to check.
    :param match_labels: The label dictionary representing the labels to
        match.
    :return: True if the object is matched by the labels, false otherwise.

    Returns true if and only if the object labels are a superset of the match
    labels and if the values for the labels are equal.

    By extension, this means that:

    - An empty `match_labels` dict matches all objects.
    - If an object gains additional labels, it will still match an unchanged
      `match_labels` dict.

    That means that this function can be used like label matchers in the
    Kubernetes API; they restrict the set of matched objects. The empty matcher
    matches all objects.
    """
    for match_key, match_value in match_labels.items():
        try:
            object_value = object_labels[match_key]
        except KeyError:
            return False
        if object_value != match_value:
            return False
    return True


class SelectorOperator(enum.Enum):
    """
    .. seealso::

        The ``operator`` key in the
        `Kubernetes V1LabelSelectorRequirement object <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#labelselectorrequirement-v1-meta>`_.

    .. attribute:: EXISTS

        The ``Exists`` operator

    .. attribute:: IN

        The ``In`` operator

    .. attribute:: NOT_IN

        The ``NotIn`` operator

    .. attribute:: NOT_EXISTS

        The ``DoesNotExist`` operator
    """  # NOQA
    EXISTS = "Exists"
    IN = "In"
    NOT_IN = "NotIn"
    NOT_EXISTS = "DoesNotExist"


@dataclasses.dataclass(frozen=True)
class LabelExpression:
    """
    Represent a single label selector expression for the Kubernetes API.

    :param key: The label key to match
    :type key: :class:`str`
    :param operator: The operator to apply
    :type operator: :class:`~.SelectorOperator`
    :param values: The right hand side of the operator.
    :type values: collection of :class:`str` or :data:`None` (for In and NotIn
        operators)

    .. automethod:: from_dict

    .. automethod:: from_api_object

    .. automethod:: object_matches

    .. automethod:: as_api_selector
    """
    key: str
    operator: SelectorOperator
    values: typing.Optional[typing.Tuple[str]]

    def __init__(self, *,
                 key: str,
                 operator: SelectorOperator,
                 values: typing.Optional[typing.Collection[str]]):
        super().__init__()
        object.__setattr__(self, "key", key)
        object.__setattr__(self, "operator", operator)
        if operator in (SelectorOperator.EXISTS, SelectorOperator.NOT_EXISTS):
            values = None
        else:
            if values is None:
                raise ValueError(
                    "missing values in label expression requiring values"
                )
            values = tuple(values)
        object.__setattr__(self, "values", values)

    @classmethod
    def from_dict(cls, d: typing.Mapping[str, typing.Any]) -> LabelExpression:
        """
        Parse a dict representing a V1LabelSelectorRequirement.

        :param d: The dictionary representing the JSON object representing the
            V1LabelSelectorRequirement.
        :raises KeyError: If a required key is missing
        :raises ValueError: If any value is invalid
        :raises ValueError: If values is missing or set to None but the
            operator requires a (possibly empty) set of values.
        :return: The resulting expression
        :rtype: :class:`~.LabelExpression`

        Parse the JSON object dictionary and return the corresponding label
        expression.
        """
        return cls(
            key=d["key"],
            operator=SelectorOperator(d["operator"]),
            values=d.get("values"),
        )

    @classmethod
    def from_api_object(
            cls,
            req: kclient.V1LabelSelectorRequirement,
            ) -> LabelExpression:
        """
        Convert a V1LabelSelectorRequirement into a LabelSelector.

        :param req: The requirement.
        :raises KeyError: If a required key is missing
        :raises ValueError: If any value is invalid
        :raises ValueError: If values is missing or set to None but the
            operator requires a (possibly empty) set of values.
        :return: The resulting expression
        :rtype: :class:`~.LabelExpression`
        """
        return cls(
            key=req.key,
            operator=SelectorOperator(req.operator),
            values=req.values,
        )

    def as_api_selector(self) -> str:
        """
        Return a string fragment representing this expression for use with the
        Kubernetes API.

        If the operator is :attr:`~.SelectorOperator.IN` and only a single
        value is given, the shorthand version (``key=value``) is used instead
        of a set expression.
        """
        if self.operator == SelectorOperator.EXISTS:
            return self.key
        elif self.operator == SelectorOperator.NOT_EXISTS:
            return f"!{self.key}"
        elif self.operator == SelectorOperator.IN:
            if self.values is None:
                raise AssertionError("values need to be specified")
            if len(self.values) == 1:
                return f"{self.key}={self.values[0]}"
            values = ", ".join(self.values)
            return f"{self.key} in ({values})"
        elif self.operator == SelectorOperator.NOT_IN:
            if self.values is None:
                raise AssertionError("values need to be specified")
            values = ", ".join(self.values)
            return f"{self.key} notin ({values})"
        raise NotImplementedError

    def object_matches(self, labels: typing.Mapping[str, str]) -> bool:
        """
        Test whether the labels of an object satisfy the expression.

        :param object_labels: The labels of an object as key-value pair
            mapping.
        :return: True if the labels satisfy the expression`, false otherwise.
        """
        if self.operator == SelectorOperator.EXISTS:
            return self.key in labels
        elif self.operator == SelectorOperator.NOT_EXISTS:
            return self.key not in labels
        elif self.operator == SelectorOperator.IN:
            if self.values is None:
                raise AssertionError("values need to be specified")
            try:
                value = labels[self.key]
            except KeyError:
                return False
            return value in self.values
        elif self.operator == SelectorOperator.NOT_IN:
            if self.values is None:
                raise AssertionError("values need to be specified")
            try:
                value = labels[self.key]
            except KeyError:
                return True
            return value not in self.values
        raise NotImplementedError


@dataclasses.dataclass(frozen=True)
class LabelSelector:
    """
    Match a set of object labels against the label selector.

    :param match_labels: A dictionary of labels to match on objects.
    :param match_expressions: A collection of expressions.

    When evaluating the selector, the expressions generated from `match_labels`
    and the expressions given via `match_expressions` are ANDed.

    .. note::

        The semantics of the :class:`LabelSelector` are very similar to the
        selectors found in Kubernetes and which are represented by
        :class:`~.V1LabelSelector`.

        This class adds helper methods to work with such selectors.

    .. automethod:: from_dict

    .. automethod:: from_api_object

    .. automethod:: object_matches

    .. automethod:: as_api_selector
    """
    match_expressions: typing.Tuple[LabelExpression]

    def __init__(self, *,
                 match_labels: typing.Mapping[str, str] = {},
                 match_expressions: typing.Collection[LabelExpression] = ()):
        super().__init__()
        expressions = tuple(
            LabelExpression(
                key=key,
                operator=SelectorOperator.IN,
                values=(value,),
            )
            for key, value in match_labels.items()
        ) + tuple(match_expressions)
        object.__setattr__(self, "match_expressions", expressions)

    @classmethod
    def from_dict(self, label_selector: typing.Mapping) -> LabelSelector:
        """
        Construct a :class:`LabelSelector` from a dictionary.

        :param label_selector: The label selector to parse.
        :raises ValueError: if the `label_selector` has an unsupported format

        The `label_selector` must be a dictionary with the following key:

        - `matchLabels`: a dictionary of label pairs to match
        - `matchExpressions`: a list of :class:`~.V1LabelSelectorRequirement`
          JSON objects, i.e. dictionaries with ``key``, ``operator``, and
          ``values``  keys.

          .. seealso::

              :class:`.LabelExpression`

        .. note::

            The format of `label_selector` is the same as the V1LabelSelector
            of the Kubernetes API.
        """
        label_selector = dict(label_selector)
        match_labels = label_selector.pop("matchLabels", {})
        match_expressions_raw = label_selector.pop("matchExpressions", [])
        if label_selector:
            raise ValueError("unsupported match expression")
        match_expressions = tuple(
            LabelExpression.from_dict(expr)
            for expr in match_expressions_raw
        )
        return LabelSelector(match_labels=match_labels,
                             match_expressions=match_expressions)

    @classmethod
    def from_api_object(
            self,
            label_selector: kclient.V1LabelSelector,
            ) -> LabelSelector:
        return LabelSelector(
            match_labels=label_selector.match_labels or {},
            match_expressions=[
                LabelExpression.from_api_object(expr)
                for expr in label_selector.match_expressions or []
            ],
        )

    def object_matches(self, object_labels: typing.Mapping[str, str]) -> bool:
        """
        Test if the labels of an object satisfy the selector.

        :param object_labels: The labels of an object as key-value pair
            mapping.
        :return: True if the labels satisfy the selector, false otherwise.
        """
        return all(expr.object_matches(object_labels)
                   for expr in self.match_expressions)

    def as_api_selector(self) -> str:
        """
        Return a Kubernetes API compatible string representation of this
        selector.

        :return: A string which can be passed to the API as `labelSelector`
            argument.
        """
        return ",".join(expr.as_api_selector()
                        for expr in self.match_expressions)


T = typing.TypeVar("T")


async def multi_selector_list(
        listfn: typing.Callable[..., typing.Coroutine[
            typing.Any, typing.Any, typing.Iterable[T]]],
        selectors: typing.Iterable[LabelSelector],
        ) -> typing.AsyncGenerator[T, None]:
    """
    Request and return all listings for the given selectors.

    :param listfn: Listing function (see below)
    :param selectors: The selectors to pass to the listing function

    For each selector in `selectors`, this calls `listfn` with the
    `label_selector` keyword argument set to the API string of the selector.

    The items from the function calls are yielded in an arbitrary order.
    """

    requests = [
        listfn(label_selector=selector.as_api_selector())
        for selector in selectors
    ]
    for items in await asyncio.gather(*requests):
        for item in items:
            yield item


def make_toleration(scheduling_key: str) -> typing.Mapping[str, typing.Any]:
    """
    Create a standard toleration for a scheduling key.
    """
    return {
        "key": scheduling_key,
        "operator": "Exists",
    }


def inject_scheduling_keys(
        pod_spec: typing.MutableMapping[str, typing.Any],
        scheduling_keys: typing.Collection[str]) -> None:
    """
    Add tolerations and a node affinity for the given set of scheduling keys.

    :param pod_spec: The specification of the Pod. Modified in-place.
    :param scheduling_keys: Collection of scheduling keys to apply to the Pod.
    :raises ValueError: if the `pod_spec` already contains scheduling
        requirements, either in ``nodeAffinity`` or ``nodeSelector``.

    The Pod spec is modified in-place.
    """
    if not scheduling_keys:
        return

    node_affinity = pod_spec.get("affinity", {}).get(
        "nodeAffinity", {},
    ).get("requiredDuringSchedulingIgnoredDuringExecution", {})
    if ("nodeSelectorTerms" in node_affinity or
            "nodeSelector" in pod_spec):
        raise ValueError(
            "conflict: scheduling keys given, but the Pod already has "
            "scheduling requirements set"
        )

    tolerations = pod_spec.setdefault("tolerations", [])
    tolerations.extend(map(make_toleration, scheduling_keys))

    node_affinity = pod_spec.setdefault("affinity", {}).setdefault(
        "nodeAffinity", {},
    ).setdefault("requiredDuringSchedulingIgnoredDuringExecution", {})
    node_affinity["nodeSelectorTerms"] = [
        {
            "matchExpressions": [
                {
                    "key": key,
                    "operator": "Exists",
                    "values": [],
                },
            ],
        }
        for key in scheduling_keys
    ]


class ResourceReference(typing.NamedTuple):
    """
    Reference a resource by its API endpoint.

    In contrast to the :class:`kclient.V1ObjectReference`,
    this reference directly refers to an API endpoint; the Kubernetes object
    reference uses `kind` instead of `plural`, which can not be used
    interchangably.
    """

    api_version: str
    plural: str
    namespace: typing.Optional[str]
    name: str

    @classmethod
    def config_map(cls, namespace: str, name: str) -> ResourceReference:
        return cls(
            api_version="v1",
            plural="configmaps",
            namespace=namespace,
            name=name,
        )

    @classmethod
    def secret(cls, namespace: str, name: str) -> ResourceReference:
        return cls(
            api_version="v1",
            plural="secrets",
            namespace=namespace,
            name=name,
        )

    @classmethod
    def persistent_volume_claim(
            cls,
            namespace: str, name: str) -> ResourceReference:
        return cls(
            api_version="v1",
            plural="persistentvolumeclaims",
            namespace=namespace,
            name=name,
        )


@functools.singledispatch
def extract_pod_references(
        pod_spec: typing.Union[dict, kclient.V1PodSpec],
        namespace: str,
        ) -> typing.Collection[ResourceReference]:
    """
    Extract all referenced resources from a Pod specification.

    :param pod_spec: The pod specification.
    :type pod_spec: :class:`dict` or
        :class:`kclient.V1PodSpec`
    :param namespace: The namespace in which the Pod exists.
    :type namespace: :class:`str`
    :return: A collection of references to the resources used by the Pod.

    This function supports extracting references to config maps, secrets and
    persistent volume claims in environment variables and volumes of the Pod.
    Other resource references are currently not supported.

    .. note::

        This function expects a Pod spec, not a full Pod body.

    The `namespace` is required to fill in the
    :attr:`.ResourceReference.namespace` for those references where no explicit
    namespace is given in the Pod spec.
    """

    data = json.dumps(pod_spec)
    wrapper = types.SimpleNamespace()
    wrapper.data = data
    client = kclient.ApiClient()
    try:
        pod_spec_obj = client.deserialize(
            wrapper,
            kclient.V1PodSpec,
        )
    finally:
        # XXX: https://github.com/tomplus/kubernetes_asyncio/issues/128
        asyncio.create_task(client.close())

    return extract_pod_references_from_api_object(
        pod_spec_obj,
        namespace,
    )


@extract_pod_references.register(kclient.V1PodSpec)
def extract_pod_references_from_api_object(
        pod_spec: kclient.V1PodSpec,
        namespace: str,
        ) -> typing.Collection[ResourceReference]:
    result = set()
    for container in itertools.chain(pod_spec.containers,
                                     pod_spec.init_containers or []):
        for env in container.env or []:
            if env.value_from is None:
                continue
            if env.value_from.config_map_key_ref is not None:
                result.add(ResourceReference.config_map(
                    namespace=namespace,
                    name=env.value_from.config_map_key_ref.name,
                ))
            if env.value_from.secret_key_ref is not None:
                result.add(ResourceReference.secret(
                    namespace=namespace,
                    name=env.value_from.secret_key_ref.name,
                ))
        for env_from in container.env_from or []:
            if env_from.config_map_ref is not None:
                result.add(ResourceReference.config_map(
                    namespace=namespace,
                    name=env_from.config_map_ref.name,
                ))
            if env_from.secret_ref is not None:
                result.add(ResourceReference.secret(
                    namespace=namespace,
                    name=env_from.secret_ref.name,
                ))

    for volume in pod_spec.volumes or []:
        if volume.config_map is not None:
            result.add(ResourceReference.config_map(
                namespace=namespace,
                name=volume.config_map.name,
            ))
        if volume.secret is not None:
            result.add(ResourceReference.secret(
                namespace=namespace,
                name=volume.secret.secret_name,
            ))
        if volume.persistent_volume_claim is not None:
            result.add(ResourceReference.persistent_volume_claim(
                namespace=namespace,
                name=volume.persistent_volume_claim.claim_name,
            ))
        if volume.projected is not None:
            for source in (volume.projected.sources or []):
                if source.config_map is not None:
                    result.add(ResourceReference.config_map(
                        namespace=namespace,
                        name=source.config_map.name,
                    ))
                if source.secret is not None:
                    result.add(ResourceReference.secret(
                        namespace=namespace,
                        name=source.secret.name,
                    ))
    return result


def _extract_cds_volume_template_references(
        cds_volume_template: typing.Mapping,
        namespace: str,
        ) -> typing.Collection[ResourceReference]:
    try:  # nosemgrep we are optimistic here and just try to find something
        config_map = cds_volume_template["configMap"]
    except KeyError:
        pass
    else:
        return [
            ResourceReference.config_map(
                namespace=namespace,
                name=config_map["name"],
            ),
        ]

    try:  # nosemgrep we are optimistic here and just try to find something
        secret = cds_volume_template["secret"]
    except KeyError:
        pass
    else:
        return [
            ResourceReference.secret(
                namespace=namespace,
                name=secret["secretName"],
            ),
        ]

    try:  # nosemgrep we are optimistic here and just try to find something
        pvc = cds_volume_template["persistentVolumeClaim"]
    except KeyError:
        pass
    else:
        return [
            ResourceReference.persistent_volume_claim(
                namespace=namespace,
                name=pvc["claimName"],
            ),
        ]

    try:  # nosemgrep we are optimistic here and just try to find something
        projected = cds_volume_template["projected"]
    except KeyError:
        pass
    else:
        result = set()
        keymap = [("configMap", "configmaps"), ("secret", "secrets")]
        for source in projected["sources"]:
            for key, plural in keymap:
                try:  # nosemgrep same optimistic approach
                    ref = source[key]
                except KeyError:
                    continue
                result.add(ResourceReference(
                    api_version="v1",
                    plural=plural,
                    namespace=namespace,
                    name=ref["name"],
                ))
        return result

    return []


def extract_cds_references(
        cds_spec: typing.Mapping,
        namespace: str,
        ) -> typing.Collection[ResourceReference]:
    """
    Extract all referenced resources from a ConfiguredDaemonSet (CDS)
    specification.

    :param cds_spec: The cds specification.
    :type cds_spec: :class:`dict`
    :param namespace: The namespace in which the CDS exists.
    :type namespace: :class:`str`
    :return: A collection of references to the resources used by the CDS.

    This function supports extracting references to config maps, secrets and
    persistent volume claims in environment variables and volumes of the Pod,
    and the volume templates in the CDS itself. Other resource references are
    currently not supported.

    .. note::

        This function expects a CDS spec, not a full CDS body.

    The `namespace` is required to fill in the
    :attr:`.ResourceReference.namespace` for those references where no explicit
    namespace is given in the spec.
    """
    result: typing.Set[ResourceReference] = set()
    result.update(extract_pod_references(
        cds_spec.get("template", {}).get("spec", {}),
        namespace,
    ))

    for template in cds_spec.get("volumeTemplates", []):
        for node_volume in template.get("nodeMap", {}).values():
            result.update(
                _extract_cds_volume_template_references(
                    node_volume["template"],
                    namespace,
                )
            )
        if "default" in template:
            result.update(
                _extract_cds_volume_template_references(
                    template["default"]["template"],
                    namespace,
                )
            )

    return result


def split_api_version(s: str) -> typing.Tuple[str, str]:
    """
    Split a Kubernetes API version string into its API group and version
    parts.

    :param s: An API version string, such as ``v1`` or `apps/v1``.
    :return: The separate string as tuple of API group and version.

    If the API version string refers to the core API group (i.e. only consits
    of a version number), the API group is returned as empty string.
    """
    try:
        api_group, version = s.split("/", 1)
    except ValueError:
        api_group = ""
        version = s
    return api_group, version


def join_api_version(api_group: str, version: str) -> str:
    """
    Compose API group and version into a single Kubernetes API version string.

    :param api_group: The API group. To refer to the core API group, use the
        empty string.
    :param version: The version string.
    :return: The correctly composed API version string.
    """
    if api_group == "":
        return version
    return f"{api_group}/{version}"


def encode_secret_data(
        data: typing.Mapping[str, typing.Union[bytes, str]],
        encoding: str = "utf-8",
        ) -> typing.MutableMapping[str, str]:
    return {
        k: base64.b64encode(
            v if isinstance(v, bytes) else v.encode(encoding)
        ).decode("ascii")
        for k, v in data.items()
    }


def decode_secret_data(
        data: typing.Mapping[str, str],
        encoding: str = "utf-8",
        ) -> typing.MutableMapping[str, str]:
    return {
        k: base64.b64decode(v).decode(encoding)
        for k, v in data.items()
    }


CLUSTER_DOMAIN_ENV_VAR = "YAOOK_OP_CLUSTER_DOMAIN"


@functools.lru_cache(maxsize=1)
def get_cluster_domain() -> str:
    if CLUSTER_DOMAIN_ENV_VAR in os.environ:
        return os.environ[CLUSTER_DOMAIN_ENV_VAR]
    try:
        response = socket.gethostbyname_ex("kubernetes.default.svc")
    except socket.gaierror:
        # We are probably not running inside a cluster
        raise KeyError("No DNS Response for kubernetes.default.svc. Maybe we "
                       "are not running inside a cluster. You can set "
                       f"{CLUSTER_DOMAIN_ENV_VAR} to override it")
    fqdn = response[0]
    if fqdn.startswith("kubernetes.default.svc."):
        # Removes 'kubernetes.default.svc.'
        return fqdn[23:]
    raise ValueError("DNS Response is returning invalid answer: '%s'" % fqdn)


def is_k8s_regex_valid(string: str) -> bool:
    regex = re.compile("(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?$")
    return regex.match(string) is not None


def matches_node_selector(
        node_selectors: typing.List[typing.Mapping],
        node_labels: typing.Mapping[str, str],
        ) -> bool:
    """
    Checks if a given set of labels matches one node selectors of a list
    """
    selectors = (
        LabelSelector.from_dict(
            node_selector
        )
        for node_selector in node_selectors
    )
    return any(selector.object_matches(node_labels)
               for selector in selectors)


DATE_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def parse_timestamp(s: str) -> datetime:
    """Parse a timestamp that is in the yaook date format"""
    return datetime.strptime(s, DATE_FORMAT)


def format_timestamp(dt: datetime) -> str:
    """Create a timestamp in the yaook date format"""
    return dt.strftime(DATE_FORMAT)
