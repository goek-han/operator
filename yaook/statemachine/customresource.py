#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import collections
import copy
import dataclasses
import enum
import logging
import typing

from datetime import datetime

import kubernetes_asyncio.client as kclient

from . import (api_utils, context, exceptions, interfaces, resources,
               statemachine, versioneddependencies, version_utils)


T = typing.TypeVar("T")


def _build_resource_usage_report(
        resources_by_type: typing.Mapping[
            typing.Tuple[str, str],
            typing.Collection[typing.Tuple[str, str]],
        ]) -> str:
    report = []
    for (api_version, plural), used_resources in sorted(
            resources_by_type.items(),
            key=lambda x: x[0]):
        report.append(f"  {plural}/{api_version}")
        report.extend(
            f"    {namespace}/{name}"
            for namespace, name in sorted(used_resources)
        )
    return "\n".join(report)


class CommonConditionTypes(enum.Enum):
    CONVERGED = "Converged"
    GARBAGE_COLLECTED = "GarbageCollected"


@dataclasses.dataclass(frozen=True)
class ConditionUpdate:
    type_: str
    reason: str
    message: str
    status: typing.Union[str, bool]
    last_transition_time: typing.Optional[datetime] = None
    last_update_time: typing.Optional[datetime] = None


async def update_status(
        interface: interfaces.ResourceInterfaceWithStatus[typing.Mapping],
        namespace: str,
        name: str,
        *,
        phase: typing.Optional[context.Phase] = None,
        generation: typing.Optional[int] = None,
        updated_generation: typing.Optional[int] = None,
        conditions: typing.Collection[ConditionUpdate] = [],
        additional_patch: resources.ResourceBody = {},
        ) -> typing.Mapping[typing.Any, typing.Any]:
    while True:
        body = dict(await interface.read_status(namespace, name))
        status = body.setdefault("status", {})
        status.setdefault("observedGeneration", 0)
        status.setdefault("phase", context.Phase.CREATED.value)
        status.update(additional_patch)
        if phase is not None:
            status["phase"] = phase.value
        if generation is not None:
            status["observedGeneration"] = generation
        if updated_generation is not None:
            status["updatedGeneration"] = updated_generation

        existing_conditions = {
            condition["type"]: condition
            for condition in status.get("conditions", [])
        }

        for condition in conditions:
            try:
                existing = existing_conditions[condition.type_]
            except KeyError:
                existing = None
            cond_status = str(condition.status)
            if condition.last_update_time is None:
                last_update_time = datetime.utcnow()
            else:
                last_update_time = condition.last_update_time
            if condition.last_transition_time is None:
                if existing is not None:
                    if existing["status"] != cond_status:
                        last_transition_time = last_update_time
                    else:
                        last_transition_time = api_utils.parse_timestamp(
                            existing["lastTransitionTime"]
                        )
                else:
                    last_transition_time = last_update_time
            else:
                last_transition_time = condition.last_transition_time
            existing_conditions[condition.type_] = {
                "type": condition.type_,
                "reason": condition.reason,
                "message": condition.message,
                "status": cond_status,
                "lastUpdateTime": api_utils.format_timestamp(last_update_time),
                "lastTransitionTime":
                    api_utils.format_timestamp(last_transition_time),
            }

        status["conditions"] = list(existing_conditions.values())

        try:
            return await interface.replace_status(namespace, name, body)
        except kclient.ApiException as exc:
            if exc.status == 409:  # resourceVersion mismatch?
                continue
            raise


async def update_ctx_status(
    interface: interfaces.ResourceInterfaceWithStatus[typing.Mapping],
    ctx: context.Context,
    *,
    phase: typing.Optional[context.Phase] = None,
    generation: typing.Optional[int] = None,
    updated_generation: typing.Optional[int] = None,
    conditions: typing.Collection[ConditionUpdate] = [],
    additional_patch: resources.ResourceBody = {},
) -> context.Context:
    """
    returns the updated Context
    """
    parent = await update_status(
        interface,
        ctx.namespace,
        ctx.parent_name,
        phase=phase,
        generation=generation,
        updated_generation=updated_generation,
        conditions=conditions,
        additional_patch=additional_patch,
    )
    return dataclasses.replace(ctx, parent=parent)


class CustomResource:
    API_GROUP: str
    API_GROUP_VERSION: str
    PLURAL: str
    KIND: str
    ADDITIONAL_PERMISSIONS: typing.ClassVar[typing.Collection[typing.Tuple[
        bool,  # cluster_wide
        str, str,  # api_group, plural
        typing.Collection[str],  # verbs
    ]]] = ()

    def __init__(self, *, logger: logging.Logger,
                 assemble_sm: bool = False,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.sm = statemachine.StateMachine(self)
        self.logger = logger

        # all of these must be set
        self.API_GROUP
        self.API_GROUP_VERSION
        self.PLURAL
        self.KIND

        if assemble_sm:
            self._assemble_sm()

    def _assemble_sm(self):
        for entry in type(self).__dict__.values():
            if isinstance(entry, resources.Resource):
                self.sm.add_state(entry, entry._dependencies)

    def get_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterfaceWithStatus[typing.Mapping]:
        return interfaces.custom_resource_interface(
            self.API_GROUP,
            self.API_GROUP_VERSION,
            self.PLURAL,
            api_client,
        )

    @classmethod
    def get_versioned_dependency(cls):
        dependencies = []
        for entry in cls.__dict__.values():
            if isinstance(entry, versioneddependencies.VersionedDependency):
                dependencies.append(entry)
        return dependencies

    async def _resolve_dependency_versions(self, ctx: context.Context) -> None:
        for entry in self.get_versioned_dependency():
            version = await entry.get_latest_version(ctx)
            url = await entry.get_versioned_url(ctx)
            self.logger.debug(
                "Using %s for the dependency %s" % (version, url))

    async def reconcile(
            self,
            ctx: context.Context,
            generation: int) -> None:
        """
        Reconcile all states of this Custom Resource’s state machine.
        """

        ctx.logger.info("starting to reconcile")

        intf = self.get_resource_interface(ctx.api_client)
        additional_patch: typing.Dict[str, str] = {}
        updated_generation: typing.Optional[int] = None
        await update_status(
            intf,
            ctx.namespace, ctx.parent_name,
            phase=context.Phase.UPDATING,
            generation=generation,
            conditions=[ConditionUpdate(
                type_=CommonConditionTypes.CONVERGED.value,
                reason="InProgress",
                message="",
                status=False,
            )],
        )
        try:
            await self._validate_config(ctx)
            blocking = await self.sm.ensure(ctx)
        except exceptions.ConfigurationInvalid as exc:
            await update_status(
                intf,
                ctx.namespace, ctx.parent_name,
                phase=context.Phase.INVALID_CONFIGURATION,
                generation=generation,
                conditions=[ConditionUpdate(
                    type_=CommonConditionTypes.CONVERGED.value,
                    reason="ConfigurationInvalid",
                    message=exc.message,
                    status=False,
                )],
            )
        except Exception as exc:
            await update_status(
                intf,
                ctx.namespace, ctx.parent_name,
                phase=context.Phase.BACKING_OFF,
                generation=generation,
                conditions=[ConditionUpdate(
                    type_=CommonConditionTypes.CONVERGED.value,
                    reason="ReconcileFailed",
                    message=str(exc),
                    status=False,
                )],
            )
            raise
        else:
            if blocking:
                phase = context.Phase.WAITING_FOR_DEPENDENCY
                condition = ConditionUpdate(
                    type_=CommonConditionTypes.CONVERGED.value,
                    reason="Dependency",
                    message=", ".join(
                        f"waiting for {resource.component}"
                        if message is None else message
                        for resource, message in blocking.items()
                    ),
                    status=False,
                )
            else:
                phase, condition =\
                    await self._get_successful_update_status(ctx)
                additional_patch = self._get_successful_update_patch(ctx)
                updated_generation = self._get_successful_updated_generation(
                    ctx, generation
                )
            try:
                await update_status(
                    intf,
                    ctx.namespace, ctx.parent_name,
                    phase=phase,
                    generation=generation,
                    updated_generation=updated_generation,
                    conditions=[condition],
                    additional_patch=additional_patch
                )
            except kclient.ApiException as exc:
                if exc.status != 404:
                    raise

                # the resource was deleted; this happens if this reconcile
                # run removed the last finalizer. It is not critical and we
                # don’t want to log a full exception here to not pollute
                # the logs.
                if ctx.parent["metadata"]["deletionTimestamp"] is not None:
                    self.logger.info("resource deleted successfully")
                else:
                    self.logger.warning(
                        "resource vanished unexpectedly during or at the "
                        "end of the reconcile run"
                    )
            else:
                ctx.logger.debug(
                    "triggering cleanup after reconcile for %s/%s of %s",
                    ctx.namespace, ctx.parent_name, self,
                )
                await self._cleanup(ctx)

        ctx.logger.info("reconcile finished")

    async def _validate_config(self, ctx: context.Context) -> None:
        """
        Allows to validate configuration before a reconcile is run.
        """
        await self._resolve_dependency_versions(ctx)

    async def _get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[
                typing.Tuple[str, str],
                typing.Collection[typing.Tuple[str, str]],
            ]:
        results = await asyncio.gather(*(
            state.get_used_resources(ctx)
            for state in self.sm.states
        ))
        result = collections.defaultdict(set)
        for used_resources in results:
            for resource in used_resources:
                result[resource.api_version, resource.plural].add(
                    (resource.namespace, resource.name),
                )
        return result

    async def _get_successful_update_status(self,
                                            ctx: context.Context,
                                            ) -> typing.Tuple[context.Phase,
                                                              ConditionUpdate]:
        """
        Define the status of the resource after it fully finished reconciling.
        """
        phase = context.Phase.UPDATED
        condition = ConditionUpdate(
            type_=CommonConditionTypes.CONVERGED.value,
            reason="Success",
            message="",
            status=True,
        )
        return phase, condition

    def _get_successful_update_patch(self,
                                     ctx: context.Context,
                                     ) -> typing.Dict[str, str]:
        """
        Define additional values that are set in the status of the resource
        after a sucessfull reconcile.
        """
        return {}

    def _get_successful_updated_generation(
        self,
        ctx: context.Context,
        generation: int,
    ) -> typing.Optional[int]:
        """
        Define the updated generation of the resource after it fully finished
        reconciling, if the generation should not be updated None must be
        returned.
        """
        return generation

    async def _cleanup_orphaned_components(self, ctx: context.Context) -> None:
        """
        Delete resources orphaned by a change in the operator structure.
        """

    async def _cleanup(self, ctx: context.Context) -> None:
        """
        Delete all unused and orphaned resources.

        Orphaned resources exist in two categories:

        - Copy-on-write resources where a new version was created
        - Resources from older releases which are not needed anymore

        It is safe to run this method in parallel. It cannot be run in parallel
        with :meth:`reconcile`, thus it is called at the end of each reconcile.
        """
        resources_by_type = await self._get_used_resources(ctx)

        for state in self.sm.states:
            if not isinstance(state, (resources.KubernetesResource,
                                      resources.Optional)):
                continue
            await state.cleanup_orphans(
                ctx,
                protect=resources_by_type,
            )

        intf = self.get_resource_interface(ctx.api_client)
        await update_status(
            intf,
            ctx.namespace, ctx.parent_name,
            conditions=[ConditionUpdate(
                type_=CommonConditionTypes.GARBAGE_COLLECTED.value,
                reason="Success",
                message="{} resources in use".format(
                    sum(len(resources)
                        for resources in resources_by_type.values())
                ),
                status=True,
            )],
        )

        ctx.logger.debug(
            "resource usage report:\n%s",
            _build_resource_usage_report(resources_by_type),
        )

    def get_listeners(self) -> typing.List[context.Listener]:
        return self.sm.get_listeners()

    async def get_all(
            self,
            api_client: kclient.ApiClient,
            ) -> typing.Iterable[typing.Mapping]:
        co = kclient.CustomObjectsApi(api_client)
        return list(map(
            api_utils.extract_metadata,
            (await co.list_cluster_custom_object(
                self.API_GROUP,
                self.API_GROUP_VERSION,
                self.PLURAL,
                ))["items"]
        ))

    async def build_context(
            self,
            api_client: kclient.ApiClient,
            namespace: str,
            name: str,
            field_manager: str,
            ) -> typing.Tuple[context.Context, int]:
        body = await self.get_resource_interface(api_client).read(
            namespace,
            name,
        )
        return context.Context(
            parent=body,
            parent_intf=self.get_resource_interface(api_client),
            namespace=namespace,
            api_client=api_client,
            logger=self.logger,
            instance=None,
            instance_data=None,
            field_manager=field_manager,
        ), body["metadata"].get("generation", 0)

    def __repr__(self) -> str:
        return "<CustomResource {}.{}/{}>".format(
            self.PLURAL,
            self.API_GROUP,
            self.API_GROUP_VERSION,
        )


class UpgradeStrategy(enum.Enum):
    SINGLE_RELEASE = "SingleRelease"
    """
    Allows targetRelease to be only one version higher than installedRelease
    and prevents larger version jumps.
    """

    MULTIPLE_RELEASE = "MultipleRelease"
    """
    Allows targetRelease to be several versions higher than installedRelease.
    Here for "nextRelease" is set to the current release to be installed.
    For example, if the currently installed version is A and the targetRelease
    is set to D then nextRelease is set to B until all dependencies are met,
    once that is the case installedRelease is set to B and nextRelease is set
    to C. This continues until the targetRelease is reached.
    All intermediate versions must be in VALID_UPGRADE_TARGETS and RELEASES.
    """


class ReleaseAwareCustomResource(CustomResource):
    # An ordered list of releases the operator supports
    RELEASES: typing.List[str]
    VALID_UPGRADE_TARGETS: typing.List[str]
    UPGRADE_STRATEGY: UpgradeStrategy = UpgradeStrategy.SINGLE_RELEASE

    def __init__(
        self,
        *,
        logger: logging.Logger,
        assemble_sm: bool = False,
        **kwargs: typing.Any,
    ):
        super().__init__(logger=logger, assemble_sm=assemble_sm, **kwargs)

        self.RELEASES  # MUST be set
        self.VALID_UPGRADE_TARGETS  # MUST be set
        self.UPGRADE_STRATEGY  # MUST be set

        # just a sanity check; if anyone got a use case for that, shout.
        broken_releases = set(self.VALID_UPGRADE_TARGETS) - set(self.RELEASES)
        if broken_releases:
            raise RuntimeError(
                "VALID_UPGRADE_TARGETS is invalid: contains releases not in"
                " RELEASES: {', '.join(sorted(broken_releases))}"
            )

    async def _get_successful_update_status(self,
                                            ctx: context.Context,
                                            ) -> typing.Tuple[context.Phase,
                                                              ConditionUpdate]:
        if self.UPGRADE_STRATEGY == UpgradeStrategy.SINGLE_RELEASE:
            return await super()._get_successful_update_status(ctx)

        # As long as we are still updating we have to make sure that our state
        # does not go to Success/Updated so that services that rely on us
        # continue to wait until we are on our targetRelease.
        if version_utils.get_next_release(
                ctx) == version_utils.get_target_release(ctx):
            return await super()._get_successful_update_status(ctx)

        return context.Phase.UPDATING, ConditionUpdate(
            type_=CommonConditionTypes.CONVERGED.value,
            reason="Updating",
            message="",
            status=True,
        )

    def _get_successful_updated_generation(
        self,
        ctx: context.Context,
        generation: int,
    ) -> typing.Optional[int]:
        if self.UPGRADE_STRATEGY == UpgradeStrategy.SINGLE_RELEASE:
            return super()._get_successful_updated_generation(ctx, generation)

        # For UpgradeStrategy.MULTIPLE_RELEASE, we want to make sure that
        # resources waiting on us as a dependency do not continue until we
        # are on the target_release. We use the updated_generation and update
        # this only as soon as we have reached the target_release.
        installed_release = version_utils.get_next_release(ctx)
        target_release = version_utils.get_target_release(ctx)
        if installed_release == target_release:
            return super()._get_successful_updated_generation(ctx, generation)

        # We are not yet on the target_release, keep waiting
        return None

    async def _validate_config(self, ctx: context.Context) -> None:
        await super()._validate_config(ctx)

        target_release = version_utils.get_target_release(ctx)
        if target_release not in self.RELEASES:
            raise exceptions.ConfigurationInvalid(
                f"The release {target_release} is unknown to this operator"
            )

        if not version_utils.is_upgrading(ctx):
            # not upgrading, nothing to validate
            return

        installed_release = version_utils.get_installed_release(ctx)
        assert installed_release is not None  # validated by is_upgrading

        if target_release not in self.VALID_UPGRADE_TARGETS:
            raise exceptions.ConfigurationInvalid(
                f"upgrade from {installed_release} to {target_release} not "
                "implemented"
            )
        installed_index = self.RELEASES.index(installed_release)
        target_index = self.RELEASES.index(target_release)
        if target_index < installed_index:
            raise exceptions.ConfigurationInvalid(
                "Downgrades are not supported. Current Version: "
                f"{installed_release}; Requested Version: {target_release}"
            )
        if self.UPGRADE_STRATEGY == UpgradeStrategy.SINGLE_RELEASE:
            if installed_index + 1 != target_index:
                raise exceptions.ConfigurationInvalid(
                    "Changes to the release do only support upgrading one "
                    f"Release at a time. Current Version: {installed_release};"
                    f" Requested Version: {target_release}"
                )
        if self.UPGRADE_STRATEGY == UpgradeStrategy.MULTIPLE_RELEASE:
            for current_index in range(installed_index + 1, target_index + 1):
                release = self.RELEASES[current_index]
                if release not in self.VALID_UPGRADE_TARGETS:
                    raise exceptions.ConfigurationInvalid(
                        f"Release {release} is not a valid upgrade target but"
                        " is needed to reach the target release"
                        f" {target_release}"
                    )

    def _replace_context_status(
        self, ctx: context.Context, patched_status: typing.Dict
    ) -> context.Context:
        parent = typing.cast(dict, copy.deepcopy(ctx.parent))
        status = parent.get("status", {})
        status.update(patched_status)
        parent["status"] = status
        return dataclasses.replace(ctx, parent=parent)

    async def get_unversioned_installed_release(
        self, ctx: context.Context
    ) -> str:
        """
        This function is called if the Deployment already exists longer
        than the lifetime of the operator and has no installedRelease set in
        the status. Therefore a previous and for the operator unknown version.
        At this point it is now possible to look at the existing
        Deployment and get the appropriate current installedRelease from
        the resources e.g. a StatefulSet or a deployment.
        """
        return self.RELEASES[0]

    async def discover_initial_installed_release(
        self, ctx: context.Context
    ) -> context.Context:
        """
        This function is called if no installedRelease is set in the
        deployment. installedRelease is essential for
        UpgradeStrategy.MULTIPLE_RELEASE. There are two cases we want to
        catch here:
        1. it is a fresh deployment, then we can directly use the
           targetRelease.
        2. it is an existing deployment without installedRelease set and
           accordingly an older version of the CustomResource. At this point
           it is important to know what the current installedRelease is even
           if it is not set in the status because depending on the current
           image the upgrade path may differ and we should not skip any
           releases between installedRelease and targetRelease.
        """
        intf = self.get_resource_interface(ctx.api_client)
        if self.UPGRADE_STRATEGY == UpgradeStrategy.SINGLE_RELEASE:
            return await update_ctx_status(
                intf,
                ctx,
                additional_patch={
                    version_utils.KEY_STATUS_INSTALLED_RELEASE:
                    version_utils.get_target_release(ctx),
                },
            )

        if ctx.parent.get("status", None) is not None:
            installed_release = await self.get_unversioned_installed_release(ctx)  # noqa: E501
            ctx = self._replace_context_status(
                ctx,
                {
                    version_utils.KEY_STATUS_INSTALLED_RELEASE:
                    installed_release,
                },
            )
            next_release = self.get_next_version(ctx)
        else:
            # If we do not have a status it is a fresh deployment
            # in which case we can deploy the target_release directly.
            target_release = version_utils.get_target_release(ctx)
            installed_release = target_release
            next_release = target_release

        return await update_ctx_status(
            intf,
            ctx,
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: installed_release,
                version_utils.KEY_STATUS_NEXT_RELEASE: next_release,
            },
        )

    def get_next_version(self, ctx: context.Context) -> str:
        """
        This function always increments from installed_release to
        target_release and returns the next_release to be rolled out until
        installed_release and target_release are equal.

        For this installed_release must always be set.
        """
        target_release = version_utils.get_target_release(ctx)
        installed_release = version_utils.get_installed_release(ctx)
        assert installed_release is not None
        if installed_release == target_release:
            return target_release

        idx = self.RELEASES.index(installed_release)
        return self.RELEASES[idx + 1]

    async def reconcile(self, ctx: context.Context, generation: int) -> None:
        if version_utils.get_installed_release(ctx) is None:
            ctx = await self.discover_initial_installed_release(ctx)

        if self.UPGRADE_STRATEGY == UpgradeStrategy.MULTIPLE_RELEASE:
            intf = self.get_resource_interface(ctx.api_client)
            if await self._needs_release_update(ctx, intf):
                updated_next_release = self.get_next_version(ctx)
                if version_utils.get_next_release(ctx) != updated_next_release:
                    ctx = await update_ctx_status(
                        intf,
                        ctx,
                        additional_patch={
                            version_utils.KEY_STATUS_NEXT_RELEASE:
                            updated_next_release
                        },
                    )

        await super().reconcile(ctx, generation)

        if (
            self.UPGRADE_STRATEGY == UpgradeStrategy.MULTIPLE_RELEASE
            and await self._needs_release_update(ctx, intf)
        ):
            raise exceptions.TriggerReconcile(
                f"Upgrade from {version_utils.get_installed_release(ctx)} to"
                f" {version_utils.get_target_release(ctx)}"
            )

    async def _needs_release_update(
        self,
        ctx: context.Context,
        interface: interfaces.ResourceInterfaceWithStatus[typing.Mapping],
    ) -> bool:
        ctx_parent = await interface.read_status(
            ctx.namespace, ctx.parent_name
        )
        phase = ctx_parent.get("status", {}).get("phase")
        if phase in [
            context.Phase.UPDATING.value,
            context.Phase.UPDATED.value,
        ]:
            installed_release = version_utils.get_installed_release(ctx)
            target_release = version_utils.get_target_release(ctx)
            return installed_release != target_release
        return False

    def _get_successful_update_patch(
        self,
        ctx: context.Context,
    ) -> typing.Dict[str, str]:
        if self.UPGRADE_STRATEGY == UpgradeStrategy.SINGLE_RELEASE:
            return {
                version_utils.KEY_STATUS_INSTALLED_RELEASE:
                version_utils.get_target_release(ctx)
            }

        # in case of 'UpgradeStrategy.MULTIPLE_RELEASE':
        # reconcile() ensures that `nextRelease` is set.
        # So we can use it here without concern.
        installed_release = version_utils.get_next_release(ctx)
        assert installed_release is not None
        ctx = self._replace_context_status(
            ctx,
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE:
                installed_release
            },
        )
        next_release = self.get_next_version(ctx)
        return {
            version_utils.KEY_STATUS_INSTALLED_RELEASE: installed_release,
            version_utils.KEY_STATUS_NEXT_RELEASE: next_release,
        }


class OneshotCustomResource(CustomResource):
    FINAL_STATE: typing.Optional[resources.FinalResource] = None

    async def reconcile(self, ctx: context.Context, generation: int) -> None:
        intf = self.get_resource_interface(ctx.api_client)
        status = await intf.read_status(ctx.namespace, ctx.parent_name)
        phase = status.get("status", {}).get("phase")
        if phase in [context.Phase.COMPLETED.value,
                     context.Phase.FAILED.value]:
            self.logger.info(
                "reconcile for resource %s requested. It is a oneshot "
                "and already in its final state. Ignoring..." %
                ctx.parent_name
            )
            return
        return await super().reconcile(ctx, generation)

    async def _get_successful_update_status(self,
                                            ctx: context.Context,
                                            ) -> typing.Tuple[context.Phase,
                                                              ConditionUpdate]:
        phase = context.Phase.COMPLETED
        condition = ConditionUpdate(
            type_=CommonConditionTypes.CONVERGED.value,
            reason="Success",
            message="",
            status=True,
        )
        if self.FINAL_STATE:
            if not await self.FINAL_STATE.is_successful(ctx):
                phase = context.Phase.FAILED
                condition = ConditionUpdate(
                    type_=CommonConditionTypes.CONVERGED.value,
                    reason="Failed",
                    message="%s indicated a Failure" % self.FINAL_STATE,
                    status=True,
                )

        return phase, condition
