#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import certifi
import collections
import typing
from OpenSSL import crypto


def build_ca_configmap(
        usercerts: typing.Set[str] = set(),
        ) -> typing.Mapping[str, str]:
    """
    Generate a list of CA certificates for the use within a configmap.
    The CA certificates are sourced from the certifi list and include
    additional user certs.
    The Method can optionally write this ca list to a file and inject it
    to the python requests library.

    :param usercerts: A list of user certificates in pem format.
    """
    certs = set()
    lines: typing.List[str] = []
    hashcounter: collections.Counter = collections.Counter()

    capath = certifi.where()

    # Certifi provides us with the Mozilla root CA list
    with open(capath) as f:
        for line in f:
            line = line.strip()
            if line.startswith("#"):
                continue
            if line == "-----BEGIN CERTIFICATE-----":
                lines = []
            lines.append(line)
            if line == "-----END CERTIFICATE-----":
                certs.add('\n'.join(lines))

    certs |= usercerts

    output = {}
    # Since `certs` is a set it does not have a stable ordering. Without
    # sorting this will causes `ca-bundle.crt` to ordered be different on
    # every invocation. While this does not interfere with the working of
    # the certificates it causes the operator to think the content has
    # changed and therefor triggers a useless rolling restart of all services.
    output["ca-bundle.crt"] = '\n'.join(sorted(certs))

    for cert_pem in sorted(certs):
        try:
            cert = crypto.load_certificate(crypto.FILETYPE_PEM, cert_pem)
        except crypto.Error as e:
            raise ValueError(f"Invalid certificate in CA bundle: {e}")
        sn_hash = format(cert.subject_name_hash(), '02x')
        ca_id = hashcounter[sn_hash]
        hashcounter[sn_hash] = ca_id + 1
        ca_hash = f"{sn_hash}.{ca_id}"
        output[ca_hash] = cert_pem

    return output
